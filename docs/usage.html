<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <title>Typical usage</title>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    <link rel="stylesheet" href="style.css">
  </head>

  <body>
    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100"><a href="../index.html"><img src="T2M_logo.png" border="0" height="50" alt="Tab2MAGE logo"></a></td>
	    <td class="pagetitle">Typical usage</td>
	  </tr>
      </table>
    </div>

    <p class="text">This page gives examples of the typical ways in
    which the scripts in this package are invoked, and addresses some
    common pitfalls.</p>

    <div class="section">

      <div class="sectionhead"><a name="columns">Unknown Data File Columns</a></div>
      
    <p class="text">Once you have created your sample annotation spreadsheet, you
    are ready to run the tab2mage.pl script. In an ideal world, you
    could simply invoke the following command:</p>

    <pre>tab2mage.pl -e &lt;spreadsheet filename&gt; -t &lt;target directory to be created&gt;</pre>

    <p class="text">This command should be executed from within the directory
    containing the data files and the annotation spreadsheet.</p>

    <p class="text">In reality, the situation is more
    complicated. Manufacturers of scanning and image processing
    software change the format of their output data files from time to
    time. The most obvious example of this is when the column headings
    (quantitation types) within a data file are altered. Additionally,
    data files may have been modified in subsequent analyses, adding
    customized columns. This creates a problem, because for accurate
    encoding in MAGE-ML we need to know more information about these
    quantitation types than just their name. Tab2MAGE stores this
    information in a file named QT_list.txt within the installed
    library directory. This file contains the information gathered so
    far by ArrayExpress curators. A new version of this file is
    released with each Tab2MAGE release as more information becomes
    available. However, it is entirely possible that you will be using
    data files which contain unknown quantitation types.</p>

    <p class="text"><em>The default action for Tab2MAGE is to ignore unknown
    quantitation types.</em> The package was designed like this to
    allow very precise control of which data columns are exported to
    MAGE-ML, and which are discarded. Tab2MAGE includes a mechanism to
    customize its behavior so that a given site installation can set
    up a default set of data columns which are then always exported.</p>

    <p class="text">Any quantitation types which are discarded during Tab2MAGE
    processing will be reported in the <tt>tab2mage.log</tt> file, created in
    the target directory. To determine which columns are at risk
    before running the tab2mage.pl script, we recommend using the <a
    href="expt_check.html">experiment checker script</a> in Tab2MAGE mode:</p>

    <pre>expt_check.pl -e &lt;Tab2MAGE spreadsheet filename&gt;</pre>

    <p class="text">This script will run some checks on the data files and generate
    a series of log files. One of these, the <tt>expt_columnheadings.log</tt>
    file, lists the unknown columns which would be discarded in a
    Tab2MAGE run.</p>

    <p class="text"><em>For Tab2MAGE to export unknown quantitation types</em>, you must
    override the built-in quantitation type information using the <tt>-q</tt> or <tt>-Q</tt>
    command-line options:</p>

    <pre>expt_check.pl -q &lt;new QT file&gt; -e &lt;spreadsheet filename&gt;</pre>
    <pre>tab2mage.pl -q &lt;new QT file&gt; -e &lt;spreadsheet filename&gt; -t &lt;target directory to be created&gt;</pre>
      
    <p class="text"><em>Note</em> that using the script with the
    <tt>-q</tt> option will ignore the default QT information supplied
    with the script; you must, therefore, include all the QTs you wish
    to export in your new QT file. Alternatively, you may wish to use
    the <tt>-Q</tt> option, which will use the new QTs alongside those
    embedded in the Tab2MAGE package (but see the <a
    href="#one_sw_per_file">note below</a>). As a last resort in case
    of difficulties, the tab2mage.pl script also accepts a <tt>-k</tt>
    option, which will keep <em>all</em> the unknown QTs, including
    them in the output MAGE-ML as generic
    SpecializedQuantitationTypes.</p>

      <p class="text">The format of the QT file is described in <a
    href="ArrayExpress/Datafile/QT_list.html#quantitation_types">these
    QT file format notes</a>, and the <a
    href="../examples/other_QTs.txt">other_QTs.txt</a> file included
    with the package provides a set of examples. At its simplest,
    however, the file can be laid out in this form:</p>

    <pre>>>>Software name[Manufacturer]
QT name 1
QT name 2
QT name 3
QT name 4
QT name 5
QT name 6...</pre>

    <p class="text">(This example will create every quantitation type as a
    SpecializedQuantitationType object).</p>

      <p class="text">For correct creation of the appropriate
      QuantitationType subclasses, with suitable associations and
      attributes, please see the <a
      href="ArrayExpress/Datafile/QT_list.html#quantitation_types">QT
      file format notes</a>. </p>

      <p class="text"><em><a
      name="one_sw_per_file">Note:</a></em> scanning software packages
      often use column headings which are ambiguous in their
      origin. Some column headings (e.g., "Flags") are used by many
      different software manufacturers, and this can lead to
      ambiguities in assigning QTs to software type. As a result, the
      Tab2MAGE scripts have to assume that there is only one software
      type per data file. The desired quantitation types for a given
      data file must all be grouped together under a single <a
      href="ArrayExpress/Datafile/QT_list.html#quantitation_types">software
      group</a>. This prevents the creation of incorrect quantitation
      types for ambiguous column headings which are used by more than
      one software type.</p>

      <p CLASS="sectionfoot">[ <a href="javascript: history.back()">Back</a> ][ <a href="#top">Top of page</a> ]</p>

    </div>

    <div class="section">

      <div class="sectionhead"><a name="features">Feature Coordinate Checking</a></div>
      
    <p class="text">It is not uncommon to encounter a situation in
    which one or more data files contain features not represented in
    the corresponding array design. The experiment checker script will
    automatically check for this problem when run in Tab2MAGE or
    MIAMExpress mode. However, if you have only a set of data files
    and a suitable <a
    href="http://www.ebi.ac.uk/miamexpress/help/adf/">Array Definition
    File</a> (ADF) to check them against, you can run the script in
    stand-alone mode:</p>

    <pre>expt_check.pl -a &lt;ADF filename&gt; -s &lt;list of data files&gt;</pre>    

    <p class="text">Please see the <a
    href="expt_check.html">experiment checker</a> and <a
    href="tab2mage.html">tab2mage.pl</a> help notes for more
    information.</p>

      <p CLASS="sectionfoot">[ <a href="javascript: history.back()">Back</a> ][ <a href="#top">Top of page</a> ]</p>

    </div>

    <hr>
    <a href="http://sourceforge.net">
      <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" width="125" height="37" border="0" alt="SourceForge.net Logo" />
    </a>
    <br>
<!-- Created: Mon Jan 17 09:45:42 GMT 2005 -->
<!-- hhmts start -->
Last modified: Sun Dec 10 12:11:16 GMT 2006
<!-- hhmts end -->
  </body>
</html>
