#!/usr/bin/env perl
#
# t2m_vizualizer.pl
#
# A script to create a graph from a Tab2MAGE spreadsheet, allowing the
# user to visualize the relationships between biomaterials and
# hybridizations.
#
# Tim Rayner 2005 ArrayExpress Team, EBI
#
# $Id: t2m_visualize.pl 1519 2007-04-19 13:39:57Z tfrayner $
#

use strict;
use warnings;

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../index.html">
                <img src="T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Script detail: t2m_visualize.pl</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

t2m_visualize.pl - a rapid visualization script for Tab2MAGE
spreadsheets.

=head1 SYNOPSIS

 t2m_visualize.pl -e <Tab2MAGE spreadsheet file>

=head1 DESCRIPTION

This script simply parses a Tab2MAGE spreadsheet into a format
readable by the Graphviz "dot" application. If Graphviz has been
installed the script then attempts to generate a PNG file. This PNG
file contains a graph which represents the experiment which has been
coded. Both the tab2mage.pl and expt_check.pl scripts include this
functionality, but t2m_visualize.pl has two advantages: it does not
need the data files to be present, and it works much faster.

=head1 OPTIONS

=over 2

=item B<-e> C<spreadsheet filename>

The Tab2MAGE spreadsheet to visualize.

=item B<-f> C<font name>

Font to use in graph generation (optional). 

=item B<-v>

Print the version of the string.

=item B<-c>

Overwrite ("clobber") existing files.

=item B<-h>

Prints a short help text.

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2005.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

use Getopt::Long;
use Pod::Usage;

use ArrayExpress::Curator::Tab2MAGE;
use ArrayExpress::Curator::MAGE qw(unique_identifier);
use ArrayExpress::Curator::Visualize qw(dot_and_png);

###########
# GLOBALS #
###########
#
# Used to track the creation of unique objects.
#

my $PROGRAMNAME = "T2M_Visualizer";
my $VERSION     = "1.2";

my $logfile = lc($PROGRAMNAME) . ".log";

########
# MAIN #
########

my ( $spreadsheet, $want_version, $font, $clobber, $want_help );

GetOptions(
    "e|edf=s"   => \$spreadsheet,
    "f|font=s"  => \$font,
    "v|version" => \$want_version,
    "c|clobber" => \$clobber,
    "h|help"    => \$want_help,
);

if ($want_version) {
    print STDERR "This is $PROGRAMNAME v$VERSION\n\n";
    exit 255;
}

if ($want_help) {
    pod2usage(
	-exitval => 255,
	-output  => \*STDERR,
	-verbose => 1,
    );
}

unless ($spreadsheet) {
    pod2usage(
	-message => 'Please see "t2m_visualize.pl -h" for further help notes.',
	-exitval => 255,
	-output  => \*STDERR,
	-verbose => 0,
    );
}

my $tab2mage = ArrayExpress::Curator::Tab2MAGE->new({
    spreadsheet_filename => $spreadsheet,
    target_directory     => q{.},
});

# Parse the EDF
$tab2mage->read_edf( \*STDOUT );

# Create and link the mage objects
foreach my $datarow (@{ $tab2mage->get_hyb_section() } ) {
    $tab2mage->create_mage_objects( $datarow, unique_identifier );
}

# Create the dotfile
my $output_base = $spreadsheet;
$output_base =~ s/\.\w{3}$//;
$output_base ||= lc($PROGRAMNAME);    # Just in case

$clobber = dot_and_png( $tab2mage->get_bags(), $output_base, $font, $clobber );

exit $tab2mage->get_error();
