#!/usr/bin/env perl
#
# Script to use Tab2MAGE routines to convert data files to MC/MR/C/R format.
#
# Tim Rayner 2005, ArrayExpress team, European Bioinformatics Institute
#
# $Id: convert_to_mcmr.pl 1852 2007-12-13 10:14:27Z tfrayner $
#

use strict;
use warnings;

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

# Added to circumvent weird Exporter bug in 5.8.3 which caused
# crashing when requiring ArrayExpress::Datafile (which in turn uses
# ArrayExpress::Curator::Common).
use ArrayExpress::Curator::Common;

require ArrayExpress::Datafile;

unless (@ARGV) {

    print STDERR <<USAGE;
Usage: 
  
  convert_to_mcmr.pl <list of files to convert>

Converted files are named with a ".fixed" suffix.

Supported formats:

     GenePix
     Agilent
     ScanAlyze
     ArrayVision
     ScanArray
     QuantArray
     Spotfinder
     BlueFuse
     UCSFSpot
     Nimblegen (use at your own risk)
     CodeLink
     AppliedBiosystems

USAGE

    exit;

}

FILENAME:
foreach my $filename (@ARGV) {

    my $file = ArrayExpress::Datafile->new({
	name            => $filename,
	data_type       => 'raw',
	array_design_id => 'UNKNOWN',
    });

    unless ( -T $filename ) {
        warn("ERROR: File $filename appears to be binary. Skipping.\n");
        next FILENAME;
    }

    # Figure out what kind of file we have, get column headings and indices
    $file->parse_header();

    print STDOUT (
        "File $filename is " . $file->get_format_type() . " format.\n" );

    unless ( @{ $file->get_column_headings() } ) {
        print STDERR (
            "Error: No column headings for file " . $file->get_name() . "\n" );
    }

    # Convert the file to MC/MR/C/R coordinates. The file continues to
    # be referred to by the $input_fh filehandle.
    ( $file->get_format_type() eq 'Generic' ) && do {
        print STDOUT (" File needs no conversion. Skipping.\n");
        next FILENAME;
    };

    my $rc = $file->fix_known_text_format();

    # Success
    if ($rc) {

	my $input_fh = $file->get_filehandle();

        # Output the column headings and data
        open (my $output_fh, '>', "$filename.fixed")
            or die("Error opening output file $filename.fixed: $!\n");

        print $output_fh ( join( "\t", @{ $file->get_column_headings() } ),
            "\n" );

        while ( my $line = <$input_fh> ) { print $output_fh $line; }

	close ($output_fh) or die("Error closing filehandle: $!\n");

    }

    # Failure
    else {

        # Fallback error report
        warn(     "ERROR: Unrecognized file format "
                . $file->get_format_type()
                . ". Skipping.\n" );
        next FILENAME;

    }

}
