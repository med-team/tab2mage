#!/usr/bin/env perl
#
# tab2mage.pl
#
# A script to generate MAGE-ML from a set of datafiles. A template
# spreadsheet file (tab-delimited text) is required.
#
# Tim Rayner 2004 ArrayExpress Team, EBI
#
# $Id: tab2mage.pl 1734 2007-09-05 15:41:17Z tfrayner $
#

use strict;
use warnings;

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

use English qw( -no_match_vars );
use Benchmark;
use File::Spec;
use Getopt::Long qw(:config no_ignore_case);
use Pod::Usage;

use ArrayExpress::Curator::Tab2MAGE;
use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Visualize qw(dot_and_png);

########
# SUBS #
########

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../index.html">
                <img src="T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Script detail: tab2mage.pl</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

tab2mage.pl - a script to produce valid MAGE-ML from a set of
raw datafiles and a summary spreadsheet of defined format.

=head1 SYNOPSIS

 tab2mage.pl -e <spreadsheet_filename> -t <target_directory>

=head1 DESCRIPTION

This script is designed to take a set of unprocessed raw datafiles and
a spreadsheet providing the experiment metadata and generate valid
MAGE-ML. The supported data file formats are listed in the
accompanying documentation.

Important: the data files must each contain a column heading line
immediately preceding the start of the data. Typically, raw
unprocessed data files have this as standard. Do not remove this line,
as the script will use it to determine which QuantitationTypes to keep
and which to discard.

There are two auxilliary files which can be supplied alongside the
data files. The first, experiment summary spreadsheet describes the
way in which the experiment was performed. The second simply defines
the QuantitationTypes which are to be extracted from the data
files. This QuantitationType file is optional, however, as the script
is supplied with a set of defaults determined by a survey of incoming
QuantitationTypes by ArrayExpress curators. The script generates a log
file in the target directory which details which columns have been
ignored. 

=head1 EXPERIMENT SUMMARY FILE

The experiment summary file has a flexible format based around a set
of predefined column headers. Comments may be inserted using the '#'
character at the start of any line. The file consists of three
sections, each of which ends with a blank line:

=head2 Experiment section

This section contains top-level information about the experiment, such
as the title, description and accession number. The section is
constructed in two columns, with row names as described in
L<ArrayExpress::Curator::MAGE::Definitions/"SUPPORTED HEADINGS">.

=head2 Protocol section

Protocols are defined as needed in this section. The section is
organized into rows, with column headings as described in
L<ArrayExpress::Curator::MAGE::Definitions/"SUPPORTED HEADINGS">. If all of the protocols
used in the experiment have previously been loaded into ArrayExpress
and given accession numbers, this whole section can be omitted.

=head2 Hybridization section

This section contains the bulk of the experiment information. At its
simplest, each row describes the route taken from BioSource to output
data file. 

Column headings for BioMaterialCharacteristics and FactorValue should
be provided in the following form:

=over 2

=item BioMaterialCharacteristics[<MGED Ontology Category>] 

=item FactorValue[<MGED Ontology Category>] 

=back

Each of these headings should contain a valid Category subclass from
the MGED ontology. The values in the columns must likewise be valid
ontology entry values for these subclasses.

Multichannel (e.g., two-colour) data can be described by
entering each channel as a separate line. Pooling can be described at
multiple levels by using as many lines as necessary to describe all
the relationships between upstream and downstream samples. 

In a sense the Hybridization table can be compared to an SQL database
table in the way that it provides links between MAGE objects. Again,
the recognized column headings for this section are described in
L<ArrayExpress::Curator::MAGE::Definitions/"SUPPORTED HEADINGS">.

=head1 OPTIONS

=over 2

=item B<-e> C<filename>

The Tab2MAGE spreadsheet to be checked.

=item B<-t> C<directory>

The target directory to be created. This directory will contain the
MAGE-ML file and external data files ready for validation.

=item B<-n> C<accession>

Normally the script uses the experiment accession number from the
spreadsheet to be parsed. In cases where no accession has been entered
in the spreadsheet, or you wish to override that accession, use this
option.

=item B<-q> C<QT filename>

QuantitationType file. This option allows you to specify a custom
QuantitationType definition file to override those defined as part of
the Tab2MAGE package. See L<ArrayExpress::Datafile::QT_list> for more
information.

=item B<-Q> C<QT filename>

QuantitationType file. This option will add the new QuantitationType
definitions to those included with the Tab2MAGE package. See
L<ArrayExpress::Datafile::QT_list> for more information.

=item B<-k>

Keep all columns in the data files, regardless of whether they are
recognized or not. Unrecognized QTs will be created as generic
SpecializedQuantitationTypes in the output MAGE-ML.

=item B<-K>

If the autosubmissions system is configured, tab2mage.pl will
automatically reassign protocol accessions to fit a local
convention. Use the -K option to suppress this behaviour.

=item B<-s>

Standalone option. This prevents the script from attempting to connect
to ArrayExpress to retrieve array information.

=item B<-R> C<namespace>

Reporter identifier prefix. By default the script uses the MIAMExpress
convention for generating reporter identifiers. This option allows you
to override this behaviour by supplying an alternate prefix for
identifiers.

=item B<-C> C<namespace>

CompositeSequence identifier prefix. By default the script uses the
MIAMExpress convention for generating composite sequence
identifiers. This option allows you to override this behaviour by
supplying an alternate prefix for identifiers.

=item B<-d> C<directory>

Source directory containing all the data files referenced in the tab2mage
spreadsheet. If this is omitted, the current working directory will be
searched for data files.

=item B<-P>

By default, native (usually binary) file formats are used for
Affymetrix CEL files and all NimbleScan (NimbleGen) files. This
encoding uses far less overhead and retains the files in their
original formats; this is often appealing to end-users. When used for
ArrayExpress submissions, this option allows for such files to be
directly downloadable from the ArrayExpress web interface. However, in
unusual circumstances when you might wish to use plain-text
encoding for these datafile, use this option.

=item B<-L>

Ignore the data file size limit as configured in Config.yml (i.e.,
MAX_DATAFILE_SIZE).

=item B<-f> C<font name>

Name of the font to be used for Graphviz-generated PNGs.

=item B<-c>

Overwrite preexisting files ("clobber" option).

=item B<-v>

Prints the version number of the script.

=item B<-h>

Print a short help text summarizing these options.

=back

=head1 QUANTITATIONTYPE FILE

Please see L<ArrayExpress::Datafile::QT_list> for a description of
the format of this file.

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2004.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. Particular credit
goes to Ele Holloway, who was responsible for curating the lists of
QuantitationTypes included with this script.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

###########
# GLOBALS #
###########
#
# Used to track the creation of unique objects.
#

my $APP_NAME = $CONFIG->get_TAB2MAGE_PROGNAME();
my $VERSION  = $CONFIG->get_TAB2MAGE_VERSION();

###############
# SUBROUTINES #
###############

sub parse_args {

    my %args;

    my ( $want_version, $want_help );

    GetOptions(
        "e|edf=s"      => \$args{spreadsheet_filename},
        "t|target=s"   => \$args{target_directory},
	"n|accession=s" => \$args{expt_accession},
	"d|source=s"   => \$args{source_directory},
        "q|qtfile=s"   => \$args{QT_file},
        "Q|QTFILE=s"   => \$args{included_QTs},
        "k|all-qts"    => \$args{keep_all_qts},
        "s|standalone" => \$args{is_standalone},
	"R|reporter=s" => \$args{reporter_prefix},
	"C|compseq=s"  => \$args{compseq_prefix},
        "P|plain-text" => \$args{text_datafiles},
	"L|large-files" => \$args{ignore_size_limits},
        "f|font=s"     => \$args{font},
        "v|version"    => \$want_version,
        "c|clobber"    => \$args{clobber},
        "h|help"       => \$want_help,
        "K|keep-accns" => \$args{keep_protocol_accns},
    );

    if ($want_version) {
        print STDERR "This is $APP_NAME v$VERSION\n\n";
        exit 255;
    }

    if ($want_help) {
	pod2usage(
	    -exitval => 255,
	    -output  => \*STDERR,
	    -verbose => 1,
	);
    }

    unless ( $args{spreadsheet_filename} && $args{target_directory} ) {
	pod2usage(
	    -message => 'Please see "tab2mage.pl -h" for further help notes.',
	    -exitval => 255,
	    -output  => \*STDERR,
	    -verbose => 0,
	);
    }

    return \%args;

}

########
# MAIN #
########

# Get our command-line options.
my $args = parse_args();

# Initialise some stuff.
my $starttime = new Benchmark;

# Generate the MAGE objects, output the MAGE-ML, and return the hash
# of MAGE container objects and the experiment accession for further
# processing (e.g. visualisation).
my $exporter = ArrayExpress::Curator::Tab2MAGE->new({
    spreadsheet_filename => $args->{spreadsheet_filename},
    target_directory     => $args->{target_directory},
    source_directory     => $args->{source_directory},
    is_standalone        => $args->{is_standalone},
    clobber              => $args->{clobber},
    qt_filename          => ( $args->{QT_file} || $args->{included_QTs} ),
    include_default_qts  => defined( $args->{included_QTs} ),
    keep_all_qts         => $args->{keep_all_qts},
    use_plain_text       => $args->{text_datafiles},
    keep_protocol_accns  => $args->{keep_protocol_accns},
    reporter_prefix      => $args->{reporter_prefix},
    compseq_prefix       => $args->{compseq_prefix},
    ignore_size_limits   => $args->{ignore_size_limits},
    external_accession   => $args->{expt_accession},
});

my ($mage, $accession, $error) = $exporter->write_mageml();

# Create graphs
my $graphfile_base
    = File::Spec->catfile( $args->{target_directory}, $accession );

$args->{clobber} = dot_and_png(
    $exporter->get_bags(),
    $graphfile_base,
    $args->{font},
    $exporter->get_clobber(),
);

print STDOUT ("Finished.\n");

my $endtime = new Benchmark;
my $timediff = timediff( $endtime, $starttime );
print STDOUT ( "\nTotal run time = ", timestr($timediff), "\n\n", );

# If we get here, we must assume that the parser ran more-or-less
# okay, so we return zero to the shell.
exit 0;

