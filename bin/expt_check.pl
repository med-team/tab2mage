#!/usr/bin/env perl
#
# expt_check.pl - Tim Rayner 2006 ArrayExpress team, EBI
# See "expt_check.pl -h" for usage information.
#
# $Id: expt_check.pl 1709 2007-08-22 12:52:09Z tfrayner $
#

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../index.html">
                <img src="T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Script detail: expt_check.pl</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

expt_check.pl - a script to check experiment data files submitted to MIAMExpress

=head1 SYNOPSIS

=over 2

=item B<Tab2MAGE mode:>

   expt_check.pl -e <Tab2MAGE spreadsheet>

=item B<MAGE-TAB mode:>

   expt_check.pl -i <IDF file>

=item B<MIAMExpress mode:>

   expt_check.pl -l <login name> -t <experiment title>

=item B<Standalone mode:>

   expt_check.pl -s <list of data files>

   (use -A or -a to check files against an array design in standalone mode).

=back

=head1 DESCRIPTION

This script can be used to check experimental data for submission to
ArrayExpress in a few different ways. For Tab2MAGE submissions it
parses the Tab2MAGE spreadsheet format and reports on problems with
data files and MIAME metadata. Used in conjunction with a local
MIAMExpress installation the script takes the experiment title and the
submitter login name, and checks the submitted data files for
errors. Several log files are written to the relevant MIAMExpress
submission directory unless the B<-p> option is used to redirect them
to the current directory. Normally the script will be able to figure
out which array design to use from the Tab2MAGE spreadsheet or by
querying the MIAMExpress database. An optional ADF filename argument
may also be provided using the B<-a> option, and ArrayExpress
accession numbers may be specified using the B<-A> option. Specifying
your own ADFs/accession numbers will make the script ignore any array
designs pointed to by the spreadsheet or database entry for the
experiment.

=head1 QUANTITATION TYPES

Known QuantitationTypes are listed in a separate file or files,
created with a simple tab-delimited format. The layout of
these files is described in L<ArrayExpress::Datafile::QT_list>.

=head1 OPTIONS

=over 4

=item B<-e> C<spreadsheet filename>

The Tab2MAGE spreadsheet to be checked.

=item B<-i> C<IDF filename>

The MAGE-TAB IDF file to be checked.

=item B<-l> C<login name> 

The MIAMExpress login name of the experiment submitter

=item B<-t> C<experiment title>

The MIAMExpress title of the experiment, surrounded by quotes if the title contains spaces.

=item B<-a> C<ADF filename>

The B<-a> switch designates the ADF filename to be used for all the
hybridizations in the experiment. This option overrides any database
links between hybridizations and array designs, and is provided
initially as a convenience.

=item B<-A> C<Array accession number (ArrayExpress)>

Use the B<-A> switch to indicate the accession number of an
ArrayExpress array design to be used for checking the data
files. Ordinarily this should not be needed, as the script should be
able to link the MIAMExpress submission with ArrayExpress array
designs automatically.

=item B<-c>

Forces overwriting of existing files ("clobber"). If this switch is
omitted the user will be asked whether to overwrite already existing
files.

=item B<-p> 

Write to files in present working directory ("pwd"). The default is to
write to files in the submission directory.

=item B<-s> 

Standalone option. The script will check the files listed on the
command line rather than connecting to MIAMExpress and
ArrayExpress. When used with the B<-e> or B<-i> options, the Tab2MAGE
or MAGE-TAB document is checked but no connection is made to ArrayExpress to
retrieve array information. To check features and reporter identifiers
with this option, an ADF must be specified with the B<-a> option or an
ArrayExpress accession number can be used with the B<-A> option. In
the latter case a connection is made to ArrayExpress without
connecting to MIAMExpress.

=item B<-d> C<directory>

Source directory. This indicates the directory to search for data
files. This option is only used for Tab2MAGE submissions checks, as
MIAMExpress defines its own directory structure which is automatically
searched by this script. If this option is omitted, only the current
working directory is searched for Tab2MAGE submission data files.

=item B<-x>

Skip data file checking. This option can be used to quickly check
experiment annotation without having to wait for the script to
validate all the data files.

=item B<-q> C<QT filename>

QuantitationType file. This option allows you to specify a custom
QuantitationType definition file to override those defined in the
ArrayExpress::Curator::Config module. See
L<ArrayExpress::Datafile::QT_list> for more information.

=item B<-Q> C<QT filename> 

QuantitationType file. This option will add the new QuantitationType
definitions to those included with the Tab2MAGE package. See
L<ArrayExpress::Datafile::QT_list> for more information.

=item B<-R> C<namespace>

Prefix of the Reporter identifier to use when checking data files
against array designs. This prefix is added to each "Reporter
Identifier" in the data files prior to comparison with the actual
identifiers in the ADF. The default is to assume MIAMExpress-like
identifiers.

=item B<-C> C<namespace>

Prefix of the CompositeSequence identifier to use when checking data
files against array designs. This prefix is added to each
"CompositeSequence Identifier" in the data files prior to comparison
with the actual identifiers in the ADF. The default is to assume
MIAMExpress-like identifiers.

=item B<-L>

Ignore the data file size limit as configured in Config.yml (i.e.,
MAX_DATAFILE_SIZE).

=item B<-m>

The checker will support MAGE-TAB documents in which a single IDF and
SDRF have been combined (in that order), with the start of each
section marked by [IDF] and [SDRF] respectively. Note that such
documents are not compliant with the MAGE-TAB format specification;
this format is used by ArrayExpress to simplify data submissions.

=item B<-v>

Prints the version number of the script.

=item B<-h>

Prints a short help text.

=back

=head1 TESTS

There are numerous tests performed by this script. Listed below are
the tests which are performed on each data file. There is also a
series of checks which are made on any MIAME metadata supplied to the
script. This metadata may be in the form of a Tab2MAGE spreadsheet, or
an experiment submission in a local MIAMExpress database.  Please see
L<ArrayExpress::Curator::ExperimentChecker/"TESTS"> for a list of
general tests performed on all submissions.  See also
L<ArrayExpress::Curator::Validate/"TESTS"> for the Tab2MAGE
spreadsheet tests, L<ArrayExpress::MAGETAB::Checker/"TESTS"> for
MAGE-TAB document checking, and
L<ArrayExpress::Curator::MIAMExpress/"TESTS"> for more information on
the MIAMExpress tests.

=head1 LOCAL MIAMEXPRESS INSTALLATIONS

Users wishing to use this script with local installations of
MIAMExpress should check the ArrayExpress::Curator::Config module
and change whatever parameters are necessary.

=head1 SEE ALSO

=over 1

=item L<ArrayExpress::Curator::ExperimentChecker>

=item L<ArrayExpress::Curator::Validate>

=item L<ArrayExpress::MAGETAB::Checker>

=item L<ArrayExpress::Curator::MIAMExpress>

=item L<ArrayExpress::Datafile::QT_list>

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2004.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments.

=head1 BUGS

Affymetrix CHP files are not currently supported. Moreover, the script
will not check the features referred to in CEL files against an array
design until I can figure out how to retrieve a list of valid features
in a timely fashion. CompositeSequence IDs in final data matrix
files from Affymetrix submissions are fully supported and checked
against the array design. Basic data quality calculations (percent
null, Benfords law) are made on Affymetrix CEL and FGEM data.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

use strict;
use warnings;

use Benchmark;
use Pod::Usage;
use Getopt::Long qw(:config no_ignore_case);

use ArrayExpress::Curator::Config qw($CONFIG);

###############
# Subroutines #
###############

sub parse_args {

    my ( %args, $want_help, $want_version );
    GetOptions(
        "a|adf=s"       => \$args{adf_filename},
        "e|edf=s"       => \$args{edf_filename},
        "i|idf=s"       => \$args{idf_filename},
        "A|accession=s" => \$args{array_accession},
        "l|login=s"     => \$args{login},
        "t|title=s"     => \$args{title},
        "p|pwd:s"       => \$args{pwd_log},
        "c|clobber"     => \$args{clobber},
	"x|skip"        => \$args{skip_data_checks},	
        "s|standalone"  => \$args{standalone},
	"d|source=s"    => \$args{source_directory},
        "q|qtfile=s"    => \$args{qtfile},
        "Q|QTFILE=s"    => \$args{QTFILE},
        "R|reporter=s"  => \$args{reporter_prefix},
        "C|compseq=s"   => \$args{compseq_prefix},
	"m|magetab=s"   => \$args{magetab_doc},
	"L|large-files" => \$args{ignore_size_limits},
        "h|help"        => \$want_help,
        "v|version"     => \$want_version,
    );

    if ($want_version) {
        print STDERR (
            "This is ", $CONFIG->get_EXPTCHECK_PROGNAME(),
            " v", $CONFIG->get_EXPTCHECK_VERSION(), "\n\n"
        );
        exit 255;
    }

    if ($want_help) {
	pod2usage(
	    -exitval => 255,
	    -output  => \*STDERR,
	    -verbose => 1,
	);
    }

    unless ( ( $args{login} && $args{title} )
        || $args{standalone}
        || $args{idf_filename}
        || $args{edf_filename}
        || $args{magetab_doc} ) {
	pod2usage(
	    -message => 'Please see "expt_check.pl -h" for further help notes.',
	    -exitval => 255,
	    -output  => \*STDERR,
	    -verbose => 0,
	);
    }

    # FIXME this could be better.
    if ( ( $args{idf_filename} || $args{edf_filename} || $args{magetab_doc} )
	     && ( $args{login} || $args{title} ) ) {
	die("Error: ambiguous options used (mixed MIAMExpress and Tab2MAGE/MAGE-TAB).\n\n");
    }

    # Clean up this path a bit, make sure it's absolute
    if ( $args{adf_filename} ) {
        $args{adf_filename} = File::Spec->rel2abs( $args{adf_filename} );
        die("Error: ADF file $args{adf_filename} is not readable")
            unless ( -f $args{adf_filename} );
    }

    return ( \%args );
}

# End subroutines

########
# MAIN #
########

# Get our arguments
my $args = parse_args();

# Initialise some stuff.
my $starttime = new Benchmark;

# Arguments common to all ExperimentChecker objects.
my %common_args = (
    is_standalone        => $args->{standalone},
    adf_filename         => $args->{adf_filename},
    array_accession      => $args->{array_accession},
    log_to_current_dir   => $args->{pwd_log},
    clobber              => $args->{clobber},
    qt_filename          => ( $args->{qtfile} || $args->{QTFILE} ),
    include_default_qts  => defined( $args->{QTFILE} ),
    skip_data_checks     => $args->{skip_data_checks},
    source_directory     => $args->{source_directory},
    reporter_prefix      => $args->{reporter_prefix},
    compseq_prefix       => $args->{compseq_prefix},
    ignore_size_limits   => $args->{ignore_size_limits},
);

my $checker;
unless (  $args->{standalone}
       || $args->{edf_filename}
       || $args->{idf_filename}
       || $args->{magetab_doc} ) {

    # MIAMExpress checking.
    require ArrayExpress::Curator::MIAMExpress;
    $checker = ArrayExpress::Curator::MIAMExpress->new({
	mx_login             => $args->{login},
	mx_title             => $args->{title},
	%common_args,
    });
}
else {

    if (  $args->{standalone}
     && ! $args->{edf_filename}
     && ! $args->{idf_filename}
     && ! $args->{magetab_doc} ) {

	# Standalone checking.
	require ArrayExpress::Curator::Standalone;
	$checker = ArrayExpress::Curator::Standalone->new({
	    data_files       => \@ARGV,
	    %common_args,
	});
    }
    elsif ( $args->{edf_filename} ) {

	# Tab2MAGE checking.
	require ArrayExpress::Curator::Validate;
	$checker = ArrayExpress::Curator::Validate->new({
	    spreadsheet_filename => $args->{edf_filename},
	    %common_args,
	});
    }
    elsif ( $args->{idf_filename} ) {

	# MAGE-TAB checking.
	require ArrayExpress::MAGETAB::Checker;
	$checker = ArrayExpress::MAGETAB::Checker->new({
	    idf => $args->{idf_filename},
	    %common_args,
	});
    }
    elsif ( $args->{magetab_doc} ) {

	# MAGE-TAB (combined IDF+SDRF) checking.
	require ArrayExpress::MAGETAB::Checker;
	$checker = ArrayExpress::MAGETAB::Checker->new({
	    magetab_doc => $args->{magetab_doc},
	    %common_args,
	});
    }
    else {
	die("Error: Ambiguous command-line options used.\n");
    }
}

$checker->check();

my $endtime = new Benchmark;
my $timediff = timediff( $endtime, $starttime );
print STDOUT ( "\nTotal run time = ", timestr($timediff), "\n\n", );

exit $checker->get_error();
