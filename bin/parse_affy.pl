#!/usr/bin/env perl
#
# Script to parse Affymetrix data files.
# Shows example usage of ArrayExpress::Datafile::Affymetrix modules.
#
# Tim Rayner 2004, ArrayExpress team, European Bioinformatics Institute
#
# $Id: parse_affy.pl 1852 2007-12-13 10:14:27Z tfrayner $
#

use strict;
use warnings;

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

=pod

=head1 NAME

parse_affy.pl - an example Affymetrix data file parsing script. 

=head1 SYNOPSIS

 parse_affy.pl -i <Affymetrix data file (CEL or CHP)>
               -o <tab-delimited data matrix output file>

=head1 DESCRIPTION

This script is a 'toy' script to illustrate the use of the
ArrayExpress::Datafile::Affymetrix parsing module supplied with the
Tab2MAGE distribution. It may nonetheless be useful in circumstances
where other Affymetrix parsers fail. The script reads data files (CEL
and CHP) in both old (version 3, GDAC) and new (version 4, XDA/GCOS)
formats. The script then outputs any combination of the following: (a)
a tab-delimited data matrix, (b) a set of MAGE identifiers making up
the DesignElementDimension of the matrix, and/or (c) a set of
identifiers comprising the QuantitationTypeDimension of the data
matrix. Please see L<ArrayExpress::Datafile::Affymetrix> for more
information on the Affymetrix parsing API.

=head1 OPTIONS

=over 2

=item B<-i> C<input filename>

Affymetrix CEL or CHP data file to parse.

=item B<-o> C<output filename>

File to be used for output (tab-delimited data).

=item B<-d> C<DED filename>

File to which the DesignElementDimension is to be written (optional).

=item B<-q> C<QTD filename>

File to which the QuantitationTypeDimension is to be written
(optional).

=item B<-c> C<input CDF filename>

Affymetrix library CDF file to use for CHP file parsing.

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2005.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

use Getopt::Long;
use IO::File;
use Pod::Usage;
use ArrayExpress::Datafile::Affymetrix;
use ArrayExpress::Datafile::Binary qw(get_integer);

sub parse_args {

    my (%args, $want_help);
    GetOptions(
        "i|infile=s"  => \$args{infile},
        "o|outfile=s" => \$args{outfile},
        "d|dedfile=s" => \$args{dedfile},
        "q|qtdfile=s" => \$args{qtdfile},
        "c|cdf=s"     => \$args{cdf},
	"h|help"      => \$want_help,
    );

    if ($want_help) {
	pod2usage(
	    -exitval => 255,
	    -output  => \*STDERR,
	    -verbose => 1,
	);
    }

    unless ( $args{infile}
        && ( $args{outfile} || $args{dedfile} || $args{qtdfile} ) ) {
	pod2usage(
	    -message => 'Please see "parse_affy.pl -h" for further help notes.',
	    -exitval => 255,
	    -output  => \*STDERR,
	    -verbose => 0,
	);
    }
    return \%args;
}

########
# MAIN #
########

# Get our arguments
my $args = parse_args();
my $affy_factory = ArrayExpress::Datafile::Affymetrix->new();

# Deal with CDF if supplied (XDA only for now)
my $cdf;
if ( $args->{cdf} ) {
    $cdf = $affy_factory->make_parser($args->{cdf});
    $cdf->parse();
}

# Open our file for input
#my $infile = IO::File->new( $args->{infile}, '<' )
#    or die("Error: cannot open input file $args->{infile}: $!\n");
#binmode($infile);

SWITCH: {
    ( $args->{infile} =~ m/\.CEL$/i ) && do {    # CEL file

        my $cel = $affy_factory->make_parser( $args->{infile} );

        $cel->parse();
        print( "Algorithm: ", $cel->get_algorithm, "\n" );
        print( "NumCells:  ", $cel->get_num_cells, "\n" );

        while ( my ( $key, $value ) = each %{ $cel->get_parameters() } ) {
            print("$key: $value\n");
        }

        # Output
        $args->{outfile} && do {
            my $outfile = IO::File->new( $args->{outfile}, '>' )
                or die(
                "Error: cannot open data output file $args->{outfile}: $!\n");
            $cel->export($outfile);
        };

        $args->{dedfile} && do {
            my $dedfile = IO::File->new( $args->{dedfile}, '>' )
                or die(
                "Error: cannot open DED output file $args->{dedfile}: $!\n");
            foreach my $identifier ( @{ $cel->get_ded() } ) {
                print $dedfile ("$identifier\n");
            }
        };

        $args->{qtdfile} && do {
            my $qtdfile = IO::File->new( $args->{qtdfile}, '>' )
                or die(
                "Error: cannot open QTD output file $args->{qtdfile}: $!\n");
            foreach my $identifier ( @{ $cel->get_qtd() } ) {
                print $qtdfile ("$identifier\n");
            }
        };

        last SWITCH;
    };

    ( $args->{infile} =~ m/\.CHP$/i ) && do {    # CHP file

        my $chp = $affy_factory->make_parser( $args->{infile} );

        unless ($cdf) {
            die(      "Unable to export CHP data: no CDF supplied. "
                    . "Please use the -c option to specify a CDF filename.\n"
            );
        }

        $chp->parse();

        # Output
        $args->{outfile} && do {
            my $outfile = IO::File->new( $args->{outfile}, '>' )
                or die(
                "Error: cannot open data output file $args->{outfile}: $!\n");
            $chp->export( $outfile, $cdf );
        };

        $args->{dedfile} && do {
            my $dedfile = IO::File->new( $args->{dedfile}, '>' )
                or die(
                "Error: cannot open DED output file $args->{dedfile}: $!\n");
            foreach my $identifier ( @{ $chp->get_ded($cdf) } ) {
                print $dedfile ("$identifier\n");
            }
        };

        $args->{qtdfile} && do {
            my $qtdfile = IO::File->new( $args->{qtdfile}, '>' )
                or die(
                "Error: cannot open QTD output file $args->{qtdfile}: $!\n");
            foreach my $identifier ( @{ $chp->get_qtd() } ) {
                print $qtdfile ("$identifier\n");
            }
        };

        last SWITCH;
    };

    ( $args->{infile} =~ m/\.EXP$/i ) && do {    # EXP file

        my $exp = $affy_factory->make_parser( $args->{infile} );

        $exp->parse();

        # Open our file for output
        my $outfile = IO::File->new( $args->{outfile}, '>' )
            or die("Error: cannot open file $args->{outfile}: $!\n");

        $exp->export($outfile);

        last SWITCH;
    };

    die("Error: Unknown file extension.\n");
}

