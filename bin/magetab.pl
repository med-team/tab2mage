#!/usr/bin/env perl
#
# magetab.pl
#
# A script to generate MAGE-ML from a MAGE-TAB document and set of
# datafiles.
#
# Tim Rayner 2007 ArrayExpress Team, EBI
#
# $Id: magetab.pl 2069 2008-06-04 14:33:52Z tfrayner $
#

use strict;
use warnings;

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

use English qw( -no_match_vars );
use Getopt::Long qw(:config no_ignore_case);
use File::Spec;
use Readonly;
use Pod::Usage;
use Benchmark;

use ArrayExpress::MAGETAB;
use ArrayExpress::Curator::Visualize qw( dot_and_png );

Readonly my $APP_NAME => 'MAGETabulator';

sub parse_args {

    my %args;

    my ( $want_version, $want_help );

    GetOptions(
        "i|idf=s"       => \$args{idf},
	"n|accession=s" => \$args{expt_accession},
        "t|target=s"    => \$args{target_directory},
	"d|source=s"    => \$args{source_directory},
        "q|qtfile=s"    => \$args{QT_file},
        "Q|QTFILE=s"    => \$args{included_QTs},
        "k|all-qts"     => \$args{keep_all_qts},
        "K|keep-accns"  => \$args{keep_protocol_accns},
	"R|reporter=s"  => \$args{reporter_prefix},
	"C|compseq=s"   => \$args{compseq_prefix},
        "s|standalone"  => \$args{is_standalone},
        "P|plain-text"  => \$args{text_datafiles},
	"L|large-files" => \$args{ignore_size_limits},
        "x|skip-data"   => \$args{skip_datafiles},
	"m|magetab=s"   => \$args{magetab_doc},
        "f|font=s"      => \$args{font},
        "v|version"     => \$want_version,
        "h|help"        => \$want_help,
	"c|clobber"     => \$args{clobber},
    );

    if ($want_version) {
        print STDERR "This is $APP_NAME v$ArrayExpress::MAGETAB::VERSION\n\n";
        exit 255;
    }

    if ($want_help) {
	pod2usage(
	    -exitval => 255,
	    -output  => \*STDERR,
	    -verbose => 1,
	);
    }

    unless ( ( $args{idf} || $args{magetab_doc} )
		  && $args{target_directory} && $args{expt_accession} ) {
	pod2usage(
	    -message => 'Please see "magetab.pl -h" for further help notes.',
	    -exitval => 255,
	    -output  => \*STDERR,
	    -verbose => 0,
	);
    }

    return \%args;

}

########
# MAIN #
########

# Get our command-line options.
my $args = parse_args();

# Note the starting time.
my $starttime = new Benchmark;

# Initialise our parser object.
my %parser_opts = (
    authority                 => 'ebi.ac.uk',
    namespace                 => 'MAGETabulator',
    expt_accession            => $args->{expt_accession},
    output_file               => "$args->{expt_accession}.xml",
    target_directory          => $args->{target_directory},
    source_directory          => $args->{source_directory},
    is_standalone             => $args->{is_standalone},
    qt_filename               => ( $args->{QT_file} || $args->{included_QTs} ),
    include_default_qts       => defined( $args->{included_QTs} ),
    keep_all_qts              => $args->{keep_all_qts},
    keep_protocol_accns       => $args->{keep_protocol_accns},
    reporter_prefix           => $args->{reporter_prefix},
    compseq_prefix            => $args->{compseq_prefix},
    use_plain_text            => $args->{text_datafiles},
    ignore_size_limits        => $args->{ignore_size_limits},
    skip_datafiles            => $args->{skip_datafiles},
    clobber                   => $args->{clobber},
    protocol_accession_prefix => 'P-MTAB-',
);

my $magetab;
if ( $args->{idf} ) {
    $magetab = ArrayExpress::MAGETAB->new({
	idf => $args->{idf},
	%parser_opts,
    });
}
elsif ( $args->{magetab_doc} ) {
    $magetab = ArrayExpress::MAGETAB->new({
	magetab_doc => $args->{magetab_doc},
	%parser_opts,
    });
    $magetab->parse_magetab_doc();
}
else {
    die("Error: Either IDF or MAGETAB doc required by script.");
}

# Parse the files and write out the MAGE-ML
$magetab->write_mageml();

# Create graphs
my $graphfile_base = File::Spec->catfile(
    $args->{target_directory},
    $args->{expt_accession},
);

my $classes = {
    'source'            => 'Source',
    'sample'            => 'Sample',
    'extract'           => 'Extract',
    'labeledextract'    => 'Labeled Extract',
    'pba'               => 'Hybridization',
    'mba'               => 'Feature Extraction',
    'datamatrix_mba'    => 'Array Data',
    'dba'               => 'Normalization',
    'datamatrix_dba'    => 'Array Data',
};

$args->{clobber} = dot_and_png(
    $magetab->get_bags(),
    $graphfile_base,
    $args->{font},
    $magetab->get_clobber(),
    $classes,
);

# We're done. Write out our benchmark info and exit.
print STDOUT ("Finished.\n");

my $endtime = new Benchmark;
my $timediff = timediff( $endtime, $starttime );
print STDOUT ( "\nTotal run time = ", timestr($timediff), "\n\n", );

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../index.html">
                <img src="MAGETAB_logo.png"
                     border="0" height="50" alt="MAGE-TAB logo"></td>
              </a>
	    <td class="pagetitle">Script detail: magetab.pl</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

magetab.pl - a script to generate valid MAGE-ML from an input MAGE-TAB
document.

=head1 SYNOPSIS

 magetab.pl -i <IDF filename> -n <experiment accession> -t <target directory>

=head1 DESCRIPTION

This script will process a MAGE-TAB document and a set of data files
to generate valid MAGE-ML. The supported data file formats are listed
in the accompanying documentation. The MAGE-TAB format specification
may be downloaded from this link:

 http://www.ebi.ac.uk/systems-srv/mp/file-exchange/MAGE-TABv1.0.tar.gz

There is one auxilliary file which can be supplied alongside the data
files. This file simply defines the QuantitationTypes which are to be
extracted from the data files. This QuantitationType file is optional,
however, as the script is supplied with a set of defaults determined
by a survey of incoming QuantitationTypes by ArrayExpress
curators. The script generates a log file in the target directory
which details which columns have been ignored. To keep all
unrecognized QuantitationTypes, invoke the script with the B<-k>
option.

=head1 OPTIONS

=over 2

=item B<-i> C<IDF>

The MAGE-TAB IDF file to be parsed.

=item B<-n> C<accession>

The experiment accession to be used as the top-level Experiment
identifier in the generated MAGE-ML.

=item B<-t> C<directory>

The target directory to be created. This directory will contain the
MAGE-ML file and external data files ready for validation.

=item B<-q> C<QT filename>

QuantitationType file. This option allows you to specify a custom
QuantitationType definition file to override those defined as part of
the Tab2MAGE package. See L<ArrayExpress::Datafile::QT_list> for more
information.

=item B<-Q> C<QT filename>

QuantitationType file. This option will add the new QuantitationType
definitions to those included with the Tab2MAGE package. See
L<ArrayExpress::Datafile::QT_list> for more information.

=item B<-k>

Keep all columns in the data files, regardless of whether they are
recognized or not. Unrecognized QTs will be created as generic
SpecializedQuantitationTypes in the output MAGE-ML.

=item B<-K>

If the autosubmissions system is configured, magetab.pl will
automatically reassign protocol accessions to fit a local
convention. Use the -K option to suppress this behaviour.

=item B<-s>

Standalone option. This prevents the script from attempting to connect
to ArrayExpress to retrieve array information.

=item B<-x>

Skip data file processing during MAGE-ML generation. The script will
attempt to generate MAGE-ML for the metadata contained in the IDF and
SDRF only.

=item B<-R> C<namespace>

Reporter identifier prefix. By default the script uses the MIAMExpress
convention for generating reporter identifiers. This option allows you
to override this behaviour by supplying an alternate prefix for
identifiers.

=item B<-C> C<namespace>

CompositeSequence identifier prefix. By default the script uses the
MIAMExpress convention for generating composite sequence
identifiers. This option allows you to override this behaviour by
supplying an alternate prefix for identifiers.

=item B<-d> C<directory>

Source directory containing all the data files referenced in the
SDRF. If this is omitted, the current working directory will be
searched for data files.

=item B<-P>

By default, native (usually binary) file formats are used for
Affymetrix CEL files and all NimbleScan (NimbleGen) files. This
encoding uses far less overhead and retains the files in their
original formats; this is often appealing to end-users. When used for
ArrayExpress submissions, this option allows for such files to be
directly downloadable from the ArrayExpress web interface. However, in
unusual circumstances when you might wish to use plain-text
encoding for these data files, use this option.

=item B<-L>

Ignore the data file size limit as configured in Config.yml (i.e.,
MAX_DATAFILE_SIZE).

=item B<-m>

The parser will support MAGE-TAB documents in which a single IDF and
SDRF have been combined (in that order), with the start of each
section marked by [IDF] and [SDRF] respectively. Note that such
documents are not compliant with the MAGE-TAB format specification;
this format is used by ArrayExpress to simplify data submissions.

=item B<-f> C<font name>

Name of the font to be used for Graphviz-generated PNGs.

=item B<-c>

Overwrite preexisting files ("clobber" option).

=item B<-v>

Prints the version number of the script.

=item B<-h>

Print a short help text summarizing these options.

=back

=head1 QUANTITATIONTYPE FILE

Please see L<ArrayExpress::Datafile::QT_list> for a description of
the format of this file.

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2007.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

