#!/usr/bin/env perl
#
# $Id: T2MInstaller.pm 2069 2008-06-04 14:33:52Z tfrayner $
#
# Module used in automated advanced deployment of tab2mage and related
# code into the ArrayExpress production environment. This is *not*
# needed for basic installation of the Tab2MAGE package.

package T2MInstaller;

use strict;
use warnings;

use File::Spec;
use File::Basename qw(basename);
use File::Copy qw(copy);

use Config::YAML;
use Config;

use Carp;

use base qw(Exporter);
our @EXPORT_OK = qw(install);

sub get_config {

    my ( $args ) = @_;

    # Default to using a config file named "InstallConfig.yml" in the
    # same directory as this module. If a config_file argument is
    # passed, use that instead.
    my $config_file;
    unless ( $config_file = $args->{config_file} ) {
	my @module_dir_array = File::Spec->splitpath(__FILE__);
	$config_file         = File::Spec->catpath(
	    @module_dir_array[ 0, 1 ], 'InstallConfig.yml',
	);
    }

    unless ( -f $config_file ) {
	if ( $args->{config_file} ) {
	    croak(qq{Error: config file "$config_file" not found.\n});
	}
	else {
	    croak(qq{Config file InstallConfig.yml not found. Please ensure that you}
		. qq{ have edited the example config and renamed it to "$config_file".\n});
	}
    }

    my $config = Config::YAML->new( config => $config_file );

    return $config;
}

sub install_aux {

    my ( $auxilliary_scripts, $bindir ) = @_;

    foreach my $aux ( @$auxilliary_scripts ) {
        my $targetfile = basename( $aux );     # just the filename
        my $targetpath = File::Spec->catfile( $bindir, $targetfile );
        print "Copying $aux to $targetpath...\n";
        copy( $aux, $targetpath )
            or croak("Error: Unable to copy file $aux: $!\n");
    }

    return;
}

sub install_modules {

    my ( $source_directories, $args ) = @_;

    my $perlmakefile_command = 'perl Makefile.PL';
    $perlmakefile_command .=
        $args->{inst_lib}
        ? " LIB=$args->{inst_lib}"
        : q{};

    $perlmakefile_command .=
        $args->{bin}
        ? " INSTALLSCRIPT=$args->{bin}"
        : q{};

    $perlmakefile_command .=
        $args->{man1}
        ? " INSTALLMAN1DIR=$args->{man1} INSTALLSITEMAN1DIR=$args->{man1}"
        : q{};

    $perlmakefile_command .=
        $args->{man3}
        ? " INSTALLMAN3DIR=$args->{man3} INSTALLSITEMAN3DIR=$args->{man3}"
        : q{};

    foreach my $sourcedir (@$source_directories) {

        chdir($sourcedir)
            or croak("Unable to change directory to $sourcedir: $!\n");

        if ( -f 'Makefile' ) {
            system('make distclean') == 0
                or croak("make distclean failed: $?");
        }

        system($perlmakefile_command) == 0
            or croak("perl Makefile.PL failed: $?");

        system('make') == 0
            or croak("make failed: $?");

        system('make test') == 0
            or croak("make test failed: $?");

        system('make install') == 0
            or croak("make install failed: $?");

    }
}

sub get_bindir {

    my ( $args ) = @_;

    my $bindir = $args->{bin} || $Config{installsitebin};

    return $bindir;
}

sub get_inst_libdir {

    my ( $args ) = @_;

    my $inst_libdir = $args->{inst_lib} || $Config{installsitelib};

    return $inst_libdir;
}

sub postprocess_scripts {

    my ( $config, $args ) = @_;

    my $auxilliary_scripts = $config->get_auxilliary_scripts() || [];
    my $scripts_to_install = $config->get_scripts_to_install() || [];

    my $perl_location       = $args->{perl_location}
	                   || $config->get_perl_location();
    my $use_lib_placeholder = $config->get_use_lib_placeholder();
    my $shebang_placeholder = $config->get_shebang_placeholder();

    my $perl_libdir = $args->{perl_lib}
                    ? join(q{','}, split /:/, $args->{perl_lib})
		    : $Config{installsitelib};
    my $inst_libdir = get_inst_libdir( $args );
    my $bindir      = get_bindir( $args );

    my $path            = $args->{path}            || $config->get_path()            || q{};
    my $oracle_home     = $args->{oracle_home}     || $config->get_oracle_home()     || q{};
    my $ora_nls33       = $args->{ora_nls33}       || $config->get_ora_nls33()       || q{};
    my $ld_library_path = $args->{ld_library_path} || $config->get_ld_library_path() || q{};

    my $ld_lib_components = join(", ", map { qq{'$_'} } split /:/, $ld_library_path);

    my $use_lib_declaration = (<<"END_USE_LIB");
use lib '$perl_libdir';
use lib '$inst_libdir';  # This takes precedence

# We need to control the environment. We do so with this unholy kludge.
BEGIN {

    # With thanks to www.perlmonks.org/?node_id=126587 and following, monks tye and mikeraz
    # (slightly modified from the originals).
    my \@needed = ($ld_lib_components);

    # Get our LD_LIBRARY_PATH before we mess around with the environment.
    my \$ld_path = \$ENV{LD_LIBRARY_PATH} || q{};

    # Set the easy things here.
    my \$tmp_home = \$ENV{HOME};
    \%ENV = ();
    \$ENV{PATH}            = "$path";
    \$ENV{HOME}            = \$tmp_home;
    \$ENV{ORACLE_HOME}     = "$oracle_home";
    \$ENV{ORA_NLS33}       = "$ora_nls33";

    # Check for the existence of each element of \@needed in the path;
    # if not found, make a note.
    my \$changed_path = 0;
    foreach my \$dir  ( \@needed ) {
	if (  \$ld_path !~ m/(^|:)\\Q\$dir\\E(:|\$)/  ) {
	    \$changed_path = 1;
	}
    }

    # If the path needs modification, do so and re-execute the script.
    if ( \$changed_path ) {
	\$ENV{LD_LIBRARY_PATH} = join ":", \@needed;
	exec 'env', \$^X, \$0, \@ARGV;
    }

    # Otherwise, we're home free! Start our engines...
}


END_USE_LIB

    SCRIPT:
    foreach my $script ( @$scripts_to_install, @$auxilliary_scripts, ) {

	my $target = basename( $script );

        my $orig = File::Spec->catfile( $bindir, $target );
        my $temp = File::Spec->catfile( $bindir, "$target.tmp" );

        printf STDOUT ( "Post-processing script: %-25s...", $target );

	open( my $orig_fh, '<', $orig )
	    or do {
		carp("Error opening $orig (skipping): $!\n");
		next SCRIPT;
	    };
        open( my $temp_fh, '>', $temp )
            or croak("Error opening temporary file $temp: $!\n");

        while ( my $line = <$orig_fh> ) {

            if (   $use_lib_placeholder
                && $line =~ s!$use_lib_placeholder!$use_lib_declaration! ) {
                print STDOUT qq{ "use lib" declaration fixed.};
            }

            if (   $perl_location
                && $shebang_placeholder
                && $line =~ s[$shebang_placeholder][#!$perl_location --] ) {
                print STDOUT qq{ shebang line redirected.};
            }

            print $temp_fh $line;
        }

        unlink($orig) or croak("Unable to unlink original file $orig: $!\n");
        rename( $temp, $orig )
            or croak("Unable to rename $temp to $orig: $!\n");

        system("chmod 0755 $orig") == 0
            or croak "chmod 0755 $orig failed: $?";

        close $temp_fh or croak("$!\n");
        close $orig_fh or croak("$!\n");

	print STDOUT "\n";
    }

    return;
}

sub postprocess_siteconfig {

    my ( $config, $args ) = @_;

    my $inst_libdir = get_inst_libdir( $args );

    my $config_pm   = "$inst_libdir/ArrayExpress/Curator/Config.pm";
    my $config_temp = "$config_pm.tmp";
    my $site_config = $args->{site_config_location}
	|| $config->get_site_config_location;

    if ( -f $config_pm && $site_config ) {
	print STDOUT "Fixing Config.pm module...";

	open( my $origpm_fh, '<', $config_pm )
	    or croak("Error opening $config_pm: $!\n");
	open( my $temppm_fh, '>', $config_temp )
	    or croak("Error opening $config_temp: $!\n");

	while ( my $line = <$origpm_fh> ) {
	    $line =~ s/(\$siteconf\s*=\s*)q\{\}/$1q\{$site_config\}/g;
	    print $temppm_fh ($line);
	}

	unlink($config_pm)
	    or croak("Unable to unlink original file $config_pm: $!\n");
	rename( $config_temp, $config_pm )
	    or croak("Unable to rename $config_temp to $config_pm: $!\n");

	close $temppm_fh or croak("$!\n");
	close $origpm_fh or croak("$!\n");

	print STDOUT " Done\n\n";
    }
    return;
}

sub install {

    my ( $args ) = @_;

    my $config = get_config( $args );
    my $bindir = get_bindir( $args );

    # Scripts not in the Makefile.PL. This is a more likely point of
    # failure, so we try it first.
    my $auxilliary_scripts = $config->get_auxilliary_scripts() || [];
    install_aux( $auxilliary_scripts, $bindir );

    # Run the main install.
    my $source_directories = $args->{source_directories}
        || $config->get_source_directories()
	    or croak("Error: No source directories\n");
    install_modules( $source_directories, $args );

    # Post-install changes.
    postprocess_scripts( $config, $args );

    # Repoint the main Config.pm module to our site config file.
    postprocess_siteconfig( $config, $args );

    return;
}

1;
