#!/usr/bin/env perl
#
# $Id: deploy.pl 1864 2008-01-02 09:58:24Z tfrayner $
#
# Script to automate advanced deployment of tab2mage and related code
# into the ArrayExpress production environment. This is *not* needed
# for basic installation of the Tab2MAGE package.

use strict;
use warnings;

use Getopt::Long;
use T2MInstaller qw(install);

my ( $config, $wanthelp );

GetOptions(
    "c|config=s" => \$config,
    "h|help"     => \$wanthelp,
);

if ( $wanthelp ) {

    print <<"USAGE";

    Usage:   ./deploy.pl

    Options:

	-c or --config     Deployment configuration file to use.
	-h or --help       Display this help message.

    The script must be launched from within the same directory as the
    T2MInstaller.pm module. If the config file is not specified the
    script will look for a file named InstallConfig.yml in the same
    directory, and fail if it is not found or is unreadable.

USAGE

    exit 255;
}

install({

    # Optional config file argument.
    config_file => $config,

    # These options can be used to fine-tune the install
    # location. Different scripts can be set up to deploy into
    # e.g. testing and production environments. Uncomment to set these
    # options.

    # The location to install the perl modules themselves.
    #    inst_lib  => '/full/path/to/local/lib/perl5/site_perl/',

    # Where to install the scripts.
    #    bin       => '/full/path/to/bin/',

    # The location for the man pages.
    #    man1      => '/full/path/to/local/man/man1',
    #    man3      => '/full/path/to/local/man/man3',

    # Any non-standard locations for the perl modules on which this
    # package depends should be included here.
    #    perl_lib  => '/all/paths:/needed/in/PERL5LIB:/environmental/variable',
});
