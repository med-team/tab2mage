[CDF]
Version=GC3.0

[Chip]
Name=YG_S98
Rows=534
Cols=534
NumberOfUnits=10
MaxUnit=9375
NumQCUnits=0
ChipReference=

[Unit1]
Name=NONE
Direction=2
NumAtoms=20
NumCells=40
UnitNumber=1
UnitType=3
NumberBlocks=1

[Unit1_Block1]
Name=AFFX-MurIL2_at
BlockNumber=1
NumAtoms=20
NumCells=40
StartPosition=1
StopPosition=20


[Unit2]
Name=NONE
Direction=2
NumAtoms=20
NumCells=40
UnitNumber=2
UnitType=3
NumberBlocks=1

[Unit2_Block1]
Name=AFFX-MurIL10_at
BlockNumber=1
NumAtoms=20
NumCells=40
StartPosition=1
StopPosition=20


[Unit3]
Name=NONE
Direction=2
NumAtoms=20
NumCells=40
UnitNumber=3
UnitType=3
NumberBlocks=1

[Unit3_Block1]
Name=AFFX-MurIL4_at
BlockNumber=1
NumAtoms=20
NumCells=40
StartPosition=1
StopPosition=20


[Unit4]
Name=NONE
Direction=2
NumAtoms=20
NumCells=40
UnitNumber=4
UnitType=3
NumberBlocks=1

[Unit4_Block1]
Name=AFFX-MurFAS_at
BlockNumber=1
NumAtoms=20
NumCells=40
StartPosition=1
StopPosition=20


[Unit5]
Name=NONE
Direction=2
NumAtoms=20
NumCells=40
UnitNumber=5
UnitType=3
NumberBlocks=1

[Unit5_Block1]
Name=AFFX-BioB-5_at
BlockNumber=1
NumAtoms=20
NumCells=40
StartPosition=1
StopPosition=20


[Unit6]
Name=NONE
Direction=2
NumAtoms=20
NumCells=40
UnitNumber=6
UnitType=3
NumberBlocks=1

[Unit6_Block1]
Name=AFFX-BioB-M_at
BlockNumber=1
NumAtoms=20
NumCells=40
StartPosition=1
StopPosition=20


[Unit7]
Name=NONE
Direction=2
NumAtoms=20
NumCells=40
UnitNumber=7
UnitType=3
NumberBlocks=1

[Unit7_Block1]
Name=AFFX-BioB-3_at
BlockNumber=1
NumAtoms=20
NumCells=40
StartPosition=1
StopPosition=20


[Unit8]
Name=NONE
Direction=2
NumAtoms=20
NumCells=40
UnitNumber=8
UnitType=3
NumberBlocks=1

[Unit8_Block1]
Name=AFFX-BioC-5_at
BlockNumber=1
NumAtoms=20
NumCells=40
StartPosition=1
StopPosition=20


[Unit9]
Name=NONE
Direction=2
NumAtoms=20
NumCells=40
UnitNumber=9
UnitType=3
NumberBlocks=1

[Unit9_Block1]
Name=AFFX-BioC-3_at
BlockNumber=1
NumAtoms=20
NumCells=40
StartPosition=1
StopPosition=20


[Unit10]
Name=NONE
Direction=2
NumAtoms=20
NumCells=40
UnitNumber=10
UnitType=3
NumberBlocks=1

[Unit10_Block1]
Name=AFFX-BioDn-5_at
BlockNumber=1
NumAtoms=20
NumCells=40
StartPosition=1
StopPosition=20


