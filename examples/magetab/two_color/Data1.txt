ATF	1																																	
24	43																																	
"Type=GenePix Results 1.2"																																		
"DateTime=2003/01/23 11:27:59"																																		
"Settings=E:\settings files\array.gps"																																		
"GalFile="																																		
"Scanner=GenePix 4000A"																																		
"Comment="																																		
"PixelSize=10"																																		
"ImageName=635 nm	532 nm"																																	
"FileName=\\Server\Data1.tif"																																		
"PMTVolts=600	590"																																	
"NormalizationFactor:RatioOfMedians=1.90064"																																		
"NormalizationFactor:RatioOfMeans=1.90384"																																		
"NormalizationFactor:MedianOfRatios=1.86554"																																		
"NormalizationFactor:MeanOfRatios=1.23878"																																		
"NormalizationFactor:RegressionRatio=2.45261"																																		
"JpegImage=\\Server\Data1.jpg"																																		
"RatioFormulation=W1/W2 (635 nm/532 nm)"																																		
"Barcode="																																		
"ImageOrigin=1240, 6320"																																		
"JpegOrigin=1840, 7160"																																		
"Creator=GenePix Pro 3.0.0.98"																																		
"Temperature=1.41243"																																		
"LaserPower=2.19092	0.863674"																																	
"LaserOnTime=86000	85977"																																	
"Block"	"Column"	"Row"	"Name"	"ID"	"X"	"Y"	"Dia."	"F635 Median"	"F635 Mean"	"F635 SD"	"B635 Median"	"B635 Mean"	"B635 SD"	"% > B635+1SD"	"% > B635+2SD"	"F635 % Sat."	"F532 Median"	"F532 Mean"	"F532 SD"	"B532 Median"	"B532 Mean"	"B532 SD"	"% > B532+1SD"	"% > B532+2SD"	"F532 % Sat."	"F Pixels"	"B Pixels"	"Sum of Medians"	"Sum of Means"	"F635 Median - B635"	"F532 Median - B532"	"F635 Mean - B635"	"F532 Mean - B532"	"Flags"
1	1	1			2100	7300	90	240	240	54	40	44	21	100	100	0	502	497	98	81	86	32	100	100	0	52	376	621	616	200	421	200	416	0
1	2	1			2410	7300	120	694	671	209	40	41	11	98	98	0	1406	1354	413	81	81	20	100	100	0	120	778	1979	1904	654	1325	631	1273	0
1	3	1			2690	7320	120	501	531	267	41	41	10	99	99	0	998	1080	498	79	82	21	99	98	0	120	810	1379	1491	460	919	490	1001	0
1	4	1			2980	7310	120	783	770	284	40	41	10	99	96	0	1034	1014	331	81	83	22	100	99	0	120	796	1696	1663	743	953	730	933	0
1	5	1			3260	7320	130	457	434	219	40	41	10	100	99	0	998	938	493	81	83	21	100	98	0	120	930	1334	1251	417	917	394	857	0
1	6	1			3570	7310	130	166	172	67	40	41	10	94	94	0	382	388	112	82	83	20	99	98	0	120	914	426	438	126	300	132	306	0
1	7	1			3860	7320	100	63	65	17	40	41	10	76	55	0	182	188	53	79	80	20	100	96	0	80	552	126	134	23	103	25	109	0
1	8	1			4130	7330	120	415	392	122	40	41	10	96	95	0	793	779	215	79	81	20	98	97	0	120	816	1089	1052	375	714	352	700	0
1	9	1			4430	7320	120	153	155	61	39	41	10	97	94	0	374	382	125	80	82	20	99	99	0	120	796	408	418	114	294	116	302	0
1	10	1			4730	7330	120	865	826	343	40	41	10	100	98	0	1011	941	389	79	81	21	99	98	0	120	810	1757	1648	825	932	786	862	0
