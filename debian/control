Source: tab2mage
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Charles Plessy <plessy@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper (>= 10)
Standards-Version: 3.9.8
Vcs-Browser: https://salsa.debian.org/med-team/tab2mage
Vcs-Git: https://salsa.debian.org/med-team/tab2mage.git
Homepage: http://tab2mage.sourceforge.net/

Package: tab2mage
Architecture: all
Depends: ${misc:Depends},
         ${perl:Depends},
         libbio-mage-perl,
         libbio-mage-utils-perl,
         libwww-perl,
         libtie-ixhash-perl,
         liblist-moreutils-perl,
         libtext-csv-perl,
         libreadonly-xs-perl,
         libconfig-yaml-perl,
         libclass-std-perl,
         libparse-recdescent-perl,
         libxml-xerces-perl
Recommends: libdbi-perl,
            libdbd-mysql-perl
Suggests: graphviz
Description: submitting large microarray experiment datasets to public repository database
 Tab2MAGE is a software package written and supported by the ArrayExpress
 curation team, which aims to ease the process of submitting large
 microarray experiment datasets to our public repository database. To
 this end, Tab2MAGE currently includes two tools, the tab2mage.pl script
 itself, and a data file checking script, expt_check.pl. With these
 scripts it is possible to perform an initial data file validation
 against an array design (e.g., in the form of an "Array Description
 File" or ADF), and then to generate MAGE-ML using these data files
 alongside a separate spreadsheet providing MIAME-compliant sample
 annotation.
