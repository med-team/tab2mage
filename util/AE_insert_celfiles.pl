#!/usr/bin/env perl
#
# Script to take a tab-delimited list of BioAssayData identifiers
# mapped to CEL file names, and insert them into the ArrayExpress
# repository.
#
# With thanks to Margus Lukk for the actual CEL insertion code.
#
# Tim Rayner, 2007, EBI
#
# $Id: AE_insert_celfiles.pl 1978 2008-02-28 12:14:14Z tfrayner $
#
# From Margus: "The files go to tt_biodatacube table to netcdf
# field. Although the name of the field indicates that the files
# should be in netcdf format, it is not really so. Nowadays original
# CEL is loaded to the field."

use strict;
use warnings;

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

use DBI;
require DBD::Oracle;

use Term::ReadKey;
use Getopt::Long;
use Readonly;
use English qw( -no_match_vars );

use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Common qw(date_now);
use ArrayExpress::Datafile::Affymetrix;

Readonly my $MARGIN => " " x (length(date_now()) + 2);

########
# SUBS #
########

sub get_char_locator {

    # Return a new lob locator for writing.

    my ( $dbh, $badata_id ) = @_;

    my $sth = $dbh->prepare(
	"select bdc.netcdf from tt_biodatacube bdc, tt_bioassaydata bad, tt_identifiable i"
      . " where i.id=bad.id and bad.biodatavalues_id=bdc.id and i.identifier=? for update",
	{ ora_auto_lob => 0 }
    );

    $sth->execute($badata_id) or die($sth->errstr);
    my $char_locator = $sth->fetchrow_array();
    $sth->finish();

    unless ($char_locator) {
	print STDOUT ("$MARGIN No record for ID $badata_id in database; skipping.\n");
    }

    return $char_locator;
}

########
# MAIN #
########

my ($listfile, $dataformat);
GetOptions(
    "f|file=s"       => \$listfile,
    "d|dataformat=s" => \$dataformat,
);

unless ($listfile) {
    print <<"USAGE";

Usage: $0 -f <file listing IDs and CEL files>

   N.B. File should contain two columns separated by tabs:

   - BioAssayData ID
   - CEL file name

   Data format will typically be CELv3 or CELv4 and should be
   automatically detected by the script. To override, use the option:

     -d <data format>

USAGE
    exit 255;
}

open (my $list_fh, '<', $listfile) or
    die("Can't open listing file $listfile: $!\n");

print STDERR ("Username: ");
chomp( my $username = <STDIN> );

ReadMode 2;
print STDERR ("Password: ");
chomp( my $password = <STDIN> );
ReadMode 0;
print STDERR ("\n");

# From Margus: "It is important that LongReadLen parameter is set big
# while connecting to the db. I used even "LongReadLen => 100000000"
# as some of the CELs where ~50MB."
#
# Note that we undertake to manage our commits ourselves.
#
my $dbh = DBI->connect(
#    "DBI:Oracle:host=progression.ebi.ac.uk;sid=AECUR;port=1521",  # AECUR
    $CONFIG->get_AE_DSN(),                                         # AEPUB1, usually
    $username,
    $password,
    {LongReadLen => 100_000_000, RaiseError => 1, AutoCommit => 0},
) or die($DBI::errstr);

# Line-buffered output (may be unnecessary).
$| = 1;

my $affy_factory = ArrayExpress::Datafile::Affymetrix->new();

my $linenum=0;
LINE:
while (my $line = <$list_fh>) {
    chomp $line;
    $linenum++;
    my ($badata_id, $celfile) = split /\t/, $line;
    unless ($badata_id && $celfile) {
	warn("Can't parse values in line $linenum; skipping.\n");
	next LINE;
    }

    printf STDOUT ("[%s] Processing file %s for BioAssayData ID %s...\n", date_now(), $celfile, $badata_id);

    # Open BLOB file for update.
    open (my $cel_fh, '<', $celfile) or die("Can't open file $celfile: $!\n"); # FIXME not to die somehow?

    my $celformat;
    if ( $dataformat ) {
	$celformat = $dataformat;
    }
    else {
	my $cel = $affy_factory->make_parser( $celfile );
	eval { $cel->parse_header() };
	if ($EVAL_ERROR) {
	    print STDOUT ("$MARGIN Error parsing CEL header for $celfile (skipping): $EVAL_ERROR\n");
	    next LINE;
	}
	$celformat = "CELv" . $cel->get_version();
	seek($cel_fh, 0, 0) or die("Error rewinding filehandle for file $celfile: $!");
    }

    # Arbitrary chunk size; benchmarked reasonably well. N.B. the
    # tt_biodatacube table is set up with 8K records, so an 8K chunk
    # size might be more appropriate, but any multiple of 8K is okay
    # (16K just beat 8K in benchmarking).
    my $chunk_size = 16384;

    # Write file to "netcdf" record.
    my $offset = 1;    # Offsets start at 1, not 0
    my $length = 0;
    my $buffer = q{};

    # Select BLOB for modification.
    my $char_locator = get_char_locator( $dbh, $badata_id ) or next LINE;

    # This loop reads in chunks and writes them to the relevant netcdf
    # field, doing a commit on each chunk. This is to help avoid row
    # locking on very long transactions.
    while( $length = read( $cel_fh, $buffer, $chunk_size ) ) {
	$dbh->ora_lob_write( $char_locator, $offset, $buffer );
	$offset += $length;
	print STDERR ".";
    }

    close($cel_fh);
    print STDERR "\n";

    # If the new BLOB in DB was smaller than the original, adjust the
    # size of the BLOB field (i.e. delete rudiments from previous blob
    # in the end of blob).
    if ( $offset < $dbh->ora_lob_length( $char_locator ) ) {
        $dbh->ora_lob_trim( $char_locator, $offset - 1 );
    }

    print STDOUT "$MARGIN File length: $offset bytes. Setting dataFormat to $celformat...\n";
    my $sth = $dbh->prepare(   # FIXME this could probably be more concise
	"update tt_biodatacube set dataformat=? where id in"
      . " (select bdc.id from tt_biodatacube bdc, tt_bioassaydata bad, tt_identifiable i"
      . " where i.id=bad.id and bad.biodatavalues_id=bdc.id and i.identifier=?)"
    );
    $sth->execute($celformat,$badata_id) or die($sth->errstr);
    $sth->finish();

    # Commit between each file.
    $dbh->commit();
}

printf STDOUT ("[%s] Done.\n", date_now() );
