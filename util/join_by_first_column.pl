#!/sw/arch/bin/perl
#
# Joins a series of data files using the first column as index.
#
# $Id: join_by_first_column.pl 1861 2007-12-24 17:26:19Z tfrayner $

use strict;
use warnings;

unless (@ARGV) {
    print STDOUT (<<"USAGE");
    Usage: $0 list of data files > new_FGEM_file.txt

USAGE

    exit 255;
}

# %HoH stores the data:
#
# {$file1 => {$index1 => \@row, $index2 => \@row}, $file2 => {}...]
#
# where $index is the first-column index.
my %HoH;

# %common_indices simply aggregates all the first-column indices in
# the files.
my %common_indices;

# $uniq is a random unique tag, can be anything; this is used to keep
# track of duplicate first-column indices. Set to a null byte since
# it's unlikely we'll encounter that in a data file.
my $uniq = qq{\x0};

foreach my $file (@ARGV) {

    open( my $in_fh, "<$file" ) or die("$!\n");

    my %store;
    while ( my $line = <$in_fh> ) {

	# DOS and unix line endings
        $line =~ s/[\r\n]*$//;

        my @larry = split /\t/, $line, -1;

        foreach my $value (@larry) {

	    # Allow zero values.
	    if ( ! defined($value) || $value eq q{} ) {
		$value = 'null';
	    }
	}

        my $index = shift(@larry);

        while ( exists( $store{$index} ) ) {
            $index .= $uniq;
        }

        $store{$index} = \@larry;

        $common_indices{$index}++;
    }

    $HoH{$file} = \%store;

}

# Figure out the row array sizes
my %max_row_sizes;
foreach my $file (@ARGV) {

    my $dataset = $HoH{$file};
    my $max_row_size = [];
    my @datarows     = values %$dataset;
    foreach my $newmax_row_size (@datarows) {
        if ( $#$max_row_size < $#$newmax_row_size ) {
            $max_row_size = $newmax_row_size;
        }
    }
    $max_row_sizes{$file} = $max_row_size;
}

# Print out a header
my @header = q{};
foreach my $file ( sort keys %HoH ) {
    push(@header, map { $file } @{ $max_row_sizes{$file} });
}
print STDOUT (join("\t", @header),"\n");

# Print out the data.
foreach my $index ( sort keys %common_indices ) {

    my $orig_index = $index;

    $orig_index =~ s/$uniq//g;

    print STDOUT "$orig_index";

    foreach my $file (@ARGV) {

	my $dataset = $HoH{$file};

        my $max_row_size = $max_row_sizes{$file};

        my $datastr;

        if ( $dataset->{$index} ) {
            $datastr = join( "\t", @{ $dataset->{$index} } );
        }

        else {
            $datastr = join( "\t", map {'null'} @$max_row_size );
        }

        print STDOUT ("\t$datastr");
    }

    print STDOUT "\n";

}
