#!/usr/bin/env perl
#
# Script to re-check legacy spreadsheet submissions, and enter the
# relevant info into the autosubs database.
#
# $Id$

use strict;
use warnings;

use ArrayExpress::Curator::Validate;
use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Common qw(date_now);
require ArrayExpress::AutoSubmission::DB::Experiment;
use Getopt::Long;

########
# SUBS #
########

sub check_sub {

    my $sub = shift;

    my $spreadsheet = $sub->spreadsheets(is_deleted => 0)->next();

    # Quick sanity check.
    unless ( $spreadsheet ) {
	printf("Submission %s has no spreadsheet. Skipping.\n",
	       $sub->id());
	return;
    }

    printf(
        "%s Checking experiment %s:  %s | %s\n",
        date_now(),
	$sub->id(),
	$sub->user_id()->login(),
        $sub->name()
    );

    my $checker = ArrayExpress::Curator::Validate->new(
        {   spreadsheet_filename => $spreadsheet->filesystem_path(),
	    source_directory     => $sub->unpack_directory(),
	    skip_data_checks     => 1,
            log_to_current_dir   => q{},
            clobber              => 1,
        }
    );

    eval { $checker->check() };

    if ($@) {
        $sub->set(
            comment => $sub->comment() . "\n\nLegacy checks crashed: $@",
        );
        print("Checker CRASH!!\n");
    }
    else {
        $sub->set(
            miame_score          => $checker->get_miame(),
            data_warehouse_ready => $checker->get_aedw_score(),
        );
    }

    $sub->update();

    return;
}

########
# MAIN #
########

my ($check_all, $experiment_type);
GetOptions(
    "a|all"    => \$check_all,
    "t|type=s" => \$experiment_type,
);

unless ( $check_all || scalar @ARGV ) {
    print(<<"USAGE");
    Usage: $0 -a            (check all experiments in submissions database)
	   $0 <sub ids>     (just check a handful)

    Optional: -t    (specify experiment type, e.g. "Tab2MAGE", "GEO")

USAGE

    exit 255;
}

$experiment_type ||= 'Tab2MAGE';

if ($check_all) {

    # Check all undeleted spreadsheet experiments.
    print("Querying autosubs DB for all $experiment_type spreadsheet experiments...\n");

    my $expt_iterator = ArrayExpress::AutoSubmission::DB::Experiment->search(
        is_deleted      => 0,
        experiment_type => $experiment_type,
    );

    EVERYEXPT:
    while ( my $sub = $expt_iterator->next() ) {
	if ( $sub->user_id()
		 && scalar( $sub->spreadsheets(is_deleted => 0) ) ) {
	    check_sub($sub);
	}
    }
}
else {

    # Check only those experiments listed on the command line.
    EACHEXPT:
    foreach my $id (@ARGV) {
        print("Querying autosubs DB for $experiment_type spreadsheet submission $id...\n");
        my $sub = ArrayExpress::AutoSubmission::DB::Experiment->retrieve(
            id                => $id,
            experiment_type   => $experiment_type,
            is_deleted        => 0,
	    in_curation       => 1,
        );
	if ( $sub ) {
	    unless ( $sub->user_id() ) {
		print ("Submission $id has no associated user. Skipping.\n");
		next EACHEXPT;
	    }

	    # Run the tests here.
	    check_sub($sub);
	}
	else {
	    print ("Error: cannot find submission with id $id in database. Skipping.\n");
	}
    }
}
