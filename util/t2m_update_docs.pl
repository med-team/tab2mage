#!/usr/bin/env perl
#
# t2m_update_docs.pl
#
# Tim Rayner 2005 ArrayExpress Team, EBI
#
# $Id: t2m_update_docs.pl 2069 2008-06-04 14:33:52Z tfrayner $
#

use 5.8.0;    # Need a recent Pod::HTML, which only comes with newer Perls

use strict;
use warnings;

use Pod::Html;
use File::Spec;

foreach my $doc qw(
    bin/expt_check.pl
    bin/tab2mage.pl
    bin/magetab.pl
    bin/t2m_visualize.pl
    bin/parse_affy.pl
    lib/ArrayExpress/ArrayMAGE.pm
    lib/ArrayExpress/AutoSubmission/DB.pm
    lib/ArrayExpress/AutoSubmission/Creator.pm
    lib/ArrayExpress/AutoSubmission/Spreadsheet.pm
    lib/ArrayExpress/AutoSubmission/WebForm.pm
    lib/ArrayExpress/Curator/Config.pm
    lib/ArrayExpress/Curator/Common.pm
    lib/ArrayExpress/Curator/Database.pm
    lib/ArrayExpress/Curator/Report.pm
    lib/ArrayExpress/Curator/Entrez_list.pm
    lib/ArrayExpress/Curator/Visualize.pm
    lib/ArrayExpress/Curator/ExperimentChecker.pm
    lib/ArrayExpress/Curator/Logger.pm
    lib/ArrayExpress/Curator/MAGE/Definitions.pm
    lib/ArrayExpress/Curator/MIAMExpress.pm
    lib/ArrayExpress/Curator/Standalone.pm
    lib/ArrayExpress/Curator/Validate.pm
    lib/ArrayExpress/Datafile/Affymetrix/CDF/GDAC_CDF.pm
    lib/ArrayExpress/Datafile/Affymetrix/CDF/XDA_CDF.pm
    lib/ArrayExpress/Datafile/Affymetrix/CDF.pm
    lib/ArrayExpress/Datafile/Affymetrix/CDFFactory.pm
    lib/ArrayExpress/Datafile/Affymetrix/CEL/CELv3.pm
    lib/ArrayExpress/Datafile/Affymetrix/CEL/CELv4.pm
    lib/ArrayExpress/Datafile/Affymetrix/CEL.pm
    lib/ArrayExpress/Datafile/Affymetrix/CELFactory.pm
    lib/ArrayExpress/Datafile/Affymetrix/CHP/CHPv12.pm
    lib/ArrayExpress/Datafile/Affymetrix/CHP/CHPv13.pm
    lib/ArrayExpress/Datafile/Affymetrix/CHP/CHPv8.pm
    lib/ArrayExpress/Datafile/Affymetrix/CHP/GDAC_CHP.pm
    lib/ArrayExpress/Datafile/Affymetrix/CHP/XDA_CHP.pm
    lib/ArrayExpress/Datafile/Affymetrix/CHP.pm
    lib/ArrayExpress/Datafile/Affymetrix/CHPFactory.pm
    lib/ArrayExpress/Datafile/Affymetrix/EXP.pm
    lib/ArrayExpress/Datafile/Affymetrix/EXPFactory.pm
    lib/ArrayExpress/Datafile/Affymetrix/Parser.pm
    lib/ArrayExpress/Datafile/Affymetrix.pm
    lib/ArrayExpress/Datafile/Parser.pm
    lib/ArrayExpress/Datafile/QT_list.pm
    lib/ArrayExpress/Datafile/ArrayDesign.pm
    lib/ArrayExpress/Datafile.pm
    lib/ArrayExpress/MAGETAB.pm
    lib/ArrayExpress/MAGETAB/Checker.pm
    ) {

    unless ( -f $doc ) {
        die("Error: Input file not found: $doc\n");
    }

    my ( $directory, $file ) = ( File::Spec->splitpath($doc) )[ 1, 2 ];

    my @dirs = File::Spec->splitdir($directory);

    shift @dirs;    # top-level dir is always stripped

    my $htmldoc
        = File::Spec->catfile( 'docs', @dirs, $file );    # docs subdir

    $htmldoc =~ s!\.p[mlh]$!\.html!i;                 # replace extension

    print "Creating $htmldoc\n";

    pod2html(
        "--infile=$doc",         "--outfile=$htmldoc",
        "--css=/docs/style.css", "--noindex",
        "--htmldir=docs",        "--podroot=lib",
    );

    system("tidy -m $htmldoc");

}
