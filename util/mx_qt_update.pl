#!/usr/bin/env perl
#
# mx_qt_update.pl
#
# Maintenance script to update a QT_list.txt file with QT information taken from a local MIAMExpress installation.
#
# Tim Rayner 2005 ArrayExpress Team, EBI
#
# $Id: mx_qt_update.pl 1269 2006-12-10 12:23:13Z tfrayner $
#

use strict;
use warnings;

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

use Getopt::Long;
use DBI;
use DBD::mysql;

use ArrayExpress::Curator::Config qw($CONFIG);

use ArrayExpress::Datafile::QT_list qw(
    get_QTs
    write_QTs
);

my $read_defaults;

GetOptions( "d|defaults" => \$read_defaults, );

my $new_QTs = {};

if ($read_defaults) {
    $new_QTs = get_QTs;
}
elsif (@ARGV) {
    print STDOUT ("Parsing old QT file(s)... \n");
    $new_QTs = get_QTs( \@ARGV );
}

print STDOUT ("Querying MIAMExpress for QT information... \n");

my $dbh = DBI->connect(
    $CONFIG->get_MX_DSN(),      $CONFIG->get_MX_USERNAME(),
    $CONFIG->get_MX_PASSWORD(), $CONFIG->get_MX_DBPARAMS()
    )
    or die("$DBI::errstr\n");

# Pull in all undeleted QTs
my $sth = $dbh->prepare(<<'QUERY');
select distinct TQUANTIT_TYPE, TQUANTIT_NAME, TQUANTIT_SUBCLASS,
TQUANTIT_ISBACKGROUND, TQUANTIT_CHANNEL, TQUANTIT_SCALE,
TQUANTIT_DATATYPE, TQUANTIT_TARGETQT, TQUANTIT_NAMESPACE,
TQUANTIT_DESCR
from TQUANTIT
where TQUANTIT_DEL_STATUS='U'
QUERY

$sth->execute() or die("$sth->errstr\n");

# Get the relevant stuff out of $sth
while ( my $rowref = $sth->fetchrow_hashref ) {

    my $software = $rowref->{'TQUANTIT_TYPE'} . "["
        . $rowref->{'TQUANTIT_NAMESPACE'} . "]";

    $new_QTs->{$software}{ $rowref->{'TQUANTIT_NAME'} }{datatype}
        = $rowref->{'TQUANTIT_DATATYPE'} || q{};

    $new_QTs->{$software}{ $rowref->{'TQUANTIT_NAME'} }{scale}
        = $rowref->{'TQUANTIT_SCALE'} || q{};

    $new_QTs->{$software}{ $rowref->{'TQUANTIT_NAME'} }{subclass}
        = $rowref->{'TQUANTIT_SUBCLASS'} || q{};

    $new_QTs->{$software}{ $rowref->{'TQUANTIT_NAME'} }{is_background}
        = $rowref->{'TQUANTIT_ISBACKGROUND'} || q{};

    $new_QTs->{$software}{ $rowref->{'TQUANTIT_NAME'} }{confmap}
        = $rowref->{'TQUANTIT_TARGETQT'} || q{};

    $new_QTs->{$software}{ $rowref->{'TQUANTIT_NAME'} }{channel}
        = $rowref->{'TQUANTIT_CHANNEL'} || q{};

    $new_QTs->{$software}{ $rowref->{'TQUANTIT_NAME'} }{description}
        = $rowref->{'TQUANTIT_DESCR'} || q{};
}

$sth->finish();  # dump the statement handler now that we are finished with it

write_QTs( $new_QTs, \*STDOUT );

