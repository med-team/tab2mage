#!/usr/bin/env perl
#
# fillin_null_rows.pl

# A script taking a list of data files, and rewriting them with added
# null rows such that subsequently generated DesignElementDimensions
# are all the same. The original files are untouched; new files are
# created with a ".new" extension.

# $Id: fillin_null_rows.pl 1861 2007-12-24 17:26:19Z tfrayner $

use strict;
use warnings;

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

# Added to circumvent weird Exporter bug in 5.8.3 which caused
# crashing when requiring ArrayExpress::Datafile (which in turn uses
# ArrayExpress::Curator::Common).
use ArrayExpress::Curator::Common;

use IO::File;
use ArrayExpress::Datafile;

# Use a null byte char to delimit the index coords.
my $tag_delim = qq{\x0};

unless (@ARGV) {
    print STDOUT (<<"USAGE");
    Usage: $0 <list of data files>

USAGE

    exit 255;
}

my %coordinate;
foreach my $filename (@ARGV) {

    print STDERR ("Recording coordinates for file $filename...\n");

    my $file = ArrayExpress::Datafile->new({
	name     => $filename,
	is_dummy => 1,
    });

    my $input_fh = $file->get_filehandle();

    # Figure out what kind of file we have, get column headings and indices
    $file->parse_header($input_fh);

    while (my $line = <$input_fh>){
	my @larry = split /\t/, $line, -1;

	$coordinate{ join($tag_delim, @larry[ @{ $file->get_index_columns() } ] ) }++;
    }
}

foreach my $filename (@ARGV) {

    print STDERR ("Rewriting file $filename...\n");

    my $output_fh = IO::File->new( "$filename.full", '>' )
        or die("Error opening output file $filename.full: $!\n");

    my $file = ArrayExpress::Datafile->new({
	name     => $filename,
	is_dummy => 1,
    });

    my $input_fh = $file->get_filehandle();

    # Figure out what kind of file we have, get column headings and indices
    $file->parse_header($input_fh);

    my %index = map { $_ => 1 } @{ $file->get_index_columns() };
    my @indexlist = @{ $file->get_index_columns() };

    my @headings = @{$file->get_column_headings()};
    my @harry;
    for (my $i = 0; $i <= $#headings; $i++) {
	push(@harry, $index{$i} ? $headings[shift(@indexlist)] : $headings[$i]);
    }
    print $output_fh (join("\t", @harry), "\n");

    my %data;
    while (my $line = <$input_fh>){

	# Handle both unix and dos cleanly
	$line =~ s/[\r\n]*$//;
	my @larry = split /\t/, $line, -1;

	$data{ join($tag_delim, @larry[ @{ $file->get_index_columns() } ] ) }
	    = \@larry;
    }

    foreach my $coord ( sort keys %coordinate ) {

	my @coords = split /$tag_delim/, $coord;

	if (my $row = $data{$coord}) {

	    my @larry;
	    for (my $i = 0; $i <= $#$row; $i++) {
		push(@larry, $index{$i} ? shift(@coords) : $row->[$i]);
	    }

	    print $output_fh (
		join("\t", @larry), "\n",
	    );
	}
	else { # empty row

	    my @larry;
	    for (my $i = 0; $i <= $#{ $file->get_column_headings() }; $i++) {
		push(@larry, $index{$i} ? shift(@coords) : q{null});
	    }
	    print $output_fh (
		join("\t", @larry), "\n",
	    );
	}
    }
}
