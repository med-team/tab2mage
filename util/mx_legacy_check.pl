#!/usr/bin/env perl
#
# Script to re-check legacy MX submissions, and enter the relevant
# info into the autosubs database.
#
# $Id: mx_legacy_check.pl 497 2006-10-10 21:04:09Z tfrayner $

use strict;
use warnings;

require ArrayExpress::Curator::MIAMExpress;
use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Common qw(date_now);
require ArrayExpress::AutoSubmission::DB::Experiment;
use DBI;
use Getopt::Long;

# Set up a global DB handle for MX; N.B. another handle will be
# created for the checker instances (see MIAMExpress.pm).
my $MX_DBH = DBI->connect(
    $CONFIG->get_MX_DSN(),      $CONFIG->get_MX_USERNAME(),
    $CONFIG->get_MX_PASSWORD(), $CONFIG->get_MX_DBPARAMS(),
    )
    or die("Database connection error: $DBI::errstr\n");

########
# SUBS #
########

sub check_sub {

    my $sub = shift;

    # Quick sanity check.
    unless ( $sub->name() && $sub->miamexpress_login() ) {
	printf("Submission %s has incomplete mx title and/or login. Skipping.\n",
	       $sub->miamexpress_subid());
	return;
    }

    printf(
        "%s Checking experiment %s:  %s | %s\n",
        date_now(),
	$sub->miamexpress_subid(),
	$sub->miamexpress_login(),
        $sub->name()
    );

    my $checker = ArrayExpress::Curator::MIAMExpress->new(
        {   mx_login           => $sub->miamexpress_login(),
            mx_title           => $sub->name(),
            skip_data_checks   => 1,
            log_to_current_dir => '',
            clobber            => 1,
        }
    );

    eval { $checker->check() };

    if ($@) {
        $sub->set(
            comment => $sub->comment() . "\n\nLegacy checks crashed: $@",
        );
        print("Checker CRASH!!\n");
    }
    else {
        $sub->set(
            miame_score          => $checker->get_miame(),
            data_warehouse_ready => $checker->get_aedw_score(),
        );
    }

    my $sth = $MX_DBH->prepare(<<'QUERY');
select TSUBMIS_LAST_CHANGE from TSUBMIS
where TSUBMIS_SYSUID=? and TSUBMIS_DEL_STATUS='U'
QUERY

    $sth->execute( $sub->miamexpress_subid() ) or die( $sth->errstr );

    my $results = $sth->fetchrow_arrayref();

    if ($results) {

        my $mx_date = $results->[0];

        my @dateparts;
        unless (
            @dateparts = (
                $mx_date
                    =~ m/\A (\d+)-(\d+)-(\d+) [ ]+ (\d+):(\d+):(\d+) \z/xms
            )
            ) {
            die( "Error: Unable to parse date for submission "
                    . $sub->miamexpress_subid() );
        }

        my $date = join( '-', @dateparts[ 0 .. 2 ] ) . 'T'
            . join( ':', @dateparts[ 3 .. 5 ] ) . 'Z';

        $sub->set( date_submitted => $date, );
    }

    $sub->update();

    return;
}

########
# MAIN #
########

my $check_all;
GetOptions( "a|all" => \$check_all );

unless ( $check_all || scalar @ARGV ) {
    print(<<"USAGE");
    Usage: $0 -a            (check all experiments in submissions database)
	   $0 <MX subids>   (just check a handful)
USAGE

    exit 255;
}

if ($check_all) {

    # Check all undeleted MX experiments.
    print("Querying autosubs DB for all MX experiments...\n");

    my $expt_iterator = ArrayExpress::AutoSubmission::DB::Experiment->search(
        is_deleted      => 0,
        experiment_type => 'MIAMExpress',
    );
    while ( my $sub = $expt_iterator->next() ) {
        check_sub($sub);
    }
}
else {

    # Check only those experiments listed on the command line.
    foreach my $subid (@ARGV) {
        print("Querying autosubs DB for MX submission $subid...\n");
        my $sub = ArrayExpress::AutoSubmission::DB::Experiment->retrieve(
            miamexpress_subid => $subid,
            experiment_type   => 'MIAMExpress',
            is_deleted        => 0,
        );

	if ( $sub ) {
	    check_sub($sub);
	}
	else {
	    print ("Error: cannot find subid $subid in database. Skipping.\n");
	}
    }
}
