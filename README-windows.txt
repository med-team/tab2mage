Unfortunately I am unable to fully support MS Windows installations,
simply due to lack of time and the relative complexity of doing
so. However, users wishing to try Tab2MAGE on Windows may find the
following instructions useful.

The following procedure has been found to work on a MS Windows 2000
installation without using either administrator rights or a compiler.

1. Install ActiveState Perl version 5.8.6 (build 811) or newer. Make a
note of the location of the perl executable. The default location is
in C:\Perl\bin. You may need to add this location to your PATH
variable.

2. Download XML-Xerces from the following directory:

http://www.apache.org/dist/xml/xerces-p/binaries/

You will need the file named "XML-Xerces-2.3.0-4.win32.zip". Newer
versions are untested, but may work. Unzip the download. The Readme
file which is extracted should help you install the module, but if it
is absent or unclear, continue as follows:

Copy or move the unzipped .dll file into the same directory as the
perl executable (e.g., C:\Perl\bin).

Also unzipped from this package are two directories, "perl5.6" and
"perl5.8". Open a command prompt, change to the perl5.8 directory, and
execute the following command:

    ppm install XML-Xerces.ppd

This should install the XML::Xerces module and supporting C library.

3. Use ppm to install the following modules and their dependencies:

    ppm install Tie-IxHash
    ppm install Bio-MAGE
    ppm install Readonly
    ppm install List-MoreUtils
    ppm install Text-CSV_XS
    ppm install Config-YAML
    ppm install Class-Std
    ppm install Parse-RecDescent

4. Install nmake, downloaded from here:

http://download.microsoft.com/download/vc15/Patch/1.52/W95/EN-US/Nmake15.exe

5. Since you've got this far, we'll assume you already have the
Tab2MAGE package. If not, download it and uncompress it.

6. In the Tab2MAGE top level directory, install the package by
executing the following:

    perl Makefile.PL
    nmake
    nmake install

You should now have a working installation of Tab2MAGE. Congratulations.

---------------------------------------------------------------------
Copyright Tim Rayner <rayner@ebi.ac.uk> ArrayExpress Team, EBI, 2007.
