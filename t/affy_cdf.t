#!/usr/bin/env perl -wT
#
# $Id: affy_cel.t 1799 2007-10-31 17:39:45Z tfrayner $

use strict;
use warnings;

use Test::More tests => 11;

use File::Spec;
BEGIN {
    use_ok ('ArrayExpress::Datafile::Affymetrix');
    use_ok ('ArrayExpress::Datafile::Affymetrix::CDF::GDAC_CDF');
}

# Use the factory class to generate a parser, hopefully of the right
# type.
my $fac = ArrayExpress::Datafile::Affymetrix->new();

my $cdf = $fac->make_parser( File::Spec->catfile( 't', 'data', 'dummy.CDF' ) );

# Create the instance
ok( defined $cdf, 'new() returned a value' );
ok( $cdf->isa('ArrayExpress::Datafile::Affymetrix::CDF::GDAC_CDF'),
    'of the correct class' );

# Parse the test file
$cdf->parse();

is( $cdf->get_version,      'GC3.0',  'version' );
is( $cdf->get_chip_type,    'YG_S98', 'chip type' );
is( $cdf->get_num_rows,     '534',    'number of rows' );
is( $cdf->get_num_columns,  '534',    'number of columns' );
is( $cdf->get_num_cells,    '10',     'expected number of cells' );
is( $cdf->get_num_qc_cells, '0',      'expected number of qc cells' );

my $expected_ids =
    [qw(
	AFFX-MurIL2_at
	AFFX-MurIL10_at
	AFFX-MurIL4_at
	AFFX-MurFAS_at
	AFFX-BioB-5_at
	AFFX-BioB-M_at
	AFFX-BioB-3_at
	AFFX-BioC-5_at
	AFFX-BioC-3_at
	AFFX-BioDn-5_at
    )];

is_deeply( $cdf->get_probeset_ids, $expected_ids, 'probeset id list' );
