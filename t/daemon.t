#!/usr/bin/env perl -wT
#
# $Id: daemon.t 2069 2008-06-04 14:33:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 1;

SKIP: {

    eval {
	require Class::DBI;
	require MIME::Lite;
	require Archive::Tar;
	require Archive::Zip;
	require Proc::Daemon;
    };

    skip 'MIME::Lite, Archive::Tar, Archive::Zip, Proc::Daemon or Class::DBI not installed',
	1 if $@;

    require_ok( 'ArrayExpress::AutoSubmission::Daemon' );

}

