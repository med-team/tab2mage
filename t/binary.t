#!/usr/bin/env perl -wT
#
# $Id: binary.t 1269 2006-12-10 12:23:13Z tfrayner $

use strict;
use warnings;

use Test::More tests => 1;

BEGIN {
    use_ok(
	'ArrayExpress::Datafile::Binary',
	qw(
	   get_integer
	   get_DWORD
	   get_unsigned_short
	   get_unsigned_char
	   get_signed_char
	   get_float
	   get_ascii
	   get_hexadecimal
       ));
}
