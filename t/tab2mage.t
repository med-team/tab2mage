#!/usr/bin/env perl -wT
#
# $Id: tab2mage.t 1298 2006-12-20 19:44:56Z tfrayner $

use strict;
use warnings;

use Test::More tests => 15;

SKIP: {

    eval { require Bio::MAGE };

    skip 'Bio::MAGE not installed', 1 if $@;

    require_ok( 'ArrayExpress::Curator::Tab2MAGE' );

}

my $t2m = ArrayExpress::Curator::Tab2MAGE->new({
    target_directory     => 't',
    spreadsheet_filename => 'test.txt',
});
ok( defined $t2m, 'new() returned a value' );
ok( $t2m->isa('ArrayExpress::Curator::Tab2MAGE'),
    'of the correct class' );

# Test our basic attempts to sanitize protocol text for display in ArrayExpress.
is($t2m->htmlify_protocoltext("&"),      "&amp;",    'htmlify ampersand');
is($t2m->htmlify_protocoltext("\n"),     "<br>\n",   'htmlify unix newline');
is($t2m->htmlify_protocoltext("\r\n"),   "<br>\r\n", 'htmlify dos newline');
is($t2m->htmlify_protocoltext("&deg;"),  "&deg;",    'htmlify named entity');
is($t2m->htmlify_protocoltext("&#916;"), "&#916;",   'htmlify numbered entity');
is($t2m->htmlify_protocoltext("�"),      "&deg;",    'htmlify degree (�) sign');
is($t2m->htmlify_protocoltext("�X"),     "&deg;",    'htmlify degree (�X) sign');
is($t2m->htmlify_protocoltext("�"),      "&#956;",   'htmlify mu (�) character');
is($t2m->htmlify_protocoltext("��"),  "&#956;",   'htmlify mu (��) character');
is($t2m->htmlify_protocoltext("�"),   "&apos;",   'htmlify apostrophe (�) character');
is($t2m->htmlify_protocoltext("�"),   "&quot;",   'htmlify quote (�) character');
is($t2m->htmlify_protocoltext("�"),   "&quot;",   'htmlify quote (�) character');
