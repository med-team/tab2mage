#!/usr/bin/env perl -wT
#
# $Id: affy_cel.t 1811 2007-11-02 10:34:42Z tfrayner $

use strict;
use warnings;

use Test::More tests => 22;

use File::Spec;

BEGIN {
    use_ok ('ArrayExpress::Datafile::Affymetrix');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::Binary');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::Parameter');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::DataColumn');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::Component');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::DataHeader');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::DataSet');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::DataGroup');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::Generic');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::Facade');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::CHP');
    use_ok ('ArrayExpress::Datafile::Affymetrix::Calvin::CEL');
}

# Use the factory class to generate a parser, hopefully of the right
# type.
my $fac = ArrayExpress::Datafile::Affymetrix->new();

my $chp = $fac->make_parser( File::Spec->catfile( 't', 'data', 'test_calvin.CHP' ) );

# Create the instance
ok( defined $chp, 'new() returned a value' );
ok( $chp->isa('ArrayExpress::Datafile::Affymetrix::Calvin::CHP'),
    'of the correct class' );

# Check our return values
is( $chp->get_chip_type,    'HG-U133A',       'chip type' );
is( $chp->get_algorithm,    'ExpressionStat', 'algorithm' );
is( $chp->get_version,      '1',              'version' );

# Parameters
my $expected_params = {
    'Alpha1'    => '0.05000',
    'Alpha2'    => '0.06500',
    'BG'        => '2',
    'HZ'        => '4',
    'NF'        => '1.00000',
    'SF'        => '1.91583',
    'ScaleMask' => 'All',
    'TGT'       => '500',
    'Tau'       => '0.01500',
    'VZ'        => '4',
};

is_deeply( $chp->get_parameters, $expected_params, 'parameters' );

# Statistics
my $expected_stats = {
    '#A'                                             => '10615',
    '#M'                                             => '363',
    '#P'                                             => '11305',
    '#Probe Sets Exceeding Probe Pair Threshold'     => '22283',
    '%A'                                             => '47.63721',
    '%M'                                             => '1.62904',
    '%P'                                             => '50.73374',
    'BG Avg'                                         => '117.28572',
    'BG Max'                                         => '124.08419',
    'BG Min'                                         => '106.84966',
    'BG Std'                                         => '3.34962',
    'Central- Avg'                                   => '21585',
    'Central- Count'                                 => '9',
    'Control Direction'                              => 'Antisense',
    'Corner+ Avg'                                    => '157',
    'Corner+ Count'                                  => '32',
    'Corner- Avg'                                    => '22492',
    'Corner- Count'                                  => '32',
    'Housekeeping_AFFX-HSAC07/X00351_3-5-ratio'      => '1.11354',
    'Housekeeping_AFFX-HSAC07/X00351_3_detection'    => 'P',
    'Housekeeping_AFFX-HSAC07/X00351_3_signal'       => '17738.18555',
    'Housekeeping_AFFX-HSAC07/X00351_5_detection'    => 'P',
    'Housekeeping_AFFX-HSAC07/X00351_5_signal'       => '15929.51465',
    'Housekeeping_AFFX-HSAC07/X00351_M_detection'    => 'P',
    'Housekeeping_AFFX-HSAC07/X00351_M_signal'       => '20702.16602',
    'Housekeeping_AFFX-HSAC07/X00351_avg-signal'     => '18123.28906',
    'Housekeeping_AFFX-HUMGAPDH/M33197_3-5-ratio'    => '1.53856',
    'Housekeeping_AFFX-HUMGAPDH/M33197_3_detection'  => 'P',
    'Housekeeping_AFFX-HUMGAPDH/M33197_3_signal'     => '13372.21094',
    'Housekeeping_AFFX-HUMGAPDH/M33197_5_detection'  => 'P',
    'Housekeeping_AFFX-HUMGAPDH/M33197_5_signal'     => '8691.37793',
    'Housekeeping_AFFX-HUMGAPDH/M33197_M_detection'  => 'P',
    'Housekeeping_AFFX-HUMGAPDH/M33197_M_signal'     => '9520.40723',
    'Housekeeping_AFFX-HUMGAPDH/M33197_avg-signal'   => '10527.99902',
    'Noise Avg'            => '6.49794',
    'Noise Max'            => '7.35717',
    'Noise Min'            => '5.61606',
    'Noise Std'            => '0.29578',
    'Probe Pair Threshold' => '1',
    'RawQ'                 => '5.36501',
    'Signal(A)'            => '65.11629',
    'Signal(All)'          => '728.75806',
    'Signal(M)'            => '211.16080',
    'Signal(P)'            => '1368.51440',
    'Spike_AFFX-r2-Bs-dap_3-5-ratio'   => '0.79929',
    'Spike_AFFX-r2-Bs-dap_3_detection' => 'A',
    'Spike_AFFX-r2-Bs-dap_3_signal'    => '2.09471',
    'Spike_AFFX-r2-Bs-dap_5_detection' => 'A',
    'Spike_AFFX-r2-Bs-dap_5_signal'    => '2.62071',
    'Spike_AFFX-r2-Bs-dap_M_detection' => 'A',
    'Spike_AFFX-r2-Bs-dap_M_signal'    => '25.02802',
    'Spike_AFFX-r2-Bs-dap_avg-signal'  => '9.91448',
    'Spike_AFFX-r2-Bs-lys_3-5-ratio'   => '2.06401',
    'Spike_AFFX-r2-Bs-lys_3_detection' => 'P',
    'Spike_AFFX-r2-Bs-lys_3_signal'    => '49.26870',
    'Spike_AFFX-r2-Bs-lys_5_detection' => 'A',
    'Spike_AFFX-r2-Bs-lys_5_signal'    => '23.87037',
    'Spike_AFFX-r2-Bs-lys_M_detection' => 'A',
    'Spike_AFFX-r2-Bs-lys_M_signal'    => '8.82265',
    'Spike_AFFX-r2-Bs-lys_avg-signal'  => '27.32057',
    'Spike_AFFX-r2-Bs-phe_3-5-ratio'   => '8.54006',
    'Spike_AFFX-r2-Bs-phe_3_detection' => 'A',
    'Spike_AFFX-r2-Bs-phe_3_signal'    => '44.97870',
    'Spike_AFFX-r2-Bs-phe_5_detection' => 'A',
    'Spike_AFFX-r2-Bs-phe_5_signal'    => '5.26679',
    'Spike_AFFX-r2-Bs-phe_M_detection' => 'A',
    'Spike_AFFX-r2-Bs-phe_M_signal'    => '18.66685',
    'Spike_AFFX-r2-Bs-phe_avg-signal'  => '22.97078',
    'Spike_AFFX-r2-Bs-thr_3-5-ratio'   => '0.10473',
    'Spike_AFFX-r2-Bs-thr_3_detection' => 'A',
    'Spike_AFFX-r2-Bs-thr_3_signal'    => '5.12869',
    'Spike_AFFX-r2-Bs-thr_5_detection' => 'A',
    'Spike_AFFX-r2-Bs-thr_5_signal'    => '48.96920',
    'Spike_AFFX-r2-Bs-thr_M_detection' => 'A',
    'Spike_AFFX-r2-Bs-thr_M_signal'    => '77.69570',
    'Spike_AFFX-r2-Bs-thr_avg-signal'  => '43.93119',
    'Spike_AFFX-r2-Ec-bioB_3-5-ratio'  => '0.91620',
    'Spike_AFFX-r2-Ec-bioB_3_detection' => 'P',
    'Spike_AFFX-r2-Ec-bioB_3_signal'    => '103.24037',
    'Spike_AFFX-r2-Ec-bioB_5_detection' => 'P',
    'Spike_AFFX-r2-Ec-bioB_5_signal'    => '112.68278',
    'Spike_AFFX-r2-Ec-bioB_M_detection' => 'P',
    'Spike_AFFX-r2-Ec-bioB_M_signal'    => '151.41463',
    'Spike_AFFX-r2-Ec-bioB_avg-signal'  => '122.44592',
    'Spike_AFFX-r2-Ec-bioC_3-5-ratio'   => '1.31688',
    'Spike_AFFX-r2-Ec-bioC_3_detection' => 'P',
    'Spike_AFFX-r2-Ec-bioC_3_signal'    => '580.93274',
    'Spike_AFFX-r2-Ec-bioC_5_detection' => 'P',
    'Spike_AFFX-r2-Ec-bioC_5_signal'    => '441.14432',
    'Spike_AFFX-r2-Ec-bioC_avg-signal'  => '511.03851',
    'Spike_AFFX-r2-Ec-bioD_3-5-ratio'   => '1.29075',
    'Spike_AFFX-r2-Ec-bioD_3_detection' => 'P',
    'Spike_AFFX-r2-Ec-bioD_3_signal'    => '4272.42627',
    'Spike_AFFX-r2-Ec-bioD_5_detection' => 'P',
    'Spike_AFFX-r2-Ec-bioD_5_signal'    => '3310.04663',
    'Spike_AFFX-r2-Ec-bioD_avg-signal'  => '3791.23633',
    'Spike_AFFX-r2-P1-cre_3-5-ratio'    => '1.06699',
    'Spike_AFFX-r2-P1-cre_3_detection'  => 'P',
    'Spike_AFFX-r2-P1-cre_3_signal'     => '7970.32422',
    'Spike_AFFX-r2-P1-cre_5_detection'  => 'P',
    'Spike_AFFX-r2-P1-cre_5_signal'     => '7469.91846',
    'Spike_AFFX-r2-P1-cre_avg-signal'   => '7720.12109',
};

is_deeply( $chp->get_stats, $expected_stats, 'statistics' );

# QTD
my $expected_headings = [
    'Probe Set Name',
    'Detection',
    'Detection p-value',
    'Signal',
    'Number of Pairs',
    'Number of Pairs Used',
];

is_deeply( $chp->get_headings, $expected_headings, 'column headings' );

my $expected_QTs = [
    'Affymetrix:QuantitationType:Probe Set Name',
    'Affymetrix:QuantitationType:Detection',
    'Affymetrix:QuantitationType:Detection p-value',
    'Affymetrix:QuantitationType:Signal',
    'Affymetrix:QuantitationType:Number of Pairs',
    'Affymetrix:QuantitationType:Number of Pairs Used',
];

is_deeply( $chp->get_qtd, $expected_QTs, 'quantitation type identifiers' );

# DED
my $expected_features = [
    'Affymetrix:CompositeSequence:HG-U133A:AFFX-BioB-5_at',
    'Affymetrix:CompositeSequence:HG-U133A:AFFX-BioB-M_at',
    'Affymetrix:CompositeSequence:HG-U133A:AFFX-BioB-3_at',
    'Affymetrix:CompositeSequence:HG-U133A:AFFX-BioC-5_at',
    'Affymetrix:CompositeSequence:HG-U133A:AFFX-BioC-3_at',
    'Affymetrix:CompositeSequence:HG-U133A:AFFX-BioDn-5_at',
    'Affymetrix:CompositeSequence:HG-U133A:AFFX-BioDn-3_at',
    'Affymetrix:CompositeSequence:HG-U133A:AFFX-CreX-5_at',
    'Affymetrix:CompositeSequence:HG-U133A:AFFX-CreX-3_at',
    'Affymetrix:CompositeSequence:HG-U133A:AFFX-DapX-5_at',
];

is_deeply( $chp->get_ded(), $expected_features,
    'design element (compseq) identifiers' );
