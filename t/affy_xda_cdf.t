#!/usr/bin/env perl -wT
#
# $Id: affy_cel.t 1799 2007-10-31 17:39:45Z tfrayner $

use strict;
use warnings;

use Test::More tests => 4;

use File::Spec;
BEGIN {
    use_ok ('ArrayExpress::Datafile::Affymetrix');
    use_ok ('ArrayExpress::Datafile::Affymetrix::CDF::XDA_CDF');
}

# Very basic testing until such time as we get a good small test XDA
# CDF file.
my $cdf = ArrayExpress::Datafile::Affymetrix::CDF::XDA_CDF->new({
    input => 'not a file',
});

# Create the instance
ok( defined $cdf, 'new() returned a value' );
ok( $cdf->isa('ArrayExpress::Datafile::Affymetrix::CDF::XDA_CDF'),
    'of the correct class' );

