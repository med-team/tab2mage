#!/usr/bin/env perl -wT
#
# $Id: affy_chp.t 1988 2008-03-10 10:43:48Z tfrayner $

use strict;
use warnings;

use Test::More tests => 17;

use File::Spec;

BEGIN {
    use_ok('ArrayExpress::Datafile::Affymetrix');
    use_ok('ArrayExpress::Datafile::Affymetrix::CHP::CHPv8');
    use_ok('ArrayExpress::Datafile::Affymetrix::CHP::CHPv12');
    use_ok('ArrayExpress::Datafile::Affymetrix::CHP::CHPv13');
}

# Use the factory class to generate a parser, hopefully of the right
# type.
my $fac = ArrayExpress::Datafile::Affymetrix->new();

my $chp = $fac->make_parser( File::Spec->catfile( 't', 'data', 'test.CHP' ) );

# Create the instance
ok( defined $chp, 'new() returned a value' );
ok( $chp->isa('ArrayExpress::Datafile::Affymetrix::CHP::GDAC_CHP'),
    'of the correct class' );

# Parse the test file
$chp->parse();

# Check our return values
is( $chp->get_chip_type,    'YG_S98',         'chip type' );
is( $chp->get_algorithm,    'ExpressionStat', 'algorithm' );
is( $chp->get_version,      '12',             'version' );
is( $chp->get_num_cells,    '10',             'expected number of cells' );
is( $chp->get_num_rows,     '534',            'number of rows' );
is( $chp->get_num_columns,  '534',            'number of columns' );

# Parameters
my $expected_params = {
    'Alpha1'       => '0.04',
    'Alpha2'       => '0.06',
    'Tau'          => '0.015',
    'Gamma1H'      => '0.0025',
    'Gamma1L'      => '0.0025',
    'Gamma2H'      => '0.003',
    'Gamma2L'      => '0.003',
    'Perturbation' => '1.1',
    'TGT'          => '100',
    'NF'           => '1.000000',
    'SF'           => '0.822926',
    'SFGene'       => 'All',
};

is_deeply( $chp->get_parameters, $expected_params, 'parameters' );

# Statistics
my $expected_stats = {
    'Background Avg'   => '60.39',
    'Background Stdev' => '1.34',
    'Background Max'   => '65.5',
    'Background Min'   => '58.0',
    'Noise Avg'        => '1.42',
    'Noise Stdev'      => '0.05',
    'Noise Max'        => '1.6',
    'Noise Min'        => '1.3',
    'RawQ'             => '1.86',
};

is_deeply( $chp->get_stats, $expected_stats, 'statistics' );

# QTD
my $expected_headings = [
    qw(
        ProbeSetName
        CHPPairs
        CHPPairsUsed
        CHPSignal
        CHPDetection
        CHPDetectionPvalue
        )
];

is_deeply( $chp->get_headings, $expected_headings, 'column headings' );

my $expected_QTs = [
    qw(
        Affymetrix:QuantitationType:ProbeSetName
        Affymetrix:QuantitationType:CHPPairs
        Affymetrix:QuantitationType:CHPPairsUsed
        Affymetrix:QuantitationType:CHPSignal
        Affymetrix:QuantitationType:CHPDetection
        Affymetrix:QuantitationType:CHPDetectionPvalue
        )
];

is_deeply( $chp->get_qtd, $expected_QTs, 'quantitation type identifiers' );

# DED
my $expected_features = [
    'Affymetrix:CompositeSequence:YG_S98:AFFX-MurIL2_at',
    'Affymetrix:CompositeSequence:YG_S98:AFFX-MurIL10_at',
    'Affymetrix:CompositeSequence:YG_S98:AFFX-MurIL4_at',
    'Affymetrix:CompositeSequence:YG_S98:AFFX-MurFAS_at',
    'Affymetrix:CompositeSequence:YG_S98:AFFX-BioB-5_at',
    'Affymetrix:CompositeSequence:YG_S98:AFFX-BioB-M_at',
    'Affymetrix:CompositeSequence:YG_S98:AFFX-BioB-3_at',
    'Affymetrix:CompositeSequence:YG_S98:AFFX-BioC-5_at',
    'Affymetrix:CompositeSequence:YG_S98:AFFX-BioC-3_at',
    'Affymetrix:CompositeSequence:YG_S98:AFFX-BioDn-5_at',
];

my $cdf = $fac->make_parser( File::Spec->catfile( 't', 'data', 'dummy.CDF' ) );
$cdf->parse();
is_deeply( $chp->get_ded($cdf), $expected_features,
    'design element (compseq) identifiers' );
