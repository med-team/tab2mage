#!/usr/bin/env perl -wT
#
# $Id: mage.t 1242 2006-12-02 19:18:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 1;

SKIP: {

    eval { require Bio::MAGE };

    skip 'Bio::MAGE not installed', 1 if $@;

    require_ok( 'ArrayExpress::Curator::MAGE' );

}

