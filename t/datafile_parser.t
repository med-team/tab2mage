#!/usr/bin/env perl -wT
#
# $Id: datafile_parser.t 1269 2006-12-10 12:23:13Z tfrayner $

use strict;
use warnings;

use Test::More tests => 1;

BEGIN { use_ok( 'ArrayExpress::Datafile::Parser' ); }

