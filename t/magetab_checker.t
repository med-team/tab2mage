#!/usr/bin/env perl -wT
#
# $Id: validate.t 1242 2006-12-02 19:18:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 2;

require_ok( 'ArrayExpress::MAGETAB::Checker' );
require_ok( 'ArrayExpress::MAGETAB::Checker::IDF' );
