#!/usr/bin/env perl -wT
#
# $Id: web_form.t 1242 2006-12-02 19:18:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 1;

SKIP: {

    eval {
	require CGI::Application;
	require CGI::Application::Plugin::Authentication;
	require CGI::Application::Plugin::ValidateRM;
	require CGI::Upload;
    };

    skip 'Some module(s) not installed: CGI::Application, CGI::Upload, CAP::Authentication or CAP::ValidateRM',
	1 if $@;

    require_ok( 'ArrayExpress::AutoSubmission::WebForm' );

}

