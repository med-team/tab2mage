#!/usr/bin/env perl -wT
#
# $Id: daemon.t 1242 2006-12-02 19:18:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 1;

SKIP: {

    eval {
	require ArrayExpress::AutoSubmission::Daemon;
	require DBD::mysql;
    };

    skip 'DBD::mysql not installed, or ArrayExpress::AutoSubmission::Daemon superclass unavailable.',
	1 if $@;

    require_ok( 'ArrayExpress::AutoSubmission::Daemon::MXExporter' );

}

