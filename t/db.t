#!/usr/bin/env perl -wT
#
# $Id: db.t 1960 2008-02-21 12:03:19Z tfrayner $

use strict;
use warnings;

use Test::More tests => 37;

SKIP: {

    eval {
	require Class::DBI;
    };

    skip 'Class::DBI not installed',
	37 if $@;

    require_ok( 'ArrayExpress::AutoSubmission::DB' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Accessionable' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::ArrayDesign' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::ArrayDesignExperiment' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::ArrayDesignOrganism' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Category' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::CategoryDesign' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::CategoryMaterial' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::CategoryTaxon' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::DataFile' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Design' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::DesignInstance' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Event' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Experiment' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::ExperimentFactor' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::ExperimentQT' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Factor' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::File' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Material' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::MaterialInstance' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Organism' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::OrganismInstance' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Permission' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::PermissionRole' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Protocol' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::QuantitationType' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Role' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::RoleUser' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Spreadsheet' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Taxon' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::User' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::LoadedData' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::ExperimentLoadedData' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::DataFormat' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::QualityMetric' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::QualityMetricInstance' );
    require_ok( 'ArrayExpress::AutoSubmission::DB::Platform' );

}

