#!/usr/bin/env perl -wT
#
# $Id: db.t 1242 2006-12-02 19:18:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 4;

SKIP: {

    eval {
	require Date::Manip;
    };

    skip 'Date::Manip not installed',
	1 if $@;

    require_ok( 'ArrayExpress::Tracking::QueryHandler' );

}

require_ok( 'ArrayExpress::Tracking::Event' );
require_ok( 'ArrayExpress::Tracking::CelQC' );

SKIP: {

    eval {
	require Class::DBI;
    };
    skip 'Class::DBI not installed',
	1 if $@;

    require_ok( 'ArrayExpress::Tracking::QCJobManager' );

}
