#!/usr/bin/env perl -wT
#
# $Id: datafile.t 2069 2008-06-04 14:33:52Z tfrayner $

use strict;
use warnings;

# Very simple mock object for testing mage_qtd mutator.

use Test::More tests => 131;
use File::Spec;
use Cwd;
use ArrayExpress::Datafile::QT_list qw(get_QTs);
use ArrayExpress::Curator::Config qw($CONFIG);

BEGIN { use_ok( 'ArrayExpress::Datafile' ); }

my $filename = File::Spec->catfile('t', 'data', 'test.txt');    # right
my $filepath = '/my/path/to/test.txt'; # wrong
my $test_columns = [qw(x y z)];
my $binfile  = File::Spec->catfile('t', 'data', 'test_xda.CEL');
my $expfile  = File::Spec->catfile('t', 'data', 'test.EXP');
my $gprfile  = File::Spec->catfile('t', 'data', 'test.gpr');
my $fgemfile = File::Spec->catfile('t', 'data', 'test_fgem.txt');
my $QTs = get_QTs();

#######################################################################################
{
    # Create the instance.
    my $file = ArrayExpress::Datafile->new({is_dummy => 1});
    ok( defined $file, 'new() returned a value' );
    ok( $file->isa('ArrayExpress::Datafile'),
	'of the correct class' );

    # File name setting.
    $file->set_name($filename);
    is( $file->get_name, $filename, 'name');
    is( $file->get_path, File::Spec->catfile(getcwd, $filename), 'name-set path');
    is( $file->get_is_binary, 0, 'text file detected');

    # Incrementable integer attribute checks.
    foreach my $attr qw(row_count parse_errors not_null) {
	my $getter = "get_$attr";
	is($file->$getter(), 0, "$attr initial value");
	
	my $setter = "set_$attr";
	$file->$setter(10);
	is($file->$getter(), 10, "$attr set value");
	
	my $adder = "increment_$attr";
	$file->$adder();
	is($file->$getter, 11, "$attr incremented value");
	
	$file->$adder(5);
	is($file->$getter(), 16, "$attr multiply incremented value");
    }

    # Simple boolean attributes.
    foreach my $attr qw(is_exp is_miamexpress) {
	my $getter = "get_$attr";
	is($file->$getter(), 0, "$attr initial value");
	
	my $setter = "set_$attr";
	$file->$setter(1);
	is($file->$getter(), 1, "$attr set value 1");
	$file->$setter(0);
	is($file->$getter(), 0, "$attr set value 0");
    }

    # Simple string attributes (empty string init).
    foreach my $attr qw(hyb_identifier
			hyb_sysuid
			array_design_id
			ded_identifier
			test_data_line
			qt_type) {
	my $getter = "get_$attr";
	is($file->$getter(), q{}, "$attr initial value");

	my $setter = "set_$attr";
	$file->$setter('test');
	is($file->$getter(), 'test', "$attr set value");
	$file->$setter(q{});
	is($file->$getter(), q{}, "$attr set value empty");
    }

    # Simple hashref attributes.
    my %testhash = (one => 1, two => 'two', three => 'III');
    foreach my $attr qw(data_metrics) {
	my $getter = "get_$attr";
	is_deeply($file->$getter(), {}, "$attr initial value");

	my $setter = "set_$attr";
	$file->$setter(\%testhash);
	is_deeply($file->$getter(), \%testhash, "$attr set value");
    }

    # Simple arrayref attributes.
    my @testarray = qw(one two three);
    foreach my $attr qw(index_columns
			column_headings
			heading_qts
			heading_hybs) {
	my $getter = "get_$attr";
	is_deeply($file->$getter(), [], "$attr initial value");
	
	my $setter = "set_$attr";
	$file->$setter(\@testarray);
	is_deeply($file->$getter(), \@testarray, "$attr set value");
    }

    # Accumulative hashref attributes
    my @testarray2 = qw(four five);
    foreach my $attr qw(fail_columns
			fail_hybs) {
	my $getter = "get_$attr";
	is_deeply($file->$getter(), [], "$attr initial value");

	my $adder = "add_$attr";
	$file->$adder(@testarray);
	is_deeply(sort($file->$getter()), [sort @testarray], "$attr add values");
	$file->$adder(@testarray);
	is_deeply(sort($file->$getter()), [sort @testarray], "$attr duplicate values");
	$file->$adder(@testarray2);
	is_deeply(sort($file->$getter()), [sort @testarray, @testarray2], "$attr add more values");
    }

    # FactorValues.
    $file->add_factor_value('category1', 'value1');
    is_deeply($file->get_factor_value(), {'category1' => ['value1']}, 'add factor category and value');
    $file->add_factor_value('category1', 'value1');
    is_deeply($file->get_factor_value(), {'category1' => ['value1']}, 'add duplicate factor value');
    $file->add_factor_value('category1', 'value2');
    is_deeply($file->get_factor_value(), {'category1' => [qw(value1 value2)]}, 'add second factor value');
    $file->add_factor_value('category2', 'value2');
    is_deeply($file->get_factor_value(), {'category1' => [qw(value1 value2)],
					  'category2' => ['value2']}, 'add second factor category and value');


    # Enumerated types; some of these tests are designed to die.
    SKIP: {

	eval {
	    require Test::Exception;
	};

	skip 'Test::Exception not installed',
	    12 if $@;

	use_ok( 'Test::Exception' );

	# ded_type
	dies_ok( sub { $file->set_ded_type('notarealdedtype') }, 'exception on bad ded_type');
	lives_ok( sub { $file->set_ded_type('Reporter') }, 'valid ded_type');
	is($file->get_ded_type(), 'Reporter', 'ded_type correctly set');

	# format_type
	dies_ok( sub { $file->set_format_type('notarealformattype') }, 'exception on bad format_type');
	lives_ok( sub { $file->set_format_type('GenePix') }, 'valid format_type');
	is($file->get_format_type(), 'GenePix', 'format_type correctly set');

	# data_type
	dies_ok( sub { $file->set_data_type('notarealdatatype') }, 'exception on bad data_type');
	lives_ok( sub { $file->set_data_type('raw') }, 'valid data_type');
	is($file->get_data_type(), 'raw', 'data_type correctly set');

	# Test mage_qtd (using mock Bio::MAGE object).
	SKIP: {
	    eval {
		require Bio::MAGE::BioAssayData::QuantitationTypeDimension;
	    };

	    skip 'Bio::MAGE::BioAssayData::QuantitationTypeDimension not installed',
		2 if $@;
	    
	    my $qtd = Bio::MAGE::BioAssayData::QuantitationTypeDimension->new();

	    lives_ok( sub {$file->set_mage_qtd($qtd) }, 'set mage_qtd');
	    is_deeply($file->get_mage_qtd(), $qtd, 'get mage_qtd');
	}
    }

    # File path setting.
    $file->set_path($filepath);
    is( $file->get_name, $filename, 'name');
    is( $file->get_path, $filepath, 'name-set path');
}
#######################################################################################
{
    # Simple string attributes (undef init).
    my $file2 = ArrayExpress::Datafile->new({is_dummy => 1});
    foreach my $attr qw(path
			target_filename) {
	my $getter = "get_$attr";
	is($file2->$getter(), undef, "$attr initial value");
					 
	my $setter = "set_$attr";
	$file2->$setter('test');
	is($file2->$getter(), 'test', "$attr set value");
	$file2->$setter(q{});
	is($file2->$getter(), q{}, "$attr set value empty");
    }
}
#######################################################################################
{
    # EXP file accessors.
    my $exp = ArrayExpress::Datafile->new({is_dummy => 1});
    my $expdata = {section => {param => 'value'}};
    $exp->set_exp_data($expdata);
    is_deeply($exp->get_exp_data(), $expdata,     'set exp_data');
    is($exp->get_is_exp(),          1,            'EXP flag set by set_exp_data');
    is($exp->get_format_type(),     'Affymetrix', 'format_type set by set_exp_data');
    is($exp->get_data_type(),       'EXP',        'data_type set by set_exp_data');
}
#######################################################################################
{
    # Create a more complex initial instance.
    my $nfile1 = ArrayExpress::Datafile->new({
	name            => $filename,
	hyb_identifier  => 'one',
	hyb_sysuid      => 'two',
	array_design_id => 'three',
	is_miamexpress  => 1,
	is_exp          => 1,
	target_filename => 'four',
	data_type       => 'raw',
	column_headings => $test_columns,
    });
    ok( defined $nfile1, 'new() returned a value' );
    ok( $nfile1->isa('ArrayExpress::Datafile'),
	'of the correct class' );

    # Test that the path is also set.
    is( $nfile1->get_name, $filename, 'constructor-set name');
    is( $nfile1->get_path, File::Spec->catfile(getcwd, $filename), 'name-constructor-set path');

    # Other init args settings.
    is( $nfile1->get_hyb_identifier(),  'one',     'init hyb_identifier');
    is( $nfile1->get_hyb_sysuid(),      'two',     'init hyb_sysuid');
    is( $nfile1->get_array_design_id(), 'three',   'init array_design_id');
    is( $nfile1->get_is_miamexpress(),  1,         'init is_miamexpress');
    is( $nfile1->get_is_exp(),          1,         'init is_exp');
    is( $nfile1->get_target_filename(), 'four',    'init target_filename');
    is( $nfile1->get_data_type(),       'raw',     'init data_type');
    is_deeply( $nfile1->get_column_headings(),  $test_columns, 'init column_headings');
}
#######################################################################################
{
    # Check precedence of path initialization
    my $nfile2 = ArrayExpress::Datafile->new({
	name     => $filename,
	path     => $filepath,
	is_dummy => 1,
    });
    is( $nfile2->get_name, $filename, 'constructor-set name');
    is( $nfile2->get_path, $filepath, 'constructor-set path');

    # Check binary file detection
    my $file3 = ArrayExpress::Datafile->new({
	name     => $binfile,
	is_dummy => 1,
    });
    is( $file3->get_is_binary(), 1, 'binary file detected');
}
#######################################################################################
{
    my $exp = ArrayExpress::Datafile->new({
	name     => $expfile,
	is_dummy => 1,
    });
    $exp->parse_exp_file();
    is( $exp->get_is_exp(), 1, 'real EXP file recognized');
    my $expected = {
	'Sample Info' => {
	    'Chip Lot'  => 12345678,
	    'Operator'  => 'Gandalf',
	    'Project'   => 'Test',
	    'Chip Type' => 'HG-U133A',
	},
	'Scanner' => {
	    'Number of Scans'  => 2,
	    'Scan Date'        => 'Sep 20 2003 01:25PM',
	    'Filter'           => 570,
	    'Scanner ID'       => '87654321',
	    'Pixel Size'       => 3,
	    'Scanner Type'     => 'HP',
	    'Scan Temperature' => 30,
	},
	'Fluidics' => {
	    'Wash A1 Recovery Mixes'      => 0,
	    'Wash A1 Temperature (C)'     => 25,
	    'Number of Wash A1 Cycles'    => 10,
	    'Mixes per Wash A1 Cycle'     => 2,
	    'Wash B Recovery Mixes'       => 0,
	    'Wash B Temperature (C)'      => 50,
	    'Number of Wash B Cycles'     => 4,
	    'Mixes per Wash B Cycle'      => 15,
	    'Stain Temperature (C)'       => 25,
	    'First Stain Time (seconds)'  => 600,
	    'Wash A2 Recovery Mixes'      => 0,
	    'Wash A2 Temperature (C)'     => 25,
	    'Number of Wash A2 Cycles'    => 10,
	    'Mixes per Wash A2 Cycle'     => 4,
	    'Second Stain Time (seconds)' => 600,
	    'Third Stain Time (seconds)'  => 600,
	    'Wash A3 Recovery Mixes'      => 0,
	    'Wash A3 Temperature (C)'     => 30,
	    'Number of Wash A3 Cycles'    => 15,
	    'Mixes per Wash A3 Cycle'     => 4,
	    'Holding Temperature (C)'     => 25,
	    'Hybridize Date'              => 'Sep 20 2003 01:10PM',
	    'Module'                      => 1,
	    'Station'                     => 2,
	    'Protocol'                    => 'EukGE-WS2v4',
	},
	'header' => {
	    'Version' => 1,
	},
    };
    is_deeply( $exp->get_exp_data, $expected, 'real EXP file fully parsed');
}
#######################################################################################
my $start = [
    'Block',
    'Column',
    'Row',
    'Name',
    'ID',
    'X',
    'Y',
];
my $ignored = [
    'Name',
    'ID',
];
my $end = [
    'MetaColumn',
    'MetaRow',
    'Column',
    'Row',
];
my $expected = [
    'Dia.',
    'F635 Median',
    'F635 Mean',
    'F635 SD',
    'B635 Median',
    'B635 Mean',
    'B635 SD',
    '% > B635+1SD',
    '% > B635+2SD',
    'F635 % Sat.',
    'F532 Median',
    'F532 Mean',
    'F532 SD',
    'B532notQTMedian',
    'B532 Mean',
    'B532 SD',
    '% > B532+1SD',
    '% > B532+2SD',
    'F532 % Sat.',
    'Ratio of Medians',
    'Ratio of Means',
    'Median of Ratios',
    'Mean of Ratios',
    'Ratios SD',
    'Rgn Ratio',
    'Rgn R�',
    'F Pixels',
    'B Pixels',
    'Sum of Medians',
    'Sum of Means',
    'Log Ratio',
    'F635 Median - B635',
    'F532 Median - B532',
    'F635 Mean - B635',
    'F532 Mean - B532',
    'flags',
];
#######################################################################################
{
    my $gpr = ArrayExpress::Datafile->new({
	name     => $gprfile,
	is_dummy => 1,
    });
    is($gpr->parse_header(),               'GenePix',             'parse_header return code');
    is($gpr->get_format_type(),            'GenePix',             'real file get_format_type');
    is_deeply($gpr->get_index_columns(),   [0,1,2,5,6],           'real file get_index_columns');
    is_deeply($gpr->get_column_headings(), [@$start, @$expected], 'real file get_column_headings');

    is($gpr->check_column_headings($QTs), q{},                         'check_column_headings return code');
    is($gpr->get_qt_type(),               'GenePix[Axon Instruments]', 'real file get_qt_type');
    is_deeply($gpr->get_heading_qts(),    [@$ignored, @$expected],       'real file get_heading_qts');

    # These are alphabetically sorted.
    is_deeply($gpr->get_fail_columns(), ['B532notQTMedian', 'flags'], 'real file get_fail_columns');
}
#######################################################################################
{
    my $gpr = ArrayExpress::Datafile->new({
	name     => $gprfile,
	is_dummy => 1,
    });
    $gpr->parse_datafile($QTs, {}, {}, 0);
    is($gpr->get_line_format(),  'Unix',  'real file get_line_format');
    is($gpr->get_row_count(),    10,      'real file row count');
    is($gpr->get_parse_errors(), 0,       'real file parse errors');
    is($gpr->get_not_null(),     320,     'real file not null');
    is($gpr->percent_null(),     5.88235, 'real file percent null');

    # This is the result of hashing a number of lines together using Digest::MD5::md5_hex.
    my $expecteddata = 'fe3055946e452021b7b2d83ac5569eaea6c1d2f25c721684a0f7c13861a997b53cb62811f2c166212ddc4ec18fa76d5ef8d0261f69960b7fe7ef41025a3ed9667d3a17bc59819151312b7c8b4a6460f8';
    is($gpr->get_test_data_line(),         $expecteddata,       'real file test_data_line');
    is_deeply($gpr->get_column_headings(), [@$end, @$ignored, @$expected], 'parsed file get_column_headings');
    is_deeply($gpr->get_heading_qts(),     [@$ignored, @$expected], 'parsed file get_heading_qts');
}
#######################################################################################
{
    my $fgem = ArrayExpress::Datafile->new({
	name            => $fgemfile,
	data_type       => 'transformed',
	array_design_id => 'UNKNOWN',
    });
    $fgem->parse_datafile($QTs, {Sigma1278b_1 => 'X', W303a_1 => 'X', W303a_2 => 'X'}, {}, 0);

    is_deeply($fgem->get_heading_qts(),  [qw(RMA RMA)],                         'fgem heading qts');
    is_deeply($fgem->get_heading_hybs(), [['Sigma1278b_1', 'Sigma1278b_2'],
					  ['W303a_1',      'W303a_2']],         'fgem heading hybs');
    is_deeply($fgem->get_fail_columns(), [],                                    'fgem fail columns');
    is_deeply($fgem->get_fail_hybs(),    ['Sigma1278b_2'],                      'fgem fail hybs');
}
#######################################################################################
{

    my $file = ArrayExpress::Datafile->new({
	name            => $filename,
	data_type       => 'raw',
	array_design_id => 'UNKNOWN',
    });

    # First pass
    $file->parse_header_with_indices( { GenePix => [qw(Block Column Row X Y)] } );

    is( $file->get_format_type(), 'GenePix', 'parse_header recognises file type' );
    is_deeply(
	$file->get_column_headings(),
	[qw(Block Column Row X Y QT1 QT2 QT3 QT4)],
	'parse_header returns correct headings'
    );
    is_deeply( $file->get_index_columns(), [qw(0 1 2 3 4)],
	       'parse_header returns correct index columns' );

    # Second pass
    seek( $file->get_filehandle(), 0, 0 ) or die($!);
    $file->set_data_type($CONFIG->get_FGEM_FILE_TYPE());
    $file->parse_header_with_indices( { FGEM => ['Reporter Identifier'] } );

    is( $file->get_format_type(), 'FGEM', 'parse_header recognises file type' );
    is_deeply(
	$file->get_column_headings(),
	[ 'Reporter Identifier', qw( QT1(Hyb1) QT2(Hyb1) QT1(Hyb2) QT2(Hyb2) ) ],
	'parse_header returns correct headings'
    );
    is_deeply( $file->get_index_columns(), [0], 'parse_header returns correct index columns' );
}

# TODO: fix_known_text_format (all types), strip_and_sort
