#!/usr/bin/env perl -wT
#
# $Id: affy_xda_cel.t 1811 2007-11-02 10:34:42Z tfrayner $

use strict;
use warnings;

use Test::More tests => 17;

use File::Spec;
BEGIN {
    use_ok('ArrayExpress::Datafile::Affymetrix');
    use_ok('ArrayExpress::Datafile::Affymetrix::CEL::CELv4');
}

my $fac = ArrayExpress::Datafile::Affymetrix->new();

my $cel = $fac->make_parser( File::Spec->catfile( 't', 'data', 'test_xda.CEL' ) );

# Create the instance
ok( defined $cel, 'new() returned a value' );
ok( $cel->isa('ArrayExpress::Datafile::Affymetrix::CEL::CELv4'),
    'of the correct class' );

# Parse a test file
$cel->parse();

# Check our return values
is( $cel->get_chip_type,   'HG-U133A',   'chip type' );
is( $cel->get_algorithm,   'Percentile', 'algorithm' );
is( $cel->get_version,     '4',          'version' );
is( $cel->get_num_cells,   '10',         'expected number of cells' );
is( $cel->get_num_rows,    '1',          'number of rows' );
is( $cel->get_num_columns, '10',         'number of columns' );
is( $cel->get_num_masked,   '0', 'number of masked cells' );
is( $cel->get_num_outliers, '3', 'number of outlier cells' );

# Parameters
my $expected_params = {
    'Percentile'  => '75',
    'CellMargin'  => '2',
    'OutlierHigh' => '1.500',
    'OutlierLow'  => '1.004',
};

is_deeply( $cel->get_parameters, $expected_params, 'parameters' );

# Statistics
my $expected_stats = {
    'Number of Cells'      => '10',
    'Rows'                 => '1',
    'Columns'              => '10',
    'Number Cells Masked'  => '0',
    'Number Outlier Cells' => '3',
};

is_deeply( $cel->get_stats, $expected_stats, 'statistics' );

# QTD
my $expected_headings = [
    qw(
        CELX
        CELY
        CELIntensity
        CELIntensityStdev
        CELPixels
        CELOutlier
        CELMask
        )
];

is_deeply( $cel->get_headings, $expected_headings, 'column headings' );

my $expected_QTs = [
    qw(
        Affymetrix:QuantitationType:CELX
        Affymetrix:QuantitationType:CELY
        Affymetrix:QuantitationType:CELIntensity
        Affymetrix:QuantitationType:CELIntensityStdev
        Affymetrix:QuantitationType:CELPixels
        Affymetrix:QuantitationType:CELOutlier
        Affymetrix:QuantitationType:CELMask
        )
];

is_deeply( $cel->get_qtd, $expected_QTs, 'quantitation type identifiers' );

# DED
my $expected_features = [
    'Affymetrix:Feature:HG-U133A:Probe(0,0)',
    'Affymetrix:Feature:HG-U133A:Probe(1,0)',
    'Affymetrix:Feature:HG-U133A:Probe(2,0)',
    'Affymetrix:Feature:HG-U133A:Probe(3,0)',
    'Affymetrix:Feature:HG-U133A:Probe(4,0)',
    'Affymetrix:Feature:HG-U133A:Probe(5,0)',
    'Affymetrix:Feature:HG-U133A:Probe(6,0)',
    'Affymetrix:Feature:HG-U133A:Probe(7,0)',
    'Affymetrix:Feature:HG-U133A:Probe(8,0)',
    'Affymetrix:Feature:HG-U133A:Probe(9,0)',
];

is_deeply( $cel->get_ded, $expected_features,
    'design element (feature) identifiers' );
