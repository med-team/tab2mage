#!/usr/bin/env perl -wT
#
# $Id: common.t 1242 2006-12-02 19:18:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 33;

BEGIN {
    require_ok(
        'ArrayExpress::Datafile::DataMatrix'
    );
}

my $dm = ArrayExpress::Datafile::DataMatrix->new();
ok( defined $dm, 'new() returned a value' );
ok( $dm->isa('ArrayExpress::Datafile::DataMatrix'), 'of the correct class' );

#########################
# get_dimension_lists() #
#########################
my @fgem_dimension_data = (
    {

        # DBQ/DQB (defaults to DBQ)
        query => [
            [ ['One'], ['Two'] ],    # Hybs
            [qw(A A)],               # QTs
        ],
        result => [
            [ ['One'], ['Two'] ],    # Hybs
            [qw(A)],                 # QTs
            'DBQ',                   # Order
        ]
    },
    {

        # DBQ
        query => [ [ ['One'], ['One'], ['Two'], ['Two'] ], [qw(A B A B)], ],
        result => [ [ ['One'], ['Two'] ], [qw(A B)], 'DBQ', ]
    },
    {

        # DBQ
        query => [
            [ ['One'], ['One'], ['One'], ['Two'], ['Two'], ['Two'] ],
            [qw(A B C A B C)],
        ],
        result => [ [ ['One'], ['Two'] ], [qw(A B C)], 'DBQ', ]
    },
    {

        # DQB
        query => [ [ ['One'], ['Two'], ['One'], ['Two'] ], [qw(A A B B)], ],
        result => [ [ ['One'], ['Two'] ], [qw(A B)], 'DQB', ]
    },
    {

        # DBQ, more complex
        query => [
            [
                [qw(One Two)], [qw(One Two)], [qw(Three Four)], [qw(Three Four)]
            ],
            [qw(A B A B)],
        ],
        result => [ [ [qw(One Two)], [qw(Three Four)] ], [qw(A B)], 'DBQ', ]
    },
    {

        # Broken, no order detectable
        query => [ [ ['One'], ['Three'], ['One'], ['Two'] ], [qw(A D B C)], ],
        result => [ [], [], undef, ]
    },
    {

        # Broken, order at first okay, then breaks.
        query => [ [ ['One'], ['Two'], ['One'], ['Two'] ], [qw(A A B C)], ],
        result => [ [], [], undef, ]
    },
    {

        # Broken, order at first okay, then breaks.
        query => [ [ ['One'], ['Two'], ['Onxe'], ['Three'] ], [qw(A A B B)], ],
        result => [ [], [], undef, ]
    },
    {

        # Broken, order at first okay, then breaks.
        query => [ [ ['One'], ['One'], ['Two'], ['Three'] ], [qw(A B A B)], ],
        result => [ [], [], undef, ]
    },
    {

        # Broken, order at first okay, then breaks.
        query => [ [ ['One'], ['One'], ['Two'], ['Two'] ], [qw(A B B A)], ],
        result => [ [], [], undef, ]
    },
);

foreach my $test (@fgem_dimension_data) {
    my ( $bas, $qts, $order ) = $dm->get_dimension_lists( @{ $test->{query} } );
    is_deeply( $bas,   $test->{result}[0], 'get_dimension_lists bioassays' );
    is_deeply( $qts,   $test->{result}[1], 'get_dimension_lists qts' );
    is_deeply( $order, $test->{result}[2], 'get_dimension_lists order' );
}

