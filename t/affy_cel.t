#!/usr/bin/env perl -wT
#
# $Id: affy_cel.t 1811 2007-11-02 10:34:42Z tfrayner $

use strict;
use warnings;

use Test::More tests => 18;

use File::Spec;

BEGIN {
    use_ok ('ArrayExpress::Datafile::Affymetrix');
    use_ok ('ArrayExpress::Datafile::Affymetrix::CEL::CELv3');
}

# Use the factory class to generate a parser, hopefully of the right
# type.
my $fac = ArrayExpress::Datafile::Affymetrix->new();

my $cel = $fac->make_parser( File::Spec->catfile( 't', 'data', 'test.CEL' ) );

# Create the instance
ok( defined $cel, 'new() returned a value' );
ok( $cel->isa('ArrayExpress::Datafile::Affymetrix::CEL::CELv3'),
    'of the correct class' );

# Parse the test file
$cel->parse();

# Check our return values
is( $cel->get_chip_type,   'Test',       'chip type' );
is( $cel->get_algorithm,   'Percentile', 'algorithm' );
is( $cel->get_version,     '3',          'version' );
is( $cel->get_num_cells,   '10',         'expected number of cells' );
is( $cel->get_num_rows,    '1',          'number of rows' );
is( $cel->get_num_columns, '10',         'number of columns' );
is( $cel->get_num_masked,   '0', 'number of masked cells' );
is( $cel->get_num_outliers, '3', 'number of outlier cells' );
is( $cel->get_num_modified, '0', 'number of modified cells' );

# Parameters
my $expected_params = {
    'Percentile'  => '75',
    'CellMargin'  => '2',
    'OutlierHigh' => '1.500',
    'OutlierLow'  => '1.004',
};

is_deeply( $cel->get_parameters, $expected_params, 'parameters' );

# Statistics
my $expected_stats = {
    'Number of Cells'       => '10',
    'Rows'                  => '1',
    'Columns'               => '10',
    'Number Cells Masked'   => '0',
    'Number Outlier Cells'  => '3',
    'Number Cells Modified' => '0',
};

is_deeply( $cel->get_stats, $expected_stats, 'statistics' );

# QTD
my $expected_headings = [
    qw(
        CELX
        CELY
        CELIntensity
        CELIntensityStdev
        CELPixels
        CELOutlier
        CELMask
        )
];

is_deeply( $cel->get_headings, $expected_headings, 'column headings' );

my $expected_QTs = [
    qw(
        Affymetrix:QuantitationType:CELX
        Affymetrix:QuantitationType:CELY
        Affymetrix:QuantitationType:CELIntensity
        Affymetrix:QuantitationType:CELIntensityStdev
        Affymetrix:QuantitationType:CELPixels
        Affymetrix:QuantitationType:CELOutlier
        Affymetrix:QuantitationType:CELMask
        )
];

is_deeply( $cel->get_qtd, $expected_QTs, 'quantitation type identifiers' );

# DED
my $expected_features = [
    'Affymetrix:Feature:Test:Probe(0,0)',
    'Affymetrix:Feature:Test:Probe(1,0)',
    'Affymetrix:Feature:Test:Probe(2,0)',
    'Affymetrix:Feature:Test:Probe(3,0)',
    'Affymetrix:Feature:Test:Probe(4,0)',
    'Affymetrix:Feature:Test:Probe(5,0)',
    'Affymetrix:Feature:Test:Probe(6,0)',
    'Affymetrix:Feature:Test:Probe(7,0)',
    'Affymetrix:Feature:Test:Probe(8,0)',
    'Affymetrix:Feature:Test:Probe(9,0)',
];

is_deeply( $cel->get_ded, $expected_features,
    'design element (feature) identifiers' );
