#!/usr/bin/env perl -wT
#
# $Id: magetab_tabfile.t 1462 2007-03-15 15:45:55Z tfrayner $

use strict;
use warnings;

use Test::More tests => 1;

require_ok( 'ArrayExpress::MAGETAB::TabFile' );
