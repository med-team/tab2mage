#!/usr/bin/env perl -wT

use strict;
use warnings;

use Test::More tests => 1;

SKIP: {

    eval {
	require Class::DBI;
    };

    skip 'Class::DBI not installed',
	1 if $@;

    require_ok( 'ArrayExpress::AutoSubmission::Creator' );

}

