#!/usr/bin/env perl -wT
#
# $Id: affy_xda_chp.t 1988 2008-03-10 10:43:48Z tfrayner $

use strict;
use warnings;

use Test::More tests => 15;

use File::Spec;

BEGIN {
    use_ok('ArrayExpress::Datafile::Affymetrix');
    use_ok('ArrayExpress::Datafile::Affymetrix::CHP::XDA_CHP');
}

# Use the factory class to generate a parser, hopefully of the right
# type.
my $fac = ArrayExpress::Datafile::Affymetrix->new();

my $chp = $fac->make_parser( File::Spec->catfile( 't', 'data', 'test_xda.CHP' ) );

# Create the instance
ok( defined $chp, 'new() returned a value' );
ok( $chp->isa('ArrayExpress::Datafile::Affymetrix::CHP::XDA_CHP'),
    'of the correct class' );

# Parse the test file
$chp->parse();

# Check our return values
is( $chp->get_chip_type,    'ATH1-121501',    'chip type' );
is( $chp->get_algorithm,    'ExpressionStat', 'algorithm' );
is( $chp->get_version,      '1',              'version' );
is( $chp->get_num_cells,    '10',             'expected number of cells' );
is( $chp->get_num_rows,     '712',            'number of rows' );
is( $chp->get_num_columns,  '712',            'number of columns' );

# Parameters
my $expected_params = {
    'VZ'             => '4',
    'Gamma1H'        => '0.0045',
    'Gamma2L'        => '0.006',
    'Epsilon'        => '0.5',
    'TGT'            => '200',
    'Perturbation'   => '1.1',
    'Alpha2'         => '0.065',
    'BG'             => '4',
    'Gamma2H'        => '0.006',
    'SF'             => '0.944273293018',
    'Alpha1'         => '0.05',
    'SFGene'         => 'All',
    'HZ'             => '4',
    'NF'             => '1.000000000000',
    'SmoothFactorBG' => '100',
    'Gamma1L'        => '0.0045',
    'Tau'            => '0.015',
};

is_deeply( $chp->get_parameters, $expected_params, 'parameters' );

# Statistics
my $expected_stats = {
    'Corner+ Count'    => '32',
    'Background Avg'   => '55.63',
    'Central- Avg'     => '13530',
    'Noise Max'        => '3.1',
    'Noise Stdev'      => '0.08',
    'Central- Count'   => '9',
    'RawQ'             => '2.32',
    'Corner+ Avg'      => '140',
    'Noise Min'        => '2.6',
    'Background Min'   => '54.1',
    'Background Stdev' => '0.67',
    'Corner- Avg'      => '15007',
    'Background Max'   => '58.3',
    'Noise Avg'        => '2.76',
    'Corner- Count'    => '32',
};

is_deeply( $chp->get_stats, $expected_stats, 'statistics' );

# QTD
my $expected_headings = [
    qw(
        ProbeSetName
        CHPPairs
        CHPPairsUsed
        CHPSignal
        CHPDetection
        CHPDetectionPvalue
        )
];

is_deeply( $chp->get_headings, $expected_headings, 'column headings' );

my $expected_QTs = [
    qw(
        Affymetrix:QuantitationType:ProbeSetName
        Affymetrix:QuantitationType:CHPPairs
        Affymetrix:QuantitationType:CHPPairsUsed
        Affymetrix:QuantitationType:CHPSignal
        Affymetrix:QuantitationType:CHPDetection
        Affymetrix:QuantitationType:CHPDetectionPvalue
        )
];

is_deeply( $chp->get_qtd, $expected_QTs, 'quantitation type identifiers' );

# DED
my $expected_features = [
    'Affymetrix:CompositeSequence:ATH1-121501:AFFX-MurIL2_at',
    'Affymetrix:CompositeSequence:ATH1-121501:AFFX-MurIL10_at',
    'Affymetrix:CompositeSequence:ATH1-121501:AFFX-MurIL4_at',
    'Affymetrix:CompositeSequence:ATH1-121501:AFFX-MurFAS_at',
    'Affymetrix:CompositeSequence:ATH1-121501:AFFX-BioB-5_at',
    'Affymetrix:CompositeSequence:ATH1-121501:AFFX-BioB-M_at',
    'Affymetrix:CompositeSequence:ATH1-121501:AFFX-BioB-3_at',
    'Affymetrix:CompositeSequence:ATH1-121501:AFFX-BioC-5_at',
    'Affymetrix:CompositeSequence:ATH1-121501:AFFX-BioC-3_at',
    'Affymetrix:CompositeSequence:ATH1-121501:AFFX-BioDn-5_at',
];

my $cdf = $fac->make_parser( File::Spec->catfile( 't', 'data', 'dummy.CDF' ) );
$cdf->parse();
is_deeply( $chp->get_ded($cdf), $expected_features,
    'design element (compseq) identifiers' );
