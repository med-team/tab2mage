#!/usr/bin/env perl -wT
#
# $Id: spreadsheet.t 1242 2006-12-02 19:18:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 1;

SKIP: {

    eval {
	require Class::DBI;
    };

    skip 'Class::DBI not installed',
	1 if $@;

    require_ok( 'ArrayExpress::AutoSubmission::Spreadsheet' );

}
