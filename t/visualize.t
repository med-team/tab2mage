#!/usr/bin/env perl -wT
#
# $Id: visualize.t 1242 2006-12-02 19:18:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 1;

BEGIN { use_ok( 'ArrayExpress::Curator::Visualize' ); }

