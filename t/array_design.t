#!/usr/bin/env perl -wT
#
# $Id: database.t 1242 2006-12-02 19:18:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 9;

BEGIN { use_ok( 'ArrayExpress::Datafile::ArrayDesign' ); }

my $array = ArrayExpress::Datafile::ArrayDesign->new();
ok( defined $array, 'new() returned a value' );
ok( $array->isa('ArrayExpress::Datafile::ArrayDesign'), 'of the correct class' );

# Simple hashref attributes.
my %testhash = (one => 1, two => 'two', three => 'III');
foreach my $attr qw(features
		    reporters
		    compseqs) {
    my $getter = "get_$attr";
    is_deeply($array->$getter(), {}, "$attr initial value");

    my $setter = "set_$attr";
    $array->$setter(\%testhash);
    is_deeply($array->$getter(), \%testhash, "$attr set value");
}

