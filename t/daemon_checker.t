#!/usr/bin/env perl -wT
#
# $Id: daemon.t 1242 2006-12-02 19:18:52Z tfrayner $

use strict;
use warnings;

use Test::More tests => 3;

SKIP: {

    eval {
	require ArrayExpress::AutoSubmission::Daemon;
    };

    skip 'Parent class ArrayExpress::AutoSubmission::Daemon not available.',
	3 if $@;

    require_ok( 'ArrayExpress::AutoSubmission::Daemon::Checker' );
    require_ok( 'ArrayExpress::AutoSubmission::Daemon::T2MChecker' );
    require_ok( 'ArrayExpress::AutoSubmission::Daemon::MAGETABChecker' );

}

