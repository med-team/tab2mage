#!/usr/bin/env perl -wT
#
# $Id: common.t 1273 2006-12-11 16:40:34Z tfrayner $

use strict;
use warnings;
use charnames qw(:full);

use File::Spec;
use Test::More tests => 22;

BEGIN {
    use_ok(
        'ArrayExpress::Curator::Common',
        qw(get_indexcol
          strip_discards
          round date_now
          check_linebreaks
          clean_hash
          )
    );
    use_ok(
        'ArrayExpress::Curator::Config',
        qw($CONFIG)
    );
}

my @test_array = qw(One Two Three Four);

##################
# get_indexcol() #
##################
my @get_indexcol_data = (
    { query => 'Two',   result => 1 },
    { query => 'Three', result => 2 },
    { query => 'Too',   result => -1 }
);

foreach my $test (@get_indexcol_data) {
    is( get_indexcol( \@test_array, $test->{query} ),
        $test->{result}, 'get_indexcol() matches' );
}

####################
# strip_discards() #
####################
my @strip_discards_data = (
    { query => [0], result => [qw(Two Three Four)] },
    { query => [ 0, 1 ], result => [qw(Three Four)] },
    { query => [ 1, 2 ], result => [qw(One Four)] },
    { query => [ 1, 3 ], result => [qw(One Three)] }
);

foreach my $test (@strip_discards_data) {
    is_deeply( strip_discards( $test->{query}, \@test_array ),
        $test->{result}, 'strip_discards() matches' );
}

###########
# round() #
###########
my @round_data = (
    { query => [ 1,           1 ],  result => 1 },
    { query => [ 1.0,         1 ],  result => 1 },
    { query => [ 1.123,       0 ],  result => 1 },
    { query => [ 1.125,       2 ],  result => 1.13 },
    { query => [ 1.123456789, 5 ],  result => 1.12346 },
    { query => [ 1.123,       -1 ], result => 0 },
    { query => [ 1234.123,    -2 ], result => 1200 },
);

foreach my $test (@round_data) {
    is( round( @{ $test->{query} } ), $test->{result}, 'round()' );
}

##############
# date_now() #
##############
# Just check the basic format returned.
ok( date_now() =~ m{\A \d{4}-\d{2}-\d{2} T \d{2}:\d{2}:\d{2} Z \z}xms,
    'date_now() returns correct format' );

######################
# check_linebreaks() #
######################

# DOS
my ( $counts, $line_ending ) =
  check_linebreaks( File::Spec->catfile( 't', 'data', 'test.CEL' ) );
is(
    $line_ending,
    "\N{CARRIAGE RETURN}\N{LINE FEED}",
    'check_linebreaks() recognises DOS file'
);
is_deeply(
    $counts,
    { dos => 49, unix => 0, mac => 0 },
    'check_linebreaks() returns correct line counts'
);

# Unix
( $counts, $line_ending ) =
  check_linebreaks( File::Spec->catfile( 't', 'data', 'test.txt' ) );
is( $line_ending, "\N{LINE FEED}", 'check_linebreaks() recognises Unix file' );
is_deeply(
    $counts,
    { dos => 0, unix => 8, mac => 0 },
    'check_linebreaks() returns correct line counts'
);

# TODO: Mac

################
# clean_hash() #
################
is_deeply(
    clean_hash( { one => 1, two => '', three => undef } ),
    { one => 1 },
    'clean_hash()'
);

# TODO: all the regexp patterns.

