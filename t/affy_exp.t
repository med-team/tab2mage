#!/usr/bin/env perl -wT
#
# $Id: affy_exp.t 1811 2007-11-02 10:34:42Z tfrayner $

use strict;
use warnings;

use Test::More tests => 21;

use File::Spec;

BEGIN {
    use_ok ('ArrayExpress::Datafile::Affymetrix');
    use_ok ('ArrayExpress::Datafile::Affymetrix::EXP');
}

# Use the factory class to generate a parser, hopefully of the right
# type.
my $fac = ArrayExpress::Datafile::Affymetrix->new();

my $exp = $fac->make_parser( File::Spec->catfile( 't', 'data', 'test.EXP' ) );

# Create the instance
ok( defined $exp, 'new() returned a value' );
ok( $exp->isa('ArrayExpress::Datafile::Affymetrix::EXP'),
    'of the correct class' );

# Parse a test file
$exp->parse();

# Check our return values
is( $exp->get_chip_type,    'HG-U133A',            'chip type' );
is( $exp->get_chip_lot,     '12345678',            'chip lot' );
is( $exp->get_version,      '1',                   'version' );
is( $exp->get_operator,     'Gandalf',             'operator' );
is( $exp->get_protocol,     'EukGE-WS2v4',         'protocol' );
is( $exp->get_station,      '2',                   'station' );
is( $exp->get_module,       '1',                   'module' );
is( $exp->get_hyb_date,     'Sep 20 2003 01:10PM', 'hyb_date' );
is( $exp->get_pixel_size,   '3',                   'pixel_size' );
is( $exp->get_filter,       '570',                 'filter' );
is( $exp->get_scan_temp,    '30',                  'scan_temp' );
is( $exp->get_scan_date,    'Sep 20 2003 01:25PM', 'scan_date' );
is( $exp->get_scanner_id,   '87654321',            'scanner_id' );
is( $exp->get_num_scans,    '2',                   'num_scans' );
is( $exp->get_scanner_type, 'HP',                  'scanner_type' );

# Parameters, ordered and named
my $expected_hyb_params = [
    { 'Wash A1 Recovery Mixes'      => '0' },
    { 'Wash A1 Temperature (C)'     => '25' },
    { 'Number of Wash A1 Cycles'    => '10' },
    { 'Mixes per Wash A1 Cycle'     => '2' },
    { 'Wash B Recovery Mixes'       => '0' },
    { 'Wash B Temperature (C)'      => '50' },
    { 'Number of Wash B Cycles'     => '4' },
    { 'Mixes per Wash B Cycle'      => '15' },
    { 'Stain Temperature (C)'       => '25' },
    { 'First Stain Time (seconds)'  => '600' },
    { 'Wash A2 Recovery Mixes'      => '0' },
    { 'Wash A2 Temperature (C)'     => '25' },
    { 'Number of Wash A2 Cycles'    => '10' },
    { 'Mixes per Wash A2 Cycle'     => '4' },
    { 'Second Stain Time (seconds)' => '600' },
    { 'Third Stain Time (seconds)'  => '600' },
    { 'Wash A3 Recovery Mixes'      => '0' },
    { 'Wash A3 Temperature (C)'     => '30' },
    { 'Number of Wash A3 Cycles'    => '15' },
    { 'Mixes per Wash A3 Cycle'     => '4' },
    { 'Holding Temperature (C)'     => '25' },
];

is_deeply( $exp->get_hyb_parameters, $expected_hyb_params,
    'named hybridization parameters' );

# Parameters, named as in MAGE-ML
my $expected_params = {
    'HybridizationStep0-EukGE-WS2v4'  => '0',
    'HybridizationStep1-EukGE-WS2v4'  => '25',
    'HybridizationStep2-EukGE-WS2v4'  => '10',
    'HybridizationStep3-EukGE-WS2v4'  => '2',
    'HybridizationStep4-EukGE-WS2v4'  => '0',
    'HybridizationStep5-EukGE-WS2v4'  => '50',
    'HybridizationStep6-EukGE-WS2v4'  => '4',
    'HybridizationStep7-EukGE-WS2v4'  => '15',
    'HybridizationStep8-EukGE-WS2v4'  => '25',
    'HybridizationStep9-EukGE-WS2v4'  => '600',
    'HybridizationStep10-EukGE-WS2v4' => '0',
    'HybridizationStep11-EukGE-WS2v4' => '25',
    'HybridizationStep12-EukGE-WS2v4' => '10',
    'HybridizationStep13-EukGE-WS2v4' => '4',
    'HybridizationStep14-EukGE-WS2v4' => '600',
    'HybridizationStep15-EukGE-WS2v4' => '600',
    'HybridizationStep16-EukGE-WS2v4' => '0',
    'HybridizationStep17-EukGE-WS2v4' => '30',
    'HybridizationStep18-EukGE-WS2v4' => '15',
    'HybridizationStep19-EukGE-WS2v4' => '4',
    'HybridizationStep20-EukGE-WS2v4' => '25',
};

is_deeply( $exp->get_parameters, $expected_params,
    'numbered hybridization parameters' );

