#!/usr/bin/env perl
#
# Fast CDF to MAGE-ML converter; a work in progress.
#
# Tim Rayner 2006, EBI Microarray Informatics Team
#
# $Id: cdf2mage.pl 1938 2008-02-10 18:34:20Z tfrayner $

use strict;
use warnings;

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

use ArrayExpress::ArrayMAGE qw(:ALL);
use ArrayExpress::ArrayMAGE::NullObject;

###########
# GLOBALS #
###########

ArrayExpress::ArrayMAGE->set_programname('CDF2MAGE');
ArrayExpress::ArrayMAGE->set_namespace('Affymetrix');
ArrayExpress::ArrayMAGE->set_separator(':');

# We do this ourselves, nothing fancy.
my $svn_revision = '$Revision: 1938 $';
our ($VERSION) = ( $svn_revision =~ /^\$Revision: ([\d\.]*)/ );

# Global flag to determine whether we're CS-only or not
my $CS_ONLY    = 0;

# This is a default, may be overwritten upon initial parsing of the CDF.
my $CDF_LINEBREAK = '\015\012';

use Getopt::Long;
use File::Spec;
use Benchmark;
use English qw( -no_match_vars );

use ArrayExpress::Curator::Common qw(check_linebreaks);

sub parse_args {

    # Parse script arguments, do some preprocessing.

    my ( %args, $output, $wantversion );

    GetOptions(
        "i|cdf=s"       => \$args{cdf},
        "a|accession=s" => \$args{accession},
        "o|output=s"    => \$output,
	"c|compseq"     => \$CS_ONLY,
        "v|version"     => \$wantversion,
    );

    if ($wantversion) {
	my $programname = ArrayExpress::ArrayMAGE->get_programname();
        print STDERR <<VERSION;
  This is $programname, SVN revision $VERSION
VERSION
        exit;
    }

    unless ( $args{cdf} && $output && $args{accession} ) {
        print STDERR <<NOINPUT;

  Usage: $0 -i <CDF filename> -a <accession number> -o <output file>

For very large CDF files, use the -c option to generate only the CompositeSequences.

NOINPUT
        exit 255;
    }

    # Basic sanity checks on the CDF file.
    if ( ! -f $args{cdf} ) {
        die("Error: CDF file not found: $args{cdf}\n");
    }
    elsif ( -B $args{cdf} ) {
        die("Error: Binary GCOS CDF files not yet supported.\n");
    }

    open( $args{output}, '>', $output )
        or die("Error: unable to open output file $output: $!\n");

    return \%args;
}

sub cs_initialize {

    my ( $accession, $reporter_group_id, $composite_group_id, $design_id ) = @_;

    # Create anon temp file objects (only the compulsory classes are
    # listed here).
    my @classes = qw(
        BioSequence
        CompositeSequence
        ArrayDesign
    );
    my @null_classes = qw(
        Feature
        Reporter
        FeatureReporterMap
        ReporterCompositeMap
        Zone
    );
    my %object;

    # Construct a single ReporterGroup
    my $reporter_group = ArrayExpress::ArrayMAGE::NullObject->new;

    # Construct a single CompositeGroup
    my $composite_group = ArrayExpress::ArrayMAGE::CompositeGroup->new({
        identifier => $composite_group_id,
        tag        => $design_id,
    });

    # Initialize the main classes.
    foreach my $type (@classes) {
	if ( $type eq 'ArrayDesign' ) {
	    $object{$type} = "ArrayExpress::ArrayMAGE::$type"->new({
		accession => $accession,
	    });
	}
	else {
	    $object{$type} = "ArrayExpress::ArrayMAGE::$type"->new();
	}
    }

    # Initialize the null classes.
    foreach my $type (@null_classes) {
	$object{$type} = ArrayExpress::ArrayMAGE::NullObject->new;
    }

    # NetAffx database used for ProbeSet ids
    $object{Database} = ArrayExpress::ArrayMAGE::Database->new();
    $object{Database}->add({ tag => 'netaffx' });

    return ( \%object, $reporter_group, $composite_group, $reporter_group_id,
        $composite_group_id );
}

sub initialize {

    my ( $accession, $reporter_group_id, $composite_group_id, $design_id ) = @_;

    # Create anon temp file objects (only the compulsory classes are
    # listed here).
    my @classes = qw(
        Feature
        Reporter
        CompositeSequence
        BioSequence
        FeatureReporterMap
        ReporterCompositeMap
        ArrayDesign
        Zone
    );
    my %object;

    # Construct a single ReporterGroup
    my $reporter_group = ArrayExpress::ArrayMAGE::ReporterGroup->new({
        identifier => $reporter_group_id,
        tag        => $design_id,
    });

    # Construct a single CompositeGroup
    my $composite_group = ArrayExpress::ArrayMAGE::CompositeGroup->new({
        identifier => $composite_group_id,
        tag        => $design_id,
    });

    # Initialize the main classes.
    foreach my $type (@classes) {
	if ( $type eq 'ArrayDesign' ) {
	    $object{$type} = "ArrayExpress::ArrayMAGE::$type"->new({
		accession => $accession,
	    });
	}
	else {
	    $object{$type} = "ArrayExpress::ArrayMAGE::$type"->new();
	}
    }

    # Only one zone for Affy arrays.
    $object{Zone}->add({
        row    => 1,
        column => 1,
    });

    # NetAffx database used for ProbeSet ids
    $object{Database} = ArrayExpress::ArrayMAGE::Database->new();
    $object{Database}->add({ tag => 'netaffx' });

    return ( \%object, $reporter_group, $composite_group, $reporter_group_id,
        $composite_group_id );
}

sub parse_unit {

    my ( $lines, $object, $reporter_group, $composite_group, $cdf ) = @_;

    # Get the probeset name - useful for SNP arrays
    my ($unit_probeset) = ( $lines->[1] =~ m/\A Name= ([\w\#\/\-\+\.\,]+) \z/xms );
    my ($unit_type)     = ( $lines->[6] =~ m/\A UnitType=         (\d+) \z/xms );
    my ($number_blocks) = ( $lines->[7] =~ m/\A NumberBlocks=     (\d+) \z/xms );

    # Add all the probeset reporters.
    # $previous_atom only scoped at this level for SNP chips, as those
    # CDFs often have more than one block per unit.
    my ( %reporter, %mismatch, $previous_atom );
    my $compseq = {};
    foreach my $blocknum ( 1 .. $number_blocks ) {
        my $chunk = <$cdf>;
        $chunk =~ s/^($CDF_LINEBREAK)*//;
        my @lines = split /$CDF_LINEBREAK/, $chunk;

        # $compseq should be a hash of probeset ids => [arrayref of
        # reporter ids]
        if ( $unit_type == 3 ) {

            # Gene Expression chip
            $compseq
                = parse_ge_block( \@lines, \%reporter, \%mismatch, $object );
        }
        elsif ( $unit_type == 2 ) {

            # SNP chip
            $previous_atom
                = parse_snp_block( \@lines, \%reporter, \%mismatch, $object,
                $unit_probeset, $previous_atom );
            $compseq->{$unit_probeset} = [ keys %reporter ];
        }
        elsif ( $unit_type == 7 ) {

            # TAG/GeneFlex chip
            $compseq
                = parse_tag_block( \@lines, \%reporter, \%mismatch, $object );
        }
        elsif ( $unit_type == 0 ) {

            # Control blocks as found in TAG chip CDFs
            $compseq = parse_control_block( \@lines, \%reporter, \%mismatch,
                $object );
        }
        elsif ( $unit_type == 1 ) {
            die("UnitType 1 (resequencing) not yet supported.");
        }
        else {
            die("Unrecognised unit type $unit_type ($unit_probeset).");
        }
    }

    # Post-process all the reporters in the compseq (probeset).
    foreach my $id ( sort keys %reporter ) {
        $object->{Reporter}->add({
            identifier   => $id,
            name         => $reporter{$id}{name},
            biosequences => $reporter{$id}{biosequences},
        });
        $object->{FeatureReporterMap}->add({
            identifier    => $id,
            features      => $reporter{$id}{features},
            mismatch_info => \%mismatch,
        });
        $reporter_group->add({ id_ref => $id, });
    }

    foreach my $id ( sort keys %$compseq ) {

        # Add the compseq (probeset) info.
	$object->{BioSequence}->add({
	    identifier  => $id,
	    polymertype => 'nucleic_acid',
	    type        => 'located_sequence_feature',
	    dbrefs      => { $id => 'netaffx' },
	});
        $object->{CompositeSequence}->add({
            identifier   => $id,
            name         => $id,
	    biosequences => [$id],
	    cs_only      => $CS_ONLY,
        });
        $object->{ReporterCompositeMap}->add({
            identifier => $id,
            reporters  => [ sort @{ $compseq->{$id} } ],
        });
        $composite_group->add({ id_ref => $id });
    }

    return;
}

sub parse_control_block {

    my ( $lines, $reporter, $mismatch, $object ) = @_;

    # TAG chip control units appear to also allow multiple probesets
    # per block. Here we detect them by sequence.
    my %probeset_cache;

    # Track the sequences in the block.
    my @sequences;

    PROBE_LINE:
    foreach my $line (@$lines) {

        my ( $row, $col, $feature_id )
            = add_feature( $line, $object->{Feature} );

        next PROBE_LINE unless ( $row && $col );

        my ($x,        $y,     $probe,      $feat,
            $probeset, $expos, $pos,        $cbase,
            $pbase,    $tbase, $atom,       $index,
            $codonind, $codon, $regiontype, $region
            )
            = split /\s+/, $line;

	# $reporter_id is empty string to prevent uninit warnings below.
        my $reporter_id = q{};
        my $biosequence_id;

        # Sequence-based probe pair detection.
        if ( length($probe) > 1 ) {

            my $probe_substr = $probe;
            substr( $probe_substr, $pos - 1, 1, q{} );

            SEQUENCE:
            foreach my $index ( 0 .. $#sequences ) {
                my $prev_substr = $sequences[$index];
                my $pm_base     = substr( $prev_substr, $pos - 1, 1, q{} );

                # Where the context sequence and tbase match the
                # PM probe, add the probe to that reporter.
                if (   ( $probe_substr eq $prev_substr )
                    && ( $tbase eq $pm_base ) ) {
                    $reporter_id    = "$probeset:ProbePair$index";
                    $biosequence_id = "$probeset:$index";
                    last SEQUENCE;
                }
            }

            unless ( ( $reporter_id && $biosequence_id )
			 || $CS_ONLY ) {

                # In case we see the MM probe first, rebuild the
                # PM probe sequence.
                my $pm_probe = $probe;
                substr( $pm_probe, $pos - 1, 1, $tbase );
                push( @sequences, $pm_probe );
                $reporter_id    = "$probeset:ProbePair" . $#sequences;
                $biosequence_id = "$probeset:" . $#sequences;

                $object->{BioSequence}->add({
                    identifier  => $biosequence_id,
                    polymertype => 'DNA',
                    type        => 'ss_oligo',
                    sequence    => $pm_probe,
                    dbrefs      => { $probeset => 'netaffx' },
                });
                push(
                    @{ $reporter->{$reporter_id}{biosequences} },
                    $biosequence_id
                );
            }
        }
        else {
            die("Error: UnitType 0 parsing only supported for units with sequence."
            );
        }

        # All probes need a feature and a reporter
        push( @{ $reporter->{$reporter_id}{features} }, $feature_id );
        $reporter->{$reporter_id}{name} = $reporter_id;

        # Cache the probeset -> probepair mapping for later.
        $probeset_cache{$probeset}{$reporter_id}++;

        unless ( $pbase eq $tbase ) {

            # MM probe, just record the mismatch info.
            $mismatch->{$feature_id} = {
                start_coord     => $pos,
                new_sequence    => lc($pbase),
                replaced_length => 1,
            };
        }
    }
    my %compseq;
    while ( my ( $id, $reporter_id_hash ) = each %probeset_cache ) {
        $compseq{$id} = [ keys %$reporter_id_hash ];
    }
    return \%compseq;
}

sub parse_tag_block {

    my ( $lines, $reporter, $mismatch, $object ) = @_;

    # Track the probe pairs with this variable.
    my $previous_probeset = q{};
    my %probeset_cache;

    PROBE_LINE:
    foreach my $line (@$lines) {

        my ( $row, $col, $feature_id )
            = add_feature( $line, $object->{Feature} );

        next PROBE_LINE unless ( $row && $col );

        my ($x,        $y,     $probe,      $feat,
            $probeset, $expos, $pos,        $cbase,
            $pbase,    $tbase, $atom,       $index,
            $codonind, $codon, $regiontype, $region
            )
            = split /\s+/, $line;

        my $reporter_id    = "$probeset:ProbePair$atom";
        my $biosequence_id = "$probeset:$atom";
        $probeset_cache{$probeset}{$reporter_id}++;

        # All probes need a feature and a reporter
        push( @{ $reporter->{$reporter_id}{features} }, $feature_id );
        $reporter->{$reporter_id}{name} = $reporter_id;

        # TAG probesets can vary within blocks; don't add BS if probe
        # seq not known.
        unless ( $probeset eq $previous_probeset
		     || length($probe) <= 1
		     || $CS_ONLY ) {

            # We rebuild the PM probe here, in case the MM probe
            # comes first.
            my $pm_probe = $probe;
            substr( $pm_probe, $pos - 1, 1, $tbase ) if $pm_probe;

            $object->{BioSequence}->add({
                identifier  => $biosequence_id,
                polymertype => 'DNA',
                type        => 'ss_oligo',
                sequence    => $pm_probe,
                dbrefs      => { $probeset => 'netaffx' },
            });
            push(
                @{ $reporter->{$reporter_id}{biosequences} },
                $biosequence_id
            );
        }

        # Sort out match and mismatch.
        unless ( $pbase eq $tbase ) {

            # MM probe, just record the mismatch info.
            $mismatch->{$feature_id} = {
                start_coord     => $pos,
                new_sequence    => lc($pbase),
                replaced_length => 1,
            };
        }

        $previous_probeset = $probeset;
    }

    my %compseq;
    while ( my ( $id, $reporter_id_hash ) = each %probeset_cache ) {
        $compseq{$id} = [ keys %$reporter_id_hash ];
    }
    return \%compseq;
}

sub parse_snp_block {

    my ( $lines, $reporter, $mismatch, $object, $probeset, $previous_atom )
        = @_;

    foreach my $line (@$lines) {
        $previous_atom
            = parse_probeline( $line, $probeset, $reporter, $mismatch,
            $object, $previous_atom );
    }
    return $previous_atom;
}

sub parse_ge_block {

    my ( $lines, $reporter, $mismatch, $object ) = @_;

    # Probeset name stored in the block, not the unit, for GE units.
    my $probeset;
    if ( $lines->[0] =~ m/^\[Unit\d+_Block\d+\]$/ ) {

        # Get the block name if the unit name was not set.
        ($probeset) = ( $lines->[1] =~ m/\A Name= ([\w\#\/\-\+\.\,]+) \z/xms );
    }
    unless ($probeset) {
        die(qq{Error: Unparseable block starts with line $lines->[0].});
    }

    # Track the probe pairs with this variable. Must start as undef.
    my $previous_atom;

    foreach my $line (@$lines) {
        $previous_atom
            = parse_probeline( $line, $probeset, $reporter, $mismatch,
            $object, $previous_atom );
    }
    return { $probeset => [ keys %$reporter ] };
}

sub parse_probeline {

    my ( $line, $probeset, $reporter, $mismatch, $object, $previous_atom )
        = @_;

    my ( $row, $col, $feature_id ) = add_feature( $line, $object->{Feature} );

    return $previous_atom unless ( $row && $col );

    my ($x,        $y,     $probe,      $feat,  $qual, $expos,
        $pos,      $cbase, $pbase,      $tbase, $atom, $index,
        $codonind, $codon, $regiontype, $region
        )
        = split /\s+/, $line;

    my $reporter_id = "$probeset:ProbePair$atom";
    my $biosequence_id;

    # All probes need a feature and a reporter
    push( @{ $reporter->{$reporter_id}{features} }, $feature_id );
    $reporter->{$reporter_id}{name} = $reporter_id;

    # Create the BioSequences where sequence known.
    if ( length($probe) > 1 && ! $CS_ONLY ) {

        $biosequence_id = "$probeset:$atom";

        # Sequence given, first probe in the pair.
        unless ( defined($previous_atom) && ( $atom eq $previous_atom ) ) {

            # We rebuild the PM probe here, in case the MM probe
            # comes first.
            my $pm_probe = $probe;
            substr( $pm_probe, $pos - 1, 1, $tbase ) if $pm_probe;

            $object->{BioSequence}->add({
                identifier  => $biosequence_id,
                polymertype => 'DNA',
                type        => 'ss_oligo',
                sequence    => $pm_probe,
                dbrefs      => { $probeset => 'netaffx' },
            });

	    # Reference the BS once for each atom.
	    push( @{ $reporter->{$reporter_id}{biosequences} }, $biosequence_id );
        }
    }

    # Sort out match and mismatch.
    unless ( $pbase eq $tbase ) {

        # MM probe, just record the mismatch info.
        $mismatch->{$feature_id} = {
            start_coord     => $pos,
            new_sequence    => lc($pbase),
            replaced_length => 1,
        };
    }

    return $atom;
}

sub add_feature {
    my ( $line, $mageobj ) = @_;

    if ( my ( $row, $col ) = ( $line =~ m/Cell\d+=(\d+)\s+(\d+)/ ) ) {
        my $feature_id = "Probe($row,$col)";
        $mageobj->add({
            identifier => $feature_id,
            metacolumn => 1,
            metarow    => 1,
            column     => $col,
            row        => $row,
        });
        return ( $row, $col, $feature_id );
    }
    return;
}

########
# MAIN #
########

# Get our arguments
my $args = parse_args();

# Initialise some stuff
my $starttime = new Benchmark;

# Get the line endings.
warn("Checking CDF linebreaks...\n");
my $linebreak_counts;
($linebreak_counts, $CDF_LINEBREAK) = check_linebreaks( $args->{cdf} );

unless ($CDF_LINEBREAK) {
    use Data::Dumper;
    print Dumper $linebreak_counts;
    die("Error: inconsistent line breaks in CDF.\n");
}

# Open the CDF and scan down to the header row
open( my $cdf, "<", $args->{cdf} )
    or die("Error: unable to open CDF file $args->{cdf}: $!\n");

my ( $object, $reporter_group, $composite_group, $design_id );
my $reporter_group_id  = 'All';
my $composite_group_id = 'All';

{

    # Paragraph mode.
    local $INPUT_RECORD_SEPARATOR = $CDF_LINEBREAK . $CDF_LINEBREAK;

    my ( $total_units, $progress_bar );

    # Parse the header.
    HEADER_LINE:
    while ( my $chunk = <$cdf> ) {

        # Strip leading empty lines
        $chunk =~ s/^($CDF_LINEBREAK)*//;
        my @lines = split /$CDF_LINEBREAK/, $chunk;

        next unless @lines;

        if ( $lines[0] eq q{[Chip]} ) {
            ($design_id)   = ( $lines[1] =~ /^Name=(.*)$/ );
            ($total_units) = ( $lines[4] =~ /^NumberOfUnits=(\d+)$/ );
            die("Error: Unable to parse number of units from [Chip] section")
                unless defined($total_units);
	    ArrayExpress::ArrayMAGE->set_design( $design_id );
            $progress_bar = ArrayExpress::ArrayMAGE::ProgressBar->new(
                width   => 40,
                max_val => $total_units
            );
            last HEADER_LINE;
        }
    }

    # Quick sanity check.
    unless ( defined($total_units) ) {
	die("Error parsing [Chip] section.\n");
    }

    # Initialize the MAGE objects (needs $design_id to be set)
    if ( $CS_ONLY ) {

	# More lightweight CS-only MAGE-ML to be generated.
	( $object, $reporter_group, $composite_group )
	    = cs_initialize(
		$args->{accession},
		$reporter_group_id,
		$composite_group_id,
		$design_id,
	    );

    }
    else {

	# Business as usual (full MAGE-ML generation).
	( $object, $reporter_group, $composite_group )
	    = initialize(
		$args->{accession},
		$reporter_group_id,
		$composite_group_id,
		$design_id,
	    );
    }

    # Parse the rest of the file.
    my $unit_num;
    while ( my $chunk = <$cdf> ) {

        # Strip leading empty lines
        $chunk =~ s/^($CDF_LINEBREAK)*//;
        my @lines = split /$CDF_LINEBREAK/, $chunk;

        next unless @lines;

        if ( $lines[0] =~ m/^\[QC\d+\]$/ ) {

            # QC cell
            foreach my $line (@lines) {
                add_feature( $line, $object->{Feature} );
            }
        }
        elsif ( $lines[0] =~ m/^\[Unit\d+\]$/ ) {

            $progress_bar->draw( $unit_num++ ) if $progress_bar;

            parse_unit( \@lines, $object, $reporter_group, $composite_group,
                $cdf );
        }
    }
}

close $cdf;

my $rg_hashref = $CS_ONLY
               ? {}
               : { $reporter_group_id  => $reporter_group };
ArrayExpress::ArrayMAGE->combine_parts(
    $object,
    $rg_hashref,
    { $composite_group_id => $composite_group },
    $args->{output},
);

print STDOUT ("Done.\n");

my $endtime = new Benchmark;
print STDOUT (
    "\nTotal run time = ",
    timestr( timediff( $endtime, $starttime ) ), "\n\n"
);
