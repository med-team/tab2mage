#!/usr/bin/env perl
#
# Fast ADF to MAGE-ML converter
#
# Tim Rayner 2005, EBI Microarray Informatics Team
#
# $Id: arraymage.pl 1938 2008-02-10 18:34:20Z tfrayner $

use strict;
use warnings;

# Next line is a placeholder. Uncomment and edit as necessary, or set PERL5LIB environmental variable
# use lib /path/to/directory/containing/Curator/modules;

use Readonly;
use ArrayExpress::ArrayMAGE qw(:ALL);

###########
# GLOBALS #
###########

# These are shared with ArrayExpress::ArrayMAGE
ArrayExpress::ArrayMAGE->set_programname('ArrayMAGE');
ArrayExpress::ArrayMAGE->set_namespace('ebi.ac.uk:ArrayMAGE');

# We do this ourselves, nothing fancy.
my $svn_revision = '$Revision: 1938 $';
our ($VERSION) = ( $svn_revision =~ /^\$Revision: ([\d\.]*)/ );

##########################
###### Main Package ######
##########################

package main;

use Getopt::Long;
use File::Spec;
use Benchmark;

Readonly my $META_COLUMN         => 'MetaColumn';
Readonly my $META_ROW            => 'MetaRow';
Readonly my $COLUMN              => 'Column';
Readonly my $ROW                 => 'Row';
Readonly my $REPORTER_IDENTIFIER => 'Reporter Identifier';
Readonly my $REPORTER_NAME       => 'Reporter Name';
Readonly my $REPORTER_COMMENT    => 'Reporter Comment';

Readonly my $BIOSEQUENCE_TYPE        => 'Reporter BioSequence Type';
Readonly my $BIOSEQUENCE_POLYMERTYPE => 'Reporter BioSequence PolymerType';
Readonly my $REPORTER_CONTROL_TYPE   => 'Reporter Control Type';
Readonly my $REPORTER_FAIL_TYPE      => 'Reporter Fail Type';
Readonly my $REPORTER_WARNING_TYPE   => 'Reporter Warning Type';

Readonly my $REPORTER_ACTUAL_SEQUENCE =>
    'Reporter BioSequence [Actual sequence]';

Readonly my $REPORTER_FWD_PRIMER => 'Reporter BioSequence [Forward primer]';

Readonly my $REPORTER_REV_PRIMER => 'Reporter BioSequence [Reverse primer]';

Readonly my $BIOSEQUENCE_DB_ENTRY_PATTERN =>
    qr{Reporter\s*BioSequence\s*Database\s*Entry\s*\[\s*(.*?)\s*\]}ixms;

Readonly my $REPORTER_DB_ENTRY_PATTERN =>
    qr{Reporter\s*Description\s*Database\s*Entry\s*\[\s*(.*?)\s*\]}ixms;

Readonly my $REPORTER_GROUP_PATTERN =>
    qr{Reporter\s*Group\s*\[\s*(.*?)\s*\]}ixms;

sub parse_args {

    # Parse script arguments, do some preprocessing.

    my ( %args, $output, $wantversion );

    GetOptions(
        "i|adf=s"       => \$args{adf},
        "a|accession=s" => \$args{design},
        "o|output=s"    => \$output,
        "v|version"     => \$wantversion,
    );

    if ($wantversion) {
	my $programname = ArrayExpress::ArrayMAGE->get_programname();
        print STDERR <<VERSION;
  This is $programname, SVN revision $VERSION
VERSION
        exit;
    }

    unless ( $args{adf} && $output && $args{design} ) {
        print STDERR <<NOINPUT;

  Usage: $0 -i <ADF filename> -a <accession number> -o <output file>

NOINPUT
        exit 255;
    }

    open( $args{output}, '>', $output )
        or die("Error: unable to open output file $output: $!\n");

    ArrayExpress::ArrayMAGE->set_design($args{design});

    return \%args;
}

sub coerce_headings {

    # Match up our headings with the set of known constant headings,
    # warn for unrecognized headings.

    my ($line) = @_;
    $line =~ s/[\r\n]*$//;
    my @headings = split /\t/, $line;

    foreach my $name (@headings) {

        my $test = $name;
        $test =~ s/ //g;

        HEADNAME: {

            ( $test =~ /^metacolumn$/i )
                && do { $name = $META_COLUMN; last HEADNAME; };

            ( $test =~ /^metarow$/i )
                && do { $name = $META_ROW; last HEADNAME; };

            ( $test =~ /^column$/i )
                && do { $name = $COLUMN; last HEADNAME; };

            ( $test =~ /^row$/i )
                && do { $name = $ROW; last HEADNAME; };

            ( $test =~ /^reporteridentifier$/i )
                && do { $name = $REPORTER_IDENTIFIER; last HEADNAME; };

            ( $test =~ /^reportername$/i )
                && do { $name = $REPORTER_NAME; last HEADNAME; };

            ( $test =~ /^reportercomment$/i )
                && do { $name = $REPORTER_COMMENT; last HEADNAME; };

            ( $test =~ /biosequencetype$/i )
                && do { $name = $BIOSEQUENCE_TYPE; last HEADNAME; };

            ( $test =~ /biosequencepolymertype$/i )
                && do { $name = $BIOSEQUENCE_POLYMERTYPE; last HEADNAME; };

            ( $test =~ /controltype$/i )
                && do { $name = $REPORTER_CONTROL_TYPE; last HEADNAME; };

            ( $test =~ /failtype$/i )
                && do { $name = $REPORTER_FAIL_TYPE; last HEADNAME; };

            ( $test =~ /warningtype$/i )
                && do { $name = $REPORTER_WARNING_TYPE; last HEADNAME; };

            ( $test =~ /actualsequence/i )
                && do { $name = $REPORTER_ACTUAL_SEQUENCE; last HEADNAME; };

            ( $test =~ /(forward|fwd)primer/i )
                && do { $name = $REPORTER_FWD_PRIMER; last HEADNAME; };

            ( $test =~ /(reverse|rev)primer/i )
                && do { $name = $REPORTER_REV_PRIMER; last HEADNAME; };

            ( $test =~ $BIOSEQUENCE_DB_ENTRY_PATTERN )
                && do { last HEADNAME };

            ( $test =~ $REPORTER_DB_ENTRY_PATTERN )
                && do { last HEADNAME };

            ( $test =~ $REPORTER_GROUP_PATTERN )
                && do { last HEADNAME };

            warn("WARNING: unrecognized column heading \"$name\"\n");

        }

    }

    return \@headings;

}

sub sort_rows {

    # Use the sort utility on unix OSes to quickly sort on Reporter
    # Identifier (and CS ID FIXME)

    my ( $fh, $headings, $name ) = @_;

    my $colno;
    for ( 0 .. $#$headings ) {
        $colno = $_ if ( $headings->[$_] eq $REPORTER_IDENTIFIER );
    }
    unless ($colno) {
        die(      "Error: File contains no "
                . $REPORTER_IDENTIFIER
                . " column.\n" );
    }

    # map 0..(n-1) onto 1..n
    $colno++;

    my $linecount = 0;
    open( my $sorted, '|-', "sort -b -t \'\t\' -k $colno -o \"$name\"" )
        or die("Error opening temporary sort file $name: $!\n");

    print STDOUT ("Sorting ADF on column $colno...\n");
    while ( my $line = <$fh> ) {
        print $sorted $line;
        $linecount++;
    }

    close $sorted or die("$!\n");
    open( $sorted, '<', $name ) or die("$!\n");

    return ( $sorted, $linecount );
}

sub assign_headings {

    # Given a line string and a list of headings, return a heading =>
    # value hash

    my ( $row, $headings ) = @_;

    $row =~ s/[\r\n]*$//;
    my @rowlist = split /\t/, $row;
    my %rowhash;

    for ( 0 .. $#rowlist ) {
        unless ( $headings->[$_] ) {
            warn(
                "WARNING: Skipping entry without column heading $rowlist[$_]\n"
            );
            next;
        }
        $rowhash{ $headings->[$_] } = $rowlist[$_];
    }
    return \%rowhash;
}

########
# MAIN #
########

# Get our arguments
my $args = parse_args;

# Initialise some stuff
my $starttime = new Benchmark;

# Open the ADF and scan down to the header row
open( my $adf, "<", $args->{adf} )
    or die("Error: unable to open ADF file $args->{adf}: $!\n");
my $line = q{};
until ( $line =~ m/\bMeta\s*Column\b/i ) { $line = <$adf> }

# Generate a $headings arrayref with standardized values
my $headings = coerce_headings($line);

# Sort the file to allow FRM and RCM shortcuts, keeping memory usage
# down.
my $tempfile
    = File::Spec->catpath( File::Spec->tmpdir, "$args->{adf}.temporary" );
my ( $sorted, $num_lines ) = sort_rows( $adf, $headings, $tempfile );

close $adf;

# Create anon temp file objects (only the compulsory classes are
# listed here).
my @classes = qw(
    Feature
    Reporter
    BioSequence
    FeatureReporterMap
    ArrayDesign
    Zone
);
my %object;

foreach my $type (@classes) {
    if ( $type eq 'ArrayDesign' ) {
        $object{$type} = "ArrayExpress::ArrayMAGE::$type"->new({
	    accession => $args->{design},
	});
    }
    else {
	$object{$type} = "ArrayExpress::ArrayMAGE::$type"->new();
    }
}

# Create database objects
my %dbtags;
foreach my $heading (@$headings) {
    foreach my $pattern ( $BIOSEQUENCE_DB_ENTRY_PATTERN,
        $REPORTER_DB_ENTRY_PATTERN ) {
        if ( my ($tag) = ( $heading =~ $pattern ) ) {
            $dbtags{$tag}++;
        }
    }
}
if ( scalar( grep { defined $_ } values %dbtags ) ) {
    $object{Database} = ArrayExpress::ArrayMAGE::Database->new();
}
foreach my $tag ( keys %dbtags ) {
    $object{Database}->add({ tag => $tag });
}

# Set up a few tracker variables
my %zone_tracker;      # hash to track Zone creation
my %reporter_group;    # hash of ReporterGroup objects
my $last_reporter = q{};
my $last_compseq  = q{};
my @frm_features;      # will be undefed in the loop
my @rcm_reporters;     # will be undefed in the loop

# Map "other" BioSequences to their respective Types from MO
my %bioseq_mapping = (
    $REPORTER_FWD_PRIMER => 'PCR_primer_forward',
    $REPORTER_REV_PRIMER => 'PCR_primer_reverse',
);

#############
# Main loop #
#############

print STDOUT ("Writing MAGE-ML to temporary files...\n");
my $progress_bar = ArrayExpress::ArrayMAGE::ProgressBar->new(
    width   => 40,
    max_val => $num_lines
);
my $linecount = 0;
while ( my $line = <$sorted> ) {

    $linecount++;

    $progress_bar->draw($linecount);

    # skip blank lines
    next if $line =~ m/^\s*$/;

    $line =~ s/>/&amp;gt;/g;
    $line =~ s/</&amp;lt;/g;
    $line =~ s/"/&quot;/g;
    $line =~ s/'/&apos;/g;
    $line =~ s/&/&amp;amp;/g;

    # {heading => value} hashref
    my $row = assign_headings( $line, $headings );

    # Zone
    unless ( $zone_tracker{ ( $row->{$META_COLUMN} . $row->{$META_ROW} ) } ) {
        $object{Zone}->add({
            row    => $row->{$META_ROW},
            column => $row->{$META_COLUMN},
        });
        $zone_tracker{ ( $row->{$META_COLUMN} . $row->{$META_ROW} ) }++;
    }

    # Feature
    $object{Feature}->add({
        metacolumn => $row->{$META_COLUMN},
        metarow    => $row->{$META_ROW},
        column     => $row->{$COLUMN},
        row        => $row->{$ROW},
    });

    # FeatureReporterMap, Reporter, BioSequence.
    # Only do these if we're processing a new reporter id.
    unless ( $row->{$REPORTER_IDENTIFIER} eq $last_reporter ) {

        # Database or ReporterGroup headings;
        my ( %bs_dbids, %r_dbids );
        while ( my ( $rname, $rvalue ) = each %$row ) {
            if ( my ($type) = ( $rname =~ $REPORTER_GROUP_PATTERN ) ) {
                my $key = "$type." . $rvalue;
                unless ( $reporter_group{$key} ) {
                    $reporter_group{$key}
                        = ArrayExpress::ArrayMAGE::ReporterGroup->new({
                        identifier => $key,
                        tag        => $rvalue,
                        is_species => ( $type =~ m/species/i ) || 0,
                    });
                }
                $reporter_group{$key}
                    ->add({ id_ref => $row->{$REPORTER_IDENTIFIER}, });
            }
            if ( my ($tag) = ( $rname =~ $BIOSEQUENCE_DB_ENTRY_PATTERN ) ) {

                # keys are accessions, values are db tags
                $bs_dbids{$rvalue} = $tag if length($rvalue);
            }
            if ( my ($tag) = ( $rname =~ $REPORTER_DB_ENTRY_PATTERN ) ) {

                # keys are accessions, values are db tags
                $r_dbids{$rvalue} = $tag if length($rvalue);
            }
        }

        # BioSequence
        # Flag to indicate whether BS creation should occur
        my $has_biosequence = ( $row->{$BIOSEQUENCE_POLYMERTYPE}
                || $row->{$BIOSEQUENCE_TYPE}
                || $row->{$REPORTER_ACTUAL_SEQUENCE}
                || scalar( grep { defined $_ } values %bs_dbids ) );
        my @biosequences;
        if ($has_biosequence) {
            $object{BioSequence}->add({
                identifier  => $row->{$REPORTER_IDENTIFIER},
                dbrefs      => \%bs_dbids,
                polymertype => $row->{$BIOSEQUENCE_POLYMERTYPE},
                type        => $row->{$BIOSEQUENCE_TYPE},
                sequence    => $row->{$REPORTER_ACTUAL_SEQUENCE},
            });
            push( @biosequences, $row->{$REPORTER_IDENTIFIER} );
        }

        # Primer sequences
        foreach my $bs_type ( $REPORTER_FWD_PRIMER, $REPORTER_REV_PRIMER ) {
            if ( $row->{$bs_type} ) {
                $object{BioSequence}->add({
                    identifier => $row->{$REPORTER_IDENTIFIER}
                        . ".$bioseq_mapping{$bs_type}",
                    polymertype => 'DNA',
                    type        => $bioseq_mapping{$bs_type},
                    sequence    => $row->{$bs_type},
                });
                push( @biosequences,
                    $row->{$REPORTER_IDENTIFIER}
                        . ".$bioseq_mapping{$bs_type}" );
                $has_biosequence++;
            }
        }

        # Reporter
        $object{Reporter}->add({
            identifier   => $row->{$REPORTER_IDENTIFIER},
            name         => $row->{$REPORTER_NAME},
            comment      => $row->{$REPORTER_COMMENT},
            biosequences => \@biosequences,
            controltype  => $row->{$REPORTER_CONTROL_TYPE},
            warningtype  => $row->{$REPORTER_WARNING_TYPE},
            failtype     => $row->{$REPORTER_FAIL_TYPE},
            dbrefs       => \%r_dbids,
        });

        # FeatureReporterMap

        # Skip over creation of first FRM; the last is created outside
        # the end of this loop.
        if ($last_reporter) {
            $object{FeatureReporterMap}->add({
                identifier => $last_reporter,
                features   => \@frm_features,
            });
        }
        undef @frm_features;

    }

    push( @frm_features,
        "$row->{$META_COLUMN}.$row->{$META_ROW}.$row->{$COLUMN}.$row->{$ROW}"
    );
    $last_reporter = $row->{$REPORTER_IDENTIFIER};

    # same for compseq FIXME

}

# Deal with the last FRM/RCM
$object{FeatureReporterMap}->add({
    identifier => $last_reporter,
    features   => \@frm_features,
});

unlink($tempfile)
    or warn("Warning: could not delete temporary file $tempfile: $!\n");

# Empty hash is composite_group placeholder FIXME
ArrayExpress::ArrayMAGE->combine_parts(
    \%object,
    \%reporter_group,
    {},
    $args->{output},
);

print STDOUT ("Done.\n");

my $endtime = new Benchmark;
print STDOUT (
    "\nTotal run time = ",
    timestr( timediff( $endtime, $starttime ) ), "\n\n"
);

