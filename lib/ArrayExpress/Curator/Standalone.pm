#!/usr/bin/env perl
#
# Module to provide basic methods used by the expt_check script.
#
# Tim Rayner 2005, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Standalone.pm 1987 2008-03-06 11:29:22Z tfrayner $
#

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: Standalone.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Curator::Standalone - Standalone experiment checking.

=head1 SYNOPSIS

 use ArrayExpress::Curator::Standalone;
 
 my $checker =
   ArrayExpress::Curator::Standalone->new({
       data_files => \@ARGV,
   });
 
 $checker->check();

=head1 DESCRIPTION

This module provides the basic operations needed for checking a set of
data files in full standalone mode. For MIAMExpress and Tab2MAGE
experiment checking, see L<ArrayExpress::Curator::MIAMExpress> and
L<ArrayExpress::Curator::Validate>, respectively.

=head1 OPTIONS

The following options must be used in addition to those provided by the
parent class (see L<ArrayExpress::Curator::ExperimentChecker>):

=over 2

=item C<data_files>

An array reference containing a list of data filenames to be checked.

=back

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2007.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

package ArrayExpress::Curator::Standalone;

use strict;
use warnings;

use Carp;
use English qw( -no_match_vars );
use Class::Std;

use ArrayExpress::Curator::Config qw($CONFIG);

require ArrayExpress::Datafile;

use base qw(ArrayExpress::Curator::ExperimentChecker);

my %data_files  : ATTR( :get<data_files>, :init_arg<data_files>, :default<[]> );

sub START {
    my ( $self, $id, $args ) = @_;

    unless ( scalar @{ $data_files{$id} } ) {
	croak("Error: No data files passed to standalone checker.");
    }

    $self->set_is_standalone(1);

    # Localise to CWD, or value of get_log_to_current_dir.
    $self->localize_logfiles();

    return;
}

sub get_files_and_annotation : RESTRICTED {

    my ( $self ) = @_;

    # Full standalone mode; MIAMExpress and Tab2MAGE checking are
    # handled by overriding this method in their respective
    # subclasses.

    my $filelist = [];
    my $hyb_ids  = {};   # Used to map FGEM column headings to hybs.
    my $norm_ids = {};   # Empty for MX; cannot map MX FGEM using Norm IDs.

    print STDOUT ("Running in Standalone mode...\n");

    $self->logprint( 'error',  "* Standalone mode *\n\n" );
    $self->logprint( 'report', "* Standalone mode *\n\n" );

    # Set up our list of Datafile objects
    foreach my $filename ( @{ $self->get_data_files() } ) {

	# We have to set a default file type and array design
	# id here.
	my $file = ArrayExpress::Datafile->new({
	    name            => $filename,
	    data_type       => 'raw',
	    array_design_id => $self->get_array_accession() || 'UNKNOWN',
	});

	push( @$filelist, $file );
    }

    $self->logprint_line( 'error', 'ADF parsing START' );
    
    # Populate the $file->get_adf_features() hashrefs within @$filelist
    $self->cache_user_supplied_arrays( $filelist );
    $self->populate_file_arraydesigns( $filelist );
    
    $self->logprint_line( 'error', 'ADF parsing END' );

    return ( $filelist, $hyb_ids, $norm_ids );
}

sub visualize_experiment : RESTRICTED {

    my ( $self ) = @_;

    # Empty method; we can't visualize in such cases.

    return;
}

1;
