#!/usr/bin/env perl
#
# Database.pm - a module derived from and used in the experiment
# checker script. Contains routines which might be useful elsewhere.
#
# Tim Rayner 2004 ArrayExpress team, EBI
#
# $Id: Database.pm 2027 2008-04-16 10:25:26Z tfrayner $
#

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../index.html">
                <img src="../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: Database.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Curator::Database - a module used by expt_check.pl

=head1 SYNOPSIS

 use ArrayExpress::Curator::Database qw(retrieve_AE_adf parse_adf);

 my $db_id = 123456789;
 my ( $fh, $accession ) = retrieve_AE_adf( $db_id, undef, \*STDERR, 0 );
 my $array_design = parse_adf( $fh, $accession );

=head1 DESCRIPTION

This module provides a set of subroutines used by the experiment
checker script to interact with the ArrayExpress databases. Queries may
be redirected to local ArrayExpress instances by editing values in
the L<ArrayExpress::Curator::Config> package.

=head1 FUNCTIONS

=over 2

=item C<get_ae_dbh()>

Returns a singleton database handle for the currently-configured
ArrayExpress repository database instance.

=item C<get_aedw_dbh()>

Returns a singleton database handle for the currently-configured
ArrayExpress warehouse database instance.

=item C<retrieve_AE_adf( $db_id, $accession, $error_fh, $skip_adf_download )>

Given either AE database table row identifier or AE accession, an open
filehandle for error reporting and a flag indicating whether the ADF
should be downloaded, check for the existence of said ADF (by database
id or accession), download it and return an opened filehandle and the
accession number (the latter is useful when querying by database id).

=item C<retrieve_AE_featurelist( $accession )>

Given an array accession number, return a hashref where the keys are
q{.}-delimited lists of MetaColumn.MetaRow.Column.Row feature
coordinates, and the values are Reporter identifiers.

=item C<parse_adf( $adf_fh, $accno, $reporter_prefix, $compseq_prefix )>

Passed an ADF filehandle and an accession number (see
retrieve_AE_adf()), and optional Reporter or Composite Sequence
identifier prefixes, parse the ADF and return an ArrayDesign object
(see L<ArrayExpress::Datafile::ArrayDesign>).

=item C<arrayaccession_in_aedw( $accession )>

Given an array design accession number, return true if the design is
loaded into the currently-configured AE data warehouse, false
otherwise.

=item C<map_affy_accno_to_name( $accession )>

Given an array design accession number, attempt to match it to an
Affymetrix design name. This relies on the names of loaded Affy array
designs including the design name (e.g. HG-U133A) in square brackets
(e.g. "Affymetrix GeneChip Human Genome HG-U133A [HG-U133A]").

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2008.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

package ArrayExpress::Curator::Database;

use strict;
use warnings;

use LWP::UserAgent;
use List::Util qw( min );
use Scalar::Util qw( openhandle );

use Carp;
use IO::File;

use ArrayExpress::Curator::Common qw(
    get_indexcol
);

use ArrayExpress::Curator::Config qw($CONFIG);

require ArrayExpress::Datafile::ArrayDesign;

use base 'Exporter';
our @EXPORT_OK = qw(
    get_ae_dbh
    get_aedw_dbh
    retrieve_AE_adf
    retrieve_AE_featurelist
    parse_adf
    arrayaccession_in_aedw
    map_affy_accno_to_name
    get_ae_array_details
);

# Globals to hold AE and AEDW database handles, if needed.
my ($AE_DBH, $AEDW_DBH);

# Global to store retrieved AE array accession number table; we only
# want to retrieve it once.
my @AE_ARRAY_ACCNO_TABLE;

sub _connect_to_oracle {

    my ($dsn, $username, $password, $params) = @_;

    eval {
	require DBI;
	require DBD::Oracle;
    };
    if ($@) {
	croak(<<END_ERROR);

Error: Connection to a local ArrayExpress database instance requires
both the DBI and the DBD::Oracle modules to be installed.
The following error was encountered:

$@

END_ERROR
    }

    my $dbh = DBI->connect( $dsn, $username, $password, $params )
	or croak( "Error: Cannot connect to local ArrayExpress"
		      . " database instance: $DBI::errstr\n" );

    return $dbh;
}

sub get_ae_dbh {

    # If we're not yet connected, and we know how to, do so.
    if ( ! $AE_DBH ) {
	if ( $CONFIG->get_AE_DSN() ) {
	    print STDOUT ("Connecting to ArrayExpress database...\n");
	    $AE_DBH = _connect_to_oracle(
		$CONFIG->get_AE_DSN(),      $CONFIG->get_AE_USERNAME(),
		$CONFIG->get_AE_PASSWORD(), $CONFIG->get_AE_DBPARAMS()
	    );
	}
    }

    # Check that we're still connected, and reconnect if
    # necessary. N.B. This still doesn't fix the problem of sharing
    # DBH between forked processes, which is generally frowned upon
    # anyway.
    elsif ( ! $AE_DBH->ping() ) {
	print STDOUT ("Reconnecting to ArrayExpress database...\n");
	$AE_DBH = _connect_to_oracle(
	    $CONFIG->get_AE_DSN(),      $CONFIG->get_AE_USERNAME(),
	    $CONFIG->get_AE_PASSWORD(), $CONFIG->get_AE_DBPARAMS()
	);
    }

    return $AE_DBH;
}

sub get_aedw_dbh {

    # If we're not yet connected, and we know how to, do so.
    if ( !$AEDW_DBH && $CONFIG->get_AEDW_DSN() ) {
	print STDOUT ("Connecting to ArrayExpress Data Warehouse...\n");
	$AEDW_DBH = _connect_to_oracle(
	    $CONFIG->get_AEDW_DSN(),      $CONFIG->get_AEDW_USERNAME(),
            $CONFIG->get_AEDW_PASSWORD(), $CONFIG->get_AEDW_DBPARAMS()
	);
    }

    return $AEDW_DBH;
}

sub map_affy_accno_to_name {

    my ( $accno ) = @_;

    unless ( scalar @AE_ARRAY_ACCNO_TABLE ) {
	retrieve_accno_table();
    }

    my $affyname = q{};

    AE_ACCNO_LINE:
    foreach my $line ( @AE_ARRAY_ACCNO_TABLE ) {
	next AE_ACCNO_LINE if ( $line =~ m/^\s*$/ );       # skip empty lines

	my @line_array = split /\t/, $line;
	if ( $line_array[1] eq $accno ) {
	    ($affyname) = ($line_array[2] =~ m/\[ ([^\]]+) \]/xms);
	    last AE_ACCNO_LINE;
	}
    }

    return $affyname;
}

sub parse_adf {    # Accepts filehandles, not filenames. This is to allow
                   # the use of temporary files via IO::File when getting
                   # the ADF from ArrayExpress.

    my ( $adf_filehandle, $accno, $reporter_prefix, $compseq_prefix ) = @_;

    openhandle($adf_filehandle)
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $feature_indices  = [];
    my $reporter_indices = [];
    my $compseq_indices  = [];

    # Special case of Affymetrix identifiers. This is tied to the
    # ArrayExpress accession number format.
    if ( $accno && $accno =~ m/\A A-AFFY-\w+ \z/xms ) {

	# Use the AE-derived name (in brackets) where available,
	# otherwise fall back to a simple default.
	my $affyname = map_affy_accno_to_name($accno) || '[^:]*';

	# Don't override user-supplied prefixes.
	unless (defined $reporter_prefix) {
	    $reporter_prefix = "Affymetrix:Reporter:$affyname:";
	}
	unless (defined $compseq_prefix) {
	    $compseq_prefix = "Affymetrix:CompositeSequence:$affyname:";
	}
    }

    # First, scan down through whatever header there is.
    ADF_HEADER_LINE:
    while ( my $line = <$adf_filehandle> ) {

        chomp $line;

	# Start parsing the ADF on the first line containing both
	# Identifier and Name columns (either Reporter or
	# CompositeSequence). This replaces a parsing start based on
	# the number of columns in the ADF, which doesn't work for too
	# many crappy ADFs in AE.
	my $identifier_re = qr/(Reporter|Composite [ ]* Sequence) [ ]* Identifier/ixms;
	my $name_re       = qr/(Reporter|Composite [ ]* Sequence) [ ]* Name/ixms;
	if (   $line =~ m/(\A|\t) [ ]* $identifier_re [ ]* (\t|\z)/xms
	    && $line =~ m/(\A|\t) [ ]* $name_re       [ ]* (\t|\z)/xms ) {

            # Scan through the column headings for identifiers and coord headings.
            my @line_array = split /\t/, $line;

	    # Feature coordinates.
            foreach my $indexname qw(MetaColumn MetaRow Column Row) {
                my $colnum = get_indexcol( \@line_array, qr/$indexname/i );
                push( @$feature_indices, $colnum ) unless ( $colnum < 0 );
            }

            # 'Reporter ?Identifier'
            foreach my $indexname (
                @{ $CONFIG->get_T2M_INDICES()->{FGEM} } ) {
                my $colnum = get_indexcol( \@line_array, qr/$indexname/i );
                push( @$reporter_indices, $colnum ) unless ( $colnum < 0 );
            }

            # 'Composite ?Sequence ?Identifier'
            foreach my $indexname (
                @{ $CONFIG->get_T2M_INDICES()->{FGEM_CS} } ) {
                my $colnum = get_indexcol( \@line_array, qr/$indexname/i );
                push( @$compseq_indices, $colnum ) unless ( $colnum < 0 );
            }

            last ADF_HEADER_LINE
                if ( ( $#$feature_indices == 3 )
                || @$reporter_indices
                || @$compseq_indices );    # Require one of these
        }
    }

    unless ( @$reporter_indices || @$compseq_indices ) {
        print STDOUT (
            "Error: ADF does not contain reporter or composite sequence identifiers! ",
            "Combined data matrix file checking will not work properly.\n"
        );
    }

    # Set up our identifier prefix regexps. Either use a supplied
    # prefix, or default to using MIAMExpress-style prefixes.
    my $reporter_prefix_regexp
	= defined($reporter_prefix) ? qr/\A $reporter_prefix/xms
	: defined($accno)           ? qr/\A (ebi.ac.uk:MIAMExpress:Reporter:$accno\.|R:$accno:)/xms
	: undef;
    my $compseq_prefix_regexp
	= defined($compseq_prefix)  ? qr/\A $compseq_prefix/xms
        : defined($accno)           ? qr/\A (ebi.ac.uk:MIAMExpress:CompositeSequence:$accno\.|CS:$accno:)/xms
	: undef;

    # Then process the remainder of the adf lines.  %design_elements
    # is a hash of (potentially) three sub-hashes, themselves with
    # keys composed of feature coords, reporter ids or compseq
    # ids.
    my (%design_elements);
    while ( my $line = <$adf_filehandle> ) {
        chomp $line;

	# split is faster than a regexp.
        my @line_array = split /\t/, $line, -1;

        # Features
        if (@$feature_indices) {

            my @coords;
            foreach my $index (@$feature_indices) {
                push( @coords, $line_array[$index] );
            }
            my $key = join( ".", @coords );
            $design_elements{adf_features}{$key}++;
        }

        # Reporters (one column only)
        if (   @$reporter_indices == 1
            && $line_array[ $reporter_indices->[0] ] ) {

            my $key = $line_array[ $reporter_indices->[0] ];

            if ( defined($reporter_prefix_regexp) ) {
                $key =~ s/$reporter_prefix_regexp//;
            }

            $design_elements{adf_reporters}{$key}++;
        }

        # Composite Sequences (one column only)
        if (   @$compseq_indices == 1
	    && $line_array[ $compseq_indices->[0] ] ) {

            my $key = $line_array[ $compseq_indices->[0] ];

            if ( defined($compseq_prefix_regexp) ) {
                $key =~ s/$compseq_prefix_regexp//;
            }

            $design_elements{adf_compseqs}{$key}++;
        }
    }

    my $array = ArrayExpress::Datafile::ArrayDesign->new({
	accession => $accno,
    });

    $array->set_features(  $design_elements{adf_features}  || {} );
    $array->set_reporters( $design_elements{adf_reporters} || {} );
    $array->set_compseqs(  $design_elements{adf_compseqs}  || {} );

    return ( $array );
}

sub retrieve_accno_table {

    if ( my $dbh = get_ae_dbh() ) {

	# If possible, connect via DBI to avoid public/private array
	# design issues.
	print STDOUT (
	    "\nConnecting to ArrayExpress database...retrieving accession number table.\n"
	);

	# Select just the fully-loaded array designs (having an entry
	# in PL_LABEL).
	my $sth = $dbh->prepare(<<'QUERY');
select i.id, i.identifier, i.name
from tt_identifiable i,
 tt_physicalarraydesign a,
 pl_label l
where i.id=a.id
and i.id=l.mainobj_id
QUERY

	$sth->execute()
	    or croak("Error retrieving array list from database: " . $sth->errstr);

	while ( my $result = $sth->fetchrow_arrayref() ) {
	    push @AE_ARRAY_ACCNO_TABLE, join("\t", map { $_ || q{} } @{ $result } );
	}
    }
    else {

	# Fall back to web access. Note that private arrays are
	# unavailable here.
	print STDOUT (
	    "\nConnecting to ArrayExpress web site...retrieving accession number table.\n"
	);

	my $ua = LWP::UserAgent->new();
	$ua->max_size( $CONFIG->get_MAX_LWP_DOWNLOAD() );

	my $response = $ua->get( $CONFIG->get_AE_ARRAYDESIGN_LIST() );

	unless ( $response->is_success ) {
	    croak(    "Error connecting to ArrayExpress: "
		    . $response->status_line
                    . "\n" );
	}

	# If the database goes down, ArrayExpress can return an error
	# page. We trap this as HTML.
	if ( $response->content =~ m/^\s*<html>/i ) {
	    croak(
		"Error connecting to ArrayExpress (web query returned HTML, not text).\n"
	    );
	}

	@AE_ARRAY_ACCNO_TABLE = split /\n/, $response->content;    # split into rows
    }

    return;
}

sub get_ae_array_details {

    my ( $args ) = @_;

    my $accession = $args->{'accession'};
    my $db_id     = $args->{'database_id'};
    my $array_name;

    unless ( $accession || $db_id ) {
        croak("Error: Neither accession nor database ID given for array design.");
    }

    # Only get this once; we use the @AE_ARRAY_ACCNO_TABLE global to
    # store the array design id -> accno -> name mapping.
    unless ( scalar @AE_ARRAY_ACCNO_TABLE ) {    # Should contain at least one row!
	retrieve_accno_table();
    }

    AE_ACCNO_LINE:
    foreach my $line ( @AE_ARRAY_ACCNO_TABLE ) {
        next AE_ACCNO_LINE if ( $line =~ m/^\s*$/ );       # skip empty lines

        my @line_array = split /\t/, $line;
        if ( $db_id && $line_array[0] == $db_id ) {
            $accession  = $line_array[1];
            $array_name = $line_array[2];
            last AE_ACCNO_LINE;
        }
        if ( $accession && $line_array[1] eq $accession ) {
            $db_id      = $line_array[0];
            $array_name = $line_array[2];
            last AE_ACCNO_LINE;
        }
    }

    {
	# Hide the exceptions from our sig handler (but set $@).
	my $sighandler = $SIG{__DIE__};
	delete $SIG{__DIE__};
	unless ( $accession && $db_id ) {
	    if ( $db_id ) {
		die(
		     q{Error: Could not find array accession number }
		  . qq{for database identifier "$db_id" in ArrayExpress.\n}
	        );
	    }
	    if ( $accession ) {
		die(
		     q{Error: Could not find array database identifier }
		  . qq{for accession number "$accession" in ArrayExpress.\n}
	        );
	    }
	    die(
		"Error: Neither accession number nor database identifier "
	      . "for array found in ArrayExpress.\n"
	    );
	}
	$SIG{__DIE__} = $sighandler;
    }

    return wantarray ?
      ($accession, $db_id, $array_name) : $accession;
}

sub retrieve_AE_adf {

    my ( $db_id, $accession, $error_fh, $skip_adf_download ) = @_;

    unless ( $accession || $db_id ) {
        carp(
            "ERROR: Neither array accession nor AE database identifier available "
                . "for at least one array.\n" );
        return;
    }

    openhandle($error_fh) or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $array_name;

    ( $accession, $db_id, $array_name )
        = get_ae_array_details({ accession   => $accession,
				 database_id => $db_id, });

    if ( $array_name && $array_name =~ m/Agilent/i ) {
        print $error_fh (
            "Warning: Agilent array design used: $array_name. "
	  . "The orientation of the array may be incorrect.\n"
        );
    }

    # Get a tab-delimited ADF from ArrayExpress, unless we've been
    # told not to (e.g. we're skipping the data file checks):
    my $temporary_adf_filehandle;
    if ( $skip_adf_download ) {
	print STDOUT (
	    "Skipping ArrayExpress ADF download.\n"
	);
    }
    else {
	print STDOUT (
	    "Connecting to ArrayExpress..."
	  . "retrieving ADF for Accession Number $accession.\n"
	);

	my $ua = LWP::UserAgent->new();
	$ua->max_size( $CONFIG->get_MAX_LWP_DOWNLOAD() );

	my $response = $ua->get( $CONFIG->get_AE_RETRIEVE_ADF() . $db_id );

	unless ( $response->is_success ) {
	    croak(    "Error retrieving ADF from ArrayExpress: "
			  . $response->status_line
			      . "\n" );
	}

	# put it into a temporary filehandle, unless it was too big to download
	$temporary_adf_filehandle = IO::File->new_tmpfile;
	if ( $response->headers()->header('Client-Aborted') ) {
	    warn(
		"Warning: ADF size exceeds download limit. Feature parsing will be skipped.\n"
	    );
	}
	else {
	    print $temporary_adf_filehandle ( $response->content );
	    seek $temporary_adf_filehandle, 0,
		0;    # reset the filehandle for reading
	}
    }
    
    return wantarray
	? ( $temporary_adf_filehandle, $accession )
	    : $temporary_adf_filehandle;

}

sub retrieve_AE_featurelist {

    my ($accession) = @_;

    $accession or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # Get feature IDs from ArrayExpress:
    print STDOUT (
        "Attempting to connect to ArrayExpress...retrieving feature identifiers for Array accession $accession.\n"
    );

    my $ua = LWP::UserAgent->new();
    $ua->max_size( $CONFIG->get_MAX_LWP_DOWNLOAD() );

    my $response
        = $ua->get( $CONFIG->get_AE_RETRIEVE_FEATURELIST() . $accession );

    unless ( $response->is_success ) {
        croak(    "Error retrieving feature list from ArrayExpress: "
                . $response->status_line
                . "\n" );
    }

    my $coords_to_identifiers;    # undef has a meaning here
    if ( $response->headers()->header('Client-Aborted') ) {
        warn("Warning: Feature table size exceeds download limit.\n");
    }

    # HTML or empty string signifies an error.
    elsif ( $response->content && ( $response->content !~ m/<html>/i ) ) {
        %$coords_to_identifiers = map {
            my ( @coords, $identifier );
            ( @coords[ 0 .. 3 ], $identifier ) = ( split /\t/ );
            join( '.', @coords ) => $identifier;
        } split /[\r\n]+/, $response->content;
    }

    return $coords_to_identifiers;

}

sub arrayaccession_in_aedw {

    # Given an array accession, query the DW and return the number of
    # rows that match (i.e. zero if not loaded).

    my ($accession) = @_;

    my $dbh = get_aedw_dbh();

    unless ($dbh) {
	warn("AEDW database handle not available; skipping array check.\n");
	return;
    }

    my $sth = $dbh->prepare(<<'QUERY');
select distinct * from AE2__ARRAYDESIGN
where ARRAYDESIGN_ACCESSION=?
QUERY

    $sth->execute($accession);

    my $count = scalar @{ $sth->fetchall_arrayref() };

    $sth->finish();

    return $count;
}

1;
