#!/usr/bin/env perl
#
# Superclass to provide basic logging facilities to
# ExperimentChecker.pm and Tab2MAGE.pm.
#
# Tim Rayner 2006, ArrayExpress team, European Bioinformatics Institute
#
# $Id$
#

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: ExperimentChecker.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Curator::Logger

=head1 SYNOPSIS

 use base qw/ArrayExpress::Curator::Logger/;

=head1 DESCRIPTION

This module is subclassed by ExperimentChecker and Tab2MAGE classes to
provide them both with logging and error recording facilities.

=head1 OPTIONS

The objects created by this module can be instantiated with the
following options.

=over 2

=item C<log_to_current_dir>

A flag indicating whether to write log files to the current directory
or not. Note that this has been overloaded such that any true value is
now interpreted as the name of a directory into which to write.

=item C<clobber>

A flag indicating whether to silently overwrite old logs or not.

=item C<progname>

A program name string, used in log headers.

=item C<version>

A version string, used in log headers.

=back

=head1 PUBLIC METHODS

=over 2

=item C<add_error( $error )>

Takes an integer, bitwise ORs it with the object's internally-stored
error, records and returns the result. Used to store the overall error
code determining suitability for MAGE-ML export. The final value is
accessible using get_error().

=item C<logfiles( $type )>

Given the desired log file type, returns the desired filename
template. Otherwise, returns a list of log file types.

=item C<log_fh( $type )>

Given the desired log file type, returns the opened filehandle.

=item C<logprint( $type, $message )>

Given the desired log file type and a message, prints the message to
that log file. Linebreaks are not added to the output.

=item C<logprint_line( $type, $message )>

Given the desired log file type and a message, prints the message as
part of a section-delimiting line in the log file. In this case
linebreaks are added to the output.

=item C<localize_logfiles( $args )>

Given an optional hashref argument (keys: "volume", "directory",
"name"), attempts to localize the logfiles to the directory structure
given. Overridden by the log_to_current_dir attribute when setting log
file location.

=back

=head1 AVAILABLE LOG TYPES AND TEMPLATES

=over 2

=item error    => 'expt_error.log',

=item report   => 'expt_report.log',

=item workflow => 'expt_biomaterials.log',

=item feature  => 'expt_feature.log',

=item columns  => 'expt_columnheadings.log',

=item sample   => 'expt_samples.log',

=item protocol => 'expt_protocols.log',

=item miame    => 'expt_miame.log',

=item aedw     => 'expt_aedw.log',

=item tab2mage => 'tab2mage.log',

=item magetab  => 'magetab.log',

=back

=head1 SEE ALSO

=over 1

=item L<ArrayExpress::Curator::ExperimentChecker>

=item L<ArrayExpress::Curator::Tab2MAGE>

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2008.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

package ArrayExpress::Curator::Logger;

use strict;
use warnings;

use Carp;
use File::Spec;
use File::Copy;
use Class::Std;
use Readonly;

use ArrayExpress::Curator::Common qw(date_now);
use ArrayExpress::Curator::Config qw($CONFIG);

my (%logfiles, %log_fh) : ATTRS;

# Attributes available for initialization.
my %log_to_current_dir : ATTR( :get<log_to_current_dir>, :init_arg<log_to_current_dir>, :default<undef> );
my %clobber            : ATTR( :name<clobber>,  :default<undef> );
my %progname           : ATTR( :name<progname>, :default<undef> );
my %version            : ATTR( :name<version>,  :default<undef> );

# Accumulative accessors; each of these has an "add_" method defined below.
my %error              : ATTR( :get<error>,     :default<0>  );

Readonly my $REPORT_WIDTH => 80;

sub add_error {

    # Takes an integer, bitwise ORs it with $error{ident $self},
    # stores and returns the result. Used to store the overall error
    # code determining suitability for MAGE-ML export.

    my ( $self, $err ) = @_;

    $error{ident $self} ||= 0;
    if ($err) { $error{ident $self} |= $err }

    return $error{ident $self};
}

sub logfiles {

    # Given an argument, returns the desired filename
    # template. Otherwise, returns a list of log file types.

    my ( $self, $wanted ) = @_;

    # Log files. The following are just templates. The files will be
    # written in the top-level directory for the submission unles the -p
    # command-line option is supplied.
    my %filenames = (
        error    => 'expt_error.log',
        report   => 'expt_report.log',
        workflow => 'expt_biomaterials.log',
        feature  => 'expt_feature.log',
        columns  => 'expt_columnheadings.log',
        sample   => 'expt_samples.log',
        protocol => 'expt_protocols.log',
        miame    => 'expt_miame.log',
        aedw     => 'expt_aedw.log',
        tab2mage => 'tab2mage.log',
        magetab  => 'magetab.log',
    );

    # Fill in any blanks in our logfile list
    @filenames{ keys %{ $logfiles{ident $self} } }
	= values %{ $logfiles{ident $self} };

    $logfiles{ident $self} = \%filenames;

    if ($wanted) {
        unless ( $logfiles{ident $self}{$wanted} ) {
            croak(
                qq{Error: Log file of type "$wanted" has no predefined filename.\n}
            );
        }
        else {
            return $logfiles{ident $self}{$wanted};
        }
    }
    else {
        return [ keys %{ $logfiles{ident $self} } ];
    }
}

sub log_fh {

    # Requires a log file type argument, returns the opened filehandle.

    my ( $self, $wanted ) = @_;

    $wanted or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    $log_fh{ident $self} ||= {};

    unless ( $log_fh{ident $self}{$wanted} ) {

        # Some of our log files we prefer without a header. This is
        # especially true if the file is normally empty (file size 0) and
        # only written to upon error.
        my %no_headers = ( columns => 1, feature => 1 );

        my @open_params =
            $no_headers{$wanted}
            ? ()
            : ( $self->get_progname(), $self->get_version() );

        my $filename = $self->logfiles($wanted)
            or croak(qq{Error: Undefined log file type "$wanted".\n});

        my $fh = $self->open_log_file( $filename, @open_params );

        croak("Error: File $filename exists. User canceled script execution.")
            unless $fh;

        $log_fh{ident $self}{$wanted} = $fh;
    }

    return $log_fh{ident $self}{$wanted};

}

sub logprint {

    my ( $self, $type, @messages ) = @_;

    print { $self->log_fh($type) }(@messages);

    return;
}

sub logprint_line {

    my ( $self, $type, @messages ) = @_;

    my $message = join(q{}, @messages);
    my $line = $message
	? q{---} . " $message "
	    . (q{-} x ($REPORT_WIDTH - (length($message) + 5)))
	: q{-} x $REPORT_WIDTH;
    $self->logprint($type, "$line\n");  

    return;
}

sub change_logfilename : PRIVATE {

    my ( $self, $type, $filename ) = @_;

    $type     or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    $filename or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    $logfiles{ident $self}{$type} = $filename;

    return;
}

sub localize_logfiles {

    my ( $self, $args ) = @_;

    $args ||= {};

    # Localize to current directory unless told otherwise.
    $args->{directory} ||= q{.};

    # Only change the logfile names (not the whole path) if we want
    # them in the current working directory anyway.
    if ( defined($self->get_log_to_current_dir) ) {
        $args->{directory} = $self->get_log_to_current_dir || q{.};
    }

    foreach my $logtype ( @{ $self->logfiles } ) {

	# Insert string into each logfile name.
	my $filename = $self->logfiles($logtype);
	if ( $args->{name} ) {
	    $filename =~ s/(expt\_)(.*.log)/$1$args->{name}\_$2/;
	}
	$self->change_logfilename(
	    $logtype,
	    File::Spec->catpath(
		$args->{volume},
		$args->{directory},
		$filename
	    )
	);
    }

    return;
}

sub open_log_file : PRIVATE {
    my ( $self, $file, $programname, $version ) = @_;

    # Test for pre-existing file, act as necessary.
    if ( -e $file ) {

        # Handle the clobber option.
        unless ( $self->get_clobber() ) {
            print STDERR ( "\n\nFile \'$file\' aleady exists. "
                    . "Overwrite? [Y(es)\/N(o)\/A(ll)]\n" );
            chomp( my $answer = lc <STDIN> );

            SWITCH:
            {
                $answer eq 'y' && do { last SWITCH; };
                $answer eq 'a' && do { $self->set_clobber(1); last SWITCH; };
                return;
            }
        }

        # Create a backup of the previous file (delete old backup first).
	if ( -e "$file.bak" ) {
	    unlink "$file.bak" or die(
		"Error: Could not overwrite old backup log file $file.bak: $!\n"
	    );
	}
        copy( $file, "$file.bak" )
            or die("Error: Could not create backup log file $file.bak: $!\n");
    }
    my $filehandle = IO::File->new( $file, '>' )
        or croak("Error: Could not open file $file: $!\n");

    # Set logs for unbuffered output (I'm tired of waiting...)
    $filehandle->autoflush();

    # Print a header
    if ( defined($version) ) {
        my $title = "$programname version $version log file output "
	    . date_now();
        print $filehandle (
            "$title\n" . ( "=" x ( length($title) ) ) . "\n\n" );
    }

    return ( $filehandle );
}

1;
