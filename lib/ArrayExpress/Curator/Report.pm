#!/usr/bin/env perl
#
# Report.pm - a module derived from and used in the experiment
# checker script. Contains routines which might be useful elsewhere.
#
# Tim Rayner 2004 ArrayExpress team, EBI
#
# $Id: Report.pm 1939 2008-02-10 23:29:47Z tfrayner $
#

package ArrayExpress::Curator::Report;

use strict;
use warnings;

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../index.html">
                <img src="../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: Reporter.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Curator::Reporter.pm - a module providing some 
functions useful in generating reports.

=head1 SYNOPSIS

 use ArrayExpress::Curator::Report qw(datafile_consistency_table);
 
 print datafile_consistency_table( \@filelist );
 
=head1 DESCRIPTION

This is a simple module providing utility functions and regexp
patterns used elsewhere in the Tab2MAGE package.

=head1 FUNCTIONS

=over 2

=item C<datafile_consistency_table( \@filelist )>

Given an arrayref of Datafile objects (see L<ArrayExpress::Datafile>),
returns a string representing a summary of files tabulated by
hyb_identifier, reporting on presence or absence in the filesystem.

=item C<biomaterials_report( $HoH, $title, $fh )>

Given a nested hashref of hashrefs (of indeterminate depth), a report
title and an output filehandle, prints a report giving links between
nodes in the hashref structure (recursively).

=item C<report_accumulated_errors( $HoH, $fh, $string )>

Given a hashref of hashrefs keyed by filename and then by elements
(e.g. duplicate feature identifiers), an output filehandle and an
explanatory string, print a report.

=item C<format_description( $text, $fh )>

Takes a text string and an output filehandle, and prints the string in
lines of less than 80 chars, splitting words on whitespace.

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2008.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

use ArrayExpress::Curator::Config qw($CONFIG);

use base 'Exporter';
our @EXPORT_OK = qw(
    datafile_consistency_table
    biomaterials_report
    report_accumulated_errors
    format_description
);

sub get_max_colwidth {

    my ( $hashref, $previousmax, $lookup ) = @_;

    my $col_width = 0;
    foreach my $key ( keys %$hashref ) {
        my $keylength = length( $lookup->{$key} );
        $col_width = $keylength if ( $keylength > $col_width );
    }

    $col_width = $previousmax
        if ( $previousmax && $previousmax > $col_width );

    return ($col_width);

}

sub _recurse_heirarchy {

    # Prints a report given a ref to a HoHoHoH of indeterminate
    # depth. Deals only with the hash keys. The terminal hash values
    # _must_ be zero or undef to finish the recursion properly. Note
    # that this function is recursive, and as such is a little hairy in
    # places.

    my ( $HoH, $widths, $depth ) = @_;

    # Start at depth 0, recurse to depth $#$widths.
    $depth ||= 0;

    my $col_width = $widths->[$depth];

    # Define the column format.
    my $col_format = "%" . "${col_width}.${col_width}s";

    # Initialise $lines (must not be undefined).
    my $lines = q{};

    my @termlist = tied(%$HoH) ? keys %$HoH : sort keys %$HoH;

    foreach my $term (@termlist) {

        my $lines_ending = substr( $lines, length($lines) - 1 );

        if ( $depth && $lines_ending eq "\n" ) {

            # If we're at depth, and we've just finished a line;
            # -- insert some spacers,
            for ( my $i = 0; $i < $depth - 1; $i++ ) {
                my $col_format
                    = "%" . $widths->[$i] . "." . $widths->[$i] . "s";
                my $spacer = sprintf( "$col_format     ", q{} );
                $lines .= $spacer;
            }

            # -- and an arrow,
            my $i          = $depth - 1;
            my $col_format = "%" . $widths->[$i] . "." . $widths->[$i] . "s";
            my $arrow      = sprintf( "$col_format \\-> ", q{} );
            $lines .= $arrow;
        }

        $lines .= sprintf( $col_format, $term );

        if ( $HoH->{$term} ) {

            # If we haven't reached the bottom of the HoHoHoH...
            # -- append a mapping arrow.
            $lines .= " --> ";

            # Recurse to the next depth in the HoHoHoH
            $lines
                .= _recurse_heirarchy( $HoH->{$term}, $widths, $depth + 1 );

        }
        else {

            $lines .= "\n";

        }
    }
    return ($lines);
}

sub _collect_widths {

    my ( $HoH, $depth ) = @_;

    $depth ||= 0;
    my @width;

    # Figure out column width at this depth
    $width[$depth] ||= 0;
    foreach my $key ( keys %$HoH ) {
        my $keylength = length($key);
        $width[$depth] = $keylength if ( $keylength > $width[$depth] );
    }

    foreach my $term ( keys %$HoH ) {
        if ( $HoH->{$term} ) {

            # If we haven't reached the bottom of the HoHoHoH, recurse to
            # the next level down.
            my $subwidth = _collect_widths( $HoH->{$term}, $depth + 1 );

            RECURSION_DEPTH:
            for ( my $i = 0; $i <= $#$subwidth; $i++ ) {

                next RECURSION_DEPTH unless defined( $subwidth->[$i] );

                # If this width is greater than that in the top-level array,
                # modify the top-level array accordingly.
                if ( !defined( $width[$i] )
                    || $subwidth->[$i] > $width[$i] ) {
                    $width[$i] = $subwidth->[$i];
                }
            }
        }
    }
    return \@width;

}

sub format_report {

    # NB. Normally this sub is passed a hashref tied to Tie::IxHash to
    # preserve creation order (ordered by SYSUID). Untied hashes are
    # sorted by their keys (typically ID). This is implemented in
    # _recurse_heirarchy.

    my ($HoH) = @_;

    # Populate an arrayref:
    # $widths->[$depth] = $column_width where $depth is (0..$levels-1)
    my $widths = _collect_widths($HoH);

    # Use the widths information to format a report string
    my $lines = _recurse_heirarchy( $HoH, $widths );

    return $lines;

}

sub biomaterials_report {

    # Prints the workflow reports

    my ( $report_data, $title, $workflow_fh ) = @_;

    # if filehandle unset, use STDOUT
    $workflow_fh =
          $workflow_fh
        ? $workflow_fh
        : \*STDOUT;

    print $workflow_fh ("\n");
    print $workflow_fh ( "#" x ( length($title) + 4 ) . "\n" );
    print $workflow_fh ("# $title #\n");
    print $workflow_fh ( "#" x ( length($title) + 4 ) . "\n" );
    print $workflow_fh ("\n");

    print $workflow_fh ( format_report($report_data) );

    print $workflow_fh ("\n");

    return;
}

sub draw_table_line {

    # Draws a horizontal table line given the width of the first column,
    # the number of other columns, and their width.

    my ( $first_colwidth, $colwidth, $datacolno ) = @_;
    my $lines = q{};

    $lines .= "+" . ( "-" x ( $first_colwidth + 2 ) ) . "+";
    $lines .= ( ( "-" x ( $colwidth + 2 ) ) . "+" ) x $datacolno;
    $lines .= "\n";

    return $lines;
}

sub datafile_consistency_table {

    # Takes an array of ArrayExpress::Datafile objects.
    # Returns a string containing a table ready to print, listing the
    # files associated with the experiment and any missing files.
    my $filelist = shift;

    my $lines = q{};

    # Skip if no files in the list.
    return $lines unless ( scalar @$filelist );

    # Get a list of hybs mapped to file types;
    # get a list of extensions.
    my $filetypes;
    my $extensions;

    DATA_FILE:
    foreach my $file (@$filelist) {

        next DATA_FILE
            if ( $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE() );

        ( my $extension ) = ( $file->get_path() =~ m/\.([^\.]*)$/ );
	$extension ||= 'UNKNOWN';
        $extension =~ tr/a-z/A-Z/;
        if ( -f $file->get_path() ) {

            # everything's rosy
            $filetypes->{ $file->get_hyb_identifier() }{$extension} = 1;
        }
        else {

            # in the DB, not found on disk
            $filetypes->{ $file->get_hyb_identifier() }{$extension} = -1;
        }
        $extensions->{$extension} = 1;
    }

    # Figure out our column widths
    my $colwidth  = 12;
    my $colformat = " %" . $colwidth . "." . $colwidth . "s |";

    my $hybwidth = 0;
    foreach my $hyb_id ( keys %$filetypes ) {
        my $hyblength = length($hyb_id);
        $hybwidth = $hyblength if ( $hyblength > $hybwidth );
    }
    my $hybformat = " %" . $hybwidth . "." . $hybwidth . "s |";

    my $num_exts = scalar( grep { defined $_ } values %$extensions );

    # Start drawing the table;
    # header lines...
    $lines .= &draw_table_line( $hybwidth, $colwidth, $num_exts );
    $lines .= sprintf( "|$hybformat", "hyb \\ file extension" );
    foreach my $ext ( sort keys %$extensions ) {
        $lines .= sprintf( $colformat, $ext );
    }
    $lines .= "\n";
    $lines .= &draw_table_line( $hybwidth, $colwidth, $num_exts );

    # table content...
    foreach my $hyb_id ( sort keys %$filetypes ) {
        $lines .= "|" . sprintf( $hybformat, $hyb_id );
        foreach my $extension ( sort keys %$extensions ) {

            SWITCH_BY_FILE_EXTENSION:
            {
                !$filetypes->{$hyb_id}{$extension} && do {
                    $lines .= sprintf( $colformat, "NOT UPLOADED" );
                    last SWITCH_BY_FILE_EXTENSION;
                };
                $filetypes->{$hyb_id}{$extension} == 1 && do {
                    $lines .= sprintf( $colformat, "ok" );
                    last SWITCH_BY_FILE_EXTENSION;
                };
                $filetypes->{$hyb_id}{$extension} == -1 && do {
                    $lines .= sprintf( $colformat, "FILE LOST" );
                    last SWITCH_BY_FILE_EXTENSION;
                };
            }
        }
        $lines .= "\n";
    }

    # final line...
    $lines .= &draw_table_line( $hybwidth, $colwidth, $num_exts );
    $lines .= "\n";

    return $lines;
}

sub report_accumulated_errors {

    my ( $bad, $output_fh, $error_string ) = @_;
    my $errorcols;

    foreach my $filename ( keys %$bad ) {

        my $arraystring = join( "\n", sort @{ $bad->{$filename} } );
        push( @{ $errorcols->{$arraystring} }, $filename );

    }

    foreach my $arraystring ( keys %$errorcols ) {
        print $output_fh (
            "$error_string\n\n",
            $arraystring,
            "\n\n were found in files:\n\n\t",
            join( "\n\t", @{ $errorcols->{$arraystring} } ),
            "\n\n-----------------------------------------------\n\n"
        );
    }

    return;
}

sub format_description {

    # Takes a text string and a filehandle, and outputs the string in
    # lines of less than 80 chars, splitting words on whitespace.

    my ( $text, $fh ) = @_;

    my $pagewidth = 80;

    # Remove newlines and tabs, and split on spaces
    $text =~ s/\s+/ /g;
    my @words = split /[ ]+/, $text;

    # Build lines of <= $pagewidth (80) chars and print them.
    while (@words) {
        my $line = shift(@words);
        while ( @words && ( length( $line . $words[0] ) < $pagewidth ) ) {
            $line .= " " . shift(@words);
        }
        print $fh "$line\n";
    }

    return;
}

1;
