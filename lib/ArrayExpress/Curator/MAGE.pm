#!/usr/bin/env perl
#
# Module to provide basic access to MAGE objects and MAGE-ML files
#
# Tim Rayner 2004, ArrayExpress team, European Bioinformatics Institute
#
# $Id: MAGE.pm 2069 2008-06-04 14:33:52Z tfrayner $
#

package ArrayExpress::Curator::MAGE;

use strict;
use warnings;

use Carp;
use Bio::MAGE qw(:ALL);

# Used for checking FVs (Value vs. Measurement)
use Scalar::Util qw(looks_like_number);
use List::Util qw(first);
use English qw( -no_match_vars );

use ArrayExpress::Curator::Common qw(decamelize);

use ArrayExpress::Curator::MAGE::Definitions qw(
    $OE_CAT_ACTION
    $OE_CAT_BIOSAMPLETYPE
    $OE_CAT_MATERIALTYPE
    $OE_CAT_PROTOCOLTYPE
    $OE_CAT_SOFTWARETYPE
    $OE_CAT_HARDWARETYPE
    $OE_CAT_DERIVEDBIOASSAYTYPE
    $OE_CAT_EXPERIMENTALFACTORCATEGORY
    $OE_CAT_DATATYPE
    $OE_CAT_SCALE
    $OE_CAT_RELEASEDATE
    $OE_CAT_SUBMISSIONDATE
    $OE_CAT_ROLE
    $OE_VAL_SYNTHETICDNA
    $OE_VAL_TOTALRNA
    $OE_VAL_GENOMICDNA
    $OE_VAL_WHOLEORGANISM
    $OE_VAL_ORGANISMPART
    $OE_VAL_EXTRACT
    $OE_VAL_NOTEXTRACT
    $OE_VAL_POOL
    $OE_VAL_SPECIFIEDBIOMATERIALACTION
    $OE_VAL_NUCLEICACIDEXTRACTION
    $OE_VAL_IMMUNOPRECIPITATE
    $OE_VAL_LABELING
    $OE_VAL_FLOAT
    $OE_VAL_UNKNOWN
    $OE_VAL_LINEARSCALE
    $AE_LABELCOMPOUND_PREFIX
    $AE_CHANNEL_PREFIX
    $MAGE_UNITS
);

use base 'Exporter';
our @EXPORT_OK = qw(
    read_mage
    write_mage
    unique_identifier
    postprocess_mage
    make_container
    new_ontologyentry
    new_person
    new_biosource
    new_biosample
    new_extract
    new_immunoprecipitate
    new_labeledextract
    new_labelcompound
    new_image
    new_hybridization
    new_scan
    new_physicalbioassay
    new_featureextraction
    new_measuredbioassay
    new_measuredbioassaydata
    new_derivedbioassay
    new_producertransformation
    new_derivedbioassaydata
    new_bioassaymap
    new_quantitationtypemap
    new_protocol
    new_parameter
    new_hardwareapplication
    new_software
    new_hardware
    new_armanuf
    new_array
    new_factorvalue
    new_experimentalfactor
    new_quantitationtype
    new_qtd
    add_summary_stats
    update_factorvalues
);

# Internal identifier counter
my $ID_Count = 0;

#
# make_container
#
# Takes:   coderef to a sub which returns a value to be entered uniquely in the closure hash
#          flag indicating whether unique identifiers should be passed to that coderef
#
# Returns: coderef to a wrapper sub incorporating a closure around the original coderef
#

# We *need* to postpone @_ unpacking here as part of the closure
# creation. We therefore turn off Perl::Critic carping about this:

## no critic RequireArgUnpacking
sub make_container {

    my ( $coderef, $pass_identifier ) = splice( @_, 0, 2 );

    my $hash = {};

    # It's actually important to use @_ in the following anon sub
    # declaration, rather than some copy of whatever arguments were
    # passed into this function at container creation time.
    return
        sub { _container_closure( $hash, $coderef, $pass_identifier, @_ ) };

}
## use critic RequireArgUnpacking

#
# _container_closure
#
# Takes:   hashref for storing values
#          coderef to sub returning those values
#          flag indicating whether to pass the identifier through
#          identifier for the value returned by coderef
#          [list of arguments passed to coderef]
#
# Returns: A unique value returned by coderef and stored in hashref container
#
#    * If no arguments are supplied to be passed to the coderef, *
#    * all the values (objects?) in the container are returned.  *
#
sub _container_closure {

    my ( $hash, $coderef, $pass_identifier, @args ) = @_;

    # Return all the objects (unsorted) if no arguments.
    return [ values %$hash ] unless scalar @args;

    my $identifier = shift @args;

    # Must have an identifier here. Confess for better debugging.
    confess("Internal Error: Undefined local identifier passed to container function.\n")
        unless defined($identifier);

    # If identifier passed but no attributes hashref, just return the
    # desired object without updating it.
    if ( ! scalar @args || ( ref $args[0] ne 'HASH' ) ) {

	if ( $args[0] && $args[0] eq 'delete' ) {

	    # Remove the object from the container.
	    my $rc = defined($hash->{$identifier});
	    delete $hash->{$identifier};

	    return $rc;
	}
	else {

	    # Query lookup for the identified object. This does not
	    # attempt to update the object (pass a hashref in @args to update).
	    return $hash->{$identifier};
	}
    }

    # Add a pre-existing object to the passed arguments if found.
    if ( $hash->{$identifier} ) {
        push( @args, ( $hash->{$identifier} ) );
    }

    # Add back the identifier to @args if required
    unshift( @args, $identifier ) if $pass_identifier;

    # Get a new object, or an updated object if we found an old one
    my @objects = ( $coderef->(@args) );

    # Store the object in the container
    $hash->{$identifier} = $objects[0];

    return wantarray ? @objects : $objects[0];

}

sub unique_identifier {

    return ++$ID_Count;

}

sub read_mage {

    my ( $magepath, $dtd, $verbose ) = @_;

    # Invoke our XML::Reader object
    require Bio::MAGE::XML::Reader;
    my $sax1 = 1;

    #  my $verbose = 5;
    my $logfh         = \*STDERR;
    my $external_data = 1;
    my $reader        = Bio::MAGE::XML::Reader->new(
        sax1          => $sax1,
        verbose       => $verbose,
        log_file      => $logfh,
        external_data => $external_data,
    );

    my $dir = ( File::Spec->splitpath($magepath) )[1];
    my $localdtd
        = $dir ? File::Spec->catfile( $dir, "MAGE-ML.dtd" ) : "MAGE-ML.dtd";

    my $dtd_delete_flag;
    unless ( -e $localdtd ) {
        print STDOUT ("Linking $dtd to $localdtd\n");
        symlink( $dtd, $localdtd )
            or croak("Error symlinking DTD file: $!\n");
        $dtd_delete_flag++;
    }

    # Actually read in the file
    my $mage = $reader->read($magepath);

    # Remove the MAGE-ML.dtd symlink, if we created it
    if ( $dtd_delete_flag and -l $localdtd ) {
        print STDOUT ("Deleting $localdtd symlink.\n");
        unlink($localdtd) or croak("Error removing $localdtd symlink: $!\n");
    }
    return $mage;
}

sub write_mage {

    my ( $mage, $fh, $dataformat, $version ) = @_;

    require Bio::MAGE::XML::Writer;
    $version    ||= "1.1";
    $dataformat ||= "tab delimited";

    my $writer = Bio::MAGE::XML::Writer->new(
        fh              => $fh,
        external_data   => 1,
        data_format     => $dataformat,
        cube_holds_path => 1,
        public_id       => "-//OMG//DTD MAGE-ML $version//EN",
    );
    $writer->write($mage);

    return;
}

sub postprocess_mage {
    my ( $ded_fh, $old_fh, $new_fh, $dummy_id, $data_format ) = @_;

    foreach my $filehandle ( $ded_fh, $old_fh, $new_fh ) {
        if ($filehandle) {
            seek( $filehandle, 0, 0 )
                or croak("Error rewinding filehandle: $!\n");
        }
    }

    my $error = 0;

    MAGE_LINE:
    while ( my $line = <$old_fh> ) {

        # Skip over any dummy FeatureDimensions we've been told about
        if (   $dummy_id
            && $line =~ m!<FeatureDimension identifier="$dummy_id"! ) {
            while ( my $skipped = <$old_fh> ) {
                next MAGE_LINE if ( $skipped =~ m!</FeatureDimension>! );
            }
        }

        # Insert our DEDs just before the close tag
        if ( $ded_fh && $line =~ m!</DesignElementDimension_assnlist>! ) {
            while ( my $fdline = <$ded_fh> ) {
                print $new_fh $fdline or $error++;
            }
        }

        if ( my ($filename)
            = ( $line =~ m/DataExternal filenameURI="([^\"]+)"/ ) ) {
            if ( $data_format->{$filename} ) {
                until ( $line
                        =~ s/dataFormat="[^\"]+"/q{dataFormat="}.$data_format->{$filename}.q{"}/e
                    ) {
                    print $new_fh $line;
                    $line = <$old_fh>;
                }
            }
        }

        print $new_fh $line or $error++;
    }

    return !$error;

}

sub new_ontologyentry {

    my ($args) = @_;

    my $obj = Bio::MAGE::Description::OntologyEntry->new(
        category => $args->{category},
        value    => $args->{value}
    );

    # OntologyReference
    ( $args->{database_ref} && $args->{database_accession} ) && do {
        my $databaseentry = Bio::MAGE::Description::DatabaseEntry->new(
            database  => $args->{database_ref},
            accession => $args->{database_accession}
        );
        $obj->setOntologyReference($databaseentry);
    };

    return $obj;

}

sub _parse_person_name {

    my $namestring = shift;

    # Parse out the first and last names, and middle initials.
    # N.B. this parsing isn't perfect, but will deal with things
    # like "Mary Ann F.X. van Doone"

    # Strip surrounding whitespace:
    $namestring =~ s/\s*(.*?)\s*/$1/;

    # Allow "Rayner, Tim F." form:
    $namestring =~ s/(.*)\s*\,\s*(.*)/$2 $1/;
    my @name_elements = split /[\.\s]+/, $namestring;

    foreach my $part (@name_elements) { $part =~ s/[\. ]//g; }

    my $last  = pop @name_elements;
    my $first = shift @name_elements;

    NAME_PART:
    while ( my $part = shift(@name_elements) ) {

        # Hopefully this will take care of van Whatever
        unless ( ( length($part) == 1 ) || ( $part eq lc($part) ) ) {
            $first = "$first $part";
        }
        else {
            unshift( @name_elements, $part );
            last NAME_PART;
        }
    }

    NAME_PART:
    while ( my $part = pop(@name_elements) ) {
        unless ( length($part) == 1 ) {
            $last = "$part $last";
        }
        else {
            push( @name_elements, $part );
            last NAME_PART;
        }
    }

    my $midinitials;
    while ( my $part = shift(@name_elements) ) {
        $midinitials .= "$part.";
    }

    return ( $first, $midinitials, $last );

}

sub new_person {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::AuditAndSecurity::Person->new unless $obj;

    # Take the name if given, otherwise just the email address;
    # otherwise croak.
    my $name_id;
    my ( $first, $midinitials, $last );
    if ( $args->{name} ) {
	( $first, $midinitials, $last ) = _parse_person_name($args->{name});
	$name_id = "$first";
	$name_id .= " $midinitials" if $midinitials;
	$name_id .= " $last";
    }
    elsif ( $args->{email} ) {
	$name_id = $args->{email};
    }
    else {
	croak("Error: Neither person name nor email address set.");
    }

    # Identify the Person object (limit name part of identifier to 50
    # chars for AE database column safety).
    my $identifier = sprintf(
	"%s:%s.Person",
	$args->{namespace},
	substr($name_id, 0, 50),
    );
    $obj->setIdentifier($identifier)
        unless $obj->getIdentifier;

    # Sort out the name if available.
    $last && do {
	$obj->setLastName($last)           unless $obj->getLastName;
    };
    $first && do {
	$obj->setFirstName($first)         unless $obj->getFirstName;
    };
    $midinitials && do {
        $obj->setMidInitials($midinitials) unless $obj->getMidInitials;
    };

    # Email, role and organization.
    $args->{email} && do {
        $obj->setEmail( $args->{email} ) unless $obj->getEmail;
    };
    $args->{role} && do {
        my $found;
        if ( my $preexisting_roles = $obj->getRoles ) {
            my $new_role = $args->{role};
            $found = first { $_->getValue eq $new_role } @$preexisting_roles;
        }
        unless ($found) {
            my $role = new_ontologyentry(
                {   category => $OE_CAT_ROLE,
                    value    => $args->{role}
                }
            );
            $obj->addRoles($role);
        }
    };
    ( $args->{organization} || $args->{address} ) && do {

	# Use just the first 50 characters in identifier creation to
	# avoid AE database column limits.
        my $identifier = sprintf(
	    "%s:%s.Organization",
	    $args->{namespace},
	    substr(( $args->{organization} || $args->{address} ), 0, 50),
	);
        my $organization = Bio::MAGE::AuditAndSecurity::Organization->new(
            identifier => $identifier,
            name       => ( $args->{organization} || $args->{address} ),
            address    => $args->{address},
        );
        $obj->setAffiliation($organization)  unless $obj->getAffiliation;
        $obj->setAddress( $args->{address} ) unless $obj->getAddress;
    };

    return $obj;
}

sub _check_pooling {

    my ( $obj, $protocol, $identifier_template ) = @_;

    return unless ( $obj && $protocol );

    # Count our biomaterials
    my %bmm_hash;
    foreach my $treatment ( @{ $obj->getTreatments() || [] } ) {

        # Check the protocol hasn't already been applied in this treatment set
        my $found;
        if ( my $preexisting_protclapps
            = $treatment->getProtocolApplications ) {
            my $new_identifier = $protocol->getIdentifier;
            $found
                = first { $_->getProtocol()->getIdentifier eq $new_identifier }
                @$preexisting_protclapps;
        }
	return if ( $found );

        # Make a hash of the BioMaterialMeasurements
        if ( my $source_bmms = $treatment->getSourceBioMaterialMeasurements )
        {
            foreach my $bmm (@$source_bmms) {
                $bmm_hash{ $bmm->getBioMaterial()->getIdentifier } = $bmm;
            }
        }
    }

    # Add the new treatment (with each BMM) if more than one BMM
    my $treatments;
    if ( scalar( grep { defined $_ } values %bmm_hash ) > 1 ) {  # More than one BMM
        $treatments = new_treatments(
            {   protocols           => [$protocol],
                identifier_template => $identifier_template,
		default_action      => $OE_VAL_POOL,
            }
        );

        # Correct the order of the treatment.
        foreach my $treatment (@$treatments) {
            $treatment->setOrder(0);    # This appears to be legal
        }

        # Put every BMM in the pooling treatment
        foreach my $bmm ( values %bmm_hash ) {
            $treatments = _update_treatments( $treatments, $bmm );
        }
    }

    return $treatments;
}

sub new_labelcompound {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioMaterial::Compound->new unless $obj;

    $obj->setIdentifier( $AE_LABELCOMPOUND_PREFIX . $args->{dye} )
        unless $obj->getIdentifier;

    $obj->setName( $args->{dye} ) unless $obj->getName;

    if ( $args->{indices} ) {
	foreach my $new ( @{ $args->{indices} } ) {
	    my $found;
	    if ( my $preexisting = $obj->getCompoundIndices() ) {
		my $new_cat = $new->getCategory();
		my $new_val = $new->getValue();
		$found = first {
		    $_->getCategory() eq $new_cat
		    && $_->getValue() eq $new_val
		} @$preexisting;
	    }
	    $obj->addCompoundIndices( $new ) unless $found;
	}
    }

    return ($obj);

}

sub new_labeledextract {

    my ( $name, $args, $obj ) = @_;

    # Set some default values.
    $args->{material_type}  ||= $OE_VAL_SYNTHETICDNA;

    # Create a new object if we don't already have one
    $obj = Bio::MAGE::BioMaterial::LabeledExtract->new unless $obj;

    # Identifier, Name , MaterialType
    my $identifier = "$args->{identifier_template}.LabeledExtract"
        . ( $args->{dye} ? ".$args->{dye}" : q{} );
    my $materialtype = &new_ontologyentry(
        {   category => $OE_CAT_MATERIALTYPE,
            value    => $args->{material_type},
        }
    );

    $obj->setIdentifier($identifier)      unless $obj->getIdentifier;
    $obj->setName($args->{name} || $name) unless $obj->getName;
    $obj->setMaterialType($materialtype)  unless $obj->getMaterialType;

    # Treatments
    if ( $args->{derived_from} ) {
	my $source_bmm
	    = Bio::MAGE::BioMaterial::BioMaterialMeasurement->new(
		bioMaterial => $args->{derived_from},
	    );

	my $treatments = new_treatments(
	    {   source_bmm          => $source_bmm,
		protocols           => $args->{protocols},
		parameters          => $args->{parameters},
		parameter_bag       => $args->{parameter_bag},
		identifier_template => "$args->{identifier_template}."
		    . ( $args->{dye} || q{} ),
		default_action      => $OE_VAL_LABELING,
	    }
	);

	if ( $obj->getTreatments ) {
	    $obj->setTreatments(
		&_update_treatments( $obj->getTreatments, $source_bmm ) );
	}
	else {
	    $obj->setTreatments($treatments);
	}
    }

    # Pooling
    if ( $args->{pooling} ) {
        my $pooling_treatments = _check_pooling(
	    $obj,
	    $args->{pooling},
            $args->{identifier_template},
	);
        $obj->addTreatments(@$pooling_treatments) if $pooling_treatments;
    }

    # Labels
    if ( $args->{dye} ) {

        my $compound = $args->{compound_bag}
            ->( $args->{dye}, { dye => $args->{dye} } );

        my $found;
        if ( my $preexisting_labels = $obj->getLabels ) {
            my $new_identifier = $compound->getIdentifier;
            $found = first { $_->getIdentifier eq $new_identifier }
                @$preexisting_labels;
        }
        $obj->addLabels($compound) unless $found;
    }

    return $obj;

}

sub new_treatments {

    my ($args) = @_;

    my @treatments;

    # Protocols and parameters are both arrays which must be fed to this
    # sub in synchronous order:
    # $protocols = [procotol1, protocol2, protocol3]
    # $parameters = [param set 1, param set 2, param set 3]
    foreach my $protocol ( @{ $args->{protocols} } ) {

        my $ptype       = $protocol->getType;
        my $actionvalue = $ptype->getValue;

        my $action = &new_ontologyentry(
            {   category => $OE_CAT_ACTION,
                value    => $actionvalue,
            }
        );

        my $protocolapplication
            = Bio::MAGE::Protocol::ProtocolApplication->new(
            protocol     => $protocol,
            activityDate => 'n/a',

            # FIXME, (also performers, hardware & software applications)
            );

        $args->{parameters} && do {

            my $param_set = shift( @{ $args->{parameters} } )
                or croak(
                "Internal script error: Insufficient parameter sets for protocol "
                    . $protocol->getIdentifier
                    . ".\n" );

            foreach my $param_id ( sort keys %{$param_set} ) {

                my $parameter = $args->{parameter_bag}->( $param_id );

		unless ( $parameter ) {
		    croak(qq{Internal error: Parameter "$param_id" not defined.\n});
		}

                $protocolapplication->addParameterValues(
                    Bio::MAGE::Protocol::ParameterValue->new(
                        value         => $param_set->{$param_id},
                        parameterType => $parameter
                    )
                );

            }
        };

        my $treatment = Bio::MAGE::BioMaterial::Treatment->new(
            identifier =>
                "$args->{identifier_template}.$actionvalue.Treatment",
            action               => $action,
            protocolApplications => [$protocolapplication],
            order                => ( $#treatments + 2 ),
        );

        $treatment->setSourceBioMaterialMeasurements(
            [ $args->{source_bmm} ] )
            if $args->{source_bmm};

        push( @treatments, $treatment );
    }

    unless ( scalar @treatments ) {

        # We want at least one treatment, even if it doesn't have a
        # protocolapplication, to hang our source_bmm off.

        # Generic action.
	my $action_val = $args->{default_action}
	              || $OE_VAL_SPECIFIEDBIOMATERIALACTION;
        my $action = new_ontologyentry(
            {   category => $OE_CAT_ACTION,
                value    => $action_val,
            }
        );

	# Make sure the identifier is truly unique; otherwise there is
	# the risk of duplicate Treatment objects.
	my $uniq_number = unique_identifier();
        my $treatment = Bio::MAGE::BioMaterial::Treatment->new(
            identifier => "$args->{identifier_template}.$uniq_number.Treatment",
            action     => $action,
            order      => ( $#treatments + 2 ),
        );
        $treatment->setSourceBioMaterialMeasurements(
            [ $args->{source_bmm} ] )
            if $args->{source_bmm};
        push( @treatments, $treatment );
    }

    return \@treatments;

}

sub _update_treatments {    # Used in e.g. pooling

    my ( $treatments, $source_bmm ) = @_;

    if ($source_bmm) {
        foreach my $treatment (@$treatments) {

            my $old_bmms       = $treatment->getSourceBioMaterialMeasurements;
            my $new_identifier = $source_bmm->getBioMaterial()->getIdentifier;
            my $found          = first {
                $_->getBioMaterial()->getIdentifier eq $new_identifier;
                }
                @$old_bmms;

            $treatment->addSourceBioMaterialMeasurements($source_bmm)
                unless $found;
        }
    }

    return $treatments;

}

sub _new_biomaterial_biosample {

    my ( $args, $obj ) = @_;

    # New object if necessary
    $obj = Bio::MAGE::BioMaterial::BioSample->new unless $obj;

    # Identifier, Name
    $obj->setIdentifier( $args->{identifier} ) unless $obj->getIdentifier;
    $obj->setName( $args->{name} )             unless $obj->getName;

    # MaterialType
    my $materialtype = &new_ontologyentry(
        {   category => $OE_CAT_MATERIALTYPE,
            value    => $args->{material_type},
        }
    );
    $obj->setMaterialType($materialtype) unless $obj->getMaterialType;

    # Type
    my $type = &new_ontologyentry(
        {   category => $OE_CAT_BIOSAMPLETYPE,
            value    => $args->{biosample_type},
        }
    );
    $obj->setType($type) unless $obj->getType;

    # Treatments
    if ( $args->{derived_from} ) {
	my $source_bmm
	    = Bio::MAGE::BioMaterial::BioMaterialMeasurement->new(
		bioMaterial => $args->{derived_from},
	    );

	my $treatments = new_treatments(
	    {   source_bmm          => $source_bmm,
		protocols           => $args->{protocols},
		parameters          => $args->{parameters},
		parameter_bag       => $args->{parameter_bag},
		identifier_template => $args->{identifier_template},
		default_action      => $args->{default_action},
	    }
	);

	if ( $obj->getTreatments ) {
	    $obj->setTreatments(
		&_update_treatments( $obj->getTreatments, $source_bmm ) );
	}
	else {
	    $obj->setTreatments($treatments);
	}
    }

    # Pooling
    if ( $args->{pooling} ) {
        my $pooling_treatments = _check_pooling(
	    $obj,
	    $args->{pooling},
            $args->{identifier_template},
	);
        $obj->addTreatments(@$pooling_treatments) if $pooling_treatments;
    }

    return $obj;

}

sub new_biosample {

    my ( $name, $args, $biosample ) = @_;

    $args->{name}          ||= $name;
    $args->{material_type} ||= $OE_VAL_ORGANISMPART; # Set some default values
    $args->{biosample_type} ||= $OE_VAL_NOTEXTRACT;
    $args->{identifier} = "$args->{identifier_template}.Sample";

    return &_new_biomaterial_biosample( $args, $biosample );

}

sub new_extract {

    my ( $name, $args, $extract ) = @_;

    $args->{name}           ||= $name;
    $args->{material_type}  ||= $OE_VAL_TOTALRNA;    # Set some default values
    $args->{biosample_type} ||= $OE_VAL_EXTRACT;
    $args->{default_action} ||= $OE_VAL_NUCLEICACIDEXTRACTION;
    $args->{identifier} = "$args->{identifier_template}.Extract";

    return &_new_biomaterial_biosample( $args, $extract );

}

sub new_immunoprecipitate {

    my ( $name, $args, $ip ) = @_;

    $args->{name}           ||= $name;
    $args->{material_type}  ||= $OE_VAL_GENOMICDNA;  # Set some default values
    $args->{biosample_type} ||= $OE_VAL_EXTRACT;
    $args->{default_action} ||= $OE_VAL_IMMUNOPRECIPITATE;
    $args->{identifier} = "$args->{identifier_template}.Immunoprecipitate";

    return &_new_biomaterial_biosample( $args, $ip );

}

sub new_protocol {

    my ( $identifier, $args, $obj ) = @_;

    $obj = Bio::MAGE::Protocol::Protocol->new unless $obj;

    $obj->setIdentifier($identifier) unless $obj->getIdentifier;

    $args->{protocol_type} && do {
        my $type = &new_ontologyentry(
            {   category => $OE_CAT_PROTOCOLTYPE,
                value    => $args->{protocol_type},
            }
        );
        $obj->setType($type) unless $obj->getType;
    };

    $args->{name}
        && do { $obj->setName( $args->{name} ) unless $obj->getName; };
    $args->{text}
        && do { $obj->setText( $args->{text} ) unless $obj->getText; };

    if ( $args->{parameters} ) {

        # Sort out the parameters
        my @parameters;

        while ( my ( $param_name, $param_hash )
            = each %{ $args->{parameters} } ) {

            push(
                @parameters,
                $args->{parameter_bag}->(
                    $param_hash->{identifier},
                    {   name => $param_name,
                        unit => $param_hash->{unit},
                    }
                )
            );
        }

        $obj->setParameterTypes( \@parameters )
            unless $obj->getParameterTypes;    # FIXME more granular update?

    }

    if ( $args->{software} ) {
	_update_protocol_software( $obj, $args->{software} );
    }
    if ( $args->{hardware} ) {
	_update_protocol_hardware( $obj, $args->{hardware} );
    }

    return $obj;

}

sub new_array {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::Array::Array->new unless $obj;

    # Identifier
    my $identifier = "$args->{identifier_template}.Array";
    $obj->setIdentifier($identifier) unless $obj->getIdentifier();

    # PhysicalArrayDesign
    my $accession;
    if ( my $ad = $args->{arraydesign} ) {
	$obj->setArrayDesign($ad) unless $obj->getArrayDesign();
	$accession = $ad->getIdentifier();
    }
    elsif ( defined( $args->{arrayaccession} ) ) {
	my $pad = Bio::MAGE::ArrayDesign::PhysicalArrayDesign->new(
	    identifier => $args->{arrayaccession},
	);
	$obj->setArrayDesign($pad) unless $obj->getArrayDesign();
	$accession = $args->{arrayaccession};
    }
    else {
	croak("Error: No array design information provided to new_array.");
    }

    # Array serial
    $args->{serial} && do {
        $obj->setArrayIdentifier( $args->{serial} )
            unless $obj->getArrayIdentifier;
    };

    if ( my $get_armanuf = $args->{armanuf_bag} ) {

	# ArrayManufacture - unique for each accession/serial no. combination
	my $armanuf_key = $accession . ( $args->{serial} || q{} );
	my $armanuf = $get_armanuf->(
	    $armanuf_key,
	    {   array               => $obj,
		identifier_template => $args->{identifier_template},
	    }
	);
	$obj->setInformation($armanuf) unless $obj->getInformation;
    }

    return $obj;

}

sub new_armanuf {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::Array::ArrayManufacture->new unless $obj;

    # Identifier
    my $pad        = $args->{array}->getArrayDesign;
    my $identifier = $pad->getIdentifier;
    $obj->setIdentifier(
        "$args->{identifier_template}.$identifier.ArrayManufacture")
        unless $obj->getIdentifier;

    # Arrays
    my $found;
    if ( my $preexisting_arrays = $obj->getArrays ) {
        my $new_identifier = $args->{array}->getIdentifier;
        $found = first { $_->getIdentifier eq $new_identifier }
            @$preexisting_arrays;
    }
    $obj->addArrays( $args->{array} ) unless $found;

    return $obj;

}

sub new_factorvalue {

    my ( $args, $obj ) = @_;

    my ( $ef, $new );

    # New object if needed; add to the ExperimentalFactor
    unless ( $obj ) {
        $obj = Bio::MAGE::Experiment::FactorValue->new();
        $new++;    # Flag so we know to add new FVs to the EF - see end of sub
    }

    # Identifier
    unless ( $obj->getIdentifier() ) {
        my $identifier = sprintf(
	    "%s:%s.%s.%s.FactorValue",
	    $args->{namespace},
	    $args->{exptaccession},
	    $args->{category},
	    $args->{value},
	);
        $obj->setIdentifier($identifier);
    }

    # Name
    $obj->setName( $args->{value} ) unless $obj->getName;

    # Value or Measurement (OE)
    ( ( $args->{value} || $args->{value} eq '0' ) && $args->{category} )
        && do {

        # All numbers treated as measurements, unit class taken from
        # category until we figure out something better
        if ( looks_like_number( $args->{value} ) ) {

            my $measurement;

            # Unit is within parens.
            if ( my ($unitname)
                = ( $args->{category} =~ m/\(\s*(.*?)\s*\)/ ) ) {

                $measurement = _measurement_from_unit_name($unitname);

                # Strip any units from the category so the
                # ExperimentalFactorCategory is correctly parsed.  Unit class
                # is everything before the first parenthesis.
                $args->{category} = ( split /\s*\(/, $args->{category} )[0];
            }

            else {
                $measurement = Bio::MAGE::Measurement::Measurement->new;
            }

            $measurement->setValue( $args->{value} );
            $obj->setMeasurement($measurement) unless $obj->getMeasurement;

        }
        else {

            my $value = &new_ontologyentry(
                {   category => $args->{category},
                    value    => $args->{value}
                }
            );
            $obj->setValue($value) unless $obj->getValue;

        }
        };

    # Get a new ExperimentalFactor, by category
    if ( my $get_ef = $args->{expt_factor_bag} ) {
	$ef = $get_ef->(

	    # Category used as internal ID for tracking
	    $args->{category},
	    $args,
	);
    }
    $ef->addFactorValues($obj) if $new;

    return ( $obj, $ef );

}

sub new_experimentalfactor {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::Experiment::ExperimentalFactor->new unless $obj;

    # Identifier
    my $identifier = sprintf(
	"%s:%s.%s.ExperimentalFactor",
	$args->{namespace},
	$args->{exptaccession},
	$args->{category},
    );
    $obj->setIdentifier($identifier) unless $obj->getIdentifier;

    # Name
    $obj->setName( $args->{category} ) unless $obj->getName;

    # Category (OE)
    my $category_value = decamelize($args->{category});

    my $category = new_ontologyentry(
        {   category => $OE_CAT_EXPERIMENTALFACTORCATEGORY,
            value    => $category_value
        }
    );
    $obj->setCategory($category) unless $obj->getCategory;

    return $obj;

}

sub new_quantitationtype {

    my ( $args, $obj ) = @_;

    my $qt_metrics = $args->{data_metrics}{ $args->{name} };

    unless ($obj) {

        # Check for subclass defined in QT data.  Avoid autovivification
        # here.
        if ( $qt_metrics && $qt_metrics->{subclass} ) {

            eval {
                $obj
                    = ("Bio::MAGE::QuantitationType::$qt_metrics->{subclass}")
                    ->new;
                }
                or do {
                print STDERR (
                    "Warning: Invalid QuantitationType subclass specified: "
                        . "$qt_metrics->{subclass}. Creating SpecalizedQuantitationType object.\n"
                );
                $obj
                    = Bio::MAGE::QuantitationType::SpecializedQuantitationType
                    ->new;
                };

            # $qt_metrics->{confmap} identifies to which QT a confidence
            # indicator maps.
            if ( $qt_metrics->{confmap} && $args->{container} ) {

                my $target_identifier
                    = $args->{prefix} . $qt_metrics->{confmap};

                # Recurse down one level only (container is not passed).
                my $target_qt = $args->{container}->(
                    $target_identifier,
                    {   identifier   => $target_identifier,
                        name         => $qt_metrics->{confmap},
                        prefix       => $args->{prefix},
                        data_metrics => $args->{data_metrics},
                    }
                );

                croak(
                    "Internal MAGE.pm error: unable to identify target QT $qt_metrics->{confmap}\n"
                    )
                    unless $target_qt;

                if (     ( $qt_metrics->{subclass} eq 'PValue' )
		      || ( $qt_metrics->{subclass} eq 'Error' )
		      || ( $qt_metrics->{subclass} eq 'ExpectedValue' ) ) {

		    # Set our target.
		    $obj->setTargetQuantitationType($target_qt);

		    # Update the target to point to us, if not already done.
		    my $found;
		    foreach my $confind (
			@{ $target_qt->getConfidenceIndicators || [] } ) {
			$found++ if ($confind->getIdentifier() eq $args->{identifier} );
		    }
		    $target_qt->addConfidenceIndicators($obj) unless $found;
                }
            }

        }
        else {

            # No subclass given; fall back to specialized QT
            $obj = Bio::MAGE::QuantitationType::SpecializedQuantitationType
                ->new;
        }
    }

    # Identifier, Name
    $obj->setIdentifier( $args->{identifier} ) unless $obj->getIdentifier();
    unless ( $obj->getName() ) {
	my $name = $args->{name};
	if ( $args->{software} ) {
	    $name = "$args->{software}:$name";
	}
	$obj->setName( $name );
    }

    $qt_metrics ||= {};

    # DataType
    my $datatypevalue = $qt_metrics->{datatype} || $OE_VAL_UNKNOWN;
    my $datatype = &new_ontologyentry(
        {   category => $OE_CAT_DATATYPE,
            value    => $datatypevalue
        }
    );
    $obj->setDataType($datatype) unless $obj->getDataType;

    # Scale
    my $scalevalue = $qt_metrics->{scale} || $OE_VAL_UNKNOWN;
    my $scale = &new_ontologyentry(
        {   category => $OE_CAT_SCALE,
            value    => $scalevalue
        }
    );
    $obj->setScale($scale) unless $obj->getScale;

    # IsBackground
    my $isbackground = $qt_metrics->{is_background} || 0;
    $obj->setIsBackground($isbackground) unless $obj->getIsBackground;

    # Channel
    $qt_metrics->{channel} && do {
        my $channel = Bio::MAGE::BioAssay::Channel->new(
            identifier => $AE_CHANNEL_PREFIX . $qt_metrics->{channel},
	    name       => $qt_metrics->{channel},
	);
        $obj->setChannel($channel);
    };

    # Description
    $qt_metrics->{description} && do {
        my $description = Bio::MAGE::Description::Description->new(
            text => $qt_metrics->{description} );
        $obj->setDescriptions( [$description] );
    };

    return $obj;

}

sub new_qtd {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssayData::QuantitationTypeDimension->new
        unless $obj;

    # Identifier
    unless ( $obj->getIdentifier ) {
        my $identifier
            = "$args->{identifier_template}.QuantitationTypeDimension";
        $obj->setIdentifier($identifier);
    }

    # QuantitationTypes. FIXME to add? - maybe rewrite QTD/QT handling
    $obj->setQuantitationTypes( $args->{qt_list} )
        unless $obj->getQuantitationTypes;

    return $obj;

}

sub new_biosource {

    my ( $args, $obj ) = @_;

    # Defaults
    $args->{material_type} ||= $OE_VAL_WHOLEORGANISM;

    # New object if necessary
    $obj = Bio::MAGE::BioMaterial::BioSource->new unless $obj;

    # Identifier, Name
    my $identifier = "$args->{identifier_template}.BioSource";
    $obj->setIdentifier($identifier) unless $obj->getIdentifier;
    $obj->setName( $args->{name} )   unless $obj->getName;

    # MaterialType
    my $materialtype = &new_ontologyentry(
        {   category => $OE_CAT_MATERIALTYPE,
            value    => $args->{material_type}
        }
    );
    $obj->setMaterialType($materialtype) unless $obj->getMaterialType;

    # BioMaterialCharacteristics
    foreach my $char ( @{ $args->{characteristics} } ) {

        my $found;
        if ( my $preexisting_chars = $obj->getCharacteristics ) {
            my $new_category = $char->getCategory;
            my $new_value    = $char->getValue;
            $found = first {
                $_->getCategory     eq $new_category
                    && $_->getValue eq $new_value;
                }
                @$preexisting_chars;
        }
        $obj->addCharacteristics($char) unless $found;

    }

    # Add a free-text description, if included.
    if ($args->{description}){
	my $description = Bio::MAGE::Description::Description->new(
	    text => $args->{description},
	);
	$obj->setDescriptions([$description]) unless $obj->getDescriptions;
    }

    return $obj;

}

sub update_factorvalues {

    my ( $obj, $newfvlist ) = @_;

    my $oldfvlist = $obj->getBioAssayFactorValues;

    my %fvhash = map { $_->getIdentifier => $_ } @$oldfvlist, @$newfvlist;

    # Note that we're using set rather than add here because apparently
    # addBioAssayFactorValues has some really bizarre behaviour
    # (i.e. updating FVs on a related PBA when DBA is passed).
    $obj->setBioAssayFactorValues( [ values %fvhash ] );

    return;
}

sub new_hybridization {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssay::Hybridization->new unless $obj;

    if ( $args->{identifier_template} ) {
        $obj->setIdentifier("$args->{identifier_template}.Hybridization")
            unless $obj->getIdentifier();
    }
    if ( $args->{name} ) {
	$obj->setName($args->{name}) unless $obj->getName();
    }
    if ( $args->{physical_bioassay} ) {
        $obj->setPhysicalBioAssayTarget( $args->{physical_bioassay} )
            unless $obj->getPhysicalBioAssayTarget();
    }

    $obj->setArray( $args->{array} ) if $args->{array};

    my $hyb_protocolapplication;
    if ( $args->{hybridization_protocol} ) {

        $hyb_protocolapplication
            = Bio::MAGE::Protocol::ProtocolApplication->new(
            protocol     => $args->{hybridization_protocol},
            activityDate => 'n/a',

            # FIXME, (also performers, hardware)
	);

        if ( $args->{hybridization_parameters} ) {

            foreach my $param_id (
                sort keys %{ $args->{hybridization_parameters} } ) {

                my $parameter = $args->{parameter_bag}->( $param_id );

		unless ( $parameter ) {
		    croak(qq{Internal error: Parameter "$param_id" not defined.\n});
		}

                $hyb_protocolapplication->addParameterValues(
                    Bio::MAGE::Protocol::ParameterValue->new(
                        value => $args->{hybridization_parameters}{$param_id},
                        parameterType => $parameter
                    )
                );

            }
        }

        if ( $args->{hyb_hardware} ) {

            my $hardwareapplication = &new_hardwareapplication(
                {   hardware   => $args->{hyb_hardware},
                    parameters => $args->{hyb_hardware_params}
                }
            );
            $hyb_protocolapplication->addHardwareApplications(
                $hardwareapplication);

        }

        $obj->setProtocolApplications( [$hyb_protocolapplication] )
            if $hyb_protocolapplication;

    }

    # We can also accept a straight arrayref of ProtocolApplications
    # (e.g. as in MAGE-TAB implementation).
    if ( $args->{hyb_protocolapps} ) {
	$obj->setProtocolApplications( $args->{hyb_protocolapps} );
    }

    return $obj;

}

sub _update_protocol_software {

    my ( $protocol, $software ) = @_;

    my $found;
    if ( my $preexisting_softwares = $protocol->getSoftwares || [] ) {
        my $new_identifier = $software->getIdentifier;
        $found = first { $_->getIdentifier eq $new_identifier }
            @$preexisting_softwares;
    }
    $protocol->addSoftwares($software) unless $found;

    return;
}

sub _update_protocol_hardware {

    my ( $protocol, $hardware ) = @_;

    my $found;
    if ( my $preexisting_hardwares = $protocol->getHardwares || [] ) {
        my $new_identifier = $hardware->getIdentifier;
        $found = first { $_->getIdentifier eq $new_identifier }
            @$preexisting_hardwares;
    }
    $protocol->addHardwares($hardware) unless $found;

    return;
}

sub new_scan_protoapp {

    my ( $args, $obj ) = @_;

    unless ( $obj ) {
	$obj = Bio::MAGE::Protocol::ProtocolApplication->new(
            protocol     => $args->{scanning_protocol},
            activityDate => 'n/a',

            # FIXME (also performers, hardware)
	);
    }

    if ( $args->{software} ) {
	my $softwareapplication
	    = Bio::MAGE::Protocol::SoftwareApplication->new(
		software => $args->{software},
	    );
	$softwareapplication->setVersion( $args->{software_version} )
	    if $args->{software_version};
	
	$obj->setSoftwareApplications(
	    [$softwareapplication],
	);

	_update_protocol_software(
	    $args->{scanning_protocol},
	    $args->{software},
	);
    }

    if ( $args->{scanning_parameters} ) {
	
	foreach  my $param_id (
	    sort keys %{ $args->{scanning_parameters} } ) {
	    
	    my $parameter = $args->{parameter_bag}->( $param_id );

	    unless ( $parameter ) {
		croak(qq{Internal error: Parameter "$param_id" not defined.\n});
	    }

	    $obj->addParameterValues(
		Bio::MAGE::Protocol::ParameterValue->new(
		    value => $args->{scanning_parameters}{$param_id},
		    parameterType => $parameter
		)
	    );
	}
    }

    if ( $args->{scan_hardware} ) {

	my $hardwareapplication = new_hardwareapplication(
	    {   hardware   => $args->{scan_hardware},
		parameters => $args->{scan_hardware_params}
	    }
	);
	$obj->addHardwareApplications(
	    $hardwareapplication,
	);
    }

    return $obj;
}

sub new_scan {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssay::ImageAcquisition->new unless $obj;

    $args->{identifier_template} && do {
        $obj->setIdentifier("$args->{identifier_template}.ImageAcquisition")
            unless $obj->getIdentifier;
    };
    $args->{physical_bioassay} && do {
        $obj->setPhysicalBioAssay( $args->{physical_bioassay} )
            unless $obj->getPhysicalBioAssay;
        $obj->setTarget( $args->{physical_bioassay} ) unless $obj->getTarget;
    };

    # ProtocolApplication and Software
    if ( $args->{scanning_protocol} ) {
        $obj->setProtocolApplications( [ new_scan_protoapp( $args ) ] );
    }

    # We can also accept a straight arrayref of ProtocolApplications
    # (e.g. as in MAGE-TAB implementation).
    if ( $args->{scan_protocolapps} ) {
	$obj->setProtocolApplications( $args->{scan_protocolapps} );
    }

    # Image URIs (NB currently assumes ony one image per hyb FIXME)
    if ( $args->{image} ) {
        my $pba = $args->{physical_bioassay};
        $args->{image}->setChannels( $pba->getChannels ) if $pba->getChannels;
        $obj->setImages(               [ $args->{image} ] );
        $pba->setPhysicalBioAssayData( [ $args->{image} ] );
    }

    return $obj;

}

{
    # Local cache for PBA data (FVs, labels attached to a given hyb).
    my %pba_fv_cache;
    
sub new_physicalbioassay {

    my ( $name, $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssay::PhysicalBioAssay->new unless $obj;

    # Identifier
    $args->{identifier_template} && do {
        my $identifier = "$args->{identifier_template}.PhysicalBioAssay";
        $obj->setIdentifier($identifier) unless $obj->getIdentifier;
    };

    # Name; store this also in $args for passing to new_hybridization
    unless ( defined( $args->{name} ) ) {
	$args->{name} = $name;
    }
    $obj->setName($args->{name}) unless $obj->getName();

    # Channels
    # Construct hash listing of old label ids
    my $old_label_identifiers;
    if ( $obj->getChannels ) {
        foreach my $old_channel ( @{ $obj->getChannels } ) {
            foreach my $old_label ( @{ $old_channel->getLabels } ) {
                $old_label_identifiers->{ $old_label->getIdentifier }++;
            }
        }
    }

    # N.B. only one label per LE supported here (although LEs can have
    # more than one label, and new_labelledextract does support
    # this). This is not likely to be a problem for tab2mage or
    # mage-tab though, as the LEs are internally uniqued by dye name.

    my $dyename;
    if ( $args->{label} ) {

        # Convert LabelCompound into Channel
        my $dye_id = $args->{label}->getIdentifier();
        unless ( ( $dyename )
		     = ( $dye_id =~ m/^$AE_LABELCOMPOUND_PREFIX(.*)$/ ) ) {
	    $dyename = 'Unknown';
        }

	# Don't add the channel if it's already present.
	unless ( $old_label_identifiers->{ $dye_id } ) {
	    my $channel = Bio::MAGE::BioAssay::Channel->new(
		identifier => $AE_CHANNEL_PREFIX . $dyename,
		name       => $dyename,
		labels     => [ $args->{label} ],
	    );
	    $obj->addChannels($channel);

	    # Cache the label object for later.
	    $pba_fv_cache{$name}{$dyename}{label} = $args->{label};
	}

    }
    else {

	# We still need a default $dyename.
	$dyename = 'Unknown';
    }

    my $merged_pba;
    if ( $args->{derived_from} ) {

	# i.e. this is a hyb-level PBA.

	# Record the factor values in the cache. These will be used
	# subsequently to rebuild two-colour hyb codings with the
	# appropriate fvs and labels.
	foreach my $fv ( @{ $args->{factor_values} } ) {
	    $pba_fv_cache{$name}{$dyename}{fv}{$fv->getIdentifier()} = $fv;
	}

	# BioAssayCreation
	my $hyb_args = { %$args };  # Make a copy.
	$hyb_args->{physical_bioassay} = $obj;
	$obj->setBioAssayCreation( new_hybridization( $hyb_args ) )
	    unless $obj->getBioAssayCreation;

	# SourceBioMaterialMeasurements - added where the BioMaterials
	# differ (e.g. Cy3, Cy5 labeled extracts).
        my $new_bm = $args->{derived_from};

        my $preexisting_bmms
            = $obj->getBioAssayCreation()->getSourceBioMaterialMeasurements;

        my $new_identifier = $new_bm->getIdentifier;

        my $found
            = first { $_->getBioMaterial()->getIdentifier eq $new_identifier }
            @$preexisting_bmms;

        unless ($found) {
            my $bmm = Bio::MAGE::BioMaterial::BioMaterialMeasurement->new(
                bioMaterial => $new_bm );
            $obj->getBioAssayCreation()
                ->addSourceBioMaterialMeasurements($bmm);
        }

	# Best-practice PBA coding for two (or more) channels.
	# Generate a channel PBA from the cached data for the original
	# hyb-level PBA, and a merged PBA.
	if ( scalar @{ $obj->getChannels() || [] } > 1 ) {

	    # Delete any preexisting BATs from single-channel coding.
	    $obj->setBioAssayTreatments([]);

	    my (@channel_specific_pbas, %merged_fvs, %merged_labels);
	    while ( my ( $dyename, $channeldata ) = each %{ $pba_fv_cache{$name} } ) {

		my $new_id_template = "$args->{identifier_template}.$dyename";
		my $label           = $channeldata->{label};
		my $factor_values   = [ values %{ $channeldata->{fv} } ];

		# We don't pass $args->{derived_from} down to the
		# extended PBA generation; this is used as a flag to
		# indicate hyb-level PBAs only.
		my $channel_pba = $args->{extended_pba_bag}->(
		    "$name.$dyename",
		    {
			identifier_template => $new_id_template,
			label               => $label,
			factor_values       => $factor_values,
		    },
		);

		# Add a BioAssayTreatment pointing back to $obj here.
		my %preexisting;
		if ( my $old_treats = $obj->getBioAssayTreatments() ) {
		    foreach my $treat ( @$old_treats ) {
			if ( my $old_pba = $treat->getPhysicalBioAssay() ) {
			    $preexisting{ $old_pba->getIdentifier() }++;
			}
		    }
		}
		unless ( $preexisting{ $channel_pba->getIdentifier() } ) {
		    $obj->addBioAssayTreatments(
			new_scan(
			    {
				identifier_template => $new_id_template,
				physical_bioassay   => $channel_pba,
			    }
			)
		    );
		}

		# Cache these for addition to the merged PBA below.
		foreach my $fv (@$factor_values) {
		    $merged_fvs{ $fv->getIdentifier() } = $fv;
		}
		$merged_labels{ $label->getIdentifier() } = $label;

		push @channel_specific_pbas, $channel_pba;

	    }

	    # Create the merged PBA here. Note that the .merged suffix
	    # convention is assumed in other modules for mapping the
	    # hyb PBA to the merged PBA and vice versa.
	    my $merged_id_template = "$args->{identifier_template}.merged";

	    # This is a little clumsy; the alternative is proper
	    # support for multiple labels FIXME.
	    foreach my $label ( values %merged_labels ) {
		$merged_pba = $args->{extended_pba_bag}->(
		    "$name.merged",
		    {
			identifier_template => $merged_id_template,
			label               => $label,
			factor_values       => [ values %merged_fvs ],
		    },
		);
	    }

	    # Add an empty BAT so we can attach image_acquisition protocol.
	    if ( $merged_pba ) {
		my $merge_args = { %$args };  # Make a copy.
		$merge_args->{identifier_template} = $merged_id_template;
		$merge_args->{physical_bioassay}   = $merged_pba;
		$merged_pba->addBioAssayTreatments( new_scan( $merge_args ) );
	    }

	    # Link the merged and channel-specific PBAs.
	    foreach my $channel_pba ( @channel_specific_pbas ) {
		my $scan_id_template
		    = "$args->{identifier_template}." . unique_identifier();
		$channel_pba->setBioAssayTreatments( [ new_scan(
		    {
			identifier_template => $scan_id_template,
			physical_bioassay   => $merged_pba,
		    },
		) ] );
	    }

	    # Repoint any MBAs pointing at the hyb-level PBA. This is
	    # particularly important for single-channel scans of
	    # two-colour hybs.
	    if ( $args->{mba_bag} ) {
		foreach my $mba ( @{ $args->{mba_bag}->() } ) {
		    if ( my $fext = $mba->getFeatureExtraction() ) {
			if ( my $source = $fext->getPhysicalBioAssaySource() ) {
			    if ( $obj->getIdentifier() eq $source->getIdentifier() ) {
				$fext->setPhysicalBioAssaySource($merged_pba);
			    }
			}
		    }
		}
	    }
	}
	else {

	    # BioAssayTreatments
	    $obj->setBioAssayTreatments( [ new_scan( $hyb_args ) ] )
		unless $obj->getBioAssayTreatments;
	}
    }
    else {

	# This is a channel-specific or merging PBA (do nothing).
    }

    # BioAssayFactorValues
    # Update if FVs already present
    $args->{factor_values}
        && do { update_factorvalues( $obj, $args->{factor_values} ) };

    return $obj;

}

}

sub new_image {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssay::Image->new unless $obj;

    my $identifier = "$args->{identifier_template}.Image";
    $obj->setIdentifier($identifier) unless $obj->getIdentifier;

    $obj->setURI( $args->{uri} ) unless $obj->getURI;

    # We assume the default image format is TIFF
    my $format = new_ontologyentry(
        {   category => "ImageFormat",
            value    => ( $args->{format} || "TIFF" )
        }
    );

    $obj->setFormat($format) unless $obj->getFormat;

    return $obj;
}

sub new_featureextraction {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssay::FeatureExtraction->new(
        identifier => "$args->{identifier_template}.FeatureExtraction",
        measuredBioAssayTarget => $args->{measured_bioassay},
        )
        unless $obj;
    if ( $args->{physical_bioassay} ) {
        $obj->setPhysicalBioAssaySource( $args->{physical_bioassay} )
            unless $obj->getPhysicalBioAssaySource;
    }

    if ( $args->{protocol} ) {

        my $protocolapplication
            = Bio::MAGE::Protocol::ProtocolApplication->new(
            protocol     => $args->{protocol},
            activityDate => 'n/a',

            # FIXME (also performers, hardware)
            );
        $args->{software} && do {
            my $softwareapplication
                = Bio::MAGE::Protocol::SoftwareApplication->new(
                software => $args->{software} );
            $softwareapplication->setVersion( $args->{software_version} )
                if $args->{software_version};
            $protocolapplication->setSoftwareApplications(
                [$softwareapplication] )
                if $softwareapplication;

            _update_protocol_software( $args->{protocol}, $args->{software} );
        };

        $args->{parameters} && do {

            foreach my $param_id ( sort keys %{ $args->{parameters} } ) {

                my $parameter = $args->{parameter_bag}->( $param_id );

		unless ( $parameter ) {
		    croak(qq{Internal error: Parameter "$param_id" not defined.\n});
		}

                $protocolapplication->addParameterValues(
                    Bio::MAGE::Protocol::ParameterValue->new(
                        value         => $args->{parameters}{$param_id},
                        parameterType => $parameter
                    )
                );
            }
        };
        $obj->setProtocolApplications( [$protocolapplication] );
    }

    # For MAGE-TAB.
    if ( $args->{protocolapps} ) {
	$obj->setProtocolApplications( $args->{protocolapps} );
    }

    return $obj;

}

sub new_measuredbioassay {

    my ( $name, $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssay::MeasuredBioAssay->new unless $obj;

    # Identifier
    if ( $args->{identifier_template} ) {
	my $identifier = "$args->{identifier_template}.MeasuredBioAssay";
	$obj->setIdentifier($identifier) unless $obj->getIdentifier;
    }

    # Name
    $obj->setName($args->{name} || $name) unless $obj->getName;

    # MBAD
    if ( $args->{measured_bioassay_data} ) {

        my $found;
        if ( my $preexisting_mbads = $obj->getMeasuredBioAssayData ) {
            my $new_identifier
                = $args->{measured_bioassay_data}->getIdentifier;
            $found = first { $_->getIdentifier eq $new_identifier }
                @$preexisting_mbads;
        }
        $obj->addMeasuredBioAssayData( $args->{measured_bioassay_data} )
            unless $found;
    };

    # Channels
    if ( $args->{channelname} ) {

	# Construct hash listing of old label ids
	my $found;
	if ( my $oldchannels = $obj->getChannels() ) {
	    foreach my $old_ch ( @$oldchannels ) {
		foreach my $old_label ( @{ $old_ch->getLabels() || [] } ) {
		    $found++ if ( $old_label->getName() eq $args->{channelname} );
		}
	    }
	}
	unless ( $found ) {
	    my $label = new_labelcompound({dye => $args->{channelname}});
	    my $channel = Bio::MAGE::BioAssay::Channel->new(
		identifier => $AE_CHANNEL_PREFIX . $args->{channelname},
		name       => $args->{channelname},
		labels     => [ $label ],
	    );
	    $obj->addChannels($channel);
	}
    }

    # BioAssayFactorValues
    # Update if FVs already present
    if ( $args->{factor_values} ) {
	update_factorvalues( $obj, $args->{factor_values} )
    }

    # FeatureExtraction
    my %fext_args = %$args;
    $fext_args{measured_bioassay} = $obj;

    # Note that this needs to be able to overwrite old FEs, e.g. when
    # switching to two-colour hyb PBA coding.
    $obj->setFeatureExtraction( new_featureextraction( \%fext_args ) );

    return $obj;

}

sub new_derivedbioassay {

    my ( $name, $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssay::DerivedBioAssay->new unless $obj;

    # Identifier
    $args->{identifier_template} && do {
	my $identifier = "$args->{identifier_template}.DerivedBioAssay";
	$obj->setIdentifier($identifier) unless $obj->getIdentifier;
    };

    # Name
    $obj->setName($args->{name} || $name) unless $obj->getName;

    # DBAD
    $args->{derived_bioassay_data} && do {

        my $found;
        if ( my $preexisting_dbads = $obj->getDerivedBioAssayData ) {
            my $new_identifier
                = $args->{derived_bioassay_data}->getIdentifier;
            $found = first { $_->getIdentifier eq $new_identifier }
                @$preexisting_dbads;
        }
        $obj->addDerivedBioAssayData( $args->{derived_bioassay_data} )
            unless $found;
    };

    # BioAssayFactorValues
    # Update if FVs already present
    $args->{factor_values}
        && do { update_factorvalues( $obj, $args->{factor_values} ) };

    # DerivedBioAssayType
    $args->{normalization_type} && do {
        my $dbatype = &new_ontologyentry(
            {   category => $OE_CAT_DERIVEDBIOASSAYTYPE,
                value    => $args->{normalization_type},
            }
        );
        $obj->setType($dbatype) unless $obj->getType;
    };

    # DerivedBioAssayMap
    $args->{source_bioassays} && do {

	unless ( $args->{bioassaymap_bag} ) {
	    croak("No BioAssayMap container passed to new_derivedbioassay.");
	}

        # BioAssayMap and DBA both have the same $name internal identifier.
        # new_bioassaymap updates source_bioassays.
        my $bioassaymap = $args->{bioassaymap_bag}->(
            $name,
            {   identifier_template => $args->{identifier_template},
                source_bioassays    => $args->{source_bioassays},
                target_bioassay     => $obj
            }
        );
        $obj->setDerivedBioAssayMap( [$bioassaymap] );
    };

    return $obj;

}

sub add_summary_stats {

    my ( $obj, $stat_hash ) = @_;

    if ($stat_hash) {
        my @statistics;
        foreach my $name ( sort keys %{$stat_hash} ) {
            push(
                @statistics,
                Bio::MAGE::NameValueType->new(
                    name  => $name,
                    value => $stat_hash->{$name},
                )
            );
        }
        $obj->setSummaryStatistics( \@statistics )
            unless $obj->getSummaryStatistics;

    }
    else {

        return 0;

    }

    return 1;
}

sub new_measuredbioassaydata {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssayData::MeasuredBioAssayData->new unless $obj;

    # Identifier
    my $identifier = "$args->{identifier_template}.MeasuredBioAssayData";
    $obj->setIdentifier($identifier) unless $obj->getIdentifier;

    # Name
    unless ( defined( $args->{name} ) ) {
	$args->{name} = $args->{filename};
    }
    $obj->setName( $args->{name} ) unless $obj->getName();

    # DED, QTD (BAD added separately at the moment FIXME)
    $args->{feature_dimension} && do {
	$obj->setDesignElementDimension( $args->{feature_dimension} )
	    unless $obj->getDesignElementDimension;
    };
    $args->{quantitation_type_dimension} && do {
	$obj->setQuantitationTypeDimension( $args->{quantitation_type_dimension} )
	    unless $obj->getQuantitationTypeDimension;
    };

    # BioDataCube
    my $biodatacube = Bio::MAGE::BioAssayData::BioDataCube->new(
        order => $args->{order} || 'BDQ',
        cube => $args->{filename}
    );
    $obj->setBioDataValues($biodatacube) unless $obj->getBioDataValues;

    # Summary Statistics
    &add_summary_stats( $obj, $args->{summary_statistics} )
        if $args->{summary_statistics};

    return $obj;

}

sub new_producertransformation {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssayData::Transformation->new(
        identifier => "$args->{identifier_template}.Transformation",
        derivedBioAssayDataTarget => $args->{derived_bioassay_data},

        # FIXME QTM
    ) unless $obj;

    if ( $args->{bioassay_map} && scalar @{ $args->{bioassay_map} } ) {
        my $bioassay_mapping = Bio::MAGE::BioAssayData::BioAssayMapping->new(
            bioAssayMaps => $args->{bioassay_map} );
        $obj->setBioAssayMapping($bioassay_mapping)
            unless $obj->getBioAssayMapping;
    }

    my $protocolapplication;
    if ( $args->{protocol} ) {
        $protocolapplication = Bio::MAGE::Protocol::ProtocolApplication->new(
            protocol     => $args->{protocol},
            activityDate => 'n/a',

            # FIXME (also performers, hardware applications)
        );
        $args->{software} && do {
            my $softwareapplication
                = Bio::MAGE::Protocol::SoftwareApplication->new(
                software => $args->{software} );
            $softwareapplication->setVersion( $args->{software_version} )
                if $args->{software_version};

            $protocolapplication->setSoftwareApplications(
                [$softwareapplication] );

            &_update_protocol_software( $args->{protocol},
                $args->{software} );
        };

        $args->{parameters} && do {

            foreach my $param_id ( sort keys %{ $args->{parameters} } ) {

                my $parameter = $args->{parameter_bag}->( $param_id );

		unless ( $parameter ) {
		    croak(qq{Internal error: Parameter "$param_id" not defined.\n});
		}

                $protocolapplication->addParameterValues(
                    Bio::MAGE::Protocol::ParameterValue->new(
                        value         => $args->{parameters}{$param_id},
                        parameterType => $parameter
                    )
                );

            }
        };
	$obj->setProtocolApplications( [$protocolapplication] );
    }

    # MAGE-TAB support.
    if ( $args->{protocolapps} ) {
	$obj->setProtocolApplications( $args->{protocolapps} );
    }

    $obj->setBioAssayDataSources( $args->{source_bioassay_dataset} )
        if ($args->{source_bioassay_dataset}
		&& scalar @{$args->{source_bioassay_dataset}});

    $obj->setQuantitationTypeMapping( $args->{quantitationtype_mapping} )
        if $args->{quantitationtype_mapping};

    return $obj;

}

sub new_quantitationtypemap {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssayData::QuantitationTypeMap->new unless $obj;

    $args->{target_qt} && do {
        my $identifier = "$args->{identifier_template}."
            . $args->{target_qt}->getName
            . '.QuantitationTypeMap';
        $obj->setIdentifier($identifier) unless $obj->getIdentifier;
        $obj->setTargetQuantitationType( $args->{target_qt} )
            unless $obj->getTargetQuantitationType;
    };

    $args->{source_qts} && do {
        if ( my $old_source_qts = $obj->getSourcesQuantitationType ) {

            # update a previous list
            foreach my $new_qt ( @{ $args->{source_qts} } ) {
                my $new_identifier = $new_qt->getIdentifier;
                my $found = first { $_->getIdentifier eq $new_identifier }
                    @$old_source_qts;
                $obj->addSourcesQuantitationType($new_qt) unless $found;
            }
        }
        else {
            $obj->setSourcesQuantitationType( $args->{source_qts} );
        }
    };

    return $obj;

}

sub new_derivedbioassaydata {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssayData::DerivedBioAssayData->new unless $obj;

    # Identifier
    my $identifier = "$args->{identifier_template}.DerivedBioAssayData";
    $obj->setIdentifier($identifier) unless $obj->getIdentifier;

    # Name
    unless ( defined( $args->{name} ) ) {
	$args->{name} = $args->{filename};
    }
    $obj->setName( $args->{name} ) unless $obj->getName();

    # DED, QTD, BAD
    $args->{de_dimension} && do {
	$obj->setDesignElementDimension( $args->{de_dimension} )
	    unless $obj->getDesignElementDimension;
    };
    $args->{ba_dimension} && do {
	$obj->setBioAssayDimension( $args->{ba_dimension} )
	    unless $obj->getBioAssayDimension;
    };

    my @qt_maps;
    if ( my $qtd = $args->{qt_dimension} ) {
        $obj->setQuantitationTypeDimension($qtd)
            unless $obj->getQuantitationTypeDimension;
        if (   $args->{source_qts}
            && $args->{qtm_bag}
            && ( my $qts = $qtd->getQuantitationTypes ) ) {
            foreach my $qt (@$qts) {

                # Note - we may need a proper container for QTMs FIXME
                my %map_args = %$args;
                $map_args{target_qt} = $qt;
                my $map = $args->{qtm_bag}->( $qt->getName, \%map_args );
                $qt->setQuantitationTypeMaps( [$map] );
                push( @qt_maps, $map );
            }
        }
    }
    my $qt_mapping;
    if (@qt_maps) {
        $qt_mapping = Bio::MAGE::BioAssayData::QuantitationTypeMapping->new(
            quantitationTypeMaps => \@qt_maps );
    }

    # BioDataCube
    my $biodatacube = Bio::MAGE::BioAssayData::BioDataCube->new(
        order => $args->{order} || 'DBQ',
        cube => $args->{filename}
    );
    $obj->setBioDataValues($biodatacube) unless $obj->getBioDataValues;

    # ProducerTransformation
    my %txn_args = %$args;
    $txn_args{derived_bioassay_data}    = $obj;
    $txn_args{quantitationtype_mapping} = $qt_mapping;
    $obj->setProducerTransformation(
        new_producertransformation( \%txn_args ) )
        unless $obj->getProducerTransformation;

    # Summary Statistics
    &add_summary_stats( $obj, $args->{summary_statistics} )
        if $args->{summary_statistics};

    return $obj;

}

sub new_measurement {

    my ($args) = @_;

    my $unit;

    my $sighandler = $SIG{__DIE__};
    delete $SIG{__DIE__};

    eval {
        $unit = ("Bio::MAGE::Measurement::$args->{class}")->new;

        # unitNameCV is required, so we need some defaults here
        if ( $args->{class} eq 'TemperatureUnit' ) {

            # The overwhelmingly likely case.
            $unit->setUnitNameCV( $args->{unit} || 'degree_C' );
        }
        else {

            # all other classes have 'other'
            $unit->setUnitNameCV( $args->{unit} || 'other' );
        }
    };

    $SIG{__DIE__} = $sighandler if $sighandler;

    if ($EVAL_ERROR) {

        # Fall-through default is generic QuantityUnit
        print STDERR (
                  qq{Warning: Unit name "$args->{unit}" not in MAGE Unit }
                . qq{controlled vocabulary. Creating generic QuantityUnit.\n}
        );
        $unit = Bio::MAGE::Measurement::QuantityUnit->new(
            unitName => ( $args->{unit} || 'n/a' ),
            unitNameCV => 'other',
        );
    }

    my $measurement
        = Bio::MAGE::Measurement::Measurement->new( unit => $unit );

    $args->{kindcv} && $measurement->setKindCV( $args->{kindcv} );

    $measurement->setValue( $args->{value} )
        if ( $args->{value}
        || ( defined( $args->{value} ) && $args->{value} eq '0' ) );

    return $measurement;

}

sub _measurement_from_unit_name {

    my ($unitname) = @_;

    my $unitclass = q{};

    # Populate a HoH with MAGE UnitNameCVs
    my $mage_units;
    foreach my $subclass ( keys %$MAGE_UNITS ) {
        foreach my $unitNameCV ( @{ $MAGE_UNITS->{$subclass} } ) {
            $mage_units->{$subclass}{$unitNameCV}++;
        }
    }

    # Find our subclass
    UNIT_SUBCLASS:
    foreach my $subclass ( keys %$mage_units ) {
        if ( $mage_units->{$subclass}{$unitname} ) {
            $unitclass = $subclass;
            last UNIT_SUBCLASS;
        }
    }

    my ($kindCV) = ( $unitclass =~ m/^(.*)Unit$/ );
    $kindCV = lc($kindCV);

    my $measurement = &new_measurement(
        {   class  => $unitclass,
            unit   => $unitname,
            kindcv => $kindCV,
        }
    );

    return $measurement;
}

sub new_parameter {

    my ( $identifier, $args, $obj ) = @_;

    $obj = Bio::MAGE::Protocol::Parameter->new unless $obj;

    $obj->setIdentifier($identifier) unless $obj->getIdentifier;

    $args->{name}
        && do { $obj->setName( $args->{name} ) unless $obj->getName; };

    ( defined( $args->{unit} ) && ( $args->{unit} ne q{} ) ) && do {

        my $measurement = _measurement_from_unit_name( $args->{unit} );
        $obj->setDefaultValue($measurement) unless $obj->getDefaultValue;

    };

    return $obj;

}

sub new_hardwareapplication {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::Protocol::HardwareApplication->new unless $obj;

    my $hardware = Bio::MAGE::Protocol::Hardware->new(
        identifier => $args->{hardware} );
    $obj->setHardware($hardware) unless $obj->getHardware;

    foreach my $param_id ( sort keys %{ $args->{parameters} } ) {

        # No need to keep track of these yet; Affy only FIXME.
        my $parameter = new_parameter( $param_id );

	unless ( $parameter ) {
	    croak(qq{Internal error: Parameter "$param_id" not defined.\n});
	}

        $obj->addParameterValues(
            Bio::MAGE::Protocol::ParameterValue->new(
                value         => $args->{parameters}{$param_id},
                parameterType => $parameter
            )
        );
    }

    return $obj;

}

sub new_bioassaymap {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssayData::BioAssayMap->new unless $obj;

    # Identifier
    $args->{identifier_template} && do {
        my $identifier = "$args->{identifier_template}.BioAssayMap";
        $obj->setIdentifier($identifier) unless $obj->getIdentifier;
    };

    # TargetBioAssay
    $args->{target_bioassay} && do {
        $obj->setBioAssayMapTarget( $args->{target_bioassay} )
            unless $obj->getBioAssayMapTarget;
    };

    # SourceBioAssays
    $args->{source_bioassays} && do {
        if ( my $preexisting_bioassays = $obj->getSourceBioAssays ) {
            foreach my $bioassay ( @{ $args->{source_bioassays} } ) {
                my $new_identifier = $bioassay->getIdentifier;
                my $found = first { $_->getIdentifier eq $new_identifier }
                    @$preexisting_bioassays;
                $obj->addSourceBioAssays($bioassay) unless $found;
            }
        }
        else {
            $obj->setSourceBioAssays( $args->{source_bioassays} );
        }
    };

    return $obj;

}

sub new_software {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::Protocol::Software->new unless $obj;

    # Identifier
    my $identifier = "$args->{identifier_template}.Software";
    $obj->setIdentifier($identifier) unless $obj->getIdentifier;

    # Type
    if ( $args->{type} ) {
	my $type = new_ontologyentry(
	    {   category => $OE_CAT_SOFTWARETYPE,
		value    => $args->{type}
	    }
	);
	$obj->setType($type) unless $obj->getType;
    }

    # Name
    $obj->setName( $args->{name} ) unless $obj->getName;

    return $obj;

}

sub new_hardware {

    my ( $args, $obj ) = @_;

    $obj = Bio::MAGE::Protocol::Hardware->new unless $obj;

    # Identifier
    my $identifier = "$args->{identifier_template}.Hardware";
    $obj->setIdentifier($identifier) unless $obj->getIdentifier;

    # Type
    if ( $args->{type} ) {
	my $type = new_ontologyentry(
	    {   category => $OE_CAT_HARDWARETYPE,
		value    => $args->{type}
	    }
	);
	$obj->setType($type) unless $obj->getType;
    }

    # Name
    $obj->setName( $args->{name} ) unless $obj->getName;

    return $obj;

}

1;

