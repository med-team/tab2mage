#!/usr/bin/env perl
#
# Module to provide basic methods used by the tab2mage script.
#
# Tim Rayner 2005, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Tab2MAGE.pm 2086 2008-06-23 10:48:42Z tfrayner $
#

package ArrayExpress::Curator::Tab2MAGE;

use strict;
use warnings;

use Carp;
use File::Spec;
use File::Path;
use File::Basename;
use IO::File;
use Text::CSV_XS;
use List::Util qw(first);
use Readonly;
use Bio::MAGE qw(:ALL);
use Class::Std;

use ArrayExpress::Curator::Common qw(
    date_now
    find_cdf
    get_filepath_from_uri
);

require ArrayExpress::Datafile;
require ArrayExpress::Datafile::Parser;
require ArrayExpress::Datafile::DataMatrix;

use ArrayExpress::Curator::Common qw(
    check_linebreaks
    $RE_EMPTY_STRING
    $RE_COMMENTED_STRING
    $RE_SURROUNDED_BY_WHITESPACE
    $RE_WITHIN_PARENTHESES
    $RE_SQUARE_BRACKETS
);

use ArrayExpress::Curator::Config qw($CONFIG);

use ArrayExpress::Curator::MAGE qw(
    write_mage
    postprocess_mage
    unique_identifier
    make_container
    new_biosource
    new_biosample
    new_extract
    new_immunoprecipitate
    new_labeledextract
    new_labelcompound
    new_image
    new_physicalbioassay
    new_measuredbioassay
    new_measuredbioassaydata
    new_derivedbioassay
    new_derivedbioassaydata
    new_bioassaymap
    new_quantitationtypemap
    new_protocol
    new_parameter
    new_software
    new_armanuf
    new_array
    new_factorvalue
    new_experimentalfactor
    new_quantitationtype
    new_qtd
    new_ontologyentry
    new_person
);

use ArrayExpress::Curator::MAGE::Definitions qw(
    validate_experiment_section
    validate_protocol_section
    validate_hybridization_section

    $EDF_EXPTACCESSION
    $EDF_EXPTDOMAIN
    $EDF_EXPTNAME
    $EDF_EXPTSUBMITTER
    $EDF_EXPTDATACODER
    $EDF_EXPTCURATOR
    $EDF_EXPTINVESTIGATOR
    $EDF_EXPTORGANIZATION
    $EDF_EXPTADDRESS
    $EDF_PUBAUTHORS
    $EDF_EXPTSUBMITTER_EMAIL
    $EDF_EXPTCURATOR_EMAIL
    $EDF_EXPTDATACODER_EMAIL
    $EDF_EXPTINVESTIGATOR_EMAIL
    $EDF_EXPTSECONDARYACCESSION
    $EDF_EXPTDESCRIPTION
    $EDF_EXPTRELEASEDATE
    $EDF_EXPTSUBMISSIONDATE
    $EDF_EXPTGEORELEASEDATE
    $EDF_EXPTDESIGNTYPE
    $EDF_EXPTQUALITYCONTROL
    $EDF_EXPTCURATEDNAME
    $EDF_EXPTURI

    $EDF_PUBTITLE
    $EDF_PUBPAGES
    $EDF_PUBJOURNAL
    $EDF_PUBVOLUME
    $EDF_PUBISSUE
    $EDF_PUBYEAR
    $EDF_PUBAUTHORS
    $EDF_PUBURI
    $EDF_PUBMEDID

    $EDF_PROTOCOLACCESSION
    $EDF_PROTOCOLNAME
    $EDF_PROTOCOLTYPE
    $EDF_PROTOCOLTEXT
    $EDF_PROTOCOLPARAMS

    $EDF_PROTOCOLACCESSION
    $EDF_PROTOCOLPARAMS
    $EDF_BIOSOURCENAME
    $EDF_BIOSOURCETYPE
    $EDF_BIOSOURCEDESCRIPTION
    $EDF_BIOSAMPLENAME
    $EDF_BIOSAMPLETYPE
    $EDF_EXTRACTNAME
    $EDF_EXTRACTTYPE
    $EDF_IMMUNOPRECIPITATENAME
    $EDF_IMMUNOPRECIPITATETYPE
    $EDF_LABELEDEXTRACTNAME
    $EDF_LABELEDEXTRACTTYPE
    $EDF_HYBRIDIZATIONNAME
    $EDF_SCANNAME
    $EDF_NORMALIZATIONNAME
    $EDF_NORMALIZATIONTYPE
    $EDF_DYE

    $EDF_GROW_PROTOCOL
    $EDF_TREAT_PROTOCOL
    $EDF_EXTRACT_PROTOCOL
    $EDF_POOL_PROTOCOL
    $EDF_LABEL_PROTOCOL
    $EDF_IP_PROTOCOL
    $EDF_HYB_PROTOCOL
    $EDF_SCAN_PROTOCOL
    $EDF_FEXT_PROTOCOL
    $EDF_NORM_PROTOCOL

    $EDF_SCAN_SOFTWARE
    $EDF_FEXT_SOFTWARE
    $EDF_NORM_SOFTWARE

    $EDF_IMAGE_FORMAT

    $EDF_ARRAYACCESSION
    $EDF_ARRAYSERIAL

    $EDF_FILE_PREFIX
    $EDF_FILE_SUFFIX
    $EDF_PARAM_PREFIX
    $EDF_PARAM_SUFFIX
    $EDF_BMC_PREFIX
    $EDF_BMC_SUFFIX
    $EDF_FV_PREFIX
    $EDF_FV_SUFFIX

    $EDF_SAMPLE_PARAMS
    $EDF_TREAT_PARAMS
    $EDF_EXTRACT_PARAMS
    $EDF_IP_PARAMS
    $EDF_LABEL_PARAMS
    $EDF_HYB_PARAMS
    $EDF_SCAN_PARAMS
    $EDF_FEXT_PARAMS
    $EDF_NORM_PARAMS

    $EDF_FEXT_STATS
    $EDF_NORM_STATS

    $EDF_HYB_HARDWARE
    $EDF_SCAN_HARDWARE
    $EDF_HYB_HW_PARAMS
    $EDF_SCAN_HW_PARAMS

    $OE_CAT_EXPERIMENTDESIGNTYPE
    $OE_CAT_QUALITYCONTROLDESCRIPTIONTYPE
    $OE_CAT_PUBLICATIONTYPE
    $OE_VAL_JOURNALARTICLE

    $OE_VAL_SUBMITTER
    $OE_VAL_DATA_CODER
    $OE_VAL_CURATOR
    $OE_VAL_INVESTIGATOR

    $OE_VAL_GROW
    $OE_VAL_POOL
    $OE_VAL_SPECIFIEDBIOMATERIALACTION
    $OE_VAL_NUCLEICACIDEXTRACTION
    $OE_VAL_IMMUNOPRECIPITATE
    $OE_VAL_LABELING
    $OE_VAL_NORMALIZATION
    $OE_VAL_HYBRIDIZATION
    $OE_VAL_SCANNING
    $OE_VAL_FEATUREEXTRACTION
    $OE_VAL_SCANNING_SOFTWARE
    $OE_VAL_ANALYSIS_SOFTWARE
    $OE_VAL_TRANSFORMATION_SOFTWARE

    $OE_VAL_SYNTHETICRNA
    $OE_VAL_TOTALRNA
);

use ArrayExpress::Curator::Logger;
use base qw(ArrayExpress::Curator::Logger);

# Class::Std attributes
my %spreadsheet_filename : ATTR( :get<spreadsheet_filename>, :init_arg<spreadsheet_filename> );
my %target_directory     : ATTR( :name<target_directory>,    :init_arg<target_directory>     );
my %source_directory     : ATTR( :get<source_directory>,    :init_arg<source_directory>    :default<undef> );
my %is_standalone        : ATTR( :get<is_standalone>,       :init_arg<is_standalone>,      :default<0> );
my %qt_filename          : ATTR( :get<qt_filename>,         :init_arg<qt_filename>         :default<undef> );
my %keep_all_qts         : ATTR( :get<keep_all_qts>,        :init_arg<keep_all_qts>        :default<undef> );
my %keep_protocol_accns  : ATTR( :get<keep_protocol_accns>, :init_arg<keep_protocol_accns> :default<undef> );
my %include_default_qts  : ATTR( :get<include_default_qts>, :init_arg<include_default_qts> :default<undef> );
my %use_plain_text       : ATTR( :get<use_plain_text>,      :init_arg<use_plain_text>      :default<undef> );
my %reporter_prefix      : ATTR( :get<reporter_prefix>,     :init_arg<reporter_prefix>,    :default<undef> );
my %compseq_prefix       : ATTR( :get<compseq_prefix>,      :init_arg<compseq_prefix>,     :default<undef> );
my %external_accession   : ATTR( :get<external_accession>,  :init_arg<external_accession>  :default<undef> );
my %external_domain      : ATTR( :get<external_domain>,     :init_arg<external_domain>     :default<undef> );
my %bags                 : ATTR( :name<bags>,         :default<{}>  );
my %namespace            : ATTR( :name<namespace>,    :default<q{}> );
my %expt_section         : ATTR( :name<expt_section>, :default<{}>  );
my %prot_section         : ATTR( :name<prot_section>, :default<[]>  );
my %hyb_section          : ATTR( :name<hyb_section>,  :default<[]>  );
my %pool_protocol_assns  : ATTR( :default<{}> );
my %pool_protocol_usage  : ATTR( :default<{}> );
my %ignore_size_limits   : ATTR( :name<ignore_size_limits>,  :default<undef> );

###########
# METHODS #
###########

sub BUILD {
    my ( $self, $id, $args ) = @_;

    # Set up the MAGE container hash
    $self->initialize_bags();

    return;
}

sub START {
    my ( $self, $id, $args ) = @_;

    croak("Error: no target_directory set")
	unless ( $target_directory{$id} );
 
    croak("Error: no spreadsheet_filename set")
	unless ( $spreadsheet_filename{$id} );

    $self->set_progname( $CONFIG->get_TAB2MAGE_PROGNAME() );
    $self->set_version( $CONFIG->get_TAB2MAGE_VERSION() );

    # Sort out our log files. N.B. the relevant directory may not have
    # been created yet, so beware of opening filehandle from these.
    my $logfile_string = File::Spec->catfile(
	$self->get_target_directory(),
	basename($self->get_spreadsheet_filename()),
    );
    $logfile_string =~ s/\.\w{3,4}$//;     # strip off the extension
    my ( $vol, $dir, $name ) = File::Spec->splitpath($logfile_string);
    $self->localize_logfiles({
	directory => $dir,
	volume    => $vol,
    });

    return;
}

sub get_readablename {

    my ($self, $datarow) = @_;

    my $readablename = q{};

    FILE_TYPE:
    foreach my $filetype ( @{ $CONFIG->get_T2M_FILE_TYPES() } )
    {    # order is important
        my $key = $EDF_FILE_PREFIX . $filetype . $EDF_FILE_SUFFIX;
        if ( $datarow->{$key} ) {
            ($readablename)
                = ( $datarow->{$key} =~ m{([^/]*?) (?: [.] \w{3})* \z}xms );
            last FILE_TYPE;    # raw is the default
        }
    }

    # Fall back to hyb id if there's no raw or normalized
    # files. Return value must be defined but can be false.
    return ($readablename || $datarow->{$EDF_HYBRIDIZATIONNAME} || q{});
}

sub initialize_bags : PRIVATE {
    my $self = shift;
    
    my %bag_of;

    $bag_of{protocol} = make_container( \&new_protocol, 1 );

    # 1 here indicates that the &new_protocol sub requires pass-through
    # of the identifier

    $bag_of{parameter}          = make_container( \&new_parameter, 1 );
    $bag_of{software}           = make_container( \&new_software );

    $bag_of{armanuf}            = make_container( \&new_armanuf );
    $bag_of{array}              = make_container( \&new_array );

    $bag_of{factorvalue}        = make_container( \&new_factorvalue );
    $bag_of{experimentalfactor} = make_container( \&new_experimentalfactor );

    $bag_of{biosource}          = make_container( \&new_biosource );
    $bag_of{biosample}          = make_container( \&new_biosample, 1 );
    $bag_of{extract}            = make_container( \&new_extract, 1 );
    $bag_of{immunoprecipitate} = make_container( \&new_immunoprecipitate, 1 );
    $bag_of{labeledextract}    = make_container( \&new_labeledextract, 1 );
    $bag_of{labelcompound}     = make_container( \&new_labelcompound );
    $bag_of{image}             = make_container( \&new_image );

    $bag_of{pba}               = make_container( \&new_physicalbioassay, 1 );
    $bag_of{mba}               = make_container( \&new_measuredbioassay, 1 );
    $bag_of{dba}               = make_container( \&new_derivedbioassay, 1 );
    $bag_of{extended_pba}      = make_container( \&new_physicalbioassay, 1 );
    $bag_of{datamatrix_mba}    = make_container( \&new_measuredbioassay, 1 );
    $bag_of{datamatrix_dba}    = make_container( \&new_derivedbioassay, 1 );

    $bag_of{mbad}              = make_container( \&new_measuredbioassaydata );
    $bag_of{dbad}              = make_container( \&new_derivedbioassaydata );
    $bag_of{datamatrix_mbad}   = make_container( \&new_measuredbioassaydata );
    $bag_of{datamatrix_dbad}   = make_container( \&new_derivedbioassaydata );

    $bag_of{bam}               = make_container( \&new_bioassaymap );
    $bag_of{qtm}               = make_container( \&new_quantitationtypemap );

    $bag_of{quantitationtype}  = make_container( \&new_quantitationtype );
    $bag_of{qtd}               = make_container( \&new_qtd );

    $bag_of{people}            = make_container( \&new_person );

    $self->set_bags( \%bag_of );

    return \%bag_of;
}

sub write_mageml {
    my ($self) = @_;

    # Get a hashref of containers for MAGE objects.
    my $bag_of = $self->get_bags();

    # Create the target directory.
    mkpath( [ $self->get_target_directory() ], 0, oct(775) );

    $self->logprint(
	'tab2mage',
	sprintf("Parsing spreadsheet %s...\n\n", $self->get_spreadsheet_filename() ),
    );

    # Make a note if standalone mode was used
    if ( $self->get_is_standalone() ) {
	$self->logprint(
	    'tab2mage',
	    "Standalone mode. Feature identifiers will be formatted without reference to ArrayExpress.\n"
	);

	# Potentially catastrophic - not allowed in automated pipelines
	$self->add_error($CONFIG->get_ERROR_PARSEBAD());
    }

    # Parse the EDF
    $self->read_edf();

    #########################
    # Add the new protocols #
    #########################
    $self->create_protocols();

    # Set default experiment accession and name. This must be done after
    # the protocol accessions have been reassigned, above, and before
    # further processing.
    my $exptinfo  = $self->get_expt_section();
    my $namespace = $self->get_namespace();
    $exptinfo->{$EDF_EXPTACCESSION} ||= '{UNASSIGNED}';
    $exptinfo->{$EDF_EXPTNAME}      ||= '{NONE}';

    # This sets up a handler object for DED and QTD-related processing
    my $parser = ArrayExpress::Datafile::Parser->new({
	namespace            => "$namespace:$exptinfo->{$EDF_EXPTACCESSION}",
	include_known_qts    => $self->get_include_default_qts(),
	quantitation_types   => $self->get_qt_filename(),
	use_binary_datafiles => ! $self->get_use_plain_text(),
	is_standalone        => $self->get_is_standalone(),
	output_directory     => $self->get_target_directory(),
	source_directory     => $self->get_source_directory(),
	error_fh             => $self->log_fh('tab2mage'),
	allow_undef_qts      => $self->get_keep_all_qts(),
	protocol_bag         => $bag_of->{protocol},
	parameter_bag        => $bag_of->{parameter},
	reporter_prefix      => $self->get_reporter_prefix(),
	compseq_prefix       => $self->get_compseq_prefix(),	
	ignore_size_limits   => $self->get_ignore_size_limits(),
    });

    # Initial file  and sanity check
    $self->_file_sanity_check();

    $self->set_target_directory(
	File::Spec->rel2abs( $self->get_target_directory() )
    );
    my $output_file = File::Spec->catfile(
	$self->get_target_directory(),
	"$exptinfo->{$EDF_EXPTACCESSION}.xml"
    );

    ###################################################
    # Top-level MAGE object and the Experiment object #
    ###################################################
    my $mage = Bio::MAGE->new();

    my ( $experiment, $experimentdesign )
	= $self->top_level_experiment();

    ###########################
    # Start Main Datarow Loop #
    ###########################

    # Hashref for tracking processed files
    my $file_seen = {};

    # Array of raw/norm data matrices (e.g. Illumina)
    my @rewritten_data_matrices;

    foreach my $datarow ( @{ $self->get_hyb_section() } ) {

	##############
	# Data Files #
	##############

	my @datarow_files;

	# START FILE LOOP
	# NB. the values determined here will be used in BioAssayData later.
	FILETYPE:
	foreach my $filetype ( @{ $CONFIG->get_T2M_FILE_TYPES() } ) {

	    # Skip this file type if not found in data row. Set the
	    # file name and path.
	    my $filename
		= $datarow->{ $EDF_FILE_PREFIX . $filetype . $EDF_FILE_SUFFIX }
		    or next FILETYPE;

	    my $path = get_filepath_from_uri(
		$filename,
		$self->get_source_directory(),
	    );

	    my $file = ArrayExpress::Datafile->new({
		path            => $path,
		name            => basename( $path ),
		data_type       => $filetype,
		array_design_id => $datarow->{$EDF_ARRAYACCESSION},
	    });
	    
	    # Process each file just once
	    next FILETYPE if $file_seen->{ $file->get_name() }++;

	    # Sort out our DesignElementDimension and get some column heading
	    # info; the $file hashref is populated.  This also generates the
	    # stripped file for loading
	    $self->add_error($parser->parse( $file, $datarow ));

	    # Keep non-transformed data matrices for later
	    if (   $file->get_data_type() eq $CONFIG->get_RAW_DM_FILE_TYPE()
	        || $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE() ) {

		# Process this later, after all the PBA objects have been created.
		push( @rewritten_data_matrices, [ $file, $datarow ] );

	    }
	    else {

		# QuantitationType and QuantitationTypeDimension for
		# per-hyb data.
		my ( $qtlist, $qtd_key ) = $self->qts_from_names( $file );
	    
		my $qtd = $bag_of->{qtd}->(
		    $qtd_key,
		    {   qt_list             => $qtlist,
			identifier_template =>
			    "$namespace.$exptinfo->{$EDF_EXPTACCESSION}."
				. unique_identifier
			    }
		);
		$file->set_mage_qtd($qtd);

		# Add to the list of per-hyb data files for later
		# BioAssay creation.
		push( @datarow_files, $file );
	    }

	}

	# END FILE LOOP

	my $identifier_template
	    = "$namespace:$exptinfo->{$EDF_EXPTACCESSION}." . unique_identifier;

	$self->create_mage_objects(
	    $datarow, $identifier_template, \@datarow_files
	);

    }

    # Make sure all pooling protocols are linked to something.
    my $identifier_template
	= "$namespace:$exptinfo->{$EDF_EXPTACCESSION}";
    $self->link_unused_pool_protocols($identifier_template);

    # END MAIN DATAROW LOOP

    ######################
    # DataMatrix parsing #
    ######################

    # Measured DMs first.
    foreach my $matrix (@rewritten_data_matrices) {

	my ($file, $datarow) = @$matrix;

	# Some raw files may be DataMatrix-style, e.g. Illumina.
	my $identifier_template
	    = "$namespace:$exptinfo->{$EDF_EXPTACCESSION}";
	
	# FIXME add support for MeasuredBioAssay creation here.
	my $dm = ArrayExpress::Datafile::DataMatrix->new();
	$dm->create_mage( $file, $datarow, $self, $identifier_template );
    }

    # Loop over the Hybridization section again, mapping DataMatrix bioassays to
    # those already created in $bag_of.
    DATAROW:
    foreach my $datarow ( @{ $self->get_hyb_section() } ) {

	my $filename = $datarow->{
	    $EDF_FILE_PREFIX
	  . $CONFIG->get_FGEM_FILE_TYPE()
          . $EDF_FILE_SUFFIX
        }
	    or next DATAROW;

	my $path;
	if ( my $dir = $self->get_source_directory() ) {
	    $path = File::Spec->catfile( $dir, $filename );
	}
	else {
	    $path = File::Spec->rel2abs($filename);
	}

	my $file = ArrayExpress::Datafile->new({
	    path            => $path,
	    name            => ( File::Spec->splitpath($path) )[2],
	    data_type       => $CONFIG->get_FGEM_FILE_TYPE(),
	    array_design_id => $datarow->{$EDF_ARRAYACCESSION},
	});

	# Do each file just once
	next DATAROW if $file_seen->{ $file->get_name() }++;

	# Sort out our DesignElementDimension and get some column heading
	# info; the $file hashref is populated.  This also generates the
	# stripped file for loading.

	$self->add_error($parser->parse( $file, $datarow ));

	my $identifier_template = "$namespace:$exptinfo->{$EDF_EXPTACCESSION}";

	my $dm = ArrayExpress::Datafile::DataMatrix->new();
	$dm->create_mage( $file, $datarow, $self, $identifier_template );
	
    }

    # END DataMatrix parsing

    # Add references to all BioAssays and BioAssayData to the Experiment object
    foreach my $ba_type qw(pba mba dba extended_pba datamatrix_mba datamatrix_dba) {
	if ( @{ $bag_of->{$ba_type}->() } ) {
	    $experiment->addBioAssays( @{ $bag_of->{$ba_type}->() } );
	}
    }
    foreach my $ba_data_type qw(mbad dbad datamatrix_mbad datamatrix_dbad) {
	if ( @{ $bag_of->{$ba_data_type}->() } ) {
	    $experiment->addBioAssayData( @{ $bag_of->{$ba_data_type}->() } );
	}
    }

    # Insert the FactorValues (may be redundant FIXME)
    $experimentdesign->setExperimentalFactors( $bag_of->{experimentalfactor}->() );

    # This next line uses MAGE1.1 features if available, otherwise 1.0
    my $mage_version;    # used to set the correct PUBLIC ID in the output
    if ( eval { $experiment->setExperimentDesigns( [$experimentdesign] ) } ) {
	$mage_version = "1.1";
    }
    else {
	$experiment->setExperimentDesign($experimentdesign);
	$mage_version = "1.0";
    }

    # Insert the top-level Experiment object into our MAGE container
    $mage->add_objects( [$experiment] );

    # Insert all the objects stored in our containers
    foreach my $bag ( values %$bag_of ) {
	$mage->add_objects( $bag->() );
    }

    my ( $ded_fh, $dummy_id );
    if ( $parser->get_ded_count() ) {

	$ded_fh = $parser->get_feature_fh();

	# Create a placeholder for the insertion of DEDs
	$dummy_id = 'DUMMY';
	my $dummy_FD = Bio::MAGE::BioAssayData::FeatureDimension->new(
	    identifier => $dummy_id,
	    name       => $dummy_id,
	);
	$mage->add_objects( [$dummy_FD] );
    }
    else {
	print STDOUT ("No DesignElementDimension found.\n");
    }

    # Construct the MAGE structure
    my $mage_identifier = sprintf(
	"MAGE:%s:%s v%s:%s:%s",
	$exptinfo->{$EDF_EXPTDOMAIN},
	$CONFIG->get_TAB2MAGE_PROGNAME(),
	$CONFIG->get_TAB2MAGE_VERSION(),
	$exptinfo->{$EDF_EXPTACCESSION},
	$exptinfo->{$EDF_EXPTNAME}
    );
    $mage->identifier( $mage_identifier );

    # Write out everything but the DED
    print STDOUT ("Writing out initial MAGE-ML...\n");
    my $mage_fh = IO::File->new_tmpfile;
    write_mage( $mage, $mage_fh, 'tab delimited', $mage_version );

    # Rewind the temporary filehandle
    seek( $mage_fh, 0, 0 );

    # Fix the DED using classical text processing (lower memory overhead)
    print STDOUT ("Inserting DesignElementDimension where found...\n");
    my $output_fh = IO::File->new( $output_file, '>' )
	or die("Error: cannot open output file $output_file: $!\n");

    postprocess_mage(
	$ded_fh,
	$mage_fh,
	$output_fh,
	$dummy_id,
	$parser->get_cel_types(),
    );

    return wantarray
	? ( $mage, $exptinfo->{$EDF_EXPTACCESSION}, $self->get_error() )
	: $mage ;
}

sub create_mage_objects {

    my ( $self, $datarow, $identifier_template, $datarow_files )
        = @_;

    my $bag_of    = $self->get_bags();
    my $namespace = $self->get_namespace();
    my $exptinfo  = $self->get_expt_section();

    # To be used in names. Raw filename minus extensions used as default
    my $readablename = $self->get_readablename($datarow);

    ################
    # FactorValues #
    ################
    my @fvlist;

    # Sort here is important for $readablename consistency
    DATAROW_COLUMN:
    foreach my $key ( sort keys %$datarow ) {

        # Skip empty values (i.e. null factorvalues are not created;
        # zeroes are allowed).
        next DATAROW_COLUMN
            if ( !defined( $datarow->{$key} )
            || $datarow->{$key} =~ $RE_EMPTY_STRING );

        my $regexp_str = "$EDF_FV_PREFIX(.*)$EDF_FV_SUFFIX";
        $regexp_str =~ s{$RE_SQUARE_BRACKETS}{\\$1}gxms;

        # Strict matching should be okay here; we've processed the
        # headings in read_edf.
        if ( ( my $category ) = ( $key =~ m/^$regexp_str$/ ) ) {

            # Most of this next line is now non-functional, as we're no
            # longer implicitly creating null FVs. Kept for reference
            # purposes.
            my $value =
                ( defined( $datarow->{$key} ) && $datarow->{$key} ne q{} )
                ? $datarow->{$key}
                : 'null';    # This should take care of null factor values

            my ( $fv, $ef ) = $bag_of->{factorvalue}->(

		# This is used for tracking FVs internally.
                "$category.$value",
                {   category        => $category,
                    value           => $value,
                    namespace       => $namespace,
                    exptaccession   => $exptinfo->{$EDF_EXPTACCESSION},
		    expt_factor_bag => $bag_of->{experimentalfactor},
                },
            );

            push( @fvlist, $fv );
        }
    }

    ###############################
    # Set up the pooling protocol #
    ###############################
    my $pooling_protocol;
    if ( $datarow->{$EDF_POOL_PROTOCOL} ) {
        $pooling_protocol = $bag_of->{protocol}->(
            $datarow->{$EDF_POOL_PROTOCOL},
            { protocol_type => $OE_VAL_POOL }
        );
    }

    ################
    ################
    # BioMaterials #
    ################
    ################

    #############
    # BioSource #
    #############
    my ( $biosource, @characteristics, $tracker_key, $biosource_name );

    # Sort here is important for BioSource tracking.
    DATAROW_COLUMN:
    foreach my $key ( sort keys %$datarow ) {

        # Skip empty columns. Note that a zero is not empty.
        next DATAROW_COLUMN
            unless ( defined( $datarow->{$key} )
            && ( $datarow->{$key} ne q{} ) );

        my $regexp_str = "$EDF_BMC_PREFIX(.*)$EDF_BMC_SUFFIX";
        $regexp_str =~ s{$RE_SQUARE_BRACKETS}{\\$1}gxms;

        if ( ( my $category ) = ( $key =~ m/^$regexp_str$/ ) ) {

            my $bmc = new_ontologyentry(
                {   category => $category,
                    value    => $datarow->{$key}
                }
            );
            push( @characteristics, $bmc );
            $tracker_key .= $datarow->{$key};
        }
    }

    if ( $datarow->{$EDF_BIOSOURCENAME} ) { # If we're given a name, we use it

        $biosource_name = $tracker_key = $datarow->{$EDF_BIOSOURCENAME};

    }
    else {    # Otherwise we assume that the combination of
              # BioMaterialCharacteristics defines a unique BioSource,
              # and make up a name for it.

        $biosource_name = "BioSource: " . ( $readablename || q{} );

    }

    # Only actually define a biosource if it's given in this datarow
    if (   $tracker_key
        || $datarow->{$EDF_BIOSOURCETYPE} ) {
        $biosource = $bag_of->{biosource}->(
            ( $tracker_key || $biosource_name ), # Fall back to name if no BMC
            {   material_type       => $datarow->{$EDF_BIOSOURCETYPE},
                characteristics     => \@characteristics,
                identifier_template => $identifier_template,
                name                => $biosource_name,
		description         => $datarow->{$EDF_BIOSOURCEDESCRIPTION},
            }
        );
    }

    ##########
    # Sample #
    ##########
    my ( $biosample, $biosample_name );
    if (   $datarow->{$EDF_BIOSAMPLENAME}
        || $datarow->{$EDF_GROW_PROTOCOL}
        || $datarow->{$EDF_TREAT_PROTOCOL}
        || $datarow->{$EDF_POOL_PROTOCOL}
        || $datarow->{$EDF_BIOSAMPLETYPE} ) {

        # Protocol and Parameter arrays have to be kept in sync.
        my $sample_protocols  = [];
        my $sample_parameters = [];
        $datarow->{$EDF_GROW_PROTOCOL} && do {
            push(
                @$sample_protocols,
                $bag_of->{protocol}->(
                    $datarow->{$EDF_GROW_PROTOCOL},
                    { protocol_type => $OE_VAL_GROW }
                )
            );
            push( @$sample_parameters,
                ( $datarow->{$EDF_SAMPLE_PARAMS} || {} ) );
        };
        $datarow->{$EDF_TREAT_PROTOCOL} && do {
            push(
                @$sample_protocols,
                $bag_of->{protocol}->(
                    $datarow->{$EDF_TREAT_PROTOCOL},
                    { protocol_type => $OE_VAL_SPECIFIEDBIOMATERIALACTION }
                )
            );
            push( @$sample_parameters,
                ( $datarow->{$EDF_TREAT_PARAMS} || {} ) );
        };

        $biosample_name = $datarow->{$EDF_BIOSAMPLENAME}
            || $biosource_name
            || "Sample: $readablename";
        $biosample = $bag_of->{biosample}->(
            $biosample_name,
            {   derived_from        => $biosource,
                material_type       => $datarow->{$EDF_BIOSAMPLETYPE},
                protocols           => $sample_protocols,
                parameters          => $sample_parameters,
                parameter_bag       => $bag_of->{parameter},
                identifier_template => $identifier_template,
                pooling             => $pooling_protocol,
            }
        );

	# Check whether we need to cache the pooling protocol.
	if ( $pooling_protocol ) {
	    $self->check_pool_protocol_usage(
		$pooling_protocol,
		$biosample,
	    );
	}
    }

    ###########
    # Extract #
    ###########
    my ( $extract, $extract_name );
    if (   $datarow->{$EDF_EXTRACTNAME}
        || $datarow->{$EDF_EXTRACT_PROTOCOL}
        || $datarow->{$EDF_EXTRACTTYPE} ) {
        my $extract_protocols = [];
        $datarow->{$EDF_EXTRACT_PROTOCOL} && do {
            push(
                @$extract_protocols,
                $bag_of->{protocol}->(
                    $datarow->{$EDF_EXTRACT_PROTOCOL},
                    { protocol_type => $OE_VAL_NUCLEICACIDEXTRACTION }
                )
            );
        };
               $extract_name = $datarow->{$EDF_EXTRACTNAME}
            || $biosample_name
            || $biosource_name
            || "Extract: $readablename";
        $extract = $bag_of->{extract}->(
            $extract_name,

            # Degrade gracefully...
            {   derived_from => ( $biosample || $biosource ),
                material_type => $datarow->{$EDF_EXTRACTTYPE},
                protocols     => $extract_protocols,
                parameters    => [ $datarow->{$EDF_EXTRACT_PARAMS} || {} ],
                parameter_bag => $bag_of->{parameter},
                identifier_template => $identifier_template,
                pooling             => $pooling_protocol
            }
        );

	# Check whether we need to cache the pooling protocol.
	if ( $pooling_protocol ) {
	    $self->record_pool_protocol_assn(
		$pooling_protocol,
		$extract,
	    );
	    $self->check_pool_protocol_usage(
		$pooling_protocol,
		$extract,
	    );
	}
    }

    #####################################
    # Immunoprecipitate (ChIP-specific) #
    #####################################
    my ( $labeled_extract, $ip );
    if (   $datarow->{$EDF_IMMUNOPRECIPITATENAME}
        || $datarow->{$EDF_IP_PROTOCOL}
        || $datarow->{$EDF_IMMUNOPRECIPITATETYPE} ) {
        my $ip_protocols = [];
        $datarow->{$EDF_IP_PROTOCOL} && do {
            push(
                @$ip_protocols,
                $bag_of->{protocol}->(
                    $datarow->{$EDF_IP_PROTOCOL},
                    { protocol_type => $OE_VAL_IMMUNOPRECIPITATE }
                )
            );
        };

        my $ip_name;
        unless ( $ip_name = $datarow->{$EDF_IMMUNOPRECIPITATENAME} ) {
                   $ip_name = $extract_name
                || $biosample_name
                || $biosource_name
                || $readablename;
            $ip_name .= ' IP';
        }

        $ip = $bag_of->{immunoprecipitate}->(
            $ip_name,

            # Degrade gracefully...
            {   derived_from => ( $extract || $biosample || $biosource ),
                material_type       => $datarow->{$EDF_IMMUNOPRECIPITATETYPE},
                protocols           => $ip_protocols,
                parameters          => [ $datarow->{$EDF_IP_PARAMS} || {} ],
                parameter_bag       => $bag_of->{parameter},
                identifier_template => $identifier_template,
                pooling             => $pooling_protocol
            }
        );

	# Check whether we need to cache the pooling protocol.
	if ( $pooling_protocol ) {
	    $self->check_pool_protocol_usage(
		$pooling_protocol,
		$ip,
	    );
	}

        ########################
        # LabeledExtracts (IP) #
        ########################
        if (   $datarow->{$EDF_LABELEDEXTRACTNAME}
            || $datarow->{$EDF_LABEL_PROTOCOL}
            || $datarow->{$EDF_LABELEDEXTRACTTYPE} ) {
            my $labeled_extract_protocols = [];
            $datarow->{$EDF_LABEL_PROTOCOL} && do {
                push(
                    @$labeled_extract_protocols,
                    $bag_of->{protocol}->(
                        $datarow->{$EDF_LABEL_PROTOCOL},
                        { protocol_type => $OE_VAL_LABELING }
                    )
                );
            };
            my $labeled_extract_name = $datarow->{$EDF_LABELEDEXTRACTNAME}
                || "$ip_name $datarow->{$EDF_DYE}";
            $labeled_extract = $bag_of->{labeledextract}->(
                $labeled_extract_name,

                # We know we have $ip...
                {   derived_from  => $ip,
                    material_type => $datarow->{$EDF_LABELEDEXTRACTTYPE},
                    protocols     => $labeled_extract_protocols,
                    parameters    => [ $datarow->{$EDF_LABEL_PARAMS} || {} ],
                    parameter_bag => $bag_of->{parameter},
                    compound_bag  => $bag_of->{labelcompound},
                    identifier_template => $identifier_template,
                    dye                 => $datarow->{$EDF_DYE},
                    pooling             => $pooling_protocol
                }
            );
        }

    }
    else {

        ############################
        # LabeledExtracts (normal) #
        ############################
        if (   $datarow->{$EDF_LABELEDEXTRACTNAME}
            || $datarow->{$EDF_LABEL_PROTOCOL}
            || $datarow->{$EDF_LABELEDEXTRACTTYPE} ) {
            my $labeled_extract_protocols = [];
            $datarow->{$EDF_LABEL_PROTOCOL} && do {
                push(
                    @$labeled_extract_protocols,
                    $bag_of->{protocol}->(
                        $datarow->{$EDF_LABEL_PROTOCOL},
                        { protocol_type => $OE_VAL_LABELING }
                    )
                );
            };

            my $labeled_extract_name;
            unless ( $labeled_extract_name = $datarow->{$EDF_LABELEDEXTRACTNAME} ) {
                       $labeled_extract_name = $extract_name
                    || $biosample_name
                    || $biosource_name
                    || "Label: $readablename";
                $labeled_extract_name .= " $datarow->{$EDF_DYE}"
                    if $datarow->{$EDF_DYE};
            }

            $labeled_extract = $bag_of->{labeledextract}->(
                $labeled_extract_name,
                {   derived_from => ( $extract || $biosample || $biosource ),
                    material_type => $datarow->{$EDF_LABELEDEXTRACTTYPE},
                    protocols     => $labeled_extract_protocols,
                    parameters    => [ $datarow->{$EDF_LABEL_PARAMS} || {} ],
                    parameter_bag => $bag_of->{parameter},
                    compound_bag  => $bag_of->{labelcompound},
                    identifier_template => $identifier_template,
                    dye                 => $datarow->{$EDF_DYE},
                    pooling             => $pooling_protocol
                }
            );
        }
    }

    # Check whether we need to cache the pooling protocol.
    if ( $pooling_protocol ) {
	$self->check_pool_protocol_usage(
	    $pooling_protocol,
	    $labeled_extract,
	);
    }

    #########
    # Array #
    #########
    my $array;
    if ( $datarow->{$EDF_ARRAYACCESSION} ) {

        $array = $bag_of->{array}->(
            ($datarow->{$EDF_HYBRIDIZATIONNAME} || $readablename),
            {   identifier_template => $identifier_template,
                arrayaccession      => $datarow->{$EDF_ARRAYACCESSION},
                serial              => $datarow->{$EDF_ARRAYSERIAL},
		armanuf_bag         => $bag_of->{armanuf},
            },
        );
    }

    # END Array

    #############################
    #############################
    # BioAssayData and BioAssay #
    #############################
    #############################

    # NB uses information from the datafile parsing at the start of the
    # datarow loop. Declared here so DBA[D] can map to MBA[D]
    my $mbad;

    # These default to raw filename if available. $hyb_name is used as
    # both internal PBA id and PBA name attribute.
    my $hyb_name = $datarow->{$EDF_HYBRIDIZATIONNAME} || $readablename;

    # $scan_name is used as the internal id of the MBA object.
    my $scan_name;
    if ( $datarow->{$EDF_SCANNAME} ) {
	$scan_name = $datarow->{$EDF_SCANNAME};
    }
    elsif ( my $hname = $datarow->{$EDF_HYBRIDIZATIONNAME} ) {

	# These fallbacks are required for accurate raw datamatrix
	# (e.g. Illumina) processing.
	if ( my $fname
	     = $datarow->{ $EDF_FILE_PREFIX . 'raw' . $EDF_FILE_SUFFIX } ) {
	    $scan_name = "$hname $fname";
	}
	else {
	    $scan_name = $hname;
	}
    }
    else {
	$scan_name = $datarow->{ $EDF_FILE_PREFIX . 'raw' . $EDF_FILE_SUFFIX };
    }

    # $mba_name is used separately as the MBA name attribute
    # (displayed in AE web interface, so needs to be kept simple).
    my $mba_name = $datarow->{$EDF_SCANNAME} || $hyb_name;

    # $norm_name is both the internal ID of the DBA and the DBA name attribute.
    my $norm_name = $datarow->{$EDF_NORMALIZATIONNAME}
        || $datarow->{ $EDF_FILE_PREFIX . 'normalized' . $EDF_FILE_SUFFIX };

    ####################################
    # PhysicalBioAssay (Hybridization) #
    ####################################

    my $pba;

    # Only do this if hybname available, otherwise we risk null
    # container identifiers.
    if ($hyb_name) {
        my ( $hyb_protocol, $scan_protocol );
        $datarow->{$EDF_HYB_PROTOCOL} && do {
            $hyb_protocol = $bag_of->{protocol}->(
                $datarow->{$EDF_HYB_PROTOCOL},
                { protocol_type => $OE_VAL_HYBRIDIZATION }
            );
        };

        # If both scan and fext protocols available, add scanning
        # here. Otherwise attach scanning to feature_extraction below.
        ( $datarow->{$EDF_SCAN_PROTOCOL} && $datarow->{$EDF_FEXT_PROTOCOL} )
            && do {
            my $ptype = $OE_VAL_SCANNING;
            $scan_protocol = $bag_of->{protocol}->(
                $datarow->{$EDF_SCAN_PROTOCOL},
                { protocol_type => $ptype }
            );
            };
        my $scan_software         = q{};
        my $scan_software_version = q{};

        # If we've got a FEXT protocol then all well and good. If not, we
        # don't need a image_acquisition_software here since it will be
        # added as feature_extraction_software below.
        ( $datarow->{$EDF_SCAN_SOFTWARE} && $datarow->{$EDF_FEXT_PROTOCOL} )
            && do {
            my $name;
            ( $name, $scan_software_version )
                = $self->parse_software( $datarow->{$EDF_SCAN_SOFTWARE} );
            $scan_software = $bag_of->{software}->(
                "SCAN:$name"
                    . (
                    $scan_software_version ? ":$scan_software_version" : q{}
                    ),
                {   identifier_template =>
                        "$identifier_template.ImageAcquisition",
                    name => $name,
                    type => $OE_VAL_SCANNING_SOFTWARE
                }
            );
            };
        my $image;
        my $image_data
            = $datarow->{ $EDF_FILE_PREFIX . 'image' . $EDF_FILE_SUFFIX };
        if ($image_data) {
            $image = $bag_of->{image}->(
                $image_data,
                {   identifier_template => $identifier_template,
                    uri                 => $image_data,
                    format              => $datarow->{$EDF_IMAGE_FORMAT}
                }
            );
        }

	my $label;
	if ( $labeled_extract && $labeled_extract->getLabels() ) {

	    # Assume only one dye per label (works for Tab2MAGE; not
	    # so for MAGE-TAB).
	    $label = $labeled_extract->getLabels()->[0];
	}

        $pba = $bag_of->{pba}->(
            $hyb_name,
            {   derived_from =>
		    ( $labeled_extract || $ip || $extract || $biosample || $biosource ),
		label                    => $label,
                array                    => $array,
                identifier_template      => $identifier_template,
                factor_values            => \@fvlist,
                hybridization_protocol   => $hyb_protocol,
                hybridization_parameters => $datarow->{$EDF_HYB_PARAMS},
                scanning_protocol        => $scan_protocol,
                scanning_parameters      => $datarow->{$EDF_SCAN_PARAMS},
                parameter_bag            => $bag_of->{parameter},
                software                 => $scan_software,
                software_version         => $scan_software_version,
                image                    => $image,
                hyb_hardware             => $datarow->{$EDF_HYB_HARDWARE},
                hyb_hardware_params      => $datarow->{$EDF_HYB_HW_PARAMS},
                scan_hardware            => $datarow->{$EDF_SCAN_HARDWARE},
                scan_hardware_params     => $datarow->{$EDF_SCAN_HW_PARAMS},
		extended_pba_bag         => $bag_of->{extended_pba},
		mba_bag                  => $bag_of->{mba},
            }
        );
    }    # end if $hybname

    # Handle the two-colour hyb coding. Assumes that merged PBAs have
    # a .merged name suffix.
    if ( $pba && ( scalar @{ $pba->getChannels() || [] } > 1 ) ) {
	my $pba_name = $pba->getName();
	$pba = $bag_of->{extended_pba}->(
	    "$hyb_name.merged"
	);
	unless ( $pba ) {
	    die(sprintf(
		"Internal error: Merged PBA undefined for two-color coding (%s).",
		$pba_name)
	    );
	}
    }

    ####################
    # MeasuredBioAssay #
    ####################
    my $mba;
    if ($scan_name) {

        my $fext_protocol;
        my $fext_protocol_accession;
        (          $fext_protocol_accession = $datarow->{$EDF_FEXT_PROTOCOL}
                || $datarow->{$EDF_SCAN_PROTOCOL} )
            && do {
            $fext_protocol = $bag_of->{protocol}->(
                $fext_protocol_accession,
                { protocol_type => $OE_VAL_FEATUREEXTRACTION }
            );
            };
        my $fext_software         = q{};
        my $fext_software_version = q{};
        my $fext_sw_string        = q{};

        # If FEXT software not available, we use SCAN software (see above
        # for scanning software treatment).
        (          $fext_sw_string = $datarow->{$EDF_FEXT_SOFTWARE}
                || $datarow->{$EDF_SCAN_SOFTWARE} )
            && do {
            my $name;
            ( $name, $fext_software_version )
                = $self->parse_software($fext_sw_string);
            $fext_software = $bag_of->{software}->(
                "FEXT:$name"
                    . (
                    $fext_software_version ? ":$fext_software_version" : q{}
                    ),
                {   identifier_template =>
                        "$identifier_template.FeatureExtraction",
                    name => $name,
                    type => $OE_VAL_ANALYSIS_SOFTWARE
                }
            );
            };
        $mba = $bag_of->{mba}->(
            $scan_name,
            {   name                => $mba_name,
		physical_bioassay   => $pba,
                factor_values       => \@fvlist,
                identifier_template => $identifier_template,
                protocol            => $fext_protocol,
                parameters          => $datarow->{$EDF_FEXT_PARAMS},
                parameter_bag       => $bag_of->{parameter},
                software            => $fext_software,
                software_version    => $fext_software_version
            }
        );

    }    # end if $scan_name

    ###################
    # DerivedBioAssay #
    ###################
    my $dba;
    if ($norm_name) {

        # Allow norm without raw bioassay.
        my $source_bioassays = $mba ? [$mba] : undef;

        # BioAssayMap (below) and DBA both have the same $norm_name
        # internal identifier.
        $dba = $bag_of->{dba}->(
            $norm_name,
            {   factor_values       => \@fvlist,
                identifier_template => $identifier_template,
                normalization_type  => $datarow->{$EDF_NORMALIZATIONTYPE},
                source_bioassays    => $source_bioassays,
		bioassaymap_bag     => $bag_of->{bam},
            },
        );
    }    # end if $norm_name (dbad added below)

    ################
    # BioAssayData #
    ################

    DATAROW_FILE:
    foreach my $file ( @{ $datarow_files || [] } ) {

        # Skip if there's no filename.
        next DATAROW_FILE unless $file->get_name();

        my $de_dimension;

        DESIGN_ELEMENT_DIMENSION:
        {

            last DESIGN_ELEMENT_DIMENSION unless ( $file->get_ded_type() );

            my $dim_class
                = 'Bio::MAGE::BioAssayData::'
		. $file->get_ded_type()
		. 'Dimension';

            eval {
                $de_dimension = $dim_class->new(
                    identifier => $file->get_ded_identifier() );
                }
                or croak(
                "Error: Unrecognized DED type: "
	        . $file->get_ded_type()
		. "\n" );

            # If we've already seen this file once, $de_dimension will end
            # up empty and ignored.

        }

        if ( $file->get_data_type() eq 'raw' ) {

            ########################
            # MeasuredBioAssayData #
            ########################
            $mbad = $bag_of->{mbad}->(
                $file->get_name(),
                {   filename           => $file->get_target_filename(),
                    feature_dimension  => $de_dimension,
                    summary_statistics => $datarow->{$EDF_FEXT_STATS},
                    quantitation_type_dimension => $file->get_mage_qtd(),
                    identifier_template         => $identifier_template
                }
            );

            # Add mbad to the mba we created earlier
            my $found;
            if ( my $preexisting_mbads = $mba->getMeasuredBioAssayData ) {
                my $new_identifier = $mbad->getIdentifier;
                $found = first { $_->getIdentifier eq $new_identifier }
                    @$preexisting_mbads;
            }
            $mba->addMeasuredBioAssayData($mbad) unless $found;

            #####################
            # BioAssayDimension #
            #####################
            my $bad = Bio::MAGE::BioAssayData::BioAssayDimension->new(
                identifier =>
                    "$identifier_template.MeasuredBioAssayDimension",
                bioAssays => [$mba],
            );
            $mbad->setBioAssayDimension($bad)
                unless $mbad->getBioAssayDimension();

        }
        elsif ( $file->get_data_type() eq 'normalized' ) {

            #######################
            # DerivedBioAssayData #
            #######################
            my $protocol;
            $datarow->{$EDF_NORM_PROTOCOL} && do {
                $protocol = $bag_of->{protocol}->(
                    $datarow->{$EDF_NORM_PROTOCOL},
                    { protocol_type => $OE_VAL_NORMALIZATION }
                );
            };

            my $norm_software    = q{};
            my $software_version = q{};
            $datarow->{$EDF_NORM_SOFTWARE} && do {
                my $name;
                ( $name, $software_version )
                    = $self->parse_software( $datarow->{$EDF_NORM_SOFTWARE} );
                $norm_software = $bag_of->{software}->(
                    "NORM:$name"
                        . ( $software_version ? ":$software_version" : q{} ),
                    {   identifier_template =>
                            "$identifier_template.Normalization",
                        name => $name,
                        type => $OE_VAL_TRANSFORMATION_SOFTWARE
                    }
                );
            };

	    # BioAssayMap and DBA both have the same $norm_name
	    # internal identifier. Omit this if there are no MBAs (no
	    # mapping necessary, and the BAMs will be empty and
	    # dropped from the output MAGE-ML).
            my @bams;
	    my $bam = $bag_of->{bam}->($norm_name);
	    if ( $mba && $bam ) {

		# same name used here and for DBA above
		push @bams, $bam;
	    }

            # BioAssayDimension
            my $bad = Bio::MAGE::BioAssayData::BioAssayDimension->new(
                identifier => "$identifier_template.DerivedBioAssayDimension",
                bioAssays  => [$dba],
            );

            # Source QTs for QTMapping
            my %source_qts;
            if ( $mbad
                && ( my $source_qtd = $mbad->getQuantitationTypeDimension ) )
            {
                foreach my $qt ( @{ $source_qtd->getQuantitationTypes } ) {
                    $source_qts{ $qt->getIdentifier } = $qt;
                }
            }

            my $source_bioassays
                = $mbad ? [$mbad] : undef;    # Allow norm data with no raw
            my $dbad = $bag_of->{dbad}->(
                $file->get_name(),
                {   filename                => $file->get_target_filename(),
                    de_dimension            => $de_dimension,
                    qt_dimension            => $file->get_mage_qtd(),
                    ba_dimension            => $bad,
                    identifier_template     => $identifier_template,
                    protocol                => $protocol,
                    parameters              => $datarow->{$EDF_NORM_PARAMS},
                    parameter_bag           => $bag_of->{parameter},
                    summary_statistics      => $datarow->{$EDF_NORM_STATS},
                    source_bioassay_dataset => $source_bioassays,
                    bioassay_map            => \@bams,
                    software                => $norm_software,
                    software_version        => $software_version,
                    source_qts              => [ sort values %source_qts ],
                    qtm_bag                 => $bag_of->{qtm},
                }
            );

            # Add dbad to the dba we created earlier
            $self->dba_add_data( $dba, $dbad );

        }
        else {

            croak(    "Error: Unsupported file type: "
                    . $file->get_data_type()
                    . "\n" );

        }
    }

    return $bag_of;
}

sub dba_add_data {

    my ( $self, $dba, $dbad ) = @_;

    # Check that the DBAD is not already present, add if not.
    my $found;
    if ( my $preexisting_dbads = $dba->getDerivedBioAssayData ) {
        my $new_identifier = $dbad->getIdentifier;
        $found = first { $_->getIdentifier eq $new_identifier }
            @$preexisting_dbads;
    }
    $dba->addDerivedBioAssayData($dbad) unless $found;

    return;
}

sub parse_software {
    my ($self, $software_string) = @_;

    # Software name is everthing up to the first parenthesis
    my $name = ( split /\s*\(/, $software_string )[0];

    # Software version is within parens
    my ($software_version) = ( $software_string =~ $RE_WITHIN_PARENTHESES );

    return ( $name, $software_version );
}

sub qts_from_names {

    my ( $self, $file, $qt_names ) = @_;

    my $bag_of = $self->get_bags();

    $qt_names ||= $file->get_column_headings();

    my ( @qtd_qtlist, $qtd_key );
    my ( $QT_prefix, $software );

    if ( $file->get_format_type() eq 'Affymetrix' ) {
        $QT_prefix = 'Affymetrix:QuantitationType:';
	$software  = 'Affymetrix';
    }
    elsif ( my ( $type, $qt_namespace )
        = ( $file->get_qt_type =~ m{\A (.*?) \[ (.*) \] \z}xms ) ) {
        $QT_prefix = "${qt_namespace}:${type}_QuantitationType:";
	$software  = $type;
    }
    else {
        $QT_prefix = sprintf('Unknown:%s_QuantitationType:', $file->get_qt_type());
	$software  = $file->get_qt_type() eq 'Ambiguous'
	           ? 'Unknown'
		   : $file->get_qt_type();
    }

    foreach my $heading (@$qt_names) {

        my $identifier       = $QT_prefix . $heading;
        my $quantitationtype = $bag_of->{quantitationtype}->(
            $identifier,
            {   identifier   => $identifier,
                prefix       => $QT_prefix,
                name         => $heading,
		software     => $software,
                data_metrics => $file->get_data_metrics(),
                container    => $bag_of->{quantitationtype},
            }
        );
        push( @qtd_qtlist, $quantitationtype );

        $qtd_key .= $heading;

    }

    # Quick check for sane QTs (This error should no longer be triggered
    # by missing confidence indicator targets - see MAGE.pm).
    foreach my $qt ( @{ $bag_of->{quantitationtype}->() } ) {
        unless ( $qt->getIdentifier ) {
            print STDERR (<<'END_ERROR');
ERROR: At least one QuantitationType found lacking an identifier.
Please check that the confidence indicator targets in your QT file 
are represented in the data files.

END_ERROR

            croak(    qq{Error occurred at data file }
                    . $file->get_name()
                    . q{QT name: "}
                    . $qt->getName()
                    . q{"} );
        }
    }

    $qtd_key ||= 'NULL';    # In case of no QTs, e.g. missing file

    return ( \@qtd_qtlist, $qtd_key );
}

sub top_level_experiment : PRIVATE {

    my ( $self ) = @_;

    my $people_carrier = $self->get_bags()->{people};
    my $namespace      = $self->get_namespace();
    my $exptinfo       = $self->get_expt_section();

    my $experiment       = $self->new_experiment();
    my $experimentdesign = $self->new_experimentdesign();

    # Just create objects for the submitter, data coder and/or curator.
    my $submitter = $exptinfo->{$EDF_EXPTSUBMITTER}
	|| $exptinfo->{$EDF_EXPTSUBMITTER_EMAIL};
    $submitter && do {
        $people_carrier->(
            $submitter,
            {   namespace    => $namespace,
		name         => $exptinfo->{$EDF_EXPTSUBMITTER},
                organization => $exptinfo->{$EDF_EXPTORGANIZATION},
                address      => $exptinfo->{$EDF_EXPTADDRESS},
                email        => $exptinfo->{$EDF_EXPTSUBMITTER_EMAIL},
                role         => $OE_VAL_SUBMITTER,
            }
        );
    };
    my $curator = $exptinfo->{$EDF_EXPTCURATOR}
	|| $exptinfo->{$EDF_EXPTCURATOR_EMAIL};
    $curator && do {
        $people_carrier->(
            $curator,
            {   namespace    => $namespace,
		name         => $exptinfo->{$EDF_EXPTCURATOR},
                organization => $exptinfo->{$EDF_EXPTORGANIZATION},
                address      => $exptinfo->{$EDF_EXPTADDRESS},
                email        => $exptinfo->{$EDF_EXPTCURATOR_EMAIL},
                role         => $OE_VAL_CURATOR,
            }
        );
    };
    my $datacoder = $exptinfo->{$EDF_EXPTDATACODER}
	|| $exptinfo->{$EDF_EXPTDATACODER_EMAIL};
    $datacoder && do {
        $people_carrier->(
            $datacoder,
            {   namespace    => $namespace,
		name         => $exptinfo->{$EDF_EXPTDATACODER},
                organization => $exptinfo->{$EDF_EXPTORGANIZATION},
                address      => $exptinfo->{$EDF_EXPTADDRESS},
                email        => $exptinfo->{$EDF_EXPTDATACODER_EMAIL},
                role         => $OE_VAL_DATA_CODER,
            }
        );
    };
    my $investigator = $exptinfo->{$EDF_EXPTINVESTIGATOR}
	|| $exptinfo->{$EDF_EXPTINVESTIGATOR_EMAIL};
    $investigator && do {
        $people_carrier->(
            $investigator,
            {   namespace    => $namespace,
		name         => $exptinfo->{$EDF_EXPTINVESTIGATOR},
                organization => $exptinfo->{$EDF_EXPTORGANIZATION},
                address      => $exptinfo->{$EDF_EXPTADDRESS},
                email        => $exptinfo->{$EDF_EXPTINVESTIGATOR_EMAIL},
                role         => $OE_VAL_INVESTIGATOR,
            }
        );
    };

    # Everyone goes on the Providers list
    $experiment->setProviders( $people_carrier->() );

    return ( $experiment, $experimentdesign );
}

sub new_experiment : PRIVATE {

    my ( $self ) = @_;

    my $exptinfo = $self->get_expt_section();

    my $experiment = Bio::MAGE::Experiment::Experiment->new(
        identifier => $exptinfo->{$EDF_EXPTACCESSION} );

    $exptinfo->{$EDF_EXPTNAME}
        && do { $experiment->setName( $exptinfo->{$EDF_EXPTNAME} ); };

    my $description = Bio::MAGE::Description::Description->new;

    # make sure we get some kind of BQS object
    $exptinfo->{$EDF_PUBPAGES} ||= "-";

    my $bqs = Bio::MAGE::BQS::BibliographicReference->new(
        title       => $exptinfo->{$EDF_PUBTITLE},
        pages       => $exptinfo->{$EDF_PUBPAGES},
        publication => $exptinfo->{$EDF_PUBJOURNAL},
        volume      => $exptinfo->{$EDF_PUBVOLUME},
        issue       => $exptinfo->{$EDF_PUBISSUE},
        year        => $exptinfo->{$EDF_PUBYEAR},
        authors     => $exptinfo->{$EDF_PUBAUTHORS},
        URI         => $exptinfo->{$EDF_PUBURI},
    );
    $bqs->addParameters(
        &new_ontologyentry(
            {   category => $OE_CAT_PUBLICATIONTYPE,
                value    => $OE_VAL_JOURNALARTICLE,
            }
        )
    );
    if ($exptinfo->{$EDF_PUBMEDID}) {
	my $db = Bio::MAGE::Description::Database->new(
	    identifier => 'ebi.ac.uk:Database:pubmed',
	);
	my $pubmed = Bio::MAGE::Description::DatabaseEntry->new(
	    accession  => $exptinfo->{$EDF_PUBMEDID},
	    database   => $db,
	);
	$bqs->addAccessions($pubmed);
    }
    $description->addBibliographicReferences($bqs);

    $exptinfo->{$EDF_EXPTDESCRIPTION}
        && do { $description->setText( $exptinfo->{$EDF_EXPTDESCRIPTION} ); };
    $exptinfo->{$EDF_EXPTURI}
	&& do { $description->setURI( $exptinfo->{$EDF_EXPTURI} ); };

    $experiment->addDescriptions($description);

    $exptinfo->{$EDF_EXPTRELEASEDATE} && do {
        my $date = Bio::MAGE::NameValueType->new(
            name  => 'ArrayExpressReleaseDate',
            value => $exptinfo->{$EDF_EXPTRELEASEDATE},
        );
        $experiment->addPropertySets($date);
    };
    $exptinfo->{$EDF_EXPTSUBMISSIONDATE} && do {
        my $date = Bio::MAGE::NameValueType->new(
            name  => 'ArrayExpressSubmissionDate',
            value => $exptinfo->{$EDF_EXPTSUBMISSIONDATE},
        );
        $experiment->addPropertySets($date);
    };
    $exptinfo->{$EDF_EXPTGEORELEASEDATE} && do {
        my $date = Bio::MAGE::NameValueType->new(
            name  => 'GEOReleaseDate',
            value => $exptinfo->{$EDF_EXPTGEORELEASEDATE},
        );
        $experiment->addPropertySets($date);
    };

    # Either add the curated name, or a placeholder in MAGE.
    my $arrayexpress_curated_name = Bio::MAGE::NameValueType->new(
        name  => 'AEExperimentDisplayName',
        value => ( $exptinfo->{$EDF_EXPTCURATEDNAME} || q{} ),
    );

    # We support multiple secondary accessions. This may need more
    # finesse in future.
    my @secondary_accessions;
    my @accnos = split /\s*;\s*/,
	( $exptinfo->{$EDF_EXPTSECONDARYACCESSION} || q{} );

    # At least one secondary accession NVT is required.
    $accnos[0] = q{} unless scalar @accnos;
    foreach my $acc ( @accnos ) {
	my $accession = Bio::MAGE::NameValueType->new(
	    name  => 'SecondaryAccession',
	    value => $acc,
	);
	push @secondary_accessions, $accession;
    }
    $experiment->addPropertySets(
	$arrayexpress_curated_name,
	@secondary_accessions,
    );

    return $experiment;

}

sub new_experimentdesign {

    my ( $self ) = @_;

    my $exptinfo = $self->get_expt_section();

    # Sort out ExperimentDesign
    my $experimentdesign = Bio::MAGE::Experiment::ExperimentDesign->new;

    $exptinfo->{$EDF_EXPTDESIGNTYPE} && do {

        # Split the string on commas, semicolons
        foreach
            my $type ( split /\s*[,;]\s*/, $exptinfo->{$EDF_EXPTDESIGNTYPE} )
        {
            my $experimentdesigntype = &new_ontologyentry(
                {   category => $OE_CAT_EXPERIMENTDESIGNTYPE,
                    value    => $type
                }
            );
            $experimentdesign->addTypes($experimentdesigntype);
        }
    };
    $exptinfo->{$EDF_EXPTQUALITYCONTROL} && do {

        # Split the string on commas, semicolons
        my @qualitycontrol;
        foreach my $type ( split /\s*[,;]\s*/,
            $exptinfo->{$EDF_EXPTQUALITYCONTROL} ) {
            push(
                @qualitycontrol,
                &new_ontologyentry(
                    {   category => $OE_CAT_QUALITYCONTROLDESCRIPTIONTYPE,
                        value    => $type
                    }
                )
            );
        }
        my $qcd = Bio::MAGE::Description::Description->new(
            annotations => \@qualitycontrol );
        $experimentdesign->setQualityControlDescription($qcd);
    };

    return $experimentdesign;

}

sub htmlify_protocoltext {

    # Make some simple substitutions to correctly format text loaded
    # into ArrayExpress. This subroutine is likely to be extended as
    # more substitutions are found.

    my ($self, $text) = @_;

    # The order of these substitutions is not currently important.

    # 1. Lone ampersands should be &amp;
    $text =~ s/\&(?!#?\w+;)/\&amp;/g;

    # 2. Angle brackets (that are not part of HTML tags).
    #    $text =~ s/<(?!\s*[a-z]+\s*\/?>)/\&lt;/gi;
    # Negative look-behind is fixed width, this is not trivial to implement.
    #    $text =~ s/>/\&gt;/gi;

    # 3. Newlines become <br>
    $text =~ s/(\r\n?|\n)/<br>$1/g;

    # 4. Various common non-ASCII characters:
    #   a. degrees
    $text =~ s/�/\&deg;/g;
    $text =~ s/�/\&deg;/g;
    $text =~ s/�X/\&deg;/g;

    #   b. mu
    $text =~ s/�/\&#956;/g;
    $text =~ s/��/\&#956;/g;

    #   c. apostrophes, quotes
    $text =~ s/�/\&apos;/g;    
    $text =~ s/�/\&quot;/g;    
    $text =~ s/�/\&quot;/g;    
    
    return $text;

}

sub create_protocols {

    my ( $self ) = @_;

    my $bag_of = $self->get_bags();
    foreach my $element (@{ $self->get_prot_section() } ) {

	# Quick check before we try and generate an object without an
	# internal identifier.
	unless ( defined( $element->{$EDF_PROTOCOLACCESSION} ) ) {
	    croak("Error: Protocol found without an accession");
	}

        $element->{$EDF_PROTOCOLTEXT}
            = $self->htmlify_protocoltext(
		$element->{$EDF_PROTOCOLTEXT}
	    );

        # Generate the MAGE protocol objects
        my $protocol = $bag_of->{protocol}->(
            $element->{$EDF_PROTOCOLACCESSION},
            {   protocol_type => $element->{$EDF_PROTOCOLTYPE},
                name          => $element->{$EDF_PROTOCOLNAME},
                text          => $element->{$EDF_PROTOCOLTEXT},
                parameters    => $element->{$EDF_PROTOCOLPARAMS},
                parameter_bag => $bag_of->{parameter},
            }
        );
    }

    # Reassign protocol accessions, if cache file location is set in
    # Config.yml. This works, because the protocols are still
    # referenced by their original accession in the $bag_of
    # container. Using the -K option to the script deactivates this so
    # that other features can be tested using dummy submissions.
    if ( $CONFIG->get_AUTOSUBS_DSN()
	     && ! $self->get_keep_protocol_accns() ) {

	print STDOUT ("Reassigning protocol accessions as necessary...\n");

	require ArrayExpress::AutoSubmission::DB::Protocol;

        # We require the experiment to have an accession number for
        # protocol accession reassignment.
	my $expt_accession;
        unless ( $expt_accession = $self->get_expt_section()->{$EDF_EXPTACCESSION} ) {
            croak("Error: Experiment accession number not defined.\n");
        }

        # Experiment accessions need to be in a defined format. We
        # constrain that here.
        my $t2m_expt_prefix = $CONFIG->get_T2M_EXPERIMENT_PREFIX();
        unless ( $expt_accession
            =~ m/\A $t2m_expt_prefix \d+ \z/xms ) {
            croak(      qq{Error: Experiment accession not in the form }
                      . qq{"${t2m_expt_prefix}N" (where N is an integer).\n} );
        }

	# Set the accession prefix.
	ArrayExpress::AutoSubmission::DB::Protocol->accession_prefix(
	    $CONFIG->get_T2M_PROTOCOL_PREFIX(),
	);
 
        # Schwarzian transform to sort by user-supplied accession
        my @sorted = map { $_->[0] }
            sort { $a->[1] cmp $b->[1] }
            map { [ $_, $_->getIdentifier ] } @{ $bag_of->{protocol}->() };
        foreach my $protocol (@sorted) {

            my $user_accession = $protocol->getIdentifier;

	    my $prot_accession
		= ArrayExpress::AutoSubmission::DB::Protocol->reassign_protocol(
		    $user_accession,
		    $expt_accession,
		    $protocol->getName(),
		);
	    
            $protocol->setIdentifier($prot_accession);
            if ( my $paramlist = $protocol->getParameterTypes ) {
                foreach my $parameter (@$paramlist) {
                    my $param_id = $parameter->getIdentifier;

		    # Note that this substitution will break if the
		    # parameter id format is ever changed FIXME
		    my $namespace = $self->get_namespace();
                    $param_id
                        =~ s/$namespace:$user_accession/"$namespace:$prot_accession"/e;
                    $parameter->setIdentifier($param_id);
                }
            }
        }
    }

    return;
}

sub _file_sanity_check : PRIVATE {

    my ( $self ) = @_;

    # NB FIXME so that predeclared files are not croaked upon when lacking
    # required info.

    my $datafile_rowlist = $self->get_hyb_section();

    # Give up if we've go no hybs. Maybe FIXME later so we can have
    # protocol-only subs?
    croak("Error: No data in Hybridization section.\n")
        unless ( scalar @$datafile_rowlist );

    # Check for column dependencies. Data files all need an array
    # accession number. This is used in both the DesignElementDimension
    # and in creating the correct Array package, so we need it for Affy
    # submissions as well.
    foreach my $datarow (@$datafile_rowlist) {
        foreach my $filetype (
            @{ $CONFIG->get_T2M_FILE_TYPES() },
            $CONFIG->get_FGEM_FILE_TYPE()
            ) {
            my $heading = $EDF_FILE_PREFIX . $filetype . $EDF_FILE_SUFFIX;
            if ( $datarow->{$heading} && !$datarow->{$EDF_ARRAYACCESSION} ) {
                croak(
                    "Error: Data file $datarow->{$heading} provided without Array accession number\n"
                );
            }
        }
    }

    # Check that all the files are present
    my $errorcount;
    foreach my $datarow (@$datafile_rowlist) {

        DATAROW_COLUMN:
        foreach my $column ( keys %$datarow ) {

	    # General form File[type] e.g. File[raw],
	    # File[normalized]; skip blanks

            my $regexp_str = "$EDF_FILE_PREFIX(.*)$EDF_FILE_SUFFIX";
            $regexp_str =~ s{$RE_SQUARE_BRACKETS}{\\$1}gxms;

            if ( ( my ($filetype) = ( $column =~ m/\A$regexp_str\z/ ) )
                && $datarow->{$column} ) {

                next DATAROW_COLUMN
                    if ( $filetype eq 'image' );    # Skip image files.

		# CDF files can be found in the directory defined in
		# Config.pm
                if ( $filetype eq 'cdf' ) {

		    # Correct the CDF name on the fly.
		    my $path;
		    my $cdffile = $datarow->{$column};
		    ($datarow->{$column}, $path) = find_cdf(
			$datarow->{$column},
			$self->get_source_directory(),
		    );

                    if ( !-e $path ) {
                        print STDERR ("Error: CDF file not found: $cdffile\n");
			$self->logprint(
			    'tab2mage',
			    "Error: CDF file not found: $cdffile\n"
			);
                        $errorcount++;
                    }
                }

                # All other files must be in the current working
                # directory or the source dir set using the -d option.
                else {

		    my $dir = $self->get_source_directory();
                    my $path = $dir
			? File::Spec->catfile( $dir, $datarow->{$column} )
                        : $datarow->{$column};
		    
		    if ( !-e $path ) {
			print STDERR (
			    "Error: Missing file of type $filetype: $datarow->{$column}\n"
			);
			$self->logprint(
			    'tab2mage',
			    "Error: Missing file of type $filetype: $datarow->{$column}\n"
			);
			$errorcount++;
		    }
                }
            }
        }
    }

    if ( $errorcount ) {
	croak(
	    "Files missing. See log file for details. This script has terminated.\n");
    }

    return;
}

sub read_edf {

    my ( $self, $error_fh ) = @_;

    $error_fh ||= $self->log_fh('tab2mage');

    my $exptinfo         = {};
    my $datafile_rowlist = [];
    my $protocollist     = [];

    my $spreadsheet = $self->get_spreadsheet_filename();
    if ( -B $spreadsheet ) {
        croak(
	    sprintf("Error: Spreadsheet file %s is a binary file format."
		  . " This script supports plain text only.",
		    $spreadsheet));
    }

    my $eol_char = check_linebreaks($spreadsheet)
        or croak(
	    sprintf("Error: Cannot correctly parse linebreaks in file %s",
		    $spreadsheet));
    if (    ( $eol_char eq "\015" )
         && ( $Text::CSV_XS::VERSION < 0.27 ) ) {

	# Mac linebreaks not supported by older versions of Text::CSV_XS.
	croak("Error: Mac linebreaks not supported by this version"
	    . " of Text::CSV_XS. Please upgrade to version 0.27 or higher.\n");
    }
    local $/ = $eol_char;

    my $fh = IO::File->new( $spreadsheet, '<' )
        or croak(
	    sprintf("Error opening spreadsheet file %s: %s",
		    $spreadsheet, $!));

    # Namespace:Authority string:
    my ( $namespace, $domain );

    EDF_LINE:
    while ( my $line = <$fh> ) {

        next EDF_LINE
            if ( $line =~ $RE_COMMENTED_STRING );    # Allow hash comments

        # Some default values, just in case.
        # We assign these between each section, as they are used in
        # Protocol and Hybridization.

        # This is the default as assigned by ArrayExpress:
        $domain ||= 'ebi.ac.uk';
        $namespace = "$domain:" . $CONFIG->get_TAB2MAGE_PROGNAME();

        if ( $line =~ m/Experiment [ ]? section/ixms ) {
            ( $exptinfo, $fh )
                = $self->_parse_columnwise( $fh, $eol_char );

            $self->add_error(
		validate_experiment_section(
		    [ sort keys %$exptinfo ],
		    $error_fh,
		)
	    );

	    # If the constructor was passed an accession number and/or
	    # domain, use it/them in preference to the one(s) in the
	    # spreadsheet.
	    if ( my $accession = $self->get_external_accession() ) {
		$exptinfo->{$EDF_EXPTACCESSION} = $accession;
	    }
	    if ( my $domain = $self->get_external_domain() ) {
		$exptinfo->{$EDF_EXPTDOMAIN} = $domain;
	    }

            $domain = $exptinfo->{$EDF_EXPTDOMAIN} || $domain;

            next EDF_LINE;
        }

        elsif ( $line =~ m/Protocol [ ]? section/ixms ) {
            ( $protocollist, $fh )
                = $self->_parse_rowwise( $fh, $eol_char );

            # Check first line of section only; it is representative.
	    if ( scalar @{ $protocollist || [] } ) {
		$self->add_error(
		    validate_protocol_section(
			[ sort keys %{ $protocollist->[0] } ],
			$error_fh,
		    )
		);
	    }

            # Fix parameters so they're a proper hash {name => unit}

            PROTOCOL_LINE:
            foreach my $row (@$protocollist) {
                next PROTOCOL_LINE unless $row->{$EDF_PROTOCOLPARAMS};

                # HoH: name => {identifier => q{}, unit => q{}}
                my %param2unit;

                # Split on semicolon; discard surrounding whitespace
                my @parameters = split /\s*;\s*/, $row->{$EDF_PROTOCOLPARAMS};

                foreach my $param (@parameters) {

                  # Parameter name is everything before the first parenthesis.
                    my $name = ( split /\s*\(/, $param )[0];

                    # Unit is within parens.
                    my ($unit) = ( $param =~ $RE_WITHIN_PARENTHESES );
                    $unit ||= q{};    # In case the pattern match failed.

                    my $param_id
                        = "$namespace:$row->{$EDF_PROTOCOLACCESSION}.$name.Parameter";
                    $param2unit{$name} = {
                        identifier => $param_id,
                        unit       => $unit,
                        name       => $name,
                    };
                }

                # Replace the original string with the new hash.
                $row->{$EDF_PROTOCOLPARAMS} = \%param2unit if @parameters;
            }
            next EDF_LINE;
        }

        elsif ( $line =~ m/Hybridi[sz]ation [ ]? section/ixms ) {
            ( $datafile_rowlist, $fh )
                = $self->_parse_rowwise( $fh, $eol_char );

            # Check first line of section only; it is representative.
	    if ( scalar @{ $datafile_rowlist || [] } ) {
		$self->add_error(
		    validate_hybridization_section(
			[ sort keys %{ $datafile_rowlist->[0] } ],
			$error_fh,
		    )
		);
	    }

            # Parameter support - reorganize our parameters method-wise.
            my %proto2param = (
                $EDF_GROW_PROTOCOL    => $EDF_SAMPLE_PARAMS,
                $EDF_TREAT_PROTOCOL   => $EDF_TREAT_PARAMS,
                $EDF_EXTRACT_PROTOCOL => $EDF_EXTRACT_PARAMS,
                $EDF_LABEL_PROTOCOL   => $EDF_LABEL_PARAMS,
                $EDF_IP_PROTOCOL      => $EDF_IP_PARAMS,
                $EDF_HYB_PROTOCOL     => $EDF_HYB_PARAMS,
                $EDF_SCAN_PROTOCOL    => $EDF_SCAN_PARAMS,
                $EDF_FEXT_PROTOCOL    => $EDF_FEXT_PARAMS,
                $EDF_NORM_PROTOCOL    => $EDF_NORM_PARAMS,
            );

            foreach my $row (@$datafile_rowlist) {

                # Correct common casing error for Cy-dyes
                $row->{$EDF_DYE}
                    && do { $row->{$EDF_DYE} =~ s{\A cy(\d+) \z}{Cy$1}ixms; };

                # Map parameters to protocol applications. Scan through the
                # protocoltype:

                PARAMETER_MAPPING:
                while ( my ( $protocoltype, $methodparams )
                    = each %proto2param ) {

                    next PARAMETER_MAPPING unless $row->{$protocoltype};

                    # Scan through our list of defined protocols
                    foreach my $protocol (@$protocollist) {

                        if ( $protocol->{$EDF_PROTOCOLACCESSION} eq
                               $row->{$protocoltype}
                            && $protocol->{$EDF_PROTOCOLPARAMS} ) {

                            # For each defined parameter:
                            while ( my ( $param_name, $param_hash )
                                = each %{ $protocol->{$EDF_PROTOCOLPARAMS} } )
                            {
                                my $value_column = $EDF_PARAM_PREFIX
                                    . $param_name
                                    . $EDF_PARAM_SUFFIX;

                                # Incorporate the parameter into the
                                # method-specific parameter hash.
                                if ( defined( $row->{$value_column} )
                                    && $row->{$value_column} ne q{} ) {
                                    $row->{$methodparams}
                                        { $param_hash->{identifier} }
                                        = $row->{$value_column};
                                }
                            }
                        }
                    }
                }
            }
            next EDF_LINE;
        }

        print STDERR (
            "Warning: Line below is not associated with any section:\n\t$line\n"
            )
            if ( $line =~ m/\S/ );
        $self->add_error($CONFIG->get_ERROR_INNOCENT());

    }

    # Check that we have some data parsed
    unless ( scalar( grep { defined $_ } values %$exptinfo ) ) {
        print STDERR "Warning: Experiment section not found.\n";
        $self->add_error($CONFIG->get_ERROR_PARSEBAD());
    }
    unless ( scalar @{ $protocollist || [] } ) {

	# Use STDOUT here as this is a common use case.
        print STDOUT "Warning: Protocol section not found.\n";
        $self->add_error($CONFIG->get_ERROR_MIAME());
    }
    unless ( scalar @{ $datafile_rowlist || [] } ) {
        print STDERR "Warning: Hybridization section not found.\n";
        $self->add_error($CONFIG->get_ERROR_PARSEFAIL());
    }

    # In case the domain is not set in the experiment section, set it
    # here.
    $exptinfo->{$EDF_EXPTDOMAIN} ||= $domain;

    $self->set_expt_section($exptinfo);
    $self->set_hyb_section($datafile_rowlist);
    $self->set_prot_section($protocollist);
    $self->set_namespace($namespace);

    return ( $self->get_error() );
}

sub _parse_columnwise : PRIVATE {

    my ( $self, $fh, $eol_char ) = @_;

    my $summary = {};

    # Set up a CSV parser object
    my $csv_format = Text::CSV_XS->new(
        {   sep_char    => qq{\t},
            quote_char  => qq{"},                   # default
            escape_char => qq{"},                   # default
            binary      => 1,
            eol         => ( $eol_char || "\n" ),
	    allow_loose_quotes => 1,
        }
    );

    my $larry;
    FILE_LINE:
    while ( $larry = $csv_format->getline($fh) ) {

        # Section ends on empty line.
        my $line = join( q{}, @$larry );
        last FILE_LINE if ( $line =~ $RE_EMPTY_STRING );

        # Allow hash comments.
        next FILE_LINE if ( $line =~ $RE_COMMENTED_STRING );

        # Strip all whitespace from row headings.
        $larry->[0] =~ s/\s*//g;

        # lc to allow case-insensitivity.
        $larry->[0] = lc( $larry->[0] );

        # Raise error on non-unique row tag; pass the error back, but
        # don't let our sig handler see it.
	{
	    my $sighandler = $SIG{__DIE__};
	    delete $SIG{__DIE__};
	    if ( $summary->{ $larry->[0] } ) {
		croak("ERROR: Row names must be unique: $larry->[0]\n");
	    }
	    $SIG{__DIE__} = $sighandler;
	}

        # Strip surrounding whitespace from value.
        $larry->[1] && $larry->[1] =~ s{$RE_SURROUNDED_BY_WHITESPACE}{$1}gxms;

        # Don't record empty values
        $summary->{ $larry->[0] } = $larry->[1]
            if ( defined( $larry->[1] ) && $larry->[1] !~ $RE_EMPTY_STRING );

    }

    # Check we've parsed to the end of the file.
    my ( $error, $mess ) = $csv_format->error_diag();
    unless ( $larry || $error == 2012 ) {    # 2012 is the Text::CSV_XS EOF code.
	croak(
	    sprintf(
		"Error in tab-delimited format: %s. Bad input was:\n\n%s\n",
		$mess,
		$csv_format->error_input(),
	    ),
	);
    }

    return ( $summary, $fh );

}

sub _parse_rowwise : PRIVATE {

    my ( $self, $fh, $eol_char ) = @_;

    my $tablelist = [];

    # Set up a CSV parser object
    my $csv_format = Text::CSV_XS->new(
        {   sep_char    => qq{\t},
            quote_char  => qq{"},                   # default
            escape_char => qq{"},                   # default
            binary      => 1,
            eol         => ( $eol_char || "\n" ),
	    allow_loose_quotes => 1,
        }
    );

    # Scan down to the first non-comment line
    my ( $firstline, $firstlarry );
    until ( $firstline && $firstline !~ $RE_COMMENTED_STRING ) {
        $firstlarry = $csv_format->getline($fh);
        $firstline = join( '', @{ $firstlarry || [] } );
    }

    # Check we've parsed to the end of the file.
    my ( $error, $mess ) = $csv_format->error_diag();
    unless ( $firstlarry || $error == 2012 ) {    # 2012 is the Text::CSV_XS EOF code.
	croak(
	    sprintf(
		"Error in tab-delimited format: %s. Bad input was:\n\n%s\n",
		$mess,
		$csv_format->error_input(),
	    ),
	);
    }

    # Handle the special case where there's a section heading but no
    # section.
    return ( $tablelist, $fh ) if ( $firstline =~ $RE_EMPTY_STRING );

    # Use only the headings which have a text label, check for
    # uniqueness.
    my ( @headings, %already_seen );
    foreach my $heading (@$firstlarry) {
	push( @headings, $heading );
	if ( $heading && $already_seen{$heading}++ ) {
	    croak("ERROR: Column headings must be unique: $heading\n");
	}
    }

    # Strip whitespace from column headings, lc to allow case-insensitivity
    foreach my $heading (@headings) {

        # Strip whitespace surrounding term in brackets.  NB. don't strip
        # parameter name internal whitespace - needed for mapping.
        if ( my ( $superclass, $category )
            = ( $heading =~ m{(.* \[) \s* (.*?) \s* \]}xms ) ) {
            $superclass =~ s/\s*//g;
            $superclass = lc($superclass);

            # Normalize the file type, array and protocol type terms, just in case.
            if (   $superclass eq $EDF_FILE_PREFIX
                || $superclass =~ m/\A Protocol/ixms
		|| $superclass =~ m/\A Array/ixms ) {
                $category =~ s/\s*//g;
                $category = lc($category);
            }
            $heading = "$superclass$category]";
        }

        else {
            $heading = lc($heading);
            $heading =~ s/\s*//g;
        }
    }

    # Now process each row of the section
    my $larry;
    FILE_LINE:
    while ( $larry = $csv_format->getline($fh) ) {

        # Section ends on empty line.
        my $line = join( q{}, @$larry );
        last FILE_LINE if ( $line =~ $RE_EMPTY_STRING );

        # Allow hash comments.
        next FILE_LINE if ( $line =~ $RE_COMMENTED_STRING );

        my $row          = {};
        my $last_heading = scalar @headings;

        COLUMN_VALUE:
        for ( my $i = 0; $i < $last_heading; $i++ ) {

            # Skip empty values.
            next COLUMN_VALUE unless defined( $larry->[$i] );

            # Strip whitespace.
            $larry->[$i] =~ s{$RE_SURROUNDED_BY_WHITESPACE}{$1}gxms;
            $row->{ $headings[$i] } = $larry->[$i];
        }
        push @$tablelist, $row;
    }

    # Check we've parsed to the end of the file.
    ( $error, $mess ) = $csv_format->error_diag();
    unless ( $larry || $error == 2012 ) {    # 2012 is the Text::CSV_XS EOF code.
	croak(
	    sprintf(
		"Error in tab-delimited format: %s. Bad input was:\n\n%s\n",
		$mess,
		$csv_format->error_input(),
	    ),
	);
    }

    return ( $tablelist, $fh );
}

sub check_pool_protocol_usage : PRIVATE {

    my ( $self, $protocol, $material ) = @_;

    my $prot_id = $protocol->getIdentifier();
    my $is_used;

    TREATMENT:
    foreach my $treatment ( @{ $material->getTreatments() || [] } ) {

	# Pooling treatments are always order 0 for tab2mage.
	next TREATMENT unless ( $treatment->getOrder() eq '0' );

	foreach my $protapp (
	    @{ $treatment->getProtocolApplications() || [] } ) {
	    
	    my $oldprot = $protapp->getProtocol();
	    $is_used++ if ( $prot_id eq $oldprot->getIdentifier() );
	}
    }

    if ( $is_used ) {
	$pool_protocol_usage{ident $self}{$prot_id}++;
    }

    return;
}

sub record_pool_protocol_assn : PRIVATE {

    my ( $self, $protocol, $biosample ) = @_;

    my $prot_id   = $protocol->getIdentifier();
    my $sample_id = $biosample->getIdentifier();

    # This is overly complex because the protocol identifier could
    # change, e.g. after protocol accession reassignment. So $prot_id
    # may not correctly point to a protocol object in the protocol bag
    # object.
    $pool_protocol_assns{ident $self}{ $prot_id }{ $sample_id } = {
	sample   => $biosample,
	protocol => $protocol,
    };

    return;
}

sub link_unused_pool_protocols : PRIVATE {

    my ( $self, $identifier_template ) = @_;

    PROTOCOL:
    foreach my $poolinghash ( values %{ $pool_protocol_assns{ident $self} } ) {

	foreach my $assn ( values %{ $poolinghash } ) {

	    my $protocol = $assn->{protocol};
	    my $extract  = $assn->{sample};

	    my $prot_id  = $protocol->getIdentifier();

	    next PROTOCOL if $pool_protocol_usage{ident $self}{$prot_id};

	    unless ( defined($protocol) && defined($extract) ) {
		confess("Error: either protocol or extract undefined in pooling recheck.");
	    }

	    $self->link_pool_protocol( $protocol, $extract, $identifier_template );
	}
    }

    return;
}

sub link_pool_protocol : PRIVATE {

    my ( $self, $protocol, $extract, $identifier_template ) = @_;

    my ( $treatment, $oldbmms );
    TREATMENT:
    foreach my $old ( @{ $extract->getTreatments() || [] } ) {

	# Take the first set of BMMs as being representative.
	$oldbmms ||= $old->getSourceBioMaterialMeasurements();

	# Pooling is always order 0 for tab2mage.
	if ( $old->getOrder() eq '0' ) {
	    $treatment = $old;
	    last TREATMENT;
	}
    }

    my $action = Bio::MAGE::Description::OntologyEntry->new(
        category => 'Action',
        value    => 'pool',
    );

    unless ( $treatment ) {

	my $identifier = "$identifier_template.pool." . unique_identifier() . '.Treatment';
	$treatment = Bio::MAGE::BioMaterial::Treatment->new(
	    identifier                    => $identifier,
	    order                         => 0,
	    action                        => $action,
	    sourceBioMaterialMeasurements => $oldbmms,
	);
	$extract->addTreatments( $treatment );
    }

    my $found;
    foreach my $old ( @{ $treatment->getProtocolApplications || [] } ) {
	my $oldprot = $old->getProtocol();
	$found++ if (   $protocol->getIdentifier()
		     eq $oldprot->getIdentifier() );
    }

    unless ( $found ) {
	my $pa = Bio::MAGE::Protocol::ProtocolApplication->new(
            protocol     => $protocol,
            activityDate => 'n/a',
	);
	$treatment->addProtocolApplications($pa);
    }

    return;
}

1;
