#!/usr/bin/env perl
#
# Entrez_list.pm
#
# $Id: Entrez_list.pm 1941 2008-02-11 12:27:15Z tfrayner $
#

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../index.html">
                <img src="../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: Entrez_list.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Curator::Entrez_list - a module used by expt_check.pl

=head1 SYNOPSIS

 use ArrayExpress::Curator::Entrez_list qw(parse_entrez_names);
 
 my $is_recognized = parse_entrez_names();
 die("Error: Unknown journal: $pub") unless $is_recognized->{ $pub };

=head1 DESCRIPTION

This module provides a very basic interface to the listing of approved
Entrez publication names included with the Tab2MAGE package.

=head1 FUNCTIONS

=over 2

=item C<parse_entrez_names( $fh )>

Parses the contents of either the optional passed filehandle, or the
internally-defined Entrez_list.txt filehandle, to generate a hashref
of publication name keys and true values.

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2008.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

package ArrayExpress::Curator::Entrez_list;

use strict;
use warnings;

use Carp;
use IO::File;

use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Common qw($RE_LINE_BREAK);

use base 'Exporter';
our @EXPORT_OK = qw(parse_entrez_names);

sub parse_entrez_names {

    # Optional filehandle argument, default is to read the standard
    # list ($CONFIG->get_DEFAULT_ENTREZ_FILENAME()).
    my $passed_fh = shift;

    # Get the Entrez list into memory
    my %entrez_approved;
    my $entrez_fh = $passed_fh || _entrez_list_fh();

    while ( my $line = <$entrez_fh> ) {
        $line =~ s/$RE_LINE_BREAK//xms;
        $entrez_approved{$line}++;
    }

    return \%entrez_approved;
}

sub _entrez_list_fh {

    my $filehandle
        = IO::File->new( $CONFIG->get_DEFAULT_ENTREZ_FILENAME(), '<' )
        or croak( "Error opening Entrez publications list file "
            . $CONFIG->get_DEFAULT_ENTREZ_FILENAME()
            . ": $!\n" );

    return ($filehandle);

}

1;
