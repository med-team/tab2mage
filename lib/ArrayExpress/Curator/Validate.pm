#!/usr/bin/env perl
#
# Module to provide basic methods used by the tab2mage and experiment
# checker scripts.
#
# Tim Rayner 2005, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Validate.pm 2069 2008-06-04 14:33:52Z tfrayner $
#

package ArrayExpress::Curator::Validate;

use strict;
use warnings;
use Class::Std;
use English qw( -no_match_vars );
use File::Basename;

use ArrayExpress::Curator::Entrez_list qw(
    parse_entrez_names
);

require ArrayExpress::Curator::Tab2MAGE;

use ArrayExpress::Curator::Config qw($CONFIG);

use ArrayExpress::Curator::MAGE qw(
    unique_identifier
);

use ArrayExpress::Curator::MAGE::Definitions qw(
    $EDF_EXPTACCESSION
    $EDF_EXPTDESCRIPTION
    $EDF_EXPTRELEASEDATE
    $EDF_EXPTSUBMISSIONDATE
    $EDF_EXPTGEORELEASEDATE
    $EDF_EXPTSUBMITTER
    $EDF_EXPTSUBMITTER_EMAIL
    $EDF_PUBJOURNAL
    $EDF_PUBMEDID
    $EDF_ARRAYACCESSION
    $EDF_FILE_PREFIX
    $EDF_FILE_SUFFIX
    $EDF_HYBRIDIZATIONNAME
    $EDF_FV_PREFIX
    $EDF_FV_SUFFIX
    $EDF_PROTOCOLACCESSION
    $EDF_PROTOCOLNAME
    $EDF_PROTOCOLTYPE
    $EDF_PROTOCOLTEXT
    $EDF_PROTOCOLPARAMS
    $EDF_GROW_PROTOCOL
    $EDF_TREAT_PROTOCOL
    $EDF_EXTRACT_PROTOCOL
    $EDF_POOL_PROTOCOL
    $EDF_LABEL_PROTOCOL
    $EDF_IP_PROTOCOL
    $EDF_HYB_PROTOCOL
    $EDF_SCAN_PROTOCOL
    $EDF_FEXT_PROTOCOL
    $EDF_NORM_PROTOCOL
    $EDF_TRXN_PROTOCOL
    $EDF_PARAM_PREFIX
    $EDF_PARAM_SUFFIX
);

use ArrayExpress::Curator::Report qw(
    datafile_consistency_table
    format_description
);

use ArrayExpress::Curator::Common qw(
    get_filepath_from_uri
);

require ArrayExpress::Datafile;

use base 'ArrayExpress::Curator::ExperimentChecker';

my %spreadsheet_filename : ATTR( :get<spreadsheet_filename>,    :init_arg<spreadsheet_filename>,    :default<undef> );
my %mage_generator       : ATTR( :get<mage_generator>,          :set<mage_generator>,               :default<undef> );

sub START {

    my ( $self, $id, $args ) = @_;

    unless ( $spreadsheet_filename{$id} ) {
	croak("Error: spreadsheet_filename must be set for Tab2MAGE checking.");
    }

    my $logfile_string = File::Spec->rel2abs($self->get_spreadsheet_filename());
    $logfile_string =~ s/\.\w{3,4}$//;     # strip off the extension
    my ( $vol, $dir, $name ) = File::Spec->splitpath($logfile_string);
    $self->localize_logfiles({
	directory => $dir,
	volume    => $vol,
	name      => $name,
    });

    return;
}

sub get_files_and_annotation : RESTRICTED {

    my ( $self ) = @_;

    # Quick check to see what we've been passed.
    unless ( $self->get_spreadsheet_filename() ) {
        croak(<<'END_ERROR');
Error: Tab2MAGE checker must be passed a spreadsheet_filename option.
END_ERROR
    }

    my $filelist;
    my $hyb_ids;         # Used to map FGEM column headings to hybs.
    my $norm_ids;        # Empty for MX; cannot map MX FGEM using Norm IDs.
    my $tab2mage;        # FIXME factor this out of here at some point.

    print STDOUT ("Running in Tab2MAGE mode...\n");

    $self->logprint( 'error',  "* Tab2MAGE mode *\n\n" );
    $self->logprint( 'report', "* Tab2MAGE mode *\n\n" );

    # Check the metadata from the spreadsheet, retrieve the
    # hybridization section.
    ( $filelist, $hyb_ids, $norm_ids ) = $self->validate_edf();

    $self->logprint_line( 'error', 'ADF parsing START' );

    # We need $file to have array info.
    if ( $self->get_adf_filename || $self->get_array_accession ) {

	$self->cache_user_supplied_arrays( $filelist || [] );

    }
    else {

	# Just populate the keys for now.
	my %array_designs;
	foreach my $file ( @{ $filelist || [] } ) {
	    unless ( $file->get_is_exp() ) {
		$array_designs{ $file->get_array_design_id() }++;
	    }
	}

	# populate the array designs hashref
	unless ( $self->get_is_standalone() ) {

	    my @accno_table;
	    foreach my $accno ( keys %array_designs ) {

		# ADF retrieval crashes when AE is down; we trap that here.
		$self->get_ae_arraydesign({
		    accession => $accno,
		});
	    }
	}
    }

    $self->populate_file_arraydesigns( $filelist || [] );
    
    $self->logprint_line( 'error', 'ADF parsing END' );

    return ( $filelist, $hyb_ids, $norm_ids );
}

sub check_bmchars_against_fvs : PRIVATE {

    my ( $self, $bags ) = @_;

    # Generate a list of FV OE category => value pairs
    my @fv_catvals;
    foreach my $ef ( @{ $bags->{'experimentalfactor'}->() } ) {
	foreach my $fv ( @{ $ef->getFactorValues() || [] } ) {
	    my ( $category, $value );
	    if ( my $fvvalue = $fv->getValue() ) {

		# Regular FV Value OE.
		$category = $fvvalue->getCategory();
		$value    = $fvvalue->getValue();
	    }
	    elsif ( my $measurement = $fv->getMeasurement() ) {

		# FV Measurements.
		my $efc = $ef->getCategory();
		$category = $efc->getValue();

		# CamelCase the term.
		$category =~ s/^(.)/uc($1)/e;
		$category =~ s/_(.)/uc($1)/ge;
		$category =~ s/_//;
		
		$value    = $measurement->getValue();
	    }
	    push @fv_catvals, { $category => $value };
	}
    }

    # Generate a list of BMC OE category => value pairs.
    my @bmc_catvals;
    foreach my $source ( @{ $bags->{'biosource'}->() || [] } ) {
	foreach my $char ( @{ $source->getCharacteristics() || [] } ) {

	    # We assume regular BMC OEs here (no nesting).
	    push @bmc_catvals, { $char->getCategory => $char->getValue };
	}
    }

    $self->compare_fvs_to_bmcs(\@fv_catvals, \@bmc_catvals);

    return;
}

sub _check_hybridizations : PRIVATE {

    my ( $self, $bag_of ) = @_;

    my %pba_mapcount;
    foreach my $mba ( @{ $bag_of->{mba}->() } ) {
        my $pba = $mba->getFeatureExtraction()->getPhysicalBioAssaySource;
        $pba_mapcount{ $pba->getName() }++;
    }

    foreach my $pba ( @{ $bag_of->{pba}->() } ) {

        # FactorValues
        my $fv_list = $pba->getBioAssayFactorValues();

        # Must have at least one FV
        unless ( $#$fv_list >= 0 ) {
            $self->logprint( 'error',
                      q{Warning: no FactorValue found for hybridization "}
                    . $pba->getName()
                    . qq{".\n} );
            $self->add_error( $CONFIG->get_ERROR_MIAME() );
        }

	# Hyb-level PBA checks.
	if ( my $bac = $pba->getBioAssayCreation() ) {

	    # Arrays (required by the model)
	    unless ( $bac->getArray() ) {
		$self->logprint(
		    'error',
		    q{ERROR: Hybridization "}
                    . $pba->getName()
                    . qq{" has no associated array (needs Array[accession]).\n}
		);
		$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
	    }

	    # Labeled Extracts
	    my @labels;
	    my $source_bmms = $bac->getSourceBioMaterialMeasurements();
	    foreach my $bmm (@{ $source_bmms || [] } ) {
		push( @labels, $bmm->getBioMaterial() );
	    }
	    unless (@labels) {
		$self->logprint(
		    'error',
		    q{Warning: no LabeledExtracts linked to hybridization "}
                    . $pba->getName()
                    . qq{".\n} );
		$self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
	    }
        }

	# The next couple of tests check for mappings to MBAs;
	# obviously this is made more complex when using the
	# two-colour codings. Here we query for the merged PBA.
	my $mappable_pba;
	if ( scalar @{ $pba->getChannels() || [] } > 1 ) {

	    # This assumes the convention established in the
	    # AE::Curator::MAGE module is adhered to.
	    $mappable_pba = $bag_of->{extended_pba}->(
		$pba->getName() . ".merged"
	    );
	    unless ( $mappable_pba ) {
		die(sprintf(
		    "Internal error: Merged PBA undefined for two-color coding (%s).",
		    $pba->getName())
		);
	    }
	}
	else {

	    # Single-channel coding.
	    $mappable_pba = $pba;
	}

        # MeasuredBioAssays; we maintain a strict 1:n PBA:MBA mapping;
        # having n PBA map to one MBA results in dangling PBAs, which we
        # detect here.
	my $mappable_pba_name = $mappable_pba->getName();
        if ( $pba_mapcount{ $mappable_pba_name }
            && ( $pba_mapcount{ $mappable_pba_name } > 1 ) ) {
            $self->logprint( 'error',
                      q{Warning: Multiple scans map to hybridization "}
                    . $mappable_pba_name
                    . qq{".\n} );
            $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
        }
        elsif ( !$pba_mapcount{ $mappable_pba_name }
            || ( $pba_mapcount{ $mappable_pba_name } < 1 ) ) {
            $self->logprint( 'error',
                      q{WARNING: Hybridization "}
                    . $mappable_pba_name
                    . q{" is not referenced by any MeasuredBioAssay: }
                    . qq{possible duplicate filename in spreadsheet?\n} );
            $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
        }
    }

    return;
}

sub _check_datatrans_protocol : PRIVATE {

    # Returns true if any $protocoltype protocol is found attached to
    # a normalized data file (this is used for MIAME compliance
    # testing).
    my ( $self,
	 $tab2mage,
	 $filetype,
	 $protocoltype,
	 $err_string,
     ) = @_;

    my $norm_data = $EDF_FILE_PREFIX . $filetype . $EDF_FILE_SUFFIX;
    my ( %filenames, %has_protocol, $protocol_found );
    foreach my $datarow ( @{ $tab2mage->get_hyb_section() || [] } ) {
        if ( $datarow->{$norm_data} ) {
            $filenames{ $datarow->{$norm_data} }++;
            if ( $datarow->{$protocoltype} ) {
		$has_protocol{ $datarow->{$norm_data} }++;
		$protocol_found = 1;
	    }
        }
    }
    foreach my $norm_file ( keys %filenames ) {

        # Affy CHP files without protocols are okay (we derive the
        # protocol from their metadata).
        unless ( $has_protocol{$norm_file} || ( $norm_file =~ m/\.CHP$/i ) ) {
            $self->logprint( 'error',
                qq{Warning: $err_string "$norm_file"\n} );
            $self->add_error( $CONFIG->get_ERROR_MIAME() );
        }
    }

    return $protocol_found;
}

sub _check_protocols : PRIVATE {

    my ( $self, $tab2mage ) = @_;

    my @protocol_headings = (
        $EDF_GROW_PROTOCOL, $EDF_TREAT_PROTOCOL, $EDF_EXTRACT_PROTOCOL,
        $EDF_POOL_PROTOCOL, $EDF_LABEL_PROTOCOL, $EDF_IP_PROTOCOL,
        $EDF_HYB_PROTOCOL,  $EDF_SCAN_PROTOCOL,  $EDF_FEXT_PROTOCOL,
        $EDF_NORM_PROTOCOL, $EDF_TRXN_PROTOCOL,
    );

    # Check that all declared protocols are referenced
    my ( %unreferenced, %undeclared );

    PROTOCOL:
    foreach my $protocol ( @{ $tab2mage->get_prot_section() || [] } ) {

       # Confirm that the protocol does actually have an accession (required).
        unless ( $protocol->{$EDF_PROTOCOLACCESSION} ) {
            $self->logprint(
                'error',
                "ERROR: Protocol found without an accession: ",
                $protocol->{$EDF_PROTOCOLNAME}, "\n"
            );
            $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
            next PROTOCOL;
        }

        # Check on protocol text length.  Arbitrary limit; found to work
        # quite well in practice, though.
        unless ( length( $protocol->{$EDF_PROTOCOLTEXT} ) > 50 ) {
            $self->logprint( 'error',
                      q{Warning: Description for }
                    . ( $protocol->{$EDF_PROTOCOLTYPE} || q{} )
                    . q{ protocol "}
                    . ( $protocol->{$EDF_PROTOCOLNAME} || q{} )
                    . qq{" is too short.\n} );
            $self->add_error( $CONFIG->get_ERROR_MIAME() );
        }

        my $protocol_referenced;
        foreach my $datarow ( @{ $tab2mage->get_hyb_section() || [] } ) {
            foreach my $heading (@protocol_headings) {
                $protocol_referenced++
                    if ( $datarow->{$heading}
                    && $datarow->{$heading} eq
                    $protocol->{$EDF_PROTOCOLACCESSION} );
            }
        }
        $unreferenced{ $protocol->{$EDF_PROTOCOLACCESSION} }++
            unless ($protocol_referenced);
    }

    # Check that all referenced protocols are declared
    foreach my $datarow ( @{ $tab2mage->get_hyb_section() || [] } ) {

        PROTOCOL_COLUMN:
        foreach my $heading (@protocol_headings) {

            # allow blanks in the spreadsheet
            next PROTOCOL_COLUMN unless $datarow->{$heading};

            my $protocol_declared;
            foreach my $protocol ( @{ $tab2mage->get_prot_section() || [] } ) {
                $protocol_declared++
                    if ( $datarow->{$heading} eq
                    $protocol->{$EDF_PROTOCOLACCESSION} );
            }
            $undeclared{ $datarow->{$heading} }++ unless ($protocol_declared);
        }
    }

    while ( my ( $protocol, $count ) = each %unreferenced ) {
        $self->logprint( 'error',
            "Warning: Protocol $protocol is declared in the Protocol section but not referenced in the Hybridization section.\n"
        );
        $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
    }

    while ( my ( $protocol, $count ) = each %undeclared ) {
        $self->logprint( 'error',
            "Warning: Protocol $protocol is referenced in the Hybridization section ($count times) but not declared in the Protocol section.\n"
        );
        $self->add_error( $CONFIG->get_ERROR_MIAME() );
    }

    # Normalization and transformation protocols have to be checked in
    # the $datafile_rowlist construct as they aren't created in the MAGE
    # objects until the data files have been fully processed.

    my $protocol_found = $self->_check_datatrans_protocol(
	$tab2mage,
	'normalized',
        $EDF_NORM_PROTOCOL,
        'No normalization protocol associated with normalized file',
    );
    $protocol_found ||= $self->_check_datatrans_protocol(
        $tab2mage,
        $CONFIG->get_FGEM_FILE_TYPE(),
        $EDF_TRXN_PROTOCOL,
        'No transformation protocol associated with final data matrix file',
    );

    # Note the results for MIAME compliance tracking.
    if ($protocol_found) {
	$self->add_miame($CONFIG->get_MIAME_NORMPROTOCOL);
    }
    else {
	$self->logprint(
	    'miame',
	    "Problem: Data transformation protocol not found.\n"
	);
    }

    return;
}

sub _check_parsed_protocols : PRIVATE {

    my ( $self, $bag_of ) = @_;

    # Check through for biomaterial treatments lacking protocols
    foreach my $bm qw(biosample extract immunoprecipitate labeledextract) {
	$self->check_mage_treatments_protapps(
	    $bag_of->{$bm}->(),
	    $bm,
	);
    }

    # Hybs lacking protocols.
    $self->check_mage_hyb_protapps( $bag_of->{pba}->() );

    # Check scan protocol (now defaults to MBA FeatureExtraction).
    foreach my $mba ( @{ $bag_of->{mba}->() } ) {
        my $fext = $mba->getFeatureExtraction();
	my $found;
	foreach my $pa ( @{ $fext->getProtocolApplications() || [] } ) {
	    $found++ if $pa->getProtocol();
	}
	unless ( $found ) {
	    $self->logprint(
		'error',
		sprintf(
		    qq{Warning: Hybridization "%s" lacks a scanning protocol.\n},
		    $mba->getName(),
		),
	    );
	    $self->add_error( $CONFIG->get_ERROR_MIAME() );
	}
    }

    # Normalization protocol can't be checked here, as dbad/mbad
    # creation is coupled to file processing. See _check_protocols()

    return;
}

sub _check_parameters : PRIVATE {

    my ( $self, $tab2mage ) = @_;

    # Check that all referenced parameters are declared, and vice versa
    my %known_params;
    foreach my $protocol ( @{ $tab2mage->get_prot_section() || [] } ) {
        if ( $protocol->{$EDF_PROTOCOLPARAMS} ) {
            foreach my $name ( keys %{ $protocol->{$EDF_PROTOCOLPARAMS} } ) {
                $known_params{$name}++;
            }
        }
    }

    # Find the problem parameters
    my ( %referenced, %undeclared );

    my $regexp_str = "$EDF_PARAM_PREFIX(.*)$EDF_PARAM_SUFFIX";
    $regexp_str =~ s/([\[\]])/\\$1/g;

    foreach my $datarow ( @{ $tab2mage->get_hyb_section() || [] } ) {
        foreach my $column ( keys %$datarow ) {
            if ( my ($name) = ( $column =~ m/$regexp_str$/ ) ) {
                $referenced{$name}++;
                unless ( $known_params{$name} ) { $undeclared{$name}++; }
            }
        }
    }

    # Print out the errors
    foreach my $name ( keys %undeclared ) {    # referenced but not declared
        $self->logprint( 'error',
            qq{Warning: Parameter "$name" is not declared in the Protocol spreadsheet section.\n}
        );
        $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
    }
    foreach my $name ( keys %known_params ) {    # declared but not referenced
        unless ( $referenced{$name} ) {
            $self->logprint( 'error',
                qq{Warning: Parameter "$name" is not referenced in the Hybridization spreadsheet section.\n}
            );
            $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
        }
    }

    return;
}

sub _datafile_to_filelist : PRIVATE {

    my ( $self, $tab2mage ) = @_;

    # Sort out our known file types
    my $known_types;

    # Standard file types
    foreach my $filetype ( @{ $CONFIG->get_T2M_FILE_TYPES() || [] },
        $CONFIG->get_FGEM_FILE_TYPE() ) {
        $known_types->{$filetype}++;
    }

    # Other file types
    my @other_known = qw(image cdf exp);
    foreach my $type (@other_known) { $known_types->{$type}++; }

    # hashref for tracking processed files.
    my $filelist   = {};
    my $regexp_str = "$EDF_FILE_PREFIX(.*)$EDF_FILE_SUFFIX";
    $regexp_str =~ s/([\[\]])/\\$1/g;

    foreach my $datarow ( @{ $tab2mage->get_hyb_section() || [] } ) {

        # Check there are no unrecognized file types in the data row
        HYB_COLUMN:
        foreach my $column ( keys %$datarow ) {
            my $matched_type;

            next HYB_COLUMN
                unless ( ($matched_type) = ( $column =~ m/$regexp_str$/ ) );

            unless ( $known_types->{$matched_type} ) {
                $self->logprint( 'error',
                    "Warning: Unrecognized file type '$matched_type' will be ignored.\n"
                );
                $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
            }
        }

        my $readablename = $tab2mage->get_readablename($datarow);

        # defaults to raw filename if available
        my $hyb_name = $datarow->{$EDF_HYBRIDIZATIONNAME} || $readablename;

        # populate $file_list
        FILE_TYPE:
        foreach my $filetype (
            @{ $CONFIG->get_T2M_FILE_TYPES() || [] },
            $CONFIG->get_FGEM_FILE_TYPE()
            ) {

            my $heading = $EDF_FILE_PREFIX . $filetype . $EDF_FILE_SUFFIX;

            # Skip rows lacking referenced files.
            next FILE_TYPE unless ( $datarow->{$heading} );

            # Get a Datafile object.
            my $file;
	    unless ( $file = $filelist->{ $datarow->{$heading} } ) {
		my $path;
		if ( $self->get_skip_data_checks() ) {
		    $path = $datarow->{$heading};
		}
		else {
		    $path = get_filepath_from_uri(
			$datarow->{$heading},
			$self->get_source_directory(),
		    );
		}
                $file = ArrayExpress::Datafile->new({
		    path            => $path,
		    name            => basename( $path ),
		    data_type       => $filetype,
		    hyb_identifier  => $hyb_name,
		    array_design_id => $datarow->{$EDF_ARRAYACCESSION} || '{UNKNOWN}',
		});
	    }

            # Sort out the FactorValues, adding new ones where we've
            # seen the file before.
            my $regexp_str = "$EDF_FV_PREFIX(.*)$EDF_FV_SUFFIX";
            $regexp_str =~ s/([\[\]])/\\$1/g;

	    # All files are labeled with their respective FVs. This is
	    # used in DW flagging and (one day) in Pearson analysis
	    # for QA purposes.
            foreach my $fv_heading ( keys %$datarow ) {
                if ( my ($category) = ( $fv_heading =~ m/$regexp_str$/ ) ) {
                    $file->add_factor_value( $category, $datarow->{$fv_heading} );
                }
            }

            # Do the full processing on each file just once.
            next FILE_TYPE if ( $filelist->{ $datarow->{$heading} } );

	    $filelist->{ $datarow->{$heading} } = $file;
        }

        # do EXP files here
        my $exp_heading = $EDF_FILE_PREFIX . 'exp' . $EDF_FILE_SUFFIX;
        if ( $datarow->{$exp_heading} ) {

            my $file;
	    unless ( $file = $filelist->{ $datarow->{$exp_heading} } ) {
		my $path;
		if ( $self->get_skip_data_checks() ) {
		    $path = $datarow->{$exp_heading};
		}
		else {
		    $path = get_filepath_from_uri(
			$datarow->{$exp_heading},
			$self->get_source_directory(),
		    );
		}

		# N.B. we really don't care about the array_design_id
		# for EXP files at this stage, but it's a required
		# attribute.
                $file = ArrayExpress::Datafile->new({
		    path            => $path,
		    name            => basename( $path ),
		    data_type       => 'raw',
		    hyb_identifier  => $hyb_name,
		    array_design_id => $datarow->{$EDF_ARRAYACCESSION} || '{UNKNOWN}',
		    is_exp          => 1,
		});
	    }

	    $filelist->{ $datarow->{$exp_heading} } = $file;
        }
    }

    # Return the list of files in filename order
    return [ map { $filelist->{$_} } sort keys %$filelist ];
}

sub _print_edf_workflow : PRIVATE {

    my ( $self, $bag_of ) = @_;

    # keys are from Tab2MAGE::create_bags()
    my @classes = (
        { biosource         => 'BioSources'            },
        { biosample         => 'Samples'               },
        { extract           => 'Extracts'              },
        { immunoprecipitate => 'IPs'                   },
        { labeledextract    => 'Labeled extracts'      },
        { pba               => 'Hybridizations'        },
        { mbad              => 'Raw data files'        },
        { dbad              => 'Normalized data files' },
    );

    $self->generate_workflow_from_bags( $bag_of, [ @classes ] );

    return;
}

sub visualize_experiment : RESTRICTED {

    my ( $self, $filelist ) = @_;

    my $tab2mage = $self->get_mage_generator();

    FILE:
    foreach my $file ( @{ $filelist || [] } ) {
	if (  $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE()
	   || $file->get_data_type() eq $CONFIG->get_RAW_DM_FILE_TYPE() ) {

	    # This can crash, e.g. if the data matrix is malformed.
	    my $sighandler = $SIG{__DIE__};
	    delete $SIG{__DIE__};
	    local $EVAL_ERROR;
	    eval{
		my $dm = ArrayExpress::Datafile::DataMatrix->new();
		$dm->create_mage($file, {}, $tab2mage, q{});
	    };
	    $SIG{__DIE__} = $sighandler if $sighandler;
	    if ($EVAL_ERROR) {
		warn(sprintf(
		    "Error parsing data matrix %s (%s); graph visualization will omit this file.",
		    $file->get_name(),
		    $EVAL_ERROR,
		));
		last FILE;
	    }
	}
    }

    # Create the dotfile (and PNG if GraphViz installed)
    my $output_base = $self->get_spreadsheet_filename();
    $output_base =~ s/\.\w{3}$//;
    $output_base ||= 'tab2mage';    # Just in case

    if ( defined($self->get_log_to_current_dir) ) {
        my $output_dir = $self->get_log_to_current_dir || q{.};
	my ($vol, $dir, $name) = File::Spec->splitpath($output_base);
	$output_base = File::Spec->catfile($output_dir, $name);
    }

    # we'll just use the default font here.
    require ArrayExpress::Curator::Visualize;
    ArrayExpress::Curator::Visualize->import(
	qw(dot_and_png)
    );
    $self->set_clobber(
        dot_and_png(
	    $tab2mage->get_bags(),
	    $output_base,
	    undef,
	    $self->get_clobber(),
	)
    );

    return;
}

sub validate_edf : PRIVATE {

    # Parses MAGE objects out of a spreadsheet, reports on any problems
    # found.  This does not touch the data files (they are checked later
    # in Datafile.pm).

    my ( $self ) = @_;

    # Use dummy accession, domain for internal MAGE generation. Don't
    # reassign protocol accessions yet.
    my $tab2mage = ArrayExpress::Curator::Tab2MAGE->new({
	spreadsheet_filename => $self->get_spreadsheet_filename(),
	target_directory     => q{.},
	external_accession   => '{ACCESSION}',
	external_domain      => '{DOMAIN}',
	keep_protocol_accns  => 1,
    });

    $self->logprint_line( 'error', 'Spreadsheet parsing START' );

    # Parse the EDF
    my $rc = $tab2mage->read_edf( $self->log_fh('error') );
    $self->add_error($rc);
    my $exptinfo = $tab2mage->get_expt_section();
    my $bag_of   = $tab2mage->get_bags();

    # Create and link the mage objects
    $tab2mage->create_protocols();
    foreach my $datarow ( @{ $tab2mage->get_hyb_section() || [] } ) {
        $tab2mage->create_mage_objects(
	    $datarow,
            unique_identifier(),
	);
    }

    # Count BioMaterials -> Hybs
    $self->_print_edf_workflow( $bag_of );

    if ( $exptinfo->{$EDF_EXPTDESCRIPTION} ) {
        format_description(
	    $exptinfo->{$EDF_EXPTDESCRIPTION},
            $self->log_fh('report'),
	);
    }

    # Check for standard journal name
    if ( $exptinfo->{$EDF_PUBJOURNAL} ) {
        my $entrez_approved = parse_entrez_names;
        unless ( $entrez_approved->{ $exptinfo->{$EDF_PUBJOURNAL} } ) {
            $self->logprint(
                'error',
                "Warning: Non-standard publication name: ",
                $exptinfo->{$EDF_PUBJOURNAL}, "\n"
            );
            $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
        }
    }

    # Check for numeric-only pubmed ID.
    if ( $exptinfo->{$EDF_PUBMEDID} ) {
	if ( $exptinfo->{$EDF_PUBMEDID} =~ m/\D/ ) {
	    $self->logprint(
		'error',
		"Warning: PubMed ID contains non-numeric characters: ",
		$exptinfo->{$EDF_PUBMEDID}, "\n",
	    );
	    $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
	}
    }

    # Check that appropriate dates have been set, and are the correct format.
    $self->check_date_format($exptinfo->{$EDF_EXPTRELEASEDATE},    'release' );
    $self->check_date_format($exptinfo->{$EDF_EXPTSUBMISSIONDATE}, 'submission' );
    if ( $exptinfo->{$EDF_EXPTGEORELEASEDATE} ) {
	$self->check_date_format($exptinfo->{$EDF_EXPTGEORELEASEDATE}, 'GEO release' );
    }

    # Check the submitter details.
    if ( !$exptinfo->{$EDF_EXPTSUBMITTER} ) {
        $self->logprint( 'error',
            "Warning: Experiment submitter has no name.\n");
        $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
    }
    if ( !$exptinfo->{$EDF_EXPTSUBMITTER_EMAIL} ) {
        $self->logprint( 'error',
            "Warning: Submitter email address is missing.\n" );
        $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
    }

    # Collect ExperimentDesignType terms for DW checking. We use the
    # MAGE.pm subroutine for parsing the appropriate $exptinfo tag.
    my $experimentdesign = $tab2mage->new_experimentdesign();
    $self->add_expt_designs(
	map { $_->getValue() }
	    @{ $experimentdesign->getTypes() || [] }
    );

    # Also collect Organism OEs.
    foreach my $source ( @{ $bag_of->{biosource}->() } ) {
	foreach my $char ( @{ $source->getCharacteristics() || [] } ) {
	    if ( $char->getCategory() eq 'Organism' ) {
		$self->add_expt_organisms( $char->getValue() );
	    }
	}
    }

    # Check that all variable BMCs are also declared as FVs.
    $self->check_bmchars_against_fvs( $bag_of );

    # Checks on protocols and parameters
    $self->_check_protocols( $tab2mage );
    $self->_check_parsed_protocols( $bag_of );
    $self->_check_parameters( $tab2mage );

    # all OEs (warn) FIXME;

    # Missing columns or situations where defaults will be
    # used. (e.g. MaterialType) FIXME this is actually non-trivial
    # without re-writing the parser.

    # Number of FVs / Arrays / BMCs per hyb/biosource
    $self->_check_hybridizations( $bag_of );
    $self->check_mage_bmchars( $bag_of->{'biosource'}->() );

    # Check Labeled Extract - Label association is present.
    $self->check_mage_labeledextracts( $bag_of->{'labeledextract'}->() );

    # Insert files into @$filelist here. Note $file->get_is_exp() flag is
    # populated.  Also checks File[*] headings that will be ignored.
    my $filelist = $self->_datafile_to_filelist( $tab2mage );

    $self->logprint_line( 'error', 'Spreadsheet parsing END' );

    # Table of files per hyb
    $self->logprint( 'report',
        "\nChecking for presence of raw and/or normalized data files:\n" );
    $self->logprint( 'report', datafile_consistency_table($filelist) );

    # List of PBA ids for FGEM checking
    my $hyb_ids = {};
    foreach my $pba ( @{ $bag_of->{pba}->() } ) {
        $hyb_ids->{ $pba->getName }++;
    }

    # List of DBA ids for FGEM checking
    my $norm_ids = {};
    foreach my $dba ( @{ $bag_of->{dba}->() } ) {
        $norm_ids->{ $dba->getName }++;
    }

    $self->set_mage_generator( $tab2mage );

    return ( $filelist, $hyb_ids, $norm_ids );

}

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: Validate.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Curator::Validate - a module used by expt_check.pl

=head1 SYNOPSIS

 use ArrayExpress::Curator::Validate;
 
 my $checker = ArrayExpress::Curator::Validate->new({
     spreadsheet_filename => $tab2mage_file,
 });
 
 $checker->check();

=head1 DESCRIPTION

This module provides a set of subroutines used by the experiment
checker script to confirm the MIAME metadata supplied in a Tab2MAGE
spreadsheet.

=head1 OPTIONS

The following options must be used in addition to those provided by the
parent class (see L<ArrayExpress::Curator::ExperimentChecker>):

=over 2

=item C<spreadsheet_filename>

The Tab2MAGE spreadsheet to check.

=back

=head1 TESTS

The following tests are performed by this module, with output
printed to the error and/or report filehandles:

=over 4

=item B<Hybridizations>

Confirms that each hybridization is associated with an array 
design (i.e., ArrayExpress accession number), and at least 
one experimental factor value.

=item B<BioMaterialCharacteristics>

Checks that each BioSource has at least some annotation. 
This is not, however, a full test of MIAME compliance.

=item B<Undeclared Experimental Factors>

Checks all material characteristics against the factors described by
the document, alerting the user if any such characteristics vary
during the experiment without having been declared as an experimental
factor.

=item B<Protocols>

Checks that all protocols referenced in the Hybridization spreadsheet 
section are declared in the Protocol section (and vice versa).

=item B<Protocol accessions>

Confirms that all declared protocols in the Protocol section have accession numbers.

=item B<Protocol text length>

Warns if any of the protocol texts seem too brief.

=item B<Protocol presence and usage>

Checks that a protocol has been attached to each step of the 
experiment (biomaterial treatments, hybridization, scanning and normalization).

=item B<Parameters>

Checks that all parameters referenced in the Hybridization spreadsheet 
section are declared in the Protocol section (and vice versa).

=item B<BioMaterials>

Creates the links between sample, extract, labeled extract and
hybridization, and writes a biomaterials log file listing the 
numbers of each. If the Graphviz software is installed 
(http://www.graphviz.org) then the script will use the 'dot' 
program to produce a PNG format graph showing how the
various components relate to each other.

=item B<Publication journal>

Warns the user if the specified publication journal name is not 
in the included list of standard Entrez journal abbreviations.

=item B<PubMed ID>

Warns the user if a non-numeric PubMed ID has been entered.

=item B<Submission and release dates>

Warns the user if submission and/or release dates have not been
specified.

=item B<Submitter>

Checks that both submitter name and email have been provided.

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2005.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

1;
