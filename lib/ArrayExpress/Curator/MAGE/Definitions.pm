#!/usr/bin/env perl
#
# Module to provide constant values for a local installation of the
# ArrayExpress::Curator::MAGE module.
#
# Tim Rayner 2005, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Definitions.pm 1852 2007-12-13 10:14:27Z tfrayner $
#

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../../index.html">
                <img src="../../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: Definitions.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Curator::MAGE::Definitions.pm - a module providing a
central location for managing EDF column names, OntologyEntry
categories and values.

=head1 SYNOPSIS

 use ArrayExpress::Curator::MAGE::Definitions 
 qw($EDF_EXPTACCESSION 
    validate_hybridization_section
    $OE_CAT_PROTOCOLTYPE 
    $OE_VAL_GROW);

=head1 DESCRIPTION

This module provides a central location for various ontology terms and
column heading definitions. In other words, if you want to change the
parsing of your EDF or the output OntologyEntries, edit this
module. Also provided is a series of optional validation subroutines for
checking column or row names against the expected EDF headers.

=head1 SUPPORTED HEADINGS

=cut

package ArrayExpress::Curator::MAGE::Definitions;

use strict;
use warnings;

use Readonly;

use ArrayExpress::Curator::Config qw($CONFIG);

use base 'Exporter';

our @EXPORT_OK = qw(
    validate_experiment_section
    validate_protocol_section
    validate_hybridization_section

    $EDF_EXPTACCESSION
    $EDF_EXPTDOMAIN
    $EDF_EXPTNAME
    $EDF_EXPTDESCRIPTION
    $EDF_EXPTRELEASEDATE
    $EDF_EXPTSUBMITTER
    $EDF_EXPTDATACODER
    $EDF_EXPTCURATOR
    $EDF_EXPTINVESTIGATOR
    $EDF_EXPTORGANIZATION
    $EDF_EXPTADDRESS
    $EDF_EXPTSUBMITTER_EMAIL
    $EDF_EXPTCURATOR_EMAIL
    $EDF_EXPTINVESTIGATOR_EMAIL
    $EDF_EXPTDATACODER_EMAIL
    $EDF_EXPTSUBMISSIONDATE
    $EDF_EXPTDESIGNTYPE
    $EDF_EXPTQUALITYCONTROL
    $EDF_EXPTCURATEDNAME
    $EDF_EXPTURI
    $EDF_EXPTGEORELEASEDATE
    $EDF_EXPTSECONDARYACCESSION

    $EDF_PUBTITLE
    $EDF_PUBPAGES
    $EDF_PUBJOURNAL
    $EDF_PUBVOLUME
    $EDF_PUBISSUE
    $EDF_PUBYEAR
    $EDF_PUBAUTHORS
    $EDF_PUBURI
    $EDF_PUBMEDID

    $EDF_PROTOCOLACCESSION
    $EDF_PROTOCOLNAME
    $EDF_PROTOCOLTYPE
    $EDF_PROTOCOLTEXT
    $EDF_PROTOCOLPARAMS

    $EDF_BIOSOURCENAME
    $EDF_BIOSOURCETYPE
    $EDF_BIOSOURCEDESCRIPTION
    $EDF_BIOSAMPLENAME
    $EDF_BIOSAMPLETYPE
    $EDF_EXTRACTNAME
    $EDF_EXTRACTTYPE
    $EDF_IMMUNOPRECIPITATENAME
    $EDF_IMMUNOPRECIPITATETYPE
    $EDF_LABELEDEXTRACTNAME
    $EDF_LABELEDEXTRACTTYPE
    $EDF_HYBRIDIZATIONNAME
    $EDF_SCANNAME
    $EDF_NORMALIZATIONNAME
    $EDF_NORMALIZATIONTYPE
    $EDF_TRANSFORMATIONNAME
    $EDF_TRANSFORMATIONTYPE
    $EDF_DYE

    $EDF_GROW_PROTOCOL
    $EDF_TREAT_PROTOCOL
    $EDF_EXTRACT_PROTOCOL
    $EDF_POOL_PROTOCOL
    $EDF_LABEL_PROTOCOL
    $EDF_IP_PROTOCOL
    $EDF_HYB_PROTOCOL
    $EDF_SCAN_PROTOCOL
    $EDF_FEXT_PROTOCOL
    $EDF_NORM_PROTOCOL
    $EDF_TRXN_PROTOCOL

    $EDF_SCAN_SOFTWARE
    $EDF_FEXT_SOFTWARE
    $EDF_NORM_SOFTWARE
    $EDF_TRXN_SOFTWARE

    $EDF_IMAGE_FORMAT

    $EDF_ARRAYACCESSION
    $EDF_ARRAYSERIAL

    $EDF_FILE_PREFIX
    $EDF_FILE_SUFFIX
    $EDF_BMC_PREFIX
    $EDF_BMC_SUFFIX
    $EDF_FV_PREFIX
    $EDF_FV_SUFFIX
    $EDF_PARAM_PREFIX
    $EDF_PARAM_SUFFIX

    $EDF_SAMPLE_PARAMS
    $EDF_TREAT_PARAMS
    $EDF_EXTRACT_PARAMS
    $EDF_IP_PARAMS
    $EDF_LABEL_PARAMS
    $EDF_HYB_PARAMS
    $EDF_SCAN_PARAMS
    $EDF_FEXT_PARAMS
    $EDF_NORM_PARAMS
    $EDF_TRXN_PARAMS

    $EDF_NORM_STATS
    $EDF_FEXT_STATS
    $EDF_TRXN_STATS

    $EDF_HYB_HARDWARE
    $EDF_SCAN_HARDWARE
    $EDF_HYB_HW_PARAMS
    $EDF_SCAN_HW_PARAMS

    $OE_CAT_ACTION
    $OE_CAT_BIOSAMPLETYPE
    $OE_CAT_MATERIALTYPE
    $OE_CAT_PROTOCOLTYPE
    $OE_CAT_SOFTWARETYPE
    $OE_CAT_HARDWARETYPE
    $OE_CAT_DERIVEDBIOASSAYTYPE
    $OE_CAT_EXPERIMENTALFACTORCATEGORY
    $OE_CAT_DATATYPE
    $OE_CAT_SCALE
    $OE_CAT_EXPERIMENTDESIGNTYPE
    $OE_CAT_QUALITYCONTROLDESCRIPTIONTYPE
    $OE_CAT_PUBLICATIONTYPE
    $OE_CAT_RELEASEDATE
    $OE_CAT_SUBMISSIONDATE
    $OE_CAT_ROLE

    $OE_VAL_SYNTHETICDNA
    $OE_VAL_SYNTHETICRNA
    $OE_VAL_TOTALRNA
    $OE_VAL_GENOMICDNA
    $OE_VAL_WHOLEORGANISM
    $OE_VAL_ORGANISMPART
    $OE_VAL_EXTRACT
    $OE_VAL_NOTEXTRACT
    $OE_VAL_FLOAT
    $OE_VAL_UNKNOWN
    $OE_VAL_LINEARSCALE
    $OE_VAL_JOURNALARTICLE
    $OE_VAL_GROW
    $OE_VAL_POOL
    $OE_VAL_SPECIFIEDBIOMATERIALACTION
    $OE_VAL_NUCLEICACIDEXTRACTION
    $OE_VAL_IMMUNOPRECIPITATE
    $OE_VAL_LABELING
    $OE_VAL_NORMALIZATION
    $OE_VAL_HYBRIDIZATION
    $OE_VAL_SCANNING
    $OE_VAL_FEATUREEXTRACTION
    $OE_VAL_SCANNING_SOFTWARE
    $OE_VAL_ANALYSIS_SOFTWARE
    $OE_VAL_TRANSFORMATION_SOFTWARE
    $OE_VAL_SUBMITTER
    $OE_VAL_DATA_CODER
    $OE_VAL_CURATOR
    $OE_VAL_INVESTIGATOR

    $OE_VAL_CHIP_CHIP

    $AE_LABELCOMPOUND_PREFIX
    $AE_CHANNEL_PREFIX

    $MAGE_UNITS
);

#####################################################################
# NOTE: all column headers are lowercased during parsing; therefore #
# only lowercase tags can be used here.                             #
#####################################################################

#######################
# EDF column headings #
#######################

# Top-level experiment information
Readonly our $EDF_EXPTACCESSION          => 'accession';
Readonly our $EDF_EXPTDOMAIN             => 'domain';
Readonly our $EDF_EXPTNAME               => 'name';
Readonly our $EDF_EXPTDESCRIPTION        => 'description';
Readonly our $EDF_EXPTRELEASEDATE        => 'release_date';
Readonly our $EDF_EXPTSUBMITTER          => 'submitter';
Readonly our $EDF_EXPTDATACODER          => 'data_coder';
Readonly our $EDF_EXPTCURATOR            => 'curator';
Readonly our $EDF_EXPTINVESTIGATOR       => 'investigator';
Readonly our $EDF_EXPTORGANIZATION       => 'organization';
Readonly our $EDF_EXPTADDRESS            => 'address';
Readonly our $EDF_EXPTSUBMITTER_EMAIL    => 'submitter_email';
Readonly our $EDF_EXPTDATACODER_EMAIL    => 'data_coder_email';
Readonly our $EDF_EXPTCURATOR_EMAIL      => 'curator_email';
Readonly our $EDF_EXPTINVESTIGATOR_EMAIL => 'investigator_email';
Readonly our $EDF_EXPTSUBMISSIONDATE     => 'submission_date';
Readonly our $EDF_EXPTDESIGNTYPE         => 'experiment_design_type';
Readonly our $EDF_EXPTQUALITYCONTROL     => 'quality_control';
Readonly our $EDF_EXPTCURATEDNAME        => 'ae_display_name';
Readonly our $EDF_EXPTURI                => 'uri';
Readonly our $EDF_EXPTGEORELEASEDATE     => 'geo_release_date';
Readonly our $EDF_EXPTSECONDARYACCESSION => 'secondary_accession';

=pod

=head2 Experiment section: row names

=over 1

=item accession

A string unique to the experiment. Used in all identifiers, and as a
top-level Experiment identifier. Synonymous with the ArrayExpress
accession number for an experiment. Example: E-MEXP-100

=item domain

A string identifying the origin of the information provided. Typically
this will be an internet domain name such as "ebi.ac.uk". This string
is used to define the namespace of the MAGE identifiers created.

=item name

The name of the experiment. The forms the name attribute of the
top-level Experiment object.

=item description

A short description of the experiment. This is inserted into a
Description text attribute attached to the top-level Experiment
object.

=item release_date

In the form YYYY-MM-DDB<T>hh:mm:ssB<Z>. This is currently used in a
NameValueType (name ArrayExpressReleaseDate) in the top-level
Experiment object.

=item submission_date

In the form YYYY-MM-DDB<T>hh:mm:ssB<Z>. This is currently used in a
NameValueType (name ArrayExpressSubmissionDate) in the top-level
Experiment object.

=item experiment_design_type

A comma-separated list of MO ExperimentDesignType terms used in the
top-level ExperimentDesign object.

=item quality_control

A comma-separated list of MO QualityControlDescriptionType used in the
top-level ExperimentDesign object.

=item submitter

The name of the person submitting the experiment. This is used to
create a Person object in the AuditAndSecurity package with a
Roles:submitter OntologyEntry. The Person object is referenced in the
Experiment package.

=item submitter_email

The email address of the person submitting the experiment.

=item data_coder

The name of the person responsible for coding the experiment into
MAGE-ML. This is used to create a Person object in the
AuditAndSecurity package with a Roles:data_coder OntologyEntry. The
Person object is referenced in the Experiment package.

=item data_coder_email

The email address of the person who coded the experiment in MAGE-ML.

=item curator

The name of the person who curated the experiment. This is used to
create a Person object in the AuditAndSecurity package with a
Roles:curator OntologyEntry. The Person object is referenced in the
Experiment package.

=item curator_email

The email address of the person who curated the experiment.

=item investigator

The name of the primary investigator on the experiment. This is used
to create a Person object in the AuditAndSecurity package with a
Roles:curator OntologyEntry. The Person object is referenced in the
Experiment package.

=item investigator_email

The email address of the primary investigator.

=item organization

The name of the organisation to which the submitter is
affiliated. This is used to create an Organization MAGE object, which
is then associated with each Person object.

=item address

The address of the organisation to which the submitter is
affiliated.

=item ae_display_name

The name of the experiment to be displayed in the ArrayExpress
repository interface. This will typically be added by the ArrayExpress
curators after data submission.

=item URI

A URI pointing to an alternative location for the experimental data,
e.g. in a public repository database.

=item geo_release_date

For processing of GEO experiments; this value is used to create a
"GEOReleaseDate" NameValueType object associated with the top-level
Experiment; this is used internally by the ArrayExpress database.

=item secondary_accession

This value is used to populate the "SecondaryAcession" NameValueType
object associated with the top-level Experiment. This is used
internally by ArrayExpress to represent e.g. GEO accession numbers.

=back

=cut

# Publication info
Readonly our $EDF_PUBTITLE   => 'publication_title';
Readonly our $EDF_PUBPAGES   => 'pages';
Readonly our $EDF_PUBJOURNAL => 'journal';
Readonly our $EDF_PUBVOLUME  => 'volume';
Readonly our $EDF_PUBISSUE   => 'issue';
Readonly our $EDF_PUBYEAR    => 'year';
Readonly our $EDF_PUBAUTHORS => 'authors';
Readonly our $EDF_PUBURI     => 'publication_uri';
Readonly our $EDF_PUBMEDID   => 'pubmed_id';

=pod

The following are used in a BibliographicReference associated with a
second Description object in the top-level Experiment object:

=over 1

=item publication_title

The free-text title of any associated publication. This is currently
assumed to have PublicationType journal_article (MO), although more
control over this may be added in future. Used in 

=item authors

A free-text list of publication authors.

=item journal

The name of the journal. This should be a standard Pubmed abbreviation.

=item year

Year of publication.

=item volume

Journal volume.

=item issue

Journal issue.

=item pages

Page range of journal article.

=item publication_URI

Any URI associated with the publication.

=item pubmed_id

The Pubmed ID associated with the publication.

=back

=cut

# Protocol information
Readonly our $EDF_PROTOCOLACCESSION => 'accession';
Readonly our $EDF_PROTOCOLNAME      => 'name';
Readonly our $EDF_PROTOCOLTYPE      => 'type';
Readonly our $EDF_PROTOCOLTEXT      => 'text';
Readonly our $EDF_PROTOCOLPARAMS    => 'parameters';

=pod

=head2 Protocol section: column names

The following are all attached to individual protocols defined by
successive lines in this section:

=over 1

=item accession

Database (e.g. ArrayExpress) accession no.

=item name

Protocol name.

=item type

MO ProtocolType term; this is an optional field. In its absence,
Tab2MAGE will use default ProtocolType terms based on how the protocol
is used within the Hybridization section.

=item text

Protocol text.

=item parameters

Protocol parameters, listed in the following form:
name1(unit1);name2(unit2);...

=back

=cut

# BioMaterials
Readonly our $EDF_BIOSOURCENAME         => 'biosource';
Readonly our $EDF_BIOSOURCETYPE         => 'biosourcematerial';
Readonly our $EDF_BIOSOURCEDESCRIPTION  => 'biosourcedescription';
Readonly our $EDF_BIOSAMPLENAME         => 'sample';
Readonly our $EDF_BIOSAMPLETYPE         => 'samplematerial';
Readonly our $EDF_EXTRACTNAME           => 'extract';
Readonly our $EDF_EXTRACTTYPE           => 'extractmaterial';
Readonly our $EDF_IMMUNOPRECIPITATENAME => 'immunoprecipitate';
Readonly our $EDF_IMMUNOPRECIPITATETYPE => 'immunoprecipitatematerial';
Readonly our $EDF_LABELEDEXTRACTNAME    => 'labeledextract';
Readonly our $EDF_LABELEDEXTRACTTYPE    => 'labeledextractmaterial';
Readonly our $EDF_HYBRIDIZATIONNAME     => 'hybridization';
Readonly our $EDF_SCANNAME              => 'scan';
Readonly our $EDF_NORMALIZATIONNAME     => 'normalization';
Readonly our $EDF_NORMALIZATIONTYPE     => 'normalizationtype';
Readonly our $EDF_TRANSFORMATIONNAME    => 'transformation';
Readonly our $EDF_TRANSFORMATIONTYPE    => 'transformationtype';
Readonly our $EDF_DYE                   => 'dye';
Readonly our $EDF_IMAGE_FORMAT          => 'ImageFormat';

=pod

=head2 Hybridization section: column names

=over 1

=item BioSource

Arbitrary name for a BioSource. This term is used as a unique
identifier within a Tab2MAGE run to determine correct linking between
objects. It may however be omitted, in favour of using the set of
BioMaterialCharacteristics associated with a BioMaterial as the sole
indicator of BioSource identity.

=item Sample

Arbitrary name for a BioSample (associated with a
BioSampleType:not_extract OntologyEntry). Used to control linking
between objects; may be omitted if desired, in which case a BioSample
name constructed from the raw data filename is used instead.

=item Extract

Arbitrary name for a BioSample (associated with a
BioSampleType:extract OntologyEntry). Used to control linking
between objects; may be omitted if desired, in which case a Extract
name constructed from the raw data filename is used instead.

=item Immunoprecipitate

Arbitrary name for a BioSample (associated with a
BioSampleType:extract OntologyEntry). Used to control linking between
objects; may be omitted if desired, in which case an Immunoprecipitate
name constructed from the raw data filename is used instead. These
objects are used in ChIP experiments and may be ignored for expression
or CGH studies.

=item LabeledExtract

Arbitrary name for a LabeledExtract. Used to control linking between
objects; may be omitted if desired, in which case a LabeledExtract name
constructed from the raw data filename and the label dye name is used
instead.

=item Dye

Name of the dye linked to the labeled extract (e.g., Cy3, Cy5, biotin).

=item BioSourceMaterial

MO MaterialType term to be attached to the BioSource. Default: whole_organism

=item SampleMaterial

MO MaterialType term to be attached to the BioSample. Default: organism_part

=item ExtractMaterial

MO MaterialType term to be attached to the Extract. Default: total_RNA

=item ImmunoprecipitateMaterial

MO MaterialType term to be attached to the Immunoprecipitate. Default: genomic_DNA

=item LabeledExtractMaterial

MO MaterialType term to be attached to the LabeledExtract. Default: synthetic_DNA

=item BioSourceDescription

Free-text description to attached to the BioSource (this should be
used sparingly, if at all).

=item Hybridization

Arbitrary name for a hybridization. Used to control linking between
objects; may be omitted if desired, in which case a Hybridization name
constructed from the raw data filename is used instead.

=item Scan

Arbitrary name for a scanning event. Used to control linking between
objects; may be omitted if desired, in which case a Scan name
constructed from the raw data filename is used instead.

=item Normalization

Arbitrary name for a normalization procedure. Used to control linking
between objects; may be omitted if desired, in which case a
Normalization name constructed from the normalized data filename is
used instead.

=item NormalizationType

MO DerivedBioAssayType term, attached to the relevant
DerivedBioAssayData object.

=item Transformation

Arbitrary name for a data transformation procedure. Used to control linking
between objects; may be omitted if desired.

=item TransformationType

MO DerivedBioAssayType term, attached to the relevant
DerivedBioAssayData object.

=item ImageFormat           

MO ImageFormat term. Only used if File[image] columns have been
included in the spreadsheet. Used to create Image objects.

=back

=cut

# Protocol accessions
Readonly our $EDF_GROW_PROTOCOL    => 'protocol[grow]';
Readonly our $EDF_TREAT_PROTOCOL   => 'protocol[treatment]';
Readonly our $EDF_EXTRACT_PROTOCOL => 'protocol[extraction]';
Readonly our $EDF_POOL_PROTOCOL    => 'protocol[pool]';
Readonly our $EDF_LABEL_PROTOCOL   => 'protocol[labeling]';
Readonly our $EDF_IP_PROTOCOL      => 'protocol[immunoprecipitate]';
Readonly our $EDF_HYB_PROTOCOL     => 'protocol[hybridization]';
Readonly our $EDF_SCAN_PROTOCOL    => 'protocol[scanning]';
Readonly our $EDF_FEXT_PROTOCOL    => 'protocol[image_analysis]';
Readonly our $EDF_NORM_PROTOCOL    => 'protocol[normalization]';
Readonly our $EDF_TRXN_PROTOCOL    => 'protocol[transformation]';

=pod

=over 1

=item Protocol[grow]

Accession number for the "growth" protocol (BioSource->BioSample
Treatment). The accession number should either be present in the
protocol section of the spreadsheet, or pre-existing in
ArrayExpress. Default ProtocolType: grow

=item Protocol[treatment]

Accession number for the "treatment" protocol (BioSource->BioSample
Treatment). The accession number should either be present in the
protocol section of the spreadsheet, or pre-existing in
ArrayExpress. Default ProtocolType: specified_biomaterial_action

=item Protocol[extraction]

Accession number for the "extraction" protocol (BioSample->Extract
Treatment). The accession number should either be present in the
protocol section of the spreadsheet, or pre-existing in
ArrayExpress. Default ProtocolType: nucleic_acid_extraction

=item Protocol[pool]

Accession number for the "pooling" protocol (BioSample->Extract
Treatment). The accession number should either be present in the
protocol section of the spreadsheet, or pre-existing in
ArrayExpress. Default ProtocolType: pool

=item Protocol[labeling]

Accession number for the "labeling" protocol (Extract->LabeledExtract
Treatment). The accession number should either be present in the
protocol section of the spreadsheet, or pre-existing in
ArrayExpress. Default ProtocolType: labeling

=item Protocol[immunoprecipitate]

Accession number for the "immunoprecipitation" protocol
(Extract->Immunoprecipitate Treatment). The accession number should
either be present in the protocol section of the spreadsheet, or
pre-existing in ArrayExpress. Omit if Immunoprecipitates are not used
in the experiment. Default ProtocolType: immunoprecipitate

=item Protocol[hybridization]

Accession number for the "hybridization" protocol (PhysicalBioAssay
BioAssayCreation). The accession number should either be present in
the protocol section of the spreadsheet, or pre-existing in
ArrayExpress. Default ProtocolType: hybridization

=item Protocol[scanning]

Accession number for the "image acquisition" protocol
(PhysicalBioAssay BioAssayTreatment). The accession number should
either be present in the protocol section of the spreadsheet, or
pre-existing in ArrayExpress. Default ProtocolType: image_acquisition;
note however that if Protocol[image_analysis] is not specified then
the scanning protocol defaults to feature_extraction. This is an
ArrayExpress-specific behaviour and relates to the appearance of the
experiment in the ArrayExpress web interface.

=item Protocol[image_analysis]

Accession number for the "feature extraction" protocol
(MeasuredBioAssay FeatureExtraction). The accession number should
either be present in the protocol section of the spreadsheet, or
pre-existing in ArrayExpress. Default ProtocolType: feature_extraction

=item Protocol[normalization]

Accession number for the "normalization" protocol (DerivedBioAssayData
ProducerTransformation). The accession number should either be present
in the protocol section of the spreadsheet, or pre-existing in
ArrayExpress. Default ProtocolType: bioassay_data_transformation

=item Protocol[transformation]

Accession number for the "transformation" protocol (DerivedBioAssayData
ProducerTransformation). The accession number should either be present
in the protocol section of the spreadsheet, or pre-existing in
ArrayExpress. Default ProtocolType: bioassay_data_transformation

=back

=cut

# Software

Readonly our $EDF_SCAN_SOFTWARE => 'software[scanning]';
Readonly our $EDF_FEXT_SOFTWARE => 'software[image_analysis]';
Readonly our $EDF_NORM_SOFTWARE => 'software[normalization]';
Readonly our $EDF_TRXN_SOFTWARE => 'software[transformation]';

=pod

Software is created in the Protocol package, and referenced as
SoftwareApplication in the appropriate ProtocolApplication (see
above).

=over 1

=item Software[scanning]

Name of the scanning software, followed by its version in
parentheses. The Software name is used to create a Software object in
the Protocol package, and the version is inserted into the relevant
SoftwareApplication objects.

=item Software[image_analysis]

Name of the feature extraction software, followed by its version in
parentheses. The Software name is used to create a Software object in
the Protocol package, and the version is inserted into the relevant
SoftwareApplication objects.

=item Software[normalization]

Name of the normalization software, followed by its version in
parentheses. The Software name is used to create a Software object in
the Protocol package, and the version is inserted into the relevant
SoftwareApplication objects.

=item Software[transformation]

Name of the data transformation software, followed by its version in
parentheses. The Software name is used to create a Software object in
the Protocol package, and the version is inserted into the relevant
SoftwareApplication objects.

=back

=cut

# Array information
Readonly our $EDF_ARRAYACCESSION => 'array[accession]';
Readonly our $EDF_ARRAYSERIAL    => 'array[serial]';

=pod

=over 1

=item Array[accession]

ArrayExpress array accession number (e.g., A-MEXP-1). This is used in
the Array package, and also to create Feature and Reporter identifiers
for the DesignElementDimensions.

=item Array[serial]

Serial or lot number of the array. This is used to define
ArrayManufacture objects within the Array package.

=back

=cut

# Compound headings
Readonly our $EDF_FILE_PREFIX  => 'file[';
Readonly our $EDF_FILE_SUFFIX  => ']';
Readonly our $EDF_BMC_PREFIX   => 'biomaterialcharacteristics[';
Readonly our $EDF_BMC_SUFFIX   => ']';
Readonly our $EDF_FV_PREFIX    => 'factorvalue[';
Readonly our $EDF_FV_SUFFIX    => ']';
Readonly our $EDF_PARAM_PREFIX => 'parameter[';
Readonly our $EDF_PARAM_SUFFIX => ']';

=pod

=over 1

=item File[raw]

Name of the raw data file for a given hybridization.

=item File[normalized]

Name of the normalized data file for a given hybridization/normalization.

=item File[transformed]

Name of the transformed final data matrix file for the experiment.

=item File[image]

URI locating the Image object associated with an ImageAcquisition
(scanning) event. ArrayExpress does not store images, although we can
link to images stored on external web sites, where desired.

=item File[cdf]

Affymetrix array library file pertaining to a given
hybridization. Required for correct parsing of Affymetrix data files,
although for standard Affymetrix arrays the actual CDF file does not
need to be supplied.

=item File[exp]

Affymetrix experiment description file pertaining to a given
hybridization. Required for correct parsing of Affymetrix data files.

=item BioMaterialCharacteristics[<I<category>>]

MO term describing some feature of the BioSource (e.g., Genotype, Sex,
DiseaseState, etc.). As many BioMaterialCharacteristics columns may be
used as are desired. Each column should contain values from a
different B<category> within the MGED Ontology.

=item FactorValue[<I<category>>]

MO term describing a FactorValue associated with a given hybridization
(e.g., Genotype, Sex, DiseaseState, etc.). As many FactorValue columns
may be used as are desired. Each column should contain values from a
different B<category> within the MGED Ontology.

=item Parameter[<I<parameter name>>]

Parameter values for the parameters declared in the Protocol section
of the spreadsheet. Note that a protocol must be declared in the
spreadsheet in order to be able to use parameters with it.

=back

=cut

# These are for internal use only. These are name=>value hashes thoughout.
Readonly our $EDF_SAMPLE_PARAMS  => 'TMparameters[sample]';
Readonly our $EDF_TREAT_PARAMS   => 'TMparameters[treat]';
Readonly our $EDF_EXTRACT_PARAMS => 'TMparameters[extract]';
Readonly our $EDF_IP_PARAMS      => 'TMparameters[ip]';
Readonly our $EDF_LABEL_PARAMS   => 'TMparameters[label]';
Readonly our $EDF_HYB_PARAMS     => 'TMparameters[hybridization]';
Readonly our $EDF_SCAN_PARAMS    => 'TMparameters[scan]';
Readonly our $EDF_FEXT_PARAMS    => 'TMparameters[fext]';
Readonly our $EDF_NORM_PARAMS    => 'TMparameters[norm]';
Readonly our $EDF_TRXN_PARAMS    => 'TMparameters[trxn]';
Readonly our $EDF_FEXT_STATS     => 'TMstatistics[fext]';
Readonly our $EDF_NORM_STATS     => 'TMstatistics[norm]';
Readonly our $EDF_TRXN_STATS     => 'TMstatistics[trxn]';

# Internal use only - Affymetrix. These will be linked to fully-formed objects
Readonly our $EDF_HYB_HARDWARE   => 'TMhardware[hybridization]';
Readonly our $EDF_SCAN_HARDWARE  => 'TMhardware[scan]';
Readonly our $EDF_HYB_HW_PARAMS  => 'TMhwparams[hybridization]';
Readonly our $EDF_SCAN_HW_PARAMS => 'TMhwparams[scan]';

##################
# Ontology terms #
##################

# Categories
Readonly our $OE_CAT_ACTION                     => 'Action';
Readonly our $OE_CAT_BIOSAMPLETYPE              => 'BioSampleType';
Readonly our $OE_CAT_MATERIALTYPE               => 'MaterialType';
Readonly our $OE_CAT_PROTOCOLTYPE               => 'ProtocolType';
Readonly our $OE_CAT_SOFTWARETYPE               => 'SoftwareType';
Readonly our $OE_CAT_HARDWARETYPE               => 'HardwareType';
Readonly our $OE_CAT_DERIVEDBIOASSAYTYPE        => 'DerivedBioAssayType';
Readonly our $OE_CAT_EXPERIMENTALFACTORCATEGORY =>
    'ExperimentalFactorCategory';
Readonly our $OE_CAT_DATATYPE                      => 'DataType';
Readonly our $OE_CAT_SCALE                         => 'Scale';
Readonly our $OE_CAT_EXPERIMENTDESIGNTYPE          => 'ExperimentDesignType';
Readonly our $OE_CAT_QUALITYCONTROLDESCRIPTIONTYPE =>
    'QualityControlDescriptionType';
Readonly our $OE_CAT_PUBLICATIONTYPE => 'PublicationType';
Readonly our $OE_CAT_RELEASEDATE     => 'ReleaseDate';
Readonly our $OE_CAT_SUBMISSIONDATE  => 'SubmissionDate';
Readonly our $OE_CAT_ROLE            => 'Roles';

# Values
Readonly our $OE_VAL_SYNTHETICDNA               => 'synthetic_DNA';
Readonly our $OE_VAL_SYNTHETICRNA               => 'synthetic_RNA';
Readonly our $OE_VAL_TOTALRNA                   => 'total_RNA';
Readonly our $OE_VAL_GENOMICDNA                 => 'genomic_DNA';
Readonly our $OE_VAL_WHOLEORGANISM              => 'whole_organism';
Readonly our $OE_VAL_ORGANISMPART               => 'organism_part';
Readonly our $OE_VAL_EXTRACT                    => 'extract';
Readonly our $OE_VAL_NOTEXTRACT                 => 'not_extract';
Readonly our $OE_VAL_FLOAT                      => 'float';
Readonly our $OE_VAL_UNKNOWN                    => 'unknown';
Readonly our $OE_VAL_LINEARSCALE                => 'linear_scale';
Readonly our $OE_VAL_JOURNALARTICLE             => 'journal_article';
Readonly our $OE_VAL_GROW                       => 'grow';
Readonly our $OE_VAL_POOL                       => 'pool';
Readonly our $OE_VAL_SPECIFIEDBIOMATERIALACTION =>
    'specified_biomaterial_action';
Readonly our $OE_VAL_NUCLEICACIDEXTRACTION => 'nucleic_acid_extraction';
Readonly our $OE_VAL_IMMUNOPRECIPITATE     => 'immunoprecipitate';
Readonly our $OE_VAL_LABELING              => 'labeling';
Readonly our $OE_VAL_NORMALIZATION         => 'bioassay_data_transformation';
Readonly our $OE_VAL_HYBRIDIZATION         => 'hybridization';
Readonly our $OE_VAL_SCANNING              => 'image_acquisition';
Readonly our $OE_VAL_FEATUREEXTRACTION     => 'feature_extraction';
Readonly our $OE_VAL_SCANNING_SOFTWARE     => 'image_acquisition_software';
Readonly our $OE_VAL_ANALYSIS_SOFTWARE     => 'feature_extraction_software';
Readonly our $OE_VAL_TRANSFORMATION_SOFTWARE =>
    'bioassay_data_transformation_software';
Readonly our $OE_VAL_SUBMITTER    => 'submitter';
Readonly our $OE_VAL_CURATOR      => 'curator';
Readonly our $OE_VAL_DATA_CODER   => 'data_coder';
Readonly our $OE_VAL_INVESTIGATOR => 'investigator';

Readonly our $OE_VAL_CHIP_CHIP    => 'binding_site_identification_design';

###############################
# MAGE identifier conventions #
###############################
Readonly our $AE_LABELCOMPOUND_PREFIX => 'ebi.ac.uk:LabelCompound:';
Readonly our $AE_CHANNEL_PREFIX       => 'ebi.ac.uk:Channel:';

########################
# MAGE Unit subclasses #
########################

Readonly our $MAGE_UNITS => {
    TemperatureUnit => [qw(degree_C degree_F K)],
    MassUnit        => [qw(kg g mg ug ng pg fg)],
    VolumeUnit      => [qw(mL cc dL L uL nL pL fL)],

    # m (meters) not supported: it clashes with m (minutes).
    DistanceUnit      => [qw(fm pm nm um mm cm)],
    TimeUnit          => [qw(years months weeks d h m s us)],
    QuantityUnit      => [qw(mol amol fmol pmol nmol umol mmol molecules)],
    ConcentrationUnit => [
        qw(M mM uM nM pM fM mg_per_mL mL_per_L g_per_L gram_percent mass_per_volume_percent mass_per_mass_percent)
    ],
};

##########################
# Validation subroutines #
##########################

=pod

=head1 VALIDATION SUBROUTINES

=over 1

=item validate_expriment_section($test_arrayref, $error_fh)

=item validate_protocol_section($test_arrayref, $error_fh)

=item validate_hybridization_section($test_arrayref, $error_fh)

Each of these subroutines simply takes a reference to an array
containing the list of headings to be checked, and prints warnings on
STDERR (and on the optional error filehandle) if it finds unrecognized
headings. No further action is taken; these subroutines merely serve
as a warning to the user.

=back

=cut

sub _check_section {

    my ( $testarray, $matcharray, $section, $error_fh ) = @_;

    my $error = 0;

    # Quote brackets and parentheses in each pattern to be matched
    foreach my $entry (@$matcharray) {
        $entry =~ s/([\[\]\(\)])/\\$1/g;
    }

    # Check each heading in the array to be tested
    foreach my $header (@$testarray) {

        # Print to STDERR and to the logfile
        unless ( map { $header =~ m/^$_$/ } @$matcharray ) {
            my $error_str
                = qq{Warning: Unknown column/row name in $section section will be ignored: "$header"\n};
            print STDERR $error_str;
            print $error_fh ($error_str) if $error_fh;
            $error |= $CONFIG->get_ERROR_PARSEBAD();
        }
    }

    return $error;
}

sub validate_experiment_section {

    my ( $testarray, $error_fh ) = @_;

    my $matcharray = [
        $EDF_EXPTACCESSION,       $EDF_EXPTDOMAIN,
        $EDF_EXPTNAME,            $EDF_EXPTDESCRIPTION,
        $EDF_EXPTRELEASEDATE,     $EDF_EXPTSUBMISSIONDATE,
        $EDF_EXPTSUBMITTER,       $EDF_EXPTCURATOR,
        $EDF_EXPTDATACODER,       $EDF_EXPTINVESTIGATOR,
        $EDF_EXPTORGANIZATION,    $EDF_EXPTADDRESS,
        $EDF_EXPTSUBMITTER_EMAIL, $EDF_EXPTCURATOR_EMAIL,
        $EDF_EXPTDATACODER_EMAIL, $EDF_EXPTINVESTIGATOR_EMAIL,
        $EDF_EXPTDESIGNTYPE,      $EDF_EXPTQUALITYCONTROL,
        $EDF_EXPTCURATEDNAME,     $EDF_EXPTURI,
	$EDF_EXPTGEORELEASEDATE,  $EDF_EXPTSECONDARYACCESSION,
	$EDF_PUBTITLE,
        $EDF_PUBPAGES,            $EDF_PUBJOURNAL,
        $EDF_PUBVOLUME,           $EDF_PUBISSUE,
        $EDF_PUBYEAR,             $EDF_PUBAUTHORS,
        $EDF_PUBURI,              $EDF_PUBMEDID,
    ];

    my $error |= _check_section(
	$testarray,
	$matcharray,
	'Experiment',
	$error_fh,
    );

    return $error;

}

sub validate_protocol_section {

    my ( $testarray, $error_fh ) = @_;

    my $matcharray = [
        $EDF_PROTOCOLACCESSION, $EDF_PROTOCOLNAME, $EDF_PROTOCOLTYPE,
        $EDF_PROTOCOLTEXT,      $EDF_PROTOCOLPARAMS,
    ];

    my $error |= _check_section(
	$testarray,
	$matcharray,
	'Protocol',
	$error_fh,
    );

    return $error;

}

sub validate_hybridization_section {

    my ( $testarray, $error_fh ) = @_;

    my $matcharray = [
        $EDF_BIOSOURCENAME,
        $EDF_BIOSOURCETYPE,
        $EDF_BIOSOURCEDESCRIPTION,
        $EDF_BIOSAMPLENAME,
        $EDF_BIOSAMPLETYPE,
        $EDF_EXTRACTNAME,
        $EDF_EXTRACTTYPE,
        $EDF_IMMUNOPRECIPITATENAME,
        $EDF_IMMUNOPRECIPITATETYPE,
        $EDF_LABELEDEXTRACTNAME,
        $EDF_LABELEDEXTRACTTYPE,
        $EDF_HYBRIDIZATIONNAME,
        $EDF_SCANNAME,
        $EDF_NORMALIZATIONNAME,
        $EDF_NORMALIZATIONTYPE,
        $EDF_TRANSFORMATIONNAME,
        $EDF_TRANSFORMATIONTYPE,
        $EDF_DYE,

        $EDF_GROW_PROTOCOL,
        $EDF_TREAT_PROTOCOL,
        $EDF_EXTRACT_PROTOCOL,
        $EDF_POOL_PROTOCOL,
        $EDF_LABEL_PROTOCOL,
        $EDF_IP_PROTOCOL,
        $EDF_HYB_PROTOCOL,
        $EDF_SCAN_PROTOCOL,
        $EDF_FEXT_PROTOCOL,
        $EDF_NORM_PROTOCOL,
        $EDF_TRXN_PROTOCOL,

        $EDF_SCAN_SOFTWARE,
        $EDF_FEXT_SOFTWARE,
        $EDF_NORM_SOFTWARE,
        $EDF_TRXN_SOFTWARE,

        $EDF_IMAGE_FORMAT,

        $EDF_ARRAYACCESSION,
        $EDF_ARRAYSERIAL,

        "$EDF_FILE_PREFIX.*$EDF_FILE_SUFFIX",
        "$EDF_BMC_PREFIX.*$EDF_BMC_SUFFIX",
        "$EDF_FV_PREFIX.*$EDF_FV_SUFFIX",
        "$EDF_PARAM_PREFIX.*$EDF_PARAM_SUFFIX",
    ];

    my $error |= _check_section(
	$testarray,
	$matcharray,
	'Hybridization',
	$error_fh,
    );

    return $error;

}

=pod

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2004.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

1;
