#!/usr/bin/env perl
#
# Module to provide basic methods used by the other ArrayExpress::Curator
# modules.
#
# Tim Rayner 2004, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Common.pm 2013 2008-03-31 17:23:14Z tfrayner $
#

package ArrayExpress::Curator::Common;

use strict;
use warnings;

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../index.html">
                <img src="../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: Common.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Curator::Common.pm - a module providing some simple
utility functions and regexps.

=head1 SYNOPSIS

 use ArrayExpress::Curator::Common qw(date_now $RE_EMPTY_STRING);
 
 if ( $test =~ $RE_EMPTY_STRING ) {
    print date_now();
 }

=head1 DESCRIPTION

This is a simple module providing utility functions and regexp
patterns used elsewhere in the Tab2MAGE package.

=head1 FUNCTIONS

=over 2

=item C<strip_discards( $indexcols, $line_array )>

Sub to strip values out of an arrayref ($line_array) based on an
arrayref of unwanted array indices ($indexcols). Does not modify the
input lists, but instead returns a suitably stripped arrayref.

=item C<round( $number, $precision )>

A simple rounding function that returns $number rounded to $precision
decimal places.

=item C<get_indexcol( $list, $name )>

Function to return the first index within @$list matching the string
or regexp passed as $name. Returns -1 on failure.

=item C<check_linebreaks( $path )>

Takes a filename as an argument, checks for Mac, Unix or DOS line
endings by reading the whole file in chunks, and regexp matching the
various linebreak types.  Returns the appropriate linebreak for
acceptable line breaks (line breaks must be unanimous), undef if a
consensus is unreachable.

=item C<clean_hash( $hashref )>

Strip out undef or empty string values from $hashref.

=item C<date_now()>

Return current date and time as a MAGE best practice date-time string
(uses mage_date(), below).

=item C<mage_date( $time )>

When passed the return value from e.g. perl's time() built-in, returns
a MAGE best practice date-time string. This string will always be from
the UTC/GMT time zone.

=item C<untaint( $string )>

A convenient data untainting function which replaces any run of
non-whitelisted characters with a single underscore.

=item C<find_cdf( $name, $dir )>

Given a filename and an optional directory, returns an actual filename
found in a case-insensitive fashion. Preferentially checks the main
Affy library directory if set in $CONFIG (see
L<ArrayExpress::Curator::Config>).

=item C<decamelize( $string )>

Function to convert CamelCase strings to underscore_delimited.

=item C<get_filepath_from_uri( $string, $dir )>

Given a string and an optional directory argument, this function
determines which URI scheme is in use (default is "file://"), and
downloads "http://" and "ftp://" URIs to either the indicated
filesystem directory or the current working directory. Returns the
filesystem path to the file.

=back

=head1 REGEXPS

=over 2

=item C<$RE_EMPTY_STRING>

Matches an empty string (whitespace ignored).

=item C<$RE_COMMENTED_STRING>

Matches a string beginning with #.

=item C<$RE_SURROUNDED_BY_WHITESPACE>

Matches a string with whitespace on either side; $1 contains the
string minus the whitespace.

=item C<$RE_WITHIN_PARENTHESES>

Matches a string with parentheses on either side; $1 contains the
string minus the parentheses.

=item C<$RE_WITHIN_BRACKETS>

Matches a string with brackets on either side; $1 contains the
string minus the brackets.

=item C<$RE_SQUARE_BRACKETS>

Matches either [ or ]; $1 contains the matched character.

=item C<$RE_LINE_BREAK>

Matches a linebreak (DOS, Unix or MacOS).

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2008.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

use Carp;
use charnames qw( :full );

use Scalar::Util qw( openhandle );
use File::Spec;
use IO::File;
use IO::Handle;
use Readonly;
use English qw( -no_match_vars );

use ArrayExpress::Curator::Config qw($CONFIG);

use base 'Exporter';
our @EXPORT_OK = qw(
    strip_discards
    round
    get_indexcol
    check_linebreaks
    clean_hash
    date_now
    mage_date
    untaint
    find_cdf
    decamelize
    get_filepath_from_uri
    mx2tab

    $RE_EMPTY_STRING
    $RE_COMMENTED_STRING
    $RE_SURROUNDED_BY_WHITESPACE
    $RE_WITHIN_PARENTHESES
    $RE_WITHIN_BRACKETS
    $RE_SQUARE_BRACKETS
    $RE_LINE_BREAK
);

# Define some standard regexps:
Readonly our $RE_EMPTY_STRING             => qr{\A \s* \z}xms;
Readonly our $RE_COMMENTED_STRING         => qr{\A [\"\s]* \#}xms;
Readonly our $RE_SURROUNDED_BY_WHITESPACE => qr{\A [\"\s]* (.*?) [\"\s]* \z}xms;
Readonly our $RE_WITHIN_PARENTHESES       => qr{\( \s* (.*?) \s* \)}xms;
Readonly our $RE_WITHIN_BRACKETS          => qr{\[ \s* (.*?) \s* \]}xms;
Readonly our $RE_SQUARE_BRACKETS          => qr{( [\[\]] )}xms;
Readonly our $RE_LINE_BREAK               => qr{[\r\n]* \z}xms;

##################################################################
# Given a list and a value in that list, returns the value index #
##################################################################

sub get_indexcol {
    my ( $columnlist, $columnname ) = @_;

    ref $columnlist eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    defined($columnname) or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # For efficiency; N.B. $columnname may contain whitepace, so no /x
    # modifier here. Similarly we rely on the caller to specify /i or not.
    my $re;

    # Empty string is a special case (won't match \b)
    if ( $columnname eq q{} ) {
	$re = qr/\A\s*$columnname\s*\z/ms;
    }
    else{
	$re = qr/\A\s*\b$columnname\b\s*\z/ms;
    }
    my $num_cols = scalar @{$columnlist};

    for ( my $i = 0; $i < $num_cols; $i++ ) {
        return $i if $columnlist->[$i] =~ $re;
    }
    return -1;
}

sub strip_discards {

    # Sub to strip values out of an array (@line_array) based on a list
    # of unwanted array indices (@indexcols). Does not modify the input
    # lists.

    my ( $indexcols, $line_array ) = @_;

    ref $indexcols eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $line_array eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my %to_be_discarded = map { $_ => 1 } @$indexcols;

    my @new_line;
    my $last_index = $#$line_array;    # Calculate this just once
    foreach my $i ( 0 .. $last_index ) {
        push( @new_line, $line_array->[$i] )
            unless ( $to_be_discarded{$i} );
    }

    return \@new_line;

}

##############################################################
# Round function to deal with floating-point rounding errors #
##############################################################

# stolen shamelessly from www.perlmonks.org

sub round {
    my ( $number, $precision ) = @_;
    my $sign = ( $number > 0 ) ? 1 : -1;    # store the sign for later

    $precision ||= 0;    # $precision should not be undefined

    $number *= 10**$precision;    # move the decimal place
                                  # $precision places to the
                                  # right

    $number = int( $number + .5 * $sign );    # add 0.5, correct the sign and
                                              # truncate the number after the
                                              # decimal point, thereby
                                              # rounding it

    return ( $number / 10**$precision );   # move the decimal place back again
}

sub clean_hash {

    # Strip out undef or empty string values

    my ($hashref) = @_;

    ref $hashref eq 'HASH'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my %cleaned;
    while ( my ( $key, $value ) = each %$hashref ) {
        $cleaned{$key} = $value if ( defined($value) && $value ne q{} );
    }

    return \%cleaned;

}

sub check_linebreaks {

    # Takes a filename as an argument, checks for Mac, Unix or Dos line
    # endings by reading the whole file in chunks, and regexp
    # matching the various linebreak types.  Returns the appropriate
    # linebreak for acceptable line breaks (line breaks must be
    # unanimous FIXME), undef for not.

    my $path = shift;
    my $fh = IO::File->new( $path, '<' )
        or croak(
        "Error: Failed to open file $path for linebreak checking: $!\n");

    my $bytelength = -s $path;

    # Count all the line endings. This can get memory intensive
    # (implicit list generation, can be over 1,000,000 entries for
    # Affy CEL). We read the file in defined chunks to address this.
    my ( $unix_count, $mac_count, $dos_count );
    my $chunk_size          = 3_000_000;    # ~10 chunks to a big CEL file.
    my $previous_final_char = q{};
    for ( my $offset = 0; $offset < $bytelength; $offset += $chunk_size ) {

        my $chunk;

	my $bytes_read = read( $fh, $chunk, $chunk_size );

	unless ( defined($bytes_read) ) {
	    croak("Error reading file chunk at offset $offset ($path): $!\n");
	}

	# Lists generated implicitly here.
        $unix_count += () = ( $chunk =~ m{\N{LINE FEED}}g );
        $mac_count  += () = ( $chunk =~ m{\N{CARRIAGE RETURN}}g );
        $dos_count  += () = ( $chunk =~ m{\N{CARRIAGE RETURN}\N{LINE FEED}}g );

        # DOS line endings could conceivably be split between chunks.
	if ( $bytes_read ) {    # Skip if at end of file.
	    if (   ( substr( $chunk, 0, 1 ) eq "\N{LINE FEED}" )
		&& ( $previous_final_char eq "\N{CARRIAGE RETURN}" ) ) {
		$dos_count++;
	    }
	    $previous_final_char = substr( $chunk, -1, 1 );
	}
    }

    close($fh)
        or croak("Error closing file $path in sub check_linebreaks: $!\n");

    my $dos  = $dos_count;
    my $mac  = $mac_count - $dos_count;
    my $unix = $unix_count - $dos_count;

    # Set to undef on failure.
    my $line_ending = undef;

    # Determine the file line endings format, return the "standard" line
    # ending to use
    if ( $unix && !$mac && !$dos ) {    # Unix
        $line_ending = "\N{LINE FEED}";
    }
    elsif ( $mac && !$unix && !$dos ) {    # Mac
        $line_ending = "\N{CARRIAGE RETURN}";
    }
    elsif ( $dos && !$mac && !$unix ) {    # DOS
        $line_ending = "\N{CARRIAGE RETURN}\N{LINE FEED}";
    }

    # Calling in scalar context just gives $line_ending.
    my $counts = {
        unix => $unix,
        dos  => $dos,
        mac  => $mac,
    };
    return wantarray ? ( $counts, $line_ending ) : $line_ending;

}

sub date_now {

    # Return current date and time as a MAGE best practice date-time
    # string.

    return mage_date(time);
}

sub mage_date {

    my $datenum = shift;

    # Return a MAGE best practice date-time string. Note that the
    # following "Z" implies UTC, hence we use gmtime here.

    my ( $sec, $min, $hour, $mday, $mon, $year, $wday ) = gmtime($datenum);
    $mon++;    # localtime starts months from 0
    $year += 1900;    # localtime starts years from 100

    return sprintf( "%04d-%02d-%02dT%02d:%02d:%02dZ",
        $year, $mon, $mday, $hour, $min, $sec );

}

sub untaint {

    # Untaint the input (runs with perl -T switch)

    my $string = shift;
    my @nameparts = $string =~ m/[-a-zA-Z0-9\.@\-\+\_]+/g;
    $string = join( '_', @nameparts );
    return $string;
}

sub find_cdf {

    # Given a filename and an optional directory, returns an actual
    # filename found in a case-insensitive fashion. Preferentially
    # checks the main Affy library directory if set in $CONFIG.
    my ( $name, $dir ) = @_;

    # Check Affy library dir or source dir or working dir.
    my $querydir  = $CONFIG->get_AFFYMETRIX_LIBRARYPATH()
	      	 || $dir
		 || q{.};

    opendir (my $dirhandle, $querydir)
	or croak("Error: Can't open directory $querydir");

    my $actualname;
    while ( my $candidate = readdir($dirhandle) ) {
	if ( $name =~ /\A $candidate \z/ixms ) {
	    $actualname = $candidate;
	    last;
	}
    }

    my $path = File::Spec->catfile( $querydir, $actualname );

    # Beware calling in list context.
    return $actualname ? ($actualname, $path)
	   : wantarray ? ()
	   : undef;
}		    

sub decamelize {

    # Function to convert CamelCase strings to underscore_delimited.
    my ( $camel ) = @_;

    # Underscore separates internal capitals
    $camel =~ s/([a-z])([A-Z])/$1\_$2/g;

    # substitute spaces
    $camel =~ s/\s+/\_/g;

    # and then lowercase
    $camel = lc($camel);

    return $camel;
}

sub get_filepath_from_uri {

    my ( $uri_string, $dir ) = @_;

    require URI;

    # N.B. URI module doesn't seem to like the file://my_filename.txt
    # URI form, and confuses the path with the authority. URI module
    # behaviour is probably correct, but the MAGE-TAB spec asks for
    # this (invalid?) URI form. We fix that here:
    $uri_string =~ s/\A file:\/\/ //ixms;

    my $uri = URI->new( $uri_string );

    # Assume file as default URI scheme.
    my $path;
    if ( ! $uri->scheme() || $uri->scheme() eq 'file' ) {

	$uri->scheme('file');

	# URI::File specific, this avoids quoting e.g. spaces in filenames.
	my $uri_path = $uri->file();

	if ( $dir ) {
	    $path = File::Spec->file_name_is_absolute( $uri_path )
		  ? $uri_path
		  : File::Spec->catfile( $dir, $uri_path );
	}
	else {
	    $path = File::Spec->rel2abs( $uri_path );
	}
    }
    # Add the common network URI schemes.
    elsif ( $uri->scheme() eq 'http' || $uri->scheme() eq 'ftp' ) {
	$path = cache_network_file( $uri, $dir );
    }
    else {
	croak(sprintf(
	    "ERROR: Unsupported URI scheme: %s\n", $uri->scheme(),
	));
    }

    return $path;
}

sub cache_network_file {

    my ( $uri, $dir ) = @_;

    require LWP::UserAgent;

    # N.B. we don't handle URI fragments, just the path.
    my ( $basename ) = ( $uri->path() =~ m!/([^/]+) \z!xms );

    my $target;
    if ( $dir ) {
	$target = File::Spec->catfile( $dir, $basename );
    }
    else {
	$target = $basename;
    }

    # Only download the file once.
    unless ( -f $target ) {

	printf STDOUT (
	    qq{Downloading network file "%s"...\n},
	    $uri->as_string(),
	);

	# Download the $uri->as_string()
	my $ua = LWP::UserAgent->new();

	my $response = $ua->get(
	    $uri->as_string(),
	    ':content_file' => $target,
	);

	unless ( $response->is_success() ) {
	    croak(sprintf(
		qq{Error downloading network file "%s" : %s\n},
		$uri->as_string(),
		$response->status_line(),
	    ));
	}
    }

    return $target;
}

sub mx2tab {

    # Temporary method to wrap calls to mx_to_mtab until better error
    # reporting is included in that call.

    my ( $submission ) = @_;

    eval {
	require ExperimentConvert;
	ExperimentConvert->import('mx_to_mtab');
    };
    if ($EVAL_ERROR) {
	# FIXME not really an error; module may just not be
	# available. This can probably be safely removed once
	# ExperimentConvert is included in tab2mage core.
    }
    else {

	eval {
	    mx_to_mtab($submission);
	};

	if ($EVAL_ERROR) {
	    # FIXME do something here (note that status has already
	    # been set). This will be more valuable once the module
	    # dies correctly on failure. N.B. may be better handled in
	    # the caller.
	}
	else {

	    # This is the best we can do at the moment to detect
	    # failure.
	    if ( $submission->status() =~ /\b fail(?:ed|ure) \b/ixms ) {
		die("Error: mx_to_mtab process failed.");
	    }

	    # Submission should have been set to status='Waiting',
	    # experiment_type='MAGE-TAB' at this point.  We also have
	    # to set the in_curation flag here:
	    $submission->set( in_curation => 1 );
	    $submission->update();
	}
    }

}

1;
