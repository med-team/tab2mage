#!/usr/bin/env perl
#
# MIAMExpress.pm - a module derived from and used in the experiment
# checker script. Contains routines which might be useful elsewhere.
#
# Tim Rayner 2004 ArrayExpress team, EBI
#
# $Id: MIAMExpress.pm 2069 2008-06-04 14:33:52Z tfrayner $
#

package ArrayExpress::Curator::MIAMExpress;

use strict;
use warnings;

require DBI;
require DBD::mysql;
use Tie::IxHash;
use Carp;
use IO::File;
use Class::Std;
use Readonly;
use English qw( -no_match_vars );

use ArrayExpress::Curator::Entrez_list qw(
    parse_entrez_names
);

use ArrayExpress::Curator::Report qw(
    datafile_consistency_table
    biomaterials_report
    format_description
);

use ArrayExpress::Curator::Config qw($CONFIG);

use ArrayExpress::Curator::Database qw(
    parse_adf
    get_ae_array_details
);

require ArrayExpress::Datafile;

use base 'ArrayExpress::Curator::ExperimentChecker';

my %mx_login             : ATTR( :get<mx_login>,                :init_arg<mx_login>,                :default<undef> );
my %mx_title             : ATTR( :get<mx_title>,                :init_arg<mx_title>,                :default<undef> );
my %tsubmis_sysuid       : ATTR( :default<undef> );
my %submission_filebase  : ATTR( :default<undef> );
my %mx_dbh               : ATTR( :default<undef> );

# Map each of the know experimental factors (TCTLVCB_DESCR values) to
# the relevant column(s) in the TSAMPLE table. This is used in
# reporting both fv->hyb relationships and the full sample annotation
# report.
Readonly my %CATEGORY_SQL_MAP => (

    # These are the core factors currently supported by MX.
    age => [
        qw(TSAMPLE_AGERANGE_MIN TSAMPLE_AGERANGE_MAX TSAMPLE_TIME_UNIT TSAMPLE_TIME_POINT)
    ],
    compound            => [],
    disease_state       => [qw(TSAMPLE_DISEASE_STATE)],
    cell_line           => [qw(TSAMPLE_CELL_LINE)],
    cell_type           => [qw(TSAMPLE_TARGET_CELL_TYPE)],
    developmental_stage => [qw(TSAMPLE_DEV_STAGE)],
    dose                => [],
    genotype            => [qw(TSAMPLE_INDIVIDUAL_GEN)],
    organism_part       => [qw(TSAMPLE_ORGANISM_PART)],
    sex                 => [qw(TSAMPLE_SEX)],
    organism            => [qw(TSAMPLE_TAXID)],
    strain_or_line      => [qw(TSAMPLE_STRAIN)],
    temperature         => [],
    time                => [],
    other               => [],
    clinical_information => [qw(TSAMPLE_ADDITIONAL)],
    individual           => [qw(TSAMPLE_INDIVIDUAL)],
    irradiate            => [],
    'ChiP-antibody'      => [],    # typo in MIAMExpress;
    'ChIP-antibody'      => [],    # the correct version, in case they ever fix it.

    # The following are not currently factors, the keys may therefore change.
    provider             => [qw(TSAMPLE_CELL_PROVIDER)],
    type                 => [qw(TSAMPLE_SAMPLE_TYPE)],
    genetic_modification => [qw(TSAMPLE_GENETIC_VARIATION)],
    separation_technique => [qw(TSAMPLE_SEPARATION_TECH)],
);

sub START {

    my ( $self, $id, $args ) = @_;

    unless ( $mx_login{$id} && $mx_title{$id} ) {
	croak("Error: Both mx_title and mx_login must be set for MIAMExpress checking.");
    }

    # Set the locations for the logfiles. N.B. get_tsubmis_sysuid()
    # connects to the MX database to retrieve TSUBMIS_SYSUID here.
    my $logfile_str = sprintf(
	"%s_sub%s",
	$self->get_mx_login(),
	$self->get_tsubmis_sysuid(),
    );
    $self->localize_logfiles({
	name      => $logfile_str,
	directory => $self->get_submission_filebase(),
    });

    return;
}

sub get_files_and_annotation : RESTRICTED {

    my ( $self ) = @_;

    # Quick check to see what we've been passed.
    unless ( $self->get_mx_login() && $self->get_mx_title() ) {
        croak(<<'END_ERROR');
Error: MIAMExpress checker must be passed both "mx_login" and "mx_title" options.
END_ERROR
    }

    my $filelist;
    my $hyb_ids;         # Used to map FGEM column headings to hybs.
    my $norm_ids = {};   # Empty for MX; cannot map MX FGEM using Norm IDs.

    print STDOUT ("Running in MIAMExpress mode...\n");

    ( $filelist, $hyb_ids ) = $self->retrieve_MX_experiment();

    return ( $filelist, $hyb_ids, $norm_ids );
}

sub get_mx_dbh : PRIVATE {

    my ( $self ) = @_;

    # Set up the DB connection, unless it's already been done.
    unless ( $mx_dbh{ident $self} ) {

	unless ( $CONFIG->get_MX_DSN() ) {
	    croak(
		"Error: MIAMExpress connection parameters"
	      . " not correctly set in site config YAML file.\n"
	    );
	}

	print STDOUT ("Connecting to MIAMExpress database...\n");
	my $dbh = DBI->connect(
	    $CONFIG->get_MX_DSN(),      $CONFIG->get_MX_USERNAME(),
	    $CONFIG->get_MX_PASSWORD(), $CONFIG->get_MX_DBPARAMS()
	)
	    or croak("$DBI::errstr\n");

	$mx_dbh{ident $self} = $dbh;
    }

    return $mx_dbh{ident $self};
}

sub get_tsubmis_sysuid {

    my ( $self ) = @_;

    unless ( defined $tsubmis_sysuid{ident $self} ) {
	my ( $sysuid, $filebase ) = $self->query_mx_for_subs_info();
	$tsubmis_sysuid{ident $self} = $sysuid;
	$submission_filebase{ident $self} = $filebase;
    }

    return $tsubmis_sysuid{ident $self};
}

sub get_submission_filebase {

    my ( $self ) = @_;

    unless ( defined $submission_filebase{ident $self} ) {
	my ( $sysuid, $filebase ) = $self->query_mx_for_subs_info();
	$tsubmis_sysuid{ident $self} = $sysuid;
	$submission_filebase{ident $self} = $filebase;
    }

    return $submission_filebase{ident $self};
}

sub _get_db_adf_features : PRIVATE {

    my ( $self, $hyb_ids ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    # First, get a list of ARRAY_DESIGNIDs
    my %array_designs;
    foreach my $hyb_sysuid ( values %$hyb_ids ) {
        my $design_id = $self->_get_array_designid( $hyb_sysuid );

        # This rationalises the list so we only parse each ADF once
        $array_designs{$design_id}++;
    }

    # Next, process that list.  Table retrieved from ArrayExpress, but
    # only if we need to:
    foreach my $design_id ( keys %array_designs ) {

        if ( $design_id > 0 ) {

            # ADF comes from MIAMExpress
            my ( $adf_filename, $arrayname )
                = $self->_get_adf_filename( $design_id );

            unless ( $adf_filename && $arrayname ) {
                $self->logprint( 'error',
                    "DATABASE ERROR: Incomplete data returned by MIAMExpress for array design id $design_id.\n"
                );

                # This is an unlikely error; if this is tripped who knows how
                # much else is broken?
                $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
            }
            else {
                $self->logprint( 'error',
                    "Warning: Using ADF from MIAMExpress: $arrayname. Array not yet loaded and public in ArrayExpress.\n"
                );

                # Loading cannot occur if the array is not loaded.
                $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
            }

	    if ( $self->get_skip_data_checks() ) {
		print STDOUT ("Skipping ADF parsing.\n");
	    }
	    else {
		my $db_adf_fh = IO::File->new( $adf_filename, '<' )
		    or croak("Error: Unable to open $adf_filename: $!\n");
		my $array = parse_adf(
		    $db_adf_fh,
		    undef,
		    $self->get_reporter_prefix(),
		    $self->get_compseq_prefix(),
		);
		close($db_adf_fh);
		$self->set_cached_array_design( $design_id, $array );
	    }
        }

        elsif ( $design_id < 0 ) {

            # ADF comes from ArrayExpress.

            # We actually want a positive database id.
            my $arrayexpress_id = -$design_id;

	    $self->get_ae_arraydesign({
		database_id => $arrayexpress_id,
	    });
        }
    }
}

sub _MX_populate_array_designs : PRIVATE {

    my ( $self, $filelist, $hyb_ids ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    if ( $self->get_adf_filename || $self->get_array_accession ) {

        $self->cache_user_supplied_arrays( $filelist );

    }
    else {

        # Go to the database(s) and get as many ADFs as are linked to
        # by the hybridizations in MIAMExpress.

        # Get the associated ADF features from the database.  These
        # are stored in an ExperimentChecker method wrapping Storable
        # caches on disk.

	$self->_get_db_adf_features( $hyb_ids );

    }

    $self->populate_file_arraydesigns( $filelist );

    return;
}

sub _check_db_publication : PRIVATE {

    # Checks the associated publication details,prints warnings and/or
    # errors to error log.

    my ( $self ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $sth = $dbh->prepare(<<'QUERY');
select distinct TCTLVCB_VALUE, TPUBLIC_SYSUID from TPUBLIC, TCTLVCB
where TPUBLIC_SUBID=?
and TPUBLIC_PUBLICATION=TCTLVCB_SYSUID
and TPUBLIC_DEL_STATUS='U' and TCTLVCB_DEL_STATUS='U'
QUERY

    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    my %publications;
    while ( my $rowref = $sth->fetchrow_arrayref ) {

        # publication sysuid => publication name
        $publications{ $rowref->[1] } = $rowref->[0];
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # Check through for others, get the appropriate name from TOTHERS
    # and replace the value in the hash
    foreach my $pub_sysuid ( keys %publications ) {

        if ( $publications{$pub_sysuid} =~ m/other/i ) {

            $self->logprint(
		'error',
                qq{Warning: "Other" publication specified in submission.\n},
	    );
            $self->add_error($CONFIG->get_ERROR_INNOCENT());

            my $sth = $dbh->prepare(<<'QUERY');
select distinct TOTHERS_VALUE from TOTHERS
where TOTHERS_PUBLICID=?
and TOTHERS_DEL_STATUS='U'
QUERY
            $sth->execute($pub_sysuid) or croak($sth->errstr);

            my $rowref = $sth->fetchrow_arrayref;
            $publications{$pub_sysuid} = $rowref->[0];

            # dump the statement handler now that we are finished with it
            $sth->finish();
        }
    }

    # Check through the publication names
    my $entrez_approved = parse_entrez_names();
    foreach my $name ( values %publications ) {
        unless ( $entrez_approved->{$name} ) {
            $self->logprint(
		'error',
		"Warning: Non-standard publication name: $name\n",
	    );
            $self->add_error($CONFIG->get_ERROR_INNOCENT());
        }
    }

    return;
}

sub retrieve_MX_experiment : PRIVATE {

    my ( $self ) = @_;

    ###########################
    # Connect to the database #
    ###########################

    print STDOUT ("Querying MIAMExpress for submission data... \n");

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    ##########################################
    # Initialise top level dir and log files #
    ##########################################

    $self->logprint( 'error', "* MIAMExpress mode *\n\n" );
    $self->logprint_line( 'error', 'DB query START' );

    #######################
    # Biomaterials report #
    #######################

    $self->_check_db_publication();

    print STDOUT ("Writing BioMaterials report file... ");

    # Get the associations between sample, extract, labeled extract, and
    # hybs as a heirarchical hashref.
    my ( $samples_to_hybs, $protocol_typemap, $protocol_others, $rc )
        = $self->_get_db_objects();
    $self->add_error($rc);

    # Count the samples, extracts, LEs and hybs. FIXME to deal with NULLs
    my %bm_summary;

    # Sample
    @{ $bm_summary{samples} }{ keys %$samples_to_hybs }
        = values %$samples_to_hybs;
    foreach my $sample ( values %$samples_to_hybs ) {

        # Extract
        @{ $bm_summary{extracts} }{ keys %$sample } = values %$sample;
        foreach my $extract ( values %$sample ) {

            # Label
            @{ $bm_summary{labels} }{ keys %$extract } = values %$extract;
            foreach my $label ( values %$extract ) {

                # Hyb
                @{ $bm_summary{hybs} }{ keys %$label } = values %$label;
            }
        }
    }

    # Count the contants of the hashrefs, print out the results.
    my %bm_count;
    foreach my $bm ( keys %bm_summary ) {
        $bm_count{$bm} = scalar( grep { defined $_ } values %{ $bm_summary{$bm} } );
    }

    $self->logprint(
        'workflow',
        "$bm_count{samples} samples -> ",
        "$bm_count{extracts} extracts -> ",
        "$bm_count{labels} labeled extracts -> ",
        "$bm_count{hybs} hybridizations\n"
    );

    # Print out the trees, forward and reverse:
    biomaterials_report(
        $samples_to_hybs,
        'Samples to Hybridizations',
        $self->log_fh('workflow')
    );
    my $hybs_to_samples = $self->_invert_heirarchy($samples_to_hybs);
    biomaterials_report(
        $hybs_to_samples,
        'Hybridizations to Samples',
        $self->log_fh('workflow')
    );

    # Get relationships between FactorValues, Hybs and data files.
    # $hyb_factors is hyb_id => {fv_string1 => 1, etc.} for reporting;
    # $hyb_category_fvs is hyb_id => {category1 => {value_string1 => 1} }
    # for mapping files to FVs.
    my $hyb_category_fvs;
    if ( $CONFIG->get_MX_EXTENDED_REPORT() ) {
	my $hyb_factors;
	( $hyb_factors, $hyb_category_fvs ) = $self->_get_db_factor_assns();
	$self->add_error($rc);
	biomaterials_report(
	    $hyb_factors,
	    'Hybridizations to FactorValues',
	    $self->log_fh('workflow')
	);
	my $reversed_factors = $self->_invert_heirarchy($hyb_factors);
	biomaterials_report(
	    $reversed_factors,
	    'FactorValues to Hybridizations',
	    $self->log_fh('workflow')
	);
    }

    print STDOUT ("done.\n");

    if ( $CONFIG->get_MX_EXTENDED_REPORT() ) {

	print STDOUT ("Generating sample annotation report... ");

	$self->_get_db_sample_annotation();

	print STDOUT ("done.\n");
    }

    # Collect the design type info for later.
    $self->add_expt_designs(
	@{ $self->_get_db_expt_designs() }
    );

    #############################
    # Get data file information #
    #############################

    # Get a list of files in the database submitted via MIAMExpress for
    # this submission.  This is an array of
    # ArrayExpress::Datafile objects.
    my $filelist = $self->get_db_files();

    # Add factor information to the datafile objects (raw and norm only).
    if ( $hyb_category_fvs ) {

	FILE:
	foreach my $file (@$filelist) {

	    # Skip FGEM - they have no hyb_identifier.
	    next FILE
		if ( $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE() );

	    # We've mapped the FVs to HYB_ID, not HYB_SYSUID.
	    my $hyb_id = $file->get_hyb_identifier();

	    while ( my ($category, $values)
			= each %{$hyb_category_fvs->{$hyb_id}} ) {

		foreach my $value_string (keys %$values) {
		    $file->add_factor_value( $category, $value_string );
		}
	    }
	}
    }

    # get a list of hyb_ids pointing to a list of hyb_sysuids
    my $hyb_ids = $self->_get_hyb_ids();

    # Check for Qualifier Value Source entries
    my $qvs = $self->_get_db_qvs();
    if (@$qvs) {
        $self->logprint( 'error', "Warning: QVS used in annotation:\n" );
        $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
        foreach my $entry (@$qvs) {

            if ( $entry->{sample_id} ) {    # Sample QVS
                $self->logprint(
                    'error',
                    sprintf(
                        "Sample qualifier      Name: %-22s Value: %s\n",
                        $entry->{qualifier}, $entry->{value}
                    )
                );

            }
            else {                          # Experiment QVS
                $self->logprint(
                    'error',
                    sprintf(
                        "Experiment qualifier  Name: %-22s Value: %s\n",
                        $entry->{qualifier}, $entry->{value}
                    )
                );
            }
        }
    }

    # Check for "Other" values
    my $others = $self->_get_db_others();

    # Skip empty values for deleted OTHERS - this seems to be a bug
    # in MX (DEL_STATUS not set).
    my @cleaned_others;
    foreach my $other (@$others) {
        if ( $other->{value} ) {
            push( @cleaned_others, $other );
        }
    }

    if (@cleaned_others) {
        $self->logprint( 'error',
            qq{Warning: "Other" values used in annotation:\n} );

        OTHER_ENTRY:
        foreach my $entry (@cleaned_others) {

            if ( $entry->{sample_id} ) {    # Sample OTHER
                $self->logprint(
                    'error',
                    sprintf(
                        "Sample value      Type: %-22s Value: %s\n",
                        $entry->{id}, $entry->{value}
                    )
                );

            }
            else {                          # Experiment OTHER
                $self->logprint(
                    'error',
                    sprintf(
                        "Experiment value  Type: %-22s Value: %s\n",
                        $entry->{id}, $entry->{value}
                    )
                );
            }
            $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
        }
    }

    # Nested hash: {protocol_type => {protocol_sysuid => {others_id =>
    #                                                        others_value}}}
    while ( my ( $ptype, $others ) = each %$protocol_others ) {
        foreach my $protocol ( values %$others ) {

            OTHER_PROTOCOL:
            while ( my ( $key, $value ) = each %$protocol ) {

                # Skip empty values for deleted OTHERS - this seems to be a
                # bug in MX (DEL_STATUS not set).
                next OTHER_PROTOCOL unless ($value);

                $self->logprint( 'error',
                    qq{Warning: "Other" values used in $ptype protocol: $key => $value\n}
                );
                $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
            }
        }
    }

    $self->logprint_line( 'error', 'DB query END' );

    $self->logprint_line( 'error', 'Protocol check START' );

    ##################
    # Protocol check #
    ##################

    print STDOUT ("Generating protocol report... ");

    # Count the number of combined (transformed) and normalized data files
    my ( $combined_count, $normalized_count );
    foreach my $file (@$filelist) {
        $combined_count++
            if ( $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE() );

        $normalized_count++
	    if ( $file->get_data_type() eq 'normalized' );
    }

    # Then we check the protocols reported by &_get_db_objects
    my (%protocol_text, $norm_protocol_found);
    foreach my $ptype ( keys %$protocol_typemap ) {

        PROTOCOL_BY_TYPE:
        foreach my $obj_id ( keys %{ $protocol_typemap->{$ptype} } ) {

            # Count the protocols associated with a given object id
            my $pcount = scalar @{ $protocol_typemap->{$ptype}{$obj_id} };

            # Check for multiple protocols associated with an object
            # (shouldn't happen).
            if ( $pcount > 1 ) {
                $self->logprint( 'error',
                    "Warning: Multiple protocols of type \'$ptype\' attached to object \'$obj_id\'\n"
                );
                $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
            }

	    # Check protocol lengths, warn if too short (<50 chars at
	    # the moment)
	    PROTOCOL_LENGTH:
            foreach my $protocol ( @{ $protocol_typemap->{$ptype}{$obj_id} } ) {

		# GLOB indicates an AE public protocol (e.g. Affymetrix).
		next PROTOCOL_LENGTH if defined($protocol->{glob});

                if ( !defined($protocol->{text})
			 || ( length($protocol->{text}) < 50 ) ) {
                    $self->logprint( 'error',
                        qq{Warning: Description too short for $ptype protocol attached to '$obj_id'\n}
                    );
                    $self->add_error( $CONFIG->get_ERROR_MIAME() );
                }

		# Note the text for reporting below.
		$protocol_text{$ptype}{$protocol->{id}} = $protocol;
            }

            # Check for pooling protocols
            if ( $ptype eq 'pooling' ) {
                if ( $pcount > 0 ) {
                    $self->logprint( 'error',
                        qq{Warning: Pooling protocol attached to extract '$obj_id'\n}
                    );
                    $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
                }
                next PROTOCOL_BY_TYPE;
            }

            # Check for normalization protocols
            if ( $ptype eq 'normalization' ) {
		$norm_protocol_found = 1 if $pcount;
                if ( $normalized_count && ( $pcount <= 0 ) ) {
                    $self->logprint( 'error',
                        qq{Warning: Normalized data for hybridization '$obj_id'}
		      . qq{ supplied without normalization protocol.\n}
                    );
                    $self->add_error( $CONFIG->get_ERROR_MIAME() );
                }
                next PROTOCOL_BY_TYPE;
            }

            # Check for transformation protocols
            if ( $ptype eq 'transformation' ) {
		$norm_protocol_found = 1 if $pcount;
                if ( $combined_count && ( $pcount <= 0 ) ) {
                    $self->logprint( 'error',
                        qq{Warning: Final data matrix file supplied}
    	              . qq{ without transformation protocol.\n}
                    );
                    $self->add_error( $CONFIG->get_ERROR_MIAME() );
                }
                next PROTOCOL_BY_TYPE;
            }

            # Check for unbound protocols (pooling, normalization omitted at
            # this stage).
            if ( $pcount <= 0 ) {
                $self->logprint( 'error',
                    "Warning: Object \'$obj_id\' not bound to $ptype protocol.\n" );
                $self->add_error( $CONFIG->get_ERROR_MIAME() );
            }
        }
    }

    # Note the results for MIAME compliance tracking.
    if ($norm_protocol_found) {
	$self->add_miame($CONFIG->get_MIAME_NORMPROTOCOL);
    }
    else {
	$self->logprint(
	    'miame',
	    "Problem: Data transformation protocol not found.\n"
	);
    }

    foreach my $ptype qw(
			 growth
			 treatment
			 pooling
			 extraction
			 labeling
			 hybridization
			 scanning
			 normalization
			 transformation
		     ) {
	foreach my $protocol ( values %{$protocol_text{$ptype}} ) {
	    $self->logprint_line(
		'protocol',
		"[$ptype protocol] $protocol->{name}");
	    $self->logprint('protocol', "$protocol->{text}\n\n");
	}
    }
    $self->logprint_line( 'error', 'Protocol check END' );
    print STDOUT ("done.\n");

    # Start the report log.
    $self->logprint( 'report', "* MIAMExpress mode *\n\n" );
    $self->logprint(
        'report', "  User login: ",
        $self->get_mx_login, "\n", "  Experiment: ",
        $self->get_mx_title, "\n\n"
    );

    $self->_report_description();

    ######################################
    # Deal with Array design information #
    ######################################

    # Get the associated array designs on a per-hyb basis

    $self->logprint_line( 'error', 'ADF parsing START' );

    # Populate the $file->get_adf_features() hashrefs within @$filelist
    $self->_MX_populate_array_designs( $filelist, $hyb_ids );

    $self->logprint_line( 'error', 'ADF parsing END' );

    # Data file check on consistency across hybs
    $self->logprint( 'report',
        "\nChecking for presence of raw and/or normalised data files:\n" );
    $self->logprint( 'report', datafile_consistency_table($filelist) );

    print STDOUT ("done.\n");

    # We don't disconnect from the database, in case we're operating
    # over a batch of experiments to be checked. The autosubs daemons
    # fork before connecting, so we shouldn't have persistent
    # connections to the database.
    return ( $filelist, $hyb_ids );

}

sub query_mx_for_subs_info : PRIVATE {

    my ( $self ) = @_;

    my $login = $self->get_mx_login() or croak("Error: mx_login not set.");
    my $title = $self->get_mx_title() or croak("Error: mx_title not set.");

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    # Retrieve data from the table.
    my $sth = $dbh->prepare(<<'QUERY');
select distinct TSUBMIS_SYSUID from TSUBMIS where TSUBMIS_LOGIN=?
and TSUBMIS_SUB_DESCR=? and TSUBMIS_DEL_STATUS='U'
QUERY
    $sth->execute( $login, $title ) or croak($sth->errstr);

    # Get all the TSUBMIS_UID values returned as an array of arrays.
    # (arrayref=[\@row1 \@row2 \@row3] where e.g. @row1=($col1,$col2))
    my $arrayref = $sth->fetchall_arrayref;

    # Count up the @rows.
    my $rowcount = scalar @$arrayref;

    # Check that we have one and only one row returned, assign the first
    # and only column value (TSUBMIS_SYSUID) to $tsubmis_sysuid.
    my $tsubmis_sysuid;
    if ( $rowcount == 1 ) {    # all is well
        $tsubmis_sysuid = $arrayref->[0][0];

    }
    elsif ( $rowcount == 0 ) {

        # Possible typo in login/title arguments
        croak(
            "Error: Login name \'$login\' with experiment title \'$title\' not found in MIAMExpress database.\n"
        );
    }
    else {

        # This should never happen
        croak(
            "ERROR: Login name \'$login\' with Experiment title \'$title\' has multiple MIAMExpress database entries.\n"
        );
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # Get the experiment filebase, while we have a connection.
    $sth = $dbh->prepare(<<'QUERY');
select distinct TSYSOPT_VALUE from TSYSOPT
where TSYSOPT_CODE='experiment_datafiles_path'
QUERY

    $sth->execute() or die($sth->errstr);

    # FIXME we're assuming the query worked here.
    my $MX_submissions_filebase = File::Spec->catdir(
	$sth->fetchrow_arrayref()->[0], $login, "submission$tsubmis_sysuid",
    );

    return ($tsubmis_sysuid, $MX_submissions_filebase);
}
    
sub get_db_files {
    my ( $self ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    # Retrieve data from the table.

    my $FGEM_ardesin;

    # Get Raw data filenames associated directly with hybs
    my $sth = $dbh->prepare(<<'QUERY');
select distinct TARRAY_DESIGNID,THYBRID_SYSUID, THYBRID_ID, TFILE_PATH
from TFILE,THYBRID,TLABHYB,TARRAY
where TLABHYB_HYBRIDID=THYBRID_SYSUID and TFILE_HYBRIDID=THYBRID_SYSUID
and THYBRID_ARRAYID=TARRAY_SYSUID and TLABHYB_SUBID=?
and TLABHYB_DEL_STATUS='U' and THYBRID_DEL_STATUS='U'
and TFILE_DEL_STATUS='U' and TARRAY_DEL_STATUS='U'
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    # Get the relevant stuff out of $sth.
    # Array of file hashes of attributes:
    my @file_list;

    # Hash of hyb_sysuids pointing to {raw} or {normalized} arrays of
    # filenames:
    my %hyb_list;

    # Array of FGEM filenames:
    my @FGEM_list;

    while ( my $rowref = $sth->fetchrow_hashref ) {

	my $path = File::Spec->catfile(
	    $self->get_submission_filebase(),
	    "hybrid$rowref->{'THYBRID_SYSUID'}",
	    $rowref->{'TFILE_PATH'}
	);

	# If we can, map the design ID to an accession.
	my $design_id = $rowref->{'TARRAY_DESIGNID'};
        if ( $design_id < 0 ) {
	    my $result = get_ae_array_details({database_id => -$design_id});
	    $design_id = $result if $result;
	}

        my $file = ArrayExpress::Datafile->new({
	    path            => $path,
	    name            => $rowref->{'TFILE_PATH'},
	    data_type       => 'raw',
	    hyb_identifier  => $rowref->{'THYBRID_ID'},
	    hyb_sysuid      => $rowref->{'THYBRID_SYSUID'},
	    array_design_id => $design_id,
	    is_miamexpress  => 1,
	});

        push( @file_list, $file );
        push(
            @{ $hyb_list{ $rowref->{'THYBRID_SYSUID'} }{raw} },
            $file->get_path()
        );
        $FGEM_ardesin ||= $design_id;
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # Get the normalized data file names
    $sth = $dbh->prepare(<<'QUERY');
select distinct TARRAY_DESIGNID,THYBRID_SYSUID,THYBRID_ID,TFILE_PATH
from TFILE,THYBRID,TLABHYB,TARRAY
where THYBRID_SYSUID=TLABHYB_HYBRIDID and THYBRID_SYSUID=TFILE_TRANSHYBRIDID
and TARRAY_SYSUID=THYBRID_ARRAYID and TLABHYB_SUBID=?
and TLABHYB_DEL_STATUS='U' and THYBRID_DEL_STATUS='U'
and TFILE_DEL_STATUS='U' and TARRAY_DEL_STATUS='U'
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    # get the relevant stuff out of $sth
    while ( my $rowref = $sth->fetchrow_hashref ) {

	my $path = File::Spec->catfile(
	    $self->get_submission_filebase(),
	    "hybrid$rowref->{'THYBRID_SYSUID'}",
	    $rowref->{'TFILE_PATH'}
        );

	# If we can, map the design ID to an accession.
	my $design_id = $rowref->{'TARRAY_DESIGNID'};
        if ( $design_id < 0 ) {
	    my $result = get_ae_array_details({database_id => -$design_id});
	    $design_id = $result if $result;
	}

        my $file = ArrayExpress::Datafile->new({
	    path            => $path,
	    name            => $rowref->{'TFILE_PATH'},
	    data_type       => 'normalized',
	    hyb_identifier  => $rowref->{'THYBRID_ID'},
	    hyb_sysuid      => $rowref->{'THYBRID_SYSUID'},
	    array_design_id => $design_id,
	    is_miamexpress  => 1,
	});

        push( @file_list, $file );
        push(
            @{ $hyb_list{ $rowref->{'THYBRID_SYSUID'} }{normalized} },
            $file->get_path()
        );
        $FGEM_ardesin ||= $design_id;
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # Deal with FGEM files here
    $sth = $dbh->prepare(<<'QUERY');
select distinct TFILE_PATH from TFILE,TEXPRFNL
where TFILE_EXPRFNLID=TEXPRFNL_SYSUID and TEXPRFNL_SUBID=?
and TFILE_DEL_STATUS='U'
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    while ( my $rowref = $sth->fetchrow_hashref ) {

	my $path = File::Spec->catfile(
	    $self->get_submission_filebase(),
	    $rowref->{'TFILE_PATH'},
	);
	
        # array_design_id here is a placeholder; MX does not associate
        # FGEM with ARDESIN. We default to the first raw data ARDESIN
        # here.
        my $file = ArrayExpress::Datafile->new({
	    path            => $path,
	    name            => $rowref->{'TFILE_PATH'},
	    data_type       => $CONFIG->get_FGEM_FILE_TYPE(),
	    is_miamexpress  => 1,
	    array_design_id => ( $FGEM_ardesin || 'UNKNOWN' ),
	});

        push( @file_list, $file );
        push( @FGEM_list, $file->get_path() );
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    return wantarray
	? ( \@file_list, \%hyb_list, \@FGEM_list )
	: \@file_list;

}

sub _get_db_expt_designs : PRIVATE {

    my ( $self ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $sth = $dbh->prepare(<<'QUERY');
select distinct TCTLVCB_DESCR from TCTLVCB, TEXPRTYP, TEXPRMNT
where TEXPRMNT_EXPRID=TEXPRTYP_EXPRID
and TEXPRTYP_ID=TCTLVCB_SYSUID
and TEXPRMNT_SUBID=?
and TCTLVCB_DEL_STATUS='U' and TEXPRMNT_DEL_STATUS='U' and TEXPRTYP_DEL_STATUS='U'
QUERY

    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    my @design_types;
    while ( my $termlist = $sth->fetchrow_arrayref() ) {
	push(@design_types, $termlist->[0]);
    }

    return \@design_types;
}

sub _get_hyb_ids : PRIVATE {

    # Returns a list of hyb ids and a second list of hyb sysuids
    # associated with the submission.

    my ( $self ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $sth = $dbh->prepare(<<'QUERY');
select distinct THYBRID_ID,THYBRID_SYSUID from THYBRID,TLABHYB
where THYBRID_SYSUID=TLABHYB_HYBRIDID and TLABHYB_SUBID=?
and TLABHYB_DEL_STATUS='U' and THYBRID_DEL_STATUS='U'
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    my %hyb_list;

    # get the relevant stuff out of $sth
    while ( my $rowref = $sth->fetchrow_hashref ) {
        my $hyb = $rowref->{'THYBRID_ID'};

        # hash, {hyb_id => hyb_sysuid}
        $hyb_list{$hyb} = $rowref->{'THYBRID_SYSUID'};
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    return ( \%hyb_list );
}

sub _protocol_length_and_others : PRIVATE {

    my ( $self, $obj_id_protocol_text, $get_ptext, $get_others )
        = @_;

    my %others;

    foreach my $obj_id ( keys %$obj_id_protocol_text ) {

        $get_ptext->execute( $self->get_tsubmis_sysuid(), $obj_id )
            or croak($get_ptext->errstr);

	# DBI row arrayref points at array with protocol text,
	# protocol sysuid.
        foreach my $rowref ( @{ $get_ptext->fetchall_arrayref } ) {

	    # Add the protocol text to the %protocol_typemap leaf for
	    # this object.
            push(
		@{ $obj_id_protocol_text->{$obj_id} },
		{
		    text => $rowref->[0],
		    id   => $rowref->[1],
		    name => $rowref->[2],
		    glob => $rowref->[3],
		},
	     );

	    # Use the protocol sysuid to pull out OTHERS info, while
	    # we're here.
            my $tprotcls_sysuid = $rowref->[1];
            $get_others->execute($tprotcls_sysuid)
                or croak($get_others->errstr);
            while ( my $rowref = $get_others->fetchrow_hashref ) {
                $rowref->{'TOTHERS_ID'} =~ s/_/ /g;
                $others{$tprotcls_sysuid}{ lc( $rowref->{'TOTHERS_ID'} ) }
                    = $rowref->{'TOTHERS_VALUE'};
            }
        }
    }

    return \%others;

}

sub _get_db_protocols : PRIVATE {

    my ( $self, $protocol_typemap ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $protocol_others;

    foreach my $type ( keys %$protocol_typemap ) {

        my $get_ptext;

        PROTOCOLTYPE:
        {

            ( $type eq 'transformation' ) && do {
                $get_ptext = $dbh->prepare(<<'QUERY');
select distinct TPROTCLS_DESCR, TPROTCLS_SYSUID, TPROTCLS_ID, TPROTCLS_GLOBID
from TPROTCLS, TEXPRFNL, TFILE
where TEXPRFNL_PROTOCOLID = TPROTCLS_SYSUID
and TFILE_EXPRFNLID = TEXPRFNL_SYSUID
and TEXPRFNL_SUBID=? and TFILE_PATH=?
and (TPROTCLS_GLOBID is not NULL or (TPROTCLS_DESCR is not NULL and TPROTCLS_ID is not NULL))
and TEXPRFNL_DEL_STATUS='U' and TPROTCLS_DEL_STATUS='U' and TFILE_DEL_STATUS='U'
QUERY
                last PROTOCOLTYPE;
            };

            ( $type eq 'normalization' ) && do {
                $get_ptext = $dbh->prepare(<<'QUERY');
select distinct TPROTCLS_DESCR, TPROTCLS_SYSUID, TPROTCLS_ID, TPROTCLS_GLOBID
from THYBRID,TLABHYB,TPROTCLS
where THYBRID_SYSUID=TLABHYB_HYBRIDID and THYBRID_NORM_PROTOCOLID = TPROTCLS_SYSUID
and TLABHYB_SUBID=? and THYBRID_ID=?
and (TPROTCLS_GLOBID is not NULL or (TPROTCLS_DESCR is not NULL and TPROTCLS_ID is not NULL))
and TLABHYB_DEL_STATUS='U' and THYBRID_DEL_STATUS='U' and TPROTCLS_DEL_STATUS='U'
QUERY
                last PROTOCOLTYPE;
            };

            ( $type eq 'hybridization' ) && do {
                $get_ptext = $dbh->prepare(<<'QUERY');
select distinct TPROTCLS_DESCR, TPROTCLS_SYSUID, TPROTCLS_ID, TPROTCLS_GLOBID
from THYBRID,TLABHYB,TPROTCLS
where THYBRID_SYSUID=TLABHYB_HYBRIDID and THYBRID_PROTOCOLID = TPROTCLS_SYSUID
and TLABHYB_SUBID=? and THYBRID_ID=?
and (TPROTCLS_GLOBID is not NULL or (TPROTCLS_DESCR is not NULL and TPROTCLS_ID is not NULL))
and TLABHYB_DEL_STATUS='U' and THYBRID_DEL_STATUS='U' and TPROTCLS_DEL_STATUS='U'
QUERY
                last PROTOCOLTYPE;
            };

            ( $type eq 'scanning' ) && do {
                $get_ptext = $dbh->prepare(<<'QUERY');
select distinct TPROTCLS_DESCR, TPROTCLS_SYSUID, TPROTCLS_ID, TPROTCLS_GLOBID
from THYBRID,TLABHYB,TPROTCLS
where THYBRID_SYSUID=TLABHYB_HYBRIDID and THYBRID_SCAN_PROTOCOLID = TPROTCLS_SYSUID
and TLABHYB_SUBID=? and THYBRID_ID=?
and (TPROTCLS_GLOBID is not NULL or (TPROTCLS_DESCR is not NULL and TPROTCLS_ID is not NULL))
and TLABHYB_DEL_STATUS='U' and THYBRID_DEL_STATUS='U' and TPROTCLS_DEL_STATUS='U'
QUERY
                last PROTOCOLTYPE;
            };

            ( $type eq 'labeling' ) && do {
                $get_ptext = $dbh->prepare(<<'QUERY');
select distinct TPROTCLS_DESCR, TPROTCLS_SYSUID, TPROTCLS_ID, TPROTCLS_GLOBID
from TLABEL,TEXTRACT,TPOOLED,TPROTCLS
where TLABEL_PROTOCOLID=TPROTCLS_SYSUID and TEXTRACT_SYSUID=TLABEL_EXTRACTID 
and TEXTRACT_SYSUID=TPOOLED_EXTRACTID and TPOOLED_SUBID=?
and TLABEL_ID=? and TLABEL_DEL_STATUS='U' and TEXTRACT_DEL_STATUS='U'
and (TPROTCLS_GLOBID is not NULL or (TPROTCLS_DESCR is not NULL and TPROTCLS_ID is not NULL))
and TPOOLED_DEL_STATUS='U' and TPROTCLS_DEL_STATUS='U'
QUERY
                last PROTOCOLTYPE;
            };

            ( $type eq 'extraction' ) && do {
                $get_ptext = $dbh->prepare(<<'QUERY');
select distinct TPROTCLS_DESCR, TPROTCLS_SYSUID, TPROTCLS_ID, TPROTCLS_GLOBID
from TEXTRACT,TPOOLED,TPROTCLS
where TEXTRACT_PROTOCOLID=TPROTCLS_SYSUID and TEXTRACT_SYSUID=TPOOLED_EXTRACTID
and TPOOLED_SUBID=? and TEXTRACT_ID=?
and (TPROTCLS_GLOBID is not NULL or (TPROTCLS_DESCR is not NULL and TPROTCLS_ID is not NULL))
and TEXTRACT_DEL_STATUS='U' and TPOOLED_DEL_STATUS='U' and TPROTCLS_DEL_STATUS='U'
QUERY
                last PROTOCOLTYPE;
            };

            ( $type eq 'pooling' ) && do {
                $get_ptext = $dbh->prepare(<<'QUERY');
select distinct TPROTCLS_DESCR, TPROTCLS_SYSUID, TPROTCLS_ID, TPROTCLS_GLOBID
from TEXTRACT,TPOOLED,TPROTCLS
where TEXTRACT_POOL_PROTOCOLID=TPROTCLS_SYSUID and TEXTRACT_SYSUID=TPOOLED_EXTRACTID
and TPOOLED_SUBID=? and TEXTRACT_ID=?
and (TPROTCLS_GLOBID is not NULL or (TPROTCLS_DESCR is not NULL and TPROTCLS_ID is not NULL))
and TEXTRACT_DEL_STATUS='U' and TPOOLED_DEL_STATUS='U' and TPROTCLS_DEL_STATUS='U'
QUERY
                last PROTOCOLTYPE;
            };

            ( $type eq 'treatment' ) && do {
                $get_ptext = $dbh->prepare(<<'QUERY');
select distinct TPROTCLS_DESCR, TPROTCLS_SYSUID, TPROTCLS_ID, TPROTCLS_GLOBID
from TPROTCLS,TSAMPLE,TEXPRMNT
where TSAMPLE_EXPRID=TEXPRMNT_EXPRID and TSAMPLE_PROTOCOLID=TPROTCLS_SYSUID
and TEXPRMNT_SUBID=? and TSAMPLE_ID=?
and (TPROTCLS_GLOBID is not NULL or (TPROTCLS_DESCR is not NULL and TPROTCLS_ID is not NULL))
and TPROTCLS_DEL_STATUS='U' and TSAMPLE_DEL_STATUS='U' and TEXPRMNT_DEL_STATUS='U'
QUERY
                last PROTOCOLTYPE;
            };

            ( $type eq 'growth' ) && do {
                $get_ptext = $dbh->prepare(<<'QUERY');
select distinct TPROTCLS_DESCR, TPROTCLS_SYSUID, TPROTCLS_ID, TPROTCLS_GLOBID
from TPROTCLS,TSAMPLE,TEXPRMNT
where TSAMPLE_EXPRID=TEXPRMNT_EXPRID and TSAMPLE_GROWTH_PROTOCOLID=TPROTCLS_SYSUID
and TEXPRMNT_SUBID=? and TSAMPLE_ID=?
and (TPROTCLS_GLOBID is not NULL or (TPROTCLS_DESCR is not NULL and TPROTCLS_ID is not NULL))
and TPROTCLS_DEL_STATUS='U' and TSAMPLE_DEL_STATUS='U' and TEXPRMNT_DEL_STATUS='U'
QUERY
                last PROTOCOLTYPE;
            };

            croak("INTERNAL SCRIPT ERROR: Unknown protocol type.\n");

        }

        my $get_others = $dbh->prepare(<<'QUERY');
select distinct TOTHERS_ID, TOTHERS_VALUE from TOTHERS, TPROTCLS
where TPROTCLS_SYSUID=TOTHERS_DETAIL_PRTCLID and TPROTCLS_SYSUID=?
QUERY
        my $others = $self->_protocol_length_and_others(
	    $protocol_typemap->{$type},
	    $get_ptext,
	    $get_others,
	);

        $protocol_others->{$type} = $others;

        $get_ptext->finish();
        $get_others->finish();

    }

    return $protocol_others;

}

sub _tree_to_table : PRIVATE {

    # Converts a hash heirarchy (key1 => {key2 => {key3 => 0}}, ...) into an
    # array of arrays ([key1, key2, key3],[key1, key4, ...], ...); used to
    # reverse a hash heirarchy.

    my ( $self, $hash ) = @_;
    my $AoA;

    foreach my $key ( keys %$hash ) {
        if ( ref( $hash->{$key} ) eq 'HASH' ) {
            my $lower_AoA = $self->_tree_to_table( $hash->{$key} );
            foreach my $row (@$lower_AoA) {
                push( @$AoA, [ $key, @$row ] );
            }
        }
        else {
            push @$AoA, [$key];
        }

    }
    return $AoA;
}

sub _invert_heirarchy : PRIVATE {

    # Takes a hashref, inverts the heirarchy using an array of arrays,
    # and returns a rebuilt inverted heirarchical hash.

    my ( $self, $hash ) = @_;

    my $AoA = $self->_tree_to_table($hash);

    my $newhash;
    foreach my $row (@$AoA) {
        $newhash = $self->_build_heirarchy( $newhash, reverse @$row );
    }

    return $newhash;
}

sub _build_heirarchy : PRIVATE {

    # Recurse through a passed list of terms, create a Tie::IxHash at
    # every level.

    # We tie these hashes to maintain database order (i.e. ordered by
    # SYSUID). This order is then reflected in the biomaterials reports,
    # which is extremely useful when hacking any output MAGE-ML.

    my ( $self, $hash, @termlist ) = @_;

    my $newterm = shift @termlist;

    unless ($hash) {
        tie( my %newhash, 'Tie::IxHash' );
        $hash = \%newhash;
    }

    ref $hash eq 'HASH' or croak;    # Just in case

    if ( scalar( @termlist ) ) {
        unless ( $hash->{$newterm} ) {
            tie( my %newhash, 'Tie::IxHash' );
            $hash->{$newterm} = \%newhash;
        }
        $self->_build_heirarchy( $hash->{$newterm}, @termlist );
    }
    else {
        $hash->{$newterm} = 0;
    }

    return $hash;
}

sub _mx_tsample_tntxsyn_name : PRIVATE {

    # Get the TNTXSYN_NAME_TXT value attached to a given sample.

    my ( $self, $tsample_sysuid ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $sth = $dbh->prepare(<<"QUERY");
select distinct TNTXSYN_NAME_TXT
from TSAMPLE, TNTXSYN
where TSAMPLE_SYSUID=?
and TSAMPLE_TAXID=TNTXSYN_TAX_ID
and TNTXSYN_TAX_ID!='0'
and TNTXSYN_NAME_TXT!='NULL'
and TSAMPLE_DEL_STATUS='U'
QUERY
    $sth->execute( $tsample_sysuid )
        or croak($sth->errstr);
    my $values = $sth->fetchall_arrayref;
    $sth->finish();

    return $values;
}

sub _mx_tsample_column_value : PRIVATE {

    # Get the actual column value from a TSAMPLE table column.

    my ( $self, $column, $tsample_sysuid ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $sth = $dbh->prepare(<<"QUERY");
select distinct $column
from TSAMPLE
where TSAMPLE_SYSUID=?
and $column!='NULL'
and TSAMPLE_DEL_STATUS='U'
QUERY
    $sth->execute( $tsample_sysuid )
        or croak($sth->errstr);
    my $values = $sth->fetchall_arrayref;
    $sth->finish();

    return $values;
}

sub _mx_tsample_tctlvcb_descr : PRIVATE {

    # Get the value in TCTLVCB mapped to by the id in a TSAMPLE table
    # column.

    my ( $self, $sql_column, $tsample_sysuid ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $sth = $dbh->prepare(<<"QUERY");
select distinct TCTLVCB_DESCR
from TSAMPLE, TCTLVCB
where TSAMPLE_SYSUID=?
and $sql_column=TCTLVCB_SYSUID
and TCTLVCB_SYSUID!='0'
and TCTLVCB_DESCR!='NULL'
and TSAMPLE_DEL_STATUS='U' and TCTLVCB_DEL_STATUS='U'
QUERY
    $sth->execute( $tsample_sysuid )
        or croak($sth->errstr);
    my $values = $sth->fetchall_arrayref;
    $sth->finish();

    return $values;
}

sub _mx_tsample_tothers_value : PRIVATE {

    # Get the value in TOTHERS mapped to by the id in a TSAMPLE table
    # column.

    my ( $self, $mapped_other, $tsample_sysuid ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $sth = $dbh->prepare(<<'QUERY');
select distinct TOTHERS_VALUE
from TOTHERS, TSAMPLE
where TOTHERS_SAMPLEID=?
and TOTHERS_ID=?
and TOTHERS_VALUE!='NULL'
and TSAMPLE_DEL_STATUS='U' and TOTHERS_DEL_STATUS='U'
QUERY
    $sth->execute( $tsample_sysuid, $mapped_other )
        or croak($sth->errstr);
    my $value = [ "OTHER: " . $sth->fetchall_arrayref()->[0][0] ];
    $sth->finish();

    return $value;
}

sub _mx_tfctrval_text_and_unit : PRIVATE {

    # Get the details from TFCTRVAL pertaining to a particular
    # EXPFCTRID and linked to a given sample.

    my ( $self, $tfctrval_expfctrid, $tsample_sysuid ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $sth = $dbh->prepare(<<'QUERY');
select distinct TFCTRVAL_FREETEXT, TFCTRVAL_FREETEXTUNIT
from TFCTRVAL
where TFCTRVAL_SAMPLEID=? and TFCTRVAL_FREETEXT!='NULL'
and TFCTRVAL_EXPFCTRID=?
and TFCTRVAL_DEL_STATUS='U'
QUERY
    $sth->execute( $tsample_sysuid, $tfctrval_expfctrid )
        or croak($sth->errstr);
    my $values = $sth->fetchall_arrayref;
    $sth->finish();

    return $values;
}

sub _mx_tsample_annotation_values : PRIVATE {

    # Retrieve a TSAMPLE annotation value based on the passed SQL
    # column.

    my ( $self, $sql_column, $tsample_sysuid ) = @_;

    my $values;

    # Special cases first.
    if (grep {/^$sql_column$/i}
        qw(TSAMPLE_AGERANGE_MIN
        TSAMPLE_AGERANGE_MAX
        TSAMPLE_DISEASE_STATE
        TSAMPLE_CELL_LINE
        TSAMPLE_TARGET_CELL_TYPE
        TSAMPLE_INDIVIDUAL_GEN
        TSAMPLE_STRAIN
        TSAMPLE_CELL_PROVIDER
        TSAMPLE_INDIVIDUAL
        TSAMPLE_ADDITIONAL)
        ) {

        # AGERANGE columns just use the TSAMPLE table value.
        # TSAMPLE_INDIVIDUAL, TSAMPLE_ADDITIONAL and several
        # other columns (see above) also need this treatment.
        $values = $self->_mx_tsample_column_value(
	    $sql_column,
	    $tsample_sysuid,
	);

    }
    elsif ( $sql_column eq 'TSAMPLE_TAXID' ) {

        # Organism is a special case, maps to TNXTSYN table.
        $values = $self->_mx_tsample_tntxsyn_name($tsample_sysuid);

    }
    else {

        # Next, check TCTLVCB via TSAMPLE; for "other" from a
        # pulldown list add the value.
        $values
            = $self->_mx_tsample_tctlvcb_descr(
		$sql_column,
		$tsample_sysuid,
	    );

        # If returned value contains 'other', map to
        # TOTHERS. TSAMPLE_DEV_STAGE can be text, so we must allow
        # this check in that case also.
        foreach my $value (@$values) {

            # Substitute "other" values.
            if ( $value->[0] eq 'other' ) {
                my $mapped_other = $self->_map_samplesql_to_othertag(
		    $sql_column,
		);
                $value = $self->_mx_tsample_tothers_value(
		    $mapped_other,
		    $tsample_sysuid,
		);
            }

            # Handle the 'not applicable' pulldown menu item by nulling it.
            elsif ( $value->[0] eq 'not applicable' ) {
                $value = [];
            }
        }
        unless ( scalar(@$values) ) {

            # Fall back to the actual values in TSAMPLE.
            $values = $self->_mx_tsample_column_value(
		$sql_column,
		$tsample_sysuid,
	    );
        }
    }
    return $values;
}

sub _mx_tsample_annotation_report : PRIVATE {

    # Print out a report on annotation for a given sample using an
    # ExperimentChecker object logprint method.

    my ( $self, $tsample_sysuid ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $sth = $dbh->prepare(<<'QUERY');
select distinct TSAMPLE_ID
from TSAMPLE
where TSAMPLE_SYSUID=?
and TSAMPLE_DEL_STATUS='U'
QUERY

    $sth->execute( $tsample_sysuid )
        or croak($sth->errstr);

    # This should only ever return one value (check FIXME)
    my $tsample_id = $sth->fetchall_arrayref()->[0][0];
    $sth->finish();

    my @categories          = sort keys %CATEGORY_SQL_MAP;
    my $name_heading        = 'Sample Name';
    my $max_category_length = 0;
    foreach my $category ( @categories, $name_heading ) {
        my $length = length($category);
        $max_category_length = $length if ( $length > $max_category_length );
    }

    # Sample name (TSAMPLE_ID) heading.
    my $printable = sprintf( "%${max_category_length}s: %s\n",
        $name_heading, $tsample_id );
    $self->logprint( 'sample', $printable );

    foreach my $category (@categories) {
        my $factorstring
            = $self->_mx_tsample_annotation_string(
		$category,
		$tsample_sysuid,
	    );

        # Only print the fields containing meaningful information.
        if ( defined($factorstring) && ( $factorstring ne q{} ) ) {
            my $printable = sprintf( "%${max_category_length}s: %s\n",
                $category, $factorstring );
            $self->logprint( 'sample', $printable );

	    if (lc($category) eq 'organism') {
		$self->add_expt_organisms($factorstring);
	    }
        }
    }

    $self->logprint( 'sample', q{-} x $max_category_length, "-\n" );

    return;
}

sub _mx_tsample_annotation_string : PRIVATE {

    # For a given category in %CATEGORY_SQL_MAP, generate the
    # sample-specific string appropriate to that factor.

    my ( $self, $category, $tsample_sysuid ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my @factor_values;    # This will get stringified later

    # Get an arrayref of places to look for controlled vocabulary terms.
    my $mapped_columns = $self->_map_efcategorytag_to_samplesql($category);

    # Unless $mapped_columns is empty (get FREETEXT, FREETEXTUNIT),
    # check for match between each mapped column and TCTLVCB; fall
    # back to TOTHERS; then just use the TSAMPLE values.
    foreach my $sql_column (@$mapped_columns) {

        my $values
            = $self->_mx_tsample_annotation_values(
		$sql_column,
		$tsample_sysuid,
	    );

        # Dump the returned values into our array of FVs.
        if (@$values) {
            push( @factor_values, map { @{$_} } @$values );
        }
        else {

            # This is needed for correct ordering of complex results,
            # e.g. age.
            push( @factor_values, q{} );
        }
    }

    # Final fallback to TFCTRVAL_FREETEXT ("other" measurement FVs)
    unless ( scalar(@factor_values) ) {

        my $sth = $dbh->prepare(<<'QUERY');
select distinct TFCTRVAL_EXPFCTRID
from TFCTRVAL, TSAMPLE, TCTLVCB
where TFCTRVAL_SAMPLEID=TSAMPLE_SYSUID
and TFCTRVAL_EXPFCTRID=TCTLVCB_SYSUID
and TSAMPLE_SYSUID=?
and TCTLVCB_DESCR=?
and TFCTRVAL_DEL_STATUS='U' and TSAMPLE_DEL_STATUS='U'
and TCTLVCB_DEL_STATUS='U'
QUERY

        $sth->execute( $tsample_sysuid, $category )
            or croak($sth->errstr);

        # This should only ever return one value (check FIXME)
        my $tfctrval_expfctrid = $sth->fetchall_arrayref()->[0][0];
        $sth->finish();

        my $values = $self->_mx_tfctrval_text_and_unit(
	    $tfctrval_expfctrid,
	    $tsample_sysuid,
	);

        if (@$values) {

            # Create a FREETEXT FREETEXTUNIT string (omit
            # FREETEXTUNIT if undef).
            push( @factor_values,
                map { $_->[0] . ( defined( $_->[1] ) ? " $_->[1]" : q{} ) }
                    @$values );
        }
    }

    # Build a human-readable string from the factorvalue result.
    my $factorstring;
    if ( scalar @factor_values ) {
        if ( $category eq 'age' ) {    # Age is a special case
            $factorstring = $self->_mx_factorvalue_agestring(\@factor_values);
        }
        else {
            $factorstring = join( ', ', @factor_values );
        }
    }
    return $factorstring;
}

sub _get_db_factor_assns : PRIVATE {

    # Get all the factorvalues for an experiment, return them in a
    # simple {hyb=>fv} heirarchical hashref.

    my ( $self ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    # Get all the categories. This is big because we want to map from
    # sample to hyb.
    my $sth = $dbh->prepare(<<'QUERY');
select distinct THYBRID_ID, TSAMPLE_SYSUID, TCTLVCB_DESCR, TFCTRVAL_EXPFCTRID
from THYBRID,TLABHYB,TLABEL,TEXTRACT,TSAMPLE,TPOOLED,TFCTRVAL,TCTLVCB
where THYBRID_SYSUID=TLABHYB_HYBRIDID and TLABEL_SYSUID=TLABHYB_LABELID
and TEXTRACT_SYSUID=TLABEL_EXTRACTID and TSAMPLE_SYSUID=TPOOLED_SAMPLEID
and TEXTRACT_SYSUID=TPOOLED_EXTRACTID and TFCTRVAL_SAMPLEID=TSAMPLE_SYSUID
and TFCTRVAL_EXPFCTRID=TCTLVCB_SYSUID
and TLABHYB_SUBID=?
and TLABHYB_DEL_STATUS='U' and THYBRID_DEL_STATUS='U' and TLABEL_DEL_STATUS='U'
and TEXTRACT_DEL_STATUS='U' and TSAMPLE_DEL_STATUS='U' and TPOOLED_DEL_STATUS='U'
and TFCTRVAL_DEL_STATUS='U' and TCTLVCB_DEL_STATUS='U'
order by TCTLVCB_DESCR, THYBRID_ID, TSAMPLE_SYSUID
QUERY

    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    # Array of hashrefs, one per experimental factor category.
    my @retrieved_factors;
    while ( my $rowref = $sth->fetchrow_hashref ) {
        push @retrieved_factors, $rowref;
    }
    $sth->finish();    # Overlapping prepares not good for portability

    my ($hyb_factors, %hyb_category_fvs);

    foreach my $factor (@retrieved_factors) {

        my $factorstring =
          $self->_mx_tsample_annotation_string(
	      $factor->{'TCTLVCB_DESCR'},
	      $factor->{'TSAMPLE_SYSUID'},
	  );

	# Construct the category-FV hash for mapping to files later. 
        $hyb_category_fvs{ $factor->{'THYBRID_ID'} }
          { $factor->{'TCTLVCB_DESCR'} }{$factorstring}++;

        $factorstring .= " ($factor->{'TCTLVCB_DESCR'})";

        # Add the factor string to the $hyb_factors heirarchy.
        $hyb_factors = $self->_build_heirarchy(
	    $hyb_factors,
	    $factor->{'THYBRID_ID'},
            $factorstring,
	);
    }
    return ($hyb_factors, \%hyb_category_fvs);
}

sub _mx_factorvalue_agestring : PRIVATE {

    # Sub takes an age fv arrayref as defined by
    # _map_efcategorytag_to_samplesql, and formats it nicely for
    # printing.

    my ( $self, $factor_values ) = @_;

    # Sort out the age (and optional range) to start with.
    my $factorstring = $factor_values->[0];
    $factorstring .= "-$factor_values->[1]"
        if $factor_values->[1];

    # Don't bother with units if there's no
    # age or age range.
    if ($factorstring) {
        $factorstring .= " $factor_values->[2]"
            if $factor_values->[2];
    }

    # We keep the initial time point regardless, as it may indicate a
    # submitter error.
    $factorstring .= " since $factor_values->[3]"
        if $factor_values->[3];

    return $factorstring;
}

sub _map_efcategorytag_to_samplesql : PRIVATE {

    my ( $self, $category ) = @_;

    # Empty arrayrefs typically signify that these are 'other' FVs, held
    # in TFCTRVAL_FREETEXT and TFCTRVAL_FREETEXTUNIT. This is an
    # egregious hack made necessary by the MIAMExpress schema.
    unless ( $CATEGORY_SQL_MAP{$category} ) {
        croak(
            qq{ERROR: Unmapped experimental factor category "$category".\n});
    }

    return $CATEGORY_SQL_MAP{$category};
}

sub _map_samplesql_to_othertag : PRIVATE {

    my ( $self, $sql_colname ) = @_;

    # These map the TCTLVCB-linked TSAMPLE columns which also have
    # "other" as an option to the relevant TOTHERS_ID. This is an
    # egregious hack made necessary by the MIAMExpress schema.
    my %mapping = (
        TSAMPLE_SAMPLE_TYPE       => 'SAMPLE_TYPE',
        TSAMPLE_DEV_STAGE         => 'DEVELOPMENTAL_STAGE',
        TSAMPLE_TIME_POINT        => 'AGE_TIME_POINTS',
        TSAMPLE_ORGANISM_PART     => 'ORGANISM_PART',
        TSAMPLE_SEX               => 'SEX',
        TSAMPLE_GENETIC_VARIATION => 'GENETIC_VARIATION',
        TSAMPLE_TARGET_CELL_TYPE  => 'CELL_TYPE',
        TSAMPLE_TREATMENT_TYPE    => 'TREATMENT_TYPE',
        TSAMPLE_SEPARATION_TECH   => 'SEPARATION_TECHNIQUE',
    );

    unless ( $mapping{$sql_colname} ) {
        croak(qq{ERROR: Unmapped TSAMPLE SQL column name "$sql_colname".\n});
    }

    return $mapping{$sql_colname};
}

sub _get_db_objects : PRIVATE {

    # Gets samples, extracts, labelled extracts, hybs, and returns
    # hashes describing how they map to each other. See
    # &heirarchy_report for how these are used.

    my ( $self ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my ( $samples_to_hybs, %protocol_typemap );

    my $error = 0;

    # Get a table with sample, extract, labeled extract and hyb
    # columns. We retrieve SYSUIDs as well, to guard against duplicate
    # names. SYSUIDs are also used to sort the tied hashes.
    my $sth = $dbh->prepare(<<'QUERY');
select distinct TSAMPLE_ID,TEXTRACT_ID,TLABEL_ID,THYBRID_ID,TSAMPLE_SYSUID,TEXTRACT_SYSUID,TLABEL_SYSUID,THYBRID_SYSUID
from THYBRID,TLABHYB,TLABEL,TEXTRACT,TSAMPLE,TPOOLED
where THYBRID_SYSUID=TLABHYB_HYBRIDID and TLABEL_SYSUID=TLABHYB_LABELID
and TEXTRACT_SYSUID=TLABEL_EXTRACTID and TSAMPLE_SYSUID=TPOOLED_SAMPLEID
and TEXTRACT_SYSUID=TPOOLED_EXTRACTID and TLABHYB_SUBID=?
and TLABHYB_DEL_STATUS='U' and THYBRID_DEL_STATUS='U' and TLABEL_DEL_STATUS='U'
and TEXTRACT_DEL_STATUS='U' and TSAMPLE_DEL_STATUS='U' and TPOOLED_DEL_STATUS='U'
order by TSAMPLE_SYSUID, TEXTRACT_SYSUID, TLABEL_SYSUID, THYBRID_SYSUID
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    while ( my $rowref = $sth->fetchrow_hashref ) {

        # Use of THYBRID_ID instead of THYBRID_SYSUID here is a bug
        # FIXME. See also below for other protocol types. Note that a
        # check should now have been added to the MIAMExpress codebase to
        # prevent this bug ever occurring.
        $protocol_typemap{hybridization}{ $rowref->{'THYBRID_ID'} } = [];
        $protocol_typemap{normalization}{ $rowref->{'THYBRID_ID'} } = [];
        $protocol_typemap{scanning}{ $rowref->{'THYBRID_ID'} }      = [];

        my @terms = (
            $rowref->{'TSAMPLE_ID'}, $rowref->{'TEXTRACT_ID'},
            $rowref->{'TLABEL_ID'},  $rowref->{'THYBRID_ID'}
        );
        $samples_to_hybs = $self->_build_heirarchy(
	    $samples_to_hybs,
	    @terms,
	);
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # We need to check for unused labels, extracts, samples here.
    # Set up our nulls:
    my $null = '**{NULL}**';

    # Labeled Extract check
    $sth = $dbh->prepare(<<'QUERY');
select distinct TSAMPLE_ID,TEXTRACT_ID,TLABEL_ID,TSAMPLE_SYSUID,TEXTRACT_SYSUID,TLABEL_SYSUID
from TLABEL,TEXTRACT,TSAMPLE,TPOOLED
where TEXTRACT_SYSUID=TLABEL_EXTRACTID and TSAMPLE_SYSUID=TPOOLED_SAMPLEID
and TEXTRACT_SYSUID=TPOOLED_EXTRACTID and TPOOLED_SUBID=?
and TLABEL_DEL_STATUS='U' and TEXTRACT_DEL_STATUS='U'
and TPOOLED_DEL_STATUS='U' and TSAMPLE_DEL_STATUS='U'
order by TSAMPLE_SYSUID, TEXTRACT_SYSUID, TLABEL_SYSUID
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    QUERY_RESULT:
    while ( my $rowref = $sth->fetchrow_hashref ) {

        $protocol_typemap{labeling}{ $rowref->{'TLABEL_ID'} } = [];

        next QUERY_RESULT
            if $samples_to_hybs->{ $rowref->{'TSAMPLE_ID'} }
            { $rowref->{'TEXTRACT_ID'} }{ $rowref->{'TLABEL_ID'} };

        my @terms = (
            $rowref->{'TSAMPLE_ID'},
            $rowref->{'TEXTRACT_ID'},
            $rowref->{'TLABEL_ID'}, $null,
        );
        $samples_to_hybs = $self->_build_heirarchy(
	    $samples_to_hybs,
	    @terms,
	);
        $error |= $CONFIG->get_ERROR_PARSEFAIL();
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # Extract check
    $sth = $dbh->prepare(<<'QUERY');
select distinct TSAMPLE_ID,TEXTRACT_ID,TSAMPLE_SYSUID,TEXTRACT_SYSUID
from TEXTRACT,TSAMPLE,TPOOLED
where TSAMPLE_SYSUID=TPOOLED_SAMPLEID
and TEXTRACT_SYSUID=TPOOLED_EXTRACTID and TPOOLED_SUBID=?
and TEXTRACT_DEL_STATUS='U' and TPOOLED_DEL_STATUS='U' and TSAMPLE_DEL_STATUS='U'
order by TSAMPLE_SYSUID, TEXTRACT_SYSUID
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    QUERY_RESULT:
    while ( my $rowref = $sth->fetchrow_hashref ) {

        $protocol_typemap{extraction}{ $rowref->{'TEXTRACT_ID'} } = [];
        $protocol_typemap{pooling}{ $rowref->{'TEXTRACT_ID'} }    = [];

        next QUERY_RESULT
            if $samples_to_hybs->{ $rowref->{'TSAMPLE_ID'} }
            { $rowref->{'TEXTRACT_ID'} };

        my @terms = (
            $rowref->{'TSAMPLE_ID'},
            $rowref->{'TEXTRACT_ID'},
            $null, $null,
        );
        $samples_to_hybs = $self->_build_heirarchy(
	    $samples_to_hybs,
	    @terms,
	);
        $error |= $CONFIG->get_ERROR_PARSEFAIL();
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # Sample check (we have to go in though TEXPRMNT here)
    $sth = $dbh->prepare(<<'QUERY');
select distinct TSAMPLE_ID,TSAMPLE_SYSUID
from TSAMPLE,TEXPRMNT
where TSAMPLE_EXPRID=TEXPRMNT_EXPRID
and TEXPRMNT_SUBID=?
and TSAMPLE_DEL_STATUS='U' and TEXPRMNT_DEL_STATUS='U'
order by TSAMPLE_SYSUID
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    QUERY_RESULT:
    while ( my $rowref = $sth->fetchrow_hashref ) {

        $protocol_typemap{treatment}{ $rowref->{'TSAMPLE_ID'} } = [];
        $protocol_typemap{growth}{ $rowref->{'TSAMPLE_ID'} }    = [];

        next QUERY_RESULT
            if $samples_to_hybs->{ $rowref->{'TSAMPLE_ID'} };

        my @terms = ( $rowref->{'TSAMPLE_ID'}, $null, $null, $null, );
        $samples_to_hybs = $self->_build_heirarchy(
	    $samples_to_hybs,
	    @terms,
	);
        $error |= $CONFIG->get_ERROR_PARSEFAIL();
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # Deal with FGEM files here
    $sth = $dbh->prepare(<<'QUERY');
select distinct TFILE_PATH from TFILE,TEXPRFNL
where TFILE_EXPRFNLID=TEXPRFNL_SYSUID and TEXPRFNL_SUBID=?
and TFILE_DEL_STATUS='U'
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);
    while ( my $rowref = $sth->fetchrow_hashref ) {
        $protocol_typemap{transformation}{ $rowref->{'TFILE_PATH'} } = [];
    }
    $sth->finish();
    
    # Further populate our protocol hash
    my $protocol_others = $self->_get_db_protocols( \%protocol_typemap );

    # Return HoHoHoH for the associations between sample, extract,
    # labeled extract and hyb (in both directions).
    return ( $samples_to_hybs, \%protocol_typemap, $protocol_others, $error );
}

sub _get_db_expr_ids : PRIVATE {

    my ( $self ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    # Use a hash to get a uniqued list of expr_ids
    my %expr_ids;

    my $sth = $dbh->prepare(<<'QUERY');
select distinct TEXPRMNT_EXPRID
from TEXPRMNT
where TEXPRMNT_SUBID=?
and TEXPRMNT_DEL_STATUS='U'
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    while ( my $rowref = $sth->fetchrow_hashref ) {
        $expr_ids{ $rowref->{'TEXPRMNT_EXPRID'} }++;
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    return \%expr_ids;

}

sub _get_db_sample_annotation : PRIVATE {

    my ( $self ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    # Find all the TSAMPLE_SYSUIDs for the experiment.
    my $sth = $dbh->prepare(<<'QUERY');
select distinct TSAMPLE_SYSUID
from TSAMPLE,TEXPRMNT
where TSAMPLE_EXPRID=TEXPRMNT_EXPRID
and TEXPRMNT_SUBID=?
and TSAMPLE_DEL_STATUS='U'
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    my @sample_sysuids;
    while ( my $sample_ref = $sth->fetchrow_arrayref() ) {
        push( @sample_sysuids, @$sample_ref );
    }
    $sth->finish();

    # Print a report on each sample.
    foreach my $tsample_sysuid (@sample_sysuids) {
        $self->_mx_tsample_annotation_report($tsample_sysuid);
    }

    return;
}

sub _get_db_others : PRIVATE {

    my ( $self ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my @others;    #AoH

    my $expr_ids = $self->_get_db_expr_ids();

    # Top-level experiment OTHERSs
    my $sth = $dbh->prepare(<<'QUERY');
select distinct TOTHERS_ID,TOTHERS_VALUE,TEXPRMNT_EXPRID
from TOTHERS, TEXPRMNT, TEXPRQC
where TEXPRQC_EXPRID=TOTHERS_EXPRID
and TOTHERS_EXPRID=TEXPRMNT_EXPRID
and TOTHERS_DEL_STATUS='U'
and TEXPRQC_DEL_STATUS='U'
and TEXPRMNT_DEL_STATUS='U'
and TEXPRMNT_SUBID=?
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    while ( my $rowref = $sth->fetchrow_hashref ) {
        $rowref->{'TOTHERS_ID'} =~ s/_/ /g;
        push(
            @others,
            {   id      => lc( $rowref->{'TOTHERS_ID'} ),
                value   => $rowref->{'TOTHERS_VALUE'},
                expr_id => $rowref->{'TEXPRMNT_EXPRID'},
            }
        );
    }

    # If there are no QC others, we need to try again to get top-level others
    unless (@others) {
        $sth = $dbh->prepare(<<'QUERY');
select distinct TOTHERS_ID,TOTHERS_VALUE,TEXPRMNT_EXPRID
from TOTHERS, TEXPRMNT
where TOTHERS_EXPRID=TEXPRMNT_EXPRID
and TOTHERS_DEL_STATUS='U'
and TEXPRMNT_DEL_STATUS='U'
and TEXPRMNT_SUBID=?
QUERY
        $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

        while ( my $rowref = $sth->fetchrow_hashref ) {
            $rowref->{'TOTHERS_ID'} =~ s/_/ /g;
            push(
                @others,
                {   id      => lc( $rowref->{'TOTHERS_ID'} ),
                    value   => $rowref->{'TOTHERS_VALUE'},
                    expr_id => $rowref->{'TEXPRMNT_EXPRID'},
                }
            );
        }
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # Get the OTHERs for samples associated with the experiment(s) - of
    # course, there should be only one experiment, but we're making this
    # extensible (maybe).
    foreach my $expr_id ( keys %$expr_ids ) {

        my $sth = $dbh->prepare(<<'QUERY');
select distinct TOTHERS_ID,TOTHERS_VALUE,TSAMPLE_ID
from TOTHERS,TSAMPLE
where TSAMPLE_SYSUID = TOTHERS_SAMPLEID
and TOTHERS_DEL_STATUS='U'
and TSAMPLE_DEL_STATUS='U'
and TSAMPLE_EXPRID=?
QUERY
        $sth->execute($expr_id) or croak($sth->errstr);

        while ( my $rowref = $sth->fetchrow_hashref ) {
            $rowref->{'TOTHERS_ID'} =~ s/_/ /g;
            push(
                @others,
                {   id        => lc( $rowref->{'TOTHERS_ID'} ),
                    value     => $rowref->{'TOTHERS_VALUE'},
                    sample_id => $rowref->{'TSAMPLE_ID'},
                    expr_id   => $expr_id,
                }
            );
        }

        # dump the statement handler now that we are finished with it
        $sth->finish();
    }
    return \@others;

}

sub _get_db_qvs : PRIVATE {

    my ( $self ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my @qvs;    #AoH

    my $expr_ids = $self->_get_db_expr_ids();

    # Top-level experiment QVSs
    my $sth = $dbh->prepare(<<'QUERY');
select distinct TQUALIF_NAME,TQUALIF_VALUE,TQUALIF_SOURCE,TEXPRMNT_EXPRID
from TQUALIF,TEXPRMNT
where TEXPRMNT_EXPRID = TQUALIF_EXPRID
and TQUALIF_DEL_STATUS='U'
and TEXPRMNT_DEL_STATUS='U'
and TEXPRMNT_SUBID=?
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    while ( my $rowref = $sth->fetchrow_hashref ) {
        push(
            @qvs,
            {   qualifier => $rowref->{'TQUALIF_NAME'},
                value     => $rowref->{'TQUALIF_VALUE'},
                source    => $rowref->{'TQUALIF_SOURCE'},
                expr_id   => $rowref->{'TEXPRMNT_EXPRID'},
            }
        );
    }

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # Get the QVSs for samples associated with the experiment(s) - of
    # course, there should be only one experiment, but we're making this
    # extensible (maybe).
    foreach my $expr_id ( keys %$expr_ids ) {

        my $sth = $dbh->prepare(<<'QUERY');
select distinct TQUALIF_NAME,TQUALIF_VALUE,TQUALIF_SOURCE,TSAMPLE_ID
from TQUALIF,TSAMPLE
where TSAMPLE_SYSUID = TQUALIF_SAMPLEID
and TQUALIF_DEL_STATUS='U'
and TSAMPLE_DEL_STATUS='U'
and TSAMPLE_EXPRID=?
QUERY
        $sth->execute($expr_id) or croak($sth->errstr);

        while ( my $rowref = $sth->fetchrow_hashref ) {
            push(
                @qvs,
                {   qualifier => $rowref->{'TQUALIF_NAME'},
                    value     => $rowref->{'TQUALIF_VALUE'},
                    source    => $rowref->{'TQUALIF_SOURCE'},
                    sample_id => $rowref->{'TSAMPLE_ID'},
                    expr_id   => $expr_id,
                }
            );
        }

        # dump the statement handler now that we are finished with it
        $sth->finish();
    }
    return \@qvs;
}

sub _get_array_designid : PRIVATE {

    my ( $self, $hyb_sysuid ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    # Get a single cell containing the array design id, hopefully.
    my $sth = $dbh->prepare(<<'QUERY');
select distinct TARRAY_DESIGNID from TARRAY,THYBRID
where TARRAY_SYSUID = THYBRID_ARRAYID
and TARRAY_DEL_STATUS='U' and THYBRID_DEL_STATUS='U'
and THYBRID_SYSUID=?
QUERY
    $sth->execute($hyb_sysuid) or croak($sth->errstr);

    my $arrayref = $sth->fetchall_arrayref;

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # One entry only
    if ( $#{$arrayref} > 0 ) {
        croak(
            "Error: MIAMExpress returns too many arrays for hyb sysuid $hyb_sysuid.\n"
        );
    }

    my $array_designid = $arrayref->[0][0];

    # No zeros allowed.
    unless ($array_designid) {
        croak(
            "Error: MIAMExpress returned array design id of zero for hyb sysuid $hyb_sysuid.\n"
        );
    }

    return ($array_designid);
}

sub _get_adf_filename : PRIVATE {

    my ( $self, $array_designid ) = @_;

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    # Get a single row containing the ADF filename, submission id and
    # array submitter login, hopefully.
    my $sth = $dbh->prepare(<<'QUERY');
select distinct TARDESIN_SYSUID,TFILE_PATH,TARDESIN_SUBID,TSUBMIS_LOGIN,TARDESIN_DESIGN_NAME
from TARDESIN, TFILE, TSUBMIS
where TFILE_ARDESINID=TARDESIN_SYSUID and TSUBMIS_SYSUID=TARDESIN_SUBID
and TARDESIN_DEL_STATUS='U' and TFILE_DEL_STATUS='U' and TSUBMIS_DEL_STATUS='U'
and TARDESIN_SYSUID=?
QUERY
    $sth->execute($array_designid) or croak($sth->errstr);

    my $results = $sth->fetchall_hashref('TARDESIN_SYSUID')
	or croak($sth->errstr);

    my $arrayinfo = $results->{$array_designid}
	or croak("Error retrieving MIAMExpress ADF filename for $array_designid.\n");

    # dump the statement handler now that we are finished with it
    $sth->finish();

    # Get the array filebase, while we have a connection.
    $sth = $dbh->prepare(<<'QUERY');
select distinct TSYSOPT_VALUE from TSYSOPT
where TSYSOPT_CODE='array_datafiles_path'
QUERY

    $sth->execute() or die($sth->errstr);

    # FIXME we're assuming the query worked here.
    my $adf_filename
	= File::Spec->catfile(
	    $sth->fetchrow_arrayref()->[0],
	    $arrayinfo->{'TSUBMIS_LOGIN'},
	    'array' . $arrayinfo->{'TARDESIN_SUBID'},
	    $arrayinfo->{'TFILE_PATH'},
	);

    return (
	$adf_filename,
	$arrayinfo->{'TARDESIN_DESIGN_NAME'},
    );
}

sub _report_description : PRIVATE {

    my ( $self ) = @_;

    my $report_fh = $self->log_fh('report');

    # Connect to MX.
    my $dbh = $self->get_mx_dbh();

    my $sth = $dbh->prepare(<<'QUERY');
select distinct TEXPRMNT_DESCR
from TEXPRMNT
where TEXPRMNT_DEL_STATUS='U'
and TEXPRMNT_SUBID=?
QUERY
    $sth->execute( $self->get_tsubmis_sysuid() ) or croak($sth->errstr);

    my $arrayref = $sth->fetchall_arrayref or croak($sth->errstr);

    foreach my $description (@$arrayref) {
        format_description( $description->[0], $report_fh );
        print $report_fh "\n";
    }

    return;
}

sub visualize_experiment : RESTRICTED {

    my ( $self, $filelist ) = @_;

    warn("Graph visualization not supported for MIAMExpress experiments.\n");

    return;
}

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: MIAMExpress.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Curator::MIAMExpress - a module used by expt_check.pl

=head1 SYNOPSIS

 use ArrayExpress::Curator::MIAMExpress;
 
 my $checker = ArrayExpress::Curator::MIAMExpress->new({
     mx_login => $login,
     mx_title => $title,
 });
 
 $checker->check();

=head1 DESCRIPTION

This module provides a set of subroutines used by the experiment
checker script to interact with a local MIAMExpress installation. It
optionally exports just two (fairly heavyweight) subroutines. Please
see the end of this document for the tests carried out by this module.

=head1 OPTIONS

The following options must be used in addition to those provided by the
parent class (see L<ArrayExpress::Curator::ExperimentChecker>):

=over 2

=item C<mx_login>

The submitter login name associated with a MIAMExpress submission. See
C<mx_title>.

=item C<mx_title>

The title of a MIAMExpress submission. See C<mx_login>.

=back

=head1 TESTS

The following tests are performed by this module, with output printed
to the error and/or report filehandles (see
L<ArrayExpress::Curator::ExperimentChecker> for a list of more general
tests which are also run):

=over 4

=item B<BioMaterials>

Checks the linking between sample, extract, labeled extract and
hybridization, and writes a biomaterials log file which attempts to
portray these links in ASCII. Highlights BioMaterials which have not
been properly linked through to hybridizations (biomaterials log).

=item B<Experimental factor values>

Generates a report showing how hybridizations have been linked to
experimental factor values, and vice versa (biomaterials log).

=item B<Sample annotation>

Generates a report on all the annotation attached to the samples in
the experiment (sample log).

=item B<Publication>

Checks whether the publication details include standard Pubmed
abbreviations; also alerts the curator if a new publication ("other")
has been indicated (error log).

=item B<QualifierValueSource>

Alerts the curator if QVS entries have been used under Experiment or
Sample annotation (error log).

=item B<Other>

Alerts the curator if "Other" entries have been used under Experiment, Protocol or
Sample annotation (error log).

=item B<Transformation protocol>

Alerts the curator if a final data matrix file has been supplied
without a transformation protocol (error log).

=item B<Protocol description length>

Alerts the curator if protocols with fewer than 50 characters are
submitted (error log).

=item B<Pooling protocol>

Alerts the curator if a pooling protocol has been linked to a
submission (error log).

=item B<Normalization protocol>

Alerts the curator if normalized data files have been supplied
without a normalization protocol (error log).

=item B<Other unbound protocols>

Alerts the curator if protocols other than pooling, normalization or
transformation have not been bound to their appropriate objects (error
log).

=item B<Number of files submitted per hybridization>

Prints a table indicating which kinds of raw and normalized data file
have been submitted for each hybridization (report log).

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2005.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

1;
