#!/usr/bin/env perl
#
# Module to provide basic methods used by the expt_check script.
#
# Tim Rayner 2005, ArrayExpress team, European Bioinformatics Institute
#
# $Id: ExperimentChecker.pm 2069 2008-06-04 14:33:52Z tfrayner $
#

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: ExperimentChecker.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Curator::ExperimentChecker - a module used by expt_check.pl

=head1 SYNOPSIS

 use base qw/ArrayExpress::Curator::ExperimentChecker/;

=head1 DESCRIPTION

This module represents an abstract parent class providing methods for
data file and experiment annotation checks to its child classes. See
the following documentation for concrete checker classes:
L<ArrayExpress::Curator::Validate> (Tab2MAGE),
L<ArrayExpress::MAGETAB::Checker> (MAGE-TAB),
L<ArrayExpress::Curator::MIAMExpress> (MIAMExpress) and
L<ArrayExpress::Curator::Standalone> (standalone).

=head1 OPTIONS

The objects created by this module can be instantiated with the
following options, common to all submission routes.

=over 2

=item C<log_to_current_dir>

Write the log files to the current working directory, rather than in
the submissions directory.

=item C<is_standalone>

Run checker in standalone mode.

=item C<clobber>

Overwrite existing log and graph files without asking for confirmation.

=item C<adf_filename>

The name of the file to use as ADF in feature/reporter checks. This
overrides any array designs specified in the submission.

=item C<array_accession>

The ArrayExpress accession number of the array design to use for
feature/reporter checks. This overrides any array accessions specified
in the submission.

=item C<qt_filename>

The name of the file to be used for QT definitions. See
L<ArrayExpress::Datafile::QT_list> for information on the format of
this file. See also C<include_default_qts>.

=item C<include_default_qts>

Use the QT definitions supplied with these scripts alongside any new
QT definitions provided by the C<qt_filename> option.

=back

=head1 METHODS

=over 2

=item C<check()>

Starts the checks, based on the options specified in the constructor.

=item C<get_miamexpress_software_type()>

Returns the software term to use for a MIAMExpress export, if a
unanimous verdict can be reached. Otherwise returns undef. This is
also used for information purposes with Tab2MAGE and MAGE-TAB
submissions, which is why it's in this superclass.

=back

=head1 FILES

There are currently five log files written out by the script:

=over 2

=item C<expt_report.log>

Contains some summary information and data quality statistics for each
data file.

=item C<expt_errors.log>

Lists the errors that were encountered in parsing the data
files. Details on Feature and QuantitationType errors are given in
separate log files. Qualifier Value Source usage is also logged in
this error log file.

=item C<expt_biomaterials.log>

Provides a basic sanity check of the flow of BioMaterials through the
experiment (i.e. Sample -> Extract -> Labeled Extract ->
Hybridization). For Tab2MAGE checking the majority of the information
in this file has been moved to an output PNG file, since it is a much
clearer and more flexible visualization format.

=item C<expt_feature.log>

Lists feature coordinates and/or reporter identifiers (FGEM only)
missing from the array design(s) used in the experiment. Entries here
will typically mean that a dummy array has to be used. Update: This
file now also lists duplicate features in a separate list.

=item C<expt_columnheadings.log>

Lists unrecognized QuantitationTypes or hybridization IDs (FGEM only)
appearing in the data column headings.

=back

=head1 TESTS

The following tests are performed by this module, with output
printed to the error and/or report filehandles:

=over 4

=item B<File existence>

Checks that the data files referred to in the Tab2MAGE
spreadsheet/MAGE-TAB SDRF/MIAMExpress database actually exist on the
filesystem (error log).

=item B<Affymetrix CHP file as normalized data>

Checks that CHP files have been submitted as normalized rather than
raw data (error log).

=item B<Text file check>

Confirms that submitted data files are text, not binary. This test
is not applied to Affymetrix CHP files.

=item B<File line endings check>

Checks for Unix/DOS/Mac line endings (report log).

=item B<Affymetrix EXP file check>

Checks that EXP files are submitted as raw data and that they all have
Protocol, Station and Module information (error log).

=item B<Basic data file summary>

Prints out the file format (Affymetrix, GenePix or Generic), the type
(raw, normalized or transformed), row and column counts (report log).

=item B<Duplicate columns>

Checks for repeated column headings in each data file (error log).

=item B<Excel truncated files>

Checks for possible data corruption by Excel truncation of the file
(error log).

=item B<QuantitationTypes>

Checks column headings against a list of known QTs. Reports on
unrecognized QTs (error log, column headings log). This is a work in
progress.

=item B<FGEM hybridization IDs>

Checks FGEM column headings against the hybridization IDs for the
submission, reports on those which are not recognized. This
incorporates a check on the QTs for FGEM files (error log, column
headings log).

=item B<FGEM BioDataCube order>

Checks that the included final data matrix is laid out in DBQ order,
rather than DQB. The ArrayExpress MAGE-ML loader software does not
support the DQB order.

=item B<Data checks>

Data checks are only performed on recognized QT columns. Checks are
for:

  Text in numeric columns
  Null values in numeric columns
  Floats in integer columns
  Inappropriate boolean values (i.e., not 0 or 1)
  Log ratios outside reasonable range
  Basic check on saturation indicators (primarily GenePix files)

(error log).

=item B<Benford's law>

Calculates Benford's law across dimensioned float data. In theory
this should be approximately 30% for good data (report log).

=item B<Percent null>

Calculates overall percent null across the whole data set. Zero values
are also counted as null. In practice under 10% null values seems to
be a reasonable expectation (report log).

=item B<Feature/Reporter check vs. array design>

Checks the feature coordinates (raw, normalized data) or the reporter
identifiers (FGEM) against either the array designs linked to the
hybridization in MIAMExpress, or against a user-supplied ADF. Prints
out a list of features not found in the array design (error log,
features log). Also alerts the curator when significantly fewer
features are found in the data file compared to the array design
(error log). Note that this will give false errors on array designs
associated with dummy array designs (e.g., some Affy arrays).

=item B<Duplicate Features/Reporters>

Checks that Features or Reporter identifiers are not repeated within
the same file. Prints out a list of duplicate features/reporters
(error log, features log).

=item B<Duplicate filenames>

Checks that there are no duplicate files associated with the
submission (error log).

=item B<Row/column count consistency>

Checks that all the files of a given type (raw, normalized)
have consistent numbers of rows and columns (error log).

=item B<Affy EXP file consistency>

Checks that the parameters in each EXP file which should be the same
are the same (error log).

=item B<Potentially duplicated files with different names>

Checks a single line from each file against the same line in every
other file, and reports on any matching pairs of files (error log).

=back

=head1 SEE ALSO

=over 1

=item L<ArrayExpress::Curator::Validate>

=item L<ArrayExpress::MAGETAB::Checker>

=item L<ArrayExpress::Curator::MIAMExpress>

=item L<ArrayExpress::Curator::Standalone>

=item L<ArrayExpress::Datafile::QT_list>

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2004.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

package ArrayExpress::Curator::ExperimentChecker;

use strict;
use warnings;

use Carp;
use File::Spec;
use Scalar::Util qw(looks_like_number);    # Used for checking data values
use English qw( -no_match_vars );
use List::Util qw(max);
use List::MoreUtils qw(any none notall);
use Readonly;
use Class::Std;
use IO::Handle;
use Digest::MD5 qw( md5 );    # Used for tracking missing/duplicated features

use ArrayExpress::Curator::Config qw($CONFIG);

require ArrayExpress::Datafile::DataMatrix;

use ArrayExpress::Datafile::QT_list qw(get_QTs);

require ArrayExpress::Datafile;
require ArrayExpress::Datafile::Affymetrix;

use ArrayExpress::Datafile::Metrics qw(
    check_data_metrics
    calculate_benford
    pearson_correlation
);

use ArrayExpress::Curator::Database qw(
    retrieve_AE_adf
    parse_adf
    arrayaccession_in_aedw
    map_affy_accno_to_name
);

use ArrayExpress::Curator::Report qw(report_accumulated_errors);

use ArrayExpress::Curator::Logger;
use base qw(ArrayExpress::Curator::Logger);

# Attributes available for initialization.
my %is_standalone        : ATTR( :name<is_standalone>,          :default<undef> );
my %adf_filename         : ATTR( :get<adf_filename>,            :init_arg<adf_filename>,            :default<undef> );
my %array_accession      : ATTR( :get<array_accession>,         :init_arg<array_accession>,         :default<undef> );
my %qt_filename          : ATTR( :get<qt_filename>,             :init_arg<qt_filename>,             :default<undef> );
my %include_default_qts  : ATTR( :get<include_default_qts>,     :init_arg<include_default_qts>,     :default<undef> );
my %skip_data_checks     : ATTR( :get<skip_data_checks>,        :init_arg<skip_data_checks>,        :default<undef> );
my %safe_filechecks      : ATTR( :get<safe_filechecks>,         :init_arg<safe_filechecks>,         :default<undef> );
my %source_directory     : ATTR( :get<source_directory>,        :init_arg<source_directory>,        :default<undef> );
my %reporter_prefix      : ATTR( :get<reporter_prefix>,         :init_arg<reporter_prefix>,         :default<undef> );
my %compseq_prefix       : ATTR( :get<compseq_prefix>,          :init_arg<compseq_prefix>,          :default<undef> );
my %ignore_size_limits   : ATTR( :name<ignore_size_limits>,     :default<undef> );

# Accumulative accessors; each of these has an "add_" method defined below.
my %miame                : ATTR( :get<miame>,              :default<0>  );
my %aedw_score           : ATTR( :get<aedw_score>,         :default<0>  );
my %software_types       : ATTR( :get<software_types>,     :default<[]> );
my %missing_features     : ATTR( :get<missing_features>,   :default<{}> );
my %duplicate_features   : ATTR( :get<duplicate_features>, :default<{}> );
my %missing_features_cache   : ATTR( :get<missing_features_cache>,   :default<{}> );
my %duplicate_features_cache : ATTR( :get<duplicate_features_cache>, :default<{}> );
my %duplicate_columns    : ATTR( :get<duplicate_columns>,  :default<{}> );
my %unknown_qts          : ATTR( :get<unknown_qts>,        :default<{}> );
my %unknown_hybs         : ATTR( :get<unknown_hybs>,       :default<{}> );

# These accessors simply accumulate unique hash keys; get_* returns an
# arrayref of those keys.
my %arrayexpress_arrays  : ATTR( :get<arrayexpress_arrays>,:default<[]> );
my %expt_designs         : ATTR( :get<expt_designs>,       :default<[]> );
my %expt_organisms       : ATTR( :get<expt_organisms>,     :default<[]> );

# This is a cache of array designs keyed by id.
my %array_design_cache   : ATTR( :default<{}> );

sub START {
    my ( $self, $id, $args ) = @_;

    $self->set_progname( $CONFIG->get_EXPTCHECK_PROGNAME() );
    $self->set_version( $CONFIG->get_EXPTCHECK_VERSION() );

    return;
}

sub get_files_and_annotation : PRIVATE {

    # This is an abstract class: MIAMExpress, Tab2MAGE, and standalone
    # checking are handled by overriding this method in their
    # respective subclasses.

    my ( $self ) = @_;

    confess("Error: Stub method called in abstract class " . __PACKAGE__);

}

sub add_miame : RESTRICTED {

    # Takes an integer, bitwise ORs it with $miame{ident $self},
    # stores and returns the result. Used to store the MIAME scores
    # for compliance reporting.

    my ( $self, $err ) = @_;

    $miame{ident $self} ||= 0;
    if ($err) { $miame{ident $self} |= $err }

    return $miame{ident $self};
}

sub add_aedw_score : RESTRICTED {

    # Takes an integer, bitwise ORs it with $aedw_score{ident $self},
    # stores and returns the result. Used to store the AEDW scores
    # for compliance reporting.

    my ( $self, $err ) = @_;

    $aedw_score{ident $self} ||= 0;
    if ($err) { $aedw_score{ident $self} |= $err }

    return $aedw_score{ident $self};
}

sub _check_aedw_and_miame : PRIVATE {

    # Run some general tests on aedw suitability and MIAME compliance.
    my ($self, $hyb_ids) = @_;

    # Initialize some logs which we will always want written to. Note
    # that this must be done _after_ connecting to the database, or
    # the names don't get localized properly:
    $self->logprint('aedw',  q{});
    $self->logprint('miame', q{});

    #######################################
    # Top-level tests on AEDW suitability #
    #######################################

    # Report on the arrays used, and check against the DW. Each array
    # must pass MIAME (in the autosubmissions DB, calculated by Anna
    # Farne's scripts) for the score to be added to the total.
    my @ae_arrays         = @{ $self->get_arrayexpress_arrays() };
    my $array_miame       = $CONFIG->get_MIAME_ARRAYSEQ();
    my $all_arrays_loaded = 1;

    my %miame_compliant_pipeline
	= map { $_ => 1 } @{ $CONFIG->get_MIAME_COMPLIANT_ARRAY_PIPELINES() || [] };

    # If the array is not in AE, we have a problem.
    unless (@ae_arrays) {
	$self->logprint(
	    'aedw',
	    "Problem: no public AE arrays associated with submission.\n"
	);
	$array_miame       = 0;
	$all_arrays_loaded = 0;
    }

    # Check on known AE-loaded arrays.
    foreach my $accno ( @ae_arrays ) {
	$self->logprint(
	    'report',
	    "ArrayExpress array used: $accno\n",
	);
	unless ( arrayaccession_in_aedw($accno) ){
	    $all_arrays_loaded = 0;
	    $self->logprint(
		'aedw',
		"Problem: Array $accno is not in the data warehouse.\n"
	    );
	}

	# Check that this isn't from a pipeline with special MIAME
	# dispensation, then check that it's compliant in the
	# autosubmissions database. Accession numbers are assumed to
	# be in the form \D{n}\d{n}.
	my ($acc_prefix) = ($accno =~ m/\A (\D*?) \d+ \z/xms);
	unless ( $miame_compliant_pipeline{$acc_prefix} ) {
	    if ( $CONFIG->get_AUTOSUBS_DSN() ) {
		require ArrayExpress::AutoSubmission::DB::ArrayDesign;
		my $array = ArrayExpress::AutoSubmission::DB::ArrayDesign->retrieve(
		    accession => $accno,
		);

		# At the moment, array MIAME is yes or no.
		unless ( $array && $array->miame_score() ) {
		    $self->logprint(
			'miame',
			"Problem: Array $accno is not MIAME compliant.\n"
		    );
		    $array_miame = 0;
		}
	    }
	    else {
		$self->logprint(
		    'miame',
		    "Problem: Unable to check array MIAME compliance in subs database.\n"
		);
	    }
	}
    }

    # Add score for the data warehouse if all loaded okay.
    $self->add_aedw_score($CONFIG->get_AEDW_ARRAYLOADED()) if $all_arrays_loaded;

    # Note the final score.
    $self->add_miame($array_miame);

    # Data warehouse only wants experiments with more than x no. of hybs.
    if (scalar(grep { defined $_ } values %$hyb_ids)
	    >= $CONFIG->get_AEDW_MINIMUM_HYBS() ) {
	$self->add_aedw_score($CONFIG->get_AEDW_HYBNUMBER());
    }
    else {
	$self->logprint(
	    'aedw',
	    "Problem: Experiment has fewer than "
		. $CONFIG->get_AEDW_MINIMUM_HYBS()
		. " hybridizations.\n"
	);
    }

    # Only certain organisms and design types allowed in the DW. At
    # least one design type must be in AEDW_DESIGN_TYPES; if not,
    # check against AEDW_UNWANTED_DESIGN_TYPES and note if the
    # experiment may be under-annotated.
    my %allowed_design   = map { $_ => 1 } @{ $CONFIG->get_AEDW_DESIGN_TYPES() || [] };
    my %unallowed_design = map { $_ => 1 } @{ $CONFIG->get_AEDW_UNWANTED_DESIGN_TYPES() || [] };
    my @design_types = @{ $self->get_expt_designs() };
    my $is_bad_design_type;

    # Check the positive stuff first.
    if ( ! scalar @design_types ) {

	# No design type
	$is_bad_design_type++;
	$self->logprint(
	    'aedw',
	    "Problem: Experiment design is not given.\n"
	);
    } elsif ( any { $allowed_design{$_} } @design_types ) {

	# Good design type
	$self->add_aedw_score($CONFIG->get_AEDW_DESIGNTYPE());
    }
    else {

	# Bad design type
	$is_bad_design_type++;
	$self->logprint(
	    'aedw',
	    "Problem: Experiment design is not one of the required types:\n\t- ",
	    join("\n\t- ", sort keys %allowed_design),
	    "\n",
	);
    }

    # Hold our noses and check the negative stuff.
    if (    $is_bad_design_type
	 && ( ! scalar @design_types || none { $unallowed_design{$_} } @design_types ) ) {
	$self->logprint(
	    'aedw',
	    "Caveat: Experiment design is not one of the following unsuitable types,\n    ",
	    "and may yet be suitable for the data warehouse:\n\t- ",
	    join("\n\t- ", sort keys %unallowed_design),
	    "\n",
	);
    }

    return;
}
    
sub check {

    # Takes a list of options (see perldocs above), checks the
    # submission and returns the error code.

    my $self = shift;

    # Set STDOUT and STDERR to unbuffered, for live feedback to a
    # command-line user.
    STDOUT->autoflush(1);
    STDERR->autoflush(1);    # in case someone else has tampered...

    # Connect to MX, or read in the spreadsheet; populate array design info.
    my ( $filelist, $hyb_ids, $norm_ids )
	= $self->get_files_and_annotation();

    # Check the return values, just in case.
    unless ( ( ref $filelist eq 'ARRAY' )
	  && ( ref $hyb_ids  eq 'HASH' )
          && ( ref $norm_ids eq 'HASH' ) ) {
	$self->logprint(
	    'error',
	    "FATAL ERROR: Unable to retrieve data file and/or annotation information\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
	return $self->get_error();
    }

    # AEDW and MIAME checks (annotation only).
    $self->_check_aedw_and_miame($hyb_ids);

    ##############################
    # Begin the datafile reports #
    ##############################

    # Sort out our QuantitationTypes

    $self->logprint_line( 'error', 'QT file parsing START' );

    # Get the QTs, either discarding or including known QTs
    my $QTs = get_QTs( $self->get_qt_filename, $self->get_include_default_qts );

    $self->logprint_line( 'error', 'QT file parsing END' );

    # This is the heavyweight method that does all the work.
    $self->check_file_list($filelist, $QTs, $hyb_ids, $norm_ids);

    # Implementation of this method is left to the concrete subclasses.
    $self->visualize_experiment($filelist);

    return $self->get_error;

}

sub visualize_experiment : RESTRICTED {

    my ( $self ) = @_;

    croak("ERROR: Abstract stub method called in ExperimentChecker superclass.");
}

sub check_file_list : PRIVATE {

    my ($self, $filelist, $QTs, $hyb_ids, $norm_ids) = @_;
    
    ######################
    # START OF FILE LOOP #
    ######################

    # Start checking the files
    $self->logprint_line( 'error', 'Data file parsing START' );

    print STDOUT ( "\n" . scalar(@$filelist) . " files to process.\n\n" );

    my (%file_name_count, %file_type_count);

    # If the -x option was used, skip the heavyweight checks.
    if ( $self->get_skip_data_checks() ) {
	$self->logprint( 'error', "\nWARNING: SKIPPING data file checks.\n\n" );
	$self->logprint( 'report', "** Data file checks were skipped. **\n\n" );
	print STDOUT ( "** Skipping data file checks **.\n\n" );
    }

    # Run some checks on the files.
    # Set FV "missing" flag to one if there are no files at all.
    my $factorval_missing = scalar @$filelist ? 0 : 1;
    foreach my $file (@$filelist) {

	# Record some aggregate metadata.
	$file_name_count{ $file->get_name() }++;
	$file_type_count{ $file->get_data_type() }++;

	# DW submissions must have factor values, attached here to the
	# files (MX checking does not attach FVs to FGEM datafile object).
	unless ( scalar( grep { defined $_ } values %{ $file->get_factor_value() } )
	      || ( $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE()
	      ||   $file->get_data_type() eq $CONFIG->get_RAW_DM_FILE_TYPE()
	      ||   $file->get_is_exp() ) ) {

	    $factorval_missing++;
	}

	# Check file, unless told not to (-x).
	unless ( $self->get_skip_data_checks() ) {
	    $self->check_file($file, $QTs, $hyb_ids, $norm_ids);
	}
    }

    # Note any FV problems.
    if ( $factorval_missing ) {

	# AEDW
	$self->logprint(
	    'aedw',
	    "Problem: Experiment does not have factor value for one or more hybs.\n"
	);

	# MIAME
	$self->logprint(
	    'miame',
	    "Problem: Experiment does not have factor value for one or more hybs.\n"
	);
    }
    else {

	# Record the AEDW and MIAME scores.
	$self->add_aedw_score($CONFIG->get_AEDW_FACTORVALUES());
	$self->add_miame($CONFIG->get_MIAME_FACTORVALUES());
    }

    ####################
    # END OF FILE LOOP #
    ####################

    $self->logprint_line( 'report' );

    $self->logprint_line( 'error', 'Data file parsing END' );

    # Commence aggregate checks on the parsed files.
    $self->logprint_line( 'error', 'Final consistency check START' );

    # Check for duplicate file uploads (simply by name; no check of contents)
    foreach my $filename ( keys %file_name_count ) {
        if ( $file_name_count{$filename} > 1 ) {
            $self->logprint( 'error',
                "ERROR: Files named $filename have been uploaded multiple times.\n"
            );
            $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
        }
    }

    # Check that we have raw and (norm || fgem) files; this is a DW
    # (and/or MIAME) requirement and may be elaborated upon later.
    my $good_aedw_data = 1;
    if ($file_type_count{'raw'}) {
	$self->add_miame($CONFIG->get_MIAME_RAWDATA);
    }
    else {
	$self->logprint(
	    'miame',
	    "Problem: Raw data is absent.\n"
	);
    }

    if ( $file_type_count{'normalized'}
	     || $file_type_count{$CONFIG->get_FGEM_FILE_TYPE()} ) {
	$self->add_miame($CONFIG->get_MIAME_NORMDATA);
    }
    else {
	$good_aedw_data = 0;
	$self->logprint(
	    'aedw',
	    "Problem: Normalized data is absent.\n"
	);
	$self->logprint(
	    'miame',
	    "Problem: Normalized data is absent.\n"
	);
    }
    $self->add_aedw_score($CONFIG->get_AEDW_GOODDATA()) if $good_aedw_data;

    # Check row and column counts across files.
    $self->_check_datafile_consistency($filelist);

    # Report on QTs, FGEM hyb_ids and duplicate columns
    report_accumulated_errors(
        $self->get_unknown_qts(),
        $self->log_fh('columns'),
        'Error: The following unknown QuantitationTypes:'
    );
    report_accumulated_errors(
        $self->get_unknown_hybs(),
        $self->log_fh('columns'),
        'Error: The following unknown Hyb/Norm IDs:'
    );
    report_accumulated_errors(
        $self->get_duplicate_columns(),
        $self->log_fh('columns'),
        'ERROR: The following duplicated column headings:'
    );
    report_accumulated_errors(
        $self->get_missing_features(),
        $self->log_fh('feature'),
        'ERROR: The following features or identifiers missing from the array:'
    );
    report_accumulated_errors(
        $self->get_duplicate_features(),
        $self->log_fh('feature'),
        'ERROR: The following duplicated feature coordinates / identifiers:'
    );

    # Sort out which are the EXP files
    my $exp_filelist;
    foreach my $file (@$filelist) {
        push( @$exp_filelist, $file ) if $file->get_is_exp();
    }

    # Check the EXP files
    my $exp_inconsistencies = $self->_check_exp_params($exp_filelist);

    # Print out what we find
    foreach my $section ( sort keys %$exp_inconsistencies ) {
        foreach my $param ( sort keys %{ $exp_inconsistencies->{$section} } )
        {
            $self->logprint( 'error',
                "Warning: parameter differs across the submitted EXP files: [$section] $param\n"
            );
            $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
        }
    }

    # Pairwise many-to-many comparison of a single selected data line
    # from each file to catch renamed duplicates. This has been
    # overloaded in the case of binary files to hold an md5 hash of
    # the entire file.
    for ( my $i = 0; $i <= $#$filelist; $i++ ) {
        for ( my $j = $i + 1; $j <= $#$filelist; $j++ ) {
            my $line_i = $filelist->[$i]->get_test_data_line();
            my $line_j = $filelist->[$j]->get_test_data_line();

            # Binary files may not have the test line.
            if ( $line_i && $line_j && ( $line_i eq $line_j ) ) {
                $self->logprint(
                    'error',
                    "WARNING: Files ",
                    $filelist->[$i]->get_name(),
                    " and ",
                    $filelist->[$j]->get_name(),
                    " may be identical.\n"
                );
                $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
            }
        }
    }

    $self->logprint_line( 'error', 'Final consistency check END' );

    print STDOUT ("\nDone processing files.\n");

    return;
}

sub non_rowbased_data_test : PRIVATE {

    # For data files which don't conform to the usual model of rows of
    # data with headings, we run some basic tests here. This includes
    # all Affy and binary file types.

    # N.B. Affy CELv3 used to be treated as row-level data but when we
    # export them as MAGE-ML we use a separate parser, so it would be
    # better to invoke that during checking (FIXME?).

    my ( $self, $file ) = @_;

    # We can't sample the data, so we just take the MD5 hash.
    $file->set_test_data_line( $file->get_md5_digest() );

    # Check the file against those already loaded in AE.
    if ( $CONFIG->get_AUTOSUBS_DSN() ) {
	$self->check_file_against_loaded_data( $file );
    }

    $file->set_row_count(0);    # reset the row count

    return;
}

sub affymetrix_data_test : PRIVATE {

    my ( $self, $file ) = @_;

    # Affymetrix CHP or XDA CEL; record software for consensus
    # calculation.
    $file->set_format_type('Affymetrix');
    $self->add_software_types('Affymetrix');

    $self->confirm_affy_chip_type( $file );

    # Very quick check to make sure CHP files are in the right place
    if (   $file->get_path() =~ m/\.CHP$/i
	       && $file->get_data_type ne 'normalized' ) {

	$self->logprint(
	    'error',
	    "Error: CHP file ",
	    $file->get_name(), " submitted as raw data or FGEM.\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    # Some generic tests we can run.
    $self->non_rowbased_data_test( $file );

    return;
}

sub affymetrix_expfile_test : PRIVATE {

    # Check Affy EXP files are in the right place, and that they
    # look okay.
    my ( $self, $file ) = @_;

    $file->set_format_type('Affymetrix');

    # EXP files should be submitted as raw data (MX) or as a dedicated
    # EXP file type (Tab2MAGE).
    unless (    $file->get_data_type() eq 'raw'
	     || $file->get_data_type() eq 'EXP' ) {
	$self->logprint(
	    'error',
	    "Error: EXP file ",
	    $file->get_name(), " submitted as either normalized or FGEM.\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    # Check on EXP files; populate the {exp_data} hash key,
    # set format etc.
    unless ( $file->parse_exp_file() ) {
	$self->logprint(
	    'error',
	    sprintf(
		"ERROR parsing EXP file %s\n",
		$file->get_name(),
	    ),
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    my $exp_data = $file->get_exp_data();

    unless (   $exp_data->{Fluidics}{Protocol}
            && $exp_data->{'Sample Info'}{'Chip Lot'} ) {

	$self->logprint(
	    'error',
	    "Warning: EXP file ",
	    $file->get_name(),
	    " lacks either chip lot or protocol info.\n",
	);

	# Have relaxed this since we won't be using GDAC when
	# automating anyway.
	$self->add_error( $CONFIG->get_ERROR_MIAME() );
    }

    return;
}

sub check_file : PRIVATE {

    # Returns 1 on successful check (whether the file is good or not),
    # undef if there was a problem checking.

    my ( $self, $file, $QTs, $hyb_ids, $norm_ids ) = @_;

    $self->logprint_line( 'report' );

    my ($feature_coords);

    # Set the data file error filehandle.
    $file->set_error_fh( $self->log_fh('error') );

    # just the filename
    $file->set_name( ( File::Spec->splitpath( $file->get_path() ) )[2] );

    my $hybdir =
      ( ! $file->get_hyb_sysuid()
          || ( $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE() ) )
      ? undef
      : "hybrid" . $file->get_hyb_sysuid();
    my $hyb_filename = $hybdir
      ? File::Spec->catfile( $hybdir, $file->get_name() )
      : $file->get_name();

    print STDOUT ( "Checking file ", $file->get_name(), "... " );
    $self->logprint_line('error', sprintf( "  %s  ", $file->get_name() ));

    # Check that files are actually present and readable.
    unless ( -r $file->get_path() ) {
        $self->logprint( 'error', "FILE ERROR: File ",
            $file->get_path(), " not found or unreadable.\n" );
        $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
        print STDOUT ("\n");
        return;    # Bail out with undef return
    }

    # Quick file size check - sometimes uploads result in
    # zero-byte files.
    if ( -s $file->get_path() == 0 ) {
        $self->logprint(
         'error', "ERROR: File ",
           $file->get_path(), " is empty (zero bytes).\n"
        );
        $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    # If file exists, list it in the report log
    $self->logprint( 'report', "Filename     : ",
		     $file->get_path(),      "\n" );
    $self->logprint( 'report', "File type    : ",
		     $file->get_data_type(), "\n" );

    # Handle Affymetrix files here. In general we now don't bother
    # running tests on these files. This at least confirms that the
    # file header can be parsed (confirm_affy_chip_type).
    if ( $file->get_path() =~ m/\. (CHP|CEL|EXP) \z/ixms ) {

	if ( $file->get_path() =~ m/\. EXP \z/ixms ) {
	    $self->affymetrix_expfile_test( $file );
	}
	else {
	    $self->affymetrix_data_test( $file );
	}

	$self->logprint(
	    'report', "File format  : ", $file->get_format_type(), "\n",
	);

	# Finish the STDOUT row.
	print STDOUT "done.\n";

        return 1;
    }

    # Quick data type check here (text vs. binary).
    if ( $file->get_is_binary() ) {    # unless file is ASCII...

        $self->logprint( 'report',
            "File format  : Binary (skipped further analysis)\n" );

	# Warn on unrecognized binary file type. We've already handled
	# Affy, so this file must not be recognized.
	print STDOUT (
	    "Error: File ",
	    $file->get_name(), " is a binary file. Skipping.\n"
	);
	$self->logprint(
	    'error',
	    "Error: File ", $file->get_name(),
	    " is a binary file; skipped further analysis.\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );

	# Some generic tests we can run.
	$self->non_rowbased_data_test( $file );

	# Finish the STDOUT row.
	print STDOUT "done.\n";

	# Bail out with undef return
        return;
    }

    # File size check for safety's sake
    unless ( ( -s $file->get_path() < $CONFIG->get_MAX_DATAFILE_SIZE() )
	       || $self->get_ignore_size_limits() ) {
        print STDERR ( "Error: File ", $file->get_name(),
            " is too large. Skipping.\n" );
        $self->logprint(
	    'report',
	    "File format  : Unknown (file too large: skipped further analysis)\n"
        );
        $self->logprint( 'error', "ERROR: File ", $file->get_name(),
            " is too large to be processed safely; further analysis skipped.\n"
        );

        # Likely these large files will break downstream processing, and
        # we haven't checked them anyway.
        $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
        $file->set_row_count(0);    # reset the row count
        return;    # Bail out with undef return
    }

    # Now we've discarded the corner cases, get some actual
    # information from the file. This also populates the $file object.
    my $rc;
    ( $feature_coords, $rc ) = $file->parse_datafile(
	$QTs,
	$hyb_ids,
	$norm_ids,
    );

    # Note down the software type so we can derive a consensus later.
    my $qt_type = $file->get_qt_type();
    if ( $qt_type ) {

	# Take everything up to the first square bracket.
	$qt_type =~ s/^([^\[]*).*/$1/;
	$self->add_software_types($qt_type);
    }
    if ($rc) {
	$self->logprint(
	    'error',
	    "PARSE ERROR: file ",
	    $file->get_name(), ": $rc",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    # Print out some basic information on the files
    $self->logprint(
        'report',
        "File format  : ",
        ( $file->get_qt_type() or 'Unknown' ), "\n"
    );
    $self->logprint( 'report', "Line endings : ",
		     $file->get_line_format(), "\n" );

    $self->logprint(
	'report',
	"Rows         : ",
	( $file->get_row_count() || 'N/A' ), "\n"
    );
    my $column_count = scalar( @{ $file->get_column_headings() } );
    $self->logprint(
	'report',
	"Columns      : ",
	( $column_count || 'N/A' ), "\n"
    );

    unless ($column_count) {
	$self->logprint(
	    'error',
	    "PARSE ERROR: Failed to parse column headings in file ",
	    $file->get_name(), "\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
	$file->increment_parse_errors();
    }

    # Check for repeated column headings in each data file.
    $self->_check_duplicate_columns( $file, $hyb_filename );

    # Crappy Excel truncates files. Here we check for that.
    if ( $file->get_row_count() == 65535 ) {
	$self->logprint(
	    'error',
	    "WARNING: Possible Excel-truncated file ",
	    $file->get_name(), "\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
    }

    # Report on bad column heading QTs
    if ( scalar @{ $file->get_fail_columns() } ) {
	$self->add_unknown_qts( $hyb_filename, $file->get_fail_columns() );
	$self->logprint(
	    'error',
	    "Warning: Possible QuantitationType problems in file ",
	    $file->get_name(), ". See column heading log file.\n",
	);

	# In the past this has been PARSEBAD for tab2mage,
	# INNOCENT for MX. Whilst the autosubmissions system uses
	# keep_all_qts, however, we can relax this.
	$self->add_error( $CONFIG->get_ERROR_INNOCENT() );
    }

    # This is for bad FGEM column heading hyb ids
    if ( scalar @{ $file->get_fail_hybs() } ) {
	$self->add_unknown_hybs( $hyb_filename, $file->get_fail_hybs() );
	$self->logprint(
	    'error',
	    "Error: Hyb/Norm ID problems in file ",
	    $file->get_name(), ". See column heading log file.\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    if ( $file->get_data_type eq $CONFIG->get_FGEM_FILE_TYPE() ) {

	my $datamatrix = ArrayExpress::Datafile::DataMatrix->new();

	my ( $ba_list, $qt_list, $order ) =
	    $datamatrix->get_dimension_lists(
		$file->get_heading_hybs(),
		$file->get_heading_qts(),
	    );
	if ( !$order ) {
	    $self->logprint(
		'error',
		"PARSE ERROR: FGEM data cube order cannot be determined.\n"
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
	}
	elsif ( $order eq 'DQB' ) {
	    $self->logprint(
		'error',
		qq{WARNING: FGEM data cube order "$order" }
	      . qq{is unsupported by ArrayExpress.\n}
	    );

	    # Not really a bad parsing, but we don't support
	    # this coding order in ArrayExpress yet.
	    $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
	}
    }

    # Call check_data_metrics() to check data for consistency, one
    # column at a time
    check_data_metrics( $file->get_data_metrics );

    while ( my ( $header, $qt_metric ) = each %{ $file->get_data_metrics() } ) {
	foreach my $error_message ( keys %{ $qt_metric->{errors} } ) {

	    # Filter out the singletons; they're not much of a
	    # problem, and all too common
	    if ( $qt_metric->{errors}{$error_message} > 10 ) {
		$self->logprint(
		    'error',
		    "Warning: $error_message (for ",
		    $qt_metric->{errors}{$error_message}
			. " values) in column \'$header\', file ",
		    $file->get_name(),
		    "\n"
		);

		# We see these all the time; maybe make this stricter
		# when QT processing is more finalized?
		$self->add_error( $CONFIG->get_ERROR_INNOCENT() );
	    }
	}
    }

    # Print out the overall Benford's law value for each datafile (can
    # do this column-wise if there's demand).  N.B. we're only
    # calculating from MeasuredSignal values at this time, because
    # that's where the interesting data is.
    my $benval = calculate_benford( $file->get_data_metrics() );
    if ( looks_like_number($benval) ) {
	$self->logprint(
	    'report',
	    sprintf( "Benford's Law value: %.2f%%\n", $benval ),
	);

# Benford's law warnings deactivated for now (we can't do much about bad data).
#  if ($benval < 20 || $benval > 40){ # This value should be around 30%
#    $self->logprint('error', "Warning: Benford's Law violated by data in file ",$file->get_name(),". See report log.\n");
#    $self->add_error($CONFIG->get_ERROR_INNOCENT());
#  }

    }
    else {
	$self->logprint(
	    'report',
	    sprintf( "Benford's Law value: %s\n", $benval ),
	);
    }

    # Print out percent null values for all the columns containing
    # recognized QTs.
    my $percent_null = $file->percent_null;
    if ( looks_like_number($percent_null) ) {
	$self->logprint(
	    'report',
	    sprintf( "Percent null values: %.2f%%\n", $percent_null ),
	);

	# This value should be less than around 10-15%, based on what
	# I've seen so far.
	if ( $percent_null > 50 ) {
	    $self->logprint(
		'error',
		"WARNING: Percent null data in ",
		$file->get_name(), " > 50%. See report log.\n",
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
	}
    }
    else {
	$self->logprint(
	    'report',
	    sprintf( "Percent null values: %s\n", $percent_null ),
	);
    }

    # We check raw and normalized files by feature coordinate,
    # combined (transformed) data files by reporter identifier.
    my $num_index_columns = scalar( @{ $file->get_index_columns() } );

    # Skip files which weren't parsed correctly.
    if (   $num_index_columns
	&& $feature_coords
        && @{$feature_coords} ) {

	$self->_compare_index_to_design(
	    $file,
	    $feature_coords,
	    $hyb_filename,
	);

	# Check for repeated features, reporters within a file. Note
	# that repeated reporters or compseqs ($num_index_columns ==
	# 1) are okay.
	unless ( $num_index_columns == 1 ) {
	    $self->_check_duplicate_features(
		$file,
		$feature_coords,
		$hyb_filename,
	    );
	}
    }

    else {

	# Bail if there are no recognizable features.
	$self->logprint(
	    'error',
	    "PARSE ERROR: Failed to parse features in file ",
	    $file->get_name(), ". Skipping feature/reporter check.\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    # And now we're done with this file
    print STDOUT ("done.\n");

    return 1;
}

sub get_miamexpress_software_type {

    # Returns the software term to use for a MIAMExpress export, if a
    # unanimous verdict can be reached. Otherwise returns undef.
    my ( $self ) = @_;

    my %consensus;

    foreach my $software ( @{ $self->get_software_types() } ) {
        $consensus{$software}++;
    }

    my $consensus_number = scalar( grep { defined $_ } values %consensus );

    if ( $consensus_number == 1 ) {
        return (keys %consensus)[0];    # Only one key.
    }
    else {
        return;    # Beware list context in caller.
    }

}

#################################
# Accumulative accessor methods #
#################################

sub add_missing_features : PRIVATE {

    my ( $self, @args ) = @_;

    if (@args) {
	my %tmp = @args;
	while ( my ( $file, $features ) = each %tmp ) {

	    # We use an md5 hash string here rather than the full
	    # listing to save on memory; typically multiple files will
	    # share a list of missing features.
	    my $md5 = Digest::MD5->new();
	    foreach my $de_id (@$features) { $md5->add($de_id); }
	    my $ded_md5 = $md5->digest;

	    # Reuse the list generated from previous files. Note that
	    # we can also try writing the cache to disk and just index
	    # via $ded_md5 in future.
	    $missing_features_cache{ident $self}{ $ded_md5 } ||= $features;
	    $missing_features{ident $self}{ $file }
		= $missing_features_cache{ident $self}{ $ded_md5 };
	}
    }

    return;
}

sub add_duplicate_features : PRIVATE {

    my ( $self, @args ) = @_;

    if (@args) {
	my %tmp = @args;
	while ( my ( $file, $features ) = each %tmp ) {

	    # We use an md5 hash string here rather than the full
	    # listing to save on memory; typically multiple files will
	    # share a list of duplicated features.
	    my $md5 = Digest::MD5->new();
	    foreach my $de_id (@$features) { $md5->add($de_id); }
	    my $ded_md5 = $md5->digest;

	    # Reuse the list generated from previous files. Note that
	    # we can also try writing the cache to disk and just index
	    # via $ded_md5 in future.
	    $duplicate_features_cache{ident $self}{ $ded_md5 } ||= $features;
	    $duplicate_features{ident $self}{ $file }
		= $duplicate_features_cache{ident $self}{ $ded_md5 };
	}
    }

    return;
}

sub add_duplicate_columns : PRIVATE {

    my ( $self, @args ) = @_;

    if (@args) {
	my %tmp = @args;
	@{ $duplicate_columns{ident $self} }{ keys %tmp } = values %tmp;
    }
    return $duplicate_columns{ident $self};
}

sub add_unknown_qts : PRIVATE {

    my ( $self, @args ) = @_;

    if (@args) {
	my %tmp = @args;
	@{ $unknown_qts{ident $self} }{ keys %tmp } = values %tmp;
    }
    return $unknown_qts{ident $self};
}

sub add_unknown_hybs : PRIVATE {

    my ( $self, @args ) = @_;

    if (@args) {
	my %tmp = @args;
	@{ $unknown_hybs{ident $self} }{ keys %tmp } = values %tmp;
    }
    return $unknown_hybs{ident $self};
}

sub add_arrayexpress_arrays : PRIVATE {

    my ( $self, @args ) = @_;

    if (@args) {
	my %array = map { $_ => 1 }
	    @{ $arrayexpress_arrays{ident $self} };
	foreach my $elem (@args) {
	    $array{$elem}++;
	}
	$arrayexpress_arrays{ident $self} = [ keys %array ];
    }
    return $arrayexpress_arrays{ident $self};
}

sub add_expt_designs : RESTRICTED {

    my ( $self, @args ) = @_;

    if (@args) {
	my %array = map { $_ => 1 }
	    @{ $expt_designs{ident $self} };
	foreach my $elem (@args) {
	    $array{$elem}++;
	}
	$expt_designs{ident $self} = [ keys %array ];
    }
    return $expt_designs{ident $self};
}

sub add_expt_organisms : RESTRICTED {

    my ( $self, @args ) = @_;

    if (@args) {
	my %array = map { $_ => 1 }
	    @{ $expt_organisms{ident $self} };
	foreach my $elem (@args) {
	    $array{$elem}++;
	}
	$expt_organisms{ident $self} = [ keys %array ];
    }
    return $expt_organisms{ident $self};
}

###################
# Private methods #
###################

sub confirm_affy_chip_type : PRIVATE {

    my ( $self, $file ) = @_;

    # Takes an Affy Datafile (CHP or CEL), finds its chip_type (needs
    # to parse the files, wrapped in an eval); compares this to the
    # value returned from map_affy_accno_to_name for the
    # ArrayDesign.accession (assuming both these values can be
    # obtained), and alert the user if they differ.

    my ( $chip_type, $is_gdac_chp );

    local $EVAL_ERROR;
    my $sighandler = $SIG{__DIE__};
    delete $SIG{__DIE__};
    eval {
	my $fac    = ArrayExpress::Datafile::Affymetrix->new();
	my $parser = $fac->make_parser( $file->get_path() );

	# FIXME One day we hope to be able to eliminate this call.
	$parser->parse_header();

	$chip_type = $parser->get_chip_type();
	$is_gdac_chp
	    = $parser->isa('ArrayExpress::Datafile::Affymetrix::CHP::GDAC_CHP');
    };
    $SIG{__DIE__} = $sighandler if $sighandler;

    if ( $EVAL_ERROR ) {

	# Affy parser failed, we need to report that.
	$self->logprint(
	    'error',
	    sprintf(
		qq{Error: Unable to parse Affymetrix file "%s": %s\n},
		$file->get_name(),
		$EVAL_ERROR,
	    ),
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }
    else {

	# Check the chip type against the array accession.
	my $accession;
	if ( my $array = $file->get_array_design() ) {
	    $accession = $array->get_accession();
	}
	if ( $accession && $chip_type ) {
	    my $wanted = map_affy_accno_to_name( $accession );
	    if ( $wanted ) {
		unless ( $wanted eq $chip_type ) {
		    $self->logprint(
			'error',
			sprintf(
			    qq{Warning: File "%s" [%s] assigned to incorrect array design (%s).\n},
			    $file->get_name(),
			    $chip_type,
			    $accession,
			),
		    );
		    $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
		}
	    } else {
		$self->logprint(
		    'error',
		    "Warning: No [chip type] term in array design name for $accession\n",
		);
		$self->add_error( $CONFIG->get_ERROR_INNOCENT() );
	    }
	}
	elsif ( ! $is_gdac_chp ) {

	    # We skip this warning for old GDAC CHP files since we'd
	    # have to parse the file fully (not just the header) to
	    # get the chip_type.
	    $self->logprint(
		'error',
		sprintf(
		    "Warning: Either array accession or chip type unavailable for %s.\n",
		    $file->get_name(),
		),
	    );
	    $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
	}
    }

    return;
}

sub check_file_against_loaded_data : PRIVATE {

    # Checks a binary (or ascii CELv3) file's md5 hash against those
    # computed for AE loaded data.
    my ( $self, $file ) = @_;

    unless ( $CONFIG->get_AUTOSUBS_DSN() ) {
	confess(
	    "Error: Cannot check file against loaded data: no database connection"
	);
    }

    require ArrayExpress::AutoSubmission::DB::LoadedData;

    my @previous = ArrayExpress::AutoSubmission::DB::LoadedData->search(
	md5_hash => $file->get_md5_digest(),
    );

    if ( scalar @previous ) {
	my @accessions;
	foreach my $data ( @previous ) {
	    foreach my $expt ( $data->experiments() ) {
		push @accessions, $expt->accession();
	    }
	}
	my $accs = join(", ", @accessions);
	my $filename = $file->get_name();
	$self->logprint(
	    'error',
	    "Warning: File $filename has been previously loaded for experiments $accs\n",
	);
	$self->add_error( $CONFIG->get_ERROR_INNOCENT() );
    }
}

sub add_software_types : PRIVATE {

    my ( $self, @args ) = @_;

    if (@args) {
        push( @{ $software_types{ident $self} }, @args );
    }

    return $software_types{ident $self};

}

sub cache_user_supplied_arrays : RESTRICTED {

    # If an ADF or AE accession has been specified, sort it out
    # here. This method should not be called unless a user-supplied
    # argument has been provided.
    my ( $self, $filelist ) = @_;

    ref $filelist eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $array_designs = {};

    # Default supplied for cases where an accessionless ADF is used.
    my $design_id = $self->get_array_accession() || 'user supplied';

    # Set the design_ids so that all the files point to the user
    # supplied design id.
    foreach my $file (@$filelist) {
        $file->set_array_design_id($design_id);
    }

    # Use the user-supplied ADF, overriding anything in the database.
    if ( $self->get_adf_filename() ) {

        $self->logprint( 'error', "ADF filename ", $self->get_adf_filename,
            " supplied to script. Ignoring links to array designs experiment annotation.\n"
        );
        $self->add_error( $CONFIG->get_ERROR_INNOCENT() );

        # Get a reference to a hash with keys corresponding to
        # period-delimited feature coordinates
	if ( $self->get_skip_data_checks() ) {
	    print STDOUT ("Skipping ADF parsing.\n");
	}
	else {
	    my $adf_fh = IO::File->new( $self->get_adf_filename, '<' )
		or croak( "Error: Unable to open ADF "
			 . $self->get_adf_filename
			 . ": $!\n" );

	    my $array = parse_adf(
		$adf_fh,
		undef,
		$self->get_reporter_prefix(),
		$self->get_compseq_prefix(),
	    );
	    $self->set_cached_array_design( $design_id, $array );
	}
    }

    # Use a user-supplied ArrayExpress accession number, if available
    elsif ( $self->get_array_accession() ) {

        $self->logprint(
            'error',
            "ArrayExpress accession number ",
            $self->get_array_accession(),
            " supplied to script. Ignoring links to array designs in MIAMExpress database.\n"
        );
        $self->add_error( $CONFIG->get_ERROR_INNOCENT() );

        # Parse the ADF
        $self->get_ae_arraydesign({
	    accession => $self->get_array_accession,
	});
    }

    else { warn("Warning: No array design associated with file.\n"); }

    return;
}

sub set_cached_array_design : RESTRICTED {

    my ( $self, $design_id, $array ) = @_;

    $design_id or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    $array && $array->isa('ArrayExpress::Datafile::ArrayDesign')
	or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # Store the array design for later perusal.
    $array_design_cache{ident $self}{$design_id} = $array;

    return;
}

sub get_cached_array_design : RESTRICTED {

    # Requires an array id
    my ( $self, $design_id ) = @_;

    $design_id or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # Return undef if array not in cache.
    return $array_design_cache{ident $self}{$design_id};
}

sub populate_file_arraydesigns : RESTRICTED {

    my ( $self, $filelist ) = @_;

    ref $filelist eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    DATAFILE:
    foreach my $file (@$filelist) {

        $file->isa('ArrayExpress::Datafile')
            or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

        # Put the ADF features in the file hash. This links up two
        # database queries: one datafile-based, the other array design
        # id-based. The design_id is the key.
	my $design_id = $file->get_array_design_id();

	# Skip EXP files.
        next DATAFILE if $file->get_is_exp();

	if ( my $array = $self->get_cached_array_design( $design_id ) ) {
	    $file->set_array_design( $array );
	}
    }

    return;
}

sub get_ae_arraydesign : RESTRICTED {

    # Takes either an AE database id or an accession number (as part
    # of the $args hashref), returns an ADF hashref as returned by
    # Database::parse_adf().
    my ($self, $args) = @_;

    my ($temp_adf_fh, $accno);

    # Attempt to get the ADF from AE, alongside the accession if only
    # the database_id is known. We trap any errors here, and avoid
    # them propagating further up the execution stack.
    local $EVAL_ERROR;
    my $sighandler = $SIG{__DIE__};
    delete $SIG{__DIE__};
    eval {
	( $temp_adf_fh, $accno ) = retrieve_AE_adf(
	    $args->{database_id},
	    $args->{accession},
	    $self->log_fh('error'),
	    $self->get_skip_data_checks(),
	);
    };
    $SIG{__DIE__} = $sighandler if $sighandler;

    # Warn if the AE connection fails.
    if ( $EVAL_ERROR ) {

	print STDERR (
	    "Warning: Problem connecting to ArrayExpress:\n  $EVAL_ERROR"
		. " ...Attempting to continue...\n"
	    );

	$self->logprint(
	    'error',
	    "Warning: ADF retrieval from"
		. " ArrayExpress failed: $EVAL_ERROR\n"
	    );

	# Set an error, since now we can't check against the array
	# design.
	$self->add_error(
	    $CONFIG->get_ERROR_PARSEBAD()
	);
    }

    # $temp_adf_fh is undef if we're skipping ADF checks.
    elsif ( $temp_adf_fh ) {
	
	my $array = parse_adf(
	    $temp_adf_fh,
	    $accno,
	    $self->get_reporter_prefix(),
	    $self->get_compseq_prefix(),
	);

	$self->set_cached_array_design( $accno, $array );
    }
    else {
	print STDOUT ("Skipping ADF parsing.\n");
    }

    # Note the array accession for further checks later.
    $self->add_arrayexpress_arrays($accno) if $accno;
}

sub _check_datafile_consistency : PRIVATE {

    my ( $self, $filelist ) = @_;

    # Sanity check of row and column counts across datafiles (raw, norm
    # separately) Now checks for inconsistencies only within data files
    # associated with a given array.  Could check the column headings
    # for consistency here also FIXME.
    foreach my $type ( @{ $CONFIG->get_T2M_FILE_TYPES() } ) {
        my ( $rowno, $colno );

        DATA_FILE:
        foreach my $file (@$filelist) {

	    # Don't bother comparing non-existent or read-protected
	    # files against everything else.
            next DATA_FILE unless ( -r $file->get_path() );

            # Only count file types from $CONFIG->get_T2M_FILE_TYPES().
            next DATA_FILE
                unless ( $file->get_data_type() eq $type );

            # Leave out EXP files (these can still be raw, although
            # they should be EXP).
            next DATA_FILE if ( $file->get_is_exp() or $file->get_parse_errors() );

            # Affy binary files are assumed okay; other binary file
            # errors are reported elsewhere.
            next DATA_FILE if ( $file->get_is_binary() );

            # Count of data rows.
            $rowno->{ $file->get_array_design_id() }{ $file->get_row_count() }++;

            # Count of column headings.
            my $column_count = scalar( @{ $file->get_column_headings } );
            $colno->{ $file->get_array_design_id() }{$column_count}++;
        }

        # Count the number of different row and column counts across the
        # data file set.
        ROW_COUNT:
        foreach my $array_rowno ( values %$rowno ) {
            unless ($array_rowno) {
                $self->logprint( 'error',
                    "Error: Data row counts not found in any files!\n" );
                $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
                next ROW_COUNT;
            }
            if ( scalar( grep { defined $_ } values %$array_rowno ) > 1 ) {
                $self->logprint( 'error',
                    "Warning: Files of type \'$type\' have inconsistent numbers of data rows:\n"
                );
                $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
                foreach my $rowcount ( keys %$array_rowno ) {
                    $self->logprint( 'error',
                        "        $array_rowno->{$rowcount} files have $rowcount rows;\n"
                    );
                }
            }
        }

        COLUMN_COUNT:
        foreach my $array_colno ( values %$colno ) {
            unless ($array_colno) {
                $self->logprint( 'error',
                    "Error: Data column counts not found in any files!\n" );
                $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
                next COLUMN_COUNT;
            }
            if ( scalar( grep { defined $_ } values %$array_colno ) > 1 ) {
                $self->logprint( 'error',
                    "Warning: Files of type \'$type\' have inconsistent numbers of columns:\n"
                );
                $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
                foreach my $colcount ( keys %$array_colno ) {
                    $self->logprint( 'error',
                        "        $array_colno->{$colcount} files have $colcount columns;\n"
                    );
                }
            }
        }
    }

    return;
}

sub _check_exp_params : PRIVATE {

    my ( $self, $exp_filelist ) = @_;

    # List of parameters that can change
    my @ignore_exp_params = (
        'Sample Type',
        'Description',
        'Chip Type',
        'Chip Lot',
        'Scan Date',
        'Hybridize Date',
        'Station',
        'Module',
        'Operator',
    );

    # Get a general list of params, divided by section, and an example
    # value for each.
    my %paramlist;
    foreach my $file (@$exp_filelist) {
	while ( my ($section, $data) = each %{ $file->get_exp_data() } ) {
	    while ( my ($param, $value) = each %{ $data } ) {

                # We don't care which value ends up here, as long as it's in
                # at least one of the files.
                $paramlist{$section}{$param} = $value;
            }
        }
    }

    # Scan through the files looking for missing or different params
    my %inconsistencies;
    foreach my $file (@$exp_filelist) {
	while ( my ($sectionname, $data) = each %paramlist ) {

	    my $expsection = $file->get_exp_data()->{$sectionname};

	    PARAM:
	    while ( my ($param, $value) = each %{ $data } ) {

		next PARAM if (any { $param eq $_ } @ignore_exp_params);

                unless ( $expsection && exists($expsection->{$param})
		    && ( $expsection->{$param} eq $value ) ) {
                    $inconsistencies{$sectionname}{$param}++;
                }
                elsif ( $expsection && exists($expsection->{$param}) ) {
                    $data->{$param} = $expsection->{$param};
                }
            }
        }
    }
    return \%inconsistencies;
}

sub _check_features : PRIVATE {

    # Accepts refs to array of data features and hash of ADF features
    my ( $self, $data_features, $adf_features, $filename, $hyb_filename,
        $type )
        = @_;

    # This subroutine only deals with feature coordinates in Generic
    # MetaColumn/MetaRow format. GenePix Block/Column/Row formats are
    # converted into this in Datafile->parse_datafile().

    $type     ||= 'Identifier';
    $filename ||= q{};

    my @missing_features;

    foreach my $coord_key (@$data_features) {
        push( @missing_features, $coord_key )
            unless ( $adf_features->{$coord_key} );
    }

    # If there are any missing features, print them out
    if ( scalar @missing_features ) {
        $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
        $self->add_missing_features( $hyb_filename, \@missing_features );
        $self->logprint( 'error',
            "WARNING: ${type}s found in data file ${filename} which are missing from the ADF. See features log file.\n"
        );
    }

    return scalar @missing_features;    # Effectively an error code.
}

sub _check_duplicate_columns : PRIVATE {

    my ( $self, $file, $hyb_filename ) = @_;

    # Check for duplicate column headings
    my $headings    = $file->get_column_headings();
    my $hyb_ids     = $file->get_heading_hybs();
    my %is_indexcol = map { $_ => 1 } @{ $file->get_index_columns() };
    my %column_name_count;
    foreach my $i ( 0..$#{ $headings } ) {
	if ( $is_indexcol{$i} || ! defined $hyb_ids->[$i] ) {
	    $column_name_count{ $headings->[$i] }++;
	}
	else {
	    $column_name_count{ "$headings->[$i]($hyb_ids->[$i])" }++;
	}
    }
    my @duplicates;
    foreach my $heading ( keys %column_name_count ) {
        if ( $column_name_count{$heading} > 1 ) {

            push( @duplicates, $heading );

            # We have to take an average of the nulls in the column to
            # prevent negative % null values in our report
	    my $qt_metric = $file->get_data_metrics()->{$heading};
            if ( $qt_metric && defined( $qt_metric->{notnull} ) ) {
                $qt_metric->{notnull}
                    = $qt_metric->{notnull} / $column_name_count{$heading};
            }
        }
    }
    if (@duplicates) {
        $self->add_duplicate_columns( $hyb_filename, \@duplicates );
        $self->logprint(
            'error',
            "ERROR: File ",
            $file->get_name(),
            " contains ",
            scalar(@duplicates),
            " duplicated columns. See column headings log file for details.\n"
        );
        $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    return;
}

sub _check_duplicate_features : PRIVATE {

    my ( $self, $file, $feature_coords, $hyb_filename ) = @_;

    # Check for duplicate features in the data file
    my $dupe_feature_check;
    foreach my $feature (@$feature_coords) {
        $dupe_feature_check->{$feature}++;
    }

    my @features;
    if ( max( values %$dupe_feature_check ) > 1 ) {    # We've got trouble

        $self->logprint( 'error',
            "ERROR: Duplicate features found in data file ",
            $file->get_name(), ". See features log file.\n" );

        # FIXME so that reporters/CSs are not flagged in this way?
        $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
        foreach my $feature ( sort keys %$dupe_feature_check ) {
            push( @features, $feature )
                if ( $dupe_feature_check->{$feature} > 1 );
        }
        $self->add_duplicate_features( $hyb_filename, \@features );
    }

    return;
}

sub _compare_index_to_design : PRIVATE {

    my ( $self, $file, $feature_coords, $hyb_filename ) = @_;

    my $array_design = $file->get_array_design();

    # Check that there's actually an array design for this file (not
    # so in some Standalone mode checks).
    unless ( $array_design ) {
	$self->logprint(
	    'error',
	    "Warning: Data file not associated with an array design: ",
	    $file->get_name(),
	    ". Skipping identifier check.\n"
	);

	# Block downstream processing on no check
	$self->add_error( $CONFIG->get_ERROR_PARSEBAD() );

	return;
    }

    # Feature coordinates.  If only one index column, we assume it is an
    # identifier column (Reporter or CS).
    if ( scalar( @{ $file->get_index_columns() } ) == 1 ) {

        # Double-check that we've got what we think we've got.
        my $identifiers;
        my $allowed = $CONFIG->get_T2M_INDICES();
        my $identifier_heading
            = $file->get_column_headings()->[ $file->get_index_columns()->[0] ];

        if ( $identifier_heading =~ m/$allowed->{FGEM}[0]/i ) {
            $identifiers = $array_design->get_reporters();
        }
        elsif ($identifier_heading =~ m/$allowed->{FGEM_CS}[0]/i
            || $identifier_heading =~ m/$allowed->{AffyNorm}[0]/i
	    || $identifier_heading =~ m/$allowed->{GEO}[0]/i ) {
            $identifiers = $array_design->get_compseqs();
        }
        else {
            die("Internal script error: File ",
                $file->get_name(),
                " has unrecognized "
                    . $file->get_format_type()
                    . " column heading: $identifier_heading\n"
            );
        }

        my $num_identifiers = scalar( grep { defined $_ } values %$identifiers );
        if ($num_identifiers) {
            $self->_check_features(
                $feature_coords, $identifiers, $file->get_name(),
                $hyb_filename,   'Identifier'
            );

            # Check that at least 95% of the array identifiers are present.
            # We may want to relax this independently of Features, below.
            if ( $#$feature_coords < ( $num_identifiers * 0.95 ) ) {
                my $pc = ( $#$feature_coords / $num_identifiers ) * 100;
                $self->logprint(
                    'error',
                    sprintf(
                        "Warning: Data file %s has fewer identifiers than expected from the array design (%d%%).\n",
                        $file->get_name(), $pc
                    )
                );
                $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
            }

        }
        else {
            $self->logprint(
                'error',
                "Warning: No suitable design element identifiers retrieved for file ",
                $file->get_name(),
                ". Skipping identifier check.\n"
            );

            # Block downstream processing on no check
            $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
        }
    }

    elsif ( scalar( @{ $file->get_index_columns() } ) > 1 ) {

	my $adf_features = $array_design->get_features();

	my $num_adf_features
            = scalar( grep { defined $_ } values %{ $adf_features } );

	if ($num_adf_features) {
            my $rc = $self->_check_features(
		$feature_coords,
                $adf_features,
		$file->get_name(),
		$hyb_filename,
		'Feature'
	    );

            # Check that at least 95% of the array features are present
            if ( $#$feature_coords < ( $num_adf_features * 0.95 ) ) {
                my $pc = ( $#$feature_coords / $num_adf_features ) * 100;
                $self->logprint(
                    'error',
                    sprintf(
                        "Warning: Data file %s has fewer features than expected from the array design (%d%%).\n",
                        $file->get_name(), $pc
                    )
                );
                $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
            }

        }
        else {
            $self->logprint( 'error',
                "Warning: No ADF features retrieved for file ",
                $file->get_name(), ". Skipping feature check.\n" );

	    # No checks? not good to go, then
            $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
        }
    }

    else {           # This really, really should never happen
        die( "Internal script error: file ",
            $file->get_name(), " has a negative number of index columns!\n" );
    }

    return;
}

sub check_date_format : RESTRICTED {

    # Check date against the AE required format, generate warnings if bad.
    my ( $self, $date, $type ) = @_;

    my $AE_dateformat = qr/\A \d{4}-\d{2}-\d{2} \z/xms;

    my $datetype = defined ( $type )
	         ? "Experiment $type date"
	         : 'Date';

    if ( $date ) {
	if ( $date !~ $AE_dateformat ) {
	    $self->logprint(
		'error',
		"Warning: $datetype ($date) not in ArrayExpress format (YYYY-MM-DD).\n",
	    );
	    $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
	}
    }
    else {
        $self->logprint(
	    'error',
            "Warning: $datetype has not been set.\n",
	);
        $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
    }

    return;
}

sub check_mage_bmchars : RESTRICTED {

    # Check a list of MAGE BioMaterials to assess whether they're
    # sufficiently annotated.

    my ( $self, $bm_list ) = @_;

    foreach my $material ( @{ $bm_list || [] } ) {
        my $bmc_list = $material->getCharacteristics();

        # Less than 3 is the warning level (arbitrary)
        if ( ! $bmc_list || ( scalar @$bmc_list < 3 ) ) {
            $self->logprint(
		'error',
		sprintf(
		    qq{Warning: Material "%s" may not be fully annotated.\n},
		    $material->getName(),
		),
	    );
            $self->add_error( $CONFIG->get_ERROR_MIAME() );
        }
    }

    return;
}

sub check_mage_labeledextracts : RESTRICTED {

    # Check that all LabeledExtracts have an associated Label.

    my ( $self, $les ) = @_;

    foreach my $le ( @{ $les } ) {
	my $labels = $le->getLabels() || [];

	unless ( scalar @{ $labels } ) {
	    $self->logprint(
		'error',
		sprintf(
		    qq{Warning: Labeled Extract "%s" is not associated with a Label.\n},
		    $le->getName(),
		),
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
	}
    }

    return;
}

sub generate_workflow_from_bags : RESTRICTED {

    my ( $self, $bags, $classes ) = @_;

    # Generate a very basic workflow from the mage bags.
    # $classes is an arrayref of single-element hashrefs.

    # Set a decent page margin at which to start.
    my $margin = 1;

    while ( my $instance = shift @{ $classes || [] } ) {

	# NB one element only here!
        my ( $thing, $name ) = each %$instance;

        my $count = $#{ $bags->{$thing}->() } + 1;

        if ($count) {
            $self->logprint(
		'workflow',
		( ( "  " x $margin ), "$count $name --> \n\n" ),
	    );
            $margin++;
        }
    }

    $self->logprint(
	'workflow',
	( ( "  " x $margin ), "Done.\n" ),
    );

    return;
}

sub check_mage_treatments_protapps : RESTRICTED {

    my ( $self, $materials, $type ) = @_;

    foreach my $obj ( @{ $materials } ) {
	foreach my $treatment ( @{ $obj->getTreatments() || [] } ) {

	    my $found;
	    foreach my $pa ( @{ $treatment->getProtocolApplications() || [] } ) {
		$found++ if $pa->getProtocol();
	    }
	    unless ( $found ) {
		my $actionvalue = $treatment->getAction()->getValue();
		$self->logprint(
		    'error',
		    sprintf(
			qq{Warning: %s "%s" lacks a %s treatment protocol.\n},
			$type,
			$obj->getName(),
			$actionvalue,
		    ),
		);
		$self->add_error( $CONFIG->get_ERROR_MIAME() );
	    }
        }
    }

    return;
}

sub check_mage_hyb_protapps : RESTRICTED {

    my ( $self, $pbas ) = @_;

    foreach my $pba ( @{ $pbas } ) {

        # Check hyb protocol.
	if ( my $bac = $pba->getBioAssayCreation() ) {
	    my $found;
            foreach my $pa ( @{ $bac->getProtocolApplications() || [] } ) {
		$found++ if $pa->getProtocol();
	    }
	    unless ( $found ) {
		$self->logprint(
		    'error',
		    sprintf(
			qq{Warning: Hybridization "%s" lacks a hybridization protocol.\n},
			$pba->getName(),
		    ),
		);
		$self->add_error( $CONFIG->get_ERROR_MIAME() );
            }
        }
        else {

	    # This is actually quite serious, as all hyb PBAs _must_ have a BAC.
            $self->logprint(
		'error',
		qq{Warning: Hybridization PBA lacks a BioAssayCreation object.\n},
	    );
            $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
        }
    }

    return;
}

sub compare_fvs_to_bmcs : RESTRICTED {

    my ( $self, $fvhashlist, $bmchashlist ) = @_;

    # Takes lists of hashrefs, each hashref containing a single
    # category => value pair, compares them and logs the results if
    # there's a BMC variable not reflected in the FVs.

    my %listed_as_variable;
    foreach my $pair ( @{ $fvhashlist } ) {
	while ( my ( $cat, $val ) = each %{ $pair } ) {
	    $listed_as_variable{$cat} = 1;
	}
    }

    my %bmc_valuecount;
    foreach my $pair ( @{ $bmchashlist } ) {
	while ( my ( $cat, $val ) = each %{ $pair } ) {
	    $bmc_valuecount{$cat}{$val}++;
	}
    }

    while ( my ( $cat, $valhash ) = each %bmc_valuecount ) {

	# More than one value constitutes a variable.
	if ( scalar(grep { defined $_ } values %{ $valhash }) > 1 ) {
	    unless ( $listed_as_variable{$cat} ) {
		$self->logprint(
		    'error',
		    qq{Warning: Experimental variable "$cat" is not listed as a FactorValue.\n},
		);
		$self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
	    }
	}
    }

    return;
}

1;
