#!/usr/bin/env perl
#
# Core module for ArrayExpress MAGE-TAB to MAGE-ML 
# implementation.
#
# Tim Rayner, 2007, EMBL-EBI Microarray Informatics Team
#
# $Id: MAGETAB.pm 2072 2008-06-04 15:08:35Z tfrayner $

package ArrayExpress::MAGETAB;

use strict;
use warnings;

use Class::Std;
use Carp;
use File::Spec;
use File::Path;
use File::Basename;
use Text::CSV_XS;

require ArrayExpress::MAGETAB::IDF;
require ArrayExpress::MAGETAB::SDRF;
require ArrayExpress::MAGETAB::DataMatrix;
require ArrayExpress::Datafile::Parser;

use ArrayExpress::Curator::MAGE qw(write_mage unique_identifier postprocess_mage);
use ArrayExpress::Curator::Common qw(check_linebreaks);
use ArrayExpress::Curator::Config qw($CONFIG);

use base qw(ArrayExpress::Curator::Logger);

my %magetab_doc   : ATTR( :name<magetab_doc>,                       :default<undef> );
my %idf           : ATTR( :name<idf>,                               :default<undef> );
my %sdrfs         : ATTR( :get<sdrfs>,         :set<sdrfs>,         :default<[]>    );
my %data_matrices : ATTR( :get<data_matrices>, :set<data_matrices>, :default<[]>    );
my %output_file   : ATTR( :name<output_file>,                       :default<[]>    );
my %mage          : ATTR( :get<mage>,          :set<mage>,          :default<undef> );
my %namespace     : ATTR( :name<namespace>,    :default<q{MAGETabulator}> );
my %authority     : ATTR( :name<authority>,    :default<q{ebi.ac.uk}> );
my %expt_accession    : ATTR( :name<expt_accession>,             :default<undef> );

# Allow API users to pass in a prot accn service of their own.
my %prot_accn_service : ATTR( :name<protocol_accession_service>, :default<undef> );
my %prot_accn_prefix  : ATTR( :name<protocol_accession_prefix>,  :default<undef> );

my %ded_filehandle    : ATTR( :get<ded_filehandle>,   :set<ded_filehandle>,   :default<undef> );
my %ded_count         : ATTR( :get<ded_count>,        :set<ded_count>,        :default<undef> );
my %native_filetypes  : ATTR( :get<native_filetypes>, :set<native_filetypes>, :default<undef> );
my %bags              : ATTR( :name<bags>,    :default<{}> );
my %clobber           : ATTR( :name<clobber>, :default<0>  );
my %target_directory     : ATTR( :name<target_directory>,   :init_arg<target_directory>, );
my %source_directory     : ATTR( :get<source_directory>,    :init_arg<source_directory>,    :default<undef> );
my %is_standalone        : ATTR( :get<is_standalone>,       :init_arg<is_standalone>,       :default<0> );
my %qt_filename          : ATTR( :get<qt_filename>,         :init_arg<qt_filename>,         :default<undef> );
my %keep_all_qts         : ATTR( :get<keep_all_qts>,        :init_arg<keep_all_qts>,        :default<undef> );
my %keep_protocol_accns  : ATTR( :get<keep_protocol_accns>, :init_arg<keep_protocol_accns>, :default<undef> );
my %include_default_qts  : ATTR( :get<include_default_qts>, :init_arg<include_default_qts>, :default<undef> );
my %reporter_prefix      : ATTR( :get<reporter_prefix>,     :init_arg<reporter_prefix>,     :default<undef> );
my %compseq_prefix       : ATTR( :get<compseq_prefix>,      :init_arg<compseq_prefix>,      :default<undef> );
my %use_plain_text       : ATTR( :get<use_plain_text>,      :init_arg<use_plain_text>,      :default<undef> );
my %skip_datafiles       : ATTR( :get<skip_datafiles>,      :init_arg<skip_datafiles>,      :default<undef> );
my %in_relaxed_mode      : ATTR( :name<in_relaxed_mode>,    :default<0> );
my %ignore_size_limits   : ATTR( :name<ignore_size_limits>, :default<undef> );

# Make this visible to users of the module.
our $VERSION = 1.1;

sub START {

    my ( $self, $id, $args ) = @_;

    croak("Error: no experiment accession set.")
	unless defined( $expt_accession{$id} );

    croak("Error: no target directory set.")
	unless defined( $target_directory{$id} );

    croak("Error: no IDF/MAGE-TAB doc filename set.")
	unless (defined( $idf{$id} ) || defined( $magetab_doc{$id} ) );

    # Create the target directory, if not already present:
    mkpath( $target_directory{$id}, undef, oct(777) );

    # If protocol accession service is defined, accession prefix must
    # be also. Ignore all this if we're not going to be using it
    # anyway (e.g. during validation).
    unless ( $self->get_keep_protocol_accns() ) {
        if ( (    $CONFIG->get_AUTOSUBS_DSN()
	       || $self->get_protocol_accession_service() )
	     && ! defined( $self->get_protocol_accession_prefix() ) ) {
	    croak("Error: protocol accession service is active"
	        . " but no accession prefix set.");
	}
    }

    $self->set_progname( 'MAGETAB' );
    $self->set_version( $VERSION );

    $self->initialize_logfiles();

    return;
}

sub preprocess_magetab_doc {

    # Class method used to parse linebreaks and split a combined
    # MAGE-TAB (IDF+SDRF) document into its IDF and SDRF components.
    my ( $class, $doc ) = @_;

    # Check linebreaks here.
    my ( $eols, $eol_char ) = check_linebreaks( $doc );
    unless ( $eol_char ) {
	croak(
	    sprintf(
		"Error: Cannot correctly parse linebreaks in file %s"
		    . " (%s unix, %s dos, %s mac)\n",
		$doc,
		$eols->{unix},
		$eols->{dos},
		$eols->{mac},
	    )
	);
    }

    if (    ( $eol_char eq "\015" )
         && ( $Text::CSV_XS::VERSION < 0.27 ) ) {

	# Mac linebreaks not supported by older versions of Text::CSV_XS.
	croak("Error: Mac linebreaks not supported by this version"
	    . " of Text::CSV_XS. Please upgrade to version 0.27 or higher.\n");
    }

    # Split the file here.
    my $idf_fh  = IO::File->new_tmpfile();
    my $sdrf_fh = IO::File->new_tmpfile();
    my ($is_idf, $is_sdrf);

    open( my $fh, '<', $doc )
	or croak(qq{Error: Unable to open MAGETAB document "$doc": $!});

    LINE:
    while ( my $line = <$fh> ) {
	if ( $line =~ /\A \"? \[IDF\] /ixms ) {
	    $is_idf  = 1;
	    next LINE;
	}
	if ( $line =~ /\A \"? \[SDRF\] /ixms ) {
	    $is_sdrf = 1;
	    next LINE;
	}

	if ( $is_idf && ! $is_sdrf ) {
	    print $idf_fh $line;
	}
	elsif ( $is_idf && $is_sdrf ) {
	    print $sdrf_fh $line;
	}
    }

    close( $fh ) or croak(qq{Error: Unable to close MAGETAB document "$doc": $!});
    
    unless ( $is_idf && $is_sdrf ) {
	croak("Error: Document must contain an [IDF] "
	    . "section and an [SDRF] section, in that order.");
    }
    seek($idf_fh,  0, 0) or die("Error rewinding temporary IDF file: $!");
    seek($sdrf_fh, 0, 0) or die("Error rewinding temporary SDRF file: $!");

    return ($idf_fh, $sdrf_fh, $eol_char);
}

sub parse_magetab_doc {

    my ( $self ) = @_;

    unless ( $self->get_magetab_doc() ) {
	croak("Error: MAGETAB::parse_magetab_doc called without setting filename.");
    }

    $self->logprint('magetab',
		    sprintf("Processing MAGE-TAB doc: %s\n\n", $self->get_magetab_doc()));

    # Split the file into IDF and SDRF, and determine the eol_char.
    my ( $idf_fh, $sdrf_fh, $eol_char )
	= __PACKAGE__->preprocess_magetab_doc( $self->get_magetab_doc() );

    # Parse the IDF component.
    my $idf_parser = ArrayExpress::MAGETAB::IDF->new({
	idf_filehandle             => $idf_fh,
	eol_char                   => $eol_char,
	namespace                  => $self->get_namespace(),
	authority                  => $self->get_authority(),
	expt_accession             => $self->get_expt_accession(),
	in_relaxed_mode            => $self->get_in_relaxed_mode(),
	source_directory           => $self->get_source_directory(),
    });
    my $experiment = $self->parse_idf( $idf_parser );

    # Parse the SDRF component.
    my $sdrf_parser = ArrayExpress::MAGETAB::SDRF->new({
	sdrf_filehandle            => $sdrf_fh,
	eol_char                   => $eol_char,
	namespace                  => $self->get_namespace(),
	authority                  => $self->get_authority(),
	expt_accession             => $self->get_expt_accession(),
	bags                       => $idf_parser->get_bags(),
	source_directory           => $self->get_source_directory(),
	in_relaxed_mode            => $self->get_in_relaxed_mode(),
	skip_datafiles             => $self->get_skip_datafiles(),
    });

    my $datafiles = $self->parse_sdrf( $sdrf_parser );
    $self->add_all_mage_objects( $experiment, $sdrf_parser );

    return wantarray
	   ? ( $datafiles, $self->get_bags(), $self->get_mage() )
	   : $self->get_mage();
}

sub parse {

    my ( $self ) = @_;

    unless ( $self->get_idf() ) {
	croak("Error: MAGETAB::parse called without setting IDF filename.");
    }

    $self->logprint('magetab',
		    sprintf("Processing IDF: %s\n\n", $self->get_idf()));

    my $idf_parser = ArrayExpress::MAGETAB::IDF->new({
	idf                        => $self->get_idf(),
	namespace                  => $self->get_namespace(),
	authority                  => $self->get_authority(),
	expt_accession             => $self->get_expt_accession(),
	in_relaxed_mode            => $self->get_in_relaxed_mode(),
	source_directory           => $self->get_source_directory(),
    });

    my $experiment = $self->parse_idf( $idf_parser );

    my $sdrfs = $self->get_sdrfs();
    my $mage  = $self->get_mage();

    # FIXME parse the SDRFS etc. here
    foreach my $sdrf_file (@$sdrfs) {

	# FIXME stitch the SDRFs together here.

    }

    my $datafiles = [];
    if ( scalar @$sdrfs ) {

	# FIXME only one SDRF supported for now:
	my $sdrf_file = $sdrfs->[0];  # FIXME delete this line when ready.

	$self->logprint('magetab',
			sprintf("Processing SDRF: %s\n\n", $sdrf_file));

	my $sdrf_parser = ArrayExpress::MAGETAB::SDRF->new({
	    sdrf                       => $sdrf_file,
	    namespace                  => $self->get_namespace(),
	    authority                  => $self->get_authority(),
	    expt_accession             => $self->get_expt_accession(),
	    bags                       => $idf_parser->get_bags(),
	    source_directory           => $self->get_source_directory(),
	    in_relaxed_mode            => $self->get_in_relaxed_mode(),
	    skip_datafiles             => $self->get_skip_datafiles(),
	});

	$datafiles = $self->parse_sdrf( $sdrf_parser );
	$self->add_all_mage_objects( $experiment, $sdrf_parser );
    }

    return wantarray
	   ? ( $datafiles, $self->get_bags(), $self->get_mage() )
	   : $self->get_mage();
}

sub write_mageml {

    my ( $self ) = @_;

    my $output_file;
    unless ( $output_file = $self->get_output_file() ) {
	croak("Error: called write_mageml without setting output_file.");
    }

    if ( my $target = $self->get_target_directory() ) {
	$output_file = File::Spec->catfile( $target, basename($output_file) );
    }

    my $mage;
    unless ( $mage = $self->get_mage() ) {
	$mage = $self->parse();
    }

    # Construct the MAGE structure
    my $mage_identifier = sprintf(
	"MAGE:%s:%s v%s:%s",
	$self->get_authority(),
	$self->get_namespace(),
	$VERSION,
	$self->get_expt_accession(),
    );
    $mage->identifier( $mage_identifier );

    # Process any external DesignElementDimension filehandles.
    my ( $ded_fh, $dummy_id );
    if ( $self->get_ded_count() ) {

	$ded_fh = $self->get_ded_filehandle();

	# Create a placeholder for the insertion of DEDs
	$dummy_id = 'DUMMY';
	my $dummy_FD = Bio::MAGE::BioAssayData::FeatureDimension->new(
	    identifier => $dummy_id,
	    name       => $dummy_id,
	);
	$mage->add_objects( [$dummy_FD] );
    }
    else {
	print STDOUT ("No DesignElementDimension found.\n");
    }

    # Write out everything but the DED
    warn ("Writing out initial MAGE-ML...\n");

    my $mage_version = 1.1;
    my $data_format  = 'tab delimited';
    my $mage_fh = IO::File->new_tmpfile;
    write_mage( $mage, $mage_fh, $data_format, $mage_version );

    # Rewind the temporary filehandle
    seek( $mage_fh, 0, 0 );

    # Fix the DED using classical text processing (lower memory overhead)
    print STDOUT ("Inserting DesignElementDimension where found...\n");
    my $output_fh = IO::File->new( $output_file, '>' )
	or die(
	    sprintf(
		"Error: cannot open output file %s: $!\n",
		$output_file,
	    ));

    postprocess_mage(
	$ded_fh,
	$mage_fh,
	$output_fh,
	$dummy_id,
	$self->get_native_filetypes(),
    );

    return;
}

sub get_id_prefix : RESTRICTED {  # FIXME consider ways to combine
                                  # this with the same TabFile method.

    my ( $self ) = @_;

    return sprintf( "%s:%s", $self->get_authority(), $self->get_namespace() );
}

###################
# Private methods #
###################

sub initialize_logfiles : PRIVATE {

    my ( $self ) = @_;

    # Default to using the IDF filename; only one of these should be
    # set anyway.
    my $sourcefile = $self->get_idf() || $self->get_magetab_doc();

    # Sort out our log files.
    my $logfile_string = File::Spec->catfile(
	$self->get_target_directory(),
	basename($sourcefile),
    );
    $logfile_string =~ s/\.\w{3,4}$//;     # strip off the extension
    my ( $vol, $dir, $name ) = File::Spec->splitpath($logfile_string);
    $self->localize_logfiles({
	directory => $dir,
	volume    => $vol,
    });

    return;
}

sub parse_idf : PRIVATE {

    my ( $self, $idf_parser ) = @_;

    # Add the protocol accession reassignment service if we've got
    # one.
    if ( my $service = $self->generate_prot_accn_service() ) {
	$idf_parser->set_protocol_accession_service( $service );
    }

    my ( $mage, $experiment, $sdrfs ) = $idf_parser->parse();

    $self->set_sdrfs($sdrfs || []);
    $self->set_mage($mage);

    return $experiment;
}

sub parse_sdrf : PRIVATE {

    my ( $self, $sdrf_parser ) = @_;

    my @datafiles = @{ $sdrf_parser->parse() };

    $self->set_bags( $sdrf_parser->get_bags() );

    # Process the datafiles here.
    if ( $self->get_skip_datafiles() ) {
	$self->logprint('magetab', "Skipping Data files...\n\n");

	# E.g. CEL, CHP; recognised solely by filename extension.
	$self->set_native_filetypes(
	    $sdrf_parser->get_native_filetypes(),
	);
    }
    else {
	$self->logprint('magetab', "Processing Data files...\n\n");

	$self->encode_datafiles( \@datafiles );
    }

    return \@datafiles;
}

sub add_all_mage_objects : PRIVATE {

    my ( $self, $experiment, $sdrf_parser ) = @_;

    my $mage = $self->get_mage();

    foreach my $bag ( values %{ $self->get_bags() } ) {
	$mage->add_objects( $bag->() );
    }

    $experiment->setBioAssays(    $sdrf_parser->get_all_bioassays()    );
    $experiment->setBioAssayData( $sdrf_parser->get_all_bioassaydata() );

    $self->set_mage($mage);

    return;
}

sub encode_datafiles : PRIVATE {

    my ( $self, $datafiles ) = @_;

    my $parser = ArrayExpress::Datafile::Parser->new({
	namespace            => $self->get_id_prefix() . ":" . $self->get_expt_accession(),
	include_known_qts    => $self->get_include_default_qts(),
	quantitation_types   => $self->get_qt_filename(),
	use_binary_datafiles => ! $self->get_use_plain_text(),
	is_standalone        => $self->get_is_standalone(),
	output_directory     => $self->get_target_directory(),
	source_directory     => $self->get_source_directory(),
	error_fh             => $self->log_fh('magetab'),
	allow_undef_qts      => $self->get_keep_all_qts(),
	protocol_bag         => $self->get_bags->{protocol},
	parameter_bag        => $self->get_bags->{parameter},
	reporter_prefix      => $self->get_reporter_prefix(),
	compseq_prefix       => $self->get_compseq_prefix(),
	ignore_size_limits   => $self->get_ignore_size_limits(),
	clobber              => $self->get_clobber(),
    });

    foreach my $file ( @{ $datafiles } ) {

	# Parse the file, generate the DED and extract QTD
	# info. Output the stripped data file.
	$parser->parse( $file );

	# If the parser has reset clobber, note it here.
	$self->set_clobber( $parser->get_clobber() );

	my $data = $file->get_mage_badata();

	# DesignElementDimension.
	if ( $file->get_ded_type() ) {

	    my $dim_class = sprintf("Bio::MAGE::BioAssayData::%sDimension",
				    $file->get_ded_type());

	    my $ded;
	    eval {
		$ded = $dim_class->new(
		    identifier => $file->get_ded_identifier(),
		);
	    } or croak(
		sprintf("Error: Unrecognized DED type: %s\n",
			$file->get_ded_type())
	    );
	    $data->setDesignElementDimension($ded) if $data;
	}

	# Process data matrices and per-hyb data a little differently.
	if (   $file->get_data_type() eq $CONFIG->get_RAW_DM_FILE_TYPE()
	    || $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE() ) {

	    # Process this later, after all the per-hyb objects have
	    # been created.
	    my $identifier_template
		= sprintf ("%s:%s",
			   $self->get_id_prefix(),
			   $self->get_expt_accession());

	    my $dm = ArrayExpress::MAGETAB::DataMatrix->new({
		datafile    => $file,
		id_template => $identifier_template,
		mage_helper => $self,
	    });
	    $dm->create_mage();
	}
	else {

	    # Process file as per-hyb data.
	    # QuantitationType and QuantitationTypeDimension.
	    my ( $qtlist, $qtd_key ) = $self->qts_from_names( $file );
	    
	    my $qtd = $self->get_bags->{qtd}->(
		$qtd_key,
		{   qt_list             => $qtlist,
		    identifier_template => sprintf("%s:%s.%s",
						   $self->get_id_prefix(),
						   $self->get_expt_accession(),
						   unique_identifier()),
		}
	    );
	    $file->set_mage_qtd($qtd);
	    $data->setQuantitationTypeDimension($qtd) if $data;
	}
    }

    # Store DesignElementDimension and native file info for later
    # writing out as MAGE-ML in the postprocessing step.
    $self->set_ded_filehandle(   $parser->get_feature_fh() );
    $self->set_ded_count(        $parser->get_ded_count()  );
    $self->set_native_filetypes( $parser->get_cel_types()  );

    return;
}

sub qts_from_names {

    my ( $self, $file, $qt_names ) = @_;

    my $bag_of = $self->get_bags();

    $qt_names ||= $file->get_column_headings();

    my ( @qtd_qtlist, $qtd_key );
    my ( $QT_prefix, $software );

    # FIXME maybe either use dm_chip_type or qt_type here to detect
    # alternative Affy data (e.g. data matrix). At the moment Affy
    # data matrix QTs are non-standard format.
    if ( $file->get_format_type() eq 'Affymetrix' ) {
        $QT_prefix = 'Affymetrix:QuantitationType:';
	$software  = 'Affymetrix';
    }
    elsif ( my ( $type, $qt_namespace )
        = ( $file->get_qt_type =~ m{\A (.*?) \[ (.*) \] \z}xms ) ) {
        $QT_prefix = "${qt_namespace}:${type}_QuantitationType:";
	$software  = $type;
    }
    else {
        $QT_prefix = sprintf('Unknown:%s_QuantitationType:', $file->get_qt_type());
	$software  = $file->get_qt_type() eq 'Ambiguous'
	           ? 'Unknown'
		   : $file->get_qt_type();
    }

    foreach my $heading (@$qt_names) {

        my $identifier       = $QT_prefix . $heading;
        my $quantitationtype = $bag_of->{quantitationtype}->(
            $identifier,
            {   identifier   => $identifier,
                prefix       => $QT_prefix,
                name         => $heading,
		software     => $software,
                data_metrics => $file->get_data_metrics(),
                container    => $bag_of->{quantitationtype},
            }
        );
        push( @qtd_qtlist, $quantitationtype );

        $qtd_key .= $heading;

    }

    # Quick check for sane QTs (This error should no longer be triggered
    # by missing confidence indicator targets - see MAGE.pm).
    foreach my $qt ( @{ $bag_of->{quantitationtype}->() } ) {
        unless ( $qt->getIdentifier ) {
            print STDERR (<<'END_ERROR');
ERROR: At least one QuantitationType found lacking an identifier.
Please check that the confidence indicator targets in your QT file 
are represented in the data files.

END_ERROR

            croak(    qq{Error occurred at data file }
                    . $file->get_name()
                    . q{QT name: "}
                    . $qt->getName()
                    . q{"} );
        }
    }

    $qtd_key ||= 'NULL';    # In case of no QTs, e.g. missing file

    return ( \@qtd_qtlist, $qtd_key );
}
    
sub generate_prot_accn_service : PRIVATE {

    # If configured to do so will return a coderef suitable for
    # passing to MAGETAB::IDF::protocol_accession_service.
    my ( $self ) = @_;

    # Skip this if we want to keep our original accessions.
    return if $self->get_keep_protocol_accns();

    # Use user-specified service if available.
    if ( my $coderef = $self->get_protocol_accession_service() ) {
	return $coderef;
    }

    if ( $CONFIG->get_AUTOSUBS_DSN() ) {

	require ArrayExpress::AutoSubmission::DB::Protocol;

	ArrayExpress::AutoSubmission::DB::Protocol->accession_prefix(
	    $self->get_protocol_accession_prefix(),
	);

	return sub {

	    my ($user_accession, $expt_accession) = @_;
	    warn(
		sprintf(
		"Assigning accession for protocol %s using autosubmission system...\n",
		$user_accession,
		)
	    );

	    # Arguments are user_accession, expt_accession, protocol name.
	    my $new_accession
		= ArrayExpress::AutoSubmission::DB::Protocol->reassign_protocol(
		$user_accession,
		$expt_accession,
		$user_accession,
	    );

	    return $new_accession;
	}
    }
    else {
	return;
    }
}

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: MAGETAB.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::MAGETAB - A parser class for MAGE-TAB documents.

=head1 SYNOPSIS

 use ArrayExpress::MAGETAB;
 my $magetab = ArrayExpress::MAGETAB->new({
     idf              => $idf_file,
     target_directory => $dir,
     expt_accession   => $accn,
 });

 $magetab->write_mageml();

=head1 DESCRIPTION

This module acts as a front end to the MAGE-TAB parsing API. Parser
objects are instantiated with a number of attributes to control how
the MAGE-TAB document is parsed. Support is only provided for IDF and
SDRF documents at present; it is anticipated that the parser will be
extended to support ADF at a later date.

Currently the parser is built using a MAGEv1.1 object model to store
the MAGE-TAB metadata. It is envisioned that this dependence on the
Bio::MAGE modules may be removed once a full MAGE-TAB object model is
agreed upon by the community.

To simplify the process of data submission, ArrayExpress has
introduced a new flavour of MAGE-TAB in which the IDF and SDRF
sections are combined into a single worksheet. This parser supports
both MAGE-TAB v1.1 documents (with separate IDF and SDRF) and these
combined documents.

=head1 METHODS

=over 2

=item C<new>

Object constructor. This recognises the following attributes:

=over 4

=item C<idf>

The path of the IDF file with which to start parsing.

=item C<magetab_doc>

The path of a combined IDF+SDRF file to parse.

=item C<output_file>

The name of the output MAGE-ML file.

=item C<namespace>

The namespace to use in MAGE identifier creation.

=item C<authority>

The authority to use in MAGE identifier creation.

=item C<expt_accession>

The accession number assigned to the experiment.

=item C<target_directory>

The directory into which to write the output files.

=item C<source_directory>

The directory which contains all data and SDRF files.

=item C<is_standalone>

A flag indicating whether the script is able to connect to
ArrayExpress to retrieve array design information. It is sometimes
desirable to skip these downloads, which can be quite large.

=item C<qt_filename>

QuantitationType file. This option allows you to specify a custom
QuantitationType definition file to override those defined as part of
the Tab2MAGE package. See L<ArrayExpress::Datafile::QT_list> for more
information.

=item C<include_default_qts>

This option can be used in conjunction with C<qt_filename> to indicate
that the QuantitationType listing from the Tab2MAGE package itself
should be included in the lists of known QuantitationTypes used in
data file parsing. The default behaviour is to deactivate these known
QTs if a custom QT file is to be used.

=item C<keep_all_qts>

A flag indicating whether unrecognised QuantitationTypes in data files
should be kept or not. The default behaviour is to strip unrecognised
columns out of the data files.

=item C<reporter_prefix>

The prefix to be used during Reporter identifier construction. This
prefix is prepended to the identifiers listed in the data files.

=item C<compseq_prefix>

The prefix to be used during CompositeElement (CompositeSequence)
identifier construction. This prefix is prepended to the identifiers
listed in the data files.

=item C<protocol_accession_service>

A code reference used to reassign protocol accessions. See
L<PROTOCOL_ACCESSIONS>, below.

=item C<protocol_accession_prefix>

The prefix to be used for protocol accession creation, when the
autosubmissions system is in use. See L<PROTOCOL_ACCESSIONS>, below.

=item C<keep_protocol_accns>

A flag indicating whether the protocols in the IDF should be assigned
new accession numbers. This option overrides C<protocol_accession_service>.

=item C<use_plain_text>

Some file formats are only supported in their native forms by
ArrayExpress. Nonetheless, this package can parse some of these data
formats into tab-delimited representations fully encoded in the
MAGE-ML document (examples include Nimblegen data, Affymetrix CEL
files).

=item C<skip_datafiles>

This option tells the parser to skip attempting to read the data files
referenced by a given MAGE-TAB document, and instead attempts to
generate MAGE in their absence. This option is particularly useful for
unsupported data file formats.

=item C<ignore_size_limits>

The Tab2MAGE configuration file allows the user to set maximum data
file sizes for parsing and web download, to provide some protection
from overloading the system in a production pipeline setting. To
temporarily ignore these size limits, use this option.

=item C<in_relaxed_mode>

A flag indicating whether to allow minor errors during parsing. At the
moment the only errors which are ignored by this option are C<Term
Source REF>, C<Protocol REF>, C<Parameter Value []> and C<Factor Value
[]> columns which reference Names which have not been defined in the
IDF.

=item C<clobber>

Flag indicating whether or not to overwrite existing files without
prompting the user.

=back

=item C<parse>

Starts the MAGE-TAB parse and loads the document into memory.

=item C<write_mageml>

Writes out MAGE-ML corresponding to the input MAGE-TAB document. If
the MAGE-TAB has not yet been parsed, C<parse()> is called
automatically.

=back

=head1 PROTOCOL_ACCESSIONS

The parser provides a set of callbacks which can be used to assign
MAGE-TAB Protocol Names to unique accessions at the point of
parsing. If the autosubmissions system has been set up and configured,
then the parser will default to using that mechanism to assign
protocol accessions. If you wish to use your own service, you may use
the C<protocol_accession_service> and C<protocol_accession_prefix>
attributes to control this. The C<protocol_accession_service> should
point to a code reference which will accept two arguments: (a) the
Protocol Name as given in the IDF, and (b) the experiment
accession. The code reference should return a unique accession which
will then be assigned to the protocol in the output MAGE-ML.

If the autosubmissions system is to be used, the
C<protocol_accession_prefix> attribute must be set, e.g. to "P-MTAB-".

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2008.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

1;
