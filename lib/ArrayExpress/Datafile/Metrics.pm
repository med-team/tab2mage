#!/usr/bin/env perl
#
# DataMetrics.pm - a module derived from and used in the experiment
# checker script. Contains routines which might be useful elsewhere.
#
# Tim Rayner 2005 ArrayExpress team, EBI
#
# $Id: Metrics.pm 1852 2007-12-13 10:14:27Z tfrayner $
#

package ArrayExpress::Datafile::Metrics;

use strict;
use warnings;

use Carp;
use Scalar::Util qw(looks_like_number);
use ArrayExpress::Curator::Config qw($CONFIG);

use base 'Exporter';
our @EXPORT_OK = qw(
    check_data_metrics
    calculate_benford
    pearson_correlation
);

## Correlation subroutine originally written by Greg Grant from RAD
sub pearson_correlation {

    my ( $hash1, $hash2 ) = @_;

    ref $hash1 eq 'HASH' or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $hash2 eq 'HASH' or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my @vector1;
    my @vector2;
    my $correlation;

    my $i = 0;

    # If both datasets have a value that looks like a number, use it in
    # the correlation calculation.
    foreach my $key ( keys %$hash2 ) {
        if (   defined $hash1->{$key}
            && looks_like_number( $hash1->{$key} )
            && looks_like_number( $hash2->{$key} ) ) {
            $vector1[$i] = $hash1->{$key};
            $vector2[$i] = $hash2->{$key};
            $i++;
        }
    }

    my $vector_length = $i - 1;

    my $xy = 0;
    my $x  = 0;
    my $y  = 0;
    my $xx = 0;
    my $yy = 0;

    for ( my $i = 0; $i < $vector_length; $i++ ) {
        $xy = $xy + $vector1[$i] * $vector2[$i];
        $x  = $x + $vector1[$i];
        $y  = $y + $vector2[$i];
        $xx = $xx + ( $vector1[$i] )**2;
        $yy = $yy + ( $vector2[$i] )**2;
    }

    if (sqrt(
            ( $vector_length * $xx - $x * $x )
            * ( $vector_length * $yy - $y * $y )
        ) != 0
        ) {

        $correlation = ( $vector_length * $xy - $x * $y ) / (
            sqrt(
                ( $vector_length * $xx - $x * $x )
                * ( $vector_length * $yy - $y * $y )
            )
        );

    }
    else {

        $correlation = -2;

    }

    return $correlation;

}

sub calculate_benford {

    my $data_metrics = shift;

    ref $data_metrics eq 'HASH'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my @benford;
    foreach my $heading ( keys %$data_metrics ) {
        for ( my $digit = 1; $digit < 10; $digit++ ) {
            $benford[$digit] +=
                $data_metrics->{$heading}{benford}
                ? vec( $data_metrics->{$heading}{benford}, $digit, 32 )
                : 0;
        }
    }

    my $bentotal = 0;
    foreach my $digitcount ( @benford[ 1 .. 9 ] ) {
        $bentotal += $digitcount if $digitcount;
    }
    my $benval = $bentotal
        ? ( $benford[1] / $bentotal ) * 100
        : 'N/A';   # Avoid illegal division by zero if we've got real problems

    return $benval;

}

sub check_data_metrics {

    my $data_metrics = shift;

    ref $data_metrics eq 'HASH'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # Do some checks on the data here.
    foreach my $header ( keys %$data_metrics ) {

        # Log ratios should be between certain limits.
        if ($data_metrics->{$header}{scale}
            && (

                # column heading looks like 'log ratio'
                ( $header =~ m/log ?ratio/i )

                || (

                    # Or subclass is Ratio...
                    $data_metrics->{$header}{subclass}
                    && (( $data_metrics->{$header}{subclass} eq 'Ratio' )

                        # and heading/scale looks like 'log'
                        && ( ( $header =~ m/\blog/i ) )
                        || ( $data_metrics->{$header}{scale} =~ m/^log_/i )
                    )
                )
            )
            ) {

            LOGRATIOSCALECHECK:
            {
                my $error_message = 'Log ratios outside range';

                ( $data_metrics->{$header}{scale} eq 'log_base_2' ) && do {
                    $data_metrics->{$header}{errors}{$error_message}++

         # Scanners seem to max out at 100,000 (i.e. log2(100,000) = 17 or so)
                        if ( ( $data_metrics->{$header}{max} > 20 )
                        || ( $data_metrics->{$header}{min} < -20 ) );
                    last LOGRATIOSCALECHECK;
                };

                ( $data_metrics->{$header}{scale} eq 'log_base_10' ) && do {
                    $data_metrics->{$header}{errors}{$error_message}++

               # Scanners seem to max out at 100,000 (i.e. log10(100,000) = 5)
                        if ( ( $data_metrics->{$header}{max} > 6 )
                        || ( $data_metrics->{$header}{min} < -6 ) );
                    last LOGRATIOSCALECHECK;
                };

                # Play it safe. We're unlikely to use this anyway
                $data_metrics->{$header}{errors}{$error_message}++
                    if ( ( $data_metrics->{$header}{max} > 1 )
                    || ( $data_metrics->{$header}{min} < -1 ) );
            }
        }

        # Check on any saturation data in the file (e.g. in GenePix files)
        my $error_message = 'Saturation indicator too high';

        # regexp may need work FIXME
        if (   ( $header =~ m/\% ?sat/i )
            && ( $data_metrics->{$header}{max} > 50 ) ) {
            $data_metrics->{$header}{errors}{$error_message}++;
        }
    }

    return;
}

1;
