#!/usr/bin/env perl
#
# Module to provide basic methods used by the tab2mage script.
#
# Tim Rayner 2005, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Parser.pm 2069 2008-06-04 14:33:52Z tfrayner $
#

package ArrayExpress::Datafile::Parser;

use strict;
use warnings;

use Class::Std;
use Digest::MD5 qw( md5 );    # Used for tracking DEDs
use IO::File;
use File::Copy;
use Carp;
use Readonly;
use Scalar::Util qw( openhandle );
use List::Util qw(first);
use List::MoreUtils qw( any );
use English qw( -no_match_vars );
use Storable qw(store_fd fd_retrieve);

use ArrayExpress::Curator::Config qw($CONFIG);

use ArrayExpress::Datafile::QT_list qw(get_QTs);

use ArrayExpress::Curator::Database qw(
    retrieve_AE_featurelist
    map_affy_accno_to_name
);

use ArrayExpress::Curator::MAGE::Definitions qw(
    $EDF_EXPTACCESSION
    $EDF_EXPTDOMAIN
    $EDF_EXPTNAME
    $EDF_DYE
    $EDF_HYB_PROTOCOL
    $EDF_SCAN_PROTOCOL
    $EDF_FEXT_PROTOCOL
    $EDF_NORM_PROTOCOL
    $EDF_SCAN_SOFTWARE
    $EDF_FEXT_SOFTWARE
    $EDF_NORM_SOFTWARE
    $EDF_IMAGE_FORMAT
    $EDF_ARRAYSERIAL
    $EDF_FILE_PREFIX
    $EDF_FILE_SUFFIX
    $EDF_HYB_PARAMS
    $EDF_SCAN_PARAMS
    $EDF_FEXT_PARAMS
    $EDF_NORM_PARAMS
    $EDF_FEXT_STATS
    $EDF_NORM_STATS
    $EDF_HYB_HARDWARE
    $EDF_SCAN_HARDWARE
    $EDF_HYB_HW_PARAMS
    $EDF_SCAN_HW_PARAMS
    $EDF_NORMALIZATIONTYPE

    $OE_VAL_NORMALIZATION
    $OE_VAL_FEATUREEXTRACTION
    $OE_VAL_SCANNING
    $OE_VAL_HYBRIDIZATION

    $AE_LABELCOMPOUND_PREFIX
    $AE_CHANNEL_PREFIX
);

use ArrayExpress::Curator::Common qw(
    get_filepath_from_uri
    strip_discards
    clean_hash
    find_cdf
    $RE_EMPTY_STRING
);

Readonly my $AFFY_PROTOCOL_PREFIX  => 'Affymetrix:Protocol:';
Readonly my $AFFY_PARAMETER_PREFIX => 'Affymetrix:Parameter:';

my %quantitation_types   : ATTR( :get<quantitation_types>,   :default<undef> );
my %use_binary_datafiles : ATTR( :get<use_binary_datafiles>, :init_arg<use_binary_datafiles>, :default<undef> );
my %output_directory     : ATTR( :get<output_directory>,     :init_arg<output_directory>,     :default<q{.}>  );
my %source_directory     : ATTR( :get<source_directory>,     :init_arg<source_directory>,     :default<q{.}>  );
my %include_known_qts    : ATTR( :get<include_known_qts>,    :init_arg<include_known_qts>,    :default<undef> );
my %allow_undef_qts      : ATTR( :get<allow_undef_qts>,      :init_arg<allow_undef_qts>,      :default<undef> );
my %reporter_prefix      : ATTR( :get<reporter_prefix>,      :init_arg<reporter_prefix>,      :default<undef> );
my %compseq_prefix       : ATTR( :get<compseq_prefix>,       :init_arg<compseq_prefix>,       :default<undef> );
my %ignore_size_limits   : ATTR( :name<ignore_size_limits>,  :default<undef> );

my %error_fh             : ATTR( :name<error_fh>,      :default<\*STDOUT> );
my %clobber              : ATTR( :name<clobber>,       :default<0>        );
my %namespace            : ATTR( :name<namespace>,     :default<q{}>      );
my %is_standalone        : ATTR( :name<is_standalone>, :default<undef>    );
my %feature_fh           : ATTR( :set<feature_fh>,     :default<undef>    );
my %cel_types            : ATTR( :get<cel_types>,      :default<{}>       );
my %array_cache          : ATTR( :default<{}> );
my %cdf_cache            : ATTR( :default<{}> );
my %ded_cache            : ATTR( :default<{}> );

my %protocol_bag         : ATTR( :get<protocol_bag>,  :init_arg<protocol_bag>,  :default<sub{ die("Error: Parser protocol_bag not initialized.")  }> );
my %parameter_bag        : ATTR( :get<parameter_bag>, :init_arg<parameter_bag>, :default<sub{ die("Error: Parser parameter_bag not initialized.") }> );

sub START {

    my ($self, $id, $args) = @_;

    # Initialize QTs (this is more complicated than a simple setter
    # method as provided by Class::Std). This *MUST* be done after
    # object initialization.
    $self->set_quantitation_types($args->{quantitation_types});

    return;
}

sub get_unique_de_dimension {

    my ( $self, $ded_array, $array, $ded_type ) = @_;

    ref $ded_array eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $array    and confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $ded_type and confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # We use an md5 hash string here rather than the full DED to save
    # on memory.
    my $md5 = Digest::MD5->new();
    foreach my $de_id (@$ded_array) { $md5->add($de_id); }
    my $ded_md5 = $md5->digest;

    # Tracker hash format: $ded_cache = {$ded_md5 => $ded_identifier}
    # If we've already seen this DED, use the old $ded_identifier and
    # skip adding it again.
    my $ded_identifier;

    unless ( $ded_identifier = $self->get_ded_cache($ded_md5) ) {

        $ded_identifier = $self->set_ded_cache(
	    $ded_md5,
	    $ded_array,
	    $array,
	    $ded_type,
	);

    }

    return wantarray ? ( $ded_identifier, $ded_type ) : $ded_identifier;

}

sub parse {

    my ( $self, $file, $datarow ) = @_;

    $datarow ||= {};

    $file->isa('ArrayExpress::Datafile')
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # recheck the file; some time may have elapsed
    unless ( -e $file->get_path() ) {
        croak(    "ERROR: Missing data file: "
                . $file->get_path()
                . ". This script is aborting.\n" );
    }

    print STDOUT ( "Processing file " . $file->get_name() . "\n" );

    # Set the error filehandle on Datafile to redirect warnings/errors.
    $file->set_error_fh( $self->get_error_fh() );

    # Draw a line in the log file.
    my $filler_length = 80 - ( 3 + 2 + length( $file->get_name() ) );
    print { $self->get_error_fh() }
        (     q{-} x 3 . q{ }
            . $file->get_name() . q{ }
            . q{-} x $filler_length
            . qq{\n} );

    # Test for file size
    if ( ( -s $file->get_path() > $CONFIG->get_MAX_DATAFILE_SIZE() )
         && ! $self->get_ignore_size_limits() ) {

        # Set some needed parameters
        if ( $file->get_data_type eq 'raw' ) {
            $file->set_ded_type('Feature');
        }
        else {
            $file->set_ded_type('Reporter');
        }
        $file->set_ded_identifier(
            '+++++++++++++++++++++ERROR++++++++++++++++++++++');

        # Alert the user
        my $error_msg = "\n    *** PARSE ERROR: File "
            . $file->get_name()
            . " is too large to be processed safely.\n"
            . "    *** This file will be skipped, but a dummy BioAssayData object will be created.\n\n";
        print { $self->get_error_fh() } ($error_msg);
        print STDERR ($error_msg);
    }

    # If the file is small enough (usually it will be) we process it here.
    elsif ( $file->get_is_binary() || ( $file->get_path() =~ m/\. (CEL|CHP) \z/ixms ) ) {

	# Deal with Affy files.
        $self->_parse_affy_datafile( $file, $datarow );
    }

    else {                          # Text files, including Affy CELv3.
        $self->_parse_text_datafile( $file, $datarow );
    }

    return;

}

####################
# Accessor methods #
####################

sub get_feature_fh {

    # defaults to temporary filehandle on first use (i.e. not on
    # object initialization).
    my ( $self ) = @_;

    $feature_fh{ident $self} ||= IO::File->new_tmpfile;

    return $feature_fh{ident $self};
}

sub get_ded_count {

    # Method to return the number of DEDs held in the DED
    # tracker. Used to detect whether DED processing should be invoked
    # following MAGE-ML generation.
    my $self = shift;

    return scalar(grep { defined $_ } values %{ $ded_cache{ident $self} });
}

###################
# Private methods #
###################

sub set_quantitation_types : PRIVATE {

    my ( $self, $qtfile ) = @_;

    if ( $qtfile || ! $quantitation_types{ident $self} ) {
        $quantitation_types{ident $self}
            = get_QTs( $qtfile, $self->get_include_known_qts() );
    }

    return $quantitation_types{ident $self};
}

sub set_cel_types : PRIVATE {

    my ( $self, %new_cels ) = @_;

    foreach my $new_key ( keys %new_cels ) {
        ref $new_key and confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
        ref $new_cels{$new_key}
            and confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
        $cel_types{ident $self}{$new_key} = $new_cels{$new_key};
    }

    return $cel_types{ident $self};
}

sub set_array_cache : PRIVATE {

    # Requires an array id and featurelist hashref
    my ( $self, $array, $featurelist ) = @_;

    $array or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $featurelist eq 'HASH'
	   or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $fh = IO::File->new_tmpfile();

    store_fd( $featurelist, $fh )
	or confess("Error caching array feature list: $!");
    seek($fh, 0, 0) or confess("Error rewinding array cache file: $!");

    $array_cache{ident $self}{$array} = $fh;

    return $array_cache{ident $self}{$array};
}

sub get_array_cache : PRIVATE {

    # Requires an array id
    my ( $self, $array ) = @_;

    $array or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # Return undef if array not in cache.
    my $featurelist;
    if ( my $fh = $array_cache{ident $self}{$array} ) {

	my $pos = tell($fh);

	seek($fh, 0, 0) or confess("Error rewinding array cache file: $!");

	$featurelist = fd_retrieve( $fh )
	    or confess("Error retrieving cached array feature list: $!");

	seek($fh, $pos, 0) or confess("Error resetting array cache file: $!");
    }

    return $featurelist;
}

sub set_cdf_cache : PRIVATE {

    # Requires a CDF id/filename and cdffile object
    my ( $self, $cdffile, $cdf ) = @_;

    $cdffile or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    $cdf->isa('ArrayExpress::Datafile::Affymetrix::CDF')
             or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    $cdf_cache{ident $self}{$cdffile} = $cdf;
    return $cdf_cache{ident $self}{$cdffile};
}

sub get_cdf_cache : PRIVATE {

    # Requires a CDF id/filename.
    my ( $self, $cdffile ) = @_;

    $cdffile or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    return $cdf_cache{ident $self}{$cdffile};
}

sub set_ded_cache : PRIVATE {

    my ( $self, $ded_md5, $ded_array, $array, $ded_type ) = @_;

    defined($ded_md5)  or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    defined($array)    or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    defined($ded_type) or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $ded_array eq 'ARRAY'
	or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $inverted_tracker;    # values to keys; we drop the ded_md5 keys
    foreach my $value ( values %{ $ded_cache{ident $self} } ) {
	$inverted_tracker->{$value}++;
    }

    # Determine the DesignElementDimension identifier
    my $i = 0;
    my $ded_identifier;
    while ( !$ded_identifier || $inverted_tracker->{$ded_identifier} ) {
	$ded_identifier = $self->get_namespace()
	    . ".$array."
            . ( ++$i ) . "."
	    . $ded_type
            . "Dimension";
    }

    $ded_cache{ident $self}{$ded_md5} = $ded_identifier;

    $self->_append_designelementdimension(
	$ded_array,
	$ded_identifier,
	$ded_type,
    );

    return $ded_cache{ident $self}{$ded_md5};

}

sub get_ded_cache : PRIVATE {

    my $self    = shift;
    my $ded_md5 = shift;

    defined($ded_md5) or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    return $ded_cache{ident $self}{$ded_md5};
}

sub _create_identifier_ded : PRIVATE {

    my ( $self, $file, $indexcols, $output_fh, $array_id ) = @_;

    local $INPUT_RECORD_SEPARATOR = $file->get_linebreak_type();

    my $input_fh = $file->get_filehandle()
	or croak("Error retrieving filehandle.");

    my $first_index_column = $file->get_column_headings()->[ $indexcols->[0] ];

    my ( $ded_array, $ded_type, $de_prefix, $de_separator );

    # Fix Affymetrix identifiers (this is AE-specific).
    my $compseq_prefix  = $self->get_compseq_prefix();
    my $reporter_prefix = $self->get_reporter_prefix();
    my $accno           = $file->get_array_design_id();
    if ( $accno && $accno =~ m/\A A-AFFY-\w+ \z/xms ) {
	my $affyname = map_affy_accno_to_name($accno);
	unless (defined $reporter_prefix) {
	    $reporter_prefix = "Affymetrix:Reporter:$affyname:";
	}
	unless (defined $compseq_prefix) {
	    $compseq_prefix = "Affymetrix:CompositeSequence:$affyname:";
	}
    }

    # Only one index column suggests a Reporter/CS identifier column
    if (   $first_index_column =~ m/Probe ?(Set)? ?(Name|ID)/i
	|| $file->get_dm_chip_type() ) {

	# Post-Affy normalized data
	$de_prefix = defined( $compseq_prefix )
	           ? $compseq_prefix
		   : "Affymetrix:CompositeSequence:$array_id:";
	
	$ded_array = $self->_extract_designelementdimension(
	    $input_fh,
	    $output_fh,
	    $indexcols,
	    $de_prefix,
	);
	$ded_type  = 'CompositeSequence';
    }

    elsif ( $first_index_column =~ m/Composite *Sequence *Identifier/i ) {

	$de_prefix = defined( $compseq_prefix )
	           ? $compseq_prefix
		   : "ebi.ac.uk:MIAMExpress:CompositeSequence:$array_id.";

	$ded_array = $self->_extract_designelementdimension(
	    $input_fh,
	    $output_fh,
	    $indexcols,
	    $de_prefix,
	);
	$ded_type = 'CompositeSequence';
    }

    elsif ( $first_index_column =~ m/Reporter *Identifier/i
         || $first_index_column eq 'ID_REF' ) {

	$de_prefix = defined( $reporter_prefix )
	           ? $reporter_prefix
		   : "ebi.ac.uk:MIAMExpress:Reporter:$array_id.";

	$ded_array = $self->_extract_designelementdimension(
	    $input_fh,
	    $output_fh,
	    $indexcols,
	    $de_prefix,
	);
	$ded_type = 'Reporter';
    }

    else {    # Unknown is flagged as a fatal error downstream

	$ded_array = $self->_extract_designelementdimension(
	    $input_fh,
	    $output_fh,
	    $indexcols,
	    'Unknown.',
	);
	$ded_type = 'Unknown';
    }

    return ( $ded_array, $ded_type );
}

sub _create_feature_ded : PRIVATE {

    my ( $self, $file, $indexcols, $output_fh, $array_id ) = @_;

    local $INPUT_RECORD_SEPARATOR = $file->get_linebreak_type();

    my $input_fh = $file->get_filehandle()
	or croak("Error retrieving filehandle.");

    my $ded_type     = 'Feature';
    my $de_prefix    = "ebi.ac.uk:MIAMExpress:Feature:$array_id.";
    my $ded_array;

    if ( $self->get_is_standalone() ) {

	# In standalone mode, we make up our own identifiers using the
	# MIAMExpress convention.
	$ded_array = $self->_extract_designelementdimension(
	    $input_fh,
	    $output_fh,
	    $indexcols,
	    $de_prefix,
	);
    }

    else {

	# Get the array feature info from the public ArrayExpress
	# database.  Attempt to get features into the array cache, if
	# we've not done so before.
	my $featurelist;
	unless ( $featurelist = $self->get_array_cache($array_id) ) {

	    # Note that this call may croak if AE is down;
	    # however, death here is the preferred outcome in such
	    # cases (otherwise we will probably generate the wrong
	    # feature ids). May revisit this later FIXME.
	    $featurelist = retrieve_AE_featurelist($array_id);

	    if ($featurelist) {
		$self->set_array_cache( $array_id, $featurelist );
	    }
	    else {
		print STDERR (
		    "Warning: Unable to retrieve feature information from"
		  . " ArrayExpress. Falling back to default Feature identifier format.\n"
	      );
	    }
	}

	# If the array is known to us, use the AE feature identifiers
	if ( $featurelist ) {
	    my $coord_array
		= $self->_extract_feature_coord_strings(
		    $input_fh,
		    $output_fh,
		    $indexcols,
		);

	    foreach my $coord (@$coord_array) {
		my $feature_id;
		unless ( $feature_id = $featurelist->{$coord} ) {

                    # This is most likely a dummy array; dying here would just
                    # be irritating. We create a dummy identifier instead, at
                    # risk of failing to validate the output MAGE-ML against
                    # the database. A better way to handle dummy arrays in AE
                    # is needed to fix this kludge.

		    print STDERR (
			"WARNING: Feature with coordinates $coord not found for array $array_id. ",
			"A generic dummy feature identifier will be created.\n"
		    );

		    $feature_id = "${de_prefix}${coord}";
		}

		# Construct the DED
		push( @$ded_array, $feature_id );
	    }
	}

	# If the array is not found in AE, we make up our own
	# identifiers using the MIAMExpress convention.
	else {
	    $ded_array = $self->_extract_designelementdimension(
		$input_fh,
		$output_fh,
		$indexcols,
		$de_prefix,
	    );
	}
    }

    return ( $ded_array, $ded_type );
}

sub _create_designelementdimension : PRIVATE {

    my ( $self, $file, $output_fh, $array_id ) = @_;

    $file->isa('ArrayExpress::Datafile')
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my ( $ded_array, $ded_type );

    my @indexcols = @{ $file->get_index_columns() };

    # Set the DE type and identifier prefix. We default to MIAMExpress
    # format here, as that is the commonest route for array submissions
    # linked to Tab2MAGE.
    if ( scalar @indexcols == 1 ) {

	( $ded_array, $ded_type )
	    = $self->_create_identifier_ded(
		$file,
		\@indexcols,
		$output_fh,
		$array_id,
	    );

    }
    else {    # Normal MC/MR/C/R features

	( $ded_array, $ded_type )
	    = $self->_create_feature_ded(
		$file,
		\@indexcols,
		$output_fh,
		$array_id,
	    );
    }

    my $ded_identifier
        = $self->get_unique_de_dimension(
	    $ded_array,
	    $array_id,
	    $ded_type,
	);

    # Fix up the Datafile object
    $file->set_ded_identifier($ded_identifier);
    $file->set_ded_type($ded_type);

    # Strip out the index column information - the file columns have
    # been stripped out by extract_designelementdimension.
    my @original_headings = @{ $file->get_column_headings() };
    my @fixed_headings;
    foreach my $index ( 0 .. $#original_headings ) {
        unless ( any { $index == $_ } @indexcols ) {
            push @fixed_headings, $original_headings[$index];
        }
    }
    $file->set_column_headings( \@fixed_headings );
    $file->set_heading_qts( \@fixed_headings );
    $file->set_index_columns( [] );
    $file->set_filehandle( $output_fh );

    return;
}

sub _append_designelementdimension : PRIVATE {

    my ( $self, $ded_array, $ded_identifier, $ded_type ) = @_;

    ref $ded_array eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $ded_identifier and confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $ded_type       and confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $tmpfh = $self->get_feature_fh();

    my $assnref_type
        = ( $ded_type eq 'Feature' ) ? 'ContainedFeatures' : $ded_type . 's';

    # seek to the end of the file
    seek( $tmpfh, 0, 2 )
        or croak(
        "Error finding DesignElementDimension filehandle EOF to append: $!\n"
        );

    print $tmpfh (
        qq{      <${ded_type}Dimension identifier="$ded_identifier">\n});

    print $tmpfh ( qq{        <${assnref_type}_assnreflist>\n});

    foreach my $designelement_id (@$ded_array) {
        print $tmpfh (
            qq{          <${ded_type}_ref identifier="$designelement_id"/>\n}
        );
    }

    print $tmpfh ( qq{        </${assnref_type}_assnreflist>\n});

    print $tmpfh ( qq{      </${ded_type}Dimension>\n});

    # Not really necessary at this point, just here for future safety.
    $self->set_feature_fh($tmpfh);

    return;
}

sub _extract_designelementdimension : PRIVATE {

    my ( $self,
	 $input_fh,
	 $output_fh,
	 $indexcols,
	 $prefix, ) = @_;

    ref $indexcols eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    openhandle($input_fh)  or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    openhandle($output_fh) or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $ded_array = $self->_extract_feature_coord_strings(
	$input_fh,
	$output_fh,
        $indexcols,
    );

    @$ded_array = map {"${prefix}${_}"} @$ded_array;

    return ($ded_array);

}

sub _extract_feature_coord_strings : PRIVATE {

    my ( $self, $input_fh, $output_fh, $indexcols ) = @_;

    ref $indexcols eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    openhandle($input_fh) or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    openhandle($output_fh)
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $coord_array = [];

    DATA_LINE:
    while ( my $line = <$input_fh> ) {

        $line =~ s/[\r\n]*$//;

	# Empty lines are skipped during data export, so we skip them
	# here also.
        if ( $line =~ $RE_EMPTY_STRING ) {
            next DATA_LINE;
        }

        my @line_array = split /\t/, $line;

        push( @$coord_array, ( join( ".", @line_array[@$indexcols] ) ) );

        my $new_line = strip_discards( $indexcols, \@line_array );

        print $output_fh ( join( "\t", @$new_line ), "\n" );

    }

    return ($coord_array);

}

sub _mage_param_hash : PRIVATE {

    # Converts a hash of parameter values from the Affy parsers into a
    # parameter hash suitable for feeding into MAGE::new_protocol().
    my ( $param_values ) = @_;

    my %param_hash =
        map { $_ => { identifier => "${AFFY_PARAMETER_PREFIX}$_",
		      name       => $_,                           } }
        keys %$param_values;
    return \%param_hash;
}

sub _extract_chp_data : PRIVATE {

    my ( $self, $chp, $datarow ) = @_;

    # Sort out the protocols
    $datarow->{$EDF_NORM_PROTOCOL}
	= "${AFFY_PROTOCOL_PREFIX}" . $chp->get_algorithm();

    # FIXME these won't always be the case.
    if ( $chp->get_version == 12 ) {
	$datarow->{$EDF_NORM_SOFTWARE}
	    ||= 'Affymetrix MicroArraySuite (5.0)';
    }
    if ( $chp->get_version == 8 ) {
	$datarow->{$EDF_NORM_SOFTWARE}
	    ||= 'Affymetrix MicroArraySuite (4.0)';
    }
    else {
	$datarow->{$EDF_NORM_SOFTWARE}
	    ||= 'Affymetrix GeneChip Operating Software (1.2)';
    }

    # Parameters
    my %chp_params;
    foreach my $param ( keys %{ $chp->get_parameters() } ) {
	$chp_params{"${AFFY_PARAMETER_PREFIX}$param"}
	    = $chp->get_parameters()->{$param};
    }
    $datarow->{$EDF_NORM_PARAMS} = \%chp_params;

    # Make sure the protocol params are declared in any
    # generated MAGE-ML.
    $self->get_protocol_bag()->(
	$datarow->{$EDF_NORM_PROTOCOL},
	{   protocol_type => $OE_VAL_NORMALIZATION,
	    name          => $chp->get_algorithm(),
	    parameters    => _mage_param_hash( $chp->get_parameters() ),
	    parameter_bag => $self->get_parameter_bag(),
	}
    );

    # QC Stats
    $datarow->{$EDF_NORM_STATS} = $chp->get_stats();

    # DerivedBioAssayType. This is probably not foolproof FIXME
    DBA_TYPE:
    {
	( $chp->get_algorithm() =~ /ExpressionStat/ ) && do {
	    $datarow->{$EDF_NORMALIZATIONTYPE}
		= "Statistical CHP Analysis";
	    last DBA_TYPE;
	};
	( $chp->get_algorithm() =~ /ExpressionCall/ ) && do {
	    $datarow->{$EDF_NORMALIZATIONTYPE}
		= "Empirical CHP Analysis";
	    last DBA_TYPE;
	};
    }

    return;
}

sub _extract_cel_data : PRIVATE {

    my ( $self, $cel, $datarow ) = @_;

    # CEL parameters
    my %cel_params;
    foreach my $param ( keys %{ $cel->get_parameters() } ) {
	$cel_params{"${AFFY_PARAMETER_PREFIX}$param"}
	    = $cel->get_parameters()->{$param};
    }
    $datarow->{$EDF_FEXT_PARAMS} = \%cel_params;

    # Make sure the protocol params are declared in any
    # generated MAGE-ML.
    $self->get_protocol_bag()->(
	$datarow->{$EDF_FEXT_PROTOCOL},
	{   protocol_type => $OE_VAL_FEATUREEXTRACTION,
	    name          => 'Affymetrix CEL analysis (Percentile)',
	    parameters    => _mage_param_hash( $cel->get_parameters() ),
	    parameter_bag => $self->get_parameter_bag(),
	}
    );

    # QC Stats
    $datarow->{$EDF_FEXT_STATS} = $cel->get_stats();

    # Hardware (This is an Affy-specific hack for now FIXME)
    $datarow->{$EDF_HYB_HARDWARE}
	||= 'Affymetrix:Hardware:FluidicsStation';
    $datarow->{$EDF_SCAN_HARDWARE} ||= 'Affymetrix:Hardware:Scanner';

    # FIXME this below won't always be the case
    if ( $cel->get_version() == 3 ) {
	$datarow->{$EDF_SCAN_SOFTWARE}
	    ||= 'Affymetrix MicroArraySuite (5.0)';
	$datarow->{$EDF_FEXT_SOFTWARE}
	    ||= 'Affymetrix MicroArraySuite (5.0)';
    }
    elsif ( $cel->get_version() == 4 ) {
	$datarow->{$EDF_SCAN_SOFTWARE}
	    ||= 'Affymetrix GeneChip Operating Software (1.2)';
	$datarow->{$EDF_FEXT_SOFTWARE}
	    ||= 'Affymetrix GeneChip Operating Software (1.2)';
    }
    else {
	$datarow->{$EDF_SCAN_SOFTWARE}
	    ||= 'Affymetrix Expression Console (1.1)';
	$datarow->{$EDF_FEXT_SOFTWARE}
	    ||= 'Affymetrix Expression Console (1.1)';
    }

    # image format
    $datarow->{$EDF_IMAGE_FORMAT} ||= 'Affymetrix_DAT';

    return;
}

sub _read_exp_file : PRIVATE {

    my ( $self, $expfile ) = @_;

    # Check we don't have an alternate source directory
    $expfile = get_filepath_from_uri(
	$expfile,
	$self->get_source_directory(),
    );

    # Parse the file.
    my $factory = ArrayExpress::Datafile::Affymetrix->new();
    my $exp = $factory->make_parser($expfile);
    $exp->parse();

    return $exp;
}
   
sub _extract_exp_data : PRIVATE {

    my ( $self, $expfile, $datarow ) = @_;

    my $exp = $self->_read_exp_file( $expfile );

    # Correct the hyb protocol, if it's known. EXP file
    # information trumps all.
    if ( $exp->get_protocol() ) {
	$datarow->{$EDF_HYB_PROTOCOL}
	    = "${AFFY_PROTOCOL_PREFIX}Hybridization-"
		. $exp->get_protocol();
    }

    # EXP parameters
    my %hyb_params;
    foreach my $param ( keys %{ $exp->get_parameters() } ) {
	$hyb_params{"${AFFY_PARAMETER_PREFIX}$param"}
	    = $exp->get_parameters()->{$param};
    }
    $datarow->{$EDF_HYB_PARAMS}  = \%hyb_params;
    $datarow->{$EDF_SCAN_PARAMS} = clean_hash(
	{
	    "${AFFY_PARAMETER_PREFIX}ScanPixelSize" =>
		$exp->get_pixel_size(),
	    "${AFFY_PARAMETER_PREFIX}NumberOfScans" =>
		$exp->get_num_scans(),
	}
    );
    $datarow->{$EDF_HYB_HW_PARAMS} = clean_hash(
	{
	    "${AFFY_PARAMETER_PREFIX}FluidicsStationNumber" =>
		$exp->get_station(),
	    "${AFFY_PARAMETER_PREFIX}FluidicsStationPosition" =>
		$exp->get_module(),
	}
    );
    $datarow->{$EDF_SCAN_HW_PARAMS} = clean_hash(
	{
	    "${AFFY_PARAMETER_PREFIX}ScannerID" =>
		$exp->get_scanner_id(),
	    "${AFFY_PARAMETER_PREFIX}ScannerType" =>
		$exp->get_scanner_type(),
	}
    );

    # Make sure the protocol params are declared in any
    # generated MAGE-ML.
    $self->get_protocol_bag()->(
	$datarow->{$EDF_HYB_PROTOCOL},
	{   protocol_type => $OE_VAL_HYBRIDIZATION,
	    name          => $exp->get_protocol(),
	    parameters    => _mage_param_hash( $exp->get_parameters() ),
	    parameter_bag => $self->get_parameter_bag(),
	}
    );
    $self->get_protocol_bag()->(
	$datarow->{$EDF_SCAN_PROTOCOL},
	{   protocol_type => $OE_VAL_SCANNING,
	    name          => 'Affymetrix Scanning',
	    parameters    => _mage_param_hash(
		{
		    'ScanPixelSize' => $exp->get_pixel_size(),
		    'NumberOfScans' => $exp->get_num_scans(),
		},
	    ),
	    parameter_bag => $self->get_parameter_bag(),
	}
    );

    # Grab the array chip lot if it's available. We default to the
    # EXP file value as it's more likely to be correct.
    $exp->get_chip_lot()
	&& do { $datarow->{$EDF_ARRAYSERIAL} = $exp->get_chip_lot(); };

    return $exp->get_chip_type();
}

sub _get_bioassays_from_mbadata : PRIVATE {

    my ( $self, $badata ) = @_;

    my (@mbas, @pbas);
    if ( my $dim = $badata->getBioAssayDimension() ) {
	foreach my $mba ( @{ $dim->getBioAssays() || [] } ) {
	    push @mbas, $mba;
	    if ( my $fext = $mba->getFeatureExtraction() ) {
		if ( my $pba = $fext->getPhysicalBioAssaySource() ) {
		    push @pbas, $pba;
		}
	    }
	}
    }

    return ( \@mbas, \@pbas );
}

sub _add_expdata_to_mbadata : PRIVATE {

    # If the file has a ba_data with a NVT referring to what looks
    # like an EXP file, we process it here and add whatever we can to
    # the PBAs (which for single-colour hybs such as Affy will just be
    # the actual hybridization PBA).
    my ( $self, $expfile, $badata ) = @_;

    my ( $mbas, $pbas ) = $self->_get_bioassays_from_mbadata( $badata );
    my $exp = $self->_read_exp_file( $expfile );

    if ( $exp->get_protocol() ) {
	$self->_add_hybdata_to_pbas( $exp, $pbas );
    }

    $self->_add_scandata_to_pbas( $exp, $pbas );

    return $exp->get_chip_type();
}

sub _add_scandata_to_pbas : PRIVATE {

    my ( $self, $exp, $pbas ) = @_;

    foreach my $pba ( @$pbas ) {

	BATREAT:
	foreach my $bat ( @{ $pba->getBioAssayTreatments() || [] } ) {

	    # Don't add if there's already a protocolapp.
	    unless ( scalar( @{ $bat->getProtocolApplications() || [] } ) ) {
		my ($protocol, $param_hash)
		    = $self->_create_affy_hyb_protocol( $exp );
		my $pa = Bio::MAGE::Protocol::ProtocolApplication->new(
		    protocol     => $protocol,
		    activityDate => $exp->get_scan_date() || 'n/a',
		);
		while ( my ($name, $value) = each %{ $param_hash } ) {
		    my $pv = Bio::MAGE::Protocol::ParameterValue->new(
			parameterType => $self->get_parameter_bag->(
			    $protocol->getIdentifier().$name
			),
			value         => $value,
		    );
		    $pa->addParameterValues( $pv );
		}
		$bat->addProtocolApplications( $pa );
	    }

	    # Should be only one treatment for Affy.
	    last BATREAT;
	}
    }
   
    return;
}
    
sub _create_affy_scan_protocol : PRIVATE {

    my ( $self, $exp ) = @_;

    my $accession = "${AFFY_PROTOCOL_PREFIX}Scan";

    my $protocol = $self->get_protocol_bag()->(
	$accession,
	{
	    accession     => $accession,
	    type          => $OE_VAL_SCANNING,
	    name          => 'Affymetrix Scanning Protocol',
	}
    );

    my @scan_params;
    my $param_hash = clean_hash(
	{
	    "ScanPixelSize" => $exp->get_pixel_size(),
	    "NumberOfScans" => $exp->get_num_scans(),
	}
    );

    my $mage_params = _mage_param_hash( $param_hash );
    while ( my ( $name, $args ) = each %{ $mage_params } ) {
	push @scan_params, $self->get_parameter_bag()->(
	    $accession.$name,
	    $args,
	);
    }
    $self->_add_params_to_protocol( \@scan_params, $protocol );

    return $protocol, $param_hash;
}

sub _add_hybdata_to_pbas : PRIVATE {

    my ( $self, $exp, $pbas ) = @_;
    
    foreach my $pba ( @$pbas ) {
	if ( my $hyb = $pba->getBioAssayCreation() ) {

	    # Don't add if there's already a protocolapp.
	    unless ( scalar( @{ $hyb->getProtocolApplications() || [] } ) ) {
		my $protocol = $self->_create_affy_hyb_protocol($exp);
		my $pa = Bio::MAGE::Protocol::ProtocolApplication->new(
		    protocol     => $protocol,
		    activityDate => $exp->get_hyb_date() || 'n/a',
		);
		while ( my ($name, $value) = each %{ $exp->get_parameters() } ) {
		    my $pv = Bio::MAGE::Protocol::ParameterValue->new(
			parameterType => $self->get_parameter_bag->(
			    $protocol->getIdentifier().$name
			),
			value         => $value,
		    );
		    $pa->addParameterValues( $pv );
		}
		$hyb->addProtocolApplications( $pa );
	    }
	}
    }

    return;
}

sub _create_affy_hyb_protocol : PRIVATE {

    my ( $self, $exp ) = @_;

    my $accession = "${AFFY_PROTOCOL_PREFIX}Hybridization-"
	. $exp->get_protocol();

    my $protocol = $self->get_protocol_bag()->(
	$accession,
	{
	    accession     => $accession,
	    type          => $OE_VAL_HYBRIDIZATION,
	    name          => $exp->get_protocol(),
	}
    );

    my @params;
    my $mage_params = _mage_param_hash( $exp->get_parameters() );
    while ( my ( $name, $args ) = each %{ $mage_params } ) {
	push @params, $self->get_parameter_bag()->(
	    $accession.$name,
	    $args,
	);
    }
    $self->_add_params_to_protocol( \@params, $protocol );

    return $protocol;
}

sub _add_celdata_to_mbadata : PRIVATE {

    # Navigate to MBA and add a protocolapp,
    # summarystats; FIXME also software, hardware etc?
    my ( $self, $cel, $badata ) = @_;

    my ( $mbas, $pbas ) = $self->_get_bioassays_from_mbadata( $badata );

    foreach my $mba ( @$mbas ) {
	if ( my $fext = $mba->getFeatureExtraction() ) {

	    # Don't add if there's already a protocolapp.
	    unless ( scalar( @{ $fext->getProtocolApplications() || {} } ) ) {
		my $protocol
		    = $self->_create_affy_fext_protocol( $cel );
		my $pa = Bio::MAGE::Protocol::ProtocolApplication->new(
		    protocol     => $protocol,
		    activityDate => 'n/a',
		);
		while ( my ($name, $value) = each %{ $cel->get_parameters() } ) {
		    my $pv = Bio::MAGE::Protocol::ParameterValue->new(
			parameterType => $self->get_parameter_bag->(
			    $protocol->getIdentifier().$name
			),
			value         => $value,
		    );
		    $pa->addParameterValues( $pv );
		}
		$fext->addProtocolApplications( $pa );
	    }
	}
    }

    $self->_add_summarystats_to_badata( $cel->get_stats(), $badata );

    return;
}

sub _add_summarystats_to_badata : PRIVATE {

    my ( $self, $stats, $badata ) = @_;

    if ( $stats ) {
        my @statistics;
        foreach my $name ( sort keys %$stats ) {
            push(
                @statistics,
                Bio::MAGE::NameValueType->new(
                    name  => $name,
                    value => $stats->{$name},
                )
            );
        }
        $badata->setSummaryStatistics( \@statistics );
    }

    return;
}

sub _create_affy_fext_protocol : PRIVATE {

    my ( $self, $cel ) = @_;

    my $accession = 'P-AFFY-6';

    my $protocol = $self->get_protocol_bag()->(
	$accession,
	{
	    accession     => $accession,
	    type          => $OE_VAL_FEATUREEXTRACTION,
	    name          => 'Affymetrix CEL analysis (Percentile)',
	}
    );

    my @params;
    my $mage_params = _mage_param_hash( $cel->get_parameters() );
    while ( my ( $name, $args ) = each %{ $mage_params } ) {
	push @params, $self->get_parameter_bag()->(
	    $accession.$name,
	    $args,
	);
    }
    $self->_add_params_to_protocol( \@params, $protocol );

    return $protocol;
}

sub _add_chpdata_to_dbadata : PRIVATE {

    my ( $self, $chp, $badata ) = @_;

    if ( my $txn = $badata->getProducerTransformation() ) {

	# Don't add if there's already a protocolapp.
	unless ( scalar( @{ $txn->getProtocolApplications() || {} } ) ) {
	    my $protocol
		= $self->_create_affy_norm_protocol( $chp );
	    my $pa = Bio::MAGE::Protocol::ProtocolApplication->new(
		protocol     => $protocol,
		activityDate => 'n/a',
	    );
	    while ( my ($name, $value) = each %{ $chp->get_parameters() } ) {
		my $pv = Bio::MAGE::Protocol::ParameterValue->new(
		    parameterType => $self->get_parameter_bag->(
			$protocol->getIdentifier().$name
		    ),
		    value         => $value,
		);
		$pa->addParameterValues( $pv );
	    }
	    $txn->addProtocolApplications( $pa );
	}
    }

    $self->_add_summarystats_to_badata( $chp->get_stats(), $badata );

    return;
}
    
sub _create_affy_norm_protocol : PRIVATE {

    my ( $self, $chp ) = @_;

    my $accession = "${AFFY_PROTOCOL_PREFIX}" . $chp->get_algorithm();

    my $protocol = $self->get_protocol_bag()->(
	$accession,
	{
	    accession     => $accession,
	    type          => $OE_VAL_NORMALIZATION,
	    name          => $chp->get_algorithm(),
	}
    );

    my @params;
    my $mage_params = _mage_param_hash( $chp->get_parameters() );
    while ( my ( $name, $args ) = each %{ $mage_params } ) {
	push @params, $self->get_parameter_bag()->(
	    $accession.$name,
	    $args,
	);
    }
    $self->_add_params_to_protocol( \@params, $protocol );

    return $protocol;
}

sub _add_params_to_protocol : PRIVATE {

    my ( $self, $params, $protocol ) = @_;

    foreach my $param ( @$params ) {
	my $new_id = $param->getIdentifier();
	my $found = first {
	    $_->getIdentifier() eq $new_id
	} @{ $protocol->getParameterTypes() || [] };
	$protocol->addParameterTypes($param) unless $found;
    }

    return;
}

sub _set_pbas_channel_and_label : PRIVATE {

    my ( $self, $pbas, $labelname ) = @_;

    foreach my $pba ( @{ $pbas || [] } ) {

	# Set up the default objects.
	my $default_label = Bio::MAGE::BioMaterial::Compound->new(
	    identifier => $AE_LABELCOMPOUND_PREFIX . $labelname,
	    name       => $labelname,
	);
	my $default_channel = Bio::MAGE::BioAssay::Channel->new(
	    identifier => $AE_CHANNEL_PREFIX . $labelname,
	    name       => $labelname,
	    labels     => [ $default_label ],
	);

	# Fix the labels. We systematically replace all "unknown"
	# labels with the default label.
	if ( my $hyb = $pba->getBioAssayCreation() ) {

	    BMM:
	    foreach my $bmm ( @{ $hyb->getSourceBioMaterialMeasurements() || [] } ) {
		my $le = $bmm->getBioMaterial();

		# Make sure we're dealing with the right kind of object.
		next BMM unless $le->can('getLabels');

		my @newlabels;
		if ( my $labels = $le->getLabels() ) {
		    while ( my $l = shift @{ $labels } ) {
			if (   lc($l->getIdentifier())
			    eq lc($AE_LABELCOMPOUND_PREFIX . 'unknown') ) {
			    push @newlabels, $default_label;
			}
			else {
			    push @newlabels, $l;
			}
		    }
		}
		else {
		    push @newlabels, $default_label;
		}
		$le->setLabels( \@newlabels );
	    }
	}

	# Fix the channels. We systematically replace all "unknown"
	# channels with the default channel.
	my @newchannels;
	if ( my $channels = $pba->getChannels() ) {
	    while ( my $ch = shift @{ $channels } ) {
		if (   lc($ch->getIdentifier())
		    eq lc($AE_CHANNEL_PREFIX . 'unknown') ) {
		    push @newchannels, $default_channel;
		}
		else {
		    push @newchannels, $ch;
		}
	    }
	}
	else {
	    push @newchannels, $default_channel;
	}
	$pba->setChannels( \@newchannels );
    }

    return;
}

sub _parse_affy_datafile : PRIVATE {

    my ( $self, $file, $datarow ) = @_;

    $file->isa('ArrayExpress::Datafile')
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $datarow eq 'HASH'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $badata = $file->get_mage_badata();

    $file->set_format_type('Affymetrix');

    require ArrayExpress::Datafile::Affymetrix;
    my $factory = ArrayExpress::Datafile::Affymetrix->new();

    # dye used
    $datarow->{$EDF_DYE} ||= 'biotin';

    if ( $file->get_name() =~ m/\.CEL$/i ) {

	# Sort out the protocols (Unknown hyb protocol is the default)
	if ( $datarow->{$EDF_HYB_PROTOCOL} ) {

	    # Users can use the Affy protocol name here directly, but this
	    # is ignored if the protocol is named like a known protocol
	    # accession pattern.
	    unless ( $datarow->{$EDF_HYB_PROTOCOL}
		  =~ m!^${AFFY_PROTOCOL_PREFIX}Hybridization!
		  || $datarow->{$EDF_HYB_PROTOCOL} =~ m!^P-\w+-\w+!i )
                {
                    $datarow->{$EDF_HYB_PROTOCOL}
                        = "${AFFY_PROTOCOL_PREFIX}Hybridization-"
			. $datarow->{$EDF_HYB_PROTOCOL};
                }
	}    # See below for EXP and fallback values

	$datarow->{$EDF_SCAN_PROTOCOL} = "${AFFY_PROTOCOL_PREFIX}Scan";
	$datarow->{$EDF_FEXT_PROTOCOL} = 'P-AFFY-6';

	# Fallback value for hyb protocol
	$datarow->{$EDF_HYB_PROTOCOL}
	    ||= "${AFFY_PROTOCOL_PREFIX}Hybridization-Unknown";

	# Sort out our EXP file. First, the Tab2MAGE version:
	my $chip_type;
	if ( my $expfile
		 = $datarow->{ $EDF_FILE_PREFIX . 'exp' . $EDF_FILE_SUFFIX } ) {

	    # EXP is still the first place we look for chip type.
	    $chip_type = $self->_extract_exp_data(
		$expfile,
		$datarow,
	    );
	}

	# MAGE-TAB object creation.
	if ( $badata ) {

	    my ( $mbas, $pbas ) = $self->_get_bioassays_from_mbadata( $badata );

	    # Set the default label in cases where it's not already given.
	    $self->_set_pbas_channel_and_label($pbas, 'biotin');

	    # Next, EXP file for MAGE-TAB:
	    my $expfile;
	    foreach my $mba ( @$mbas ) {
		if ( my $nvts = $mba->getPropertySets() ) {

		    NVT:
		    foreach my $nvt ( @$nvts ) {
			if ( $nvt->getName() =~ m/\A EXP \z/ixms
			 && $nvt->getValue() =~ m/\.EXP \z/ixms ) {
			    $expfile = $nvt->getValue();
			    last NVT;  # Only process the first EXP comment.
			}
		    }
		}
	    }
	    if ( $expfile ) {
		$chip_type = $self->_add_expdata_to_mbadata(
		    $expfile,
		    $badata,
		);
	    }
	}
	    
	# Parse the CEL file, output the raw data
	my $cel = $factory->make_parser( $file->get_path() );

	if ( $self->get_use_binary_datafiles() ) {

	    # Code the Affy files natively in MAGE-ML; just populate the
	    # metadata.
	    $cel->parse_header();

	    # Make a note of the CEL version for later
	    # introduction into the MAGE-ML
	    my %typemap = (
		3 => 'CELv3',
		4 => 'CELv4',
		1 => 'CEL_AGCC',
	    );
	    my $type = $typemap{ $cel->get_version() }
		or croak( sprintf(
		    "ERROR: Unrecognized CEL version: %s\n",
		    $cel->get_version()
		));

	    # Copy the file to the target directory, note the type.
	    $self->_mageml_file_as_binary( $file, $type );
	}

	else {

	    # Parse the whole CEL file and export it as a matrix for
	    # encoding in MAGE. This will use a lot of memory for larger
	    # Affy chips.
	    $cel->parse();

	    my $target_file
		= File::Spec->catfile( $self->get_output_directory(),
				       $file->get_name() . ".proc" );
	    $file->set_target_filename(
		File::Spec->abs2rel(
		    $target_file, $self->get_output_directory()
		)
	    );
	    my $data_fh = IO::File->new( $target_file, '>' )
		or croak(
                    "Error: Unable to open output file $target_file: $!\n");
	    $cel->export($data_fh);
	}

	# Sort out our fallback chip type if no EXP file available. We
	# could go as far as parsing the CDF here, but that only works
	# for some versions and is extra overhead.
	my $stripped_cdfname;
	if ( $stripped_cdfname
		 = $datarow->{ $EDF_FILE_PREFIX . 'cdf' . $EDF_FILE_SUFFIX } ) {
	    $stripped_cdfname =~ s/\.cdf//i;
	}
	$chip_type ||= $cel->get_chip_type()
	    || $stripped_cdfname
            || 'Unknown';

	# Get the DesignElementDimension
	$file->set_ded_type('Feature');

	# If we're not fully parsing the CEL data, just use a standard DED
	if ( $self->get_use_binary_datafiles() ) {
	    $file->set_ded_identifier(
		"Affymetrix:FeatureDimension:$chip_type");
	}

	# For full CEL parsing, derive a new DED and link to that.
	else {
	    my $ded = $cel->get_ded($chip_type);
	    my $ded_identifier = $self->get_unique_de_dimension(
		$ded,
		$file->get_array_design_id(),
		$file->get_ded_type(),
	    );
	    $file->set_ded_identifier($ded_identifier);
	}

	# Define the QuantitationTypeDimension
	$file->set_column_headings( $cel->get_headings() );
	$file->check_column_headings( $self->get_quantitation_types );

	# Check here whether $file->get_mage_badata
	# set, if so navigate to MBA and add a protocolapp,
	# summarystats, software, hardware etc.
	if ( $badata ) {

	    # MAGE-TAB parsing here.
	    $self->_add_celdata_to_mbadata( $cel, $badata );
	}
	else {

	    # Tab2MAGE parsing here; dumps CEL data into $datarow for
	    # later.
	    $self->_extract_cel_data( $cel, $datarow );
	}

    }
    elsif ( $file->get_name() =~ m/\.CHP$/i ) {

	# Sort out our CDF file (note that this is backed up by
	# $chp->get_chip_type, below).
	my $cdffile
	    = $datarow->{ $EDF_FILE_PREFIX . 'cdf' . $EDF_FILE_SUFFIX };

	# Parse the CHP file, output the normalized data
	my $chp = $factory->make_parser( $file->get_path() );

	my $sighandler = $SIG{__DIE__};
	delete $SIG{__DIE__};
	local $EVAL_ERROR;
	eval {
	    $chp->parse();
	};
	$SIG{__DIE__} = $sighandler if $sighandler;

	# Handle CHP parsing failure (CHPv8 will do this).
	if ( $EVAL_ERROR ) {

	    if ( $chp->get_version == 8 ) {
		$file->set_ded_type('CompositeSequence');
		$file->set_ded_identifier(
		    "Affymetrix:CompositeSequenceDimension:"
		     . $chp->get_chip_type()
		 );

		# Copy the file to the target directory, note the
		# type.
		$self->_mageml_file_as_binary(
		    $file,
		    "CHPv" . $chp->get_version,
		);
	    }
	    else {
		croak("Error: Cannot parse CHP: $EVAL_ERROR");
	    }

	}
	else {

	    # Successful parse; write out the data, set DED.
	    $self->_output_chp_data(
		$chp,
		$file,
		$cdffile,
	    );
	}

	# Define the QuantitationTypeDimension
	$file->set_column_headings( $chp->get_headings() );
	$file->check_column_headings( $self->get_quantitation_types );

	# Check here whether $file->get_mage_badata
	# set, if so navigate to DBAD and add a protocolapp,
	# summarystats.
	if ( $badata ) {

	    # MAGE-TAB.
	    $self->_add_chpdata_to_dbadata( $chp, $badata );
	}
	else {

	    # Tab2MAGE. Extract CHP data into $datarow.
	    $self->_extract_chp_data( $chp, $datarow );
	}
    }
    else {
	croak(
            sprintf(
		qq{Error: unrecognized file type for file "%s" (Affymetrix must be .CEL or .CHP)\n},
		$file->get_name(),
	    ),
	);
    }

    return;
}

sub _output_chp_data : PRIVATE {

    my ($self, $chp, $file, $cdffile) = @_;

    my $array_accession = $file->get_array_design_id()
	or croak("Error: Datafile has no array accession.");

    $cdffile ||= $chp->get_chip_type() . ".cdf";

    # Parse the CDF, unless we have it already cached.
    my $cdf;
    unless ( $cdf = $self->get_cdf_cache(lc($cdffile)) ) {

	my $factory = ArrayExpress::Datafile::Affymetrix->new();

	# Check Affy library dir or source dir or working dir.
	my ($actualcdf, $filepath) = find_cdf(
	    $cdffile,
	    $self->get_source_directory(),
	);

	unless ( $actualcdf ) {
	    croak("Error: CDF file $cdffile not found\n");
	}

	$cdf = $factory->make_parser($filepath);
	$cdf->parse();
	$self->set_cdf_cache( lc($cdffile), $cdf );
    }

    # Output the stripped data.
    my $target_file = File::Spec->catfile(
	$self->get_output_directory(),
	$file->get_name() . ".proc",
    );

    $file->set_target_filename(
	File::Spec->abs2rel( $target_file, $self->get_output_directory() )
    );

    my $data_fh = IO::File->new( $target_file, '>' )
	or croak("Error: Unable to open output file $target_file: $!\n");

    $chp->export( $data_fh, $cdf );

    # Set the DesignElementDimension.
    my $ded = $chp->get_ded($cdf);
    $file->set_ded_type('CompositeSequence');
    my $ded_identifier = $self->get_unique_de_dimension(
	$ded,
	$array_accession,
	$file->get_ded_type(),
    );
    $file->set_ded_identifier($ded_identifier);

    return;
}

sub _parse_binary_nimblescan : PRIVATE {

    my ( $self, $file ) = @_;

    $file->isa('ArrayExpress::Datafile')
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # Enter a dummy DED identifier (no longer used, code kept here in
    # case this policy ever changes).

#    my $ded_type = 'Reporter';
#    $file->set_ded_type($ded_type);
#    my $ded_identifier = $self->get_unique_de_dimension(
#	[sprintf( "ebi.ac.uk:Nimblegen:Reporter:%s", $file->get_array_design_id() )],
#	$file->get_array_design_id(),
#	$ded_type
#    );
#    $file->set_ded_identifier($ded_identifier);

    # Make sure we know which software is being used.
    my $rc = $file->check_column_headings( $self->get_quantitation_types );
    if ($rc) {
        print { $self->get_error_fh() } ( "ERROR: " . $file->get_name() . ": $rc" );
    }

    # Copy the file to the target dir, label it with a
    # dataExternal format tag (This tag will probably change FIXME).
    my $format = (   $file->get_format_type() eq 'NimbleScanFeature'
		  || $file->get_format_type() eq 'NimbleScanNorm')
               ? 'NimbleScan'
	       : $file->get_format_type();
    $self->_mageml_file_as_binary( $file, $format );

    return;
}

sub _mageml_file_as_binary : PRIVATE {

    # Takes a Datafile object and a type string (CELv3, CELv4
    # etc.). Copies the actual file across to the target MAGE-ML
    # directory, stores the type for use later.

    my ( $self, $file, $type ) = @_;

    $file->isa('ArrayExpress::Datafile')
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # For MAGE-TAB, rewrite the referenced filename back to its
    # original form.
    if ( my $data = $file->get_mage_badata() ) {
	my $bdc = $data->getBioDataValues();
	$bdc->setCube( $file->get_name() );
    }

    # Copy the untouched binary file across to the target directory
    my $target_file
        = File::Spec->rel2abs( $file->get_name(), $self->get_output_directory() );
    $file->set_target_filename( $file->get_name() );
    if ( $file->get_path ne $target_file ) {
        copy( $file->get_path, $target_file )
            or croak( "ERROR: Unable to copy binary data file "
                . $file->get_path
                . " into target directory: $!\n" );
    }
    else {
        croak(    q{ERROR: Source and target filenames are identical}
               . qq{ ("$target_file"). Copying may destroy the original file.}
	        . q{ Please use alternative target directory.});
    }

    # Make a note of the file type (CELv3, CELv4, NimbleScan).
    if ($type) {
        $self->set_cel_types( $file->get_target_filename() => $type );
    }
    else {
        croak("ERROR: No binary file type specified");
    }

    return;
}

sub _parse_text_datafile : PRIVATE {

    my ( $self, $file, $datarow ) = @_;

    $file->isa('ArrayExpress::Datafile')
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $datarow eq 'HASH'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # Default Array accession. This is changed if we encounter post-Affy
    # data files (e.g. RMA data) to the name of the array. Used in
    # constructing the CompositeSequence_refs.
    my $arrayaccession = $file->get_array_design_id();

    # This will hold a list of coordinate columns to be discarded.
    my @unwanted_coord_columns;

    # Figure out our line ending style (FIXME this should now be superfluous).
    local $INPUT_RECORD_SEPARATOR = $file->get_linebreak_type();

    # Figure out what kind of file we have, get column headings and indices
    $file->parse_header();

    unless ( scalar @{ $file->get_column_headings() } ) {
        print STDERR (
           "Error: No column headings for file " . $file->get_name() . "\n" );
    }

    # Used for AffyNorm; if not undef, will be used instead of the array
    # accession to generate DesignElement ids.
    my $chip_type;

    FILEFORMAT:
    {

        # NimbleScan data files may be coded as binary.
        if ( $self->get_use_binary_datafiles()
            && ( $file->get_format_type =~ m/\A Nimble(Scan|gen)/xms ) ) {
            $self->_parse_binary_nimblescan( $file );
            return;
        }

	# For Illumina, check whether we have per-hyb data or not,
	# prior to fixing anything.
	elsif (   $file->get_format_type() =~ /^Illumina/
	       && $file->is_illumina_fgem() ) {
	    $file->rewrite_illumina_as_fgem();
	}

        # Next, scan through known file formats which need fixing
        # (GenePix, etc.)
	else {
	    my $rc = $file->fix_known_text_format();
	    last FILEFORMAT if $rc;
	}

        # Other minor tweaks. These first few are basically FGEM:
        if ( $file->get_format_type eq 'FGEM_CS' ) {
            $file->set_format_type('FGEM');
        }
        elsif ( $file->get_format_type eq 'GEO' ) {
            $file->set_format_type('FGEM');

	    # ID_REF is assumed to be CompSeq ID.
	    my $cols = $file->get_column_headings();
	    $cols->[ $file->get_index_columns()->[0] ]
		= 'CompositeSequence Identifier';
	    $file->set_column_headings($cols);
        }
        elsif ( $file->get_format_type eq 'AffyNorm' ) {
            $file->set_format_type('FGEM');

            # Now we know the dye
            $datarow->{$EDF_DYE} ||= 'biotin';

            # Use the name of the CDF file as our array name.
            # FIXME to use strings from EXP, CEL, CHP where available.
            if ( my $cdffile
                = $datarow->{ $EDF_FILE_PREFIX . 'cdf' . $EDF_FILE_SUFFIX } ) {
                $cdffile =~ s/\.cdf$//i;
                $chip_type = $cdffile;
            }

	    # Fall back to unknown.
	    $chip_type ||= 'Unknown';

	    # Record the chip_type for these data matrix files.
	    unless ( $file->get_dm_chip_type() ) {
		$file->set_dm_chip_type($chip_type);
	    }
        }

    }    # end FILEFORMAT

    # Figure out which columns we want to keep, populate heading_qts,
    # heading_hybs etc.
    my $rc = $file->check_column_headings( $self->get_quantitation_types );
    if ($rc) {
        printf { $self->get_error_fh() } ( "ERROR: %s: %s", $file->get_name(), $rc );
    }

    # We've made this generic by now, or at least we should have (Affy
    # would crash here, but we've already diverted it).
    unless ( $file->get_format_type() eq 'Generic'
        or $file->get_format_type() eq 'FGEM' ) {
        croak(    "Error: file "
                . $file->get_name()
                . " not in recognized format: "
                . ( $file->get_format_type() || 'Unknown' )
                . "\n" );
    }

    # FGEM (combined) data is treated as a special case - column
    # headings do not equate directly to QTs.
    my @column_headings = @{ $file->get_column_headings };

    # Next, select only our columns of interest (based on QT name),
    # write the outcome to log file.
    my @new_column_headings;
    foreach my $heading (@column_headings) {
        if ( $file->get_data_metrics()->{$heading} ) {
            push( @new_column_headings, $heading );
            print { $self->get_error_fh() }
                ( $file->get_name() . ": Recognized column kept: $heading\n" );
        }
	elsif ( $file->is_ignored_qt($heading) ) {
            print { $self->get_error_fh() }
                ( $file->get_name() . ": Stripping unwanted column: $heading\n" );
	}
        elsif ( $self->get_allow_undef_qts() ) {
            push( @new_column_headings, $heading );
            print { $self->get_error_fh() }
                ( $file->get_name() . ": Keeping unrecognized column: $heading\n" );
        }
        else {
            print { $self->get_error_fh() }
                ( $file->get_name() . ": Omitting unrecognized column: $heading\n" );
        }
    }

    # Set up some target filenames ($file->get_name() will be modified by
    # strip_and_sort).
    my $temporary_file = File::Spec->catfile( $self->get_output_directory(),
        $file->get_name() . ".temporary",
    );
    my $target_file = File::Spec->catfile( $self->get_output_directory(),
        $file->get_name() . ".proc" );

    # Strip the file of unwanted columns, sort on the indices (coord
    # or identifier), and move those indices to the leftmost
    # columns. The Datafile is relinked to the temp filehandle.
    $file->strip_and_sort( $temporary_file, \@new_column_headings );

    # Open output filehandle
    my $output_fh = IO::File->new( $target_file, '>' )
        or croak("Error opening output file $target_file: $!\n");

    # Generate DesignElementDimension, while stripping the feature
    # coordinates/identifiers from the data file. This also relinks
    # the Datafile object to the output filehandle.
    $self->_create_designelementdimension(
	$file,
        $output_fh,
	( $chip_type || $arrayaccession ),
    );

    # End of DesignElementDimension processing
    unlink($temporary_file)
        or carp(
        "Warning: Unable to delete temporary file $temporary_file: $!\n");

    $file->set_target_filename(
        File::Spec->abs2rel( $target_file, $self->get_output_directory() ) );

    return;
}

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../../index.html">
                <img src="../../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: ExperimentChecker.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Datafile::Parser - a module handling Datafile parsing

=head1 SYNOPSIS

 use ArrayExpress::Datafile::Parser;
 my $parser = ArrayExpress::Datafile::Parser->new;

=head1 DESCRIPTION

The ArrayExpress::Datafile module provides methods for
parsing individual data files. This module provides a global handler
for Datafile objects.

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2005.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

1;

