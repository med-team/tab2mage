#!/usr/bin/env perl
#
# Module to help generation of DataMatrix MAGE objects.
#
# Tim Rayner 2005, ArrayExpress team, European Bioinformatics Institute
#
# $Id: DataMatrix.pm 2069 2008-06-04 14:33:52Z tfrayner $
#

package ArrayExpress::Datafile::DataMatrix;

use strict;
use warnings;

use Carp;
use English qw( -no_match_vars );
use Class::Std;

use Bio::MAGE qw(:ALL);

require ArrayExpress::Datafile;
use ArrayExpress::Curator::MAGE qw(
    update_factorvalues
    unique_identifier
);

use ArrayExpress::Curator::Config qw($CONFIG);

use ArrayExpress::Curator::MAGE::Definitions qw(
    $EDF_TRANSFORMATIONNAME
    $EDF_TRANSFORMATIONTYPE
    $EDF_TRXN_PROTOCOL
    $EDF_TRXN_SOFTWARE
    $OE_VAL_NORMALIZATION
    $OE_VAL_TRANSFORMATION_SOFTWARE
);

sub get_dimension_lists {

    # Takes an AoA of hyb names and an array of qt names, returns the
    # ba and qt dimensions as arrayrefs of names, and a BioDataCube
    # order string.

    my ( $self, $datamatrix_hybs, $datamatrix_qts ) = @_;

    ref $datamatrix_qts eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $datamatrix_hybs eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $previous_qts = {};
    my $ba_namerefs  = [];
    my $qt_names     = [];
    my $order;
    my $period;

    # Use hybs as the benchmark; QTs may have been dropped on parsing.
    for ( my $i = 0; $i <= $#$datamatrix_hybs; $i++ ) {

        # Get the variables for this column, for readability.
        my $qt      = $datamatrix_qts->[$i];
        my $ba_list = $datamatrix_hybs->[$i];

        # QT dimension
        my $qt_seen;
        if ($qt) {
            unless ( $qt_seen = $previous_qts->{$qt} ) {
                push( @$qt_names, $qt );
                $previous_qts->{$qt}++;
            }
        }

        # BA dimension
        my %ba_key = map { $_ => 1 } sort @{$ba_list};
        my $ba_seen;
        foreach my $old (@$ba_namerefs) {

            # Compare arrays $bioassays and $old.
            # Set $ba_seen unless there is a difference.
            $ba_seen++ unless ( grep { !exists $ba_key{$_} } sort @$old );
        }
        unless ($ba_seen) {

            # add the bioassay to @$ba_namerefs
            push( @$ba_namerefs, $ba_list );
        }

        # Set our BioDataCube order. This should be settled by the
        # second DataMatrix column. Setting this on any other iteration
        # leads to spurious results. See below for the special cases
        # where either B or Q is 1.
        if ( $i == 1 ) {
            if ( $qt_seen && !$ba_seen ) { $order ||= 'DQB'; }
            if ( $ba_seen && !$qt_seen ) { $order ||= 'DBQ'; }
        }

        # Confirm that the detected order is followed in subsequent columns.
        elsif ( $i > 1 && $order ) {

            # Set the periodicity of the file as soon as the next (qt
            # || ba) is seen (dependent on the order of the file).
            if ( !$period ) {
                if ( $order eq 'DBQ' && !$ba_seen ) {
                    $period = scalar @$qt_names;
                }
                elsif ( $order eq 'DQB' && !$qt_seen ) {
                    $period = scalar @$ba_namerefs;
                }
            }

            # First block done, $order is determined, now we start
            # checking that the rest of the file is okay.
            if ($period) {
                my $prev_qt      = $datamatrix_qts->[ $i - 1 ];
                my $prev_ba_list = $datamatrix_hybs->[ $i - 1 ];

                if ( $order eq 'DBQ' ) {
                    if ( $qt ne $qt_names->[ $i % $period ] ) {
                        $order = undef;
                    }
                    if (( $i % $period != 0 )
                        && (grep { !exists $ba_key{$_} }
                            sort @{$prev_ba_list}
                        )
                        ) {
                        $order = undef;
                    }
                }

                elsif ( $order eq 'DQB' ) {
                    if (grep { !exists $ba_key{$_} }
                        sort @{ $ba_namerefs->[ $i % $period ] }
                        ) {
                        $order = undef;
                    }
                    if ( ( $i % $period != 0 ) && ( $qt ne $prev_qt ) ) {
                        $order = undef;
                    }
                }
            }
        }
    }

    # One extra check for special case where either Q==1 or B==1
    # We prefer DBQ as ArrayExpress can handle it.
    if ( @$qt_names == 1 || @$ba_namerefs == 1 ) { $order = 'DBQ'; }

    if ($order) {
        return ( $ba_namerefs, $qt_names, $order );
    }
    else {
        return ( [], [], $order );
    }

}

sub get_bioassay_qts : RESTRICTED {

    my ($self, $bioassays) = @_;

    ref $bioassays eq 'ARRAY'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # Find the highest-level QTs available for QuantitationTypeMapping
    # N.B. this assumes a few things - that BioAssayData is associated
    # with BioAssay (MAGE best practice), and that we've fully created
    # these objects before trying to add the DataMatrix.
    my %source_qts;
    my %source_data;
    foreach my $bioassay (@$bioassays) {
        my $datalist;
        if ( $bioassay->isa('Bio::MAGE::BioAssay::MeasuredBioAssay') ) {
            $datalist = $bioassay->getMeasuredBioAssayData;
        }
        elsif ( $bioassay->isa('Bio::MAGE::BioAssay::DerivedBioAssay') ) {
            $datalist = $bioassay->getDerivedBioAssayData;
        }
        if ($datalist) {
            foreach my $data (@$datalist) {
                $source_data{ $data->getIdentifier } = $data;
                my $source_qtd;
                if (   ( $source_qtd = $data->getQuantitationTypeDimension )
                    && ( my $source_qts = $source_qtd->getQuantitationTypes )
                    ) {
                    foreach my $qt (@$source_qts) {
                        $source_qts{ $qt->getIdentifier } = $qt;
                    }
                }
            }
        }
    }

    my @sorted_qts  = map { $source_qts{$_} } sort keys %source_qts;
    my @sorted_data = map { $source_data{$_} } sort keys %source_data;

    return ( \@sorted_qts, \@sorted_data );
}

sub create_mage {

    my ( $self, $file, $datarow, $tab2mage, $identifier_template ) = @_;

    $file->isa('ArrayExpress::Datafile')
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $datarow eq 'HASH'
        or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    $tab2mage->isa('ArrayExpress::Curator::Tab2MAGE')
	or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $identifier_template
        and confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $datamatrix_hybs = $file->get_heading_hybs();
    my $datamatrix_qts  = $file->get_heading_qts();

    my $bag_of    = $tab2mage->get_bags();

    # Construct the dimensions
    my ( $ba_namerefs, $qt_names, $order )
        = $self->get_dimension_lists( $datamatrix_hybs, $datamatrix_qts );

    # Internal consistency check
    warn("Warning: Order of DataMatrix data cube is indeterminate.\n")
        unless $order;

    # Warn on unsupported cube order
    if ( $order && $order eq 'DQB' ) {
        warn(
            qq{WARNING: A BioDataCube order of "$order" is not supported by the ArrayExpress MAGE-ML loader.\n}
        );
    }

    # Create the actual qtd here
    my ( $qt_list, $qtd_key ) = $tab2mage->qts_from_names( $file, $qt_names );

    $identifier_template .= '.DataMatrix.' . unique_identifier;

    my $qtd = $bag_of->{qtd}->(
        $qtd_key,
        {   qt_list             => $qt_list,
            identifier_template => $identifier_template,
        }
    );
    $file->set_mage_qtd($qtd);

    # The per-column bioassays are created here.
    my $ba_list = [];
    my %source_qts;
    my %source_data;
    foreach my $listref (@$ba_namerefs) {

        my $bioassay;

	if ( $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE() ) {

	    # DBA can map to bag_of dba, mba or pba propagated up from
	    # pba (and one day directly to dba). QT and data mapping info needed.
	    my ( $qts, $datalist );
	    ( $bioassay, $qts, $datalist ) = $self->find_or_create_dm_dba(
		    $listref,
		    $bag_of,
		    $identifier_template,
		);
	    foreach my $qt   (@$qts) {
		$source_qts { $qt->getIdentifier   } = $qt;
	    }
	    foreach my $data (@$datalist) {
		$source_data{ $data->getIdentifier } = $data;
	    }
	}
	elsif ( $file->get_data_type() eq $CONFIG->get_RAW_DM_FILE_TYPE() ) {

	    # MBA maps only to PBA although multiple PBAs may be
	    # combined into a single column.
	    $bioassay = $self->find_or_create_mba(
		    $listref,
		    $bag_of,
		    $identifier_template,
		);
	}
	else {
	    croak(sprinf("Error: unknown DataMatrix data_type %s\n",
			 $file->get_data_type()) );
	}
        push( @$ba_list, $bioassay );
    }

    # Top-level DBA and DBAD. Note that a raw data matrix should
    # ideally be coded as MBA here, but due to MAGEv1.1 limitations
    # that's not possible. Instead we create a DBA pointing at the
    # per-hyb MBAs.
    $self->generate_top_level_dba_and_dbad(
	$file,
	$ba_list,
	\%source_qts,
	\%source_data,
	$datarow,
	$tab2mage,
	$order,
	$identifier_template,
    );

    return;
}

sub generate_top_level_dba_and_dbad : PRIVATE {

    my ($self,
	$file,
	$ba_list,
	$source_qts,
	$source_data,
	$datarow,
	$tab2mage,
	$order,
	$identifier_template,
    ) = @_;

    ###################
    # DerivedBioAssay #
    ###################

    my $bag_of = $tab2mage->get_bags();

    # Convention dictates that Transformation name is 1:1 with file name.
    my $trxn_name = $datarow->{$EDF_TRANSFORMATIONNAME} || $file->get_name();

    # BioAssayMap and DBA both have the same $trxn_name internal identifier
    my $dba = $bag_of->{datamatrix_dba}->(
        $trxn_name,
        {   identifier_template => $identifier_template,
            normalization_type  => $datarow->{$EDF_TRANSFORMATIONTYPE},
            source_bioassays    => $ba_list,
	    bioassaymap_bag     => $bag_of->{bam},
        },
    );

    # BioAssayDimension and BioAssayMap
    my $ba_dimension = Bio::MAGE::BioAssayData::BioAssayDimension->new(
        identifier => "$identifier_template.DerivedBioAssayDimension",
        bioAssays  => $ba_list,
    );
    my $ba_map = $dba->getDerivedBioAssayMap;

    # Propagate the FVs to the new DBA
    my %fv_list;
    foreach my $bioassay (@$ba_list) {
        if ( $bioassay->getBioAssayFactorValues ) {
            foreach my $fv ( @{ $bioassay->getBioAssayFactorValues } ) {
                $fv_list{ $fv->getIdentifier } = $fv;
            }
        }
    }
    $dba->setBioAssayFactorValues( [ values %fv_list ] );

    #######################
    # DerivedBioAssayData #
    #######################
    my $protocol;
    $datarow->{$EDF_TRXN_PROTOCOL} && do {
        $protocol = $bag_of->{protocol}->(
            $datarow->{$EDF_TRXN_PROTOCOL},
            { protocol_type => $OE_VAL_NORMALIZATION }
        );
    };

    my $trxn_software    = q{};
    my $software_version = q{};
    $datarow->{$EDF_TRXN_SOFTWARE} && do {
        my $name;
        ( $name, $software_version )
            = $tab2mage->parse_software( $datarow->{$EDF_TRXN_SOFTWARE} );
        $trxn_software = $bag_of->{software}->(
            "TRXN:$name" . ( $software_version ? ":$software_version" : q{} ),
            {   identifier_template => "$identifier_template.Transformation",
                name                => $name,
                type                => $OE_VAL_TRANSFORMATION_SOFTWARE,
            }
        );
    };

    # Allow empty ded_type, e.g. for checker graph visualization.
    my $ded;
    if ( my $dedtype = $file->get_ded_type() ) {
	my $dedclass
	    = "Bio::MAGE::BioAssayData::" . $file->get_ded_type() . "Dimension";
	$ded = $dedclass->new( identifier => $file->get_ded_identifier() );
    }

    my @sorted_qts  = map { $source_qts->{$_} } sort keys %$source_qts;
    my @sorted_data = map { $source_data->{$_} } sort keys %$source_data;

    my $dbad = $bag_of->{datamatrix_dbad}->(
        $file->get_name(),
        {   filename                => $file->get_target_filename(),
            de_dimension            => $ded,
            qt_dimension            => $file->get_mage_qtd(),
            ba_dimension            => $ba_dimension,
            identifier_template     => $identifier_template,
            protocol                => $protocol,
            source_bioassay_dataset => \@sorted_data,
            bioassay_map            => $ba_map,
            software                => $trxn_software,
            software_version        => $software_version,
            order                   => $order,
            source_qts              => \@sorted_qts,
            qtm_bag                 => $bag_of->{qtm},
        }
    );

    # Add data to the DBA.
    $tab2mage->dba_add_data( $dba, $dbad );

    return;
}

sub mbas_from_pbas : PRIVATE {

    # Get a list of MBAs which were derived from a given list of PBAs.
    my ( $self, $pbas, $bag_of ) = @_;
    my @mbas;

    foreach my $pba (@$pbas) {

	# Handle the two-colour hyb coding. Assumes that merged PBAs
	# have a .merged name suffix.
	if (    $pba->getBioAssayCreation()
	     && ( scalar @{ $pba->getChannels() || [] } > 1 ) ) {

	    # FIXME we want to follow the BioAssayTreatment chain
	    # here, instead of relying on naming conventions.
	    my $pba_name = $pba->getName();
	    $pba = $bag_of->{extended_pba}->(
		$pba->getName() . ".merged"
	    );
	    unless ( $pba ) {
		die(sprintf(
		    "Internal error: Merged PBA undefined for two-color coding (%s).",
		    $pba_name)
		);
	    }
	}

        foreach my $bioassay ( @{ $bag_of->{mba}->() } ) {
            my $fext = $bioassay->getFeatureExtraction;
            if ( $fext->getPhysicalBioAssaySource()->getIdentifier eq
                $pba->getIdentifier ) {
                push( @mbas, $bioassay );
            }
        }
    }
    return \@mbas;
}

sub dbas_from_mbas : PRIVATE {

    # Get a list of non-DataMatrix DBAs derived from a given list of MBAs.
    my ( $self, $mbas, $bag_of ) = @_;
    my @dbas;

    foreach my $mba (@$mbas) {
        foreach my $dba ( @{ $bag_of->{dba}->() } ) {
            my @bams = @{ $dba->getDerivedBioAssayMap() || [] };
            foreach my $bam (@bams) {
                foreach my $source ( @{ $bam->getSourceBioAssays || [] } ) {
                    if ( $source->getIdentifier eq $mba->getIdentifier ) {
                        push( @dbas, $dba );
                    }
                }
            }
        }
    }
    return \@dbas;
}

sub retrieve_bioassay : RESTRICTED {

    # Here we check for the existence of a bioassay with internal key
    # $heading_hyb, and die if there isn't. This prevents a much
    # messier crash later on. Return the MAGE object if found.

    my ( $self, $heading_hyb, $ba_bag, $id_column, $fallback_bag ) = @_;

    $id_column ||= 'hybridization/normalization ID';

    # We don't want errors here to email the autosubmissions system admin.
    my $sighandler = $SIG{__DIE__};
    delete $SIG{__DIE__};

    my $bioassay = $ba_bag->( $heading_hyb );

    # Fallback if possible (e.g. MAGE-TAB mapping to Scan for
    # single-hyb experiments).
    if ( ! $bioassay && $fallback_bag ) {
	$bioassay = $self->find_pba_for_bat( $heading_hyb, $fallback_bag );
    }
    unless ( $bioassay ) {
	croak("ERROR: unrecognized data matrix $id_column:"
	    . " $heading_hyb\n");
    }

    # Reinstate the __DIE__ signal handler.
    $SIG{__DIE__} = $sighandler if $sighandler;

    return $bioassay;
}

sub find_pba_for_bat : PRIVATE {

    my ( $self, $heading_hyb, $bag ) = @_;

    foreach my $bioassay ( @{ $bag->() } ) {
	foreach my $bat ( @{ $bioassay->getBioAssayTreatments() || [] } ) {

	    # Success.
	    return $bioassay if ( $bat->getName() eq $heading_hyb );
	}
    }
    return;   # Failure.
}

sub find_or_create_mba : PRIVATE {

    my ( $self, $hybs, $bag_of, $identifier_template ) = @_;

    ref $hybs eq 'ARRAY' or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $bag_of eq 'HASH' or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $identifier_template
        and confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my %contributing_bioassays;

    my %overall_pba;

    foreach my $heading_hyb (@$hybs) {

	my $pba = $self->retrieve_bioassay( $heading_hyb, $bag_of->{pba} );
	$overall_pba{ $pba->getName() } = $pba;
    }

    # Either return the column bioassay, or a new ba mapping to
    # multiple column bioassays.
    my $datamatrix_bioassay = $self->find_or_create_per_column_mba(
	[ values %overall_pba ],
	$identifier_template,
	$bag_of,
    );

    return $datamatrix_bioassay;
}

sub find_or_create_dm_dba : PRIVATE {

    my ( $self, $hybs, $bag_of, $identifier_template ) = @_;

    ref $hybs eq 'ARRAY' or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $bag_of eq 'HASH' or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $identifier_template
        and confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my %contributing_bioassays;

    my %known_pba_names = map { $_->getName => 1 } @{ $bag_of->{pba}->() };

    # We need to propagate PBA object up to top-level MBA/DBA for
    # correct mapping.
    my @overall_mbas;
    foreach my $heading_hyb (@$hybs) {

	my $pba;

	# This may fail if the data matrix maps to Normalizations
	# rather than Hybridizations.
	eval {
	    $pba = $self->retrieve_bioassay( $heading_hyb, $bag_of->{pba} );
	};

	if ($EVAL_ERROR) {

	    # Try again with DBAs if PBAs no good. We now allow this
	    # to croak on failure.
	    my $dba = $self->retrieve_bioassay( $heading_hyb, $bag_of->{dba} );
	    $contributing_bioassays{ $dba->getIdentifier } = $dba;
	}
	else {

	    # Get lists of MBAs and/or DBAs linked to the PBA. N.B. Any
	    # DBAs returned here should be certified non-DataMatrix.
	    my $mbas = $self->mbas_from_pbas( [$pba], $bag_of );
	    my $dbas = $self->dbas_from_mbas( $mbas, $bag_of );

	    push( @overall_mbas, @$mbas );

	    # Get a uniqued list of the highest-level bioassays possible
	    if ( scalar(@$dbas) ) {
		foreach my $bioassay (@$dbas) {
		    $contributing_bioassays{ $bioassay->getIdentifier }
			= $bioassay;
		}
	    }
	    elsif ( scalar(@$mbas) ) {
		foreach my $bioassay (@$mbas) {
		    $contributing_bioassays{ $bioassay->getIdentifier }
			= $bioassay;
		}
	    }
	    else {
		$contributing_bioassays{ $pba->getIdentifier } = $pba;
	    }
	}
    }

    # Get a list of QTs to use in QuantitationTypeMap(ping).
    my ( $source_qts, $source_data )
        = $self->get_bioassay_qts( [ values %contributing_bioassays ] );

    # If no QTs returned from the top level, try again with all the MBAs
    # we found.
    unless (@$source_qts) {
        ( $source_qts, $source_data )
	    = $self->get_bioassay_qts( \@overall_mbas );
    }

    # Either return the column bioassay, or a new ba mapping to
    # multiple column bioassays.
    my $datamatrix_bioassay = $self->find_or_create_per_column_bioassay(
	[ values %contributing_bioassays ],
	$identifier_template,
	$bag_of->{datamatrix_dba},  # future derived matrices can't map to these.
	$bag_of->{bam},
    );

    return ( $datamatrix_bioassay, $source_qts, $source_data );
}

sub find_or_create_per_column_mba : PRIVATE {

    # Get a list of MBAs from the PBAs; pass this on up to
    # find_or_create_per_column_bioassay to either return a single
    # MBA or a DBA linked to multiple MBAs (e.g. from multiple scans).
    my ( $self,
	 $pbas,
	 $identifier_template,
	 $bag_of,
     ) = @_;

    if ( scalar( @$pbas ) > 1 ) {

	# This is a bit of a MAGE limitation - we'd need to come up
	# with some kind of PBA-based hack to support this. These
	# cases should be using DerivedBioAssays anyway.
	croak(
	    "Error: Cannot map to multiple hybridizations in a single data matrix column."
	);
    }
    elsif ( scalar( @$pbas ) != 1 ) {

	# This should be unusual, we crash earlier in such cases.
	croak(
	    "Error: Cannot retrieve PBA information."
	);
    }

    my $mbas = $self->mbas_from_pbas( $pbas, $bag_of );

    # Check that @$mbas has entries, if not create one and return it.
    my $bioassay;

    if ( scalar @$mbas ) {

	$bioassay = $self->find_or_create_per_column_bioassay(
	    $mbas,
	    $identifier_template,
	    $bag_of->{dba},    # future derived matrices can map to these.
	    $bag_of->{bam},
	);
    }
    else {

	# MBA creation here.
	my $unique = unique_identifier();
	my $pba    = $pbas->[0];
	my $mba    = $bag_of->{mba}->(
	    $pba->getName(),
	    {
		identifier_template => $unique,
		physical_bioassay   => $pba,
	    }
	);

	# Propagate FVs from the PBA.
	$self->update_bioassay_fvs(
	    $mba,
	    $pba->getBioAssayFactorValues(),
	);

	$bioassay = $mba;
    }

    return $bioassay;
}

sub find_or_create_per_column_bioassay : RESTRICTED {

    my ( $self,
	 $source_bioassays,
	 $identifier_template,
	 $bioassay_bag,
	 $bam_bag,
     ) = @_;

    # $datamatrix_bioassay is the returned bioassay. It will be either a
    # single contributing bioassay, or a new bioassay mapping to
    # multiple contributing bioassays.
    my $datamatrix_bioassay;

    # Create BAM from @$source_bioassays unless there's only
    # one bioassay involved.
    if ( scalar( @$source_bioassays ) == 1 ) {

        # Only one bioassay; simple case
        $datamatrix_bioassay = $source_bioassays->[0];
    }

    else {

        # Multiple bioassays specified
        my $unique = unique_identifier;
        my @ba_names;
        foreach my $bioassay ( @$source_bioassays ) {
            push( @ba_names, $bioassay->getName() );
        }
        my $name = join( ";", @ba_names ) || "DataMatrix.$unique";
        my $identifier = "$identifier_template.$unique";
        $datamatrix_bioassay = $bioassay_bag->(
            $name,
            {   identifier_template => $identifier,
                source_bioassays    => $source_bioassays,
		bioassaymap_bag     => $bam_bag,
            },
        );

        # Propagate FVs
        foreach my $bioassay ( @$source_bioassays ) {
            $self->update_bioassay_fvs( $datamatrix_bioassay,
                $bioassay->getBioAssayFactorValues() );
        }
    }

    return $datamatrix_bioassay;
}

sub update_bioassay_fvs : RESTRICTED {

    # Wrapper method for updating the FVs associated with a bioassay.
    my ( $self, $bioassay, $fvs ) = @_;

    update_factorvalues( $bioassay, $fvs );

    return;
}

1;
