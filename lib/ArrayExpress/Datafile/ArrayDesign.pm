#!/usr/bin/env perl
#
# ArrayDesign.pm
#
# Tim Rayner 2008 ArrayExpress team, EBI
#
# $Id: ArrayDesign.pm 1987 2008-03-06 11:29:22Z tfrayner $
#

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: ExperimentChecker.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Datafile::ArrayDesign - memory-efficient handling of
array design information.

=head1 SYNOPSIS

 use ArrayExpress::Datafile:::ArrayDesign

=head1 DESCRIPTION

ArrayDesign objects are used during experiment checking to handle
array design elements in a memory-efficient manner.

=head2 Accessor methods

=over 2

=item set_accession

Setter method for the array accession number relating to this design.

=item get_accession

Getter method for the array accession number relating to this design.

=item set_adf_features

Setter method for a hashref with keys which are period-delim strings
of the feature coordinates found in the array design associated with
the file.

=item get_adf_features

Getter method for a hashref with keys which are period-delim strings
of the feature coordinates found in the array design associated with
the file.

=item set_adf_reporters

Setter method for a hashref with keys which are the reporter
identifiers found in the array design associated with the file.

=item get_adf_reporters

Getter method for a hashref with keys which are the reporter
identifiers found in the array design associated with the file.

=item set_adf_compseqs

Setter method for a hashref with keys which are the composite sequence
identifiers found in the array design associated with the file.

=item get_adf_compseqs

Getter method for a hashref with keys which are the composite sequence
identifiers found in the array design associated with the file.

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2004.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

package ArrayExpress::Datafile::ArrayDesign;

use strict;
use warnings;

use Carp;
use IO::File;
use Class::Std;
use Storable qw(store_fd fd_retrieve);

use ArrayExpress::Curator::Config qw($CONFIG);

my %feature_fh    : ATTR( :default<undef> ); 
my %reporter_fh   : ATTR( :default<undef> ); 
my %compseq_fh    : ATTR( :default<undef> );

my %accession     : ATTR( :name<accession>, :default<undef> );

sub set_features {

    my ( $self, $features ) = @_;

    $self->set_design_elements( \%feature_fh, $features );
}

sub get_features {

    my ( $self ) = @_;

    return $self->get_design_elements( \%feature_fh );
}

sub set_reporters {

    my ( $self, $reporters ) = @_;

    $self->set_design_elements( \%reporter_fh, $reporters );
}

sub get_reporters {

    my ( $self ) = @_;

    return $self->get_design_elements( \%reporter_fh );
}

sub set_compseqs {

    my ( $self, $compseqs ) = @_;

    $self->set_design_elements( \%compseq_fh, $compseqs );
}

sub get_compseqs {

    my ( $self ) = @_;

    return $self->get_design_elements( \%compseq_fh );
}

sub set_design_elements : PRIVATE {

    my ( $self, $attr_var, $elements ) = @_;

    defined($attr_var)
	or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );
    ref $elements eq 'HASH'
	or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    my $fh = IO::File->new_tmpfile();

    store_fd( $elements, $fh )
	or confess("Error caching array design element list: $!");
    seek($fh, 0, 0) or confess("Error rewinding design element cache file: $!");

    $attr_var->{ident $self} = $fh;

    return;
}

sub get_design_elements : PRIVATE {

    my ( $self, $attr_var ) = @_;

    defined($attr_var)
	or confess( $CONFIG->get_ERROR_MESSAGE_ARGS() );

    # Return empty hash if array not in cache.
    my $elements = {};
    if ( my $fh = $attr_var->{ident $self} ) {

	my $pos = tell($fh);

	seek($fh, 0, 0) or confess("Error rewinding design element cache file: $!");

	$elements = fd_retrieve( $fh )
	    or confess("Error retrieving cached array design elements: $!");

	seek($fh, $pos, 0) or confess("Error resetting design element cache file: $!");
    }

    return $elements;
}

1;
