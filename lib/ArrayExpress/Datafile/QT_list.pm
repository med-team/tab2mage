#!/usr/bin/env perl
#
# QT_list.pm
#
# $Id: QT_list.pm 1852 2007-12-13 10:14:27Z tfrayner $
#
# This file contains QuantitationType information parsed out from our
# QuantitationType survey spreadsheet, and from the relevant database
# tables in MIAMExpress. This information has been primarily gathered
# and curated by Ele Holloway.
#

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: QT_list.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::Datafile::QT_list.pm - a module handling QuantitationType
information.

=head1 SYNOPSIS

 use ArrayExpress::Datafile::QT_list qw(get_QTs writeQTs)
 my $QT_hash = get_QTs();
 write_QTs($QT_hash);

=head1 DESCRIPTION

This is a module providing QuantitationType support information and
subroutines for the Tab2MAGE and experiment checker package.

=head1 QUANTITATION TYPES

Known QuantitationTypes are listed in the QT_list.pm module included
with this distribution. This file includes a simple tab-delimited text
section with subsections specific to a given software/manufacturer
type. Each subsection begins with the tag "C<E<gt>E<gt>E<gt>>". There are
several columns, but only the first is required:

=over 1

=begin html

<table class="example">
<tr><th>&gt;&gt;&gt;QT&nbsp;software&nbsp;1</th></tr>
<tr><td>QT1 name</td><td>QT1 datatype</td><td>QT1 scale</td><td>QT1 subclass</td><td>QT1 is_background</td><td>QT1 target (confidence indicators)</td><td>QT1 channel</td><td>QT1 description</td></tr>
<tr><td>QT2 name</td><td>QT2 datatype</td><td>QT2 scale</td><td>QT2 subclass</td><td>QT2 is_background</td><td>QT2 target (confidence indicators)</td><td>QT2 channel</td><td>QT2 description</td></tr>
<tr><td>QT3 name</td><td>QT3 datatype</td><td>QT3 scale</td><td>QT3 subclass</td><td>QT3 is_background</td><td>QT3 target (confidence indicators)</td><td>QT3 channel</td><td>QT3 description</td></tr>
<tr><td>QT4 name...</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
<tr><th>&gt;&gt;&gt;QT&nbsp;software&nbsp;2</th></tr>
<tr><td>QT5 name</td><td>QT5 datatype</td><td>QT5 scale</td><td>QT5 subclass</td><td>QT5 is_background</td><td>QT5 target (confidence indicators)</td><td>QT5 channel</td><td>QT5 description</td></tr>
<tr><td>QT6 name...</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td></tr>
</table>

=end html

=begin man

 >>>QT source 1 (software name)[manufacturer 1]
 QT1 name  QT1 datatype  QT1 scale  QT1 subclass  QT1 is_background  QT1 conf. indicator tgt.  QT1 channel  QT1 description
 QT2 name  QT2 datatype  QT2 scale  QT2 subclass  QT2 is_background  QT2 conf. indicator tgt.  QT2 channel  QT2 description
 QT3 name  QT3 datatype  QT3 scale  QT3 subclass  QT3 is_background  QT3 conf. indicator tgt.  QT3 channel  QT3 description
 QT4 name...
 >>>QT source 2 (software name)[manufacturer 2]
 QT5 name  QT5 datatype  QT5 scale  QT5 subclass  QT5 is_background  QT5 conf. indicator tgt.  QT5 channel  QT5 description
 QT6 name...

=end man

=back

where QT1, QT2 and so on are the quantitation types. Note that for the
resulting MAGE identifiers to have the correct namespace, the ">>>QT
software" strings should contain the namespace in square brackets, for
example:

 >>>Feature Extraction Software[Agilent Technologies]

Note also that to add to a pre-existing set of quantitation types from
a given software, this initial string B<must> match that included in the
Tab2MAGE package. Currently included software types are as follows:

 AIDA[Raytest]
 Affymetrix[Affymetrix]
 AppliedBiosystems
 ArrayGauge[FUJIFILM]
 ArrayVision[Imaging Research]
 BZScan[TAGC ERM206]
 BlueFuse[BlueGnome]
 ChipSkipper[EMBL]
 CodeLink[Motorola Life Sciences]
 CodeLink Expression Analysis[GE Healthcare]
 Feature Extraction Software[Agilent Technologies]
 GEMTools[Incyte Genomics]
 GLEAMS[NuTec Sciences]
 GenePix[Axon Instruments]
 GeneTAC
 ImaGene[BioDiscovery]
 NimbleScan[NimbleGen Systems]
 QuantArray[PerkinElmer]
 ScanAlyze[Stanford University]
 ScanArray Express[PerkinElmer]
 SpotFinder[TIGR]
 UCSF Spot
 Uni of Toronto in-house analysis software
 Tab2MAGE_FGEM[ebi.ac.uk]

Each file may contain QT information from any number of software
vendors. The contents of each column are described below:

=head2 QT file columns

=over 2

=item name

The name of the QuantitationType. This corresponds to the exact string found in the data files.

=item datatype

The DataType OntologyEntry to be used (MGED Ontology). Default: float

=item scale

The Scale OntologyEntry to be used (MGED Ontology). Default:
linear_scale

=item subclass

The QuantitationType subclass to use (e.g., MeasuredSignal, Error
etc.). Default: SpecializedQuantitationType

=item is_background

Boolean flag (0 or 1) indicating whether the QT is background. Default: 0

=item confidence indicator target

Only used for ConfidenceIndicator QTs (Error, PValue, and
ExpectedValue). Indicates the target QT name to which the
ConfidenceIndicator applies. Defaults to undefined.

=item channel

The channel to be associated with the QT. Note that QTs can only have
one channel; this is a current limitation of the MAGE object
model. QTs linked to more than one channel in the real world
(e.g. ratios) should omit the channel value here.

=item description

A plain-text description of the QT.

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2005.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. Particular credit
goes to Ele Holloway, who was responsible for curating the lists of
QuantitationTypes included with this script.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

package ArrayExpress::Datafile::QT_list;

use strict;
use warnings;

use Carp;
use IO::File;

use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Common qw($RE_LINE_BREAK);

use base 'Exporter';
our @EXPORT_OK = qw(
    get_QTs
    write_QTs
);

sub get_QTs {

    # Accepts single filenames or arrayref list; also an optional
    # "include_known" QTs argument.
    my ( $extra_file_or_files, $include_known ) = @_;

    # We now track whether a QT is user-defined or not.
    my %QT_file = ( old => [], new => [] );

    # Get a list of files containing QT info.  Start with the default
    # - the QTs listed in the QT_list.txt file in the same dir as this
    # module. The default QTs can then be overwritten as desired by
    # the user.
    my $module_path      = File::Spec->rel2abs(__FILE__);
    my @module_dir_array = File::Spec->splitpath($module_path);
    my $package_default  = $ENV{PAR_TEMP}   # Running under PAR
                         ? File::Spec->catfile( $ENV{PAR_TEMP},
						qw( inc lib ArrayExpress Datafile QT_list.txt ) )
                         : File::Spec->catpath(
			     @module_dir_array[ 0, 1 ], 'QT_list.txt' );
    my $default = $CONFIG->get_DEFAULT_QT_FILENAME() || $package_default;

    if ( $include_known || !$extra_file_or_files ) {
        push( @{ $QT_file{old} }, $default );
    }

    if ($extra_file_or_files) {
        if ( ref($extra_file_or_files) eq 'ARRAY' ) {
            foreach my $file (@$extra_file_or_files) {
                push( @{ $QT_file{new} }, $file );
            }
        }
        else {
            push( @{ $QT_file{new} }, $extra_file_or_files );
        }
    }

    # Add any files supplied to the routine.
    my %QT_filehandle = ( old => [], new => [] );
    foreach my $type ( keys %QT_file ) {
        foreach my $file ( @{ $QT_file{$type} } ) {
            my $filehandle = IO::File->new( $file, '<' )
                or
                croak("Error: QuantitationType file $file not found: $!\n");
            push( @{ $QT_filehandle{$type} }, $filehandle );
        }
    }

    # Loop through the filehandles.
    my %QT_hash;
    foreach my $type ( keys %QT_filehandle ) {
        foreach my $filehandle ( @{ $QT_filehandle{$type} } ) {
            my $software;

            QTFILE_LINE:
            while ( my $line = <$filehandle> ) {

                # skip comment or blank lines
                next QTFILE_LINE if ( $line =~ /^\#/ or $line =~ /^\s*$/ );

		# This should support both unix or DOS line breaks.
                $line =~ s/$RE_LINE_BREAK//g;

                # Header for a software/manufacturer name. Remove
                # leading and trailing whitespace
                if ( $line =~ m/^>>>\s*(.*?)\s*$/ ) {
                    $software = $1;
                    next QTFILE_LINE;

                }

                # Everything else; the meat of the QTs
                else {

                    # Strip whitespace in a moderately efficient manner
                    $line =~ s/^ *//;
                    $line =~ s/ *$//;
                    $line =~ s/ *\t */\t/g;

                    my @line_array = split /\t/, $line, -1;

                    $QT_hash{$software}{ $line_array[0] } = {
                        datatype      => $line_array[1] || q{},
                        scale         => $line_array[2] || q{},
                        subclass      => $line_array[3] || q{},
                        is_background => $line_array[4] || q{},
                        confmap       => $line_array[5] || q{},
                        channel       => $line_array[6] || q{},
                        description   => $line_array[7] || q{},
                        is_user_defined => ( $type eq 'new' ) || undef,
                    };
                }
            }
        }
    }
    return ( \%QT_hash );
}

sub write_QTs {

    my ( $new_QTs, $fh ) = @_;

    print $fh <<"HEADER";
# Name\tDataType\tScale\tMAGE Subclass\tisBackground\tConfidenceIndicator target QT\tChannel\tDescription
HEADER

    foreach my $software ( sort keys %$new_QTs ) {

        print $fh (">>>$software\n");

        my @lines;
        while ( my ( $name, $slots ) = each %{ $new_QTs->{$software} } ) {
            push(
                @lines,
                join( "\t",
                    $name,                   $slots->{datatype},
                    $slots->{scale},         $slots->{subclass},
                    $slots->{is_background}, $slots->{confmap},
                    $slots->{channel},       $slots->{description},
                )
            );
        }

        print $fh ( join( "\n", sort @lines ), "\n" );
    }

    return;
}

1;
