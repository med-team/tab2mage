#!/usr/bin/env perl
#
# Module used to construct checker and exporter daemon objects
#
# Tim Rayner 2006, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Daemon.pm 2017 2008-04-01 21:24:23Z tfrayner $
#

use strict;
use warnings;

package ArrayExpress::AutoSubmission::Daemon;

use Carp;
use Class::Std;
use MIME::Lite;
use Proc::Daemon;
use POSIX qw(:sys_wait_h);
use Socket;
use IO::Socket;
use IO::Select;
use IO::Handle;
use Scalar::Util qw(looks_like_number);
use English qw( -no_match_vars );
use Sys::Hostname;

use ArrayExpress::Curator::Common qw(date_now);
use ArrayExpress::Curator::Config qw($CONFIG);
require ArrayExpress::AutoSubmission::DB::Experiment;

# These are only set upon initialization
my %polling_interval  : ATTR( :get<polling_interval>,  :init_arg<polling_interval>  );
my %experiment_type   : ATTR( :get<experiment_type>,   :init_arg<experiment_type>   );
my %accession_prefix  : ATTR( :get<accession_prefix>,  :init_arg<accession_prefix>,  :default<undef> );
my %autosubs_admin    : ATTR( :get<autosubs_admin>,    :init_arg<autosubs_admin>    );

# Optional for exporter daemons; the checker daemons will check that
# this is defined.
my %pidfile           : ATTR( :get<pidfile>,           :init_arg<pidfile>,           :default<undef> );
my %checker_threshold : ATTR( :get<checker_threshold>, :init_arg<checker_threshold>, :default<undef> );

# The log file names are manipulated post-init.
my %logfile           : ATTR( :name<logfile>, :init_arg<logfile>, :default<q{}> );

# Optional means to control the set of QTs used by the daemons.
my %qt_filename       : ATTR( :get<qt_filename>, :init_arg<qt_filename>, :default<undef> );

# Attribute holding a flag notifying the process that it should quit.
my %quit_when_done    : ATTR( :name<quit_when_done>, :default<undef> );

# Initialization subroutine.
sub START {
    my ( $self, $id, $args ) = @_;

    croak("Error: no polling_interval set")
	unless ( $self->get_polling_interval() );

    croak("Error: no experiment_type set")
	unless ( $self->get_experiment_type() );

    croak("Error: no autosubs_admin email set.")
	unless ( $self->get_autosubs_admin() );

    return;
}

sub run {

    my ( $self ) = @_;

    if (my $prefix = $self->get_accession_prefix() ) {
	ArrayExpress::AutoSubmission::DB::Experiment->accession_prefix(
	    $prefix,
	);
    }

    # Check that there are no other processes running, if a pidfile
    # has been provided. Typically the checker daemons have a pidfile,
    # but the exporter daemons do not.
    if ( $self->get_pidfile() && -f $self->get_pidfile() ) {
        die(
            sprintf(
"Error: Old pidfile %s exists; another checker daemon may be running.\n",
                $self->get_pidfile() )
        );
    }

    # Check that the right user has launched the process; otherwise we
    # end up with multiple copies running under multiple accounts and
    # it's a nightmare to get control again.
    if ( my $admin_user = $CONFIG->get_AUTOSUBS_ADMIN_USERNAME() ) {
	unless ( getlogin() eq $admin_user ) {
	    die(  "Error: Non-admin user attempting to launch daemon processes"
		. " (you should be $admin_user). Please ensure AUTOSUBS_ADMIN_USERNAME"
	        . " is set to the correct login in the site configuration file.\n");
	}
    }
    else {
	die(  "Error: AUTOSUBS_ADMIN_USERNAME must be set to an admin"
	    . " username in the site configuration file.\n");
    }

    # Run as a daemon
    printf STDOUT ( "Starting %s %s daemon...\n",
        $self->get_experiment_type(), ref($self) );
    Proc::Daemon::Init;

    ##########################
    ### Exception handlers ###
    ##########################
    $SIG{TERM} = sub { $self->alert_on_signal(@_) };    # 15
    $SIG{QUIT} = sub { $self->alert_on_signal(@_) };    # 3
    $SIG{SEGV} = sub { $self->alert_on_signal(@_) };    # 11
    $SIG{INT}  = sub { $self->alert_on_signal(@_) };    # 2
    $SIG{USR1} = sub { $self->set_quit_when_done(1) };  # 16

    # From here, all die()s should mail us a notification.
    $SIG{__DIE__} = sub { $self->alert_admin(@_) };

    if ( $self->get_pidfile() ) {
	
	# Create a file containing the process id ($$). This has to
	# happen after daemon forking, above.
	open( my $pid, ">", $self->get_pidfile() )
	    or die(sprintf("Unable to open pidfile %s: %s\n",
			   $self->get_pidfile(), $!));
	print $pid "$PROCESS_ID\n";
	close $pid or die("$!\n");
    }

    # The daemon starts via this entry point method.
    $self->monitor_submissions();

    return;
}

{
    my ( $oldout, $olderr );

    sub redirect_stdout_to_file : RESTRICTED {

	my ( $self, $logfile ) = @_;

	unless ( defined($logfile) ) {
	    die("Error: Undefined log file name.");
	}

	open( $oldout, '>&', \*STDOUT ) and seek( $oldout, 0, 0 );
	open( $olderr, '>&', \*STDERR ) and seek( $olderr, 0, 0 );

	open( STDOUT, '>', $logfile )
	    or die("Can't redirect STDOUT to logfile $logfile\n");
	open( STDERR, '>&', \*STDOUT )
	    or die("Can't redirect STDERR to STDOUT");
	*STDERR->autoflush();    # make unbuffered
	*STDOUT->autoflush();    # make unbuffered

	return;
    }

    sub restore_stdout_from_file : RESTRICTED {

	my ( $self ) = @_;

	unless ( $oldout && $olderr ) {
	    die("Error: STDOUT and STDERR not cached;"
	      . " have you called redirect_stdout_to_file() yet?");
	}

	( close(STDOUT) and open( STDOUT, '>&', $oldout ) )
	    or die("Can't restore STDOUT: $!\n");
	( close(STDERR) and open( STDERR, '>&', $olderr ) )
	    or die("Can't restore STDERR: $!\n");

	return;
    }
}

sub expt_check_submission : RESTRICTED {

    my ( $self, $submission, $checker ) = @_;

    # Set up our sockets for parent-child communication
    socketpair( CHILD, PARENT, AF_UNIX, SOCK_STREAM, PF_UNSPEC )
      or die "Cannot create socketpair: $!";

    CHILD->autoflush(1);
    PARENT->autoflush(1);

    my $pid;
    my $selector = IO::Select->new();
    $selector->add( \*CHILD );

    # Launch a subprocess to check the submission and return a score.
    my ( $checker_score,
	 $consensus_software,
	 $dw_score,
	 $miame_score,
	 $comment );

    my $start_time = date_now();

    if ( $pid = fork ) {

        # Parent process code

        close PARENT;

        my $checker_signal;
        while ( my @handles = $selector->can_read ) {
            foreach my $handle (@handles) {
                recv( $handle, $checker_signal, 1024, 0 );
            }
            last;
        }

        # Post-process the child signal.
        chomp $checker_signal;
        ( $checker_score,
	  $consensus_software,
	  $dw_score,
	  $miame_score,
	  $comment ) =
          split /\t/, $checker_signal;

        close CHILD;
        waitpid( $pid, 0 );

    }

    else {

        # Child process code; must end with an exit

        die "Error: Cannot fork: $!" unless defined $pid;
        close CHILD;
        $PROGRAM_NAME .= '_child';

	$checker->set_clobber(1);

        # Redirect STDOUT and STDERR to our log file.
        my $logfile = $self->get_logfile()
          or croak("Error: log file name not set.");

	$self->redirect_stdout_to_file( $logfile );

        # Run all the checks
        eval { $checker->check(); };

        # Communicate results with parent process
        if ($EVAL_ERROR) {

            # Checker score of 512 is reserved for checker crashes.
            print PARENT (
		join( "\t",
		      $CONFIG->get_ERROR_CHECKERCRASH(),
		      q{Unknown},
		      q{},
		      q{},
		      $EVAL_ERROR,
		  ),
                "\n" );
        }
        else {

            # Print out to the log.
            print STDOUT ( "\n\nExperiment checker exit code: "
                  . $checker->get_error()
                  . "\n\n" );

            # Pass the results to parent.
            print PARENT (
                join( "\t",
		      $checker->get_error(),
		      $checker->get_miamexpress_software_type() || q{},
		      $checker->get_aedw_score(),
		      $checker->get_miame(),
		      q{}
		  ),
                "\n"
            );
        }
        close PARENT;

        # Restore STDOUT and STDERR.
        $self->restore_stdout_from_file();

        exit;    # quit the child

    }

    my $end_time = date_now();

    #################################################################
    # Post-checking update of the database record for a submission. #
    #################################################################
	    
    # Checker score 512 is reserved for checker crashes
    if ( $checker_score & $CONFIG->get_ERROR_CHECKERCRASH() ) {
	$submission->set(
	    status              => $CONFIG->get_STATUS_CRASHED(),
	    comment             => $submission->comment()
		                   . ( $comment ? "\n\n$comment\n" : q{} ),
	    date_last_processed => $end_time,
	);
	$submission->update();

	# Give up at this point.
	return;
    }

    # Update the cache with the checker score etc.
    my $accession = $submission->get_accession({
	checker_score        => $checker_score,
	software             => ( $consensus_software || 'Unknown' ),
	data_warehouse_ready => $dw_score,
	miame_score          => $miame_score,
	comment              => $submission->comment()
		                   . ( $comment ? "\n\n$comment\n" : q{} ),
	date_last_processed  => $end_time,
    });
    $submission->update();

    # Add an event to record this run.
    $submission->add_to_events({
	event_type       => 'Experiment Checker',
	was_successful   => ( $checker_score & $CONFIG->get_ERROR_CHECKERCRASH() ? 0 : 1 ),
	source_db        => $submission->experiment_type(),
	start_time       => $start_time,
	end_time         => $end_time,
	machine          => hostname(),
	operator         => $submission->curator(),
	log_file         => $self->get_logfile(),
	is_deleted       => 0,
    });

    # Check the returned info, act as necessary.
    if (   looks_like_number($checker_score)
	   && ( $checker_score <= $self->get_checker_threshold() ) ) {

	# Submission passes checks - mark for export.
	$submission->set(
	    status              => $CONFIG->get_STATUS_PASSED(),
	    date_last_processed => $end_time,
	);
	$submission->update();
    }
    else {

	# Submission failed checks.
	$submission->set(
	    status              => $CONFIG->get_STATUS_FAILED(),
	    date_last_processed => $end_time,
	);
	$submission->update();
    }

    return;
}

sub is_magetab_doc : RESTRICTED {

    # Tests for combined MAGETAB IDF+SDRF document versus IDF only,
    # returns true in the former case, false for the latter. Used in
    # both Daemon::Checker and Daemon::Exporter subclasses.

    my ( $self, $doc ) = @_;

    open( my $fh, '<', $doc )
	or croak(qq{Error: Unable to open document "$doc": $!\n});

    my ( $idf_found, $sdrf_found );
    while ( my $line = <$fh> ) {
	$idf_found++        if ( $line =~ m/\A \s* \"? \s* \[IDF\]/ixms );
	$sdrf_found++       if ( $line =~ m/\A \s* \"? \s* \[SDRF\]/ixms );
    }

    close( $fh ) or croak(qq{Error: Unable to close document "$doc": $!\n});

    if ( $idf_found && $sdrf_found ) {
	return 1;
    }

    return;
}

sub get_curation_experiments : RESTRICTED {
    confess("Error: stub method called in abstract parent class.");
}

sub monitor_submissions : RESTRICTED {
    confess("Error: stub method called in abstract parent class.");
}

# Signal handling subroutines (not OO as such)
sub alert_admin : PRIVATE {

    my ($self, $error) = @_;

    my $mailbody =
        "The automatic experiment checking system ($PROCESS_ID) "
      . "has crashed with the following error message: \n\n  $error\n\n";

    my $mail = MIME::Lite->new(
        From     => $self->get_autosubs_admin(),
        To       => $self->get_autosubs_admin(),
        Subject  => 'Crash notification',
        Type     => 'text/plain',
        Encoding => 'quoted-printable',
        Data     => $mailbody,
    );
    $mail->send();

    return;
}

sub alert_on_signal : PRIVATE {

    my ($self, $signal) = @_;

    # Delete our PID file, if we're using one.
    my $pidfile = $self->get_pidfile();
    unlink $pidfile if (defined $pidfile);

    # Call for help. NB if we die here we get two emails instead of
    # one. I assume that when this signal handler returns a die() is
    # invoked, not sure about that though.
    $self->alert_admin("Error: Signal SIG$signal received.\n");

    # FIXME this is currently undetected by any parent processes.
    exit(255);
}

1;
