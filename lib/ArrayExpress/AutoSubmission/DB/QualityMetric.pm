#!/usr/bin/env perl
#
# $Id: QualityMetric.pm 1960 2008-02-21 12:03:19Z tfrayner $

use strict;
use warnings;

package ArrayExpress::AutoSubmission::DB::QualityMetric;
use base 'ArrayExpress::AutoSubmission::DB';

__PACKAGE__->table('quality_metrics');
__PACKAGE__->columns(
    All => qw(
        id
	type
        description
        )
);

__PACKAGE__->has_many(
    loaded_data => [
        'ArrayExpress::AutoSubmission::DB::QualityMetricInstance' => 'loaded_data_id'
    ]
);
__PACKAGE__->has_many(
    quality_metric_instances => 'ArrayExpress::AutoSubmission::DB::QualityMetricInstance'
);

1;
