#!/usr/bin/env perl
#
# $Id: MAGETABExporter.pm 1868 2008-01-03 14:26:51Z tfrayner $

use strict;
use warnings;

package ArrayExpress::AutoSubmission::Daemon::MAGETABExporter;
use base 'ArrayExpress::AutoSubmission::Daemon::Exporter';

use Class::Std;
use File::Spec;
require ArrayExpress::MAGETAB;
use ArrayExpress::Curator::Config qw($CONFIG);

sub create_exporter : RESTRICTED {

    my ( $self, $submission, $spreadsheet ) = @_;

    my %common_opts = (
	authority                 => $CONFIG->get_AUTOSUBS_DOMAIN(),
	namespace                 => 'MAGETabulator',
	expt_accession            => $submission->get_accession(),
	output_file               => $submission->get_accession() . '.xml',
	target_directory          => File::Spec->catdir(
	    $self->get_targetdir(),
	    $submission->accession(),
	),
	source_directory          => $submission->unpack_directory(),
	use_plain_text            => 0,
	protocol_accession_prefix => 'P-MTAB-',
	keep_protocol_accns       => $self->get_keep_protocol_accns(),
	clobber                   => 1,
	qt_filename               => $self->get_qt_filename(),

	# Note that if we change this we must also revert the
	# ExperimentChecker code to flag unknown QTs as
	# PARSEBAD.
	keep_all_qts              => 1,   # review this FIXME
    );

    # Decide whether this is a combined MAGETAB (IDF+SDRF) or IDF-only
    # document.
    my $magetab;
    my $file = $spreadsheet->filesystem_path();
    if ( $self->is_magetab_doc( $file ) ) {
	$magetab = ArrayExpress::MAGETAB->new({
	    magetab_doc => $file,
	    %common_opts,
	});
    }
    else {
	$magetab = ArrayExpress::MAGETAB->new({
	    idf => $file,
	    %common_opts,
	});
    }

    $self->set_logfile( $magetab->logfiles('magetab') );

    return $magetab;
}

1;
