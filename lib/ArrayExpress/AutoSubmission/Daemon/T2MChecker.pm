#!/usr/bin/env perl
#
# $Id: T2MChecker.pm 2020 2008-04-08 13:15:33Z tfrayner $

use strict;
use warnings;

package ArrayExpress::AutoSubmission::Daemon::T2MChecker;
use base 'ArrayExpress::AutoSubmission::Daemon::Checker';

require ArrayExpress::Curator::Validate;

use Class::Std;

sub check : RESTRICTED {

    my ( $self, $submission, $spreadsheet ) = @_;

    $self->expt_check_submission(
	$submission,
	ArrayExpress::Curator::Validate->new({
	    spreadsheet_filename => $spreadsheet->filesystem_path(),
	    source_directory     => $submission->unpack_directory(),
	    qt_filename          => $self->get_qt_filename(),
            safe_filechecks      => 1,
	}),
    );

    return;
}

1;
