#!/usr/bin/env perl
#
# Module used to construct MIAMExpress checker daemon objects
#
# Tim Rayner 2007, ArrayExpress team, European Bioinformatics Institute
#
# $Id: MXChecker.pm 2013 2008-03-31 17:23:14Z tfrayner $
#

use strict;
use warnings;

package ArrayExpress::AutoSubmission::Daemon::MXChecker;

use base 'ArrayExpress::AutoSubmission::Daemon';

use Class::Std;
use DBI;
use Carp;
use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Common qw(date_now mx2tab);

require ArrayExpress::AutoSubmission::DB::Experiment;
require ArrayExpress::Curator::MIAMExpress;

my %mx_dsn       : ATTR( :get<mx_dsn>,       :init_arg<mx_dsn>       );
my %mx_username  : ATTR( :get<mx_username>,  :init_arg<mx_username>  );
my %mx_password  : ATTR( :get<mx_password>,  :init_arg<mx_password>  );
my %mx_dbparams  : ATTR( :get<mx_dbparams>,  :init_arg<mx_dbparams>  );

sub START {
    my ( $self, $id, $args ) = @_;

    croak("Error: no mx_dsn set.")
	unless ( $self->get_mx_dsn() );

    croak("Error: no mx_username set.")
	unless ( $self->get_mx_username() );

    croak("Error: no mx_password set.")
	unless ( $self->get_mx_password() );

    croak("Error: no mx_dbparams set.")
	unless ( $self->get_mx_dbparams() );

    croak("Error: no checker_threshold set.")
	unless ( defined( $self->get_checker_threshold() ) );

    return;
}

sub get_curation_expts : PRIVATE {
    my $self = shift;

    my $dbh = DBI->connect(
        $self->get_mx_dsn(),      $self->get_mx_username(),
        $self->get_mx_password(), $self->get_mx_dbparams(),
    ) or die("Database connection error: $DBI::errstr\n");

    my $sth = $dbh->prepare(<<'QUERY');
select distinct TSUBMIS_SYSUID from TSUBMIS, TEXPRMNT
where TSUBMIS_PROC_STATUS in ('C','S')
and TSUBMIS_COMP_STATUS='U'
and TSUBMIS_SYSUID=TEXPRMNT_SUBID
and TSUBMIS_DEL_STATUS='U'
and TEXPRMNT_DEL_STATUS='U'
and TSUBMIS_LOGIN!='test'
QUERY

    $sth->execute() or die($sth->errstr);

    my $results = $sth->fetchall_hashref('TSUBMIS_SYSUID');

    $dbh->disconnect()
      or die( "Database disconnection error: " . $dbh->errstr() . "\n" );

    return [ sort keys %{$results} ];

}

sub info_from_subid : PRIVATE {

    my ( $self, $subid ) = @_;

    my $dbh = DBI->connect(
        $self->get_mx_dsn(),      $self->get_mx_username(),
        $self->get_mx_password(), $self->get_mx_dbparams(),
    ) or die("Database connection error: $DBI::errstr\n");

    my $sth = $dbh->prepare(<<'QUERY');
select distinct TSUBMIS_LOGIN, TSUBMIS_SUB_DESCR, TSUBMIS_LAST_CHANGE, TEXPRMNT_VARS
from TSUBMIS, TEXPRMNT
where TSUBMIS_SYSUID=?
and TSUBMIS_SYSUID=TEXPRMNT_SUBID
and TEXPRMNT_DEL_STATUS='U'
and TSUBMIS_DEL_STATUS='U'
QUERY

    $sth->execute($subid) or die($sth->errstr);

    my $results = $sth->fetchall_arrayref;

    $sth->finish();

    if ( scalar(@$results) != 1 ) {
        die(
	    "Error: submission id $subid maps to "
	    . scalar(@$results)
	    . " submissions in the database!\n"
        );
    }

    my ( $login, $title, $date, $vars ) = @{ $results->[0] };

    my $is_mxbl = 0;
    if ( $vars && $vars =~ m/BatchUploadTool/ixms ) {
	$is_mxbl = 1;
    }

    # Get the experiment filebase, while we have a connection.
    $sth = $dbh->prepare(<<'QUERY');
select distinct TSYSOPT_VALUE from TSYSOPT
where TSYSOPT_CODE='experiment_datafiles_path'
QUERY

    $sth->execute() or die($sth->errstr);

    # FIXME we're assuming the query worked here.
    my $subsdir = File::Spec->catdir(
	$sth->fetchrow_arrayref()->[0], $login, "submission$subid",
    );

    $dbh->disconnect()
      or die( "Database disconnection error: " . $dbh->errstr() . "\n" );

    return ( $login, $title, $subsdir, $date, $is_mxbl );

}

sub monitor_submissions : RESTRICTED {
    my $self = shift;

    # Loop forever
    EVENT_LOOP:
    while (1) {

        # Get the list of all experiments in curation.
        my $new_results = $self->get_curation_expts();

        # Process the new submissions in order of id.
	SUBMISSION:
        foreach my $subid ( @{$new_results} ) {

            # Retrieve each experiment record in turn, creating
            # records where none exist.
            my $submission =
		ArrayExpress::AutoSubmission::DB::Experiment->retrieve(
		    miamexpress_subid => $subid,
		    is_deleted        => 0,
		)
	         ||
	        ArrayExpress::AutoSubmission::DB::Experiment->insert({
		    miamexpress_subid => $subid,
		    status            => $CONFIG->get_STATUS_PENDING(),
		    experiment_type   => $self->get_experiment_type(),
		    is_deleted        => 0,
		});

            # We avoid checking non-MX submissions with MX submission
            # IDs here (these will typically have been switched to
            # e.g. MAGE-TAB).
            next SUBMISSION
              if ( $submission->experiment_type() ne $self->get_experiment_type() );

	    # Start a transaction.
	    my $dbh = ArrayExpress::AutoSubmission::DB::Experiment->db_Main();
	    $dbh->begin_work();

	    # Get current status and a row-level read lock.
	    my $current_status =
		ArrayExpress::AutoSubmission::DB::Experiment->sql_status_for_update()
		    ->select_val( $submission->id() ) || q{};

            # Skip the submission if it has a non-pending status
            # already in the database. Occasionally checking crashes
            # due to e.g. AE being down, so all submissions pending
            # checking (or with NULL status) are fair game. Crashed
            # subs are no longer checked.
	    # Double-check, in case something changes in the meantime.
	    unless ( $current_status eq $CONFIG->get_STATUS_PENDING() ) {
		$dbh->commit();
		next SUBMISSION;
	    }

            $submission->set(
                status              => $CONFIG->get_STATUS_DB_RETRIEVAL(),
                date_last_processed => date_now(),
            );
            $submission->update();

	    # End the transaction.
	    $dbh->commit();

            my ( $login, $title, $subsdir, $subsdate, $is_mxbl )
		= $self->info_from_subid($subid);

            # This is specific to the current MIAMExpress version.
            $self->set_logfile(
                File::Spec->catfile(
		    $subsdir,
                    "expt_${login}_sub${subid}_stdout.log"
                )
            );

            # Enter the login and expt name information.
            $submission->set(
                miamexpress_login   => $login,
                name                => $title,
                status              => $CONFIG->get_STATUS_CHECKING(),
		date_submitted      => $subsdate,
                date_last_processed => date_now(),
                is_mx_batchloader   => $is_mxbl,
            );
            $submission->update();

            # Actually check the submission for errors. This also
            # updates the autosubs db.
	    $self->expt_check_submission(
		$submission,
		ArrayExpress::Curator::MIAMExpress->new({
		    mx_login => $login,
		    mx_title => $title,
		}),
	    );

	    # FIXME maybe bring the status changes from mx2tab into
	    # this module so that they can be more easily managed
	    # within a transaction. NOTE however that when I tried
	    # wrapping this call in a transaction quite odd things
	    # happened; possibly a DBD::mysql bug? The script failed
	    # while complaining that the DBD driver hadn't implemented
	    # the AutoCommit attribute (which, of course, it
	    # has). Note also that the transaction earlier works just
	    # fine, maybe there's a cryptic interaction between the
	    # two.
	    my $new_status =
		ArrayExpress::AutoSubmission::DB::Experiment->sql_status_for_update()
		    ->select_val( $submission->id() ) || q{};

	    # If checks passed, we now switch the submission to
	    # MAGE-TAB.
	    if ( $new_status eq $CONFIG->get_STATUS_PASSED() ) {
		eval {
		    mx2tab($submission);
		};
	    }

	    last EVENT_LOOP if $self->get_quit_when_done();

	}
	
        sleep( $self->get_polling_interval() * 60 );

	last EVENT_LOOP if $self->get_quit_when_done();

    }

    return;
}

1;
