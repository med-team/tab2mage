#!/usr/bin/env perl
#
# $Id: MAGETABChecker.pm 2020 2008-04-08 13:15:33Z tfrayner $

use strict;
use warnings;

package ArrayExpress::AutoSubmission::Daemon::MAGETABChecker;
use base 'ArrayExpress::AutoSubmission::Daemon::Checker';

require ArrayExpress::MAGETAB::Checker;

use Class::Std;

sub check : RESTRICTED {

    my ( $self, $submission, $spreadsheet ) = @_;

    my $file = $spreadsheet->filesystem_path();

    # For MIAMExpress submissions converted to MAGE-TAB, we don't want
    # to repeat the data file checks. Here we assume that the
    # miamexpress_subid column is a good indicator (which, for the
    # most part, it should be).
    my $skip_data = $submission->miamexpress_subid() ? 1 : 0;

    # Parameters passed to ExperimentChecker object.
    my %checker_params;
    if ( $self->is_magetab_doc( $file ) ) {
	%checker_params = (
	    magetab_doc        => $file,
	    source_directory   => $submission->unpack_directory(),
	    log_to_current_dir => $submission->filesystem_directory(),
	    qt_filename        => $self->get_qt_filename(),
            skip_data_checks   => $skip_data,
            safe_filechecks    => 1,
	);
    }
    else {
	%checker_params = (
	    idf                => $file,
	    source_directory   => $submission->unpack_directory(),
	    log_to_current_dir => $submission->filesystem_directory(),
	    qt_filename        => $self->get_qt_filename(),
            skip_data_checks   => $skip_data,
            safe_filechecks    => 1,
	);
    }
    $self->expt_check_submission(
	$submission,
	ArrayExpress::MAGETAB::Checker->new(\%checker_params),
    );

    return;
}

1;
