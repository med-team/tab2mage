#!/usr/bin/env perl
#
# Module used to construct spreadsheet checker daemon objects
#
# Tim Rayner 2007, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Checker.pm 2012 2008-03-30 17:27:46Z tfrayner $
#

use strict;
use warnings;

package ArrayExpress::AutoSubmission::Daemon::Checker;
use base 'ArrayExpress::AutoSubmission::Daemon';

use Class::Std;
use Archive::Tar;
use Archive::Zip;
use IO::Zlib;
use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Common qw(date_now);
require ArrayExpress::AutoSubmission::DB::Experiment;
use File::Spec;
use File::Find;
use File::Copy;
use Carp;
use Cwd;
use English qw( -no_match_vars );

sub START {
    my ( $self, $id, $args ) = @_;

    croak("Error: no checker_threshold set.")
	unless ( defined( $self->get_checker_threshold() ) );

    return;
}

sub unpack_archive : PRIVATE {

    my ($self, $file, $submission) = @_;

    my $filepath   = $file->filesystem_path();
    my $unpack_dir = $submission->unpack_directory();

    # Unpack the files into the appropriate directory. This should not
    # create a significant memory problem - files are written directly
    # to disk.
    my $starting_dir = getcwd();
    chdir $unpack_dir
	or croak ("Error changing to unpack directory $unpack_dir: $!");

    # Previously we used Archive::Any; however, memory requirements
    # necessitated that we make the selection ourselves. Maybe add
    # File::MMagic processing here as well as file extensions FIXME?
    if ( $filepath =~ m/\. (?:tar.gz|tgz|tar) \z/ixms) {

	# Tar archive

	# In general the tar command suffers from fewer memory issues
	# than Archive::Tar, which loads each archived data file
	# completely into memory (not good for large files). We only
	# fall back to Archive::Tar when tar isn't available.
	my $rc = system("tar xvzf $filepath");

	if ( $rc ) {

	    # Try again, using Archive::Tar. Trap any errors;
	    # otherwise this daemon will crash silently.
	    local $EVAL_ERROR;

	    # Apparently this call dies quite a lot; presumably
	    # trapped with an eval. However, to cut out the spurious
	    # alert emails we temporarily deactivate the die signal
	    # handler.
	    my $sighandler = $SIG{__DIE__};
	    delete $SIG{__DIE__};

	    eval {
		Archive::Tar->extract_archive($filepath);
	    };
	    $SIG{__DIE__} = $sighandler if $sighandler;
	    if ($EVAL_ERROR) {
		croak ("Error extracting archive $filepath: $EVAL_ERROR");
	    }
	}
    }
    elsif ( $filepath =~ m/\. (?:zip) \z/ixms) {

	# Zip archive
	my $zip = Archive::Zip->new();
	my $rc  = $zip->read($filepath);
	if ($rc) {
	    croak("Error reading zip file $filepath: $rc");
	}
	$rc = $zip->extractTree();
	if ($rc) {
	    croak("Error extracting zip file $filepath: $rc");
	}
    }
    else {
	croak("Error: unrecognized file type for $filepath");
    }

    # We now want to flatten the directory structure.
    find(
	sub {
	    if ( -f $_ && -o $_ ) {
		chmod(oct($CONFIG->get_FILE_PERMISSIONS()), $_)
		    or croak("Error changing permissions on $_: $!");
		move($_, $unpack_dir)
		    or croak("Error moving file $_: $!");
	    }
	    elsif ( -d $_ && -o $_ ) {
		chmod(oct($CONFIG->get_DIR_PERMISSIONS()), $_)
			or croak("Error changing permissions on $_: $!");
	    }
	},
	$unpack_dir,
    );

    # Back out of the unpacking directory.
    chdir $starting_dir
	or croak ("Error changing back to original directory: $!");

    return;
}

sub monitor_submissions : RESTRICTED {
    my $self = shift;

    # Loop forever
    EVENT_LOOP:
    while (1) {

        # Get the list of all experiments in curation.
        my $new_results = ArrayExpress::AutoSubmission::DB::Experiment->search(
	    in_curation     => 1,
	    status          => $CONFIG->get_STATUS_PENDING(),
	    experiment_type => $self->get_experiment_type(),
	    is_deleted      => 0,
	);

        # Process the new submissions in order of id.
	SUBMISSION:
        while ( my $submission = $new_results->next() ) {

	    # Skip submissions by test user, or submissions without any user.
	    next SUBMISSION if ( ! $submission->user_id()
				|| $submission->user_id()->login() eq 'test' );

	    # Start a transaction.
	    my $dbh = ArrayExpress::AutoSubmission::DB::Experiment->db_Main();
	    $dbh->begin_work();

	    # Get current status and a row-level read lock.
	    my $current_status =
		ArrayExpress::AutoSubmission::DB::Experiment->sql_status_for_update()
		    ->select_val( $submission->id() ) || q{};

	    # Double-check, in case something changes in the meantime.
	    unless ( $current_status eq $CONFIG->get_STATUS_PENDING() ) {
		$dbh->commit();
		next SUBMISSION;
	    }

	    # Mark the submission as in checking.
	    $submission->set(
		status              => $CONFIG->get_STATUS_CHECKING(),
		date_last_processed => date_now(),
	    );
	    $submission->update();

	    # End the transaction.
	    $dbh->commit();

	    # Iterate over all the data file archives, unpacking them.
	    my $datafiles = $submission->data_files(is_deleted => 0);

	    # Note that data files are returned in order of their database id.
	    FILE:
	    while ( my $file = $datafiles->next() ) {

		# Don't unpack a file more than once.
		next FILE if $file->is_unpacked();

		my $filepath = $file->filesystem_path();

		# If the file exists, do something with it.
		if ( -f $filepath ) {

		    # This area is prone to crashes, we want to trap them locally.
		    local $EVAL_ERROR;

		    # Process those files that Archive::Any can handle.
		    if ( $filepath =~ m/\. (?:tar.gz|tgz|tar|zip) \z/ixms ) {
			eval {
			    $self->unpack_archive( $file, $submission )
			};
		    }

		    # Gzipped files can be mistaken for tarred
		    # files. We handle them separately here.
		    elsif (my ($outfile) = ($filepath =~ m/(.*)\. (?:gz) \z/ixms) ) {
			eval {
			    my $zlib_fh = new IO::Zlib;
			    if ( $zlib_fh->open($filepath, "rb") ) {
				open (my $output_fh, ">", $outfile)
				    or croak("Error opening gzip output file $outfile: $!");
				while ( my $line = <$zlib_fh> ) {
				    print $output_fh $line;
				}

				$zlib_fh->close();
				close( $output_fh )
				    or croak("Error closing output file: $!");
			    }
			    move($outfile, $submission->unpack_directory())
				or croak("Error moving file $outfile: $!");
			};
		    }

		    # All uncompressed or unrecognized files are just
		    # copied to the unpack directory.
		    else {
			eval{copy($filepath, $submission->unpack_directory())
			    or croak("Error copying file $filepath: $!")};
		    }

		    # Check that there were no problems.
		    if ( $EVAL_ERROR ) {
			$submission->set(
			    status              => $CONFIG->get_STATUS_CRASHED(),
			    comment             => $submission->comment()
				                   . "\n\nError: Unable to unpack file or archive: $filepath\n",
			    date_last_processed => date_now(),
			);
			$submission->update();
			next SUBMISSION;
		    }

		    # Mark the file as unpacked in the database.
		    $file->set(is_unpacked => 1);
		    $file->update();
                }
		else {

		    # File not found. Mark the submission as bad, skip to next one.
		    $submission->set(
			status              => $CONFIG->get_STATUS_CRASHED(),
			comment             => $submission->comment()
			                       . "\n\nError: File in database not found on filesystem: $filepath\n",
			date_last_processed => date_now(),
		    );
		    $submission->update();
		    next SUBMISSION;
		}
	    }

	    # Only one spreadsheet per submission at this point.
	    my $spreadsheet
		= $submission->spreadsheets(is_deleted => 0)->next();

	    # Sort out the STDOUT logfile.
            my $logfile_string = $spreadsheet->name();
            $logfile_string =~ s/\.\w{3,4}$//;     # strip off the extension
	    $self->set_logfile(
		File::Spec->catfile(
		    $submission->filesystem_directory(),
		    "expt_${logfile_string}_stdout.log"
		)
	    );

            # Actually check the submission for errors. This also
            # updates the autosubs db.
	    $self->check( $submission, $spreadsheet );

	    last EVENT_LOOP if $self->get_quit_when_done();

	}

        sleep( $self->get_polling_interval() * 60 );

	last EVENT_LOOP if $self->get_quit_when_done();

    }

    return;
}

sub check : RESTRICTED {
    confess("Error: stub method called in abstract superclass.");
}

1;
