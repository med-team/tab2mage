#!/usr/bin/env perl
#
# $Id: T2MExporter.pm 1868 2008-01-03 14:26:51Z tfrayner $

use strict;
use warnings;

package ArrayExpress::AutoSubmission::Daemon::T2MExporter;
use base 'ArrayExpress::AutoSubmission::Daemon::Exporter';

use Class::Std;
use File::Spec;
require ArrayExpress::Curator::Tab2MAGE;
use ArrayExpress::Curator::Config qw($CONFIG);

sub create_exporter : RESTRICTED {

    my ( $self, $submission, $spreadsheet ) = @_;

    my $tab2mage = ArrayExpress::Curator::Tab2MAGE->new({

	# Only one spreadsheet per submission at the moment.
	spreadsheet_filename => $spreadsheet->filesystem_path(),
	target_directory     => File::Spec->catdir(
	    $self->get_targetdir(),
	    $submission->accession(),
	),
	source_directory     => $submission->unpack_directory(),
	external_accession   => $submission->get_accession(),
	external_domain      => $CONFIG->get_AUTOSUBS_DOMAIN,
	clobber              => 1,
	binary_datafiles     => 1,
	keep_protocol_accns  => $self->get_keep_protocol_accns(),
	qt_filename          => $self->get_qt_filename(),

	# Note that if we change this we must also revert the
	# ExperimentChecker code to flag unknown QTs as
	# PARSEBAD.
	keep_all_qts         => 1,   # review this FIXME
    });

    $self->set_logfile( $tab2mage->logfiles('tab2mage') );

    return $tab2mage;
}

1;
