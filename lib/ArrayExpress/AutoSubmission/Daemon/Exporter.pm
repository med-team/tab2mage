#!/usr/bin/env perl
#
# Module used to construct spreadsheet checker exporter objects
#
# Tim Rayner 2007, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Exporter.pm 2012 2008-03-30 17:27:46Z tfrayner $
#

use strict;
use warnings;

package ArrayExpress::AutoSubmission::Daemon::Exporter;
use base 'ArrayExpress::AutoSubmission::Daemon';

use Class::Std;
use English qw( -no_match_vars );
use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Common qw(date_now);
require ArrayExpress::AutoSubmission::DB::Experiment;
use Carp;
use POSIX qw(:sys_wait_h);
use Socket;
use IO::Socket;
use IO::Select;
use Sys::Hostname;

# This is simply passed through to the Tab2MAGE object.
my %keep_protocol_accns : ATTR( :get<keep_protocol_accns>, :init_arg<keep_protocol_accns>, :default<undef> );

# Track which subdirectory to write the output to.
my %pipeline_subdir : ATTR( :get<pipeline_subdir>, :init_arg<pipeline_subdir>, :default<undef> );

sub export_tab2mage : PRIVATE {

    my ( $self, $submission ) = @_;

    # Set up our sockets for parent-child communication
    socketpair( CHILD, PARENT, AF_UNIX, SOCK_STREAM, PF_UNSPEC )
      or die "Cannot create socketpair: $!";

    CHILD->autoflush(1);
    PARENT->autoflush(1);

    my $pid;
    my $selector = IO::Select->new();
    $selector->add( \*CHILD );

    my $exporter_signal;

    if ( $pid = fork ) {

        # Parent process code

        close PARENT;

        while ( my @handles = $selector->can_read ) {
            foreach my $handle (@handles) {
                recv( $handle, $exporter_signal, 1024, 0 );
            }
            last;
        }
	chomp $exporter_signal;

        close CHILD;
        waitpid( $pid, 0 );
    }

    else {

        # Child process code; must end with an exit

        die "Error: Cannot fork: $!" unless defined $pid;
        close CHILD;
        $PROGRAM_NAME .= '_child';

	my $spreadsheet
	    = $submission->spreadsheets(is_deleted => 0)->next();

	# Create the Tab2MAGE or MAGE-TAB exporter object.
	my $exporter = $self->create_exporter( $submission, $spreadsheet );

	# Here we actually do the MAGE-ML export.
	eval {

	    # Handle the combined MAGETAB doc case here.
	    if ( $exporter->can('parse_magetab_doc')
		 && $exporter->get_magetab_doc() ) {
		$exporter->parse_magetab_doc();
	    }

	    # Actually export the MAGE-ML.
	    $exporter->write_mageml();
	};

	print PARENT ("$EVAL_ERROR\n");

	close PARENT;

	exit;
    }

    return $exporter_signal; # non-zero is failure
}

sub monitor_submissions : RESTRICTED {
    my $self = shift;

    # Loop forever
    EVENT_LOOP:
    while (1) {

	# Create the accession cache object.
	my $iterator
	    = ArrayExpress::AutoSubmission::DB::Experiment->search(
		experiment_type => $self->get_experiment_type(),
		status          => $CONFIG->get_STATUS_PASSED(),
		is_deleted      => 0,
	);

	SUBMISSION:
	while ( my $submission = $iterator->next() ) {

	    # Skip submissions by test user, or submissions without any user.
	    next SUBMISSION if ( ! $submission->user_id()
				|| $submission->user_id()->login() eq 'test' );

	    # Start a transaction.
	    my $dbh = ArrayExpress::AutoSubmission::DB::Experiment->db_Main();
	    $dbh->begin_work();

	    # Get current status and a row-level read lock.
	    my $current_status =
		ArrayExpress::AutoSubmission::DB::Experiment->sql_status_for_update()
		    ->select_val( $submission->id() ) || q{};

	    # Double-check, in case something changes in the meantime.
	    unless ( $current_status eq $CONFIG->get_STATUS_PASSED() ) {
		$dbh->commit();
		next SUBMISSION;
	    }

	    # Set the status so a parallel daemon doesn't pick up.
	    $submission->set(
		status              => $CONFIG->get_STATUS_EXPORT(),
		date_last_processed => date_now(),
	    );
	    $submission->update();

	    # End the transaction.
	    $dbh->commit();

	    # Actually export the MAGE-ML.
	    my $start_time = date_now();
	    my $rc = $self->export_tab2mage( $submission );
	    my $end_time = date_now();

	    if ( ! $rc ) {    # Test for export success.
		$submission->set(
		    status              => $CONFIG->get_STATUS_COMPLETE(),
		    date_last_processed => $end_time,
		);
	    }
	    else {                    # Export failed
		$submission->set(
		    status              => $CONFIG->get_STATUS_EXPORT_ERROR(),
		    date_last_processed => $end_time,
		    comment             => $submission->comment()
			                   . "\n\n$rc\n",
		);
	    }
	    $submission->update();

	    # Add an event to record this run.
	    $submission->add_to_events({
		event_type       => 'MAGE-ML Export',
		was_successful   => ( $rc ? 0 : 1 ),
		source_db        => $submission->experiment_type(),
		start_time       => $start_time,
		end_time         => $end_time,
		machine          => hostname(),
		operator         => $submission->curator(),
		log_file         => $self->get_logfile(),
		is_deleted       => 0,
	    });

	    last EVENT_LOOP if $self->get_quit_when_done();

	}
	
	sleep( $self->get_polling_interval() * 60 );

	last EVENT_LOOP if $self->get_quit_when_done();

    }

    return;
}

sub get_targetdir : RESTRICTED {

    my ( $self ) = @_;

    my $targetdir = $CONFIG->get_AUTOSUBMISSIONS_TARGET();
    if ( my $subdir = $self->get_pipeline_subdir() ) {
	$targetdir = File::Spec->catdir(
	    $targetdir,
	    $subdir,
	);
    }

    return $targetdir;
}

sub create_exporter : RESTRICTED {
    confess("Error: stub method called in abstract superclass");
}

1;
