#!/usr/bin/env perl
#
# Module used to construct MIAMExpress exporter daemon objects
#
# Tim Rayner 2007, ArrayExpress team, European Bioinformatics Institute
#
# $Id: MXExporter.pm 2012 2008-03-30 17:27:46Z tfrayner $
#

use strict;
use warnings;

package ArrayExpress::AutoSubmission::Daemon::MXExporter;
use base 'ArrayExpress::AutoSubmission::Daemon';

use English qw( -no_match_vars );
use Class::Std;
use DBI;
use File::Spec;
use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Common qw(date_now);
require ArrayExpress::AutoSubmission::DB::Experiment;
use Carp;
use POSIX qw(:sys_wait_h);
use Socket;
use IO::Socket;
use IO::Select;
use Sys::Hostname;

my %mx_export_command : ATTR( :get<mx_export_command>, :init_arg<mx_export_command> );

sub START {
    my ( $self, $id, $args ) = @_;

    croak("Error: no mx_export_command set.")
	unless ( $self->get_mx_export_command() );

    return;
}

sub export_mx_mageml : PRIVATE {

    my ( $self, $title, $software, $accession, $login, $subid ) = @_;

    unless ( $self->get_mx_export_command() ) {
        die("Error: MIAMExpress export command not defined.\n");
    }

    # Set up our sockets for parent-child communication
    socketpair( CHILD, PARENT, AF_UNIX, SOCK_STREAM, PF_UNSPEC )
      or die "Cannot create socketpair: $!";

    CHILD->autoflush(1);
    PARENT->autoflush(1);

    my $pid;
    my $selector = IO::Select->new();
    $selector->add( \*CHILD );

    my ( $exporter_signal, $logfile );

    if ( $pid = fork ) {

        # Parent process code

        close PARENT;

	my $child_signal;
        while ( my @handles = $selector->can_read ) {
            foreach my $handle (@handles) {
                recv( $handle, $child_signal, 1024, 0 );
            }
            last;
        }
	chomp $child_signal;

	# It's important to know where our logfile is.
	( $logfile, $exporter_signal ) = split /\t/, $child_signal;

        close CHILD;
        waitpid( $pid, 0 );
    }

    else {

        # Child process code; must end with an exit

        die "Error: Cannot fork: $!" unless defined $pid;
        close CHILD;
        $PROGRAM_NAME .= '_child';

	# First, sort out a log file for STDOUT/STDERR redirection.
	my $dbh = DBI->connect(
	    $CONFIG->get_MX_DSN(),      $CONFIG->get_MX_USERNAME(),
	    $CONFIG->get_MX_PASSWORD(), $CONFIG->get_MX_DBPARAMS(),
	) or die("Database connection error: $DBI::errstr\n");

	my $sth = $dbh->prepare(<<'QUERY');
select distinct TSYSOPT_VALUE from TSYSOPT
where TSYSOPT_CODE='experiment_datafiles_path'
QUERY

	$sth->execute() or die($sth->errstr);

	# FIXME we're assuming the query worked here.
	my $logfile = File::Spec->catfile(
	    $sth->fetchrow_arrayref()->[0],
	    $login,
	    "submission$subid",
	    "expt_${login}_sub${subid}_mxexporter.log",
	);

	$dbh->disconnect();

	my $sys_command = $self->get_mx_export_command()
	    . qq{ "$title" "$software" "$accession"};

	$self->redirect_stdout_to_file( $logfile );
	print STDOUT "Running command:\n\n$sys_command\n\n";

	my $rc = system($sys_command);

	# Pass the logfile back to our parent.
	print PARENT ("$logfile\t$rc\n");

	close PARENT;

	$self->restore_stdout_from_file();

	exit;
    }

    return ( $logfile, $exporter_signal ); # non-zero is failure
}

sub monitor_submissions : RESTRICTED {
    my $self = shift;

    # Loop forever
    EVENT_LOOP:
    while (1) {

	# Create the accession cache iterators.
	my $passed
	    = ArrayExpress::AutoSubmission::DB::Experiment->search(
		experiment_type => $self->get_experiment_type(),
		status          => $CONFIG->get_STATUS_PASSED(),
		is_deleted      => 0,
	);
	my $postponed
	    = ArrayExpress::AutoSubmission::DB::Experiment->search(
		experiment_type => $self->get_experiment_type(),
		status          => $CONFIG->get_STATUS_EXPORT_POSTPONED(),
		is_deleted      => 0,
	);

	SUBMISSION:
	while ( my $submission = ( $passed->next() || $postponed->next() ) ) {

	    # Start a transaction.
	    my $dbh = ArrayExpress::AutoSubmission::DB::Experiment->db_Main();
	    $dbh->begin_work();

	    # Get current status and a row-level read lock.
	    my $current_status =
		ArrayExpress::AutoSubmission::DB::Experiment->sql_status_for_update()
		    ->select_val( $submission->id() ) || q{};

	    # Double-check, in case something changes in the meantime.
	    unless (   $current_status eq $CONFIG->get_STATUS_PASSED()
		    || $current_status eq $CONFIG->get_STATUS_EXPORT_POSTPONED() ) {
		$dbh->commit();
		next SUBMISSION;
	    }

	    $submission->set(
		status              => $CONFIG->get_STATUS_EXPORT(),
		date_last_processed => date_now(),
	    );
	    $submission->update();

	    # End the transaction.
	    $dbh->commit();

	    my $start_time = date_now();
	    my ( $logfile, $rc ) = $self->export_mx_mageml(
		$submission->name(),
		$submission->software(),
		$submission->accession(),
		$submission->miamexpress_login(),
		$submission->miamexpress_subid(),
	    );
	    my $end_time = date_now();

	    if ( ! $rc ) {

		# False == success.
		$submission->set(
		    status              => $CONFIG->get_STATUS_COMPLETE(),
		    date_last_processed => $end_time,
		);
	    }
	    elsif ( (unpack "c", pack "C", $rc >> 8) == -7 ) {

		# Export failed due to connectivity problem ("exit -7").
		$submission->set(
		    status              => $CONFIG->get_STATUS_EXPORT_POSTPONED(),
		    date_last_processed => $end_time,
		);
	    }
	    else {

		# Export failed.
		$submission->set(
		    status              => $CONFIG->get_STATUS_EXPORT_ERROR(),
		    date_last_processed => $end_time,
		);
	    }
	    $submission->update();

	    # Add an event to record this run.
	    $submission->add_to_events({
		event_type       => 'MAGE-ML Export',
		was_successful   => ( $rc ? 0 : 1 ),
		source_db        => $submission->experiment_type(),
		start_time       => $start_time,
		end_time         => $end_time,
		machine          => hostname(),
		operator         => $submission->curator(),
		log_file         => $logfile,
		is_deleted       => 0,
	    });

	    last EVENT_LOOP if $self->get_quit_when_done();

	}
	
	sleep( $self->get_polling_interval() * 60 );

	last EVENT_LOOP if $self->get_quit_when_done();

    }

    return;
}

1;
