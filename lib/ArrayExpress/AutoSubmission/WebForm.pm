#!/usr/bin/env perl
#
# Module used to generate and control web submission forms
#
# Tim Rayner 2006, ArrayExpress team, European Bioinformatics Institute
#
# $Id: WebForm.pm 2069 2008-06-04 14:33:52Z tfrayner $
#

package ArrayExpress::AutoSubmission::WebForm;

use strict;
use warnings;

use base 'CGI::Application';
use CGI::Application::Plugin::Authentication;
use CGI::Application::Plugin::Session;
use CGI::Application::Plugin::ValidateRM qw(check_rm);
use CGI::Upload;
use Data::FormValidator::Constraints qw(:closures);
use HTTP::BrowserDetect;
use MIME::Lite;

require ArrayExpress::AutoSubmission::DB::User;
require ArrayExpress::AutoSubmission::DB::Experiment;
require ArrayExpress::AutoSubmission::DB::Design;
require ArrayExpress::AutoSubmission::DB::Material;
require ArrayExpress::AutoSubmission::DB::Organism;
require ArrayExpress::AutoSubmission::DB::Role;
require ArrayExpress::AutoSubmission::DB::RoleUser;
require ArrayExpress::AutoSubmission::DB::DataFile;
require ArrayExpress::AutoSubmission::DB::Spreadsheet;

require ArrayExpress::AutoSubmission::Spreadsheet;
use ArrayExpress::Curator::Common qw(mage_date date_now untaint);
use ArrayExpress::Curator::Config qw($CONFIG);
use File::Spec;
use File::Path;
use File::Copy;
use File::Basename;
use IO::File;
use Readonly;
use Carp;

# The DevPopup plugin can be commented out in production.
#use CGI::Application::Plugin::DevPopup;
#use CGI::Application::Plugin::HtmlTidy;

#################
# Runmode names #
#################

# N.B. these terms are used in the web interface.
Readonly my $RM_INTRO       => 'Introduction';
Readonly my $RM_SIGNUP      => 'New submitter';
Readonly my $RM_CREATEUSER  => 'Create user';
Readonly my $RM_LOGIN       => 'Log in';
Readonly my $RM_LOGOUT      => 'Log out';
Readonly my $RM_CHOOSE      => 'Experiment list';
Readonly my $RM_CREATE      => 'Create new';
Readonly my $RM_INSERT      => 'Save experiment';
Readonly my $RM_EDIT        => 'Edit';
Readonly my $RM_UPDATE      => 'Update';
Readonly my $RM_DELETE      => 'Delete';
Readonly my $RM_TEMPLATE    => 'Generate template';
Readonly my $RM_UPLOAD      => 'Upload files';
Readonly my $RM_FILE_UPLOAD => 'Upload';
Readonly my $RM_SUBMIT      => 'Submit experiment';
Readonly my $RM_ERROR       => 'Error';

Readonly my $ERROR_FORMAT   => q{<span style="color:red;font-weight:bold" class="dfv_errors">- %s</span>};

#########################
# Package configuration #
#########################

__PACKAGE__->authen->config(
    DRIVER => [
        'CDBI',
        CLASS => 'ArrayExpress::AutoSubmission::DB::User',

        # Use MD5_hex:password for greater security (this would also
        # need fixing in the ruby code).
        FIELD_METHODS => [qw(login password)],
    ],
    CREDENTIALS           => [qw(auth_username auth_password)],
    LOGIN_RUNMODE         => $RM_LOGIN,
    POST_LOGIN_RUNMODE    => $RM_CHOOSE,
    POST_LOGIN_CALLBACK   => \&update_user_access,
    LOGOUT_RUNMODE        => $RM_LOGOUT,
    LOGIN_SESSION_TIMEOUT => {
        IDLE_FOR => '60m',
        EVERY    => '1d'
    },
);

__PACKAGE__->authen->protected_runmodes(
    qw(
      choose
      create
      insert
      edit
      update
      delete_expt
      template
      upload
      file_upload
      )
);

sub setup {
    my $self = shift;
    $self->start_mode($RM_INTRO);
    $self->error_mode('error');
    $self->run_modes(
        $RM_INTRO       => 'intro',
        $RM_SIGNUP      => 'signup',
	$RM_CREATEUSER  => 'create_user',
        $RM_LOGIN       => 'login',
        $RM_LOGOUT      => 'logout',
        $RM_CHOOSE      => 'choose',
	$RM_CREATE      => 'create',
	$RM_INSERT      => 'insert',
        $RM_EDIT        => 'edit',
        $RM_UPDATE      => 'update',
	$RM_DELETE      => 'delete_expt',
        $RM_TEMPLATE    => 'spreadsheet_template',
        $RM_UPLOAD      => 'upload',
	$RM_FILE_UPLOAD => 'file_upload',
	$RM_SUBMIT      => 'submit',
        $RM_ERROR       => 'error',
    );
    $self->mode_param('rm');

    # Templates are kept in a subdirectory within the package, but can
    # be overridden using the 'template_path' param..
    my @module_dir_array = File::Spec->splitpath(__FILE__);
    my $template_path    =
      File::Spec->catpath( @module_dir_array[ 0, 1 ], 'templates' );
    $self->tmpl_path($self->param('template_path') || $template_path);

    return;
}

##################
# Shared methods #
##################

sub cgiapp_init {

    my $self = shift;

    # Data::FormValidator defaults.
    $self->param('dfv_defaults') ||
	$self->param('dfv_defaults', {
	    msgs => {
		any_errors  => 'some_errors',
		prefix      => 'err_',
		invalid_separator => ' <br/> ',
		missing  => q{Missing value(s)},
		invalid  => q{Invalid value(s)},
		format   => $ERROR_FORMAT
		}
	});

    return;
}

sub cgiapp_postrun {

    my ($self, $contentref) = @_;

# Uncomment this for production code.
#  $self->htmltidy_clean($contentref);

    return;
}

sub _shared_html_params {
    my $self = shift;

    # Set the HTML::Template params sent to all runmodes.
    return {
	CGI_BASE   => $self->param('cgi_base')          || '/tab2mage.cgi',
        STYLESHEET => $self->param('stylesheet')        || undef,
	SIDEBAR_ICONS => $self->param('sidebar_icons'),
	SIDEBAR_LINKS => $self->param('sidebar_links'),
        RM         => $self->mode_param(),
	AUTHEN     => $self->authen->is_authenticated() || undef,
        EXPT_TYPE  => $self->param('experiment_type')   || undef,
	LOGOUT     => $RM_LOGOUT,
	CURATOR_EMAIL => $CONFIG->get_AUTOSUBS_CURATOR_EMAIL(),
    };
}

sub _load_my_tmpl {
    my ($self, $tmpl_file, $errors, $params) = @_;

    # Convenience method for generating templates.

    # Filter to allow for dynamic TMPL_INCLUDEs.
    my $header = $self->param('header_html_template') || '_head.html';
    my $footer = $self->param('footer_html_template') || '_foot.html';
    my $filter = sub {
	my $text_ref = shift;
	$$text_ref =~ s{<(!--)? *TMPL_INCLUDE *HEADER *(--)?>}
	               {<!-- TMPL_INCLUDE NAME="$header" -->}i;
	$$text_ref =~ s{<(!--)? *TMPL_INCLUDE *FOOTER *(--)?>}
	               {<!-- TMPL_INCLUDE NAME="$footer" -->}i;
    };

    # Create the actual HTML::Template object.
    my $template = $self->load_tmpl(
	$tmpl_file,
	filter            => $filter,
	die_on_bad_params => 1,
    );
    $template->param($errors) if $errors;
    $template->param(
	%{$self->_shared_html_params()},
	%{$params},
    );

    return $template;
}

sub _annotation_labels {
    my ($self, $experiment) = @_;

    # Get the design, material and organism info from the database.

    # Designs:
    my %selected_design = $experiment
	? map { $_->id() => 1 } $experiment->designs()
	: ();
    my @biol_design_labels  = map {
	{ name      => $_->display_label(),
	  id        => $_->id(),
	  selected  => $selected_design{$_->id()}}
    } ArrayExpress::AutoSubmission::DB::Design->search(
	design_type => 'biological',
	is_deleted  => 0,
	{order_by   => 'display_label'},
    );
    my @meth_design_labels  = map {
	{ name      => $_->display_label(),
	  id        => $_->id(),
	  selected  => $selected_design{$_->id()}}
    } ArrayExpress::AutoSubmission::DB::Design->search(
	design_type => 'methodological',
	is_deleted  => 0,
	{order_by   => 'display_label'},
    );
    my @tech_design_labels  = map {
	{ name      => $_->display_label(),
	  id        => $_->id(),
	  selected  => $selected_design{$_->id()}}
    } ArrayExpress::AutoSubmission::DB::Design->search(
	design_type => 'technological',
	is_deleted  => 0,
	{order_by   => 'display_label'},
    );

    # Materials:
    my %selected_material = $experiment
	? map { $_->id() => 1 } $experiment->materials()
	: ();
    my @material_labels  = map {
	{ name      => $_->display_label(),
	  id        => $_->id(),
	  selected  => $selected_material{$_->id()}}
    } ArrayExpress::AutoSubmission::DB::Material->search_like(
	id          => '%',
	is_deleted  => 0,
	{order_by   => 'display_label'},
    );

    # Organisms (only list those with taxon ids):
    my %selected_organism = $experiment
	? map { $_->id() => 1 } $experiment->organisms()
	: ();
    my @organism_labels  = map {
	{ name      => $_->scientific_name(),
	  id        => $_->id(),
	  selected  => $selected_organism{$_->id()}}
    } ArrayExpress::AutoSubmission::DB::Organism->search_like(
	id          => '%',
	taxon_id    => '_%',
	is_deleted  => 0,
	{order_by   => 'scientific_name'},
    );

    return {
	BIOL_DESIGNS     => \@biol_design_labels,
	METH_DESIGNS     => \@meth_design_labels,
	TECH_DESIGNS     => \@tech_design_labels,
	MATERIALS        => \@material_labels,
	ORGANISMS        => \@organism_labels,
    };
}

sub _is_not_duplicate_filename {
    my ($name, $experiment_id) = @_;

    # Ensure name is stringified - this sub is passed CGI upload
    # params; they do not stringify correctly with all versions of the
    # underlying modules.
    $name = sprintf("%s", $name);

    # Check the database for entries, return false if found.
    my $spreadsheet
	= ArrayExpress::AutoSubmission::DB::Spreadsheet->retrieve(
	    name          => $name,
	    experiment_id => $experiment_id,
	    is_deleted    => 0,
	);
    my $data_file
	= ArrayExpress::AutoSubmission::DB::DataFile->retrieve(
	    name          => $name,
	    experiment_id => $experiment_id,
	    is_deleted    => 0,
	);
    if ($spreadsheet || $data_file) {
	return 0;
    }

    # Check the filesystem, return true if found, false otherwise.
    my $experiment
	= ArrayExpress::AutoSubmission::DB::Experiment->retrieve(
	    id => $experiment_id,
	);
    my $subs_dir     = $experiment->filesystem_directory();
    my $upload_file  = untaint(basename( $name ));
    my $filesys_test = -e File::Spec->catfile($subs_dir, $upload_file);

    return $filesys_test ? 0 : 1;
}

################################
# Data::FormValidater profiles #
################################

sub _user_profile {
    my $self = shift;

    return {
	required           => [qw(username password realname email)],
	optional           => [qw()],
	dependency_groups  => {password_group => [qw/password password2/]},
	filters            => ['trim'],
	constraints        => {
	    email    => 'email',
	    username => [
		{
		    constraint => qr/^\w{6,10}$/,
		},
		{
		    name       => 'duplicate_username',
		    constraint => sub {
			my $user
			    = ArrayExpress::AutoSubmission::DB::User->retrieve(
				login => shift,
			    );
			return $user ? 0 : 1;
		    },
		},
	    ],
	    password => [
		{
		    constraint => qr/^\w{6,10}$/,
		},
		{
		    name       => 'password_mismatch',
		    constraint => sub {
			my ($password, $password2) = @_;
			return ($password eq $password2);
		    },
		    params => [qw(password password2)],
		},
	    ],
	},
	msgs => {
	    constraints => {
		'duplicate_username'
		    => q{Username is already taken},
		'password_mismatch'
		    => q{Passwords do not match},
	    },
	},
    };
}

sub _choose_profile {
    my $self = shift;

    return {
	required => [qw(experiment)],
	msgs     => {
	    missing => q{No experiment selected.},
	}
    };
}

sub _delete_profile {
    my $self = shift;

    return {
	required           => [qw(experiment)],
	constraints        => {
	    experiment     => [
		{
		    name   => 'files_exist',
		    constraint => sub {
			my $experiment
			    = ArrayExpress::AutoSubmission::DB::Experiment->retrieve(shift);
			my $files = scalar($experiment->data_files(is_deleted => 0))
			    + scalar($experiment->spreadsheets(is_deleted => 0));
			return $files ? 0 : 1;
		    },
		},
	    ],
	},
	msgs => {
	    constraints => {
		'files_exist'
		    => q{Submission includes uploaded files and cannot be deleted.},
	    },
	    missing => q{No experiment selected.},
	},
    };
}

sub _edit_profile {
    my $self = shift;

    return {
	required           => [qw(experiment_name)],
	filters            => ['trim'],
	dependency_groups  => {annotation_group => [qw/biol_designs meth_designs tech_designs materials organisms/]},
	constraints        => {
	    experiment_name => [
		{
		    name       => 'duplicate_submission',
		    constraint => sub {
			my ($name, $id) = @_;
			my $experiment
			    = ArrayExpress::AutoSubmission::DB::Experiment->retrieve(
				name       => $name,
				is_deleted => 0,
			    );
			# No experiment found, or experiment has our
			# id (i.e. this is an update).
			return (!$experiment || $experiment->id() == $id);
		    },
		    params => [qw(experiment_name experiment)],
		},
	    ],
	},
	msgs => {
	    constraints => {
		'duplicate_submission'
		    => q{Experiment name is already taken},
	    },
	},
    };
}

sub _upload_profile {
    my $self = shift;

    return {
	require_some       => {
	    upload_files => [ 1, qw(data_file spreadsheet) ],
	},
    };
}

sub _submit_profile {

    return {
	required => [qw(experiment)],
        constraints => {
            experiment => [
                {
                    name       => 'missing_uploads',
                    constraint => sub {
                        my $id = shift;
                        my $experiment
                            = ArrayExpress::AutoSubmission::DB::Experiment->retrieve($id)
				or croak("Internal error: No experiment found in database");
                        my $allfiles = scalar($experiment->data_files(is_deleted => 0))
                            && scalar($experiment->spreadsheets(is_deleted => 0));
                        return $allfiles ? 1 : 0;
                    },
                    params     => [qw(experiment)],
                },
            ],
        },
	msgs     => {
	    constraints => {
		'missing_uploads' => q{Some file(s) have not been uploaded yet},
	    },
	    missing => q{Internal error: No experiment parameter defined},
	},
    };
}

#############################
# Other convenience methods #
#############################

sub update_user_access {
    my $self = shift;

    return unless ($self->authen->is_authenticated());

    my $user
	= ArrayExpress::AutoSubmission::DB::User->retrieve(
	    login => $self->authen->username(),
	);

    $user->set(
	access => mage_date($self->authen->last_access()),
    );

    $user->update();

    return;
}

sub get_secure_experiment {
    my ($self, $id) = @_;
    
    my $experiment =
      ArrayExpress::AutoSubmission::DB::Experiment->retrieve($id);

    # Double-check that the experiment does actually belong to the user.
    unless ( $experiment->user_id->login() eq $self->authen->username() ) {
        return (undef, $self->error('Attempt to access forbidden page.'));
    }

    return $experiment;
};

sub email_confirm_signup {

    # $user is a Class::DBI object.
    my ($self, $user) = @_;

    # No point trying to send to a null address.
    return unless $user->email();

    my $template = $self->load_tmpl('email_signup_confirmation.txt');
    $template->param(
	TYPE     => $self->param('experiment_type'),
	USERNAME => $user->login(),
	PASSWORD => $user->password(),
	REALNAME => $user->name(),
	URL      => $self->query()->url(),
    );

    my $mail = MIME::Lite->new(
	From     => $CONFIG->get_AUTOSUBS_CURATOR_EMAIL(),
	To       => $user->email(),
	Subject  => 'Welcome to ArrayExpress '
	    . $self->param('experiment_type') . ' Submissions',
	Encoding => 'quoted-printable',
	Data     => $template->output,
	Type     => 'text/plain',
    );

    # Must use SMTP if running under CGI::Application::Server
    if ( my $server = $CONFIG->get_AUTOSUBS_SMTP_SERVER() ) {
	$mail->send('smtp', $server)
	    or die("Error sending signup email: $!");
    }
    else {
	$mail->send()
	    or die("Error sending signup email: $!");
    }

    return;
}

sub email_confirm_submission {
    my ($self, $experiment) = @_;

    # No point trying to send to a null address.
    return unless $experiment->user_id()->email();

    # $experiment is a Class::DBI object.
    my $template = $self->load_tmpl('email_subs_confirmation.txt');
    $template->param(
	TYPE          => $self->param('experiment_type'),
	EXPTNAME      => $experiment->name(),
	REALNAME      => $experiment->user_id()->name(),
	SUBSDATE      => $experiment->date_submitted(),
	CURATOR_EMAIL => $CONFIG->get_AUTOSUBS_CURATOR_EMAIL(),
    );

    my $submitter_mail = MIME::Lite->new(
	From     => $CONFIG->get_AUTOSUBS_CURATOR_EMAIL(),
	To       => $experiment->user_id()->email(),
	Subject  => 'Confirming '
	    . $self->param('experiment_type')
		. ' submission: '
		    . $experiment->name(),
	Encoding => 'quoted-printable',
	Data     => $template->output,
	Type     => 'text/plain',
    );

    my $curator_tmpl = $self->load_tmpl('subs_notify_curator.txt');
    $curator_tmpl->param(
	TYPE          => $self->param('experiment_type'),
	EXPTNAME      => $experiment->name(),
	EXPTDIR       => $experiment->filesystem_directory(),
	REALNAME      => $experiment->user_id()->name(),
	LOGIN         => $experiment->user_id()->login(),
	SUBSDATE      => $experiment->date_submitted(),
	SUBMTR_EMAIL  => $experiment->user_id()->email(),
    );

    # Check whether this is a resubmission, and set the subject line
    # appropriately.
    my $subject;
    if ( $experiment->num_submissions() ) {
	$subject = sprintf(
	    "%s RESUBMISSION: %s",
	    $self->param('experiment_type'),
	    $experiment->name(),
	);
    }
    else {
	$subject = sprintf(
	    "New %s submission: %s",
	    $self->param('experiment_type'),
	    $experiment->name(),
	);
    }
    my $curator_mail = MIME::Lite->new(
	From     => $CONFIG->get_AUTOSUBS_ADMIN(),
	To       => $CONFIG->get_AUTOSUBS_CURATOR_EMAIL(),
	Subject  => $subject,
	Encoding => 'quoted-printable',
	Data     => $curator_tmpl->output,
	Type     => 'text/plain',
    );

    # Must use SMTP if running under CGI::Application::Server
    if ( my $server = $CONFIG->get_AUTOSUBS_SMTP_SERVER() ) {
	foreach my $missive ($submitter_mail, $curator_mail) {
	    $missive->send('smtp', $server)
		or die("Error sending signup email: $!");
	}
    }
    else {
	foreach my $missive ($submitter_mail, $curator_mail) {
	    $missive->send()
		or die("Error sending signup email: $!");
	}
    }

    return;
}

###################
# Runmode methods #
###################

sub intro {
    my ($self, $errors) = @_;

    my $template = $self->_load_my_tmpl(
	$self->param('intro_html_template') || 'intro.html',
	$errors,
	{
	    TITLE      => $self->param('experiment_type')
	                . ' ArrayExpress submissions',
	    LOGIN      => $RM_LOGIN,
	    SIGNUP     => $RM_SIGNUP,
	},
    );

    return $template->output();
}

sub signup {
    my ($self, $errors) = @_;

    my $template = $self->_load_my_tmpl(
	$self->param('signup_html_template') || 'signup.html',
	$errors,
	{
	    TITLE      => 'Create new account',
	    USERNAME   => 'username',
	    PASSWORD   => 'password',
	    PASSWORD2  => 'password2',
	    REALNAME   => 'realname',
	    EMAIL      => 'email',
	    CREATEUSER => $RM_CREATEUSER,
	},
    );

    return $template->output();
}

sub create_user {
    my ($self, $errors) = @_;

    # Validate the input.
    my ($results, $err_page) =
	$self->check_rm('signup', '_user_profile');
    return $err_page if $err_page;

    # Get CGI query object.
    my $q = $self->query();

    # FIXME we also need a confirmation email sent to the user. FIXME
    # also strip surrounding whitespace.
    my $user
	= ArrayExpress::AutoSubmission::DB::User->insert({
	login      => $q->param('username'),
	password   => $q->param('password'),
	name       => $q->param('realname'),
	email      => $q->param('email'),
	created_at => date_now(),
	is_deleted => 0,
    });

    # All users signing up here are just submitters.
    my $role
	= ArrayExpress::AutoSubmission::DB::Role->retrieve(
	    name => 'submitter',
	);
    ArrayExpress::AutoSubmission::DB::RoleUser->insert({
	role_id => $role,
	user_id => $user,
    });

    $self->email_confirm_signup($user);

    my $template = $self->_load_my_tmpl(
	$self->param('create_user_html_template') || 'create_user.html',
	$errors,
	{
	    TITLE      => 'User successfully created',
	    USERNAME   => 'auth_username',
	    PASSWORD   => 'auth_password',
	    USER       => $user->login(),
	    PASS       => $user->password(),
	    REAL       => $user->name(),
	    EMAIL      => $user->email(),
	    CHOOSE     => $RM_CHOOSE,
	},
    );

    return $template->output();
}

sub login {
    my ($self, $errors) = @_;

    # Get CGI query object
    my $q = $self->query();

    # N.B. CGI 'login' param should only be set if the previous login
    # failed.
    my $template = $self->_load_my_tmpl(
	$self->param('login_html_template') || 'login.html',
	$errors,
	{
	    TITLE => 'Log in to '
		. $self->param('experiment_type')
		. ' submissions',
	    LOGIN      => $RM_LOGIN,
	    SIGNUP     => $RM_SIGNUP,
	    USERNAME   => 'auth_username',
	    PASSWORD   => 'auth_password',
	    FAILED     => $q->param('login') || 0,
	},
    );

    return $template->output();
}

sub logout {
    my ($self, $errors) = @_;

    # Log the user out. Check for success FIXME.
    $self->authen->logout();

    my $template = $self->_load_my_tmpl(
	$self->param('logout_html_template') || 'logout.html',
	$errors,
	{
	    LOGIN => $RM_LOGIN,
	},
    );

    return $template->output();
}

sub choose {
    my ( $self, $errors ) = @_;
    $self->authen->is_authenticated() or return $self->login();

    my $user =
      ArrayExpress::AutoSubmission::DB::User->retrieve(
        login => $self->authen->username(),
    );
    my @experiments = ArrayExpress::AutoSubmission::DB::Experiment->search(
        user_id         => $user->id(),
        experiment_type => $self->param('experiment_type'),
        in_curation     => 0,
	is_deleted      => 0,
    );
    my @submitted = ArrayExpress::AutoSubmission::DB::Experiment->search(
        user_id         => $user->id(),
        experiment_type => $self->param('experiment_type'),
        in_curation     => 1,
	is_deleted      => 0,
    );

    my $template = $self->_load_my_tmpl(
        $self->param('choose_html_template') || 'choose.html',
        $errors,
        {
            EXPERIMENTS =>
              [ map { { name => $_->name(), id => $_->id() } } @experiments ],
            SUBMITTED => [
                map { { name => $_->name(), date => $_->date_submitted() } }
                  @submitted
            ],
            TITLE => 'Your ' . $self->param('experiment_type') . ' submissions',
            CREATE => $RM_CREATE,
            UPLOAD => $RM_UPLOAD,
            EDIT   => $RM_EDIT,
            DELETE => $RM_DELETE,
        },
    );

    return $template->output();
}

sub create {
    my ($self, $errors) = @_;
    $self->authen->is_authenticated() or return $self->login();

    # Get CGI query object.
    my $q = $self->query();

    # Build the page and return it.
    my $template = $self->_load_my_tmpl(
	$self->param('create_html_template') || 'create.html',
	$errors,
	{
	    %{$self->_annotation_labels()},
	    EXPT_NAME        => undef,
	    EXPT_NAME_PARAM  => 'experiment_name',
	    AFFYMETRIX       => undef,
	    AFFYMETRIX_PARAM => 'is_affymetrix',
	    TITLE            => 'Create experiment',
	    INSERT           => $RM_INSERT,
	},
    );

    return $template->output();
}

sub insert {
    my ($self, $errors) = @_;
    $self->authen->is_authenticated() or return $self->login();

    # Validate the input.
    my ($results, $err_page) =
	$self->check_rm('create', '_edit_profile');
    return $err_page if $err_page;

    # Get CGI query object
    my $q = $self->query();

    my $user = ArrayExpress::AutoSubmission::DB::User->retrieve(
	login => $self->authen->username()
    );
    my @designs   = map {
	ArrayExpress::AutoSubmission::DB::Design->retrieve($_)
    } $q->param('biol_designs'),
	$q->param('meth_designs'),
	    $q->param('tech_designs');
    my @materials   = map {
	ArrayExpress::AutoSubmission::DB::Material->retrieve($_)
    } $q->param('materials');
    my @organisms   = map {
	ArrayExpress::AutoSubmission::DB::Organism->retrieve($_)
    } $q->param('organisms');

    my $experiment =
      ArrayExpress::AutoSubmission::DB::Experiment->insert({
	  name             => $q->param('experiment_name'),
	  user_id          => $user->id(),
	  experiment_type  => $self->param('experiment_type'),
	  is_affymetrix    => $q->param('is_affymetrix') ? 1 : 0,
	  date_last_edited => date_now(),
	  in_curation      => 0,
	  num_submissions  => 0,
	  is_deleted       => 0,
      });
    foreach my $design (@designs) {
	$experiment->add_to_design_instances({
	    experiment_id => $experiment,
	    design_id     => $design,
	});
    }
    foreach my $material (@materials) {
	$experiment->add_to_material_instances({
	    experiment_id => $experiment,
	    material_id   => $material,
	});
    }
    foreach my $organism (@organisms) {
	$experiment->add_to_organism_instances({
	    experiment_id => $experiment,
	    organism_id   => $organism,
	});
    }

    my $is_annotated = scalar(@designs)
	            && scalar(@materials)
	            && scalar(@organisms);

    # Build the page and return it.
    my $template = $self->_load_my_tmpl(
	$self->param('insert_html_template') || 'insert.html',
	$errors,
	{
	    EXPT_ID    => $experiment->id(),
	    EXPT_NAME  => $experiment->name(),
	    TITLE      => 'Experiment created',
	    TEMPLATE   => $RM_TEMPLATE,
	    ANNOTATED  => $is_annotated,
	    UPLOAD     => $RM_UPLOAD,
	    SUBMIT     => $RM_SUBMIT,
	    CHOOSE     => $RM_CHOOSE,
	    AFFYMETRIX => $q->param('is_affymetrix') ? 1 : 0,
	    DESIGNS    => [map { {name => $_->display_label()} } @designs],
	    MATERIALS  => [map { {name => $_->display_label()} } @materials],
	    ORGANISMS  => [map { {name => $_->scientific_name()} } @organisms],
	},
    );

    return $template->output();
}

sub edit {
    my ($self, $errors) = @_;
    $self->authen->is_authenticated() or return $self->login();

    # Validate the input.
    my ($results, $err_page) =
	$self->check_rm('choose', '_choose_profile');
    return $err_page if $err_page;

    # Get CGI query object.
    my $q = $self->query();

    # Get the experiment, checking that the user is the owner.
    my ($experiment, $illegal_access) = $self->get_secure_experiment(
	$q->param('experiment'),
    );
    return $illegal_access unless $experiment;

    # Build the page and return it.
    my $template = $self->_load_my_tmpl(
	$self->param('edit_html_template') || 'edit.html',
	$errors,
	{
	    %{$self->_annotation_labels($experiment)},
	    EXPT_ID          => $experiment->id(),
	    EXPT_NAME        => $experiment->name(),
	    EXPT_NAME_PARAM  => 'experiment_name',
	    AFFYMETRIX       => $experiment->is_affymetrix(),
	    AFFYMETRIX_PARAM => 'is_affymetrix',
	    TITLE            => 'Edit submission',
	    UPDATE           => $RM_UPDATE,
	},
    );

    return $template->output();
}

sub update {
    my ($self, $errors) = @_;
    $self->authen->is_authenticated() or return $self->login();

    # Validate the input.
    my ($results, $err_page) =
	$self->check_rm('edit', '_edit_profile');
    return $err_page if $err_page;

    # Get CGI query object
    my $q = $self->query();

    # Get the experiment, checking that the user is the owner.
    my ($experiment, $illegal_access) = $self->get_secure_experiment(
	$q->param('experiment'),
    );
    return $illegal_access unless $experiment;

    $experiment->set(
	name             => $q->param('experiment_name'),
	is_affymetrix    => $q->param('is_affymetrix') ? 1 : 0,
	date_last_edited => date_now(),
    );
    $experiment->update();

    my @designs   = map {
	ArrayExpress::AutoSubmission::DB::Design->retrieve($_)
    } $q->param('biol_designs'),
	$q->param('meth_designs'),
	    $q->param('tech_designs');
    $experiment->update_designs(\@designs);

    my @materials   = map {
	ArrayExpress::AutoSubmission::DB::Material->retrieve($_)
    } $q->param('materials');
    $experiment->update_materials(\@materials);

    my @organisms   = map {
	ArrayExpress::AutoSubmission::DB::Organism->retrieve($_)
    } $q->param('organisms');
    $experiment->update_organisms(\@organisms);

    my $is_annotated = scalar(@designs)
	            && scalar(@materials)
	            && scalar(@organisms);

    # Build the page and return it.
    my $template = $self->_load_my_tmpl(
	$self->param('update_html_template') || 'update.html',
	$errors,
	{
	    EXPT_ID    => $experiment->id(),
	    EXPT_NAME  => $experiment->name(),
	    AFFYMETRIX => $q->param('is_affymetrix') ? 1 : 0,
	    TITLE      => 'Experiment updated',
	    TEMPLATE   => $RM_TEMPLATE,
	    ANNOTATED  => $is_annotated,
	    UPLOAD     => $RM_UPLOAD,
	    SUBMIT     => $RM_SUBMIT,
	    CHOOSE     => $RM_CHOOSE,
	    DESIGNS    => [map { {name => $_->display_label()} } @designs],
	    MATERIALS  => [map { {name => $_->display_label()} } @materials],
	    ORGANISMS  => [map { {name => $_->scientific_name()} } @organisms],
	},
    );

    return $template->output();
}

sub delete_expt {
    my ($self, $errors) = @_;
    $self->authen->is_authenticated() or return $self->login();

    # Validate the input. This checks if files have been uploaded, and
    # forbids deletion if so.
    my ($results, $err_page) =
	$self->check_rm('choose', '_delete_profile');
    return $err_page if $err_page;

    # Get CGI query object.
    my $q = $self->query();

    # Get the experiment, checking that the user is the owner.
    my ($experiment, $illegal_access) = $self->get_secure_experiment(
	$q->param('experiment'),
    );
    return $illegal_access unless $experiment;

    # Delete the object. We just mark it as is_deleted, rather than
    # removing it from the database.
    my $experiment_name = $experiment->name();
    $experiment->set(is_deleted => 1);
    $experiment->update();

    # Build the page and return it.
    my $template = $self->_load_my_tmpl(
	$self->param('delete_html_template') || 'delete.html',
	$errors,
	{
	    EXPT_NAME  => $experiment_name,
	    TITLE      => 'Experiment deleted',
	    CHOOSE     => $RM_CHOOSE,
	},
    );

    return $template->output();
}

sub spreadsheet_template {
    my ($self, $errors) = @_;
    $self->authen->is_authenticated() or return $self->login();

    # Get CGI query object
    my $q = $self->query();

    # Get the experiment, checking that the user is the owner.
    my ($experiment, $illegal_access) = $self->get_secure_experiment(
	$q->param('experiment'),
    );
    return $illegal_access unless $experiment;

    # Generate the spreadsheet and return it.
    my $spreadsheet = ArrayExpress::AutoSubmission::Spreadsheet->new({
	experiment => $experiment,
	url        => $self->query()->url(),
	date       => date_now(),
    });

    my $basename = sprintf(
	"%s_%s_%s_%s.txt",
	untaint($experiment->user_id()->login()),
	$self->param('experiment_type'),
	untaint($experiment->name()),
	$spreadsheet->get_date(),
    );

    # Strip some unwanted characters from the filename.
    $basename =~ s/[ :\\\/]/_/g;

    my $filename = File::Spec->catfile($CONFIG->get_WEBFORM_TEMPLATE_DIR(), $basename);
    my $fileurl  = $CONFIG->get_WEBFORM_TEMPLATE_URL();
    $fileurl =~ s/[\/]?$/\/$basename/;

    my $ss_fh = IO::File->new($filename, q{>})
	or croak("Error: unable to open spreadsheet template file $filename for writing: $!");
    my $ss_content;
    if ( $self->param('spreadsheet_type') =~ m/\A mage-?tab \z/ixms ) {
	$ss_content = $spreadsheet->magetab();
    }
    else {
	$ss_content = $spreadsheet->tab2mage();
    }
	   
    print $ss_fh $ss_content
	or croak("Error: unable to write output to spreadsheet template file $filename: $!");

    # Build the page and return it.
    my $template = $self->_load_my_tmpl(
	$self->param('template_html_template') || 'template.html',
	$errors,
	{
	    EXPT_ID    => $experiment->id(),
	    EXPT_NAME  => $experiment->name(),
	    TITLE      => 'Template download',
	    SS_LINK    => $fileurl,
	    UPLOAD     => $RM_UPLOAD,
	    CHOOSE     => $RM_CHOOSE,
	},
    );

    return $template->output();
}

sub upload {
    my ($self, $errors) = @_;
    $self->authen->is_authenticated() or return $self->login();

    # Validate the input.
    my ($results, $err_page) =
	$self->check_rm('choose', '_choose_profile');
    return $err_page if $err_page;

    # Get CGI query object
    my $q = $self->query();

    # Get the experiment, checking that the user is the owner.
    my ($experiment, $illegal_access) = $self->get_secure_experiment(
	$q->param('experiment'),
    );
    return $illegal_access unless $experiment;

    my @spreadsheets = map {
	{ name      => $_->name(), }
    } $experiment->spreadsheets(is_deleted => 0);

    my @data_files = map {
	{ name      => $_->name(), }
    } $experiment->data_files(is_deleted => 0);

    # Build the page and return it.
    my $template = $self->_load_my_tmpl(
	$self->param('upload_html_template') || 'upload.html',
	$errors,
	{
	    EXPT_NAME       => $experiment->name(),
	    EXPT_ID         => $experiment->id(),
	    TITLE           => 'File upload',
	    CHOOSE          => $RM_CHOOSE,
	    FILE_UPLOAD     => $RM_FILE_UPLOAD,
	    SUBMIT          => $RM_SUBMIT,
	    DATA_FILE       => 'data_file',
	    SPREADSHEET     => 'spreadsheet',
	    SHEETS_LOADED   => \@spreadsheets,
	    FILES_LOADED    => \@data_files,
	},
    );

    return $template->output();
}

sub detect_platform {

    # Return a string that fileparse_set_fstype can understand. Note
    # that only MSWin32 and MacOS are really understood, and the rest
    # are assumed to be flavours of Unix.
    my $self = shift;

    # Attempt to detect non-unix platforms. Windows is the main one at
    # the moment - MacOSX conforms to unix path structures.
    my $ua = $self->query()->user_agent();
    my $browser = HTTP::BrowserDetect->new($ua);

    if ( $browser->windows() ) {
	return 'MSWin32';
    }
    elsif ( $browser->mac() ) {
	if ( $browser->macosx() ) {
	    return 'MacOSX';
	}
	else {
	    return 'MacOS';
	}
    }
    else {

	# Standardise on a default. I happen to be partial to linux...
	return 'linux';
    }

}

sub file_upload {

    my ($self, $errors) = @_;
    $self->authen->is_authenticated() or return $self->login();

    # Set the platform before attempting validation.
    fileparse_set_fstype( $self->detect_platform() );

    # Validate the input.
    my ($results, $err_page) =
	$self->check_rm('upload', '_upload_profile');
    return $err_page if $err_page;

    # Get CGI query object
    my $q = $self->query();

    # Get the experiment, checking that the user is the owner.
    my ($experiment, $illegal_access) = $self->get_secure_experiment(
	$q->param('experiment'),
    );
    return $illegal_access unless $experiment;

    # Uploads are particularly fraught.
    if ( my $error = $q->cgi_error() ) {
	return $self->error('CGI error triggered: ', $error);
    }

    # Create the submissions directory.
    my $submissions_dir = $experiment->filesystem_directory();

    # Upload the files. Spreadsheet first:
    if ( my $upload_path = $q->param('spreadsheet') ){

	# Trim off any directories (Win, Mac or Unix).
	my $upload_file = untaint(basename( $upload_path ));

	# Input file.
	my $upload    = CGI::Upload->new;
	my $upload_fh = $upload->file_handle('spreadsheet');
	$upload_fh->binmode();

	unless ($upload->mime_type('spreadsheet') eq 'text/plain') {
	    return $self->upload({
		'err_spreadsheet' =>
		    sprintf (
			$ERROR_FORMAT,
			  q{Error: the spreadsheet file must be in}
			. q{ plain ASCII tab-delimited text format.},
		    ),
		},
	    );
	}

	# Output file.
	my $spreadsheet = File::Spec->catfile($submissions_dir, $upload_file);
	my $out_fh      = IO::File->new($spreadsheet, '>')
	    or die("Error: unable to upload file: $!");
	$out_fh->binmode();

	# Backup spreadsheet.
	my $backup = File::Spec->catfile(
	    $submissions_dir,
	    "$upload_file.backup." . untaint(date_now())
	);
	my $backup_fh      = IO::File->new($backup, '>')
	    or die("Error: unable to upload file: $!");
	$backup_fh->binmode();

	# Copy the file somewhere useful.
	my ($buffer, $bytes_read);
	while (my $bytes = read($upload_fh, $buffer, 1024)){
	    $bytes_read += $bytes;
	    print $out_fh $buffer
		or die("Error writing spreadsheet file to filesystem");

	    # Do we really need to croak on error here FIXME?
	    print $backup_fh $buffer
		or die("Error writing backup spreadsheet file to filesystem");
	}

	# We don't set permissions on the backup file as curators
	# should not need to change it. Note that setting permissions
	# fails if a curator owns the spreadsheet, so we can't afford
	# to croak indiscriminately. In such cases it's up to the
	# curator to fix the problem, we only handle the files we own.
	if ( -o $spreadsheet ) {
	    chmod(oct($CONFIG->get_FILE_PERMISSIONS()), $spreadsheet)
		or croak("Error changing permissions on $spreadsheet: $!");
	}

	# Save to database. Note that we leave any previous upload in
	# place; we may want to revisit that at some point FIXME.
	my $record = ArrayExpress::AutoSubmission::DB::Spreadsheet->find_or_create(
	    experiment_id => $experiment->id(),
	    is_deleted    => 0,
	);
	$record->set(name => $upload_file);
	$record->update();
	$experiment->set(
	    date_last_edited => date_now(),
	);
	$experiment->update();
    }

    # Now upload the data file:
    if ( my $upload_path = $q->param('data_file') ){

	# Trim off any directories (Win, Mac or Unix).
	my $upload_file = untaint(basename( $upload_path ));

	# Input file.
	my $upload      = CGI::Upload->new;
	my $upload_fh   = $upload->file_handle('data_file');
	$upload_fh->binmode();

	# Output file.
	my $data_file   = File::Spec->catfile($submissions_dir, $upload_file);
	my $out_fh      = IO::File->new($data_file, '>')
	    or die("Error: unable to upload file: $!");
	$out_fh->binmode();

	# Copy the file somewhere useful.
	my ($buffer, $bytes_read);
	while (my $bytes = read($upload_fh, $buffer, 1024)){
	    $bytes_read += $bytes;
	    print $out_fh $buffer
		or die("Error writing data archive file to filesystem");
	}

	if ( -o $data_file ) {
	    chmod(oct($CONFIG->get_FILE_PERMISSIONS()), $data_file)
		or croak("Error changing permissions on $data_file: $!");
	}

	# Save to database, overwriting any uploaded files with the
	# same name.
	my $record = ArrayExpress::AutoSubmission::DB::DataFile->find_or_create(
	    name          => $upload_file,
	    experiment_id => $experiment->id(),
	    is_deleted    => 0,
	);
	$record->set(is_unpacked => 0);
	$record->update();
	$experiment->set(
	    date_last_edited => date_now(),
	);
	$experiment->update();
    }

    my @spreadsheets = map {
	{ name      => $_->name(), }
    } $experiment->spreadsheets(is_deleted => 0);

    my @data_files = map {
	{ name      => $_->name(), }
    } $experiment->data_files(is_deleted => 0);
    
    # Build the page and return it.
    my $template = $self->_load_my_tmpl(
	$self->param('file_upload_html_template') || 'file_upload.html',
	$errors,
	{
	    EXPT_NAME       => $experiment->name(),
	    EXPT_ID         => $experiment->id(),
	    TITLE           => 'Uploads complete',
	    UPLOAD          => $RM_UPLOAD,
	    SUBMIT          => $RM_SUBMIT,
	    CHOOSE          => $RM_CHOOSE,
	    SPREADSHEET     => 'spreadsheet',
	    DATA_FILE       => 'data_file',
	    SHEETS_LOADED   => \@spreadsheets,
	    FILES_LOADED    => \@data_files,
	    SHEETS_OK       => scalar @spreadsheets || undef,
	    FILES_OK        => scalar @data_files || undef,
	},
    );

    return $template->output();
}

sub submit {
    my ($self, $errors) = @_;
    $self->authen->is_authenticated() or return $self->login();

    # Validate the input.
    my ($results, $err_page) =
	$self->check_rm('upload', '_submit_profile');
    return $err_page if $err_page;

    # Get CGI query object
    my $q = $self->query();

    # Get the experiment, checking that the user is the owner.
    my ($experiment, $illegal_access) = $self->get_secure_experiment(
	$q->param('experiment'),
    );
    return $illegal_access unless $experiment;

    # in_curation determines web interface visibility; status is used
    # only by the back-end processing.
    $experiment->set(
	in_curation    => 1,
	status         => $CONFIG->get_STATUS_PENDING(),
	date_submitted => date_now(),
    );
    $experiment->update();

    # Email the user to confirm submission (but not if it's from the
    # test user).
    unless ($experiment->user_id()->login() eq 'test') {
	$self->email_confirm_submission($experiment);
    }

    # Increment the number of submissions (this must happen after the
    # confirmation emails are sent).
    $experiment->set(
	num_submissions => ( $experiment->num_submissions() + 1 ),
    );
    $experiment->update();
    
    # Build the page and return it.
    my $template = $self->_load_my_tmpl(
	$self->param('submitted_html_template') || 'submitted.html',
	$errors,
	{
	    EXPT_NAME       => $experiment->name(),
	    EXPT_ID         => $experiment->id(),
	    TITLE           => 'Experiment submitted',
	    CHOOSE          => $RM_CHOOSE,
	},
    );

    return $template->output();
}

sub error {

    # Generate a generic error page that doesn't look too bad.
    my ( $self, @messages ) = @_;

    my $template = $self->_load_my_tmpl(
	'error.html',
	undef,		
	{
	    MESSAGE => (join(q{}, @messages)
			    || 'Unknown application crash.'),
	    RETURN  => $self->authen->is_authenticated()
		       ? $RM_CHOOSE
		       : $RM_INTRO,
	},
    );

    return $template->output();
}

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: WebForm.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::AutoSubmission::WebForm - A CGI::Application web form
for Tab2MAGE/MAGE-TAB data submissions.

=head1 SYNOPSIS

 use ArrayExpress::AutoSubmission::WebForm;
 my $webapp = ArrayExpress::AutoSubmission::WebForm->new(
     PARAMS => {
	experiment_type  => 'MAGE-TAB',
	spreadsheet_type => 'MAGE-TAB',
	cgi_base         => '/cgi-bin/microarray/magetab.cgi',
	stylesheet       => '/microarray/tab2mage.css',
	intro_html_template    => 'magetab_intro.html',
	upload_html_template   => 'magetab_upload.html',
	template_html_template => 'magetab_template.html',
	sidebar_icons   => [
	    {
		image       => '/microarray/aelogo.png',
		destination => 'http://www.ebi.ac.uk/arrayexpress/',
		alt         => 'ArrayExpress',
	    },
	],
	sidebar_links   => [
	    {
		text        => 'Microarray group',
		destination => 'http://www.ebi.ac.uk/microarray/',
	    },
	],
    }
 );

 # Start the CGI script.
 $webapp->run();

=head1 DESCRIPTION

This module is the core of the web application used in the Tab2MAGE
and MAGE-TAB data submission web forms. Objects are instantiated with
a PARAMS attribute which can be used to customise various aspects of
the web form behaviour.

=head1 METHODS

=over 2

=item C<new>

Object constructor. See the CGI::Application documentation for details.

=item C<run>

The method used to start the web application.

=back

=head1 TODO

Documentation of the options which can be customised using the
PARAMS attribute.

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2008.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

1;
