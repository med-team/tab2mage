#!/usr/bin/env perl
#
# Module used to generate spreadsheet templates for Tab2MAGE submissions
#
# Tim Rayner 2006, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Spreadsheet.pm 2069 2008-06-04 14:33:52Z tfrayner $
#

package ArrayExpress::AutoSubmission::Spreadsheet;

use strict;
use warnings;

use Class::Std;
use Carp;
use Readonly;
use ArrayExpress::Curator::Common qw(decamelize);
require ArrayExpress::AutoSubmission::DB::Category;
use ArrayExpress::Curator::MAGE::Definitions qw(
  $EDF_EXPTACCESSION
  $EDF_EXPTDOMAIN
  $EDF_EXPTNAME
  $EDF_EXPTDESCRIPTION
  $EDF_EXPTRELEASEDATE
  $EDF_EXPTSUBMITTER
  $EDF_EXPTDATACODER
  $EDF_EXPTCURATOR
  $EDF_EXPTINVESTIGATOR
  $EDF_EXPTORGANIZATION
  $EDF_EXPTADDRESS
  $EDF_EXPTSUBMITTER_EMAIL
  $EDF_EXPTCURATOR_EMAIL
  $EDF_EXPTINVESTIGATOR_EMAIL
  $EDF_EXPTDATACODER_EMAIL
  $EDF_EXPTSUBMISSIONDATE
  $EDF_EXPTDESIGNTYPE
  $EDF_EXPTQUALITYCONTROL
  $EDF_EXPTCURATEDNAME

  $EDF_PUBTITLE
  $EDF_PUBPAGES
  $EDF_PUBJOURNAL
  $EDF_PUBVOLUME
  $EDF_PUBISSUE
  $EDF_PUBYEAR
  $EDF_PUBAUTHORS
  $EDF_PUBURI
  $EDF_PUBMEDID

  $EDF_PROTOCOLACCESSION
  $EDF_PROTOCOLNAME
  $EDF_PROTOCOLTYPE
  $EDF_PROTOCOLTEXT
  $EDF_PROTOCOLPARAMS

  $OE_VAL_CHIP_CHIP
);

my %experiment : ATTR( :init_arg<experiment>, :get<experiment>, :default<undef> );
my %url        : ATTR( :init_arg<url>,        :get<url>,        :default<undef> );
my %date       : ATTR( :init_arg<date>,       :get<date>,       :default<undef> );

# 1 = all spreadsheets; 2 = dye-swap only; 4 = Affy only, 8 = ChIP
Readonly my $ALL      => 1;
Readonly my $DYESWAP  => 2;
Readonly my $AFFY     => 4;
Readonly my $CHIPCHIP => 8;

Readonly my $MGED_ONTOLOGY => 'MGED Ontology';

sub START {

    my ( $self, $id, $args ) = @_;

    my $experiment = $self->get_experiment();
    unless (   $experiment
	    && $experiment->isa('ArrayExpress::AutoSubmission::DB::Experiment') ) {
	croak('Error: experiment attribute not set to an'
	    . ' ArrayExpress::AutoSubmission::DB::Experiment object.');
    }

    unless ( $self->get_url() ) {
	carp('Warning: URL attribute not set.');
    }
    unless ( $self->get_date() ) {
	carp('Warning: Date attribute not set.');
    }

    return;
}

sub _tab2mage_expt_section : PRIVATE {

    my ( $self ) = @_;

    my $experiment = $self->get_experiment();

    my $experiment_name = $experiment->name();
    my $experiment_type = $experiment->experiment_type();
    my $submitter       = $experiment->user_id()->name();
    my $submitter_email = $experiment->user_id()->email();

    my $url  = $self->get_url()  || q{};
    my $date = $self->get_date() || q{};
    
    my $design_string =
      join( ', ', map { $_->ontology_value() } $experiment->designs() );

    my $template = <<"EXPTSECTION";
#
# $experiment_type Submission '$experiment_name', generated at $date
#
# Please save this page to disk, and edit it using any spreadsheet program (e.g.,
# Microsoft Excel, OpenOffice.org Calc). This file can be imported into either of
# these applications as a tab-delimited text file. It is highly recommended that
# you set the cell format to plain text throughout the spreadsheet, to avoid
# 'helpful' changes made by the spreadsheet application. Once you have completed
# this spreadsheet please upload it using the submissions webpage alongside your
# data files:
#    $url
#
# Please see http://tab2mage.sourceforge.net/docs/spreadsheet.html for
# information on filling in the spreadsheet.
#
# All lines beginning with the '#' character are treated as comments and ignored.
# Blank lines may only be used between Experiment, Protocol and Hybridization sections.
#
#
#
# This section contains the top-level information for your experiment.
Experiment section
$EDF_EXPTNAME\t$experiment_name
$EDF_EXPTDESCRIPTION\t
$EDF_EXPTDESIGNTYPE\t$design_string
# $EDF_EXPTQUALITYCONTROL examples: dye_swap_quality_control, biological_replicate, technical_replicate
$EDF_EXPTQUALITYCONTROL\t
# Dates should be entered in the form YYYY-MM-DD. If you are using MS Excel,
# it is recommended that you set your spreadsheet to 'text' format throughout
# to help avoid any unwanted changes made by Excel.
$EDF_EXPTRELEASEDATE\t
$EDF_EXPTSUBMISSIONDATE\t
$EDF_EXPTSUBMITTER\t$submitter
$EDF_EXPTORGANIZATION\t
$EDF_EXPTADDRESS\t
$EDF_EXPTSUBMITTER_EMAIL\t$submitter_email
$EDF_PUBTITLE\t
$EDF_PUBPAGES\t
$EDF_PUBJOURNAL\t
$EDF_PUBVOLUME\t
$EDF_PUBISSUE\t
$EDF_PUBYEAR\t
$EDF_PUBAUTHORS\t
$EDF_PUBMEDID\t
EXPTSECTION

    return $template;
}

sub _tab2mage_prot_section : PRIVATE {

    my ( $self ) = @_;
    
    my $experiment    = $self->get_experiment();
    my $is_affymetrix = $experiment->is_affymetrix();

    my $template = <<"PROTHEADER";

# The next section describes the protocols used in your experiment. Please select
# your own accession codes (e.g. 'GROWTH', 'TREATMENT') for use within this spreadsheet,
# or re-use ArrayExpress protocols by using the assigned ArrayExpress protocol
# accession numbers. If you have already submitted protocols to ArrayExpress,
# you can retrieve these using this web page:
#    http://www.ebi.ac.uk/aerep/
#
Protocol section
$EDF_PROTOCOLACCESSION\t$EDF_PROTOCOLNAME\t$EDF_PROTOCOLTEXT
GROWTH\tGrowth\t<your growth protocol text here>
TREATMENT\tTreatment\t<your treatment protocol text here>
EXTRACTION\tExtraction\t<your nucleic acid extraction protocol text here>
LABELING\tLabeling\t<your labeling protocol text here>
PROTHEADER

    if ( $is_affymetrix ) {
        $template .= <<"PROTAFFY";
# For Affymetrix experiments you do not need to provide hybridization or scanning
# protocols. If you are supplying CHP files, you do not need to enter a normalization
# protocol.
NORMALIZATION\tNormalization\t<your normalization protocol text here>
PROTAFFY
    }
    else {
        $template .= <<"PROTDYESWAP";
HYBRIDIZATION\tHybridization\t<your hybridization protocol text here>
SCANNING\tScanning\t<your scanning protocol text here>
NORMALIZATION\tNormalization\t<your normalization protocol text here>
PROTDYESWAP
    }
    $template .= <<"PROTEND";
TRANSFORMATION\tTransformation\t<your final data transformation protocol text here>
# Add any other protocols here...
PROTEND

    return $template;
}

sub _retrieve_factorvalues : PRIVATE {

    my ( $self ) = @_;

    # Retrieve the factor values for the experiment. FIXME consider
    # caching these?

    my $experiment = $self->get_experiment();

    # FactorValues. The factors are derived only from the design.  We
    # go round the houses here because Class::DBI many-to-many sucks.
    my @fv_categories;
    my @design_categories = map { $_->categories() } $experiment->designs();
    foreach my $cat ( @design_categories ) {
	push(@fv_categories, $cat) if $cat->is_fv();
    }
    my %factorvalue = map { $_ => 1 }
          map { $_->ontology_term() } @fv_categories;

    return [ keys %factorvalue ];
}

sub _retrieve_bmcs : PRIVATE {

    my ( $self ) = @_;

    # Retrieve the BioMaterialCharacteristics for the
    # experiment. FIXME consider caching these?

    my $experiment = $self->get_experiment();

    # BioMaterialCharacteristics: Designs.
    my @all_categories;
    push( @all_categories,
          map { $_->categories() } $experiment->designs() );

    # BioMaterialCharacteristics: Materials.
    push( @all_categories,
          map { $_->categories() } $experiment->materials() );

    # BioMaterialCharacteristics: Organisms.
    push( @all_categories,
          map { $_->categories() }
          map { $_->taxon_id() } $experiment->organisms() );

    # Filter out the non-BMC categories.
    my @bmc_categories;
    foreach my $cat ( @all_categories ) {
	push(@bmc_categories, $cat) if $cat->is_bmc();
    }

    # BioMaterialCharacteristics: Common categories
    push(
	@bmc_categories,
	    ArrayExpress::AutoSubmission::DB::Category->search(
		is_common => 1,
	    )
	);

    # Unique the BMC categories.
    my %bmc =
	map { $_->ontology_term() => 1 } @bmc_categories;

    return [ keys %bmc ];
}

sub _get_tab2mage_hyb_terms : PRIVATE {

    my ( $self ) = @_;

    my $experiment    = $self->get_experiment();
    my $is_affymetrix = $experiment->is_affymetrix();

    my $factorvalues    = $self->_retrieve_factorvalues();
    my $characteristics = $self->_retrieve_bmcs();

    # Custom help and example texts for BioMaterialCharacteristics.
    my %bmc_help = (
	'Organism' => 'Sample latin species name',
	'Age'      => 'Sample Age value, e.g. 20 years',
    );
    my $example_organism = ($experiment->organisms())[0];
    my $example_material = ($experiment->materials())[0];
    my %bmc_example = (
	'Organism' => $example_organism ? $example_organism->scientific_name() : q{},
    );

    # Here we define the basic structure of the template.
    my @terms = (
        { 'BioSource'         => {
	    usage   => $ALL,
	    help    => 'Unique IDs for biosources (experimental starting materials)',
	    example => 'Cell culture 1',
	} },
        { 'BioSourceMaterial' => {
	    usage   => $ALL,
	    help    => 'MaterialType term from MGED ontology',
	    example => $example_material ? $example_material->ontology_value() : q{},
	} },
        (
            map {
		my $charcol = {
		    "BioMaterialCharacteristics[$_]" =>
			{
			    usage   => $ALL,
			    help    => $bmc_help{$_}    || "Sample $_ value",
			    example => $bmc_example{$_} || q{},
			}
		    };

		# Some Characteristics are measurements, not OE values. Return
		# value determined here:
		($_ =~ m/\A Time|Age \z/ixms) ? ( $charcol,
			{
			    'BioMaterialCharacteristics[TimeUnit]' => {
				usage   => $ALL,
				help    => 'Time units for Characteristic',
				example => 'hours',
			    }
			}
		    )
	      : ($_ =~ m/\A Dose \z/ixms) ? ( $charcol,
		      {
			  'BioMaterialCharacteristics[ConcentrationUnit]' => {
			      usage   => $ALL,
			      help    => 'Concentration units for Characteristic',
			      example => 'mg_per_ml',
			  }
		      }
		  )
	       : $charcol;
	    } @$characteristics
        ),
        { 'Protocol[grow]'              => {
	    usage   => $ALL,
	    help    => 'Growth protocol, where applicable',
	    example => 'GROWTH',
	} },
        { 'Protocol[treatment]'         => {
	    usage   => $ALL,
	    help    => 'Sample treatment protocol',
	    example => 'TREATMENT',
	} },
        { 'Sample'                      => {
	    usage   => $DYESWAP,
	    help    => 'Unique IDs for samples (after growth and treatment)',
	    example => 'Cell pellet 1',
	} },
        { 'Protocol[extraction]'        => {
	    usage   => $ALL,
	    help    => 'RNA/DNA extraction',
	    example => 'EXTRACTION',
	} },
        { 'Extract'                     => {
	    usage   => $DYESWAP,
	    help    => 'Unique IDs for extracts (RNA or DNA)',
	    example => 'RNA Extract 1',
	} },
        { 'Protocol[immunoprecipitate]' => {
	    usage   => $CHIPCHIP,
	    help    => 'ChIP protocol',
	} },
        { 'Immunoprecipitate'           => {
	    usage   => $CHIPCHIP,
	    help    => 'Unique IDs for IPs',
	    example => 'Immunoprecipitate 1'
	} },
        { 'Protocol[labeling]'          => {
	    usage   => $ALL,
	    help    => 'RNA/DNA labeling',
	    example => 'LABELING',
	} },
        { 'LabeledExtract'              => {
	    usage   => $DYESWAP,
	    help    => 'Unique IDs for labeled extracts',
	    example => 'Labeled Extract 1.' . $is_affymetrix ? 'biotin' : 'Cy3',
	} },
        { 'Dye'                         => {
	    usage   => $DYESWAP,
	    help    => 'Labeling dye, e.g. Cy3, biotin, 33P',
	    example => $is_affymetrix ? 'biotin' : 'Cy3',
	} },
        { 'Protocol[hybridization]'     => {
	    usage   => $DYESWAP,
	    help    => 'Hyb protocol',
	    example => 'HYBRIDIZATION',
	} },
        { 'Hybridization'               => {
	    usage   => $ALL,
	    help    => 'Unique IDs for hybs',
	    example => 'Hybridization 1',
	} },
        (
            map {
		# Some FVs are measurements, not OE values:
		{ ( ($_ =~ m/\A Time|Age|Dose \z/ixms)
		      ? "FactorValue[$_(units)]" : "FactorValue[$_]") =>
		      {
			  usage   => $ALL,
			  help    => $bmc_help{$_}    || "Value for $_ variable",
			  example => $bmc_example{$_} || q{},
		      }
		  }
	    }
		@$factorvalues
        ),
        { 'Protocol[scanning]'       => {
	    usage   => $DYESWAP,
	    help    => 'Image acquisition',
	    example => 'SCANNING',
	} },
        { 'Protocol[normalization]'  => {
	    usage   => $DYESWAP,
	    help    => 'Data normalization',
	    example => 'NORMALIZATION',
	} },
        { 'Array[accession]'         => {
	    usage   => $ALL,
	    help    => 'ArrayExpress array accession, e.g. A-MEXP-27',
	} },
        { 'Array[serial]'            => {
	    usage   => $ALL,
	    help    => 'Array serial number/chip lot',
	} },
        { 'File[raw]'                => {
	    usage   => $ALL,
	    help    => 'Unprocessed data file, e.g. CEL, GPR',
	} },
        { 'File[normalized]'         => {
	    usage   => $ALL,
	    help    => 'Normalized data',
	} },
        { 'File[exp]'                => {
	    usage   => $AFFY,
	    help    => 'EXP file where available',
	} },
        { 'File[cdf]'                => {
	    usage   => $AFFY,
	    help    => 'CDF library file (do not upload CDF unless custom array used)',
	} },
        { 'Protocol[transformation]' => {
	    usage   => $ALL,
	    help    => 'Protocol producing final data matrix file',
	    example => 'TRANSFORMATION',
	} },
        { 'File[transformed]'        => {
	    usage   => $ALL,
	    help    => 'Final data matrix file(s)',
	} },

    );

    return \@terms;
}

sub _table_from_terms : PRIVATE {

    my ( $self, $terms ) = @_;

    my $experiment    = $self->get_experiment();
    my $is_affymetrix = $experiment->is_affymetrix();

    my %designs_include =
      map { $_->ontology_value() => 1 } $experiment->designs();
    my (@headings, @explanations, @examples);

    foreach my $term ( @{ $terms } ) {
        while ( my ( $name, $termhash ) = each %$term ) {
            if (
                   ( $termhash->{'usage'} & $ALL )
                || ( $termhash->{'usage'} & $DYESWAP && !$is_affymetrix )
                || ( $termhash->{'usage'} & $AFFY    && $is_affymetrix )
                || (   $termhash->{'usage'} & $CHIPCHIP
                    && $designs_include{$OE_VAL_CHIP_CHIP} )
              )
            {
                push( @headings,     $name );
                push( @explanations, "# $termhash->{'help'}" || q{} );
                push( @examples,     $termhash->{'example'}  || q{} );
            }
        }
    }

    my $template = join( "\t", @headings )     . "\n"
	         . join( "\t", @explanations ) . "\n"
		 . join( "\t", @examples )     . "\n";

    return $template;
}

sub _tab2mage_hyb_section : PRIVATE {

    my ( $self ) = @_;

    my $template = <<"HYBHEADER";

# The Hybridization section contains all the information linking your samples to your
# data files. Please feel free to add as many new BioMaterialCharacteristics[] or
# FactorValue[] columns as necessary to completely describe your experiment. See this
# web page for help with supported data file formats:
#   http://tab2mage.sourceforge.net/docs/datafiles.html
#
# The first line below has been filled in with example terms, where possible:
Hybridization section
HYBHEADER

    # Get the tab2mage-specific hyb section template data.
    my $terms = $self->_get_tab2mage_hyb_terms();

    # Generate the table.
    $template .= $self->_table_from_terms( $terms );

    return $template;
}

sub tab2mage {

    my ( $self ) = @_;

    # Build the spreadsheet.
    # Also incorporate simplified Affy form.
    # FIXME to use the terms from MAGE::Definitions.
    my $template = $self->_tab2mage_expt_section()
  	         . $self->_tab2mage_prot_section()
	         . $self->_tab2mage_hyb_section();

    # Return the spreadsheet as a string.
    return $template;
}

sub _stylize_factor_name : PRIVATE {

    my ( $self, $category ) = @_;

    return uc($category);
}

sub _magetab_idf_factorvalue : PRIVATE {

    my ( $self ) = @_;
    
    my $factorvalues    = $self->_retrieve_factorvalues();

    my ( @factornames, @factortypes, @factortermsources );

    # Note that the factorname convention below must be matched in the SDRF.
    foreach my $category ( @$factorvalues ) {
	push @factornames, $self->_stylize_factor_name($category);

	# Switch from CamelCase category to underscore_word value
	push @factortypes, decamelize($category);
	push @factortermsources, $MGED_ONTOLOGY;
    }
    my $template = join("\t", 'Experimental Factor Name', @factornames) . "\n"
	         . join("\t", 'Experimental Factor Type', @factortypes) . "\n";

    return $template;
}

sub _magetab_idf_protocols : PRIVATE {

    my ( $self ) = @_;

    my $experiment = $self->get_experiment();
    my $is_affymetrix = $experiment->is_affymetrix();

    my @protocolnames = qw(
			   GROWTH
			   TREATMENT
			   EXTRACTION
			   LABELING
		       );
    my @protocoltypes = qw(
			   grow
			   specified_biomaterial_action
			   nucleic_acid_extraction
			   labeling
		       );
    my @protocoldescs = (
	q{<your growth protocol text here>},
	q{<your treatment protocol text here>},
	q{<your nucleic acid extraction protocol text here>},
	q{<your labeling protocol text here>},
    );
    my @protocoltermsources = (
	$MGED_ONTOLOGY,
	$MGED_ONTOLOGY,
	$MGED_ONTOLOGY,
	$MGED_ONTOLOGY,
    );

    # These will usually be automatically derived from CEL and EXP for
    # affy subs:

    unless ($is_affymetrix) {
	push @protocolnames, qw(HYBRIDIZATION SCANNING);
	push @protocoltypes, qw(hybridization image_acquisition);
	push @protocoldescs, (
	    q{<your hybridization protocol text here>},
	    q{<your scanning protocol text here>},
	);
	push @protocoltermsources, (
	    $MGED_ONTOLOGY,
	    $MGED_ONTOLOGY,
	);
    }

    push @protocolnames, qw(NORMALIZATION TRANSFORMATION);
    push @protocoltypes, qw(bioassay_data_transformation bioassay_data_transformation);
    push @protocoldescs, (
	q{<your normalization protocol text here>},
	q{<your transformation protocol text here>},
    );
    push @protocoltermsources, (
	q{},
	q{},
    );

    my $template = join("\t", 'Protocol Name',            @protocolnames      ) . "\n"
	         . join("\t", 'Protocol Type',            @protocoltypes      ) . "\n"
	         . join("\t", 'Protocol Description',     @protocoldescs      ) . "\n";

    return $template;
}

sub _magetab_idf : PRIVATE {

    my ( $self ) = @_;

    my $experiment = $self->get_experiment();

    my $experiment_name = $experiment->name();
    my $experiment_type = $experiment->experiment_type();
    my $submitter       = $experiment->user_id()->name();
    my $submitter_email = $experiment->user_id()->email();

    my $url  = $self->get_url()  || q{};
    my $date = $self->get_date() || q{};
    
    my $design_string =
      join( "\t", map { $_->ontology_value() } $experiment->designs() );

    my $template = <<"EXPTSECTIONSTART";
#
# $experiment_type Submission '$experiment_name', generated at $date
#
# Please save this page to disk, and edit it using any spreadsheet program (e.g.,
# Microsoft Excel, OpenOffice.org Calc). This file can be imported into either of
# these applications as a tab-delimited text file. It is highly recommended that
# you set the cell format to plain text throughout the spreadsheet, to avoid
# 'helpful' changes made by the spreadsheet application. Once you have completed
# this spreadsheet please upload it using the submissions webpage alongside your
# data files:
#    $url
#
# Please see http://tab2mage.sourceforge.net/docs/magetab_subs.html for
# information on filling in the IDF and SDRF sections below.
#
# All lines beginning with the '#' character are treated as comments and ignored.


# This section contains the top-level information for your experiment.
Investigation Title\t$experiment_name
Experiment Description\t
Experimental Design\t$design_string

# Please create as many Experimental Factors here as you need to
# describe the variables investigated by your experiment.
EXPTSECTIONSTART

    $template .= $self->_magetab_idf_factorvalue();

    $template .= <<"EXPTSECTIONMID";

# Quality Control Type examples: dye_swap_quality_control, biological_replicate, technical_replicate
Quality Control Type\t

# Dates should be entered in the form YYYY-MM-DD. If you are using MS Excel,
# it is recommended that you set your spreadsheet to 'text' format throughout
# to help avoid any unwanted changes made by Excel.
Public Release Date\t

# Please list contact details in columns, below:
Person Last Name\t$submitter\t
Person First Name\t\t
Person Mid Initials\t\t
Person Email\t$submitter_email\t
Person Phone\t\t
Person Address\t\t
Person Affiliation\t\t
Person Roles\tsubmitter\t

# Please list all publications associated with this submission, in columns:
PubMed ID\t
Publication Author List\t
Publication Title\t
Publication Status\t

# The next section describes the protocols used in your
# experiment. Please select your own Protocol Names (e.g. 'GROWTH',
# 'TREATMENT') for use within this spreadsheet, or re-use ArrayExpress
# protocols by using the assigned ArrayExpress protocol accession
# numbers in your SDRF. If you have already submitted protocols to
# ArrayExpress, you can retrieve these using this web page:
#    http://www.ebi.ac.uk/aerep/
EXPTSECTIONMID

    $template .= $self->_magetab_idf_protocols();

    return $template;
}

sub _magetab_sdrf : PRIVATE {

    my ( $self ) = @_;

    my $experiment = $self->get_experiment();

    my $template = <<"HYBHEADER";
# The SDRF section contains all the information linking your samples
# to your data files. Please feel free to add as many new
# Characteristics[] or FactorValue[] columns as necessary to
# completely describe your experiment. See this web page for help with
# supported data file formats:
#   http://tab2mage.sourceforge.net/docs/datafiles.html
#
# The first line below has been filled in with example terms, where possible:
HYBHEADER

    # Get the magetab-specific hyb section template data.
    my $terms = $self->_get_magetab_hyb_terms();

    # Generate the table.
    $template .= $self->_table_from_terms( $terms );

    return $template;
}

sub _get_magetab_hyb_terms : PRIVATE {

    my ( $self ) = @_;

    my $experiment    = $self->get_experiment();
    my $is_affymetrix = $experiment->is_affymetrix();

    my $factorvalues    = $self->_retrieve_factorvalues();
    my $characteristics = $self->_retrieve_bmcs();

    # Custom help and example texts for BioMaterialCharacteristics.
    my %bmc_help = (
	'Organism' => 'Sample latin species name',
	'Age'      => 'Sample Age value, e.g. 20 years',
    );
    my $example_organism = ($experiment->organisms())[0];
    my $example_material = ($experiment->materials())[0];
    my %bmc_example = (
	'Organism' => $example_organism ? $example_organism->scientific_name() : q{},
    );

    # Here we define the basic structure of the template.
    my @terms = (
        { 'Source Name' => {
	    usage   => $ALL,
	    help    => 'Unique IDs for biosources (experimental starting materials)',
	    example => 'Cell culture 1',
	} },
        { 'Material Type' => {
	    usage   => $ALL,
	    help    => 'MaterialType term from MGED ontology',
	    example => $example_material ? $example_material->ontology_value() : q{},
	} },
        (
            map {
		my $charcol = {
		    "Characteristics[$_]" =>
			{
			    usage   => $ALL,
			    help    => $bmc_help{$_}    || "Sample $_ value",
			    example => $bmc_example{$_} || q{},
			}
		    };

		# Some Characteristics are measurements, not OE values. Return
		# value determined here:
		($_ =~ m/\A Time|Age \z/ixms) ? ( $charcol,
			{
			    'Unit[TimeUnit]' => {
				usage   => $ALL,
				help    => 'Time units for Characteristic',
				example => 'hours',
			    }
			}
		    )
	      : ($_ =~ m/\A Dose \z/ixms) ? ( $charcol,
		      {
			  'Unit[ConcentrationUnit]' => {
			      usage   => $ALL,
			      help    => 'Concentration units for Characteristic',
			      example => 'mg_per_ml',
			  }
		      }
		  )
	       : $charcol;
	    } @$characteristics
        ),
        { 'Protocol REF' => {
	    usage   => $ALL,
	    help    => 'Growth protocol, where applicable',
	    example => 'GROWTH',
	} },
        { 'Protocol REF' => {
	    usage   => $ALL,
	    help    => 'Sample treatment protocol',
	    example => 'TREATMENT',
	} },
        { 'Sample Name' => {
	    usage   => $DYESWAP,
	    help    => 'Unique IDs for samples (after growth and treatment)',
	    example => 'Cell pellet 1',
	} },
        { 'Protocol REF' => {
	    usage   => $ALL,
	    help    => 'RNA/DNA extraction',
	    example => 'EXTRACTION',
	} },
        { 'Extract Name' => {   # all dye-swap except ChIP-chip.
	    usage   => ($DYESWAP & ~ $CHIPCHIP),
	    help    => 'Unique IDs for extracts (RNA or DNA)',
	    example => 'RNA Extract 1',
	} },
        { 'Sample Name' => {
	    usage   => $CHIPCHIP,
	    help    => 'Unique IDs for extracts (RNA or DNA)',
	    example => 'RNA Extract 1',
	} },
        { 'Protocol REF' => {
	    usage   => $CHIPCHIP,
	    help    => 'ChIP protocol',
	} },
        { 'Extract Name' => {
	    usage   => $CHIPCHIP,
	    help    => 'Unique IDs for IPs',
	    example => 'Immunoprecipitate 1'
	} },
        { 'Protocol REF' => {
	    usage   => $ALL,
	    help    => 'RNA/DNA labeling',
	    example => 'LABELING',
	} },
        { 'Labeled Extract Name' => {
	    usage   => $ALL,
	    help    => 'Unique IDs for labeled extracts',
	    example => 'Labeled Extract 1.' . ($is_affymetrix ? 'biotin' : 'Cy3'),
	} },
        { 'Label' => {
	    usage   => $ALL,
	    help    => 'Labeling dye, e.g. Cy3, biotin, 33P',
	    example => ($is_affymetrix ? 'biotin' : 'Cy3'),
	} },
        { 'Protocol REF' => {
	    usage   => ($DYESWAP | $CHIPCHIP),
	    help    => 'Hyb protocol',
	    example => 'HYBRIDIZATION',
	} },
        { 'Hybridization Name' => {
	    usage   => $ALL,
	    help    => 'Unique IDs for hybs',
	    example => 'Hybridization 1',
	} },
        { 'Array Design REF' => {
	    usage   => $ALL,
	    help    => 'ArrayExpress array accession, e.g. A-MEXP-27',
	} },
        { 'Comment[Array serial number]' => {
	    usage   => $ALL,
	    help    => 'Array serial number/chip lot',
	} },
        { 'Protocol REF' => {
	    usage   => ($DYESWAP | $CHIPCHIP),
	    help    => 'Image acquisition',
	    example => 'SCANNING',
	} },
        { 'Scan Name' => {
	    usage   => ($DYESWAP | $CHIPCHIP),
	    help    => 'Unique IDs for scans',
	    example => 'Scan 1',
	} },
        { 'Array Data File' => {
	    usage   => $ALL,
	    help    => 'Unprocessed data file, e.g. CEL, GPR',
	} },
        { 'Comment[EXP]' => {
	    usage   => $AFFY,
	    help    => 'EXP file where available',
	} },
        { 'Protocol REF' => {
	    usage   => $ALL,
	    help    => 'Data normalization',
	    example => 'NORMALIZATION',
	} },
        { 'Derived Array Data File' => {
	    usage   => $ALL,
	    help    => 'Normalized data',
	} },
        { 'Protocol REF' => {
	    usage   => $ALL,
	    help    => 'Protocol producing final data matrix file',
	    example => 'TRANSFORMATION',
	} },
        { 'Derived Array Data Matrix File' => {
	    usage   => $ALL,
	    help    => 'Final data matrix file(s)',
	} },
        { 'Comment[CDF]' => {
	    usage   => $AFFY,
	    help    => 'CDF library file (do not upload CDF unless custom array used)',
	} },
        (
            map {

		# $fvname convention must match that in the IDF.
		my $fvname = $self->_stylize_factor_name($_);
		my $fvcol = {
		    "Factor Value[$fvname]" => {
			usage   => $ALL,
			help    => $bmc_help{$_}    || "Value for $fvname Experimental Factor",
			example => $bmc_example{$_} || q{},
		    }
		};

		# Some FVs are measurements, not OE values. Return
		# value determined here:
		($_ =~ m/\A Time|Age \z/ixms) ? ( $fvcol,
			{
			    'Unit[TimeUnit]' => {
				usage   => $ALL,
				help    => 'Time units for Factor Value',
				example => 'hours',
			    }
			}
		    )
	      : ($_ =~ m/\A Dose \z/ixms) ? ( $fvcol,
		      {
			  'Unit[ConcentrationUnit]' => {
			      usage   => $ALL,
			      help    => 'Concentration units for Factor Value',
			      example => 'mg_per_ml',
			  }
		      }
		  )
	       : $fvcol;
	    }
		@$factorvalues
        ),

    );

    return \@terms;
}

sub magetab {

    my ( $self ) = @_;

    my $template = "#####################################################\n[IDF]\n"
	         . $self->_magetab_idf()
		 . "\n#####################################################\n[SDRF]\n"
	         . $self->_magetab_sdrf();

    return $template;
}

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: Spreadsheet.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::AutoSubmission::Spreadsheet - A Tab2MAGE/MAGE-TAB
document templating class.

=head1 SYNOPSIS

 use ArrayExpress::AutoSubmission::Spreadsheet;
 my $ss = ArrayExpress::AutoSubmission::Spreadsheet->new({
     experiment => $expt,
     url        => $self->query()->url(),
     date       => date_now(),
 });

 $ss_content = $spreadsheet->magetab();

=head1 DESCRIPTION

This module provides Tab2MAGE and MAGE-TAB document templating
functions. The functions take Class::DBI experiment objects from the
tracking database and generate the appropriate output strings which
can then be printed to file.

=head1 METHODS

=over 2

=item C<new>

Object constructor, taking the following arguments:

=over 4

=item C<experiment>

The Class::DBI experiment object to use in template generation.

=item C<url>

The URL of the page to which completed templates should be submitted
(this is embedded in the template for convenience).

=item C<date>

The date of template generation.

=back

=item C<magetab>

Return a string representing the MAGE-TAB template for this experiment.

=item C<tab2mage>

Return a string representing the Tab2MAGE template for this experiment.

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2008.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

1;

