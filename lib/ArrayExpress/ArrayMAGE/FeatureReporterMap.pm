#!/usr/bin/env perl
#
# $Id: FeatureReporterMap.pm 1937 2008-02-10 13:15:30Z tfrayner $

use strict;
use warnings;

######################
# FeatureReporterMap #
######################
package ArrayExpress::ArrayMAGE::FeatureReporterMap;
use base 'ArrayExpress::ArrayMAGE';

sub START {

    my ( $self, $id, $args ) = @_;

    print { $self->get_fh() } <<"MAGE_OUT";
    <FeatureReporterMap_assnlist>
MAGE_OUT

    return;
}

sub add {

    my ( $self, $args ) = @_;

    my $fh = $self->get_fh();

    defined $args->{identifier} or die;
    defined $args->{features}   or die;

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();
    my $separator = $self->get_separator();

    print $fh <<"MAGE_OUT";
      <FeatureReporterMap identifier="$namespace:FeatureReporterMap:$design$separator$args->{identifier}">
        <Reporter_assnref>
          <Reporter_ref identifier="$namespace:Reporter:$design$separator$args->{identifier}"/>
        </Reporter_assnref>
        <FeatureInformationSources_assnlist>
MAGE_OUT

    foreach my $identifier ( @{ $args->{features} } ) {
        print $fh <<"MAGE_OUT";
          <FeatureInformation>
            <Feature_assnref>
              <Feature_ref identifier="$namespace:Feature:$design$separator$identifier"/>
            </Feature_assnref>
MAGE_OUT

        if ( $args->{mismatch_info} && $args->{mismatch_info}{$identifier} ) {
            my %info = %{ $args->{mismatch_info}{$identifier} };
            print $fh <<"MAGE_OUT";
            <MismatchInformation_assnlist>
              <MismatchInformation startCoord="$info{start_coord}"
                                   newSequence="$info{new_sequence}"
                                   replacedLength="$info{replaced_length}"/>
            </MismatchInformation_assnlist>
MAGE_OUT
        }
        print $fh <<"MAGE_OUT";
          </FeatureInformation>
MAGE_OUT
    }

    print $fh <<"MAGE_OUT";
        </FeatureInformationSources_assnlist>
      </FeatureReporterMap>
MAGE_OUT

    return;
}

sub end : RESTRICTED {

    my $self = shift;

    print { $self->get_fh() } <<"MAGE_OUT";
    </FeatureReporterMap_assnlist>
MAGE_OUT

    return;
}

1;
