#!/usr/bin/env perl
#
# $Id: CompositeSequence.pm 1937 2008-02-10 13:15:30Z tfrayner $

use strict;
use warnings;

#####################
# CompositeSequence #
#####################
package ArrayExpress::ArrayMAGE::CompositeSequence;
use base 'ArrayExpress::ArrayMAGE';

sub START {

    my ( $self, $id, $args ) = @_;

    print { $self->get_fh() } <<"MAGE_OUT";
    <CompositeSequence_assnlist>
MAGE_OUT

    return;
}

sub add {

    my ( $self, $args ) = @_;

    my $fh = $self->get_fh();

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();
    my $separator = $self->get_separator();

    defined $args->{identifier} or die;

    print $fh
        "      <CompositeSequence identifier=\"$namespace:CompositeSequence:$design$separator$args->{identifier}\"";
    $args->{name} && print $fh "\n                name=\"$args->{name}\"";
    print $fh ">\n";

    my $db_refs_provided =
        $args->{dbrefs}
        ? scalar( grep { defined $_ } values %{ $args->{dbrefs} } )
        : undef;

    ( $args->{comment} || $db_refs_provided ) && do {
        print $fh "        <Descriptions_assnlist>\n          <Description";
        $args->{comment} && print $fh " text=\"$args->{comment}\"";
        print $fh ">\n";
        $db_refs_provided && do {
            print $fh "            <DatabaseReferences_assnlist>\n";
            while ( my ( $acc, $db ) = each %{ $args->{dbrefs} } ) {
                print $fh <<"MAGE_OUT";
	      <DatabaseEntry accession="$acc">
		<Database_assnref>
		  <Database_ref identifier="ebi.ac.uk:Database:$db"/>
		</Database_assnref>
	      </DatabaseEntry>
MAGE_OUT
            }
            print $fh "            </DatabaseReferences_assnlist>\n";
        };
        print $fh <<"MAGE_OUT";
	  </Description>
	</Descriptions_assnlist>
MAGE_OUT
    };

    $args->{controltype} && print $fh <<"MAGE_OUT";
        <ControlType_assn>
          <OntologyEntry category="ControlType"
                         value="$args->{controltype}">
          </OntologyEntry>
        </ControlType_assn>
MAGE_OUT

    ( $args->{biosequences} && @{ $args->{biosequences} } ) && do {
        print $fh <<"MAGE_OUT";
        <BiologicalCharacteristics_assnreflist>
MAGE_OUT
        foreach my $bsid ( @{ $args->{biosequences} } ) {
            print $fh <<"MAGE_OUT";
          <BioSequence_ref identifier="$namespace:BioSequence:$design$separator$bsid"/>
MAGE_OUT
        }
        print $fh <<"MAGE_OUT";
        </BiologicalCharacteristics_assnreflist>
MAGE_OUT
    };

    # Skip RCM if we're only generating CSs:
    unless ( $args->{cs_only} ) {

	# assume this is 1:1 CompositeSequence:FRM for now (1:n FRM:Feature)
	print $fh <<"MAGE_OUT";
        <ReporterCompositeMaps_assnreflist>
          <ReporterCompositeMap_ref identifier="$namespace:ReporterCompositeMap:$design$separator$args->{identifier}"/>
        </ReporterCompositeMaps_assnreflist>
MAGE_OUT
    }

    print $fh "      </CompositeSequence>\n";

    return;
}

sub end : RESTRICTED {

    my $self = shift;

    print { $self->get_fh() } <<"MAGE_OUT";
    </CompositeSequence_assnlist>
MAGE_OUT

    return;
}

1;
