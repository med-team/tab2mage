#!/usr/bin/env perl
#
# $Id: Feature.pm 1937 2008-02-10 13:15:30Z tfrayner $

use strict;
use warnings;

###########
# Feature #
###########
package ArrayExpress::ArrayMAGE::Feature;
use base 'ArrayExpress::ArrayMAGE';

sub START {

    my ( $self, $id, $args ) = @_;

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();

    # Omitting TechnologyType OE here FIXME
    print { $self->get_fh() } <<"MAGE_OUT";
        <FeatureGroups_assnlist>
          <FeatureGroup identifier="$namespace:FeatureGroup:$design">
            <Features_assnlist>
MAGE_OUT

    return;
}

sub add {

    my ( $self, $args ) = @_;

    my $fh = $self->get_fh();

    defined( $args->{row} )        or die;
    defined( $args->{column} )     or die;
    defined( $args->{metarow} )    or die;
    defined( $args->{metacolumn} ) or die;

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();
    my $separator = $self->get_separator();

    my $identifier = "$namespace:Feature:$design$separator";

    $identifier .= $args->{identifier}
        || "$args->{metacolumn}.$args->{metarow}.$args->{column}.$args->{row}";

    print $fh <<"MAGE_OUT";
              <Feature identifier="$identifier">
                <Zone_assnref>
                  <Zone_ref identifier="$namespace:Zone:$design$separator$args->{metacolumn}.$args->{metarow}"/>
                </Zone_assnref>
                <FeatureLocation_assn>
                  <FeatureLocation column="$args->{column}"
                                   row="$args->{row}">
                  </FeatureLocation>
                </FeatureLocation_assn>
              </Feature>
MAGE_OUT

    return;
}

sub end : RESTRICTED {

    my $self = shift;

    print { $self->get_fh() } <<"MAGE_OUT";
            </Features_assnlist>
          </FeatureGroup>
        </FeatureGroups_assnlist>
MAGE_OUT

    return;
}

1;
