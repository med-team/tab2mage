#!/usr/bin/env perl
#
# $Id: ProgressBar.pm 1858 2007-12-17 12:47:08Z tfrayner $

use strict;
use warnings;

###############
# ProgressBar #
###############
# Progress bar code
# Credit to perlmonks user tmoertel (see PM node 396857)
# TGM 2004-10-05

package ArrayExpress::ArrayMAGE::ProgressBar;
use Class::Struct;
use List::Util qw( min );

struct( width => '$', portion => '$', max_val => '$' );

sub draw {

    my ( $self, $x, $max_val ) = @_;

    local $| = 1;

    $max_val ||= $self->max_val;
    my $old_portion = $self->portion || 0;
    my $new_portion = int( $self->width * $x / $max_val + 0.5 );
    print "\b" x ( 6 + $self->width - min( $new_portion, $old_portion ) )
        if defined $self->portion;
    print "*" x ( $new_portion - $old_portion ),
        " " x ( $self->width - $new_portion ), sprintf " %3d%% ",
        int( 100 * $x / $max_val + 0.5 );
    $self->portion($new_portion);

    return;
}

1;
