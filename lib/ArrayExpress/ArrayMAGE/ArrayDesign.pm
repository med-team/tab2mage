#!/usr/bin/env perl
#
# $Id: ArrayDesign.pm 1937 2008-02-10 13:15:30Z tfrayner $

use strict;
use warnings;

###############
# ArrayDesign #
###############
package ArrayExpress::ArrayMAGE::ArrayDesign;
use base 'ArrayExpress::ArrayMAGE';

my %accession   : ATTR( :get<accession>, :init_arg<accession>, :default<undef> );

sub START {

    my ( $self, $id, $args ) = @_;

    my $identifier = $self->get_accession();
    defined( $identifier )
        or croak("Error: ArrayDesign accession must be set");

    print { $self->get_fh() } <<"MAGE_OUT";
    <ArrayDesign_assnlist>
      <PhysicalArrayDesign identifier="$identifier">
MAGE_OUT

    return;
}

sub end : RESTRICTED {

    my $self = shift;

    print { $self->get_fh() } <<"MAGE_OUT";
MAGE_OUT

    return;
}

1;
