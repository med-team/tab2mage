#!/usr/bin/env perl
#
# $Id: Zone.pm 1937 2008-02-10 13:15:30Z tfrayner $

use strict;
use warnings;

########
# Zone #
########
package ArrayExpress::ArrayMAGE::Zone;
use base 'ArrayExpress::ArrayMAGE';

sub START {

    my ( $self, $id, $args ) = @_;

    # Missing zonesPerX, zonesPerY FIXME
    print { $self->get_fh() } <<"MAGE_OUT";
        <ZoneGroups_assnlist>
          <ZoneGroup>
            <ZoneLocations_assnlist>
MAGE_OUT

    return;
}

sub add {

    my ( $self, $args ) = @_;

    defined $args->{row}    or die;
    defined $args->{column} or die;

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();
    my $separator = $self->get_separator();

    print { $self->get_fh() } <<"MAGE_OUT";
              <Zone column="$args->{column}"
                    row="$args->{row}"
                    identifier="$namespace:Zone:$design$separator$args->{column}.$args->{row}">
              </Zone>
MAGE_OUT

    return;
}

sub end : RESTRICTED {

    my $self = shift;

    print { $self->get_fh() } <<"MAGE_OUT";
            </ZoneLocations_assnlist>
          </ZoneGroup>
        </ZoneGroups_assnlist>
MAGE_OUT

    return;
}

1;
