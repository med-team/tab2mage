#!/usr/bin/env perl
#
# $Id: ReporterCompositeMap.pm 1937 2008-02-10 13:15:30Z tfrayner $

use strict;
use warnings;

########################
# ReporterCompositeMap #
########################
package ArrayExpress::ArrayMAGE::ReporterCompositeMap;
use base 'ArrayExpress::ArrayMAGE';

sub START {

    my ( $self, $id, $args ) = @_;

    print { $self->get_fh() } <<"MAGE_OUT";
    <ReporterCompositeMap_assnlist>
MAGE_OUT

    return;
}

sub add {

    my ( $self, $args ) = @_;

    my $fh = $self->get_fh();

    defined $args->{identifier} or die;
    defined $args->{reporters}  or die;

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();
    my $separator = $self->get_separator();

    print $fh <<"MAGE_OUT";
      <ReporterCompositeMap identifier="$namespace:ReporterCompositeMap:$design$separator$args->{identifier}">
        <CompositeSequence_assnref>
          <CompositeSequence_ref identifier="$namespace:CompositeSequence:$design$separator$args->{identifier}"/>
        </CompositeSequence_assnref>
        <ReporterPositionSources_assnlist>
MAGE_OUT

    foreach my $reporter ( @{ $args->{reporters} } ) {
        print $fh <<"MAGE_OUT";
          <ReporterPosition>
            <Reporter_assnref>
              <Reporter_ref identifier="$namespace:Reporter:$design$separator$reporter"/>
            </Reporter_assnref>
          </ReporterPosition>
MAGE_OUT
    }

    print $fh <<"MAGE_OUT";
        </ReporterPositionSources_assnlist>
      </ReporterCompositeMap>
MAGE_OUT

    return;
}

sub end : RESTRICTED {

    my $self = shift;

    print { $self->get_fh() } <<"MAGE_OUT";
    </ReporterCompositeMap_assnlist>
MAGE_OUT

    return;
}

1;
