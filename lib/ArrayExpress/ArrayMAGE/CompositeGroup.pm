#!/usr/bin/env perl
#
# $Id: CompositeGroup.pm 1938 2008-02-10 18:34:20Z tfrayner $

use strict;
use warnings;

##################
# CompositeGroup #
##################
package ArrayExpress::ArrayMAGE::CompositeGroup;
use base 'ArrayExpress::ArrayMAGE';

my %identifier   : ATTR( :get<identifier>, :init_arg<identifier>, :default<undef> );
my %tag          : ATTR( :get<tag>,        :init_arg<tag>,        :default<undef> );
my %is_species   : ATTR( :get<is_species>, :init_arg<is_species>, :default<undef> );

sub START {

    my ( $self, $id, $args ) = @_;

    my $identifier = $self->get_identifier();
    defined( $identifier )
        or croak("Error: CompositeGroup identifier must be set");
    my $tag = $self->get_tag();
    defined( $tag )
        or croak("Error: CompositeGroup tag must be set");

    my $fh   = $self->get_fh();

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();
    my $separator = $self->get_separator();

    print $fh <<"MAGE_OUT";
      <CompositeGroup identifier="$namespace:CompositeGroup:$design$separator$identifier"
                     name="$tag">
MAGE_OUT

    $self->get_is_species() && print $fh <<"MAGE_OUT";
        <Species_assn>
          <OntologyEntry category="Organism"
                         value="$tag">
          </OntologyEntry>
        </Species_assn>
MAGE_OUT

    print $fh <<"MAGE_OUT";
        <CompositeSequences_assnreflist>
MAGE_OUT

    return;
}

sub add {

    my ( $self, $args ) = @_;

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();
    my $separator = $self->get_separator();

    defined $args->{id_ref} or die;

    print { $self->get_fh() } <<"MAGE_OUT";
          <CompositeSequence_ref identifier="$namespace:CompositeSequence:$design$separator$args->{id_ref}"/>
MAGE_OUT

    return;
}

sub end : RESTRICTED {

    my $self = shift;

    print { $self->get_fh() } <<"MAGE_OUT";
        </CompositeSequences_assnreflist>
      </CompositeGroup>
MAGE_OUT

    return;
}

1;
