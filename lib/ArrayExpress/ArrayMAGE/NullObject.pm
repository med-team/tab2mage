#!/usr/bin/env perl
#
# $Id: NullObject.pm 2088 2008-06-25 13:23:09Z tfrayner $

use strict;
use warnings;

######################
# NullObject package #
######################

package ArrayExpress::ArrayMAGE::NullObject;

use Class::Std;

# This package provides empty objects which don't store information
# when e.g. only exporting CompositeSequences for very large CDFs.

sub add {}

sub end {}

sub output {}

1;
