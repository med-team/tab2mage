#!/usr/bin/env perl
#
# $Id: Database.pm 1937 2008-02-10 13:15:30Z tfrayner $

use strict;
use warnings;

############
# Database #
############
package ArrayExpress::ArrayMAGE::Database;
use base 'ArrayExpress::ArrayMAGE';

sub START {

    my ( $self, $id, $args ) = @_;

    print { $self->get_fh() } <<"MAGE_OUT";
  <Description_package>
    <Database_assnlist>
MAGE_OUT

    return;
}

sub add {

    my ( $self, $args ) = @_;

    defined $args->{tag} or die;

    print { $self->get_fh() } <<"MAGE_OUT";
      <Database identifier="ebi.ac.uk:Database:$args->{tag}"
                name="$args->{tag}">
      </Database>
MAGE_OUT

    return;
}

sub end : RESTRICTED {

    my $self = shift;

    print { $self->get_fh() } <<"MAGE_OUT";
    </Database_assnlist>
  </Description_package>
MAGE_OUT

    return;
}

1;
