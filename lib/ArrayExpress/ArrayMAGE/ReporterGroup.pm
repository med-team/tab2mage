#!/usr/bin/env perl
#
# $Id: ReporterGroup.pm 1938 2008-02-10 18:34:20Z tfrayner $

use strict;
use warnings;

#################
# ReporterGroup #
#################
package ArrayExpress::ArrayMAGE::ReporterGroup;
use base 'ArrayExpress::ArrayMAGE';

my %identifier   : ATTR( :get<identifier>, :init_arg<identifier>, :default<undef> );
my %tag          : ATTR( :get<tag>,        :init_arg<tag>,        :default<undef> );
my %is_species   : ATTR( :get<is_species>, :init_arg<is_species>, :default<undef> );

sub START {

    my ( $self, $id, $args ) = @_;

    my $identifier = $self->get_identifier();
    defined( $identifier )
        or croak("Error: ReporterGroup identifier must be set");
    my $tag = $self->get_tag();
    defined( $tag )
        or croak("Error: ReporterGroup tag must be set");

    my $fh   = $self->get_fh();

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();
    my $separator = $self->get_separator();

    print $fh <<"MAGE_OUT";
      <ReporterGroup identifier="$namespace:ReporterGroup:$design$separator$identifier"
                     name="$tag">
MAGE_OUT

    $self->get_is_species() && print $fh <<"MAGE_OUT";
        <Species_assn>
          <OntologyEntry category="Organism"
                         value="$tag">
          </OntologyEntry>
        </Species_assn>
MAGE_OUT

    print $fh <<"MAGE_OUT";
        <Reporters_assnreflist>
MAGE_OUT

    return;
}

sub add {

    my ( $self, $args ) = @_;

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();
    my $separator = $self->get_separator();

    defined $args->{id_ref} or die;

    print { $self->get_fh() } <<"MAGE_OUT";
          <Reporter_ref identifier="$namespace:Reporter:$design$separator$args->{id_ref}"/>
MAGE_OUT

    return;
}

sub end : RESTRICTED {

    my $self = shift;

    print { $self->get_fh() } <<"MAGE_OUT";
        </Reporters_assnreflist>
      </ReporterGroup>
MAGE_OUT

    return;
}

1;
