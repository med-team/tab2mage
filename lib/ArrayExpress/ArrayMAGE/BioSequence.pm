#!/usr/bin/env perl
#
# $Id: BioSequence.pm 1937 2008-02-10 13:15:30Z tfrayner $

use strict;
use warnings;

###############
# BioSequence #
###############
package ArrayExpress::ArrayMAGE::BioSequence;
use base 'ArrayExpress::ArrayMAGE';

sub START {

    my ( $self, $id, $args ) = @_;

    print { $self->get_fh() } <<"MAGE_OUT";
  <BioSequence_package>
    <BioSequence_assnlist>
MAGE_OUT

    return;
}

sub add {

    my ( $self, $args ) = @_;

    my $fh = $self->get_fh();

    my $namespace = $self->get_namespace();
    my $design    = $self->get_design();
    my $separator = $self->get_separator();

    defined $args->{identifier} or die;

    print $fh
        "      <BioSequence identifier=\"$namespace:BioSequence:$design$separator$args->{identifier}\"";
    $args->{sequence} && do {
        my $seqlen = length $args->{sequence};
        print $fh
            "\n                   sequence=\"$args->{sequence}\"\n                   length=\"$seqlen\"";
    };
    print $fh ">\n";

    if ( $args->{dbrefs}
	     && scalar( grep { defined $_ } values %{ $args->{dbrefs} } ) ) {
        ref $args->{dbrefs} eq 'HASH' or die;
        print $fh "        <SequenceDatabases_assnlist>\n";
        while ( my ( $acc, $db ) = each %{ $args->{dbrefs} } ) {
            print $fh <<"MAGE_OUT";
          <DatabaseEntry accession="$acc">
            <Database_assnref>
              <Database_ref identifier="ebi.ac.uk:Database:$db"/>
            </Database_assnref>
          </DatabaseEntry>
MAGE_OUT
        }
        print $fh "        </SequenceDatabases_assnlist>\n";
        };

    $args->{polymertype} && print $fh <<"MAGE_OUT";
        <PolymerType_assn>
          <OntologyEntry category="PolymerType"
                         value="$args->{polymertype}">
          </OntologyEntry>
        </PolymerType_assn>
MAGE_OUT

    $args->{type} && print $fh <<"MAGE_OUT";
        <Type_assn>
          <OntologyEntry category="BioSequenceType"
                         value="$args->{type}">
          </OntologyEntry>
        </Type_assn>
MAGE_OUT

    print $fh "      </BioSequence>\n";

    return;
}

sub end : RESTRICTED {

    my $self = shift;

    print { $self->get_fh() } <<"MAGE_OUT";
    </BioSequence_assnlist>
  </BioSequence_package>
MAGE_OUT

    return;
}

1;
