#!/usr/bin/env perl
#
# $Id: ArrayMAGE.pm 1938 2008-02-10 18:34:20Z tfrayner $

use strict;
use warnings;

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../index.html">
                <img src="../T2M_logo.png"
                     border="0" height="50" alt="Tab2MAGE logo"></td>
              </a>
	    <td class="pagetitle">Module detail: ArrayMAGE.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::ArrayMAGE.pm - an OO module providing methods
for writing MAGE-ML for array designs.

=head1 SYNOPSIS

 # Import all the ArrayMAGE classes.
 use ArrayExpress::ArrayMAGE qw(:ALL);
 
 # Instantiate the objects we require.
 my @classes = qw(
     Feature
     Reporter
     BioSequence
     FeatureReporterMap
     ArrayDesign
     Zone
 );
 my %object;
 foreach my $type (@classes) {
     if ( $type eq 'ArrayDesign' ) {
 
         # ArrayDesign requires an accession.
         $object{$type} = "ArrayExpress::ArrayMAGE::$type"->new({
	     accession => $acc,
	 });
     }
     else {
	 $object{$type} = "ArrayExpress::ArrayMAGE::$type"->new();
     }
 }
 
 # ReporterGroup and CompositeGroup are typically handled
 # slightly differently, because multiple groups may be
 # instantiated during a run (see below).
 my ( %reporter_group, %composite_group );
 
 # Add objects, e.g. while scanning through an ADF or CDF file.
 foreach my $row_hashref ( @list_of_lines ) {
    $object{Feature}->add({
        metacolumn => $row->{'MetaColumn'},
        metarow    => $row->{'MetaRow'},
        column     => $row->{'Column'},
        row        => $row->{'Row'},
    });
 
    # ReporterGroups and CompositeGroups are handled in similar
    # ways (only ReporterGroup is shown here):
    while ( my ( $rname, $rvalue ) = each %$row ) {
       if ( my ($type) = ( $rname =~ m!ReporterGroup\[(.*?)\]!i ) ) {
           my $key = "$type.$rvalue";
           $reporter_group{$key}
               ||= ArrayExpress::ArrayMAGE::ReporterGroup->new({
               identifier => $key,
               tag        => $rvalue,
               is_species => ( $type =~ m!species!i ) || 0,
           });
           $reporter_group{$key}->add({
               identifier => $row->{'Reporter Identifier'},
           });
       }
    }
 }
 
 # Write out the MAGE-ML to STDOUT.
 ArrayExpress::ArrayMAGE->combine_parts(
     \%objects,
     \%reporter_group,
     \%composite_group,
     \*STDOUT,
 );

=head1 DESCRIPTION

This is a module designed to allow the creation of MAGE-ML for array
designs in a memory-efficient fashion. This module is the abstract
superclass for a series of MAGE-like classes representing parts of the
MAGE-ML document to be written. These classes, when instantiated, all
point to temporary filehandles into which is written the XML
describing MAGE object instances. Each subclass implements an 'add'
method in addition to those inherited from this base
class. Instantiating the object with new() opens up the XML tags for a
list of objects, the 'add' adds objects based on the values it is
passed (no surprise there) and finally the XML tags are closed and the
MAGE-ML written out by calling the class method combine_parts() in this
top-level class.

=head1 PUBLIC METHODS

=over 2

=item C<combine_parts( \%obj, \%rg, \%cg, $fh )>

Class method. Takes: a hashref of ArrayMAGE objects, a hashref of
ReporterGroups, a hashref of CompositeGroups, and an open output
filehandle, and writes the completed MAGE-ML to that filehandle.

=item C<set_namespace("namespace")>

Class method. Allows the user to set the identifier namespace for the
output MAGE objects.

=item C<set_design("design")>

Class method. Allows the user to set the design name used in
identifiers for the output MAGE objects. Typically this will be the
same as the array accession, but e.g. for Affymetrix arrays will
correspond to the design name.

=item C<set_separator("separator")>

Class method. Allows the user to set the identifier separator for the
output MAGE objects. Usually this will be q{:} or q{.}.

=item C<set_programname("programname")>

Class method. Allows the user to set the name of the program (this is
not particularly useful).

=back

=head1 SUBCLASSES

=head2 ArrayExpress::ArrayMAGE::ArrayDesign

=over 2

=item new() required attribute: C<accession>

The accession attribute becomes the MAGE PhysicalArrayDesign
identifier.

=item add() is not implemented for this class.

=back

=head2 ArrayExpress::ArrayMAGE::BioSequence

=over 2

=item new() requires no attributes.

=item add() required attribute: C<identifier>

This identifier is combined with the class namespace to create the
BioSequence identifier.

=item add() optional attribute: C<dbrefs>

A hashref with db accessions as keys, and db tags as values
(e.g. C<{ A12345 =E<gt> 'embl' }> ). Typically used for Reporter
BioSequence Database Entry ADF columns.

=back

=head2 ArrayExpress::ArrayMAGE::CompositeGroup

=over 2

=item new() required attribute: C<identifier>

This identifier is combined with the class namespace to create a
CompositeGroup identifier.

=item new() required attribute: C<tag>

This tag is used as the CompositeGroup name. If C<is_species> is set,
then it will also be used in a Species_assn element as the species
name.

=item new() optional attribute: C<is_species>

A flag indicating whether this is a grouping based on species or not.

=item add() required attribute: C<id_ref>

This identifier is combined with the class namespace to create a
CompositeSequence_ref identifier.

=back

=head2 ArrayExpress::ArrayMAGE::CompositeSequence

=over 2

=item new() requires no attributes.

=item add() required attribute: C<identifier>

This identifier is combined with the class namespace to create the
CompositeSequence identifier.

=item add() optional attribute: C<name>

The name of the CompositeSequence.

=item add() optional attribute: C<biosequences>

An arrayref of BioSequence identifiers to be attached to this
CompositeSequence.

=item add() optional attribute: C<dbrefs>

A hashref with db accessions as keys, and db tags as values
(e.g. C<{ A12345 =E<gt> 'embl' }> ). Typically used for CompositeSequence
Description Database Entry ADF columns.

=item add() optional attribute: C<controltype>

A ControlType ontology term to be attached to this CompositeSequence.

=item add() optional attribute: C<comment>

A comment to be added to this CompositeSequence as a Description_assn.

=item add() optional attribute: C<cs_only>

A flag indicating that we are not creating Reporters (and can
therefore skip ReporterCompositeMap creation).

=back

=head2 ArrayExpress::ArrayMAGE::Database

=over 2

=item new() requires no attributes.

=item add() required attribute: C<tag>

This tag is combined with the class namespace to create the Database
identifier. Typically this will be a standard database tag,
e.g. C<embl>.

=back

=head2 ArrayExpress::ArrayMAGE::Feature

=over 2

=item new() requires no attributes.

=item add() required attribute: C<metacolumn>

=item add() required attribute: C<metarow>

=item add() required attribute: C<column>

=item add() required attribute: C<row>

Feature coordinates, used in the Feature and Zone_ref identifiers and
in FeatureLocation.

=back

=head2 ArrayExpress::ArrayMAGE::FeatureReporterMap

=over 2

=item new() requires no attributes.

=item add() required attribute: C<identifier>

The Reporter identifier to which Features should be mapped.

=item add() required attribute: C<features>

An arrayref of Feature identifiers mapped to the Reporter.

=item add() optional attribute: C<mismatch_info>

A hashref of hashrefs, keyed by Feature identifier and then by
C<start_coord>, C<new_sequence> and C<replaced_length>, used to create
a MismatchInformation_assnlist (useful for Affymetrix array designs).

=back

=head2 ArrayExpress::ArrayMAGE::Reporter

=over 2

=item new() requires no attributes.

=item add() required attribute: C<identifier>

This identifier is combined with the class namespace to create the
Reporter identifier.

=item add() optional attribute: C<name>

The name of the Reporter.

=item add() optional attribute: C<biosequences>

An arrayref of BioSequence identifiers to be attached to this
Reporter.

=item add() optional attribute: C<dbrefs>

A hashref with db accessions as keys, and db tags as values
(e.g. C<{ A12345 =E<gt> 'embl' }> ). Typically used for Reporter
Description Database Entry ADF columns.

=item add() optional attribute: C<controltype>

A ControlType ontology term to be attached to this Reporter.

=item add() optional attribute: C<warningtype>

A WarningType ontology term to be attached to this Reporter.

=item add() optional attribute: C<failtype>

A FailType ontology term to be attached to this Reporter.

=item add() optional attribute: C<comment>

A comment to be added to this Reporter as a Description_assn.

=back

=head2 ArrayExpress::ArrayMAGE::ReporterCompositeMap

=over 2

=item new() requires no attributes.

=item add() required attribute: C<identifier>

The CompositeSequence identifier to which Reporters should be mapped.

=item add() required attribute: C<reporters>

An arrayref of Reporter identifiers mapped to the CompositeSequence.

=back

=head2 ArrayExpress::ArrayMAGE::ReporterGroup

=over 2

=item new() required attribute: C<identifier>

This identifier is combined with the class namespace to create a
ReporterGroup identifier.

=item new() required attribute: C<tag>

This tag is used as the ReporterGroup name. If C<is_species> is set,
then it will also be used in a Species_assn element as the species
name.

=item new() optional attribute: C<is_species>

A flag indicating whether this is a grouping based on species or not.

=item add() required attribute: C<id_ref>

This identifier is combined with the class namespace to create a
Reporter_ref identifier.

=back

=head2 ArrayExpress::ArrayMAGE::Zone

=over 2

=item new() requires no attributes.

=item add() required attribute: C<column>

=item add() required attribute: C<row>

Zone coordinates, used in the Zone identifier and in its row and
column attributes.

=back

=head1 AUTHOR

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2008.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments. 

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

package ArrayExpress::ArrayMAGE;

use IO::File;
use Carp;
use Class::Std;

my %filehandle : ATTR( :set<filehandle>, :default<undef> );

# These are class variables used to set global behaviour.
my %ClassOptions = (
    programname => q{ArrayMAGE},
    separator   => q{.},
    design      => undef,
);

$ClassOptions{namespace} = sprintf( "ebi.ac.uk:%s", $ClassOptions{programname} );

# Define instance/class methods to set and get options.
{
    ## no critic ProhibitNoStrict
    no strict qw(refs);
    ## use critic ProhibitNoStrict

    foreach my $method ( qw(programname namespace separator design) ) {
	my $setter = "set_$method";
	my $getter = "get_$method";

	*{$setter} = sub {
	    my ( $class, @args ) = @_;

	    unless ( scalar @args && defined $args[0] ) {
		croak("Error: no value passed to $method setter.\n");
	    }

	    $ClassOptions{$method} = $args[0];

	    return;
	};

	*{$getter} = sub {
	    
	    my ( $class ) = @_;

	    return $ClassOptions{$method};
	}
    }
}

# Convenience import method.
sub import {

    my ( $pkg, @tags ) = @_;

    foreach (@tags) {
        if ( $_ =~ m/ALL/i ) {
            import_all();
        }
    }

    return;
}

sub import_all {

    eval {
        require ArrayExpress::ArrayMAGE::ArrayDesign;
        require ArrayExpress::ArrayMAGE::BioSequence;
        require ArrayExpress::ArrayMAGE::CompositeGroup;
        require ArrayExpress::ArrayMAGE::CompositeSequence;
        require ArrayExpress::ArrayMAGE::Database;
        require ArrayExpress::ArrayMAGE::Feature;
        require ArrayExpress::ArrayMAGE::FeatureReporterMap;
        require ArrayExpress::ArrayMAGE::Reporter;
        require ArrayExpress::ArrayMAGE::ReporterCompositeMap;
        require ArrayExpress::ArrayMAGE::ReporterGroup;
        require ArrayExpress::ArrayMAGE::Zone;
        require ArrayExpress::ArrayMAGE::ProgressBar;
    };

    if ($@) {
        croak "ArrayMAGE load error: $@";
    }

    return;
}

sub combine_parts {

    # Given hashrefs to a general set of ArrayMAGEs and the
    # ReporterGroups ArrayMAGEs, merge everything with a bit of glue
    # and output to the passed filehandle.

    my ( $class, $objects, $reporter_group, $composite_group, $fh ) = @_;

    # Close out the various XML tags.
    foreach my $object ( values %{ $objects },
			 values %{ $reporter_group },
			 values %{ $composite_group } ) {
	$object->end();
    }

    my $namespace = $class->get_namespace();
    my $design    = $class->get_design();
    my $separator = $class->get_separator();

    print STDOUT ("\nCombining MAGE-ML parts into output file...\n");

    print $fh <<"MAGE_OUT";
<?xml version="1.0" encoding="ISO-8859-1" standalone="no"?>
<!DOCTYPE MAGE-ML PUBLIC "-//OMG//DTD MAGE-ML 1.1//EN"  "MAGE-ML.dtd">
<MAGE-ML identifier="MAGE:$namespace:$design">
MAGE_OUT

    foreach my $type qw(Database BioSequence) {
        $objects->{$type}->output($fh) if $objects->{$type};
    }

    print $fh <<"MAGE_OUT";
  <DesignElement_package>
MAGE_OUT

    foreach my $type
        qw(CompositeSequence Reporter ReporterCompositeMap FeatureReporterMap)
    {
        $objects->{$type}->output($fh) if $objects->{$type};
    }

    print $fh <<"MAGE_OUT";
  </DesignElement_package>
MAGE_OUT

    print $fh <<"MAGE_OUT";
  <ArrayDesign_package>
MAGE_OUT

    if ( scalar( grep { defined $_ } values %$reporter_group ) ) {
	print $fh <<"MAGE_OUT";
    <ReporterGroup_assnlist>
MAGE_OUT

	foreach my $group ( values %$reporter_group ) {
	    $group->output($fh);
	}
	print $fh <<"MAGE_OUT";
    </ReporterGroup_assnlist>
MAGE_OUT
    }

    if ( scalar( grep { defined $_ } values %$composite_group ) ) {
        print $fh <<"MAGE_OUT";
    <CompositeGroup_assnlist>
MAGE_OUT
        foreach my $group ( values %$composite_group ) {
            $group->output($fh);
        }
        print $fh <<"MAGE_OUT";
    </CompositeGroup_assnlist>
MAGE_OUT
    }

    foreach my $type qw(ArrayDesign Feature) {
        $objects->{$type}->output($fh) if $objects->{$type};
    }

    if ( scalar( grep { defined $_ } values %$reporter_group ) ) {
        print $fh <<"MAGE_OUT";
        <ReporterGroups_assnreflist>
MAGE_OUT
        foreach my $key ( keys %$reporter_group ) {
            print $fh <<"MAGE_OUT";
          <ReporterGroup_ref identifier="$namespace:ReporterGroup:$design$separator$key"/>
MAGE_OUT
        }
        print $fh <<"MAGE_OUT";
        </ReporterGroups_assnreflist>
MAGE_OUT
    }

    if ( scalar( grep { defined $_ } values %$composite_group ) ) {
        print $fh <<"MAGE_OUT";
        <CompositeGroups_assnreflist>
MAGE_OUT
        foreach my $key ( keys %$composite_group ) {
            print $fh <<"MAGE_OUT";
          <CompositeGroup_ref identifier="$namespace:CompositeGroup:$design$separator$key"/>
MAGE_OUT
        }
        print $fh <<"MAGE_OUT";
        </CompositeGroups_assnreflist>
MAGE_OUT
    }

    foreach my $type qw(Zone) {
        $objects->{$type}->output($fh) if $objects->{$type};
    }

    print $fh <<"MAGE_OUT";
      </PhysicalArrayDesign>
    </ArrayDesign_assnlist>
  </ArrayDesign_package>
</MAGE-ML>
MAGE_OUT

    return;
}

sub get_fh : RESTRICTED {

    my ( $self ) = @_;

    unless ( $filehandle{ident $self} ) {
        $filehandle{ident $self} = IO::File->new_tmpfile();
    }
    return $filehandle{ident $self};
}

sub output : RESTRICTED {

    my ( $self, $output_fh ) = @_;

    my $fh = $self->get_fh();
    $fh->isa('IO::File') or die("Error: object filehandle uninitialized.");
    seek( $fh, 0, 0 );
    while ( my $line = <$fh> ) {
        print $output_fh $line;
    }

    return;
}

1;
