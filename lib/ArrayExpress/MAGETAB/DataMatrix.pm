#!/usr/bin/env perl
#
# Module to help in the generation of MAGETAB DataMatrix MAGE objects.
#
# Tim Rayner 2007, ArrayExpress team, European Bioinformatics Institute
#
# $Id: DataMatrix.pm 1852 2007-12-13 10:14:27Z tfrayner $
#

package ArrayExpress::MAGETAB::DataMatrix;

use strict;
use warnings;

use Carp;
use English qw( -no_match_vars );
use Class::Std;

use ArrayExpress::Curator::Config qw( $CONFIG );
use ArrayExpress::Curator::MAGE qw(unique_identifier);

use base 'ArrayExpress::Datafile::DataMatrix';

my %id_template    : ATTR( :name<id_template>,    :default<undef> );
my %datafile       : ATTR( :name<datafile>,       :default<undef> );
my %mage_helper    : ATTR( :name<mage_helper>,    :default<undef> );
my %dm_id_template : ATTR( :name<dm_id_template>, :default<undef> );

sub START {

    my ( $self, $id, $args ) = @_;

    unless ( $self->get_datafile() ) {
	croak("Error: datafile attribute not set.");
    }
    unless ( $self->get_id_template() ) {
	croak("Error: id_template attribute not set.");
    }
    unless ( $self->get_mage_helper() ) {
	croak("Error: mage_helper attribute not set.");
    }

    # This is a per-file template. 
    $self->set_dm_id_template(
	sprintf(
	    "%s.%s.DataMatrix",
	    $self->get_id_template(),
	    unique_identifier(),
	)
    );

    return;
}

sub create_mage {

    my ( $self ) = @_;

    # Figure out BADim, QTDim and data cube order. Generate MAGE for
    # QTD and add to data object ($file->get_mage_badata). Map BADim
    # to bioassays as specified by $file->get_sdrf_id_column.

    my $file    = $self->get_datafile();
    my $badata  = $file->get_mage_badata();
    my $dm_hybs = $file->get_heading_hybs();
    my $dm_qts  = $file->get_heading_qts();

    my $magetab = $self->get_mage_helper();
    my $bag_of  = $magetab->get_bags();

    # Construct the dimensions
    my ( $ba_namerefs, $qt_names, $order )
        = $self->get_dimension_lists( $dm_hybs, $dm_qts );

    # Internal consistency check; order should always be set.
    croak("Error: Order of Data Matrix data cube is indeterminate.\n")
        unless $order;

    # Warn on unsupported cube order
    if ( $order eq 'DQB' ) {
        warn(
            qq{WARNING: A BioDataCube order of "$order" is not }
	  . qq{supported by the ArrayExpress MAGE-ML loader.\n}
        );
    }
    my $bdc = $badata->getBioDataValues();
    $bdc->setOrder( $order );

    # Create the actual qtd here
    my ( $qt_list, $qtd_key ) = $magetab->qts_from_names( $file, $qt_names );

    my $qtd = $bag_of->{qtd}->(
        $qtd_key,
        {
	    qt_list             => $qt_list,
            identifier_template => $self->get_dm_id_template(),
        }
    );
    $file->set_mage_qtd($qtd);
    $badata->setQuantitationTypeDimension($qtd);

    # Store the old MBAs from the BioAssayDimension for later
    # repointing to a new DBA/DBAD if this is an Array Data File
    # converted to Matrix (e.g. Illumina).
    my @old_mbas;
    if ( $file->get_data_type() eq $CONFIG->get_RAW_DM_FILE_TYPE() ) {
	if ( my $ded = $badata->getBioAssayDimension() ) {
	    foreach my $old ( @{ $ded->getBioAssays() || [] } ) {
		my $name = $old->getName();
		push @old_mbas, $old;
		$bag_of->{mba}->( $name, 'delete' )
		    or die("Error deleting unwanted MBA: $name");
	    }
	}
    }

    # Find or create list of BioAssay objects given the $ba_namerefs
    # arrayref of arrayrefs.
    my $mapped_bioassays = $self->retrieve_mapped_bioassays($ba_namerefs);

    # Sort out the BioAssayMaps here.
    $self->add_bioassays_to_bioassaymaps( $mapped_bioassays, \@old_mbas );

    # Create and link the BioAssayDimension here.
    my $bad = Bio::MAGE::BioAssayData::BioAssayDimension->new(
	identifier => $self->get_dm_id_template() . '.MeasuredBioAssayDimension',
	bioAssays  => $mapped_bioassays,
    );
    $badata->setBioAssayDimension( $bad );

    # FIXME figure out how to add MBA protocol applications? These are
    # FeatureExtraction protocols, not yet supported by SDRF; this is
    # not an issue for Derived data, which attaches
    # ProtocolApplication to ProducerTransformation.

    return;
}

sub repoint_mba_to_dba : PRIVATE {

    # This is pretty much Illumina-only at the moment.

    my ( $self, $dba, $old_mbas, $file, $bags ) = @_;

    my @bams = @{ $bags->{bam}->() };

    foreach my $old ( @$old_mbas ) {
	my $old_name = $old->getName();
	foreach my $bam ( @bams ) {
	    my @new;
	    foreach my $source ( @{ $bam->getSourceBioAssays() || [] } ) {
		if ( $source->isa('Bio::MAGE::BioAssay::MeasuredBioAssay')
			 && $source->getName() eq $old_name ) {

		    # Replace the old MBA with the new DBA.
		    push @new, $dba;

		    # Make DBAD from MBAD, repoint Dimensions and
		    # BioDataCube. FIXME this only remaps the
		    # first MBAD, not a general solution.
		    if ( my $mbad = shift @{ $source->getMeasuredBioAssayData() || [] } ) {
			my $dbad = $bags->{dbad}->(
			    $mbad->getName(),
			    {
				identifier_template => $self->get_dm_id_template(),
				de_dimension        => $mbad->getDesignElementDimension(),
				qt_dimension        => $mbad->getQuantitationTypeDimension(),
				filename            => $mbad->getBioDataValues()->getCube(),
			    }
			);
			$dba->setDerivedBioAssayData( [ $dbad ] );
			$bags->{mbad}->( $mbad->getName(), 'delete' );
			$file->set_mage_badata( $dbad );
		    }

		    my @fvs = @{ $source->getBioAssayFactorValues() || [] };
		    $dba->setBioAssayFactorValues( \@fvs );

		    # FIXME also switch around FeatureExtraction
		    # ProtocolApps here?
		}
		else {
		    push @new, $source;
		}
	    }
	    $bam->setSourceBioAssays( \@new );
	}
    }

    return;
}

sub add_bioassays_to_bioassaymaps : PRIVATE {

    my ( $self, $mapped_bioassays, $old_mbas ) = @_;

    my $file    = $self->get_datafile();
    my $badata  = $file->get_mage_badata();
    my $magetab = $self->get_mage_helper();

    my $maps = [];
    if ( $file->get_data_type() eq $CONFIG->get_RAW_DM_FILE_TYPE() ) {

	# Create new DBA if this is an Array Data Matrix, link to the
	# BioAssay list using $magetab->bam_bag(). Note that we can't
	# add it to the MBAData because ProducerTransformation is only
	# for DBAData.
	my $name = $file->get_name();
	my $dba = $magetab->get_bags()->{datamatrix_dba}->(
	    $name,
	    {
		identifier_template => $self->get_dm_id_template(),
		name                => $name,
	    }
	);
	$maps = [
	    $magetab->get_bags()->{bam}->(
		$name,
		{
		    identifier_template => $self->get_dm_id_template(),
		    target_bioassay     => $dba,
		}
	    ),
	];
	$dba->setDerivedBioAssayMap( $maps );

	# Remap old MBA links (generated during SDRF parsing) to the
	# newly-created DBA.
	my $bags = $magetab->get_bags();
	$self->repoint_mba_to_dba( $dba, $old_mbas, $file, $bags );

	# FIXME Array Data Matrix BioAssay lacks FVs at the moment
	# (Illumina is okay though).

    }
    elsif ( $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE() ) {

	# Use the BioAssayMaps from $badata.
	my $txn = $badata->getProducerTransformation()
	    or croak("Error: DBAD has no ProducerTransformation.");

	my $mapping = $txn->getBioAssayMapping()
	    or croak("Error: DBAD ProducerTransformation has no BioAssayMapping.");

	$maps = $mapping->getBioAssayMaps();

	# Since we're here, add the QuantitationTypeMapping.
	$self->add_qt_mapping(
	    $txn,
	    $file->get_mage_qtd(),
	    $mapped_bioassays,
	    $magetab->get_bags()->{qtm},
	);
    }
    else {
	croak("Error: unrecognized data type: " . $file->get_data_type() );
    }

    unless ( scalar @$maps ) {
	croak("Error: BioAssayData cannot be mapped to source BioAssays.");
    }

    foreach my $map ( @$maps ) {
	$map->setSourceBioAssays( $mapped_bioassays );
    }

    return;
}

sub retrieve_mapped_bioassays : PRIVATE {

    my ( $self, $ba_namerefs ) = @_;

    my $bags      = $self->get_mage_helper()->get_bags();
    my $id_column = $self->get_datafile->get_sdrf_id_column();

    my $bioassays;
    if ( $id_column =~ /(?:Hybridi[zs]ation|Assay)(?: *Name)?(?: *REF)?/i ) {
	$bioassays = $self->map_physical_bioassays(
	    $ba_namerefs,
	    $bags->{pba},
	    $bags->{mba},
	    $bags->{datamatrix_dba},
	    $bags->{bam},
	    $id_column,
	);	
    }
    elsif ( $id_column =~ /Scan(?: *Name)?(?: *REF)?/i ) {
	$bioassays = $self->map_physical_bioassays(
	    $ba_namerefs,
	    $bags->{extended_pba},
	    $bags->{mba},
	    $bags->{datamatrix_dba},
	    $bags->{bam},
	    $id_column,
	    $bags->{pba},
	);
    }
    elsif ( $id_column =~ /Normali[zs]ation(?: *Name)?(?: *REF)?/i ) {
	$bioassays = $self->map_derived_bioassays(
	    $ba_namerefs,
	    $bags->{dba},
	    $bags->{datamatrix_dba},
	    $bags->{bam},
	    $id_column,
	);
    }
    else {
	croak("Error: Unrecognized/unsupported column: $id_column\n");
    }

    return $bioassays;
}    

sub map_derived_bioassays : PRIVATE {

    # Data matrices mapped to Normalization allows us to query just for
    # Normalization Names.

    my ( $self,
	 $ba_namerefs,
	 $dba_bag,
	 $dm_dba_bag,
	 $bam_bag,
	 $id_column ) = @_;

    my @ba_list;
    foreach my $ba_names ( @$ba_namerefs ) {

	my @column_bas;
	foreach my $ba_name ( @$ba_names ) {
	    my $col_ba = $self->retrieve_bioassay($ba_name, $dba_bag, $id_column);
	    push @column_bas, $col_ba;
	}

	my $bioassay = $self->find_or_create_per_column_bioassay(
	    \@column_bas,
	    $self->get_dm_id_template(),
	    $dm_dba_bag,
	    $bam_bag,
	);
	push @ba_list, $bioassay;
    }

    return \@ba_list;
}

sub map_physical_bioassays : PRIVATE {

    # Data matrices mapped to Scan or Hybridization need MBAs
    # generated for each PBA (hyb/scan). Grouping DBAs are created as
    # necessary.

    my ( $self,
	 $ba_namerefs,
	 $pba_bag,
	 $mba_bag,
	 $dm_dba_bag,
	 $bam_bag,
	 $id_column,
	 $fallback_bag ) = @_;

    my @ba_list;
    foreach my $ba_names ( @$ba_namerefs ) {

	my @column_bas;
	foreach my $ba_name ( @$ba_names ) {

	    # Scan Name can be used to look up PBAs in
	    # extended_pba_bag, but note that this won't work for
	    # single-channel data, which needs a suitable fallback to
	    # hyb PBA having ImageAcquisition with name=Scan Name.
	    my $col_pba = $self->retrieve_bioassay(
		$ba_name,
		$pba_bag,
		$id_column,
		$fallback_bag,
	    );

	    # Follow the BioAssayTreatments to the end of the PBA
	    # chain.
	    $col_pba = $self->find_last_pba($col_pba);

	    # Find or create the MBA here.
	    my $col_mba = $self->find_or_create_mba(
		$col_pba,
		$mba_bag,
	    );

	    push @column_bas, $col_mba;
	}

	my $bioassay = $self->find_or_create_per_column_bioassay(
	    \@column_bas,
	    $self->get_dm_id_template(),
	    $dm_dba_bag,
	    $bam_bag,
	);
	push @ba_list, $bioassay;
    }

    return \@ba_list;
}

sub find_last_pba : PRIVATE {

    my ( $self, $pba ) = @_;

    # If no treatments, give up immediately.
    my $bats = $pba->getBioAssayTreatments()
	or return $pba;

    BAT:
    foreach my $bat ( @$bats ) {
	my $new_pba = $bat->getTarget() or next BAT;

	if ( $new_pba->getIdentifier() eq $pba->getIdentifier() ) {

	    # Recursion endpoint.
	    return $new_pba;
	}
	else {

	    # Return the PBA linked to the first Target PBA we find.
	    return $self->find_last_pba($new_pba);
	}
    }

    # If no BATs with Target, return our original PBA.
    return $pba;
}

sub find_or_create_mba : PRIVATE {

    my ( $self, $pba, $mba_bag ) = @_;

    my $pba_id = $pba->getIdentifier();

    MBA:
    foreach my $mba ( @{ $mba_bag->() } ) {

	my $fext = $mba->getFeatureExtraction()
	    or next MBA;

	my $mba_pba = $fext->getPhysicalBioAssaySource()
	    or next MBA;

	if ( $mba_pba->getIdentifier() eq $pba_id ) {

	    # Success; return the MBA
	    return $mba;
	}
    }

    # Not found; create a new MBA.
    my $name = $pba->getName();
    my $mba  = $mba_bag->(
	$name,
	{
	    name                => $name,
	    physical_bioassay   => $pba,
	    identifier_template => $self->get_id_template() . ".$name",
	}
    );

    # Propagate factor values.
    $self->update_bioassay_fvs(
	$mba,
	$pba->getBioAssayFactorValues()
    );

    return $mba;
}

sub add_qt_mapping : PRIVATE {

    my ( $self, $txn, $mage_qtd, $source_bioassays, $qtm_bag ) = @_;

    $self->add_target_qts_to_mapping( $txn, $mage_qtd, $qtm_bag );
    
    $self->add_source_qts_to_mapping( $txn, $source_bioassays, $qtm_bag );

    return;
}

sub add_target_qts_to_mapping : PRIVATE {

    my ( $self, $txn, $mage_qtd, $qtm_bag ) = @_;

    my $qtmapping;
    unless ( $qtmapping = $txn->getQuantitationTypeMapping ) {
	$qtmapping = Bio::MAGE::BioAssayData::QuantitationTypeMapping->new();
	$txn->setQuantitationTypeMapping( $qtmapping );
    }

    my %found;
    foreach my $qtmap ( @{ $qtmapping->getQuantitationTypeMaps() || [] } ) {
	if ( my $oldqt = $qtmap->getTargetQuantitationType() ) {
	    $found{ $oldqt->getIdentifier() }++;
	}
    }
    foreach my $newqt ( @{ $mage_qtd->getQuantitationTypes() || [] } ) {
	unless ( $found{ $newqt->getIdentifier() } ) {
	    my $qtmap = $qtm_bag->(
		$newqt->getName(),
		{
		    target_qt           => $newqt,
		    identifier_template => $self->get_id_template(),
		}
	    );
	    $qtmapping->addQuantitationTypeMaps( $qtmap );
	}
    }

    return;
}

sub add_source_qts_to_mapping : PRIVATE {

    my ( $self, $txn, $source_bioassays, $qtm_bag ) = @_;

    my ($source_qts, $source_data) = $self->get_bioassay_qts( $source_bioassays );

    $txn->setBioAssayDataSources( $source_data );

    my $qtmapping;
    unless ( $qtmapping = $txn->getQuantitationTypeMapping() ) {
	carp("Warning: No QuantitationTypeMapping for Transformation object "
		 . $txn->getIdentifier() );
	return;
    }

    foreach my $qtmap ( @{ $qtmapping->getQuantitationTypeMaps() || [] } ) {
	my %found;
	foreach my $oldqt ( @{ $qtmap->getSourcesQuantitationType() || [] } ) {
	    $found{ $oldqt->getIdentifier() }++;
	}
	foreach my $newqt ( @$source_qts ) {
	    unless ( $found{ $newqt->getIdentifier() } ) {
		$qtmap->addSourcesQuantitationType( $newqt );
	    }
	}
    }

    return;
}

1;
