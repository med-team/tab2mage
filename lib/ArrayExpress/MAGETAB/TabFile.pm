#!/usr/bin/env perl
#
# Module providing IDF parsing functions to the ArrayExpress MAGE-TAB
# implementation.
#
# Tim Rayner, 2007, EMBL-EBI Microarray Informatics Team
#
# $Id: TabFile.pm 2017 2008-04-01 21:24:23Z tfrayner $

package ArrayExpress::MAGETAB::TabFile;

use strict;
use warnings;

use Class::Std;
use Carp;
use List::Util qw(first);
use Bio::MAGE qw(:ALL);
use Text::CSV_XS;
use Readonly;

use ArrayExpress::Curator::Common qw(check_linebreaks);
use ArrayExpress::Curator::MAGE qw(
    make_container
    unique_identifier

    new_armanuf
    new_array
    new_image
    new_biosource
    new_biosample
    new_extract
    new_labeledextract
    new_labelcompound
    new_measuredbioassay
    new_derivedbioassay
    new_measuredbioassaydata
    new_derivedbioassaydata
    new_bioassaymap
    new_quantitationtypemap
    new_quantitationtype
    new_qtd
);

use ArrayExpress::Curator::MAGE::Definitions qw(
    $AE_LABELCOMPOUND_PREFIX
    $AE_CHANNEL_PREFIX
);

Readonly my $BLANK => qr/\A (->)? \z/xms;

## Accessor attributes.
my %bags               : ATTR( :name<bags>,               :default<undef> );
my %namespace          : ATTR( :name<namespace>,          :default<MAGETabulator> );
my %authority          : ATTR( :name<authority>,          :default<ebi.ac.uk> );
my %expt_accession     : ATTR( :name<expt_accession>,     :default<undef> );
my %in_relaxed_mode    : ATTR( :name<in_relaxed_mode>,    :default<undef> );
my %tabfile_eol_char   : ATTR( :name<eol_char>,           :default<undef> );
my %source_directory   : ATTR( :name<source_directory>,   :default<undef> );

#########################
# Object initialization #
#########################

sub START {

    my ( $self, $id, $args ) = @_;

    # Constructor calls may pass in previously-used bags (e.g. when
    # transferring IDF MAGE to the SDRF parser).
    if ( defined $self->get_bags() ) {
	$self->generate_bag_methods();
    }
    else{
	$self->initialize_bags();
    }

    return;
}

##################
# Public methods #
##################

###################
# Private methods #
###################

sub initialize_bags : RESTRICTED {

    my ( $self ) = @_;

    my %bag_of;

    # These differ from the tab2mage versions of the same, implemented
    # here as OO with an eye to migrating everything to OO design in
    # the future. Currently these methods must comply with the
    # conventions laid down in ArrayExpress::Curator::MAGE.

    # These are used in both IDF and SDRF.
    $bag_of{termsource} = make_container( sub{ $self->new_termsource(@_)    } );
    $bag_of{factor}     = make_container( sub{ $self->new_factor(@_)        } );
    $bag_of{people}     = make_container( sub{ $self->new_person(@_)        } );
    $bag_of{places}     = make_container( sub{ $self->new_organization(@_)  } );
    $bag_of{protocol}   = make_container( sub{ $self->new_protocol(@_)      } );
    $bag_of{parameter}  = make_container( sub{ $self->new_parameter(@_)     } );
    $bag_of{software}   = make_container( sub{ $self->new_protocol_ware(@_) } );
    $bag_of{hardware}   = make_container( sub{ $self->new_protocol_ware(@_) } );

    # These are SDRF only.
    $bag_of{armanuf}           = make_container( \&new_armanuf );
    $bag_of{array}             = make_container( \&new_array );
    $bag_of{arraydesign}       = make_container( sub{ $self->new_arraydesign(@_) } );
    $bag_of{factorvalue}       = make_container( sub{ $self->new_factorvalue(@_) } );
    $bag_of{source}            = make_container( \&new_biosource );
    $bag_of{sample}            = make_container( \&new_biosample, 1 );
    $bag_of{extract}           = make_container( \&new_extract, 1 );
    $bag_of{labeledextract}    = make_container( \&new_labeledextract, 1 );
    $bag_of{label}             = make_container( \&new_labelcompound );
    $bag_of{image}             = make_container( \&new_image );
    $bag_of{pba}               = make_container( sub{ $self->new_physicalbioassay(@_) } );
    $bag_of{mba}               = make_container( \&new_measuredbioassay, 1 );
    $bag_of{dba}               = make_container( \&new_derivedbioassay, 1 );
    $bag_of{extended_pba}      = make_container( sub{ $self->new_physicalbioassay(@_) } );
    $bag_of{datamatrix_mba}    = make_container( \&new_measuredbioassay, 1 );
    $bag_of{datamatrix_dba}    = make_container( \&new_derivedbioassay, 1 );
    $bag_of{mbad}              = make_container( \&new_measuredbioassaydata );
    $bag_of{dbad}              = make_container( \&new_derivedbioassaydata );
    $bag_of{datamatrix_mbad}   = make_container( \&new_measuredbioassaydata );
    $bag_of{datamatrix_dbad}   = make_container( \&new_derivedbioassaydata );
    $bag_of{bam}               = make_container( \&new_bioassaymap );
    $bag_of{qtm}               = make_container( \&new_quantitationtypemap );
    $bag_of{quantitationtype}  = make_container( \&new_quantitationtype );
    $bag_of{qtd}               = make_container( \&new_qtd );
    
    $self->set_bags( \%bag_of );

    $self->generate_bag_methods();

    return \%bag_of;
}

sub generate_bag_methods : RESTRICTED {

    my ( $self ) = @_;

    my $bag = $self->get_bags();

    {   # Create convenience methods for bag access.
	## no critic ProhibitNoStrict ProhibitNoWarnings
	no strict qw(refs);
	no warnings qw(redefine);
	## use critic ProhibitNoStrict ProhibitNoWarnings
	while ( my ( $bagname, $sub ) = each %$bag ) {
	    my $method = "${bagname}_bag";

	    # The shift here corrects the @_ argument array.
	    *{$method} = sub{ my $id = shift; $sub->(@_) };
	}
    }

    return;
}

sub get_id_prefix : RESTRICTED {

    my ( $self ) = @_;

    return sprintf( "%s:%s", $self->get_authority(), $self->get_namespace() );
}

sub get_all_bioassays {

    # Method to retrieve all bioassays of any type.
    my ( $self ) = @_;

    my @bioassays;

    # Add references to all BioAssays
    foreach my $type ( qw(pba
			  mba
			  dba
			  extended_pba
			  datamatrix_mba
			  datamatrix_dba) ) {

	push @bioassays, @{ $self->get_bags->{ $type }->() };
    }

    return(\@bioassays);
}

sub get_all_bioassaydata {

    # Method to retrieve all bioassaydata of any type.
    my ( $self ) = @_;

    my @bioassaydata;

    # Add references to all BioAssayData
    foreach my $type ( qw(mbad
			  dbad
			  datamatrix_mbad
			  datamatrix_dbad) ) {

	push @bioassaydata, @{ $self->get_bags->{ $type }->() };
    }

    return(\@bioassaydata);
}

sub create_ontologyentry {

    # Method takes two mandatory arguments, $category and $value, and
    # four optional arguments ($termsource, $accession, $uri and
    # $db). Returns an OE object. If termsource defined but db is not,
    # termsource is used to look up a suitable db.
    my ( $self, $category, $value, $termsource, $accession, $uri, $db ) = @_;

    return if ( $value =~ $BLANK );

    my $oe = Bio::MAGE::Description::OntologyEntry->new(
	category          => $category,
	value             => $value,
    );

    my $database;
    if ( $db ) {
	$database = $db;
    }
    elsif ( $termsource ) {

	# We need an accession in some form for the OE to be valid. We
	# don't just want to drop the termsource so we fake it
	# here. N.B. this line actually holds for MO in its current form.
	$accession ||= $value;

	# If the Term Source is detectably MO, use some defaults.
	if ( lc($termsource) eq 'mo' || $termsource =~ m/MGED ?Ontology/i ) {
	    $uri       ||= sprintf(
		"http://mged.sourceforge.net/ontologies/MGEDontology.php#%s",
		$value
# FIXME use the following if the OWLdocs ever go production-ready.
#		"http://mged.sourceforge.net/ontologies/MOhtml/%s.html",
#		lc($value)
	    );
	}

	my $args;
	if ( $self->get_in_relaxed_mode() ) {
	    $args = {name       => $termsource,
		     identifier => "ebi.ac.uk:Database:$termsource"};
	}

	$database = $self->termsource_bag( $termsource, $args )
	    or croak(qq{Error: cannot find Term Source Name "$termsource"\n});
    }

    if ( $database ) {

	# Create the actual DB entry.
	my $dbentry = Bio::MAGE::Description::DatabaseEntry->new(
	    accession => $accession,
	    URI       => $uri,
	    database  => $database,
	);
	$oe->setOntologyReference( $dbentry );
    }

    return $oe;
}
    
sub new_termsource : RESTRICTED {

    my ( $self, $args, $obj ) = @_;

    $obj = Bio::MAGE::Description::Database->new(
	identifier => $args->{identifier},
    ) unless $obj;

    if ( $args->{name}    && ! $obj->getName() ) {
	$obj->setName(    $args->{name}    );
    }

    if ( $args->{version} && ! $obj->getVersion() ) {
	$obj->setVersion( $args->{version} );
    }

    if ( $args->{uri}     && ! $obj->getURI() ) {
	$obj->setURI(     $args->{uri}     );
    }

    return $obj;
}

sub new_factor : RESTRICTED {
    
    my ( $self, $args, $obj ) = @_;

    $obj = Bio::MAGE::Experiment::ExperimentalFactor->new() unless $obj;

    # Identifier.
    unless ( $obj->getIdentifier() ) {
	my $identifier
	    = sprintf("%s:%s.%s.%s.ExperimentalFactor",
		      $self->get_id_prefix(),
		      $self->get_expt_accession(),
		      unique_identifier(),
		      $args->{type});
	$obj->setIdentifier($identifier);
    }

    # Name.
    $obj->setName( $args->{name} ) unless $obj->getName();

    # Category OE.
    my $oe = $self->create_ontologyentry(
	'ExperimentalFactorCategory',
	$args->{type},
	$args->{termsource},
	$args->{termaccession},
    );
    $obj->setCategory($oe) unless $obj->getCategory;

    return $obj;
}

sub new_person : RESTRICTED {

    my ( $self, $args, $obj ) = @_;

    $obj = Bio::MAGE::AuditAndSecurity::Person->new() unless $obj;

    $obj->setIdentifier(  $args->{identifier}  ) if $args->{identifier};
    $obj->setName(        $args->{name}        ) if $args->{name};
    $obj->setFirstName(   $args->{firstname}   ) if $args->{firstname};
    $obj->setMidInitials( $args->{midinitials} ) if $args->{midinitials};
    $obj->setLastName(    $args->{lastname}    ) if $args->{lastname};
    $obj->setPhone(       $args->{phone}       ) if $args->{phone};
    $obj->setEmail(       $args->{email}       ) if $args->{email};
    $obj->setFax(         $args->{fax}         ) if $args->{fax};
    $obj->setAddress(     $args->{address}     ) if $args->{address};

    if ( $args->{roles} ) {
	my @newroles;
	my @oldroles = @{ $obj->getRoles() || [] };
	foreach my $value (split /\s*;\s*/, $args->{roles} ) {

	    my $found = first {
		$_->getValue() eq $value;
	    }   @oldroles;
	    unless ( $found ) {
		my $role = $self->create_ontologyentry(
		    'Roles',
		    $value,
		    $args->{termsource},
		    $args->{termaccession},
		);
		push @newroles, $role;
	    }
	}
	$obj->addRoles( @newroles ) if scalar(@newroles);
    }

    my $affiliation;
    if ( $args->{affiliation} ) {
	$affiliation = $self->places_bag(
	    $args->{affiliation},
	    {
		name => $args->{affiliation},
	    },
	);
	$obj->setAffiliation( $affiliation );
    }

    return $obj;
}

sub new_organization : RESTRICTED {

    my ( $self, $args, $obj ) = @_;

    $obj = Bio::MAGE::AuditAndSecurity::Organization->new() unless $obj;

    unless ( $obj->getIdentifier() ) {

	# Only first 50 characters used in identifier creation to
	# avoid ArrayExpress schema problems with over-long ids.
	my $identifier = sprintf(
	    "%s:%s.Organization",
	    $self->get_id_prefix(),
	    substr( $args->{name}, 0, 50 ),
	);

	$obj->setIdentifier($identifier);
    }

    $obj->setName($args->{name}) unless $obj->getName();

    return $obj;
}

sub new_protocol : RESTRICTED {

    my ( $self, $args, $obj ) = @_;

    $obj = Bio::MAGE::Protocol::Protocol->new unless $obj;

    # We allow the SDRF to re-identify protocols if Term Source and
    # Accession are used.
    $obj->setIdentifier($args->{accession}) if $args->{accession};

    if ( $args->{type} && ! $obj->getType() ) {
        my $type = $self->create_ontologyentry(
            'ProtocolType',
	    $args->{type},
	    $args->{termsource},
	    $args->{termaccession},
        );
        $obj->setType($type);
    }
    if ( $args->{name} && ! $obj->getName() ) {
        $obj->setName( $args->{name} );
    }
    if ( $args->{description} && ! $obj->getText() ) {
        $obj->setText( $args->{description} );
    }

    # FIXME more granular update for parameters here?
    if ( $args->{parameters} && ! $obj->getParameterTypes() ) {

        my $parameters = $self->create_parameters(
	    $args->{parameters},
	    $args->{accession},
	    $args->{name},
	);

        $obj->setParameterTypes( $parameters );
    }

    # Create software, hardware, attach to protocol and each other.
    my ( $software, $hardware ) = $self->create_and_add_protocol_wares(
	$obj,
	$args->{software},
	$args->{hardware},
    );

    # FIXME contact is implemented as a NVT association for now.
    if ( $args->{contact} ) {
	my $contact = Bio::MAGE::NameValueType->new(
	    name  => 'Protocol Contact',
	    value => $args->{contact},
	);
	$obj->addPropertySets($contact);
    }

    return $obj;

}

sub create_parameters : RESTRICTED {

    # We need to control protocol accession, authority and namespace
    # at this point, use them in the parameter identifiers. Note that
    # we don't have parameter units at this point; we need to modify
    # these later at the SDRF parsing stage.
    my ( $self, $parameters, $prot_accession, $prot_name ) = @_;

    my $param_namespace =
	sprintf("%s:%s",
		$self->get_id_prefix(),
		$prot_accession);
    my @parameters;
    foreach my $param_name ( split /\s*;\s*/, $parameters ) {
	my $param_id = sprintf("%s.%s.Parameter",
			       $param_namespace,
			       $param_name);
	push(
	    @parameters,
	    $self->parameter_bag(
		"$param_name.$prot_name",
		{
		    identifier => $param_id,
		    name       => $param_name,
		}
	    )
	);
    }

    return \@parameters;
}

sub new_parameter : RESTRICTED {

    my ( $self, $args, $obj ) = @_;

    $obj = Bio::MAGE::Protocol::Parameter->new unless $obj;

    $obj->setIdentifier($args->{identifier}) unless $obj->getIdentifier();

    if ( $args->{name} && ! $obj->getName() ) {
	$obj->setName( $args->{name} );
    }

    # Units are also added at the SDRF parsing stage.
    if ( $args->{unit} ) {
	my $defaultval = Bio::MAGE::Measurement::Measurement->new(
	    unit => $args->{unit},
	);
	$obj->setDefaultValue( $defaultval );
    }

    return $obj;
}

sub new_protocol_ware : RESTRICTED {

    # Create a Software or Hardware object; $class is set during the
    # creation of the %bags attribute hash (the bag knows what it contains).
    my ( $self, $args, $obj ) = @_;

    croak("Error: must supply a 'class' attribute to new_protocol_ware.")
	unless $args->{class};

    my $mageclass = "Bio::MAGE::Protocol::$args->{class}";

    $obj = $mageclass->new unless $obj;

    # Identifier
    $obj->setIdentifier( $args->{identifier} ) unless $obj->getIdentifier();

    # Type (not actually supported by IDF yet; FIXME maybe by
    # inferring from ProtocolType (questionable). This is included for later.
    if ( $args->{type} && ! $obj->getType() ) {
	my $type = $self->create_ontologyentry(
	    $args->{category} || "$args->{class}Type",
	    $args->{type},
	);
	$obj->setType($type);
    }

    # Name
    $obj->setName( $args->{name} ) unless $obj->getName;

    return $obj;
}

sub create_and_add_protocol_wares : RESTRICTED {

    my ( $self, $protocol, $sw_name, $hw_name) = @_;

    my ( $software, $hardware );

    my $ware_namespace =
	sprintf("%s:%s.%s",
		$self->get_id_prefix(),
		$self->get_expt_accession(),
		unique_identifier());

    # FIXME software and hardware type not currently captured by
    # MAGE-TAB specification (as of v1.0).
    if ( $sw_name ) {
	$software = $self->software_bag(
	    $sw_name,
	    {
		identifier => "$ware_namespace.Software",
		name       => $sw_name,
		class      => 'Software',
	    }
	);
	$self->update_object_wares_assnlist( $protocol, $software );
    }
    if ( $hw_name ) {
	$hardware = $self->hardware_bag(
	    $hw_name,
	    {
		identifier => "$ware_namespace.Hardware",
		name       => $hw_name,
		class      => 'Hardware',
	    }
	);
	$self->update_object_wares_assnlist( $protocol, $hardware );
    }

    # Link sw and hw here (checking that they're not already linked).
    if ( $hardware && $software ) {

	# HW to SW is 1:n
	$software->setHardware($hardware) unless $software->getHardware();
	$self->update_object_wares_assnlist( $hardware, $software );
    }

    return ( $software, $hardware );
}

sub update_object_wares_assnlist : RESTRICTED {

    # Update the list of hardware or software associated with an
    # object (protocol or hardware, or conceivably software (to other
    # softwares)) to contain a new entry.
    my ( $self, $object, $ware ) = @_;

    # Figure out which list we're updating from the $ware object
    # passed in.
    my $attribute = ref $ware;
    $attribute =~ s/^.*:://;

    my $getter = "get${attribute}s";
    my $adder  = "add${attribute}s";

    my $found;
    if ( my $preexisting_wares = $object->$getter ) {
        my $new_identifier = $ware->getIdentifier();
        $found = first { $_->getIdentifier() eq $new_identifier }
            @$preexisting_wares;
    }

    $object->$adder($ware) unless $found;

    return;
}

sub new_arraydesign : RESTRICTED {

    my ( $self, $args, $obj ) = @_;

    $obj = Bio::MAGE::ArrayDesign::PhysicalArrayDesign->new()
	unless $obj;

    if ( defined( $args->{accession} ) ) {
	$obj->setIdentifier( $args->{accession} )
	    unless $obj->getIdentifier();
    }

    if ( defined( $args->{termsource} ) ) {

	my $ts_args;
	if ( $self->get_in_relaxed_mode() ) {
	    $ts_args = {name       => $args->{termsource},
		        identifier => "ebi.ac.uk:Database:$args->{termsource}"};
	}

	my $database = $self->termsource_bag( $args->{termsource}, $ts_args )
	    or croak(qq{Error: cannot find Term Source Name "$args->{termsource}"\n});

	my $dbentry = Bio::MAGE::Description::DatabaseEntry->new(
	    accession => $args->{accession},
	    database  => $database,
	);
	my $desc = Bio::MAGE::Description::Description->new(
	    databaseReferences => [ $dbentry ],
	);
	$obj->setDescriptions( [ $desc ] );
    }

    return $obj;
}

sub new_physicalbioassay : RESTRICTED {

    my ( $self, $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssay::PhysicalBioAssay->new unless $obj;

    # Identifier
    $args->{identifier_template} && do {
        my $identifier = "$args->{identifier_template}.PhysicalBioAssay";
        $obj->setIdentifier($identifier) unless $obj->getIdentifier;
    };

    # Name
    $obj->setName($args->{name}) unless $obj->getName();

    # Channels
    # Construct hash listing of old label ids
    my $old_label_ids;
    if ( my $old_channels = $obj->getChannels() ) {
        foreach my $old_ch ( @{ $old_channels } ) {
	    if ( my $old_labels = $old_ch->getLabels() ) {
		foreach my $old_l ( @{ $old_labels } ) {
		    $old_label_ids->{ $old_l->getIdentifier() }++;
		}
            }
        }
    }

    # N.B. only one label per LE supported here (although LEs can have
    # more than one label, and new_labeledextract does support
    # this).
    if ( $args->{label} ) {
	my $dyename;

        # Convert LabelCompound into Channel
        my $dye_id = $args->{label}->getIdentifier();
        unless ( ( $dyename )
		     = ( $dye_id =~ m/^$AE_LABELCOMPOUND_PREFIX(.*)$/ ) ) {
	    $dyename = 'Unknown';
        }

	# Don't add the channel if it's already present.
	unless ( $old_label_ids->{ $dye_id } ) {
	    my $channel = Bio::MAGE::BioAssay::Channel->new(
		identifier => $AE_CHANNEL_PREFIX . $dyename,
		name       => $dyename,
		labels     => [ $args->{label} ],
	    );
	    $obj->addChannels($channel);
	}
    }

    # This is only the case for hyb-level PBAs.
    if ( my $new_bm = $args->{derived_from} ) {

	my $hyb;
	unless ( $hyb = $obj->getBioAssayCreation() ) {
	    $hyb = $self->new_hybridization(
		{
		    identifier_template => $args->{identifier_template},
		    name                => $args->{name},
		    array               => $args->{array},
		    target              => $obj,
		    protocolapps        => $args->{hyb_protocolapps},
		    is_assay            => $args->{is_assay},
		},
	    );
	    $obj->setBioAssayCreation( $hyb );
	}

	# SourceBioMaterialMeasurements - added where the BioMaterials
	# differ (e.g. Cy3, Cy5 labeled extracts).
	my $preexisting_bmms = $hyb->getSourceBioMaterialMeasurements();
	my $new_bm_id        = $new_bm->getIdentifier;
	my $found = first {
	    $_->getBioMaterial()->getIdentifier() eq $new_bm_id
	} @$preexisting_bmms;

	unless ($found) {
	    my $bmm = Bio::MAGE::BioMaterial::BioMaterialMeasurement->new(
		bioMaterial => $new_bm,
	    );
	    $hyb->addSourceBioMaterialMeasurements($bmm);
	}
    }

    if ( my $factorvals = $args->{factor_values} ) {
	foreach my $fv ( @$factorvals ) {
	    $self->add_factorval_to_bioassay( $fv, $obj );
	}
    }

    # BioAssayTreatments; initial PBA creation just creates a dummy
    # BAT (see scan, data for per-channel BATs).  *Don't* update here
    # if there are preexisting BATs.
    unless ( $obj->getBioAssayTreatments() ) {
	$obj->setBioAssayTreatments(
	    [
		$self->new_scan(
		    {
			identifier_template => $args->{identifier_template},
			name                => $args->{name},
			pba                 => $obj,
			target              => $obj,
			protocolapps        => $args->{scan_protocolapps},
		    }
		)
	    ]
	);
    }

    return $obj;
}

sub new_factorvalue : RESTRICTED {

    my ( $self, $args, $obj ) = @_;

    # Generate an empty FV object; Value_assn, Measurement_assn will
    # be added elsewhere.
    unless ( $obj ) {
        $obj = Bio::MAGE::Experiment::FactorValue->new();
    }

    if ( $args->{identifier_template} ) {
	$obj->setIdentifier("$args->{identifier_template}.FactorValue")
	    unless $obj->getIdentifier();
    }
    $obj->setName( $args->{name} ) unless $obj->getName;

    return $obj;
}
    
sub add_factorval_to_bioassay : RESTRICTED {

    my ( $self, $factorval, $bioassay ) = @_;

    # FIXME we should probably be comparing category, value, unit
    # etc. here.
    my $found = first {
	$_->getIdentifier() eq $factorval->getIdentifier()
    } @{ $bioassay->getBioAssayFactorValues() || [] };
    $bioassay->addBioAssayFactorValues( $factorval ) unless $found;

    return;
}

sub new_hybridization : RESTRICTED {

    my ( $self, $args, $obj ) = @_;

    my $identifier;
    unless ( $obj ) {
	if ( $args->{is_assay} ) {
	    $obj = Bio::MAGE::BioAssay::BioAssayCreation->new();
	    $identifier = "$args->{identifier_template}.Assay";
	}
	else {
	    $obj = Bio::MAGE::BioAssay::Hybridization->new();
	    $identifier = "$args->{identifier_template}.Hybridization";
	}
    }

    if ( $args->{identifier_template} ) {
	$obj->setIdentifier( $identifier )
	    unless $obj->getIdentifier();
    }

    $obj->setName(  $args->{name} )                    if $args->{name};
    $obj->setArray( $args->{array} )                   if $args->{array};
    $obj->setPhysicalBioAssayTarget( $args->{target} ) if $args->{target};

    if ( $args->{protocolapps} && scalar( @{ $args->{protocolapps} } ) ) {
	$obj->setProtocolApplications( $args->{protocolapps} );
    }

    return $obj;
}

sub new_scan : RESTRICTED {

    my ( $self, $args, $obj ) = @_;

    $obj = Bio::MAGE::BioAssay::ImageAcquisition->new() unless $obj;

    if ( $args->{identifier_template} ) {
	$obj->setIdentifier("$args->{identifier_template}.ImageAcquisition")
	    unless $obj->getIdentifier();
    }

    $obj->setName(  $args->{name} )           if $args->{name};
    $obj->setPhysicalBioAssay( $args->{pba} ) if $args->{pba};
    $obj->setTarget( $args->{target} )        if $args->{target};

    if ( $args->{protocolapps} && scalar( @{ $args->{protocolapps} } ) ) {
	$obj->setProtocolApplications( $args->{protocolapps} );
    }

    return $obj;
}

sub calculate_eol_char : RESTRICTED {

    my ( $self, $file ) = @_;

    unless ( $self->get_eol_char() ) {
	my ($eols, $eol_char) = check_linebreaks( $self->get_filename() );
	unless ( $eol_char ) {
	    croak(
		sprintf(
		    "Error: Cannot correctly parse linebreaks in file %s"
			. " (%s unix, %s dos, %s mac)\n",
		    $self->get_filename(),
		    $eols->{unix},
		    $eols->{dos},
		    $eols->{mac},
		)
	    );
	}
	$self->set_eol_char( $eol_char );
    }

    if (    ( $self->get_eol_char eq "\015" )
         && ( $Text::CSV_XS::VERSION < 0.27 ) ) {

	# Mac linebreaks not supported by older versions of Text::CSV_XS.
	croak("Error: Mac linebreaks not supported by this version"
	    . " of Text::CSV_XS. Please upgrade to version 0.27 or higher.\n");
    }

    return $self->get_eol_char();
}

sub get_csv_parser {

    my ( $self ) = @_;

    # FIXME consider caching this in a private attribute?
    my $csv_parser = Text::CSV_XS->new(
        {   sep_char    => qq{\t},
            quote_char  => qq{"},                   # default
            escape_char => qq{"},                   # default
            binary      => 1,
            eol         => ( $self->calculate_eol_char() || "\n" ),
	    allow_loose_quotes => 1,
        }
    );

    return $csv_parser;
}

sub raise_error : RESTRICTED {

    my ( $self, @messages ) = @_;

    croak(@messages);
}

sub raise_warning : RESTRICTED {

    my ( $self, @messages ) = @_;

    carp(@messages);

    return;
}

1;
