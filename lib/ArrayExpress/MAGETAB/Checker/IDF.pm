#!/usr/bin/env perl
#
# $Id: IDF.pm 1853 2007-12-13 17:53:43Z tfrayner $

use strict;
use warnings;

package ArrayExpress::MAGETAB::Checker::IDF;

use Class::Std;

use ArrayExpress::Curator::Config qw($CONFIG);

use base qw(ArrayExpress::MAGETAB::IDF);

my %checker : ATTR( :name<checker>, :default<\*STDOUT> );

sub START {

    my ( $self, $id, $args ) = @_;

    unless ( $checker{$id}
	  && $checker{$id}->isa('ArrayExpress::Curator::ExperimentChecker') ) {
	die("Error: Invalid or absent checker object.");
    }

    return;
}

sub read_file {

    my ( $self ) = @_;

    # Parse the IDF file into memory here.
    my $array_of_rows = $self->read_as_arrayref();

    # Check tags for duplicates, make sure that tags are recognized.
    my $idf_hash = $self->validate_arrayref_tags( $array_of_rows );

    # Populate the internal data store with whatever we can recognise;
    # discard the rest, as it has already been reported.
    while ( my ( $tag, $values ) = each %{ $idf_hash } ) {
	if ( my $sub = $self->retrieve_sub( $tag ) ) {

	    # Recognised tag.
	    $sub->( @$values );
	}
    }

    return;
}

# This method overrides that in the superclass, allowing what would
# normally be fatal parse errors to be recorded in the logs.
sub raise_error : RESTRICTED {

    my ( $self, @messages ) = @_;

    my $checker = $self->get_checker();

    my $message = join(q{}, @messages);

    # Make sure the message ends in a single newline.
    $message =~ s/[\r\n]* \z/\n/xms;

    $checker->logprint(
	'error',
	$message,
    );

    $checker->add_error( $CONFIG->get_ERROR_PARSEFAIL() );

    return;
}

1;
