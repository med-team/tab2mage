#!/usr/bin/env perl
#
# Module to provide basic methods used by the expt_check script.
#
# Tim Rayner 2005, ArrayExpress team, European Bioinformatics Institute
#
# $Id: Checker.pm 2069 2008-06-04 14:33:52Z tfrayner $
#

use strict;
use warnings;

=pod

=begin html

    <div><a name="top"></a>
      <table class="layout">
	  <tr>
	    <td class="whitetitle" width="100">
              <a href="../../../index.html">
                <img src="../../MAGETAB_logo.png"
                     border="0" height="50" alt="MAGE-TAB logo"></td>
              </a>
	    <td class="pagetitle">Module detail: Checker.pm</td>
	  </tr>
      </table>
    </div>

=end html

=head1 NAME

ArrayExpress::MAGETAB::Checker - MAGE-TAB experiment checking.

=head1 SYNOPSIS

 use ArrayExpress::MAGETAB::Checker;
 
 my $checker = ArrayExpress::MAGETAB::Checker->new({
     idf => $magetab_idf,
 });
 
 $checker->check();

=head1 DESCRIPTION

This module provides the basic operations needed for checking a
MAGE-TAB document and associated data files.

=head1 OPTIONS

The following options must be used in addition to those provided by the
parent class (see L<ArrayExpress::Curator::ExperimentChecker>):

=over 2

=item C<idf>

The MAGE-TAB IDF filename to be checked.

=back

=head1 TESTS

The following tests are performed by this module, with output
printed to the error and/or report logs:

=over 4

=item B<IDF Contents>

Checks that at least one of each of the following items are present in
the IDF, warning the user if any are missing:

 Experimental Factor
 Contact
 Publication
 Protocol
 SDRF

=item B<Experiment info>

Warns the user if any of the following pieces of experiment annotation
are missing:

 Experiment Design
 Release Date
 Title
 Description

=item B<Term Sources>

Checks all instances of Term Source REFs, and warns the user if an
undeclared Term Source Name has been used. Also notifies the user if a
Term Source declared in the IDF has not been used in the SDRF.

=item B<Experimental Factors>

Checks all SDRF Factor Value columns, and warns the user if an
undeclared Experimental Factor Name has been used. Also notifies the
user if a Experimental Factor declared in the IDF has not been used in
the SDRF.

=item B<Undeclared Experimental Factors>

Checks all material characteristics against the factors described by
the document, alerting the user if any such characteristics vary
during the experiment without having been declared as an experimental
factor.

=item B<Source Characteristics>

Checks that each Source material has at least some annotation. 
This is not, however, a full test of MIAME compliance.

=item B<PubMed ID>

Warns the user if a non-numeric PubMed ID has been entered.

=item B<Submission and release dates>

Warns the user if experiment or release dates are in an incorrect
format.

=item B<Submitter>

Checks that a submitter contact has been provided, and that their last
name and email address have both been included in the IDF.

=item B<Protocols>

Checks that all protocols referenced in the Hybridization spreadsheet 
section are declared in the Protocol section (and vice versa).

=item B<Protocol description length>

Warns if any of the protocol texts seem too brief.

=item B<Protocol usage>

Checks that a protocol has been attached to each step of the 
experiment (biomaterial treatments, hybridization, scanning and normalization).

=item B<Parameters>

Checks that all parameters referenced in the Hybridization spreadsheet 
section are declared in the Protocol section (and vice versa).

=item B<Files>

Confirms that each data file is associated with an array design (i.e.,
Array Design REF or Array Design File), and at least one experimental
factor value.

=item B<Experiment Design Graph>

Creates the links between sample, extract, labeled extract and
hybridization, and writes a biomaterials log file listing the 
numbers of each. If the Graphviz software is installed 
(http://www.graphviz.org) then the script will use the 'dot' 
program to produce a PNG format graph showing how the
various components relate to each other.

=back

Tim Rayner (rayner@ebi.ac.uk), ArrayExpress team, EBI, 2007.

Acknowledgements go to the ArrayExpress curation team for feature
requests, bug reports and other valuable comments.

=begin html

<hr>
<a href="http://sourceforge.net">
  <img src="http://sourceforge.net/sflogo.php?group_id=120325&amp;type=2" 
       width="125" 
       height="37" 
       border="0" 
       alt="SourceForge.net Logo" />
</a>

=end html

=cut

package ArrayExpress::MAGETAB::Checker;

use Carp;
use English qw( -no_match_vars );
use Class::Std;
use List::Util qw(first);
use List::MoreUtils qw(pairwise);
use File::Spec;
use File::Basename;

use ArrayExpress::Curator::Config qw($CONFIG);
use ArrayExpress::Curator::Common qw(
    get_filepath_from_uri
    $RE_EMPTY_STRING
    $RE_COMMENTED_STRING
    $RE_SURROUNDED_BY_WHITESPACE
);

require ArrayExpress::Datafile;
require ArrayExpress::MAGETAB;
require ArrayExpress::MAGETAB::Checker::IDF;
require ArrayExpress::MAGETAB::SDRF;

use ArrayExpress::Curator::Report qw(
    format_description
);

use base qw(ArrayExpress::Curator::ExperimentChecker);

# The entry-point IDF file.
my %idf          : ATTR( :get<idf>,         :init_arg<idf>,    :default<undef> );

# Attributes used for checking combined IDF/SDRF documents.
my %magetab_doc     : ATTR( :name<magetab_doc>,     :default<undef> );
my %idf_filehandle  : ATTR( :name<idf_filehandle>,  :default<undef> );
my %sdrf_filehandle : ATTR( :name<sdrf_filehandle>, :default<undef> );
my %eol_char        : ATTR( :name<eol_char>,        :default<undef> );

# Flag indicating we can overwrite files.
my %clobber      : ATTR( :name<clobber>, :default<0>  );

# Used to store Names defined in the IDF.
my %termsources  : ATTR( :get<termsources>, :set<termsources>, :default<{}> );
my %factors      : ATTR( :get<factors>,     :set<factors>,     :default<{}> );
my %protocols    : ATTR( :get<protocols>,   :set<protocols>,   :default<{}> );
my %parameters   : ATTR( :get<parameters>,  :set<parameters>,  :default<{}> );

# Used to accumulate SDRF info for passing back to the data file
# checks.
my %file_info      : ATTR( :get<file_info>,      :set<file_info>,      :default<[]> );
my %hybridizations : ATTR( :get<hybridizations>, :set<hybridizations>, :default<{}> );
my %scans          : ATTR( :get<scans>,          :set<scans>,          :default<{}> );
my %normalizations : ATTR( :get<normalizations>, :set<normalizations>, :default<{}> );

# Used to store per-row SDRF info.
my %row_files    : ATTR( :get<row_files>,   :set<row_files>,   :default<[]>    );
my %row_array    : ATTR( :get<row_array>,   :set<row_array>,   :default<undef> );
my %row_factors  : ATTR( :get<row_factors>, :set<row_factors>, :default<[]>    );

# Used to check usage of IDF objects in the SDRF.
my %protocols_used   : ATTR( :get<protocols_used>,   :set<protocols_used>,   :default<{}> );
my %parameters_used  : ATTR( :get<parameters_used>,  :set<parameters_used>,  :default<{}> );
my %factors_used     : ATTR( :get<factors_used>,     :set<factors_used>,     :default<{}> );
my %termsources_used : ATTR( :get<termsources_used>, :set<termsources_used>, :default<{}> );

# Used to track Characteristics for each material, to check for consistency.
my %current_material : ATTR( :get<current_material>, :set<current_material>, :default<undef> );
my %char_cache       : ATTR( :get<char_cache>,       :set<char_cache>,       :default<{}>    );
my %reported_cache   : ATTR( :get<reported_cache>,   :set<reported_cache>,   :default<{}>    );

# Used to store the MAGE container hashrefs for e.g. visualization.
my %mage_generator : ATTR( :get<mage_generator>, :set<mage_generator>, :default<undef> );

sub START {
    my ( $self, $id, $args ) = @_;

    unless ( defined( $idf{$id} || defined( $magetab_doc{$id} ) ) ) {
	croak("Error: No IDF filename passed to MAGE-TAB checker.");
    }

    # Localise to CWD, or value of get_log_to_current_dir.
    my $inputfile = $self->get_idf() || $self->get_magetab_doc();
    my $logfile_string = File::Spec->rel2abs($inputfile);
    $logfile_string =~ s/\.\w{3,4}$//;     # strip off the extension
    my ( $vol, $dir, $name ) = File::Spec->splitpath($logfile_string);
    $self->localize_logfiles({
	directory => $dir,
	volume    => $vol,
	name      => $name,
    });

    return;
}

sub get_files_and_annotation : RESTRICTED {

    my ( $self ) = @_;

    print STDOUT ("Running in MAGE-TAB mode...\n");

    $self->logprint( 'error',  "* MAGE-TAB mode *\n\n" );
    $self->logprint( 'report', "* MAGE-TAB mode *\n\n" );

    # Sort out any combined MAGE-TAB IDF+SDRF documents here.
    if ( $self->get_magetab_doc() ) {
	my ( $idf_fh, $sdrf_fh, $eol_char );
	my $sighandler = $SIG{__DIE__};
	delete $SIG{__DIE__};
	local $EVAL_ERROR;
	eval {
	   ( $idf_fh, $sdrf_fh, $eol_char ) =
	       ArrayExpress::MAGETAB->preprocess_magetab_doc(
		   $self->get_magetab_doc(),
	       );
        };
	$SIG{__DIE__} = $sighandler if $sighandler;
	if ( $EVAL_ERROR ) {
	    $self->logprint(
		'error',
		"ERROR: Failed to parse IDF and SDRF section headings: $EVAL_ERROR\n",
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
	    return;
	}
	else {
	    $self->set_idf_filehandle( $idf_fh );
	    $self->set_sdrf_filehandle( $sdrf_fh );
	    $self->set_eol_char( $eol_char );
	}
    }

    # Check the passed IDF/SDRF and set up our list of Datafile
    # objects.

    # Check the document naively before we invoke the main MAGE export
    # parser, which will most likely crash in any case.
    $self->logprint_line( 'error', 'Naive parsing START' );

    $self->parse_naively();

    $self->logprint_line( 'error', 'Naive parsing END' );

    # Attempt to read in the document using the parser. This will fail
    # unless the document is structurally valid.
    $self->logprint_line( 'error', 'Full MAGE-TAB parsing START' );

    # Check if our error level is at ERROR_PARSEFAIL before attempting
    # MAGE creation.
    my ( $filelist, $bags, $mage );
    if ( $self->get_error() & $CONFIG->get_ERROR_PARSEFAIL() ) {
	$self->logprint(
	    'error',
	    "ERROR: Previous checks have determined that a full MAGE-TAB"
	  . " parse will fail. Skipping and attempting to continue.\n",
	);
    }
    else {

	# Attempt to generate MAGE.
	( $filelist, $bags, $mage ) = $self->generate_mage();
    }

    $self->logprint_line( 'error', 'Full MAGE-TAB parsing END' );

    unless ( $mage && $bags ) {

	# If MAGE creation failed, fall back to the naive parse for
	# files to check; hyb, scan and norm ids.
	$filelist = $self->get_files_from_naive_parse();
    }

    # Rewrite Datafile objects which use URIs to reference files.
    unless ( $self->get_skip_data_checks() ) {
	foreach my $file ( @$filelist ) {
	    $self->rewrite_datafile_uri( $file );
	}
    }

    # Keys used to map FGEM column headings to hybs, scans, norms;
    # also stores links to the actual objects as values (PBA for hyb,
    # DBA for norm, BAT for scan; these are checked later for
    # protocolapps).
    my $normalizations  = $self->get_normalizations();
    my $scans           = $self->get_scans();
    my $hybridizations  = $self->get_hybridizations();
    
    $self->logprint_line( 'error', 'ADF parsing START' );
    
    # Populate the $file->get_adf_features() hashrefs within @$filelist
    if ( $self->get_adf_filename || $self->get_array_accession ) {
	$self->cache_user_supplied_arrays( $filelist );
    }
    else {

	# just populate the keys for now
	my %array_designs;
	foreach my $file (@$filelist) {
	    unless ( $file->get_is_exp() ) {
		$array_designs{ $file->get_array_design_id() }++;
	    }
	}

	# populate the array designs hashref
	unless ( $self->get_is_standalone() ) {

	    my @accno_table;
	    foreach my $accno ( keys %array_designs ) {

		# ADF retrieval crashes when AE is down; we trap that here.
		$self->get_ae_arraydesign({
		    accession => $accno,
		});
	    }
	}
    }
    $self->populate_file_arraydesigns( $filelist );
    
    $self->logprint_line( 'error', 'ADF parsing END' );

    # FIXME consider returning $scans as well.
    return ( $filelist, $hybridizations, $normalizations );
}

sub generate_mage : PRIVATE {

    my ( $self ) = @_;

    my ( $filelist, $bags, $mage, $magetab );

    my %common_opts = (
	authority                 => '{AUTHORITY}',
	namespace                 => '{NAMESPACE}',
	expt_accession            => '{ACCESSION}',
	target_directory          => ( $self->get_log_to_current_dir() || q{.} ),
	source_directory          => $self->get_source_directory(),
	is_standalone             => $self->get_is_standalone(),
	qt_filename               => $self->get_qt_filename(),
	include_default_qts       => $self->get_include_default_qts(),
	keep_protocol_accns       => 1,
	skip_datafiles            => 1,
	clobber                   => $self->get_clobber(),
    );

    # Don't propagate fatal errors up the call stack.
    my $sighandler = $SIG{__DIE__};
    delete $SIG{__DIE__};
    local $EVAL_ERROR;

    # Either one or the other of the following should be valid.
    if ( my $idf = $self->get_idf() ) {
	$magetab = ArrayExpress::MAGETAB->new({
	    idf => $idf,
	    %common_opts,
	});

	# Attempt to generate MAGE.
	eval {
	    ( $filelist, $bags, $mage ) = $magetab->parse();
	};
    }
    elsif ( my $doc = $self->get_magetab_doc() ) {
	$magetab = ArrayExpress::MAGETAB->new({
	    magetab_doc => $doc,
	    %common_opts,
	});

	# Attempt to generate MAGE.
	eval {
	    ( $filelist, $bags, $mage ) = $magetab->parse_magetab_doc();
	};
    }
    else {
	croak("ERROR: No IDF or combined MAGE-TAB document to parse.");
    }

    # Reinstate our DIE sighandler.
    $SIG{__DIE__} = $sighandler if $sighandler;

    # Reset our clobber value, based on any changes in the magetab
    # parser value.
    $self->set_clobber( $magetab->get_clobber() );

    if ( $EVAL_ERROR ) {
	$self->logprint(
	    'error',
	    "ERROR: MAGE-TAB document is not structurally valid."
	  . " Parser fails with this error: $EVAL_ERROR\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }
    else {

	# Store this object for later visualization.
	$self->set_mage_generator( $magetab );

	# Record hyb, scan and normalization names for later use in
	# checking DataMatrices.
	my (%hybridization, %scan, %normalization);
	foreach my $pba ( @{ $bags->{'pba'}->() } ) {
	    $hybridization{$pba->getName()} = $pba;

	    # Single-channel scan BATs. We assume that in such cases
	    # the extra two-color PBAs have been pruned and we can
	    # assume a single BAT which represents the Scan event.
	    if ( scalar @{ $pba->getChannels() || [] } == 1 ) {
		foreach my $bat ( @{ $pba->getBioAssayTreatments() || [] } ) {
		    $scan{$bat->getName()} = $bat;
		}
	    }
	}
	foreach my $pba ( @{ $bags->{'extended_pba'}->() } ) {

	    # Two-channel scan BATs. We look for PBAs pointing to
	    # themselves as indication that this is the last PBA in
	    # the PBA -> PBA -> PBA -> MBA chain.
	    foreach my $bat ( @{ $pba->getBioAssayTreatments() || [] } ) {
		if ( my $target = $bat->getTarget() ) {
		    if ( $pba->getIdentifier() eq $target->getIdentifier() ) {

			# End of the PBA chain, must therefore be the Scan PBA.
			$scan{$bat->getName()} = $bat;
		    }
		}
	    }
	}
	foreach my $dba ( @{ $bags->{'dba'}->() } ) {
	    $normalization{$dba->getName()} = $dba;
	}
	$self->set_hybridizations(\%hybridization);
	$self->set_scans(\%scan);
	$self->set_normalizations(\%normalization);

	# Check Labeled Extract - Label association is present.
	$self->check_mage_labeledextracts( $bags->{'labeledextract'}->() );

	# Check annotation of the Sources.
	$self->check_mage_bmchars( $bags->{'source'}->() );

	# Check that all variation in BMCs are also FVs.
	$self->check_bmchars_against_fvs( $bags );

	# FIXME check protocols attached at various stages.
	$self->check_protocol_attachments( $bags );

	# Generate SDRF workflow in biomaterials log.
	$self->print_sdrf_workflow( $bags );
    }

    # $mage should be undef on failure.
    return ( $filelist, $bags, $mage );
}

sub rewrite_datafile_uri : PRIVATE {

    my ( $self, $file ) = @_;

    my $path;
    my $sighandler = $SIG{__DIE__};
    delete $SIG{__DIE__};
    local $EVAL_ERROR;
    eval {
	$path = get_filepath_from_uri(
	    $file->get_path(),
	    $self->get_source_directory(),
	);
    };
    $SIG{__DIE__} = $sighandler if $sighandler;
    if ( $EVAL_ERROR ) {
	$self->logprint(
	    'error',
	    $EVAL_ERROR,
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }
    else {
	$file->set_path( $path );
	$file->set_name( basename( $path ) );
    }
}

sub parse_naively : PRIVATE {

    my ( $self ) = @_;

    # We start with the IDF.
    my $sdrfs = $self->parse_idf_naively();

    # Check we're not about to read an OS-level file when we've been told not to.
    foreach my $sdrf ( @{ $sdrfs } ) {
	if ( File::Spec->file_name_is_absolute($sdrf)
	    && $self->get_safe_filechecks() ) {
	    $self->logprint(
		'error',
		"ERROR: Absolute SDRF path used in safe file checking mode.\n",
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
	}
    }

    # This also populates the files attribute with a list of Datafile
    # objects.
    $self->parse_sdrfs_naively($sdrfs);

    # Rewind any filehandles (not needed for regular IDF/SDRF). Maybe
    # consider pos/tell here FIXME?
    if ( my $idf_fh = $self->get_idf_filehandle() ) {
	seek($idf_fh, 0, 0)
	    or croak("Error rewinding IDF temporary filehandle: $!");
    }
    if ( my $sdrf_fh = $self->get_sdrf_filehandle() ) {
	seek($sdrf_fh, 0, 0)
	    or croak("Error rewinding SDRF temporary filehandle: $!");
    }

    return;
}

sub parse_individual_sdrf_naively : PRIVATE {

    my ( $self, $sdrf_parser ) = @_;

    # Check for the presence of the SDRF, or alternatively a filehandle.
    my $sdrf = $sdrf_parser->get_sdrf();

    # This will crash if the SDRF isn't present, so we eval it.
    my $sdrf_fh;
    eval { $sdrf_fh = $sdrf_parser->get_sdrf_filehandle() };
    
    unless ( $sdrf_fh ) {
	$self->logprint(
	    'error',
	    qq{ERROR: Unable read SDRF file "$sdrf".\n},
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );

	return;
    }	

    my $BLANK      = $sdrf_parser->get_blank_regexp();
    my $csv_parser = $sdrf_parser->get_csv_parser();

    # Scan past any empty or commented lines to get to the header row.
    my $headings;
    my $header_string;
    HEADERLINE:
    while ( $headings = $csv_parser->getline( $sdrf_fh ) ) {
	$header_string = join( q{}, @$headings ) if $headings;
        last HEADERLINE if (   $header_string
	                    && $header_string !~ $RE_EMPTY_STRING
	                    && $header_string !~ $RE_COMMENTED_STRING );
    }

    # Check we've parsed to the end of the file.
    my ( $error, $mess ) = $csv_parser->error_diag();
    unless ( $headings || $error == 2012 ) {    # 2012 is the Text::CSV_XS EOF code.
	$self->logprint(
	    'error',
	    sprintf(
		"Error in tab-delimited format: %s. Bad input was:\n\n%s\n",
		$mess,
		$csv_parser->error_input(),
	    ),
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
	return;
    }

    if ( $header_string && $headings && scalar @{ $headings } ) {
	my $rowchecks = $self->check_sdrf_header( $headings );

	# Read through the rest of the file, checking values as appropriate.
	FILE_LINE:
	while ( my $row = $csv_parser->getline( $sdrf_fh ) ) {

	    # Reset our per-row trackers.
	    $self->set_row_array(undef);
	    $self->set_row_files([]);
	    $self->set_row_factors([]);

	    # Skip empty lines.
	    my $line = join( q{}, @$row );
	    next FILE_LINE if ( $line =~ $RE_EMPTY_STRING );

	    # Allow hash comments (FIXME NOT IN SPEC).
	    next FILE_LINE if ( $line =~ $RE_COMMENTED_STRING );

	    # Strip surrounding whitespace from each element.
	    foreach my $element ( @$row ) {
		$element =~ s/$RE_SURROUNDED_BY_WHITESPACE/$1/xms;
	    }

	    # Actually run the row-level checks here. FIXME we might
	    # want to run certain tests even if the value is blank;
	    # e.g. characteristics consistency checking.
	    for ( my $i = 0; $i < scalar @$row; $i++ ) {
		unless ( $row->[$i] =~ $BLANK ) {
		    my $sub = $rowchecks->[$i];
		    if ( ref $sub eq 'CODE' ) {
			$sub->( $row->[$i], $headings->[$i] );
		    }
		}
	    }

	    $self->check_file_info();
	}

	# Check we've parsed to the end of the file.
	my ( $error, $mess ) = $csv_parser->error_diag();
	unless ( $error == 2012 ) {    # 2012 is the Text::CSV_XS EOF code.
	    $self->logprint(
		'error',
		sprintf(
		    "Error in tab-delimited format: %s. Bad input was:\n\n%s\n",
		    $mess,
		    $csv_parser->error_input(),
		),
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
	    return;
	}
    }
    else {
	my $sdrf = $sdrf_parser->get_sdrf() || 'section';
	$self->logprint(
	    'error',
	    qq{ERROR: Unable to parse header line of SDRF $sdrf.\n},
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    return;
}

sub parse_sdrfs_naively : PRIVATE {

    my ( $self, $sdrfs ) = @_;

    $self->logprint_line( 'error', '  Naive SDRF parsing START  ' );

    if ( $sdrfs && scalar @{ $sdrfs } ) {
	foreach my $sdrf ( @{ $sdrfs } ) {
	    my $sdrf_parser = ArrayExpress::MAGETAB::SDRF->new({
		sdrf             => $sdrf,
		source_directory => $self->get_source_directory(),
	    });
	    $self->parse_individual_sdrf_naively( $sdrf_parser );
	}
    }
    elsif ( my $sdrf_fh = $self->get_sdrf_filehandle() ) {
	my $sdrf_parser = ArrayExpress::MAGETAB::SDRF->new({
	    sdrf_filehandle  => $sdrf_fh,
	    eol_char         => $self->get_eol_char(),
	    source_directory => $self->get_source_directory(),
	});
	$self->parse_individual_sdrf_naively( $sdrf_parser );
    }
    else {
	$self->logprint(
	    'error',
	    "ERROR: No SDRF files or annotation available.\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    $self->check_idf_term_usage();
    
    $self->logprint_line( 'error', '  Naive SDRF parsing END  ' );

    return;
}

sub check_char_against_cache : PRIVATE {

    my ( $self, $value, $heading ) = @_;

    my $cache    = $self->get_char_cache();
    my $reported = $self->get_reported_cache();
    my $material = $self->get_current_material();

    # Drop out if there's no material for these characteristics (this
    # is a separate error, and will be reported elsewhere).
    return unless defined($material);

    # Beware autovivification here (of the $material keys).
    if (     exists $cache->{ $material }{ $heading }
        && ! exists $reported->{ $material }{ $heading } ){

	unless ( $cache->{ $material }{ $heading } eq $value ) {
	    $self->logprint(
		'error',
		qq{ERROR: The material "$material" has inconsistent }
	      . qq{characteristics ("$heading") between SDRF rows.\n},
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEBAD );
	    $reported->{ $material }{ $heading } = $value;
	}
    }
    else {
	$cache->{ $material }{ $heading } = $value;
    }

    $self->set_char_cache( $cache );
    $self->set_reported_cache( $reported );

    return;
}

sub check_sdrf_protocol : PRIVATE {

    my ( $self, $name ) = @_;

    # Check we have a declared protocol.
    unless ( $self->is_valid_protocol( $name ) ) {
	$self->logprint(
	    'error',
	    qq{Warning: Unknown Protocol REF in SDRF: "$name" (must be declared in IDF).\n},
	);

	# FIXME this should really be PARSEFAIL, but for Protocol REF
	# with associated Term Source REF this would lead to false
	# positives. PARSEFAIL will be set if the main parser fails.
	$self->add_error( $CONFIG->get_ERROR_INNOCENT() );
    }

    # Record the protocol usage for later.
    $self->get_protocols_used()->{$name}++;

    return;
}

sub check_sdrf_termsource : PRIVATE {

    my ( $self, $name ) = @_;

    # Check this is a valid term source.
    unless ( $self->is_valid_termsource( $name ) ) {
	$self->logprint(
	    'error',
	    qq{ERROR: Unknown Term Source REF in SDRF: "$name" (must be declared in IDF).\n},
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    # Record the termsource usage for later.
    $self->get_termsources_used()->{$name}++;

    return;
}

sub check_sdrf_factorvalue : PRIVATE {

    my ( $self, $name ) = @_;

    # Check this is a valid factor name.
    unless ( $self->is_valid_factor( $name ) ) {
	$self->logprint(
	    'error',
	    qq{ERROR: Unknown Experimental Factor in SDRF: "$name" (must be declared in IDF).\n},
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    # Record the factor usage for later.
    $self->get_factors_used()->{$name}++;

    return;
}

sub check_sdrf_parameter : PRIVATE {

    my ( $self, $name ) = @_;

    # Check this is a valid parameter.
    unless ( $self->is_valid_parameter( $name ) ) {
	$self->logprint(
	    'error',
	    qq{ERROR: Unknown Parameter in SDRF: "$name" (must be declared in IDF).\n},
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    # Record the parameter usage for later.
    $self->get_parameters_used()->{$name}++;

    return;
}

sub add_hybridization : PRIVATE {

    my ( $self, $name ) = @_;

    $hybridizations{ident $self}{$name}++;

    return;
}

sub add_scan : PRIVATE {

    my ( $self, $name ) = @_;

    $scans{ident $self}{$name}++;

    return;
}

sub add_normalization : PRIVATE {

    my ( $self, $name ) = @_;

    $normalizations{ident $self}{$name}++;

    return;
}

sub check_sdrf_header : PRIVATE {

    my ( $self, $headings ) = @_;

    # Used in multiple sets of tests, we define these regexps here only once.
    my $characteristics_re = qr/ Characteristics?             # heading prefix
				 [ ]*
				 \[ [ ]* ([^\]]+?) [ ]* \]    # bracketed term
				 /ixms;
    my $factorvalue_re     = qr/ Factor [ ]* Values?          # heading prefix
				 [ ]*
				 \[ [ ]* ([^\]]+?) [ ]* \]    # bracketed term
				 (
				   [ ]*
				   \( [ ]* ([^\)]+?) [ ]* \)  # optional parens term
			         )?
				 /ixms;
    my $parameter_re       = qr/ Parameter [ ]* Values?       # heading prefix
				 [ ]*
				 \[ [ ]* ([^\]]+?) [ ]* \]    # bracketed term
				 /ixms;
    my $unit_re            = qr/ Unit                         # heading prefix
				 [ ]*
				 \[ [ ]* ([^\]]+?) [ ]* \]    # bracketed term
				 /ixms;

    # Define the tests to be run on header values. Any subs here will
    # be passed match values $1 and $2 from the key regexps. FIXME
    # consider validating the ontology terms held in Characteristics[]
    # and Unit[] column headers.
    my %header_test = (
	$characteristics_re => 1,
	$factorvalue_re     => sub { $self->check_sdrf_factorvalue(@_) },
	$parameter_re       => sub { $self->check_sdrf_parameter(@_) },
	$unit_re            => 1,
    );

    # Define any tests and/or aggregation methods to be run on the
    # column values. Any subs here will be passed each value from the
    # column in turn, and the full column header itself.
    my %recognized = (

	qr/ Source    [ ]*              Names? /ixms
	    => sub { $self->set_current_material(shift) },

	qr/ Sample    [ ]*              Names? /ixms
	    => sub { $self->set_current_material(shift) },

	qr/ Extract   [ ]*              Names? /ixms
	    => sub { $self->set_current_material(shift) },

	qr/ Labell?ed [ ]* Extract [ ]* Names? /ixms
	    => sub { $self->set_current_material(shift) },

	qr/ Term [ ]* Accession [ ]* Numbers?  /ixms => 1,
	qr/ Providers?                         /ixms => 1,
	qr/ Material [ ]* Types?               /ixms => 1,
	qr/ Technology [ ]* Types?             /ixms => 1,
	qr/ Labels?                            /ixms => 1,
	qr/ Descriptions?                      /ixms => 1,
	qr/ Performers?                        /ixms => 1,
	qr/ Dates?                             /ixms => 1,
	qr/ Image [ ]* Files?                  /ixms => 1,

	qr/ Hybridi[sz]ation [ ]* Names? /ixms
	    => sub { $self->add_hybridization(@_) },

	qr/ Assay [ ]* Names? /ixms
	    => sub { $self->add_hybridization(@_) },

	qr/ Scan [ ]* Names? /ixms
	    => sub { $self->add_scan(@_) },

	qr/ Normali[sz]ation [ ]* Names? /ixms
	    => sub { $self->add_normalization(@_) },

	qr/ Array [ ]* Design [ ]* Files? /ixms
	    => sub { $self->protest_unsupported('Array Design File') },

	qr/ Array [ ]* Data [ ]* Files? /ixms
	    => sub { $self->add_row_file('raw', @_) },

	qr/ Derived [ ]* Array [ ]* Data [ ]* Files? /ixms
	    => sub { $self->add_row_file('normalized', @_) },

	qr/ Array [ ]* Data [ ]* Matrix [ ]* Files? /ixms
	    => sub { $self->add_row_matrix_file($CONFIG->get_RAW_DM_FILE_TYPE(), @_) },

	qr/ Derived [ ]* Array [ ]* Data [ ]* Matrix [ ]* Files? /ixms
	    => sub { $self->add_row_matrix_file($CONFIG->get_FGEM_FILE_TYPE(), @_) },

	# Only check Term Source REF if the optional namespace is omitted.
	qr/ Term [ ]* Source [ ]* REFs?  # main heading only
	   /ixms
	       => sub { $self->check_sdrf_termsource(@_) },

	qr/ Term [ ]* Source [ ]* REFs?  # main heading, and
	    [ ]* :[^\t\"]+                # optional namespace
	    /ixms => 1,

	# Only check Protocol REF if the optional namespace is omitted.
	qr/ Protocol [ ]* REFs?          # main heading only
	    /ixms
	       => sub { $self->check_sdrf_protocol(@_) },

	qr/ Protocol [ ]* REFs?          # main heading, and
	    [ ]* :[^\t\"]+                # optional namespace       
	    /ixms => 1,

	qr/ Array [ ]* Design [ ]* REFs? # main heading
	    ( [ ]* :[^\t\"]+ )?           # optional namespace       
	    /ixms
	       => sub { $self->set_row_array(shift) },

	qr/ Comments?                    # heading prefix
	    [ ]*
	    \[ [ ]* ([^\]]+?) [ ]* \]    # bracketed term
	    /ixms => 1,


	# See above for these regexps.
	$characteristics_re => sub { $self->check_char_against_cache(@_) },
	$factorvalue_re     => sub { $self->add_row_factor(@_) },
	$parameter_re       => 1,
	$unit_re            => 1,
    );

    # An array of either CODE refs to run on a given column, or
    # other true scalar values.
    my @rowchecks;

    foreach my $col ( @{ $headings || [] } ) {
	if ( my $result = first { $col =~ m/\A [ ]* $_ [ ]* \z/xms } keys %recognized ) {
	    push @rowchecks, $recognized{$result};

	    # Check Parameter Value, Factor Value headings here.
	    my @args;
	    if ( my $key = first { @args = ($col =~ $_) } keys %header_test ) {
		my $test = $header_test{$key};
		if ( ref $test eq 'CODE' ) {
		    $test->(@args);
		}
	    }
	}
	else {
	    push @rowchecks, 0;
	    $self->logprint(
		'error',
		qq{ERROR: Unrecognized SDRF column heading: "$col".\n},
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
	}
    }

    return \@rowchecks;
}

sub parse_idf_naively : PRIVATE {

    my ( $self ) = @_;

    $self->logprint_line( 'error', '  Naive IDF parsing START  ' );

    my $idf_checker = ArrayExpress::MAGETAB::Checker::IDF->new({
	idf              => $self->get_idf(),
	checker          => $self,
	source_directory => $self->get_source_directory(),

	# magetab_doc-specific options. These should be undef for
	# regular IDFs.
	idf_filehandle => $self->get_idf_filehandle(),
	eol_char       => $self->get_eol_char(),

	expt_accession => 'DUMMY',
    });

    # Read the file in, performing some basic checks on structure.
    eval {
	$idf_checker->read_file();
    };
    if ( $EVAL_ERROR ) {
	$self->logprint(
	    'error',
	    "Error reading IDF format: $EVAL_ERROR",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    # Start the checks. These should now be mainly content, not
    # structure.

    # First, simply check on the existence of required annotation.
    $self->check_required_idf_annotation($idf_checker);

    # Capture the Term Source Names for later checking (this obviously
    # needs to happen before any such checking takes place).
    $self->populate_termsources($idf_checker);

    # Experiment designs, dates, description.
    $self->check_experiment_annotation($idf_checker);

    # Publications.
    $self->check_publications($idf_checker);

    # Submitters.
    $self->check_submitters($idf_checker);

    # Protocols.
    $self->check_protocols($idf_checker);

    # Experimental Factors.
    $self->check_factors($idf_checker);

    $self->logprint_line( 'error', '  Naive IDF parsing END  ' );

    return $idf_checker->get_sdrfs();
}
    
sub check_required_idf_annotation : PRIVATE {

    my ( $self, $idf_checker ) = @_;

    my %required_list = (
	'Experimental Factors'  => 'get_factors',
	'Contacts (Persons)'    => 'get_people',
	'Publications'          => 'get_publications',
	'Protocols'             => 'get_protocols',
    );
    while ( my ( $needed_items, $method ) = each %required_list ) {
	unless ( scalar( @{ $idf_checker->$method } ) ) {
	    $self->logprint(
		'error',
		"Warning: IDF provides no $needed_items.\n",
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
	}
    }

    # SDRFs are a special case - they're a real problem if missing,
    # but not if this is a combined IDF+SDRF document.
    unless ( scalar( @{ $idf_checker->get_sdrfs() } )
		 || $self->get_magetab_doc() ) {
	$self->logprint(
	    'error',
	    "ERROR: IDF references no SDRF documents.\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
    }

    # FIXME these hashes should perhaps instead be lightweight
    # objects? (re-implement in MAGETAB::IDF superclass).
    my $expt_hash = $idf_checker->get_experiment();

    $self->check_required_expt_info( $expt_hash );

    my $people = $idf_checker->get_people();
    if ( scalar @{ $people } ) {
	$self->check_required_person_info( $people );
    }

    return;
}

sub check_required_expt_info : PRIVATE {

    my ( $self, $expt_hash ) = @_;

    my %required_expt_info = (
	'Release Date'    => 'releasedate',
	'Title'           => 'title',
	'Description'     => 'description',
    );
    while ( my ( $needed_info, $key ) = each %required_expt_info ) {
	if ( ! defined( $expt_hash->{ $key } ) ) {
	    $self->logprint(
		'error',
		"Warning: IDF provides no Experiment $needed_info.\n",
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
	}
	elsif ( my ($type) = ($needed_info =~ /\A (\w*) [ ]* date \z/ixms) ) {
	    $self->check_date_format( $expt_hash->{ $key }, $type );
	}
    }

    return;
}

sub check_required_person_info : PRIVATE {

    my ( $self, $people ) = @_;

    # Info required for all contacts.  N.B. submitter-specific checks
    # are made later, in check_submitters().
    my %required_info = (
	'Last Name'   => 'lastname',
	'Affiliation' => 'affiliation',
    );

    foreach my $person ( @{ $people } ) {

	# Find something to use to identify the Person.
	my $name =  $person->{'lastname'}
	         || $person->{'email'}
		 || '**UNNAMED CONTACT**';

	# Check required info.
	while ( my ( $needed_info, $key ) = each %required_info ) {
	    if ( ! defined( $person->{ $key } ) ) {
		$self->logprint(
		    'error',
		    qq{Warning: IDF provides no $needed_info for Person "$name".\n},
		);

		# FIXME - we may want to upgrade this if truly required
		# info is being checked for; currently any requirement
		# for Affiliation is a bit wishy-washy.
		$self->add_error( $CONFIG->get_ERROR_INNOCENT() );
	    }
	}
    }

    return;
}

sub populate_termsources : PRIVATE {

    my ( $self, $idf_checker ) = @_;

    my %termsourcename;
    foreach my $termsource ( @{ $idf_checker->get_termsources() } ) {

	$termsourcename{$termsource->{'name'}}++
	    if defined($termsource->{'name'});

	# Term Source without file risks creating meaningless MAGE
	# Database objects.
	unless ( $termsource->{'file'} ) {
	    $self->logprint(
		'error',
		sprintf(
		    "Warning: Term Source %s has no File information.\n",
		    ($termsource->{'name'} || q{}),
		),
	    );
	    $self->add_error($CONFIG->get_ERROR_PARSEBAD());
	}
    }
    $self->set_termsources(\%termsourcename);

    return;
}

sub check_experiment_annotation : PRIVATE {

    my ( $self, $idf_checker ) = @_;

    # Experiment Designs is an arrayref.
    my $expt_hash = $idf_checker->get_experiment();
    my $designs  = $expt_hash->{'design'};
    if ( defined $designs && scalar @{ $designs || [] } ) {

	# Collect ExperimentDesignType terms for DW checking.
	$self->add_expt_designs( @{ $designs } );
    }
    else {
	$self->logprint(
	    'error',
	    "Warning: IDF provides no Experimental Design.\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
    }

    # Design term sources.
    my $design_termsources = $expt_hash->{'termsource'};
    if ( defined $design_termsources ) {
	foreach my $ts ( @{ $design_termsources || [] } ) {
	    unless( $self->is_valid_termsource( $ts ) ) {
		$self->logprint(
		    'error',
		    qq{Error: Experimental Design Term Source "$ts" is not declared in the IDF.\n},
		);
		$self->add_error($CONFIG->get_ERROR_PARSEFAIL());
	    }
	}
    }

    # Experiment date format also needs checking.
    if ( defined ( $expt_hash->{'experimentdate'} ) ) {
	$self->check_date_format( $expt_hash->{'experimentdate'}, q{} );
    }

    # Print the experiment description to the report log.
    if ( defined ( $expt_hash->{'description'} ) ) {
	format_description(
	    $expt_hash->{'description'},
            $self->log_fh('report'),
	);
    }

    my %typemap = (
	'Quality Control Type' => 'get_qualitycontroltypes',
	'Replicate Type'       => 'get_replicatetypes',
	'Normalization Type'   => 'get_normalizationtypes',
    );
    while ( my ( $typeclass, $method ) = each %typemap ) {
	foreach my $type ( @{ $idf_checker->$method } ) {
	    unless ( $type->{'type'} ) {
		$self->logprint(
		    'error',
		    "Warning: $typeclass missing value in IDF.\n",
		);
		$self->add_error($CONFIG->get_ERROR_PARSEBAD());
	    }
	    if ( my $ts = $type->{'termsource'} ) {
		unless( $self->is_valid_termsource( $ts ) ) {
		    $self->logprint(
			'error',
			qq{Error: $typeclass Term Source "$ts" is not declared in the IDF.\n},
		    );
		    $self->add_error($CONFIG->get_ERROR_PARSEFAIL());
		}
	    }
	}
    }

    return;
}

sub check_publications : PRIVATE {

    my ( $self, $idf_checker ) = @_;
    
    # Check for numeric PubMed ID.
    foreach my $publication ( @{ $idf_checker->get_publications() } ) {
	if (   defined ($publication->{'pubmedid'})
	    && $publication->{'pubmedid'} =~ m/\D/ ) {
	    $self->logprint(
		'error',
		"Warning: PubMed ID contains non-numeric characters: ",
		$publication->{'pubmedid'}, "\n",
	    );
	    $self->add_error( $CONFIG->get_ERROR_INNOCENT() );
	}
	if ( my $ts = $publication->{'termsource'} ) {
	    unless( $self->is_valid_termsource( $ts ) ) {
		$self->logprint(
		    'error',
		    qq{Error: Publication Status Term Source "$ts" is not declared in the IDF.\n},
		);
		$self->add_error($CONFIG->get_ERROR_PARSEFAIL());
	    }
	}
    }

    return;
}

sub check_submitters : PRIVATE {

    my ( $self, $idf_checker ) = @_;

    my $submitters_found;
    foreach my $contact ( @{ $idf_checker->get_people() } ) {
	if (   defined($contact->{'roles'})
	    && $contact->{'roles'} =~ /\b submitter \b/xms ) {

	    $submitters_found++;
	    unless ( $contact->{'lastname'} ) {
		$self->logprint(
		    'error',
		    "Warning: Experiment submitter has no last name.\n",
		);
		$self->add_error( $CONFIG->get_ERROR_INNOCENT() );
	    }
	    unless ( $contact->{'email'} ) {
		$self->logprint(
		    'error',
		    "Warning: Submitter email address is missing.\n",
		);
		$self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
	    }
	}
	if ( my $ts = $contact->{'termsource'} ) {
	    unless( $self->is_valid_termsource( $ts ) ) {
		$self->logprint(
		    'error',
		    qq{Error: Person Roles Term Source "$ts" is not declared in the IDF.\n},
		);
		$self->add_error($CONFIG->get_ERROR_PARSEFAIL());
	    }
	}
    }
    unless ( $submitters_found ) {
	$self->logprint(
	    'error',
	    "Warning: No submitters provided in IDF.\n",
	);
	$self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
    }

    return;
}

sub check_protocols : PRIVATE {

    my ( $self, $idf_checker ) = @_;

    # Check protocol descriptions.
    my (%protocolname, %parametername);
    foreach my $protocol ( @{ $idf_checker->get_protocols() } ) {

	$protocolname{$protocol->{'name'}}++ if defined($protocol->{'name'});

	if ( $protocol->{'description'} ) {
	    unless ( length( $protocol->{'description'} ) > 50 ) {
		$self->logprint(
		    'error',
		    sprintf(
			qq{Warning: Description for %s protocol "%s" is too short.\n},
			( $protocol->{'type'} || q{} ),
			( $protocol->{'name'} || q{} ),
		    ),
		);
		$self->add_error( $CONFIG->get_ERROR_MIAME() );
	    }
	}
	else {
	    $self->logprint(
		'error',
		sprintf(
		    qq{Warning: Description is missing for %s protocol "%s".\n},
		    ( $protocol->{'type'} || q{} ),
		    ( $protocol->{'name'} || q{} ),
		),
	    );
	    $self->add_error( $CONFIG->get_ERROR_MIAME() );
	}
	if ( $protocol->{'parameters'} ) {
	    foreach my $name ( split /\s*;\s*/, $protocol->{'parameters'} ) {

		# We store the protocol name if we can, so that we can
		# try and match parameters to protocols later. NOT
		# IMPLEMENTED FIXME. N.B. this won't work for
		# identically named parameters in separate protocols
		# anyway (which is legal). FIXME.
		$parametername{$name} = ($protocol->{'name'} || 1)
		    if defined $name;
	    }
	}
	if ( my $ts = $protocol->{'termsource'} ) {
	    unless ( $self->is_valid_termsource( $ts ) ) {
		$self->logprint(
		    'error',
		    qq{Error: Protocol Term Source "$ts" is not declared in the IDF.\n},
		);
		$self->add_error($CONFIG->get_ERROR_PARSEFAIL());
	    }
	}
    }
    $self->set_protocols(\%protocolname);
    $self->set_parameters(\%parametername);

    return;
}

sub check_factors : PRIVATE {

    my ( $self, $idf_checker ) = @_;

    my %factorname;
    foreach my $factor ( @{ $idf_checker->get_factors() } ) {

	$factorname{$factor->{'name'}}++ if defined($factor->{'name'});

	unless ( $factor->{'type'} ) {
	    $self->logprint(
		'error',
		sprintf(
		    qq{Warning: Experimental Factor "%s" has no Type.\n},
		    ( $factor->{'name'} || q{} ),
		),
	    );
	    $self->add_error($CONFIG->get_ERROR_PARSEBAD());
	}
	if ( my $ts = $factor->{'termsource'} ) {
	    unless ( $self->is_valid_termsource( $ts ) ) {
		$self->logprint(
		    'error',
		    qq{Error: Experimental Factor Term Source "$ts" is not declared in the IDF.\n},
		);
		$self->add_error($CONFIG->get_ERROR_PARSEFAIL());
	    }
	}
    }
    $self->set_factors(\%factorname);

    return;
}

sub is_valid_termsource : PRIVATE {

    my ( $self, $name ) = @_;

    return $termsources{ident $self}{$name} ? 1 : 0;
}

sub is_valid_factor : PRIVATE {

    my ( $self, $name ) = @_;

    return $factors{ident $self}{$name} ? 1 : 0;
}

sub is_valid_protocol : PRIVATE {

    my ( $self, $name ) = @_;

    return $protocols{ident $self}{$name} ? 1 : 0;
}

sub is_valid_parameter : PRIVATE {

    my ( $self, $name ) = @_;

    return $parameters{ident $self}{$name} ? 1 : 0;
}

sub add_row_file : PRIVATE {

    my ( $self, $type, $name ) = @_;

    my %files = map { $_->{'name'} => $_ } @{ $self->get_row_files() };

    $files{$name} = { 'name' => $name, 'type' => $type };

    $self->set_row_files( [ values %files ] );

    return;
}

sub add_row_matrix_file : PRIVATE {

    my ( $self, $type, $name ) = @_;

    my %files = map { $_->{'name'} => $_ } @{ $self->get_row_files() };

    my $mage_ba_data;

    if ( $type eq $CONFIG->get_FGEM_FILE_TYPE() ) {
	require Bio::MAGE::BioAssayData::DerivedBioAssayData;
	$mage_ba_data = Bio::MAGE::BioAssayData::DerivedBioAssayData->new();
    }
    elsif ( $type eq $CONFIG->get_RAW_DM_FILE_TYPE() ) {
	require Bio::MAGE::BioAssayData::MeasuredBioAssayData;
	$mage_ba_data = Bio::MAGE::BioAssayData::MeasuredBioAssayData->new();
    }
    else {
	croak("Error: unrecognized file type: $type.");
    }

    $files{$name} = {
	'name'    => $name,
	'type'    => $type,
	'ba_data' => $mage_ba_data,
    };

    $self->set_row_files( [ values %files ] );

    return;
}

sub add_row_factor : PRIVATE {

    my ( $self, $value ) = @_;

    my %factors = map { $_ => 1 } @{ $self->get_row_factors() };

    $factors{$value} = 1;

    $self->set_row_factors( [ keys %factors ] );

    return;
}

sub get_files_from_naive_parse : PRIVATE {

    my ( $self ) = @_;

    my @filelist;
    foreach my $info ( @{ $self->get_file_info() } ) {

	my $path;
	if ( my $dir = $self->get_source_directory() ) {
	    $path = File::Spec->catfile( $dir, $info->{'name'} );
	}
	else {
	    $path = $info->{'name'};
	}

	# N.B. this will get rewritten later to handle URIs.
	my $file = ArrayExpress::Datafile->new({
	    path            => $path,
	    name            => $info->{'name'},
	    data_type       => $info->{'type'},
	    array_design_id => ($info->{'array'} || 'Unknown'),
	});

	foreach my $factor ( @{ $info->{'factors'} || [] } ) {

	    # FIXME category not easily determined at this point.
	    $file->add_factor_value('DUMMY', $factor);
	}

	# Data matrices are distinguished from ordinary FGEMs by
	# having a mage_badata attribute. FIXME consider adding CDF
	# info here as an NVT of the badata object.
	if ( my $obj = $info->{'ba_data'} ) {
	    $file->set_mage_badata( $obj );
	}
	push @filelist, $file;
    }

    return \@filelist;
}

sub check_file_info : PRIVATE {

    # Read the data from row_files, arrays, factors, check for
    # consistency, and store it in %fileinfo for later.

    my ( $self ) = @_;

    foreach my $info ( @{ $self->get_row_files() } ) {

	if ( my $array = $self->get_row_array() ) {
	    $info->{'array'} = $array;
	}
	else {
	    $self->logprint(
		'error',
		qq{WARNING: Data file "$info->{name}" not associated with any array designs.\n},
	    );
	    $self->add_error( $CONFIG->get_ERROR_PARSEBAD() );
	}

	my @factors = @{ $self->get_row_factors() };
	if ( scalar @factors ) {
	    $info->{'factors'} = \@factors;
	}
	else {
	    $self->logprint(
		'error',
		qq{Warning: Data file "$info->{name}" not associated with any factor values.\n},
	    );
	    $self->add_error( $CONFIG->get_ERROR_MIAME() );
	}

	$self->add_file_info( $info );
    }

    return;
}

sub add_file_info : PRIVATE {

    my ( $self, $info ) = @_;

    my %files = map { $_->{'name'} => $_ } @{ $self->get_file_info() };

    $files{$info->{'name'}} = $info;

    $self->set_file_info( [ values %files ] );

    return;
}

sub check_idf_term_usage : PRIVATE {

    my ( $self ) = @_;

    # Check that Term Sources, Experimental Factors, Protocols and
    # Parameters declared in the IDF are all used in the SDRF.

    my %check = (
	'Term Source'         => { 'idf'  => 'get_termsources',
				   'sdrf' => 'get_termsources_used', },
	'Experimental Factor' => { 'idf'  => 'get_factors',
				   'sdrf' => 'get_factors_used', },
	'Protocol'            => { 'idf'  => 'get_protocols',
				   'sdrf' => 'get_protocols_used', },
	'Parameter'           => { 'idf'  => 'get_parameters',
				   'sdrf' => 'get_parameters_used', },
    );

    while ( my ($item, $methods) = each %check ) {
	my $idf_terms  = $methods->{idf};
	my $sdrf_terms = $methods->{sdrf};
	foreach my $term ( keys %{ $self->$idf_terms } ) {
	    unless ( $self->$sdrf_terms()->{$term} ) {
		$self->logprint(
		    'error',
		    qq{Warning: $item Name "$term" is declared in the IDF but not used in the SDRF.\n},
		);
		$self->add_error( $CONFIG->get_ERROR_INNOCENT() );
	    }
	}
    }

    return;
}

sub protest_unsupported : PRIVATE {

    my ( $self, $feature ) = @_;

    $self->logprint(
	'error',
	qq{ERROR: The feature "$feature" is unsupported at this time.\n},
    );
    $self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );

    return;
}

sub print_sdrf_workflow : PRIVATE {

    my ( $self, $bags ) = @_;

    # keys are from Tab2MAGE::create_bags()
    my @classes = (
        { 'source'            => 'Sources'               },
        { 'sample'            => 'Samples'               },
        { 'extract'           => 'Extracts'              },
        { 'labeledextract'    => 'Labeled extracts'      },
        { 'pba'               => 'Hybridizations'        },
        { 'mbad'              => 'Raw data files'        },
        { 'dba'               => 'Normalizations'        },
        { 'dbad'              => 'Normalized data files' },
    );

    $self->generate_workflow_from_bags( $bags, [ @classes ] );

    return;
}

sub check_protocol_attachments : PRIVATE {

    my ( $self, $bags ) = @_;

    foreach my $bm_type qw(sample extract labeledextract) {
	$self->check_mage_treatments_protapps(
	    $bags->{$bm_type}->(),
	    $bm_type,
	);
    }

    $self->check_mage_hyb_protapps( $bags->{'pba'}->() );

    # This needs to support one and two-color scans, but we've already
    # gathered all the relevant BATs while generating MAGE.
    $self->check_mage_scan_protapps( [ values %{ $self->get_scans() } ] );
    $self->check_mage_norm_protapps( $bags->{'dbad'}->() );

    return;
}

sub check_mage_scan_protapps : PRIVATE {

    # Check all passed BioAssayTreatments have protocol apps attached
    # to them. This is MAGE-TAB specific, and we're only using it here
    # to check on Scan protocols.
    my ( $self, $bats ) = @_;

    foreach my $bat ( @$bats ) {
	my $found;
	foreach my $pa ( @{ $bat->getProtocolApplications() || [] } ) {
	    $found++ if $pa->getProtocol();
	}
	unless ( $found ) {
	    $self->logprint(
		'error',
		sprintf(
		    qq{Warning: Scan "%s" lacks a scanning protocol.\n},
		    $bat->getName(),
		),
	    );
	    $self->add_error( $CONFIG->get_ERROR_MIAME() );
	}
    }

    return;
}

sub check_mage_norm_protapps : PRIVATE {

    # Check all DBADs have a protocol attached to them. This is
    # MAGE-TAB specific.
    my ( $self, $dbads ) = @_;

    foreach my $dbad ( @$dbads ) {
	my $found;
	if ( my $txn = $dbad->getProducerTransformation() ) {
	    foreach my $pa ( @{ $txn->getProtocolApplications() || [] } ) {
		$found++ if $pa->getProtocol();
	    }
	}
	unless ( $found ) {
	    $self->logprint(
		'error',
		sprintf(
		    qq{Warning: Derived Array Data "%s" lacks a normalization protocol.\n},
		    $dbad->getName(),
		),
	    );
	    $self->add_error( $CONFIG->get_ERROR_MIAME() );
	}
    }

    return;
}

sub check_bmchars_against_fvs : PRIVATE {

    my ( $self, $bags ) = @_;

    # Generate a list of FV OE category => value pairs
    my @fv_catvals;
    foreach my $ef ( @{ $bags->{'factor'}->() } ) {
	foreach my $fv ( @{ $ef->getFactorValues() || [] } ) {
	    my ( $category, $value );
	    if ( my $fvvalue = $fv->getValue() ) {

		# Regular FV Value OE.
		$category = $fvvalue->getCategory();
		$value    = $fvvalue->getValue();
	    }
	    elsif ( my $measurement = $fv->getMeasurement() ) {

		# FV Measurements.
		my $efc = $ef->getCategory();
		$category = $efc->getValue();

		# CamelCase the term.
		$category =~ s/^(.)/uc($1)/e;
		$category =~ s/_(.)/uc($1)/ge;
		$category =~ s/_//;
		
		$value    = $measurement->getValue();
	    }
	    push @fv_catvals, { $category => $value };
	}
    }

    # Generate a list of BMC OE category => value pairs.
    foreach my $type ( qw(source sample extract labeledextract) ) {
	my @bmc_catvals;
	foreach my $material ( @{ $bags->{$type}->() || [] } ) {
	    foreach my $char (
		@{ $material->getCharacteristics() || [] } ) {
		my ( $category, $value );
		if ( $char->getCategory() eq $char->getValue() ) {

		    # Handle nested OEs (e.g. BMC measurements).
		    my $flattened = $self->flatten_nested_oe($char);
		    $category = $flattened->getCategory();
		    $value    = $flattened->getValue();
		}
		else {

		    # Regular BMC OEs.
		    $category = $char->getCategory();
		    $value    = $char->getValue();
		}
		push @bmc_catvals, { $category => $value };
	    }
	}
	$self->compare_fvs_to_bmcs(\@fv_catvals, \@bmc_catvals);
    }

    return;
}

sub flatten_nested_oe : PRIVATE {

    my ( $self, $oe ) = @_;

    # Reduce a nested OE to a flattened form in which everything but
    # the value is discarded. Relies on the value being coded as a
    # has_value property.

    croak("Error: OntologyEntry is not nested")
	unless ( $oe->getCategory() eq $oe->getValue() );

    my $category = $oe->getCategory();
    my $value    = $self->find_nested_oe_value( $oe );

    my $flattened;
    if ( defined $value ) {
	require Bio::MAGE::Description::OntologyEntry;
	$flattened = Bio::MAGE::Description::OntologyEntry->new(
	    category => $category,
	    value    => $value,
	);
    }

    return $flattened;
}

sub find_nested_oe_value : PRIVATE {

    my ( $self, $oe ) = @_;

    # Given a nested OE, return the first has_value value encountered
    # by recursing through the tree.

    # Beware this is very MO-specific FIXME?
    return $oe->getValue() if ( $oe->getCategory() eq 'has_value'
			     && $oe->getCategory() ne $oe->getValue() );

    foreach my $child ( @{ $oe->getAssociations() || [] } ) {
	if ( my $value = $self->find_nested_oe_value( $child ) ) {
	    return $value;
	}
    }

    return;
}

sub create_data_matrix_mage : PRIVATE {

    my ( $self, $filelist, $magetab ) = @_;

    FILE:
    foreach my $file ( @{ $filelist || [] } ) {
	if (  $file->get_data_type() eq $CONFIG->get_FGEM_FILE_TYPE()
	   || $file->get_data_type() eq $CONFIG->get_RAW_DM_FILE_TYPE() ) {

	    # Sort out any data matrix links here. These are processed
	    # after all the per-hyb objects have been created.
	    require ArrayExpress::MAGETAB::DataMatrix;

	    my $dm = ArrayExpress::MAGETAB::DataMatrix->new({
		datafile    => $file,
		id_template => 'DUMMY',
		mage_helper => $magetab,
	    });
	    my $sighandler = $SIG{__DIE__};
	    delete $SIG{__DIE__};
	    local $EVAL_ERROR;
	    eval{
		$dm->create_mage();
	    };
	    $SIG{__DIE__} = $sighandler if $sighandler;
	    if ($EVAL_ERROR) {
		my $err_message = sprintf(
		    "Error parsing data matrix %s (%s); graph visualization will omit this file.\n",
		    $file->get_name(),
		    $EVAL_ERROR,
		);
		warn($err_message);
		$self->logprint(
		    'error',
		    $err_message,
		);
		$self->add_error( $CONFIG->get_ERROR_PARSEFAIL() );
		last FILE;
	    }
	}
    }
}

sub visualize_experiment : RESTRICTED {

    my ( $self, $filelist ) = @_;

    my $magetab = $self->get_mage_generator();

    unless ( defined($magetab) && $magetab->can('get_bags') ) {
	$self->logprint(
	    'error',
	    "Warning: MAGE objects unavailable for visualization. Skipping.\n",
	);
	return;
    }

    my $bags = $magetab->get_bags();

    # Parse any data matrix headers into MAGE objects.
    unless ( $self->get_skip_data_checks() ) {
	$self->create_data_matrix_mage( $filelist, $magetab );
    }

    # Create the dotfile (and PNG if GraphViz installed)
    my $output_base = $self->get_idf() || $self->get_magetab_doc();
    $output_base =~ s/\.\w{3}$// if $output_base;
    $output_base ||= 'magetab';    # Just in case

    if ( defined($self->get_log_to_current_dir) ) {
        my $output_dir = $self->get_log_to_current_dir || q{.};
	my ($vol, $dir, $name) = File::Spec->splitpath($output_base);
	$output_base = File::Spec->catfile($output_dir, $name);
    }

    # we'll just use the default font here.
    require ArrayExpress::Curator::Visualize;
    ArrayExpress::Curator::Visualize->import(
	qw(dot_and_png)
    );
    my $classes = {
        'source'            => 'Source',
        'sample'            => 'Sample',
        'extract'           => 'Extract',
        'labeledextract'    => 'Labeled Extract',
        'pba'               => 'Hybridization',
        'mba'               => 'Feature Extraction',
        'datamatrix_mba'    => 'Array Data',
        'dba'               => 'Normalization',
        'datamatrix_dba'    => 'Array Data',
    };
    $self->set_clobber(
        dot_and_png(
	    $bags,
	    $output_base,
	    undef,
	    $self->get_clobber(),
	    $classes,
	)
    );

    return;
}

1;
