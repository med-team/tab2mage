#!/usr/bin/env perl
#
# Module providing IDF parsing functions to the ArrayExpress MAGE-TAB
# implementation.
#
# Tim Rayner, 2007, EMBL-EBI Microarray Informatics Team
#
# $Id: IDF.pm 2069 2008-06-04 14:33:52Z tfrayner $

package ArrayExpress::MAGETAB::IDF;

use strict;
use warnings;

use Class::Std;
use Carp;
use List::Util qw(first);
use Bio::MAGE qw(:ALL);
use Readonly;

use ArrayExpress::Curator::MAGE qw(
    unique_identifier
    make_container
);
use ArrayExpress::Curator::Common qw(
    get_filepath_from_uri
    $RE_EMPTY_STRING
    $RE_COMMENTED_STRING
    $RE_SURROUNDED_BY_WHITESPACE
);

use base qw(ArrayExpress::MAGETAB::TabFile);

# Accessor attributes.
my %idf                : ATTR( :name<idf>,                :default<undef> );
my %idf_filehandle     : ATTR( :init_arg<idf_filehandle>, :default<undef> );
my %dispatch           : ATTR( :name<dispatch>,           :default<{}>    );
my %sdrfs              : ATTR( :name<sdrfs>,              :default<[]>    );
my %protocol_accession_service : ATTR( :name<protocol_accession_service>, :default<undef> );

# Singleton data.
my %experiment         : ATTR( :get<experiment>,          :default<{}> );

# Grouped data.
my %factor             : ATTR( :get<factors>,             :default<[]> );
my %person             : ATTR( :get<people>,              :default<[]> );
my %publication        : ATTR( :get<publications>,        :default<[]> );
my %protocol           : ATTR( :get<protocols>,           :default<[]> );
my %termsource         : ATTR( :get<termsources>,         :default<[]> );
my %qualitycontrol     : ATTR( :get<qualitycontroltypes>, :default<[]> );
my %replicate          : ATTR( :get<replicatetypes>,      :default<[]> );
my %normalization      : ATTR( :get<normalizationtypes>,  :default<[]> );
my %comment            : ATTR( :get<comments>,            :default<{}> );

Readonly my $COMMENT_TAG => qr/\A \s* Comment \s* \[ ([^\]]+) \] \s* \z/ixms;

#########################
# Object initialization #
#########################

sub START {

    my ( $self, $id, $args ) = @_;

    # If we have a filename, great. If all we have is a filehandle
    # then eol_char *must* be set because check_linebreaks can't be
    # used. Linebreak checking must be the responsibility of the
    # caller when filehandles are used.
    unless (     $self->get_idf()
	    || ( $self->get_idf_filehandle() && $self->get_eol_char() ) ) {
	confess("Error: MAGETAB::IDF object requires either filename or (filehandle and eol_char) attributes.");
    }

    unless ( $self->get_expt_accession() ) {
	confess("Error: MAGETAB::IDF object requires expt_accession to be set.");
    }

    # This dispatch table is used to validate IDF tags and to populate
    # internal data structures prior to MAGE generation.
    $self->set_dispatch({
	qr/Investigation *Title/i
	    => sub{ $self->add_singleton_datum(\%experiment, 'title',          @_) },
	qr/Date *Of *Experiment/i
	    => sub{ $self->add_singleton_datum(\%experiment, 'experimentdate', @_) },
	qr/Public *Release *Date/i
	    => sub{ $self->add_singleton_datum(\%experiment, 'releasedate',    @_) },
	qr/Experiment *Description/i
	    => sub{ $self->add_singleton_datum(\%experiment, 'description',    @_) },

	qr/Experimental *Designs?/i
	    => sub{ $self->add_singleton_data(\%experiment, 'design',     @_) },
	qr/Experimental *Designs? *Term *Source *REF/i
	    => sub{ $self->add_singleton_data(\%experiment, 'termsource', @_) },
	qr/Experimental *Designs? *Term *Accession *Numbers?/i
	    => sub{ $self->add_singleton_data(\%experiment, 'termaccession', @_) },

	qr/Experimental *Factor *Names?/i
	    => sub{ $self->add_grouped_data(\%factor, 'name',       @_) },
	qr/Experimental *Factor *Types?/i
	    => sub{ $self->add_grouped_data(\%factor, 'type',       @_) },
	qr/Experimental *Factor *(Types?)? *Term *Source *REF/i
	    => sub{ $self->add_grouped_data(\%factor, 'termsource', @_) },
	qr/Experimental *Factor *(Types?)? *Term *Accession *Numbers?/i
	    => sub{ $self->add_grouped_data(\%factor, 'termaccession', @_) },

	qr/Person *Last *Names?/i
	    => sub{ $self->add_grouped_data(\%person, 'lastname',    @_) },
	qr/Person *First *Names?/i
	    => sub{ $self->add_grouped_data(\%person, 'firstname',   @_) },
	qr/Person *Mid *Initials?/i
	    => sub{ $self->add_grouped_data(\%person, 'midinitials', @_) },
	qr/Person *Emails?/i
	    => sub{ $self->add_grouped_data(\%person, 'email',       @_) },
	qr/Person *Phones?/i
	    => sub{ $self->add_grouped_data(\%person, 'phone',       @_) },
	qr/Person *Fax(es)?/i
	    => sub{ $self->add_grouped_data(\%person, 'fax',         @_) },
	qr/Person *Address(es)?/i
	    => sub{ $self->add_grouped_data(\%person, 'address',     @_) },
	qr/Person *Affiliations?/i
	    => sub{ $self->add_grouped_data(\%person, 'affiliation', @_) },
	qr/Person *Roles?/i
	    => sub{ $self->add_grouped_data(\%person, 'roles',       @_) },
	qr/Person *Roles? *Term *Source *REF/i
	    => sub{ $self->add_grouped_data(\%person, 'termsource',  @_) },
	qr/Person *Roles? *Term *Accession *Numbers?/i
	    => sub{ $self->add_grouped_data(\%person, 'termaccession',  @_) },

	qr/Quality *Control *Types?/i
	    => sub{ $self->add_grouped_data(\%qualitycontrol, 'type',       @_) },
	qr/Quality *Control *(Types?)? *Term *Source *REF/i
	    => sub{ $self->add_grouped_data(\%qualitycontrol, 'termsource', @_) },
	qr/Quality *Control *(Types?)? *Term *Accession *Numbers?/i
	    => sub{ $self->add_grouped_data(\%qualitycontrol, 'termaccession', @_) },
	qr/Replicate *Types?/i
	    => sub{ $self->add_grouped_data(\%replicate,      'type',       @_) },
	qr/Replicate *(Types?)? *Term *Source *REF/i
	    => sub{ $self->add_grouped_data(\%replicate,      'termsource', @_) },
	qr/Replicate *(Types?)? *Term *Accession *Numbers?/i
	    => sub{ $self->add_grouped_data(\%replicate,      'termaccession', @_) },
	qr/Normali[sz]ation *Types?/i
	    => sub{ $self->add_grouped_data(\%normalization,  'type',       @_) },
	qr/Normali[sz]ation *(Types?)? *Term *Source *REF/i
	    => sub{ $self->add_grouped_data(\%normalization,  'termsource', @_) },
	qr/Normali[sz]ation *(Types?)? *Term *Accession *Numbers?/i
	    => sub{ $self->add_grouped_data(\%normalization,  'termaccession', @_) },
 
	qr/PubMed *IDs?/i
	    => sub{ $self->add_grouped_data(\%publication, 'pubmedid',   @_) },
	qr/Publication *DOIs?/i
	    => sub{ $self->add_grouped_data(\%publication, 'doi',        @_) },
	qr/Publication *Authors? *Lists?/i
	    => sub{ $self->add_grouped_data(\%publication, 'authorlist', @_) },
	qr/Publication *Titles?/i
	    => sub{ $self->add_grouped_data(\%publication, 'title',      @_) },
	qr/Publication *Status/i
	    => sub{ $self->add_grouped_data(\%publication, 'status',     @_) },
	qr/Publication *Status *Term *Source *REF/i
	    => sub{ $self->add_grouped_data(\%publication, 'termsource', @_) },
	qr/Publication *Status *Term *Accession *Numbers?/i
	    => sub{ $self->add_grouped_data(\%publication, 'termaccession', @_) },

	qr/Protocol *Names?/i
	    => sub{ $self->add_grouped_data(\%protocol, 'name',        @_) },
	qr/Protocol *Types?/i
	    => sub{ $self->add_grouped_data(\%protocol, 'type',        @_) },
	qr/Protocol *Descriptions?/i
	    => sub{ $self->add_grouped_data(\%protocol, 'description', @_) },
	qr/Protocol *Parameters?/i
	    => sub{ $self->add_grouped_data(\%protocol, 'parameters',  @_) },
	qr/Protocol *Hardwares?/i
	    => sub{ $self->add_grouped_data(\%protocol, 'hardware',    @_) },
	qr/Protocol *Softwares?/i
	    => sub{ $self->add_grouped_data(\%protocol, 'software',    @_) },
	qr/Protocol *Contacts?/i
	    => sub{ $self->add_grouped_data(\%protocol, 'contact',     @_) },
	qr/Protocol *(Types?)? *Term *Source *REF/i
	    => sub{ $self->add_grouped_data(\%protocol, 'termsource',  @_) },
	qr/Protocol *(Types?)? *Term *Accession *Numbers?/i
	    => sub{ $self->add_grouped_data(\%protocol, 'termaccession',  @_) },

	qr/Term *Source *Names?/i
	    => sub{ $self->add_grouped_data(\%termsource, 'name',     @_) },
	qr/Term *Source *Files?/i
	    => sub{ $self->add_grouped_data(\%termsource, 'file',     @_) },
	qr/Term *Source *Versions?/i
	    => sub{ $self->add_grouped_data(\%termsource, 'version',  @_) },

	qr/SDRF *Files?/i
	    => sub{ $self->add_sdrfs( @_) },
    });

    # ArrayExpress-specific tweak; AE wants these NVTs added
    # regardless of whether they're specified or not. This is a
    # departure from the MAGE-TAB specification, but can be easily
    # deactivated by deleting the following two lines:
    $self->add_comment('AEExperimentDisplayName', q{});
    $self->add_comment('SecondaryAccession',      q{});

    return;
}

##################
# Public methods #
##################

sub parse {

    my ( $self ) = @_;

    # Parse the IDF file into memory here.
    my $array_of_rows = $self->read_as_arrayref();

    # Check tags for duplicates, make sure that tags are recognized.
    my $idf_hash = $self->validate_arrayref_tags($array_of_rows);

    # Populate the IDF object's internal data structures.
    while ( my ($tag, $values) = each %$idf_hash ) {
	$self->dispatch($tag, @$values);
    }

    # FIXME store this somewhere?
    my ( $mage, $experiment ) = $self->generate_mage();

    return ( $mage, $experiment, $self->get_sdrfs() );
}

###################
# Private methods #
###################

sub generate_mage : PRIVATE {

    my ( $self ) = @_;

    # Databases.
    $self->create_termsources();

    # Protocols.
    foreach my $protocol_data ( @{ $self->get_protocols() } ) {
	$self->create_protocol($protocol_data);
    }

    my $experiment = $self->create_experiment();

    # MAGE top-level object.
    my $mage = Bio::MAGE->new();

    $mage->add_objects( [ $experiment ] );

    foreach my $obj_type ( qw(termsource protocol people) ) {
	my $method = "${obj_type}_bag";
	$mage->add_objects( $self->$method() );
    }

    return ( $mage, $experiment );
}

sub create_termsources : PRIVATE {

    my ( $self ) = @_;

    my @termsources;
    TS_DATA:
    foreach my $ts_data ( @{ $self->get_termsources() } ) {

	# FIXME the Database authority:namespace tag hard-coded at the
	# moment.
	unless( defined( $ts_data->{name} ) ) {
	    if ( scalar( grep { defined $ts_data->{$_} } qw(version file) ) ) {
	        $self->raise_error("Error: Found a Term Source without a Name.\n");
	    }
	    else {
	        next TS_DATA;
	    }
	}
	my $identifier =
	    sprintf("%s:%s",
		    'ebi.ac.uk:Database',
		    $ts_data->{name});
	my $termsource = $self->termsource_bag(
	    $ts_data->{name},
	    {
		identifier => $identifier,
		name       => $ts_data->{name},
		version    => $ts_data->{version},
		uri        => $ts_data->{file},
	    }
	);
	push @termsources, $termsource;
    }

    return \@termsources;
}

sub create_experiment : PRIVATE {

    my ( $self ) = @_;

    my $factors      = $self->create_factors();
    my $people       = $self->create_people();
    my $publications = $self->create_publications();
    my $design       = $self->create_design($factors);

    my $description = Bio::MAGE::Description::Description->new(
	text                    => $self->get_experiment()->{description},
	bibliographicReferences => $publications,
    );

    my $comments = $self->create_comments();
    my $dates    = $self->create_dates();

    my $experiment = Bio::MAGE::Experiment::Experiment->new(
	name              => $self->get_experiment()->{title},
	identifier        => $self->get_expt_accession(),
	experimentDesigns => [$design],
	providers         => $people,
	descriptions      => [$description],
	propertySets      => [@{ $comments }, @{ $dates }],
    );

    return $experiment;
}

sub create_comments : PRIVATE {

    my ( $self ) = @_;

    my @comments;
    while (my ($name, $value) = each %{ $self->get_comments() } ) {
	my $comment = Bio::MAGE::NameValueType->new(
	    name  => $name,
	    value => $value, 
	);
	push @comments, $comment;
    }

    return \@comments;
}

sub create_dates : PRIVATE {

    my ( $self ) = @_;

    # Validate the dates here FIXME.
    # NVT names are hard-coded for now FIXME.
    my @dates;
    if ( my $date = $self->get_experiment()->{releasedate} ) {
	push @dates, Bio::MAGE::NameValueType->new(
	    name  => 'ArrayExpressReleaseDate',
	    value => $date,
	);
    }
    if ( my $date = $self->get_experiment()->{experimentdate} ) {
	push @dates, Bio::MAGE::NameValueType->new(
	    name  => 'ExperimentDate',
	    value => $date,
	);
    }

    return \@dates;
}
    
sub create_design : PRIVATE {

    my ( $self, $factors ) = @_;

    my @types;
    foreach my $value ( @{ $self->get_experiment()->{design} } ) {
	my $termsource = shift ( @{ $self->get_experiment()->{termsource} } );
	my $termaccno  = shift ( @{ $self->get_experiment()->{termaccession} } );
	my $oe = $self->create_ontologyentry(
	    'ExperimentDesignType',
	    $value,
	    $termsource,
	    $termaccno,
	);
	push @types, $oe;
    }

    # Quality Control, Normalization and Replicate Types.
    my $qc_description = $self->create_exptdescriptiontype(
	'QualityControlDescriptionType',
	$self->get_qualitycontroltypes(),
    );
    my $norm_description = $self->create_exptdescriptiontype(
	'NormalizationDescriptionType',
	$self->get_normalizationtypes(),
    );
    my $repl_description = $self->create_exptdescriptiontype(
	'ReplicateDescriptionType',
	$self->get_replicatetypes(),
    );

    my $design = Bio::MAGE::Experiment::ExperimentDesign->new(
	types                     => \@types,
	experimentalFactors       => $factors,
    );

    $design->setQualityControlDescription( $qc_description )
	if $qc_description;
    $design->setNormalizationDescription( $norm_description )
	if $norm_description;
    $design->setReplicateDescription( $repl_description )
	if $repl_description;

    return $design;
}

sub create_exptdescriptiontype : PRIVATE {

    # Used to generate OE-containing Descriptions for QC,
    # normalization and replicate types to attach to ExperimentDesign.
    my ( $self, $category, $typelist ) = @_;

    my @types;
    foreach my $type_data ( @{ $typelist } ) {
	my $type = $self->create_ontologyentry(
	    $category,
	    $type_data->{type},
	    $type_data->{termsource},
	    $type_data->{termaccession},
	);
	push @types, $type;
    }

    my $description;
    if ( scalar @types ) {
	$description = Bio::MAGE::Description::Description->new(
	    annotations => \@types,
	);
    }

    return $description;
}
    
sub create_factors : PRIVATE {

    my ( $self ) = @_;

    my @factors;

    FACTOR_DATA:
    foreach my $factor_data ( @{ $self->get_factors() } ) {

	unless( defined( $factor_data->{name} ) ) {
	    if ( scalar( grep { defined $factor_data->{$_} }
			     qw(type termsource termaccession) ) ) {
	        $self->raise_error("Error: Found an Experimental Factor without a Name.\n");
	    }
	    else {
	        next FACTOR_DATA;
	    }
	}
	my $factor = $self->factor_bag(
	    $factor_data->{name}, $factor_data
	);
	push @factors, $factor;
    }

    return \@factors;
}

sub create_people : PRIVATE {

    my ( $self ) = @_;

    my @people;
    foreach my $p_data ( @{ $self->get_people() } ) {

	my @nameparts;
	foreach my $field qw(firstname midinitials lastname) {
	    push @nameparts, $p_data->{$field} if $p_data->{$field};
	}
	unless( scalar @nameparts ) {
	    croak(
		"Error: Found a Person without First Name, Last Name or Mid Initials.\n"
	    );
	}
	$p_data->{identifier} = sprintf("%s:%s.Person",
				$self->get_id_prefix(),
				join(q{ }, @nameparts));

	my $person = $self->people_bag(
	    $p_data->{identifier}, $p_data
	);

	push @people, $person;
    }

    return \@people;
}

sub create_publications : PRIVATE {

    my ( $self ) = @_;

    # FIXME PubMed database is hard-coded for the moment.
    my $pubmed_db = Bio::MAGE::Description::Database->new(
	name       => 'Entrez PubMed',
	identifier => 'ebi.ac.uk:Database:pubmed',
	URI        => 'http://www.ncbi.nlm.nih.gov/entrez/query.fcgi?db=PubMed',
    );

    my @publications;
    foreach my $article_data ( @{ $self->get_publications() } ) {

	my (@parameters, @nvts, @pubmed_accns);

	# FIXME MAGE-TAB v1.0 does not capture publication type
	# (e.g. journal_article). Deferred until a later release.

	if ( $article_data->{pubmedid} ) {
	    push @pubmed_accns, Bio::MAGE::Description::DatabaseEntry->new(
		accession => $article_data->{pubmedid},
		database  => $pubmed_db,
	    );
	}

        # Nowhere to specify this in MAGE-TAB, but MAGE requires at
        # least one parameter so we add this.
        my $pubtype = $self->create_ontologyentry(
            'PublicationType',
            'journal_article',
        );
        push @parameters, $pubtype;

	# FIXME "PublicationStatus" not in MO yet.
	if ( $article_data->{status} ) {
	    my $status = $self->create_ontologyentry(
		'PublicationStatus',
		$article_data->{status},
		$article_data->{termsource},
		$article_data->{termaccession},
	    );
	    push @parameters, $status;
	}

	if ( $article_data->{doi} ) { 
	    my $doi = Bio::MAGE::NameValueType->new(
		name  => 'DOI',
		value => $article_data->{doi},
	    );
	    push @nvts, $doi;
	}

	my $publication = Bio::MAGE::BQS::BibliographicReference->new(
	    authors      => $article_data->{authorlist},
	    title        => $article_data->{title},
	    accessions   => \@pubmed_accns,
	    parameters   => \@parameters,
	    propertySets => \@nvts,
	);

	push @publications, $publication;
    }

    return \@publications;
}

sub create_protocol : PRIVATE {

    my ( $self, $protocol_data ) = @_;

    unless( defined( $protocol_data->{name} ) ) {
        if ( scalar( grep { defined $protocol_data->{$_} }
		     qw(type
			description
			parameters
			hardware
			software
			contact
			termsource
			termaccession) ) ) {
	    $self->raise_error("Error: Found a Protocol without a Name.\n");
	}
	else {
	    return;
	}
    }

    # First, give the protocol_accession_service callback the
    # opportunity to reassign protocol accession.
    $protocol_data->{accession} = $self->get_protocol_accession(
	$protocol_data->{name},
    );

    # Then generate the protocol. This also handles parameters,
    # software and hardware.
    my $protocol = $self->protocol_bag(
	$protocol_data->{name}, $protocol_data
    );

    return $protocol;
}
    
sub get_protocol_accession : PRIVATE {

    my ( $self, $accession ) = @_;

    if ( my $service = $self->get_protocol_accession_service() ) {
	$accession = $service->(
	    $accession,
	    $self->get_expt_accession(),
	);
    }
    
    return $accession;
}

sub read_as_arrayref : RESTRICTED {

    # Method to parse the IDF object file into an array of
    # arrayrefs. This method uses Text::CSV_XS to parse tab-delimited
    # text.

    my ( $self ) = @_;

    # First, determine the file linebreak type and generate a CSV
    # parser object.
    my $csv_parser = $self->get_csv_parser();

    # This is still required for Text::CSV_XS.
    local $/ = $self->calculate_eol_char();

    # Open the file
    my $fh = $self->get_idf_filehandle();

    my (@rows, $larry);

    FILE_LINE:
    while ( $larry = $csv_parser->getline($fh) ) {

        # Skip empty lines.
        my $line = join( q{}, @$larry );
        next FILE_LINE if ( $line =~ $RE_EMPTY_STRING );

        # Allow hash comments (FIXME NOT IN SPEC).
        next FILE_LINE if ( $line =~ $RE_COMMENTED_STRING );

	# Strip surrounding whitespace from each element.
	foreach my $element ( @$larry ) {
	    $element =~ s/$RE_SURROUNDED_BY_WHITESPACE/$1/xms;
	}

	# Strip off empty trailing values.
	my $end_value;
	until ( defined($end_value) && $end_value !~ /\A \s* \z/xms ) {
	    $end_value = pop(@$larry);
	}
	push @$larry, $end_value;

	# Reset empty strings to undefs.
	foreach my $value ( @$larry ) {
	    undef($value) if ( defined($value) && $value eq q{} );
	}

	push @rows, $larry;
    }

    # Check we've parsed to the end of the file.
    my ( $error, $mess ) = $csv_parser->error_diag();
    unless ( $error == 2012 ) {    # 2012 is the Text::CSV_XS EOF code.
	croak(
	    sprintf(
		"Error in tab-delimited format: %s. Bad input was:\n\n%s\n",
		$mess,
		$csv_parser->error_input(),
	    ),
	);
    }

    return \@rows;
}

sub normalize_tag : PRIVATE {

    # Takes a string, returns the lowercase, whitespace-stripped
    # version.
    my ( $self, $tag ) = @_;

    $tag =~ s/\s+//g;
    $tag = lc($tag);

    return $tag
}

sub validate_arrayref_tags : RESTRICTED {

    # Method to check the return value from read_as_arrayref to check
    # for (a) duplicate tags, and (b) unrecognised tags. Returns a
    # hash with keys corresponding to IDF tags and value arrayrefs
    # containing the IDF annotation.

    my ($self, $array_of_rows) = @_;

    # Duplicate tag check. This is somewhat primitive at the moment,
    # and can be fooled FIXME.
    my (%seen);
    foreach my $row ( @$array_of_rows ) {

	# Two-dimensional hash; normalized then actual IDF tags.
	my $normtag = $self->normalize_tag( $row->[0] );
	$seen{ $normtag }{ $row->[0] } ++;
    }
    while ( my ($norm_tag, $idf_tags) = each %seen ) {

	# Differently typed but identical tags.
	if ( scalar (grep { defined $_ } values %$idf_tags ) > 1 ) {
	    my $tagstring = join(", ", keys %$idf_tags);
	    $self->raise_error(qq{Error: duplicated IDF tag(s): "$tagstring"});
	}

	# Identically typed duplicate tags.
	while ( my ($idf_tag, $count) = each %$idf_tags ) {
	    if ( $count > 1 ) {
		$self->raise_error(qq{Error: duplicated IDF tag: "$idf_tag"});
	    }
	}
    }

    # Hash of row tag keys with rest-of-row arrayref values.
    my %idf_hash = map
	{ $_->[0] => [ @{ $_ }[1 .. $#$_] ] }
	    @{ $array_of_rows };

    # A list of acceptable tags, expressed as qr//
    my @acceptable = keys %{ $self->get_dispatch() };
    while ( my ( $tag, $values ) = each %idf_hash ) {

	# N.B. acceptable tag REs may contain whitespace; no x option
	# here.
	next if $tag =~ /\A\s*$COMMENT_TAG\s*\z/ms;

	# Check for recognised tags here.
	unless ( first { $tag =~ /\A\s*$_\s*\z/ms } @acceptable ) {
	    $self->raise_error(qq{Error: unrecognized IDF tag(s): "$tag"});
	}

	# Empty Name tags are invalid and will cause fatal crashes
	# later; we check for them here.
	if ( $tag =~ m/name \s* \z/ixms ) {
	    foreach my $value ( @$values ) {
		warn(
		    qq{Warning: IDF Name attribute "$tag" is empty.\n}
		) unless $value;
	    }
	}
    }

    return \%idf_hash;
}

sub add_grouped_data : PRIVATE {

    # Create an ordered set of data groups indexed by $i.
    my ( $self, $group, $tag, @args ) = @_;

    for ( my $i = 0; $i <= $#args; $i++ ) {
	$group->{ident $self}[$i]{$tag} = $args[$i];
    }

    return;
}

sub add_singleton_data : PRIVATE {

    # Record a 1:n object:args relationship.
    my ( $self, $object, $tag, @args ) = @_;

    # Make a copy of @args, just in case.
    $object->{ident $self}{$tag} = [ @args ];

    return;
}

sub add_singleton_datum : PRIVATE {

    # Record a 1:1 object:arg relationship.
    my ( $self, $object, $tag, $arg ) = @_;

    $object->{ident $self}{$tag} = $arg;

    return;
}

sub add_comment : PRIVATE {

    # Comments are currently processed at the level of experiment
    # only.
    my ( $self, $name, $value ) = @_;

    $comment{ident $self}{$name} = $value;

    return;
}
    
sub add_sdrfs : PRIVATE {

    # Store the list of SDRF files.
    my ( $self, @sdrf_uri_strings ) = @_;

    my @sdrfs = map { get_filepath_from_uri(
	$_, $self->get_source_directory(),
    ) } @sdrf_uri_strings;

    $self->set_sdrfs(\@sdrfs);

    return;
}

sub retrieve_sub : RESTRICTED {

    my ( $self, $tag ) = @_;

    my $rc;

    while ( my ( $key, $sub ) = each %{ $self->get_dispatch() } ) {

	# $key may contain whitespace, no x option here.
	if ( $tag =~ /\A\s*$key\s*\z/ms ) {
	    $rc = $sub;
	}
	
	# Have to loop through the rest of the list to reset while()
	# on the hash.
    }

    return $rc;
}

sub dispatch : PRIVATE {

    my ( $self, $tag, @args ) = @_;

    unless (defined $tag) {
	confess("Error: get_dispatch needs a defined tag name.");
    }

    my $sub = $self->retrieve_sub( $tag );
    unless (defined $sub && ref $sub eq 'CODE') {
	if ( my ( $commentname ) = ( $tag =~ /\A\s*$COMMENT_TAG\s*\z/ms ) ) {
	    $self->add_comment($commentname, @args);
	}
	else {

	    # This should have been caught in validate_arrayref_tags
	    croak(qq{Error: Cannot parse the IDF tag: "$tag".});
	}
    }

    return $sub ? $sub->(@args) : undef;
}

sub get_filename : RESTRICTED {

    # Method to pass the IDF filename to check_linebreaks, called from
    # SUPER->calculate_eol_char()
    my ( $self ) = @_;
    
    return $self->get_idf();
}
    
sub get_idf_filehandle : RESTRICTED {

    my ( $self ) = @_;

    unless ( $idf_filehandle{ident $self} ) {
	if ( my $file = $self->get_idf() ) {
	    open (my $fh, '<', $file)
		or croak("Error opening IDF for reading: $!\n");
	    $idf_filehandle{ident $self} = $fh;
	}
	else {
	    confess("Error: No IDF filename given.");
	}
    }
    return $idf_filehandle{ident $self};
}

1;
