#!/usr/bin/env perl
#
# $Id: CelQC.pm 1960 2008-02-21 12:03:19Z tfrayner $

use strict;
use warnings;

package ArrayExpress::Tracking::CelQC;

use Class::Std;
use File::Temp qw(tempfile);
use Carp;
use POSIX;

my %input  : ATTR( :get<input>,  :init_arg<input>,  :default<undef> );
my %quiet  : ATTR( :get<quiet>,  :init_arg<quiet>,  :default<undef> );
my %stdout : ATTR( :name<stdout>, :default<undef> );

sub START {

    my ( $self, $id, $args ) = @_;

    unless ( defined $self->get_input() ) {
	confess(qq{Error: Input file not set.\n});
    }
    unless ( -r $self->get_input() ) {
	croak(qq{Error: Input file does not exist or is unreadable.\n});
    }

    if ( $self->get_quiet() ) {
	$self->set_stdout('/dev/null');
    }
    else {
	$self->set_stdout(POSIX::ctermid());
    }

    return;
}

sub create_r_script : PRIVATE {

    my ( $self, $reference_fh ) = @_;

    # Set up our temporary R script.
    my ( $r_fh, $r_script ) = tempfile();
    unless ( $r_fh && $r_script ) {
	croak(qq{Error: Unable to open temporary R script file "$r_script": $!\n});
    }
    while( <$reference_fh> ) {
	print $r_fh $_;
    }
    close( $r_fh )
	or croak(qq{Error: Unable to close temporary R script filehandle: $!\n});

    return $r_script;
}

sub call_r_script : PRIVATE {

    my ( $self, $output ) = @_;

    # Set up our temporary R script.
    my $r_script = $self->create_r_script( \*DATA );

    my $input  = $self->get_input();
    my $stdout = $self->get_stdout();

    # Call the R script and do the interesting stuff.
    my $syscall = "R CMD BATCH --slave --no-save --no-restore"
	. " -input=$input -output=$output $r_script $stdout";

    # Quote some legal characters which would otherwise confuse the shell.
    $syscall =~ s/([(){}\[\]])/\\$1/g;

    system($syscall) == 0
	or croak("Error executing R code: $?");

    unlink( $r_script )
	or croak(qq{Error: Unable to delete temporary R script file "$r_script": $!\n});

    return;
}

sub run_metrics {

    my ( $self ) = @_;

    # Set up our temporary output file.
    my $result_file;
    (undef, $result_file) = tempfile();

    $self->call_r_script( $result_file );

    # Parse the results file into a hash.
    open( my $result_fh, '<', $result_file )
	or croak(qq{Error: Unable to open results file "$result_file": $!\n});
    my %result;
    while ( my $line = <$result_fh> ) {
	my ( $key, $value ) = ( $line =~ m/"([^\"]+)" \s* "([^\"]+)" \s* \z/xms );
	$result{$key} = $value;
    }

    close( $result_fh )
	or croak(qq{Error closing results filehandle: $!\n});

    unlink( $result_file )
	or croak(qq{Error deleting temporary results file: $!\n});

    return \%result;
}

1;

__DATA__

# Actual R code follows; this now works, and is used to generate a
# temporary R script which is run on the input CEL file.
#
# Run using:
# DYLD_LIBRARY_PATH='' R CMD BATCH -input=<cel file> -output=<output file> <this script file>.
input<-FALSE;
output<-FALSE;

# This is a fairly generic options parsing loop, modified to store
# -input and -output values. Taken from
# https://sws.stat.iastate.edu/resources/programmingExamples/R/R-Command-Line.html
for (e in commandArgs()) {
    ta = strsplit(e,"=",fixed=TRUE);
    if(! is.na(ta[[1]][2])) {
        temp = ta[[1]][2];
        if(substr(ta[[1]][1],nchar(ta[[1]][1]),nchar(ta[[1]][1])) == "I") {
            temp = as.integer(temp);
        }
        if(substr(ta[[1]][1],nchar(ta[[1]][1]),nchar(ta[[1]][1])) == "N") {
            temp = as.numeric(temp);
        }
        assign(ta[[1]][1],temp);
        if ( ta[[1]][1] == "-input" ) {
            input = temp;
        }
        if ( ta[[1]][1] == "-output" ) {
            output = temp;
        }
#        cat("assigned ",ta[[1]][1]," the value of |",temp,"|\n");
    } else {
        assign(ta[[1]][1],TRUE);
#        cat("assigned ",ta[[1]][1]," the value of TRUE\n");
    }
}

library('simpleaffy');
scorecel <- function(infile, outfile) {
    celdata<-ReadAffy(filenames=infile);
    celqc<-qc(celdata);
    RNAdeg=AffyRNAdeg(celdata);
    celavgbg=avbg(celqc);
    perc=percent.present(celqc);
# Each tag here will correspond to a qc type in the tracking DB. The
# exception is "platform" which has its own table.
    capture.output(c("platform",cdfName(celdata)),
                      file=outfile, append=FALSE);
    capture.output(c("affy_average_background",prettyNum(as.double(celavgbg))),
                      file=outfile, append=TRUE);
    capture.output(c("affy_scale_factor",prettyNum(sfs(celqc))),
                      file=outfile, append=TRUE);
    capture.output(c("affy_percent_present",prettyNum(as.double(perc))),
                      file=outfile, append=TRUE);
    capture.output(c("affy_RNAdeg_slope",prettyNum(RNAdeg$slope)),
                      file=outfile, append=TRUE);
}
scorecel(infile=input, outfile=output);
