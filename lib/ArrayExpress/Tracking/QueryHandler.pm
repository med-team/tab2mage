#!/usr/bin/env perl
#
# $Id: QueryHandler.pm 2077 2008-06-10 09:39:46Z tfrayner $

use strict;
use warnings;

package ArrayExpress::Tracking::QueryHandler;

use Class::Std;
use Carp;
use Date::Manip qw(ParseDate UnixDate);
use Readonly;

use ArrayExpress::Curator::Database qw(
    get_ae_dbh
    get_aedw_dbh
);

my %dbhandle        : ATTR( :name<dbhandle>,        :default<{}>        );
my %event_cache     : ATTR( :name<event_cache>,     :default<undef>     );
my %last_jobid      : ATTR( :name<last_jobid>,      :default<{}>        );
my %cached_sth      : ATTR( :name<cached_sth>,      :default<{}>        );

Readonly my $AEREP_DB  => 'AEPUB1';
Readonly my $AEDW_DB   => 'AEDWDEV';

sub BUILD {

    my ( $self, $id, $args ) = @_;

    $dbhandle{ident $self}{ $AEREP_DB } = get_ae_dbh()
	or croak("Error: Unable to connect to AE repository DB.");

    $dbhandle{ident $self}{ $AEDW_DB }  = get_aedw_dbh()
	or croak("Error: Unable to connect to AE warehouse DB.");

    # Long values are trimmed at 1000 chars.
    $dbhandle{ident $self}->{ $AEREP_DB }{LongReadLen} = 1000;
    $dbhandle{ident $self}->{ $AEREP_DB }{LongTruncOk} = 1;
    $dbhandle{ident $self}->{ $AEDW_DB }{LongReadLen}  = 1000;
    $dbhandle{ident $self}->{ $AEDW_DB }{LongTruncOk}  = 1;

    return;
}

sub START {

    my ( $self, $id, $args ) = @_;

    # Set the default last job id to zero for each DB instance.
    foreach my $instance ( @{ $self->get_instances() } ) {
	$self->get_last_jobid()->{ $instance } ||= 0;
    }

    # Create our cached statement handles.
    $self->cache_statement_handles();

    return;
}

sub cache_statement_handles : PRIVATE {

    my ( $self ) = @_;

    print STDOUT "Caching statement handles...\n";

    my $dbh = $self->get_dbhandle()->{ $AEREP_DB };
    
    $cached_sth{ident $self}{expt_species} = $dbh->prepare(<<"QUERY");
select unique o.value
from tt_experiment e,
tt_biomaterials_experiments eb,
tt_poly_biomaterial pb,
tt_characteris_t_biomateri bso,
tt_ontologyentry o,
tt_identifiable i
where i.identifier = ?
and e.id = eb.experiments_id
and i.id = eb.experiments_id
and eb.biomaterials_id = pb.id
and pb.t_biosource_id is not null
and pb.t_biosource_id = bso.t_biomaterial_id
and bso.characteristics_id = o.id
and lower(o.category) = 'organism'
QUERY

    $cached_sth{ident $self}{array_species} = $dbh->prepare(<<"QUERY");
select unique oe.value
from tt_arraydesign ad,
tt_reportergro_t_arraydesi rg,
tt_designelementgroup de,
tt_ontologyentry oe,
tt_identifiable i
where i.identifier = ?
and ad.id = i.id
and rg.t_arraydesign_id = ad.id
and de.id = rg.reportergroups_id
and de.species_id = oe.id
and lower(oe.category) = 'organism'
QUERY

    $cached_sth{ident $self}{expt_arrays} = $dbh->prepare(<<"QUERY");
select unique iden.identifier
from tt_bioassays_t_experiment bt,
tt_poly_bioassay poly_b,
tt_physicalbioassay pba,
tt_bioassaycreation bc,
tt_array ar,
tt_physicalarraydesign ard,
tt_identifiable iden,
tt_identifiable i
where i.identifier = ?
and i.id = bt.t_experiment_id
and poly_b.t_physicalbioassay_id = bt.bioassays_id
and pba.id = poly_b.t_physicalbioassay_id
and pba.bioassaycreation_id = bc.id
and bc.array_id = ar.id
and ar.arraydesign_id = ard.id
and ard.id = iden.id
QUERY

    $cached_sth{ident $self}{num_samples} = $dbh->prepare(<<"QUERY");
select count (unique pb.t_biosource_id) as samples
from tt_biomaterials_experiments be,
tt_biomaterial bm,
tt_poly_biomaterial pb,
tt_biosample bs,
tt_identifiable i
where i.identifier = ?
and be.experiments_id = i.id
and be.biomaterials_id = bm.id
and bm.id = pb.t_biosource_id
QUERY

    $cached_sth{ident $self}{num_hybridizations} = $dbh->prepare(<<"QUERY");
select count( cp.t_hybridization_id ) as hybs
from tt_experiment e,
tt_bioassays_t_experiment eb,
tt_poly_bioassay bp,
tt_physicalbioassay pba,
tt_poly_bioassaycreation cp,
tt_identifiable i
where i.identifier = ?
and e.id = i.id
and e.id = eb.t_experiment_id
and eb.bioassays_id = bp.t_physicalbioassay_id
and bp.t_physicalbioassay_id = pba.id
and pba.bioassaycreation_id = cp.id
and cp.t_hybridization_id is not null
QUERY

    $cached_sth{ident $self}{has_raw_data} = $dbh->prepare(<<"QUERY");
select count(*)
from tt_identifiable i,
tt_bioassaydatagroup bg,
tt_bioassaydat_bioassaydat bb,
tt_poly_bioassaydata pb
where i.identifier = ?
and i.id = bg.experiment_id
and bg.id=bb.bioassaydatagroups_id
and bb.bioassaydatas_id = pb.t_measuredbioassaydata_id
QUERY

    $cached_sth{ident $self}{has_processed_data} = $dbh->prepare(<<"QUERY");
select count(*)
from tt_identifiable i,
tt_bioassaydatagroup bg,
tt_bioassaydat_bioassaydat bb,
tt_poly_bioassaydata pb
where i.identifier = ?
and i.id = bg.experiment_id
and bg.id=bb.bioassaydatagroups_id
and bb.bioassaydatas_id = pb.t_derivedbioassaydata_id
QUERY

    $cached_sth{ident $self}{expt_factors} = $dbh->prepare(<<"QUERY");
select oe.value as value
from tt_experimentdesign ed,
tt_experimentalfactor ef,
tt_ontologyentry oe,
tt_identifiable i
where i.identifier = ?
and ed.t_experiment_id = i.id
and ef.t_experimentdesign_id = ed.id
and oe.id = ef.category_id
QUERY

    $cached_sth{ident $self}{expt_qts} = $dbh->prepare(<<"QUERY");
select unique iden.name as name
from tt_bioassaydatagroup bg,
tt_bioassaydat_bioassaydat bb,
tt_bioassaydata ba,
tt_quantitationtypedimension qtd,
tt_quantitatio_t_quantitat qtq,
tt_quantitationtype qt,
tt_identifiable iden,
tt_identifiable i
where i.identifier = ?
and bg.experiment_id = i.id
and bg.id=bb.bioassaydatagroups_id
and ba.id = bb.bioassaydatas_id
and qtd.id = ba.quantitationtypedimension_id
and qtq.t_quantitationtypedimension_id = ba.quantitationtypedimension_id
and qt.id = qtq.quantitationtypes_id
and iden.id = qt.id
QUERY

    $cached_sth{ident $self}{is_released} = $dbh->prepare(<<"QUERY");
select usr.name
from pl_visibility vi,
pl_label la,
tt_identifiable i,
pl_user usr
where i.identifier = ?
and i.identifier = la.mainobj_name
and la.id = vi.label_id
and vi.user_id = usr.id
and usr.name = 'guest'
QUERY

    $cached_sth{ident $self}{is_loaded} = $dbh->prepare(<<"QUERY");
select count(*)
from pl_label la,
tt_identifiable i
where i.identifier = ?
and i.identifier = la.mainobj_name
QUERY

    $cached_sth{ident $self}{release_date} = $dbh->prepare(<<"QUERY");
select nvt.value
from tt_namevaluetype nvt,
tt_identifiable i
where i.identifier = ?
and nvt.t_extendable_id = i.id
and nvt.name ='ArrayExpressReleaseDate'
QUERY

    $cached_sth{ident $self}{ae_miame_score} = $dbh->prepare(<<"QUERY");
select nvt.value
from tt_namevaluetype nvt,
tt_identifiable i
where i.identifier = ?
and nvt.t_extendable_id = i.id
and nvt.name ='AEMIAMESCORE'
QUERY

    $cached_sth{ident $self}{curated_name} = $dbh->prepare(<<"QUERY");
select nvt.value
from tt_namevaluetype nvt,
tt_identifiable i
where i.identifier = ?
and nvt.t_extendable_id = i.id
and nvt.name ='AEExperimentDisplayName'
and nvt.value != i.name
QUERY

    $cached_sth{ident $self}{submitter_description} = $dbh->prepare(<<"QUERY");
select d.text
from tt_description d,
tt_identifiable i
where i.identifier = ?
and d.t_describable_id = i.id
and d.text not like '(Generated description)%'
and length(d.text) > 0
QUERY
    
    return;
}

sub get_instances {

    my ( $self ) = @_;

    return [ keys %{ $self->get_dbhandle() } ];
}

sub get_experiments {

    my ( $self ) = @_;

    # FIXME we might want to extend this to include the AEDW.
    my $dbh = $self->get_dbhandle()->{ $AEREP_DB };

    my $sth = $dbh->prepare(<<"QUERY");
select i.identifier
from tt_identifiable i,
tt_experiment e
where e.id = i.id
QUERY

    $sth->execute() or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return scalar @{ $results }
           ? [ map { $_->[0] } @{ $results } ]
	   : [];
}

sub get_array_designs {

    # FIXME we might want to extend this to include the AEDW.
    my ( $self ) = @_;

    my $dbh = $self->get_dbhandle()->{ $AEREP_DB };

    my $sth = $dbh->prepare(<<"QUERY");
select i.identifier
from tt_identifiable i,
tt_physicalarraydesign a
where a.id = i.id
QUERY

    $sth->execute() or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return scalar @{ $results }
           ? [ map { $_->[0] } @{ $results } ]
	   : [];
}

sub get_array_species {

    my ( $self, $accession ) = @_;

    # Query returns Organism OE values.
    my $sth = $self->get_cached_sth()->{array_species}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );

    my $results = $sth->fetchall_arrayref();

    return scalar @{ $results }
           ? [ map { $_->[0] } @{ $results } ]
	   : [];
}

sub get_expt_species {

    my ( $self, $accession ) = @_;

    # Query returns Organism OE values.
    my $sth = $self->get_cached_sth()->{expt_species}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );

    my $results = $sth->fetchall_arrayref();

    return scalar @{ $results }
           ? [ map { $_->[0] } @{ $results } ]
	   : [];
}

sub get_expt_arrays {

    my ( $self, $accession ) = @_;

    # Query returns array accessions.
    my $sth = $self->get_cached_sth()->{expt_arrays}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return scalar @{ $results }
           ? [ map { $_->[0] } @{ $results } ]
	   : [];
}

sub get_expt_factors {

    my ( $self, $accession ) = @_;

    # Query returns ExperimentalFactorCategory OE values.
    my $sth = $self->get_cached_sth()->{expt_factors}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return scalar @{ $results }
           ? [ map { $_->[0] } @{ $results } ]
	   : [];
}

sub get_expt_qts {

    my ( $self, $accession ) = @_;

    # Query returns QT names.
    my $sth = $self->get_cached_sth()->{expt_qts}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return scalar @{ $results }
           ? [ map { $_->[0] } @{ $results } ]
	   : [];
}

sub get_submitter_description {

    my ( $self, $accession ) = @_;

    # Query returns Description texts.
    my $sth = $self->get_cached_sth()->{submitter_description}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return $results->[0][0];
}

sub get_curated_name {

    my ( $self, $accession ) = @_;

    # Query returns the curated name (AEExperimentDisplayName NVT).
    my $sth = $self->get_cached_sth()->{curated_name}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return $results->[0][0];
}

sub get_num_samples {

    my ( $self, $accession ) = @_;

    # Query returns count of samples.
    my $sth = $self->get_cached_sth()->{num_samples}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return $results->[0][0];
}

sub get_num_hybridizations {

    my ( $self, $accession ) = @_;

    # Query returns count of samples.
    my $sth = $self->get_cached_sth()->{num_hybridizations}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return $results->[0][0];
}

sub get_has_raw_data {

    my ( $self, $accession ) = @_;

    # Query returns count of MBADatas.
    my $sth = $self->get_cached_sth()->{has_raw_data}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return $results->[0][0] ? 1 : 0;
}

sub get_has_processed_data {

    my ( $self, $accession ) = @_;

    # Query returns count of DBADatas.
    my $sth = $self->get_cached_sth()->{has_processed_data}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return $results->[0][0] ? 1 : 0;
}

sub get_release_date {

    my ( $self, $accession ) = @_;

    # Query returns the release date (ArrayExpressReleaseDate NVT).
    my $sth = $self->get_cached_sth()->{release_date}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return $results->[0][0];
}

sub get_is_released {

    my ( $self, $accession ) = @_;

    # If query returns anything, object is public.
    my $sth = $self->get_cached_sth()->{is_released}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return scalar @{ $results } ? 1 : 0;
}

sub get_is_loaded {

    my ( $self, $accession ) = @_;

    # If query returns > 0, object is loaded.
    my $sth = $self->get_cached_sth()->{is_loaded}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return $results->[0][0];
}

sub get_expt_in_data_warehouse {

    my ( $self, $accession ) = @_;

    my $dbh = $self->get_dbhandle()->{ $AEDW_DB };

    # If query returns > 0, object is loaded.
    my $sth = $dbh->prepare(<<"QUERY");
select experiment_identifier
from ae1__experiment__main
where experiment_identifier = ?
QUERY

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return scalar( @$results );
}

sub get_array_in_data_warehouse {

    my ( $self, $accession ) = @_;

    my $dbh = $self->get_dbhandle()->{ $AEDW_DB };

    # If query returns > 0, object is loaded.
    my $sth = $dbh->prepare(<<"QUERY");
select arraydesign_identifier
from ae2__arraydesign
where arraydesign_identifier = ?
QUERY

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return scalar( @$results );
}

sub get_ae_miame_score {

    my ( $self, $accession ) = @_;

    # Query returns the AE-computed MIAME score (AEMIAMESCORE NVT).
    my $sth = $self->get_cached_sth()->{ae_miame_score}
	or die("Error: Undefined statement handle.");

    $sth->execute( $accession ) or die( $sth->errstr() );
    
    my $results = $sth->fetchall_arrayref();

    return $results->[0][0];
}

sub get_ae_data_warehouse_score {

    my ( $self, $accession ) = @_;

    confess("Not implemented yet"); # FIXME once this has been implemented in AE.
}

sub get_events {

    my ( $self, $accession ) = @_;

    my $events = $self->get_event_cache();

    unless ( $events ) {

	# Cache our jobregister events.
	$self->cache_aerep_events();
	$self->cache_aedw_events();
    }

    return $self->get_event_cache()->{ $accession } || [];
}

sub get_updated_event_data {

    my ( $self, $dbid, $instance ) = @_;

    my $dbh = $self->get_dbhandle()->{ $instance }
	or confess("Error: unrecognized database instance $instance.");

    my $sth = $dbh->prepare(<<"QUERY");
select ENDTIME, PHASE
from jobregister
where id = ?
and starttime is not null
QUERY

    $sth->execute( $dbid ) or die( $sth->errstr );

    # This query is by primary key, so we can rely on only one row
    # returned.
    my $results = $sth->fetchrow_hashref();
    my $success = $self->parse_job_phase( $results->{'PHASE'} );
    my $endtime;
    if ( defined( $results->{'ENDTIME'} ) ) {
	my $date = ParseDate($results->{'ENDTIME'});
	$endtime = UnixDate($date, "%Y-%m-%d %T");
    }

    return ( $endtime, $success );
}

sub cache_aerep_events : PRIVATE {

    my ( $self ) = @_;

    print STDOUT "Caching AE repository events...\n";

    $self->cache_events( $AEREP_DB );

    return;
}

sub cache_aedw_events : PRIVATE {

    my ( $self ) = @_;

    print STDOUT "Caching AE warehouse events...\n";

    $self->cache_events( $AEDW_DB );

    return;
}

sub cache_events : PRIVATE {

    my ( $self, $instance ) = @_;

    my $dbh = $self->get_dbhandle()->{ $instance }
	or confess("Error: Undefined database handle ($instance).");

    # Note that currently the AEDW and AEREP jobregister schemas are
    # identical, so we use this code for both. We may need to change
    # this if these schemas change.

    # Populate the event_cache with ArrayExpress::Tracking::Event
    # objects here.
    my $sth = $dbh->prepare(<<"QUERY");
select distinct ID, USERNAME, DIRNAME, JOBTYPE, STARTTIME, ENDTIME, PHASE
from jobregister
where id > ?
QUERY

    $sth->execute( $self->get_last_jobid()->{ $instance } || 0 )
	or die( $sth->errstr() );

    my $event = $self->get_event_cache() || {};

    JOB:
    while ( my $hashref = $sth->fetchrow_hashref() ) {

	# FIXME this regexp will miss some accessions (e.g. E-TABM-145b).
	my ( $accession )
	    = ( $hashref->{'DIRNAME'} =~ m/\/([AE]-[A-Z]{4}-[0-9]+)/ );
	next JOB unless $accession;

	my $obj = $self->make_event_object( $hashref, $instance );
	push( @{ $event->{$accession} }, $obj );
    }

    $self->set_event_cache($event);

    return;
}

sub make_event_object : PRIVATE {

    my ( $self, $hashref, $instance ) = @_;

    # N.B. currently the AEREP and AEDW jobregister tables have
    # identical columns; we may need to make database-specific
    # versions of this method if that ever changes.

    my $success = $self->parse_job_phase( $hashref->{'PHASE'} );

    # Here we map the AEREP column names to our events. Attributes not
    # set: source_db, machine, log_file, comment. Note that we need to
    # parse the returned date format here.
    foreach my $key qw(STARTTIME ENDTIME) {
	my $date = ParseDate($hashref->{$key});
	$hashref->{$key} = UnixDate($date, "%Y-%m-%d %T");
    }

    my $obj = ArrayExpress::Tracking::Event->new({
	event_type       => $hashref->{'JOBTYPE'},
	success          => $success,
	target_db        => $instance,
	starttime        => $hashref->{'STARTTIME'},
	endtime          => $hashref->{'ENDTIME'},
	operator         => $hashref->{'USERNAME'},
	jobregister_dbid => $hashref->{'ID'},
    });

    return $obj;
}

sub parse_job_phase : PRIVATE {

    my ( $self, $phase ) = @_;

    # Return 1 if finished, 0 if crashed, or undef if unknown (assumed
    # elsewhere to mean unfinished).

    if    ( $phase && $phase =~ /\b (?: crashed | error | dead ) \b/ixms ) {

	# Failure.
	return 0;
    }
    elsif ( $phase && $phase =~ /\b (?: finish | finished ) \b/ixms ) {

	# Success.
	return 1;
    }
    else{

	# Unfinished?
	return;
    }
}

1;
