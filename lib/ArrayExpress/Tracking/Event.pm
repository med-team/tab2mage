#!/usr/bin/env perl
#
# $Id: Event.pm 1853 2007-12-13 17:53:43Z tfrayner $

use strict;
use warnings;

package ArrayExpress::Tracking::Event;

# Class used as a bridge between the AE database query objects and the
# tracking database.

use Class::Std;

my %event_type       : ATTR( :name<event_type>,       :default<undef> );
my %success          : ATTR( :name<success>,          :default<undef> );
my %source_db        : ATTR( :name<source_db>,        :default<undef> );
my %target_db        : ATTR( :name<target_db>,        :default<undef> );
my %starttime        : ATTR( :name<starttime>,        :default<undef> );
my %endtime          : ATTR( :name<endtime>,          :default<undef> );
my %machine          : ATTR( :name<machine>,          :default<undef> );
my %operator         : ATTR( :name<operator>,         :default<undef> );
my %log_file         : ATTR( :name<log_file>,         :default<undef> );
my %jobregister_dbid : ATTR( :name<jobregister_dbid>, :default<undef> );
my %comment          : ATTR( :name<comment>,          :default<undef> );

1;
