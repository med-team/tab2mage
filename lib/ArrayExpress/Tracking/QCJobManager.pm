# $Id: QCJobManager.pm 1978 2008-02-28 12:14:14Z tfrayner $

package ArrayExpress::Tracking::QCJobManager;

use strict;
use warnings;

use Class::Std;
use Carp;

use ArrayExpress::AutoSubmission::DB;

# Attributes.
my %ae_sth           : ATTR( :name<ae_sth>,          :default<undef>   );
my %ae_dbh           : ATTR( :name<ae_dbh>,          :default<undef>   );
my %num_procs        : ATTR( :name<num_procs>,       :default<1>       );

my $CHILD_SCRIPT = 'cel_qcstats.pl';

sub START {

    my ( $self, $id, $args ) = @_;

    unless ( $self->get_ae_sth() ) {
	confess("Error: no ae_sth set in " . __PACKAGE__);
    }
    unless ( $self->get_ae_dbh() ) {
	confess("Error: no ae_dbh set in " . __PACKAGE__);
    }

    return;
}

sub run {

    # Main bjob management event loop.

    my ( $self ) = @_;

    # Start an initial run of $self->get_num_procs();
    $self->start_new_set();

    # Approx 30 seconds per process.
    my $sleep_period = 30;
    sleep( $sleep_period );

    while ( 1 ) {

	# Loop ends when there are no more rows to process.
	$self->start_new_set() or return;
	sleep( $sleep_period );
    }

    return;
}

sub count_submitted_jobs : PRIVATE {

    my ( $self ) = @_;

    open ( my $bjobs, '-|', 'bjobs -w 2>&1' )
	or croak("Error setting up bjobs handle: $!");

    my $count = 0;
    while ( my $out = <$bjobs> ) {
	$count++ if ( $out =~ m/$CHILD_SCRIPT/ );
    }

    close( $bjobs ) or carp( $! ? "Error closing bjobs pipe: $!"
			        : "Exit status $? from bjobs." );

    return $count;
}

sub get_old_hashed_data {

    print( q{Caching data identifiers...} );

    my $hashed_data = {};

    my $dbh = ArrayExpress::AutoSubmission::DB->db_Main();

    my $sth = $dbh->prepare(<<'QUERY');
select d.identifier, d.needs_metrics_calculation
from loaded_data d
where is_deleted=0
QUERY

    $sth->execute() or die( $sth->errstr() );

    while ( my $row = $sth->fetchrow_hashref( 'NAME_lc' ) ) {
	$hashed_data->{ $row->{'identifier'} }
	    = $row->{'needs_metrics_calculation'};
    }

    $sth->finish();

    print( qq{ done.\n} );

    return $hashed_data;
}

{
    # Hash of identifier => needs_metrics_calculation
    my $cached_query;

sub start_new_set : PRIVATE {

    my ( $self ) = @_;

    # Quick look to see if the data has been already hashed.
    $cached_query ||= get_old_hashed_data();

    my $sth = $self->get_ae_sth();

    # Double-check that the statement handle has been executed.
    unless ( $sth->{'Executed'} ) {
	$sth->execute() or die( $sth->errstr() );
    }

    my $prev = $self->count_submitted_jobs();

    PROC:
    for ( my $i = $prev; $i < $self->get_num_procs(); $i++ ) {
	my $hashref = $sth->fetchrow_hashref('NAME_lc');

	# This would be the endpoint.
	return unless $hashref;

	# Skip those rows which don't need calculations.
	redo PROC unless $cached_query->{ $hashref->{'identifier'} };

	$self->process_row( $hashref );
    }

    return 1;
}

}

sub process_row : PRIVATE {

    my ( $self, $hashref ) = @_;

    my $ae_dbh = $self->get_ae_dbh();

    # Get the CEL file, dispatch the subprocess FIXME.
    my $lob_locator = $hashref->{'lob'};
    my $length      = $ae_dbh->ora_lob_length( $lob_locator );
    my $chunksize   = 65536;

    # Read in the LOB and write it to a file.
    my $filename = $hashref->{'dbid'} . '.cel';
    open( my $fh, '>', $filename )
	or croak("Error: unable to open file for writing: $!");
    for ( my $i = 1; $i <= $length; $i += $chunksize ) {
	my $chunk = $ae_dbh->ora_lob_read( $lob_locator, $i, $chunksize );
	syswrite( $fh, $chunk ) or croak("Error writing to file: $!");
    }
    close( $fh ) or croak("Error closing filehandle: $!");

    # Make sure our log file at least exists, otherwise we get a whole
    # lot of silent failure from the oh-so-wonderful LSF system.
    my $logfile = 'legacy_qc.log';
    unless ( -e $logfile ) {
	open( my $fh, '>', $logfile )
	    or croak("Error initialising log file: $!\n");
	close( $fh );
    }

    # The subprocess will need to delete the file when done, hence the -d option.
    system(
	qq{bsub -q production -o "$logfile" }
      . qq{'$CHILD_SCRIPT -i "$hashref->{identifier}" -f $filename -q -d'}
    ) == 0 or croak("Error submitting LSF process.");

    return;
}

1;
